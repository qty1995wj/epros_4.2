package com.jecn.epros.server.service.dataImport.sync.excelOrDb.buss;

import java.io.File;
import java.util.List;

import jxl.Workbook;
import jxl.format.Alignment;
import jxl.format.Border;
import jxl.format.BorderLineStyle;
import jxl.format.Colour;
import jxl.format.UnderlineStyle;
import jxl.format.VerticalAlignment;
import jxl.write.Label;
import jxl.write.NumberFormats;
import jxl.write.WritableCellFormat;
import jxl.write.WritableFont;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
import jxl.write.WriteException;
import jxl.write.biff.RowsExceededException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.jecn.epros.server.bean.dataimport.BasePosBean;
import com.jecn.epros.server.bean.dataimport.JecnBasePosBean;
import com.jecn.epros.server.service.dataImport.sync.excelOrDb.unit.SyncConstant;
import com.jecn.epros.server.service.dataImport.sync.excelOrDb.unit.SyncTool;
import com.jecn.epros.server.util.JecnUtil;
import com.jecn.epros.server.webBean.dataImport.sync.excelOrDB.DeptBean;
import com.jecn.epros.server.webBean.dataImport.sync.excelOrDB.UserBean;

/**
 * 
 * 文件导出处理、本机生成excel保存到本地
 * 
 * @author Administrator
 * 
 */
public abstract class AbstractFileSaveDownBuss extends AbstractFileDownBuss {

	protected final Log log = LogFactory.getLog(this.getClass());
	/** 导出excel模板路径 */
	protected String outputExcelModelPath = null;
	/** 错误导出excel数据路径 */
	protected String outputExcelDataPath = null;
	/** 导出人员及岗位数据时，单个Sheet的行数 */
	protected final int userPosSheetLen = 10000;

	/**
	 * 
	 * 从DB中读数据写入到excel再保存到本地
	 * 
	 * @param deptBeanList
	 *            部门
	 * 
	 * @param userBeanList
	 *            人员 岗位
	 * @throws Exception
	 * @return boolean
	 * 
	 */
	protected boolean editExcel(List<DeptBean> deptBeanList, List<UserBean> userBeanList, boolean hasErrorColumn) {
		log.info("错误信息存储开始，错误信息存放路径：" + this.getOutputExcelDataPath());

		WritableWorkbook wbookData = null;
		try {
			// 判断模板文件路径和待保存文件路径是否
			if (SyncTool.isNullOrEmtryTrim(this.getOutputExcelModelPath())
					|| SyncTool.isNullOrEmtryTrim(this.getOutputExcelDataPath())) {
				return false;
			}

			// 人员导出数据文件 不是文件返回
			File fileData = new File(this.getOutputExcelDataPath());

			wbookData = Workbook.createWorkbook(fileData);
			// 创建两个sheet
			// 部门
			WritableSheet deptSheet = wbookData.createSheet("部门", 0);
			wbookData.createSheet("人员及岗位", 1);
			// 创建人员岗位Sheet
			createUserPosSheetArray(wbookData, userBeanList.size());
			// 获取所有Sheet
			WritableSheet[] allSheetArray = wbookData.getSheets();
			// 编辑部门人员岗位数据
			editDept(deptSheet, deptBeanList,hasErrorColumn);
			editUserPos(allSheetArray, userBeanList,hasErrorColumn);
			// 写入本地
			wbookData.write();
			// workbook在调用close方法时，才向输出流中写入数据
			wbookData.close();
			wbookData = null;

			log.info("错误信息存储结束，错误信息存放路径：" + this.getOutputExcelDataPath());
		} catch (Exception ex) {
			log.error("人员数据导出失败：" + this.getClass().getName() + "类editExcel方法 " + ex.getMessage());
            ex.printStackTrace();
			return false;

		} finally {

			if (wbookData != null) {
				try {
					wbookData.close();
				} catch (Exception e) {
					log.error("人员数据导出失败：" + this.getClass().getName() + "类editExcel方法 " + e.getMessage());
					return false;
				}
			}
		}
		return true;
	}

	/**
	 * 
	 * 从DB中读数据写入到excel再保存到本地
	 * 
	 * @param deptBeanList
	 *            部门
	 * 
	 * @param userBeanList
	 *            人员 岗位
	 * @param jecnBasePosBeanList
	 *            基准岗位
	 * @param basePosRelBeanList
	 *            基准岗位与岗位关系
	 * @throws Exception
	 * @return boolean
	 * 
	 */
	protected boolean editExcel(List<DeptBean> deptBeanList, List<UserBean> userBeanList,
			List<JecnBasePosBean> jecnBasePosBeanList, List<BasePosBean> basePosRelBeanList ,boolean hasErrorColumn) {
		log.info("错误信息存储开始，错误信息存放路径：" + this.getOutputExcelDataPath());

		WritableWorkbook wbookData = null;
		try {

			// 判断模板文件路径和待保存文件路径是否
			if (SyncTool.isNullOrEmtryTrim(this.getOutputExcelModelPath())
					|| SyncTool.isNullOrEmtryTrim(this.getOutputExcelDataPath())) {
				return false;
			}

			// 人员导出数据文件 不是文件返回
			File fileData = new File(this.getOutputExcelDataPath());

			wbookData = Workbook.createWorkbook(fileData);
			
			// 创建sheet
			wbookData.createSheet("部门", 0);
			wbookData.createSheet("人员及岗位", 1);
			wbookData.createSheet("基准岗位", 2);
			wbookData.createSheet("基准岗位所包含岗位", 3);

			// 获取sheet
			WritableSheet[] sheetArry = wbookData.getSheets();
			if (sheetArry.length != 4) {
				return false;
			}

			// 部门
			WritableSheet deptSheet = sheetArry[0];
			// 创建人员岗位Sheet
			createUserPosSheetArray(wbookData, userBeanList.size());
			// 获取所有Sheet
			WritableSheet[] allSheetArray = wbookData.getSheets();

			// 编辑部门人员岗位数据
			editDept(deptSheet, deptBeanList ,hasErrorColumn);
			int index = editUserPos(allSheetArray, userBeanList ,hasErrorColumn);
			if (index == 1) {
				index++;
			}
			// 编辑基准岗位
			editBasePos(allSheetArray[index], jecnBasePosBeanList);
			index++;
			// 编辑基准岗位与岗位关系
			editBaseRelPos(allSheetArray[index], basePosRelBeanList);

			// 写入本地
			wbookData.write();
			// workbook在调用close方法时，才向输出流中写入数据
			wbookData.close();
			wbookData = null;

			log.info("错误信息存储结束，错误信息存放路径：" + this.getOutputExcelDataPath());
		} catch (Exception ex) {
			log.error("人员数据导出失败：" + this.getClass().getName() + "类editExcel方法 " + ex.getMessage());

			return false;

		} finally {

			if (wbookData != null) {
				try {
					wbookData.close();
				} catch (Exception e) {
					log.error("人员数据导出失败：" + this.getClass().getName() + "类editExcel方法 " + e.getMessage());
					return false;
				}
			}
		}

		return true;
	}

	/**
	 * 编辑基准岗位与岗位关系
<<<<<<< .working
	 * @throws Exception 
=======
	 * @param hasErrorColumn 
	 * @throws Exception 
>>>>>>> .merge-right.r31601
	 * 
	 * 
	 * 
	 * **/
	protected void editBaseRelPos(WritableSheet baseSheet, List<BasePosBean> basePosRelBeanList)
			throws Exception {
		
		createBaseRelPosTitle(baseSheet);
		
		int index = 0;
		for (BasePosBean basePosBean : basePosRelBeanList) {
			// 行数
			int rowNum = index + SyncConstant.WIRTER_ROW_NUM;
			index++;
			// 基准岗位名称
			baseSheet.addCell(new Label(0, rowNum, basePosBean.getBasePosName()));
			// 岗位名称
			baseSheet.addCell(new Label(1, rowNum, basePosBean.getPosName()));
			// 错误信息
//			 baseSheet.addCell(new
//			 Label(2,rowNum,SyncTool.nullToEmtryByObj(jecnPoseBean.getError())));
		}
	}
	
	private void createBaseRelPosTitle(WritableSheet wsheet) throws Exception {
		int col=0;
		int row=0;
		wsheet.setRowView(row, 400);
		// 表头的样式 背景为黄色 字体为红色
		WritableCellFormat titleStyleY = getSheetTitleStyleY();
		// ------------------------写出表头------------------------
		// -------------调整列宽
		wsheet.setColumnView(col++, 28);
		wsheet.setColumnView(col++, 28);
		wsheet.setColumnView(col++, 28);

		col=0;
		// 设置高度
		wsheet.setRowView(row, 800);
		// 表头
		wsheet.addCell(new Label(col++, row, "", titleStyleY));
		wsheet.addCell(new Label(col++, row, "此岗位名称指基准岗位所包含的岗位名称", titleStyleY));
		wsheet.addCell(new Label(col++, row, "", titleStyleY));
		
		row++;
		col=0;
		// 设置高度
		wsheet.setRowView(row, 400);
		// 表头
		wsheet.addCell(new Label(col++, row, "基准岗位编号", titleStyleY));
		wsheet.addCell(new Label(col++, row, "岗位名称", titleStyleY));
		wsheet.addCell(new Label(col++, row, "错误信息", titleStyleY));
		
		row++;
		col=0;
		// 设置高度
		wsheet.setRowView(row, 400);
		// 表头
		wsheet.addCell(new Label(col++, row, "必填", titleStyleY));
		wsheet.addCell(new Label(col++, row, "必填", titleStyleY));
		wsheet.addCell(new Label(col++, row, "", titleStyleY));
	}


	/**
	 * 编辑基准岗位
	 * 
	 * @param baseSheet
	 *            基准岗位sheet jecnBasePosBeanList 基准岗位
	 * 
	 * @author cheshaowei
	 * @param hasErrorColumn 
	 * @throws Exception 
	 * 
	 * */
	protected void editBasePos(WritableSheet baseSheet, List<JecnBasePosBean> jecnBasePosBeanList)
			throws Exception {
		
		createBasePosTitle(baseSheet);
		
		int index = 0;
		for (JecnBasePosBean jecnBasePosBean : jecnBasePosBeanList) {
			// 行数
			int rowNum = index + SyncConstant.WIRTER_ROW_NUM;
			index++;
			// 岗位编号
			baseSheet.addCell(new Label(0, rowNum, jecnBasePosBean.getBasePosNum()));
			// 岗位名称
			baseSheet.addCell(new Label(1, rowNum, jecnBasePosBean.getBasePosName()));
			// 错误信息
			baseSheet.addCell(new Label(2, rowNum, SyncTool.nullToEmtryByObj(jecnBasePosBean.getError())));
		}
	}
	
	private void createBasePosTitle(WritableSheet wsheet) throws Exception {
		int col=0;
		int row=0;
		wsheet.setRowView(row, 400);
		// 表头的样式 背景为黄色 字体为红色
		WritableCellFormat titleStyleY = getSheetTitleStyleY();
		// ------------------------写出表头------------------------
		// -------------调整列宽
		wsheet.setColumnView(col++, 28);
		wsheet.setColumnView(col++, 28);
		wsheet.setColumnView(col++, 28);

		col=0;
		// 设置高度
		wsheet.setRowView(row, 800);
		// 表头
		wsheet.addCell(new Label(col++, row, "基准编号唯一", titleStyleY));
		wsheet.addCell(new Label(col++, row, "", titleStyleY));
		wsheet.addCell(new Label(col++, row, "", titleStyleY));
		
		row++;
		col=0;
		// 设置高度
		wsheet.setRowView(row, 400);
		// 表头
		wsheet.addCell(new Label(col++, row, "基准岗位编号", titleStyleY));
		wsheet.addCell(new Label(col++, row, "基准岗位名称", titleStyleY));
		wsheet.addCell(new Label(col++, row, "错误信息", titleStyleY));
		
		row++;
		col=0;
		// 设置高度
		wsheet.setRowView(row, 400);
		// 表头
		wsheet.addCell(new Label(col++, row, "必填", titleStyleY));
		wsheet.addCell(new Label(col++, row, "必填", titleStyleY));
		wsheet.addCell(new Label(col++, row, "", titleStyleY));
	}

	/**
	 * 创建人员及岗位工作表
	 * 
	 * @author weidp
	 * @date 2014-7-29 上午11:35:08
	 * @param wbookData
	 *            excel文件对象
	 * @param dataSize
	 *            数据长度
	 */
	private void createUserPosSheetArray(WritableWorkbook wbookData, int dataSize) {
		// 获取需要生成的Sheet数量
		int arrayLen = dataSize / userPosSheetLen + (dataSize % userPosSheetLen == 0 ? 0 : 1);
		// 数据超过1个Sheet时，修改第二个sheet的名字为 人员及岗位(1)
		if (arrayLen > 1) {
			wbookData.getSheet(1).setName(JecnUtil.getValue("userAndPos") + " (1)");
		}
		// 生成2-》arrayLen的Sheet
		for (int index = 2; index <= arrayLen; index++) {
			// 复制人员及岗位Sheet，生成新的人员及岗位Sheet
			wbookData.copySheet(1, JecnUtil.getValue("userAndPos") + " (" + index + ")", index);
		}
	}

	/**
	 * 
	 * 编辑部门sheet，把DB中部门数据写入给定sheet中
	 * 
	 * @param deptSheet
	 *            部门sheet
	 * @param deptBeanList部门数据
	 * @throws Exception 
	 */
	protected void editDept(WritableSheet deptSheet, List<DeptBean> deptBeanList,boolean hasErrorColumn) throws Exception {
		// 写出表头
		createDeptTitle(deptSheet,hasErrorColumn);
        // 写出内容
		createDeptContent(deptSheet, deptBeanList,hasErrorColumn);
	}

	private void createDeptContent(WritableSheet deptSheet,
			List<DeptBean> deptBeanList, boolean hasErrorColumn) throws WriteException,
			RowsExceededException {
		
		// 添加部门数据
		for (int i = 0; i < deptBeanList.size(); i++) {
			// 获取一条部门数据
			DeptBean deptBean = deptBeanList.get(i);
			// 部门编号为空，退出本次循环
			// if (SyncTool.isNullOrEmtryTrim(deptBean.getDeptNum())) {
			// continue;
			// }
			// 行数
			int rowNum = i + SyncConstant.WIRTER_ROW_NUM;

			// 部门编号
			deptSheet.addCell(new Label(0, rowNum, deptBean.getDeptNum()));
			// 部门名称
			deptSheet.addCell(new Label(1, rowNum, deptBean.getDeptName()));
			// 上级部门编号
			deptSheet.addCell(new Label(2, rowNum, deptBean.getPerDeptNum()));
			if (hasErrorColumn){
				// 错误信息
				deptSheet.addCell(new Label(3, rowNum, SyncTool.nullToEmtryByObj(deptBean.getError())));
			}
		}
	}

	private void createDeptTitle(WritableSheet wsheet, boolean hasErrorColumn) throws Exception {
		int col=0;
		int row=0;
		wsheet.setRowView(row, 400);
		// 表头的样式 背景为黄色 字体为红色
		WritableCellFormat titleStyleY = getSheetTitleStyleY();
		// ------------------------写出表头------------------------
		// -------------调整列宽
		wsheet.setColumnView(col++, 28);
		wsheet.setColumnView(col++, 28);
		wsheet.setColumnView(col++, 28);
		if(hasErrorColumn){
			wsheet.setColumnView(col++, 40);
		}

		col=0;
		// 设置高度
		wsheet.setRowView(row, 800);
		// 表头
		wsheet.addCell(new Label(col++, row, "部门编号唯一\n**谨慎修改**", titleStyleY));
		wsheet.addCell(new Label(col++, row, "", titleStyleY));
		wsheet.addCell(new Label(col++, row, "顶级部门的上级部门填写为0，顶级部门有且只有一个\n**谨慎修改**", titleStyleY));
		if(hasErrorColumn){
			wsheet.addCell(new Label(col++, row, "", titleStyleY));
		}
		
		row++;
		col=0;
		// 设置高度
		wsheet.setRowView(row, 400);
		// 表头
		wsheet.addCell(new Label(col++, row, "部门编号", titleStyleY));
		wsheet.addCell(new Label(col++, row, "部门名称", titleStyleY));
		wsheet.addCell(new Label(col++, row, "上级部门编号", titleStyleY));
		if(hasErrorColumn){
			wsheet.addCell(new Label(col++, row, "错误信息", titleStyleY));
		}
		
		row++;
		col=0;
		// 设置高度
		wsheet.setRowView(row, 400);
		// 表头
		wsheet.addCell(new Label(col++, row, "必填", titleStyleY));
		wsheet.addCell(new Label(col++, row, "必填", titleStyleY));
		wsheet.addCell(new Label(col++, row, "必填", titleStyleY));
		if(hasErrorColumn){
			wsheet.addCell(new Label(col++, row, "", titleStyleY));
		}
	}
	
	/**
	 * 设置表头的样式
	 */
	private WritableCellFormat getSheetTitleStyleY() throws WriteException {
		WritableFont wfc = new WritableFont(WritableFont.createFont("宋体"), 10,
				WritableFont.BOLD, false, UnderlineStyle.NO_UNDERLINE,
				Colour.RED);
		WritableCellFormat tempCellFormat = new WritableCellFormat(wfc);
		// 居中对齐
		tempCellFormat.setAlignment(Alignment.CENTRE);
		tempCellFormat.setVerticalAlignment(VerticalAlignment.CENTRE);
		tempCellFormat.setBackground(Colour.YELLOW);
		tempCellFormat.setBorder(Border.ALL, BorderLineStyle.THIN);
		tempCellFormat.setWrap(true);

		return tempCellFormat;
	}

	/**
	 * 设置表头的样式
	 */
	private WritableCellFormat getSheetTitleStyleO() throws WriteException {
		WritableFont wfc = new WritableFont(WritableFont.createFont("宋体"), 10,
				WritableFont.BOLD, false, UnderlineStyle.NO_UNDERLINE,
				Colour.BLACK);

		WritableCellFormat tempCellFormat = new WritableCellFormat(wfc,
				NumberFormats.TEXT);
		// 居中对齐
		tempCellFormat.setAlignment(Alignment.CENTRE);
		tempCellFormat.setVerticalAlignment(VerticalAlignment.CENTRE);
		tempCellFormat.setBorder(Border.ALL, BorderLineStyle.THIN);
		tempCellFormat.setWrap(true);
		tempCellFormat.setBackground(Colour.LIGHT_ORANGE);
		return tempCellFormat;
	}

	/**
	 * 
	 * 编辑人员sheet，把DB中人员数据写入给定sheet中
	 * 
	 * @param userPosSheet
	 *            人员Sheet
	 * @param userPosList人员数据
	 * @throws Exception 
	 */
	protected int editUserPos(WritableSheet[] allDataSheet, List<UserBean> userPosList,boolean hasErrorColumn) throws Exception {
		
		// 人员及岗位Sheet索引
		int dataIndex = 1;
		// 人员及岗位行号索引
		int rowIndex = 0;
		// 人员及岗位Sheet
		WritableSheet userPosSheet = null;
		for (int i = 0; i < userPosList.size(); i++) {
			// 获取新的Sheet
			if (i % userPosSheetLen == 0) {
				userPosSheet = allDataSheet[dataIndex];
				// 写出表头
				createPosTitle(userPosSheet,hasErrorColumn);
				dataIndex++;
				rowIndex = 0;
			}
			// 获取一条人员数据
			UserBean userBean = userPosList.get(i);
			// 行数
			int rowNum = rowIndex + SyncConstant.WIRTER_ROW_NUM;
			rowIndex++;
			// 登录名称
			userPosSheet.addCell(new Label(0, rowNum, userBean.getUserNum()));
			// 真实姓名
			userPosSheet.addCell(new Label(1, rowNum, userBean.getTrueName()));
			// 任职岗位编号
			userPosSheet.addCell(new Label(2, rowNum, userBean.getPosNum()));
			// 任职岗位名称
			userPosSheet.addCell(new Label(3, rowNum, userBean.getPosName()));
			// 岗位所属部门编号
			userPosSheet.addCell(new Label(4, rowNum, userBean.getDeptNum()));
			// 邮箱地址
			userPosSheet.addCell(new Label(5, rowNum, userBean.getEmail()));
			// 内外网邮件标识(内网:0 外网:1)
			String emailTypeStr = "";
			if (userBean.checkEmailType()) {
				emailTypeStr = userBean.getEmailType().toString();
			}
			userPosSheet.addCell(new Label(6, rowNum, emailTypeStr));
			// 联系电话
			userPosSheet.addCell(new Label(7, rowNum, userBean.getPhone()));
			if(hasErrorColumn){
				// 错误信息
				userPosSheet.addCell(new Label(8, rowNum, SyncTool.nullToEmtryByObj(userBean.getError())));
			}
		}
		return dataIndex;
	}

	private void createPosTitle(WritableSheet wsheet, boolean hasErrorColumn) throws Exception {

		int col=0;
		int row=0;
		wsheet.setRowView(row, 800);
		// 表头的样式 背景为黄色 字体为红色
		WritableCellFormat titleStyleY = getSheetTitleStyleY();
		// 表头的样式 背景为橘黄色 字体为黑色
		WritableCellFormat titleStyleO = getSheetTitleStyleO();
		// ------------------------写出表头------------------------
		// -------------调整列宽
		wsheet.setColumnView(col++, 28);
		wsheet.setColumnView(col++, 28);
		wsheet.setColumnView(col++, 28);
		wsheet.setColumnView(col++, 28);
		wsheet.setColumnView(col++, 28);
		wsheet.setColumnView(col++, 28);
		wsheet.setColumnView(col++, 28);
		wsheet.setColumnView(col++, 28);
		if(hasErrorColumn){
			wsheet.setColumnView(col++, 40);
		}

		col=0;
		// 表头
		wsheet.addCell(new Label(col++, row, "登录名称唯一，建议采用工号、电子邮件等\n**谨慎修改**", titleStyleY));
		wsheet.addCell(new Label(col++, row, "", titleStyleY));
		wsheet.addCell(new Label(col++, row, "不填写任职岗位编号，说明人员没有对应岗位\n**谨慎修改**", titleStyleO));
		wsheet.addCell(new Label(col++, row, "任职岗位编号填写，任职岗位名称就必须填写", titleStyleO));
		wsheet.addCell(new Label(col++, row, "任职岗位编号填写，岗位所属部门编号就必须填写", titleStyleO));
		wsheet.addCell(new Label(col++, row, "", titleStyleO));
		wsheet.addCell(new Label(col++, row, "邮箱地址填写，内外网邮件标识必须填写", titleStyleO));
		wsheet.addCell(new Label(col++, row, "", titleStyleO));
		if(hasErrorColumn){
			wsheet.addCell(new Label(col++, row, "", titleStyleY));
		}
		
		row++;
		col=0;
		// 设置高度
		wsheet.setRowView(row, 500);
		// 表头
		wsheet.addCell(new Label(col++, row, "登录名称", titleStyleY));
		wsheet.addCell(new Label(col++, row, "真实姓名", titleStyleY));
		wsheet.addCell(new Label(col++, row, "任职岗位编号", titleStyleO));
		wsheet.addCell(new Label(col++, row, "任职岗位名称", titleStyleO));
		wsheet.addCell(new Label(col++, row, "岗位所属部门编号", titleStyleO));
		wsheet.addCell(new Label(col++, row, "邮箱地址", titleStyleO));
		wsheet.addCell(new Label(col++, row, "内外网邮件标识(内网:0 外网:1)", titleStyleO));
		wsheet.addCell(new Label(col++, row, "联系电话", titleStyleO));
		if(hasErrorColumn){
			wsheet.addCell(new Label(col++, row, "错误信息", titleStyleY));
		}
		
		row++;
		col=0;
		// 设置高度
		wsheet.setRowView(row, 300);
		// 表头
		wsheet.addCell(new Label(col++, row, "必填", titleStyleY));
		wsheet.addCell(new Label(col++, row, "必填", titleStyleY));
		wsheet.addCell(new Label(col++, row, "非必填", titleStyleO));
		wsheet.addCell(new Label(col++, row, "非必填", titleStyleO));
		wsheet.addCell(new Label(col++, row, "非必填", titleStyleO));
		wsheet.addCell(new Label(col++, row, "非必填", titleStyleO));
		wsheet.addCell(new Label(col++, row, "非必填", titleStyleO));
		wsheet.addCell(new Label(col++, row, "非必填", titleStyleO));
		if(hasErrorColumn){
			wsheet.addCell(new Label(col++, row, "", titleStyleY));
		}
	}

	public String getOutputExcelModelPath() {
		return outputExcelModelPath;
	}

	public void setOutputExcelModelPath(String outputExcelModelPath) {
		this.outputExcelModelPath = outputExcelModelPath;
	}

	public String getOutputExcelDataPath() {
		return outputExcelDataPath;
	}

	public void setOutputExcelDataPath(String outputExcelDataPath) {
		this.outputExcelDataPath = outputExcelDataPath;
	}

}
