package com.jecn.epros.server.dao.popedom.impl;

import java.util.List;
import java.util.Set;
import java.util.UUID;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.hibernate.Query;
import org.hibernate.Session;

import com.jecn.epros.server.bean.dataimport.JecnBasePosBean;
import com.jecn.epros.server.bean.popedom.JecnPositionGroup;
import com.jecn.epros.server.common.AbsBaseDao;
import com.jecn.epros.server.common.JecnCommon;
import com.jecn.epros.server.common.JecnCommonSql;
import com.jecn.epros.server.common.JecnContants;
import com.jecn.epros.server.common.Pager;
import com.jecn.epros.server.common.JecnCommonSql.DBType;
import com.jecn.epros.server.dao.popedom.IPositoinGroupDao;

public class PositionGroupDaoImpl extends AbsBaseDao<JecnPositionGroup, Long>
		implements IPositoinGroupDao {
  
	 private final Logger log =Logger.getLogger(this.getClass());
	@Override
	public void deletePositionGroup(Set<Long> setIds) throws Exception {
		// 角色与岗位组的关系 临时
		String hql = "delete from JecnFlowStationT where type='1' and figurePositionId in";
		List<String> listSqls = JecnCommonSql.getSetSqls(hql, setIds);
		for (String delSql : listSqls) {
			execteHql(delSql);
		}
		// /角色与岗位组的关系
		hql = "delete from JecnFlowStation where type='1' and figurePositionId in";
		listSqls = JecnCommonSql.getSetSqls(hql, setIds);
		for (String delSql : listSqls) {
			execteHql(delSql);
		}
		// 删除岗位组与岗位关联表
		hql = "delete from JecnPositionGroupOrgRelated where groupId in";
		listSqls = JecnCommonSql.getSetSqls(hql, setIds);
		for (String delSql : listSqls) {
			execteHql(delSql);
		}
		// 删除岗位组查阅权限
		hql = "delete from JecnPosGroupPermissionsT where posGrouId in ";
		listSqls = JecnCommonSql.getSetSqls(hql, setIds);
		for (String delSql : listSqls) {
			execteHql(delSql);
		}
		hql = "delete from JecnPosGroupPermissions where posGrouId in ";
		listSqls = JecnCommonSql.getSetSqls(hql, setIds);
		for (String delSql : listSqls) {
			execteHql(delSql);
		}
		// 删除角岗位组表
		hql = "delete from JecnPositionGroup where id in";
		listSqls = JecnCommonSql.getSetSqls(hql, setIds);
		for (String delSql : listSqls) {
			execteHql(delSql);
		}

	}

	@Override
	public void deletePositionGroup(Long projectId) throws Exception {
		// 角色与岗位组的关系 临时
		String hql = "delete from JecnFlowStationT where type='1' and figurePositionId in (select id from JecnPositionGroup where projectId = ?)";
		execteHql(hql, projectId);
		// /角色与岗位组的关系
		hql = "delete from JecnFlowStation where type='1' and figurePositionId in (select id from JecnPositionGroup where projectId = ?)";
		execteHql(hql, projectId);
		// 删除岗位组与岗位关联表
		hql = "delete from JecnPositionGroupOrgRelated where groupId in (select id from JecnPositionGroup where projectId = ?)";
		execteHql(hql, projectId);
		// 删除角岗位组表
		hql = "delete from JecnPositionGroup where projectId =?";
		execteHql(hql, projectId);
	}

	@Override
	public List<Object[]> getPositionsByPosGroupId(long posGroupId)
			throws Exception {
		// 根据岗位组Id获取所有岗位信息
		String sql = "select distinct jr.figure_id, jfoi.figure_text, jfo.ORG_NAME"
				+ " from jecn_position_group_r jr,"
				+ " jecn_position_group   j,"
				+ " jecn_flow_org_image   jfoi,"
				+ " jecn_flow_org         jfo"
				+ " where jr.figure_id = jfoi.figure_id"
				+ " and jfoi.org_id = jfo.org_id"
				+ " and jfo.del_state = 0"
				+ " and jr.group_id = ?";
		return listNativeSql(sql, posGroupId);
	}

	/**
	 * 查询基准岗位
	 * @author cheshaowei
	 * @date 2015-03-24
	 * @param projectId 项目ID
	 *        baseName 基准岗位名称
	 * */
	public List<JecnBasePosBean> getJecnBasePosBean(Long projectId,
			String baseName){
		try {
			//查询基准岗位sql
			String sql = "SELECT JBP.BASE_POS_ID,JBP.BASE_POS_NUMBER,JBP.BASE_POS_NAME FROM JECN_BASE_POS JBP";
			Session session =this.getSession();
			if(session!=null){
				if(StringUtils.isNotBlank(baseName)){
					sql+=" WHERE JBP.BASE_POS_NAME like ?";
				}
				Query query = session.createSQLQuery(sql).addEntity(JecnBasePosBean.class);
				if(StringUtils.isNotBlank(baseName)){
					query.setParameter(0,"%"+baseName+"%");
				}
				List<JecnBasePosBean> jecnBasePosBeanList = query.list();
				return jecnBasePosBeanList;
			}
			return null;
		} catch (Exception e) {
			log.error("查询基准岗位信息出现异常",e);
			return null;
		}
	}
	
	/**
	 * 查询岗位组关联的基准岗位
	 * 
	 * @author cheshaowei
	 * 
	 * */
	public List<JecnBasePosBean> getJecnbasePosRelBean(Long projectId,String posGroupId){
		
		try {
			List<JecnBasePosBean> basePosBeanList =null;
			String sql = "SELECT jbp.base_pos_id,jbp.base_pos_number,BASE_POS_NAME FROM JECN_BASE_POS JBP" +
			             " INNER JOIN JECN_BASEPOS_REL_GROUP JBRG" + 
			             " ON JBRG.BASE_ID = JBP.BASE_POS_NUMBER" + 
			             " AND JBRG.GROUP_ID = ?";
			Session session =this.getSession();
			if(session!=null){
				Query query =session.createSQLQuery(sql).addEntity(JecnBasePosBean.class);
				query.setParameter(0, posGroupId);
				basePosBeanList = query.list();
			}
			return basePosBeanList;
		} catch (Exception e) {
			log.error("查询岗位组对应的基准岗位出现异常！"+this.getClass()+"getJecnbasePosRelBean() line 156",e);
			return null;
		}
	}
	
	
	/**
	 * 根据ID查询基准岗位对应岗位
	 * 
	 * @author cheshaowei
	 * @param baseId 基准岗位ID
	 *        pageNum 当前页
	 *        pageSize 每页显示记录数
	 * */
	public List<Object[]> getBaseRelPos(String baseId,String pageNum,String pageSize,String operationType) throws Exception{

		try {
			int currentNum = Integer.parseInt(pageNum);
			int pageSizes = Integer.parseInt(pageSize);
			int totalCount = 0;
			String sql = "SELECT COUNT(JFOI.FIGURE_ID)" +
							"  FROM JECN_BASE_REL_POS JBP" + 
							" INNER JOIN JECN_FLOW_ORG_IMAGE JFOI" + 
							"    ON JFOI.FIGURE_NUMBER_ID = JBP.POS_ID" + 
							"  LEFT JOIN JECN_USER_POSITION_RELATED JUP" + 
							"    ON JUP.FIGURE_ID = JFOI.FIGURE_ID" + 
							"  LEFT JOIN JECN_USER JU" + 
							"    ON JU.PEOPLE_ID = JUP.PEOPLE_ID" + 
							"  LEFT JOIN JECN_FLOW_ORG JFO" + 
							"    ON JFO.ORG_ID = JFOI.ORG_ID" + 
							" WHERE JBP.BASE_ID = ?";
			// 总行数
			totalCount = this.countAllByParamsNativeSql(sql, baseId);
			Pager pager = JecnCommon.getPager(currentNum, pageSizes,
					operationType, totalCount);
			List<Object[]> objectList = null;
			sql = "SELECT JFOI.FIGURE_ID, JFOI.FIGURE_TEXT, JFO.ORG_NAME, JU.TRUE_NAME" +
							"  FROM JECN_BASE_REL_POS JBP" + 
							" INNER JOIN JECN_FLOW_ORG_IMAGE JFOI" + 
							"    ON JFOI.FIGURE_NUMBER_ID = JBP.POS_ID" + 
							"  LEFT JOIN JECN_USER_POSITION_RELATED JUP" + 
							"    ON JUP.FIGURE_ID = JFOI.FIGURE_ID" + 
							"  LEFT JOIN JECN_USER JU" + 
							"    ON JU.PEOPLE_ID = JUP.PEOPLE_ID" + 
							"  LEFT JOIN JECN_FLOW_ORG JFO" + 
							"    ON JFO.ORG_ID = JFOI.ORG_ID" + 
							" WHERE JBP.BASE_ID = ?" + 
							" ORDER BY JU.TRUE_NAME, JFOI.FIGURE_TEXT";


			objectList = this.listNativeSql(sql, pager.getStartRow(), pager
					.getPageSize(), baseId);
			Object[] obj = new Object[3];
			// 总页数
			obj[0] = pager.getTotalPages();
			// 当前页
			obj[1] = pager.getCurrentPage();
			obj[2] = "";
			objectList.add(objectList.size(), obj);
			return objectList;
		} catch (Exception e) {
			log.error("根据ID查询基准岗位对应岗位出现异常！" + this.getClass()
					+ "getBaseRelPos()", e);
			throw e;
		}
	}
	
	/**
	 * 岗位组关联基准岗位，去除已关联的并且要删除的基准岗位相关数据
	 * 
	 * @param @param deleteBasePosList
	 * @param @param groupId
	 * @param @throws Exception
	 * @date 2015-7-29 下午03:53:33
	 * @throws
	 */
	@Override
	public void deleteBasePosRel(List<String> deleteBasePosList, Long groupId)
			throws Exception {
		if (deleteBasePosList.size() == 0) {
			return;
		}
		// 2、删除根据基准岗位岗位组和岗位对应关系
		String sql = "DELETE FROM JECN_POSITION_GROUP_R GR"
				+ "      WHERE GR.STATE = 1" + "        AND GR.GROUP_ID = "
				+ groupId 
				+ "        AND GR.FIGURE_ID IN"
				+ "            (SELECT OI.FIGURE_ID"
				+ "               FROM JECN_BASE_REL_POS RP" +
						" INNER JOIN JECN_FLOW_ORG_IMAGE OI ON OI.FIGURE_NUMBER_ID = RP.POS_ID"
				+ "              WHERE RP.BASE_ID IN"
				+ JecnCommonSql.getStrs(deleteBasePosList) + ")";
		this.execteNative(sql);

		// 3、删除基准岗位与岗位关系表
		sql = "DELETE FROM JECN_BASEPOS_REL_GROUP  WHERE GROUP_ID = ? AND BASE_ID IN"
				+ JecnCommonSql.getStrs(deleteBasePosList);
		this.execteNative(sql, groupId);
	}
	
	/**
	 * 添加岗位组和岗位关联关系（岗位组关联基准岗位）
	 * 
	 * @param addBaseIds
	 *            新增的基准岗位，对应岗位组关联基准岗位表
	 * @param updateOrInsetBaseIds
	 *            岗位组和岗位关联表，新增的或存在的基准岗位IDs
	 * @param groupId
	 * @date 2015-7-29 下午03:56:21
	 * @throws
	 */
	@Override
	public void addBasePosRelGroup(List<String> addBaseIds,List<String> updateOrInsetBaseIds, Long groupId) throws Exception{
		String sql = "";
		if (updateOrInsetBaseIds.size() > 0) {
			if (JecnContants.dbType.equals(DBType.ORACLE)) {
				sql = "INSERT INTO JECN_POSITION_GROUP_R(ID,GROUP_ID,FIGURE_ID,STATE)"
						+ "SELECT JECN_POSITION_GROUP_R_SQUENCE.NEXTVAL,GROUP_ID,FIGURE_ID,1 STATE ";
			} else {
				sql = "INSERT INTO JECN_POSITION_GROUP_R(GROUP_ID,FIGURE_ID,STATE)" + "SELECT GROUP_ID,FIGURE_ID,1 STATE ";
			}
			// 判断添加和删除的基准岗位集合关联的岗位是否存在重复
			sql = sql +
				"  FROM (SELECT DISTINCT REL_POS_TABLE.FIGURE_ID," + 
				groupId + "  GROUP_ID," + 
				"                        GROUP_POS_TABLE.STATE" + 
				"          FROM (SELECT JFOI.FIGURE_ID" + 
				"                  FROM JECN_BASE_REL_POS JBRP" + 
				"                 INNER JOIN JECN_FLOW_ORG_IMAGE JFOI" + 
				"                    ON JBRP.POS_ID = JFOI.FIGURE_NUMBER_ID" + 
				"                 WHERE JBRP.BASE_ID IN " + JecnCommonSql.getStrs(updateOrInsetBaseIds) + 
				") REL_POS_TABLE" + 
				"          LEFT JOIN (SELECT PGR.FIGURE_ID, PGR.STATE" + 
				"                      FROM JECN_POSITION_GROUP_R PGR" + 
				"                     WHERE PGR.GROUP_ID = " + groupId +
				") GROUP_POS_TABLE" + 
				"            ON REL_POS_TABLE.FIGURE_ID = GROUP_POS_TABLE.FIGURE_ID) TMP" + 
				" WHERE TMP.STATE IS NULL";
			this.execteNative(sql);
		}
		
		for (String baseId : addBaseIds) {
			// 添加基准岗位与岗位组的关系
			sql = "INSERT INTO JECN_BASEPOS_REL_GROUP(ID,GROUP_ID,BASE_ID) VALUES(?,?,?)";
			this.execteNative(sql, UUID.randomUUID(), groupId, baseId);
		}
	}
}
