package com.jecn.epros.server.emailnew;

import java.util.List;

import com.jecn.epros.server.bean.popedom.JecnUser;

public abstract class BaseEmailBuilder {

	private List<JecnUser> users;
	private Object[] data;

	/**
	 * 构建Email数据对象
	 * 
	 * @return
	 */
	public abstract List<EmailBasicInfo> buildEmail();

	/**
	 * 设置生成Email所需的数据
	 * 
	 * @param users
	 * @param data
	 */
	public void setData(List<JecnUser> users, Object... data) {
		if (users == null || users.size() == 0) {
			throw new IllegalArgumentException(
					"非法的参数值：[ users == null || users.size() == 0 ]");
		}
		this.users = users;
		this.data = data;
	}

	public List<JecnUser> getUsers() {
		return users;
	}

	public Object[] getData() {
		return data;
	}

}
