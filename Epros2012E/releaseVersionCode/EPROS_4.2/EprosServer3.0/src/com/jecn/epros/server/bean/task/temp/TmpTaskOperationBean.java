package com.jecn.epros.server.bean.task.temp;

/**
 * 任务操作配置
 * 
 * @author Administrator
 * @date： 日期：Sep 27, 2013 时间：5:23:25 PM
 */
public class TmpTaskOperationBean {
	/** 返回 1:显示 */
	private int reBack;

	public int getReBack() {
		return reBack;
	}

	public void setReBack(int reBack) {
		this.reBack = reBack;
	}

}
