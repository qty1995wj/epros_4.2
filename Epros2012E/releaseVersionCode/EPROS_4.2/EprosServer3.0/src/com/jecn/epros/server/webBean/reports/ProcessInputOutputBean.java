package com.jecn.epros.server.webBean.reports;

import java.util.List;

import com.jecn.epros.server.webBean.process.ProcessWebBean;

/**
 * 流程输入输出统计
 * 
 * @author fuzhh Feb 28, 2013
 * 
 */
public class ProcessInputOutputBean {
	/** 流程名称 */
	private String processName;
	/** 流程ID */
	private long processId;
	/** 上游流程 */
	private List<ProcessWebBean> upstreamProcess;
	/** 上游流程 */
	private String upstreamProcessName;
	/** 上游流程个数 */
	private int upstreamProcessCount;
	
	/** 下游流程 */
	private List<ProcessWebBean> downstreamProcess;
	/** 下游流程个数 */
	private int downstreamProcessCount;
	/** 下游流程 */
	private String downstreamProcessName;

	public String getUpstreamProcessName() {
		return upstreamProcessName;
	}

	public void setUpstreamProcessName(String upstreamProcessName) {
		this.upstreamProcessName = upstreamProcessName;
	}

	public String getDownstreamProcessName() {
		return downstreamProcessName;
	}

	public void setDownstreamProcessName(String downstreamProcessName) {
		this.downstreamProcessName = downstreamProcessName;
	}

	public String getProcessName() {
		return processName;
	}

	public void setProcessName(String processName) {
		this.processName = processName;
	}

	public long getProcessId() {
		return processId;
	}

	public void setProcessId(long processId) {
		this.processId = processId;
	}

	public List<ProcessWebBean> getUpstreamProcess() {
		return upstreamProcess;
	}

	public void setUpstreamProcess(List<ProcessWebBean> upstreamProcess) {
		this.upstreamProcess = upstreamProcess;
	}

	public int getUpstreamProcessCount() {
		return upstreamProcessCount;
	}

	public void setUpstreamProcessCount(int upstreamProcessCount) {
		this.upstreamProcessCount = upstreamProcessCount;
	}

	public List<ProcessWebBean> getDownstreamProcess() {
		return downstreamProcess;
	}

	public void setDownstreamProcess(List<ProcessWebBean> downstreamProcess) {
		this.downstreamProcess = downstreamProcess;
	}

	public int getDownstreamProcessCount() {
		return downstreamProcessCount;
	}

	public void setDownstreamProcessCount(int downstreamProcessCount) {
		this.downstreamProcessCount = downstreamProcessCount;
	}
}
