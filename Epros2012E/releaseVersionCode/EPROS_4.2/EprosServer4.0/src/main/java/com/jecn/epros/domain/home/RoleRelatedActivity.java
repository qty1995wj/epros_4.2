package com.jecn.epros.domain.home;

public class RoleRelatedActivity {
    private Long roleId;
    private String activityName;
    private String activityNum;
    private Long activityFigureId;

    public Long getRoleId() {
        return roleId;
    }

    public void setRoleId(Long roleId) {
        this.roleId = roleId;
    }

    public String getActivityName() {
        return activityName;
    }

    public void setActivityName(String activityName) {
        this.activityName = activityName;
    }

    public Long getActivityFigureId() {
        return activityFigureId;
    }

    public void setActivityFigureId(Long activityFigureId) {
        this.activityFigureId = activityFigureId;
    }

    public String getActivityNum() {
        return activityNum;
    }

    public void setActivityNum(String activityNum) {
        this.activityNum = activityNum;
    }
}
