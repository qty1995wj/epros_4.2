package epros.draw.visio;

/**
 * 存储visio 样式bean
 * 
 * @author fuzhh 2013-6-20
 * 
 */
public class JecnVisioStyleDateBean {
	/** 唯一标识 */
	private String ix;
	/** 指定的ID号来格式化文本所使用的字体 */
	private Long fontId;
	/** 颜色 */
	private String color;
	/** 字体类型 1:黑体 2:斜体 3:下划线 4:小写 */
	private String style;
	/** 0:正常情况 1:所有字母大写 2:仅首字母大写 */
	private String caseStr;
	/** 指定形状的文本相对于基线的位置 0:正常位置 1:上标 2:下标 */
	private String pos;
	/** 指定的字体宽度 */
	private String fontScale;
	/** 指定字体的大小 */
	private String size;
	/** 指定的文本范围是否它下面有双下划线 0:没有 1：有 */
	private String dblUnderline;
	/** 指定文本是否有它上面的一条线 0:文本没有它上面的一条线 1:它上面有一行文字 */
	private String overline;
	/** 指定文本是否格式化为带删除线 0:文本没有被格式化为带删除线 1:文本格式化为带删除线 */
	private String strikethru;
	/** 确定文本是否被格式化为双删除线 0:不要双删除线文本格式设置为（默认） 1:双删除线格式文本 */
	private String doubleStrikethrough;
	/** 确定是否运行当前字符是从左到右或从右到左的文字方向 0:文字方向是从左右侧（默认） 1:文字方向是从右到左 */
	private String rTLText;
	/** 确定字符是否运行垂直或水平 0:文字方向是水平的（默认） 1:文字方向是垂直的 */
	private String useVertical;
	/** 指定两个或多个字符之间的空间量。 空间可以添加或减去点增量的二十分之一。 */
	private String letterspace;
	/** 确定层或形状的文本颜色的透明度，从0（完全不透明）到1（完全透明） */
	private String colorTrans;
	/** 指定的ID号，用来格式化文本包含亚洲字符的字体。 */
	private String asianFont;

	public String getIx() {
		return ix;
	}

	public void setIx(String ix) {
		this.ix = ix;
	}

	public Long getFontId() {
		return fontId;
	}

	public void setFontId(Long fontId) {
		this.fontId = fontId;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public String getStyle() {
		return style;
	}

	public void setStyle(String style) {
		this.style = style;
	}

	public String getCaseStr() {
		return caseStr;
	}

	public void setCaseStr(String caseStr) {
		this.caseStr = caseStr;
	}

	public String getPos() {
		return pos;
	}

	public void setPos(String pos) {
		this.pos = pos;
	}

	public String getFontScale() {
		return fontScale;
	}

	public void setFontScale(String fontScale) {
		this.fontScale = fontScale;
	}

	public String getSize() {
		return size;
	}

	public void setSize(String size) {
		this.size = size;
	}

	public String getDblUnderline() {
		return dblUnderline;
	}

	public void setDblUnderline(String dblUnderline) {
		this.dblUnderline = dblUnderline;
	}

	public String getOverline() {
		return overline;
	}

	public void setOverline(String overline) {
		this.overline = overline;
	}

	public String getStrikethru() {
		return strikethru;
	}

	public void setStrikethru(String strikethru) {
		this.strikethru = strikethru;
	}

	public String getDoubleStrikethrough() {
		return doubleStrikethrough;
	}

	public void setDoubleStrikethrough(String doubleStrikethrough) {
		this.doubleStrikethrough = doubleStrikethrough;
	}

	public String getrTLText() {
		return rTLText;
	}

	public void setrTLText(String rTLText) {
		this.rTLText = rTLText;
	}

	public String getUseVertical() {
		return useVertical;
	}

	public void setUseVertical(String useVertical) {
		this.useVertical = useVertical;
	}

	public String getLetterspace() {
		return letterspace;
	}

	public void setLetterspace(String letterspace) {
		this.letterspace = letterspace;
	}

	public String getColorTrans() {
		return colorTrans;
	}

	public void setColorTrans(String colorTrans) {
		this.colorTrans = colorTrans;
	}

	public String getAsianFont() {
		return asianFont;
	}

	public void setAsianFont(String asianFont) {
		this.asianFont = asianFont;
	}
}
