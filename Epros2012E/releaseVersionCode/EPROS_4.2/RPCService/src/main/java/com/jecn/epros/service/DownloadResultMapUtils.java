package com.jecn.epros.service;

import java.util.HashMap;
import java.util.Map;

public class DownloadResultMapUtils {
	private static final String BODY = "body";
	private static final String FILE_NAME = "fileName";

	public static Map<String, Object> newInstance(String fileName, byte[] bytes) {
		return new ResultMap().setFileName(fileName).setContent(bytes).result;
	}

	static final class ResultMap {
		private Map<String, Object> result = new HashMap<String, Object>();

		ResultMap setFileName(String fileName) {
			result.put(FILE_NAME, fileName);
			return this;
		}

		ResultMap setContent(byte[] bytes) {
			result.put(BODY, bytes);
			return this;
		}

	}

	public static String getFileName(Map<String, Object> resultMap) {
		if (!resultMap.containsKey(FILE_NAME)) {
			throw new IllegalArgumentException("必须存在  key[ " + FILE_NAME + " ]");
		}
		return resultMap.get(FILE_NAME).toString();
	}

	public static byte[] getContent(Map<String, Object> resultMap) {
		if (!resultMap.containsKey(BODY)) {
			throw new IllegalArgumentException("必须存在  key[ " + BODY + " ] ");
		}
		return (byte[]) resultMap.get(BODY);
	}
}
