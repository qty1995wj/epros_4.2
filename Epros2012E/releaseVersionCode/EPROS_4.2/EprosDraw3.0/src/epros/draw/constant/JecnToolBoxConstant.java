package epros.draw.constant;

/**
 * 
 * 流程元素库常量类
 * 
 * @author Administrator
 * 
 */
public class JecnToolBoxConstant {

	/** 画图面板类型：流程地图、流程图 */
	public enum MapType {
		totalMap, // 流程地图
		partMap, // 流程图
		orgMap, // 组织图
		totalMapRelation, // 集成关系图
		none
		// 无
	}

	/** 流程模板类型 */
	public enum ModeType {
		totalMapMode, // 流程地图模板
		partMapMode, // 流程图模板
		none
		// 无
	}

	/** 所有图和线标识 */
	public enum MapElemType {
		guide, // 指针
		addFigure, // 添加符号

		ParallelLines, // 横分割线 1
		VerticalLine, // 竖分割线 2

		CommonLine, // 连接线 3
		ManhattanLine, // 连接线(带箭头) 4
		OneArrowLine, // 单向箭头 5
		TwoArrowLine, // 双向箭头 6
		DividingLine, // 分割线 7

		OvalSubFlowFigure, // 流程图：子流程 8
		Oval, // 流程地图：圆形 9

		Triangle, // 流程地图 三角形 10
		FunctionField, // 流程地图 矩形功能域 11
		Pentagon, // 五边形 12
		RightHexagon, // 箭头六边形 13
		ModelFigure, // 泳池 14
		DottedRect, // 协作框 15
		FreeText, // 自由文本框 16
		CommentText, // 注释框 17
		CommentLine, // 注释框小线段
		IconFigure, // 图标插入框 18
		InputHandFigure, // 手动输入 19
		SystemFigure, // 制度 20

		RoundRectWithLine, // 活动 21
		RoleFigure, // 角色 22
		CustomFigure, // 客户 23
		ReverseArrowhead, // 返入 24
		RightArrowhead, // 返出 25
		Rhombus, // 决策 26
		EventFigure, // 事件 27
		ImplFigure, // 接口 28
		FileImage, // 文档 29
		DataImage, // 信息系统 30
		ToFigure, // 跳转:转出 31
		FromFigure, // 跳转:转入 32
		EndFigure, // 结束 33
		PAFigure, // PA:问题区域 34
		KSFFigure, // KSF:关键成功因素 35
		KCPFigure, // KCP:关键控制点 36
		KCPFigureComp, // KCP(合规)
		XORFigure, // XOR:多种情况下只能选中一种 37
		ORFigure, // OR:多种情况下只能选中一种或多种 38
		FlowFigureStart, // 流程开始 39
		FlowFigureStop, // 流程结束 40
		AndFigure, // AND 41
		VirtualRoleFigure, // 虚拟角色 42
		// 以下画图工具没有
		PositionFigure, // 岗位 43
		Rect, // 组织 44
		PartitionFigure, // 阶段分隔符 45
		ITRhombus, // IT决策 46

		HDividingLine, // 横线
		VDividingLine, // 竖线
		DateFreeText, // 最后更改时间文本框
		IsoscelesTrapezoidFigure, // 等腰梯形
		FlowLevelFigureOne, // 流程等级图形
		FlowLevelFigureTwo, // 流程等级图形
		FlowLevelFigureThree, // 流程等级图形
		FlowLevelFigureFour, // 流程等级图形
		MapNameText, // 流程图/流程地图/组织图名称文本框

		KeyPointFigure, // 关键控制点
		RiskPointFigure, // 风险点
		MapRhombus, // 菱形
		MapLineFigure, // 流程架构图添加连接线，连接线不存在开始元素和结束元素情况，默认生成此标识位的虚拟元素
		ActivityImplFigure, // 活动-接口
		ActivityOvalSubFlowFigure, // 活动-子流程

		/** 【石勘院，新增元素 start】 */
		ActiveFigureAR, // 活动
		StartFigureAR, // 开始/起点
		EndFigureAR, // 结束/终点
		RoleFigureAR, // 角色
		KCPFigureAR, // 关键控制点
		XORFigureAR, // XOR:多种情况下只能选中一种
		AndFigureAR, // 多种情况下 并行（必须同时发生）
		ORFigureAR, // 多种情况下只能选中一种或多种
		TermFigureAR, // 术语
		ExpertRhombusAR,
		// 专家决策

		/** 【石勘院，新增元素 end】 */

		MapFigureEnd, // 流程架构结束
		SystemDept, // 流程架构 信息部
		TrialReport, // 流程架构 试制报表

		StageSeparatorPentagon,
		// 阶段分隔符 五边形
		ReverseMapArrowhead, // 架构返入
		RightMapArrowhead, // 架构返出
		ERPImage,
		// 架构ERP
		InterfaceRightHexagon,// 接口六边形
		ArrowheadRight,
		ArrowheadBottom
		
	}

	/**
	 * 
	 * 所有流程元素类型
	 * 
	 */
	// public final static MapElemType[] allFlowElementType = {
	// MapElemType.ParallelLines, // 横分割线 1
	// MapElemType.VerticalLine, // 竖分割线 2
	//
	// MapElemType.CommonLine, // 连接线 3
	// MapElemType.ManhattanLine, // 连接线(带箭头) 4
	// MapElemType.OneArrowLine, // 单向箭头 5
	// MapElemType.TwoArrowLine, // 双向箭头 6
	// MapElemType.DividingLine, // 分割线 7
	//
	// MapElemType.OvalSubFlowFigure, // 流程图：子流程 8
	// MapElemType.Oval, // 流程地图：圆形 9
	//
	// MapElemType.Triangle, // 流程地图 三角形 10
	// MapElemType.FunctionField, // 流程地图 矩形功能域 11
	// MapElemType.Pentagon, // 五边形 12
	// MapElemType.RightHexagon, // 箭头六边形 13
	// MapElemType.ModelFigure, // 泳池 14
	// MapElemType.DottedRect, // 协作框 15
	// MapElemType.FreeText, // 自由文本框 16
	// MapElemType.CommentText, // 注释框 17
	// MapElemType.IconFigure, // 图标插入框 18
	// MapElemType.InputHandFigure, // 手动输入 19
	// MapElemType.SystemFigure, // 制度 20
	//
	// MapElemType.RoundRectWithLine, // 活动 21
	// MapElemType.RoleFigure, // 角色 22
	// MapElemType.CustomFigure, // 客户 23
	// MapElemType.ReverseArrowhead, // 返入 24
	// MapElemType.RightArrowhead, // 返出 25
	// MapElemType.Rhombus, // 决策 26
	// MapElemType.EventFigure, // 事件 27
	// MapElemType.ImplFigure, // 接口 28
	// MapElemType.FileImage, // 文档 29
	// MapElemType.DataImage, // 信息系统 30
	// MapElemType.ToFigure, // 跳转:转出 31
	// MapElemType.FromFigure, // 跳转:转入 32
	// MapElemType.EndFigure, // 结束 33
	// MapElemType.PAFigure, // PA:问题区域 34
	// MapElemType.KSFFigure, // KSF:关键成功因素 35
	// MapElemType.KCPFigure, // KCP:关键控制点 36
	// MapElemType.XORFigure, // XOR:多种情况下只能选中一种 37
	// MapElemType.ORFigure, // OR:多种情况下只能选中一种或多种 38
	// MapElemType.FlowFigureStart, // 流程开始 39
	// MapElemType.FlowFigureStop, // 流程结束 40
	// MapElemType.AndFigure, // AND 41
	// MapElemType.VirtualRoleFigure, // 虚拟角色 42
	//
	// // 以下画图工具没有
	// MapElemType.PositionFigure, // 岗位 43
	// MapElemType.Rect, // 组织 444
	// MapElemType.PartitionFigure,// 阶段分隔符
	// MapElemType.ITRhombus, // IT决策
	//
	// MapElemType.HDividingLine,// 横线
	// MapElemType.VDividingLine,// 竖线
	// MapElemType.DateFreeText,// 最后更改时间文本框
	// MapElemType.IsoscelesTrapezoidFigure, // 等腰梯形
	// MapElemType.FlowLevelFigureOne,// 流程等级图形
	// MapElemType.FlowLevelFigureTwo,// 流程等级图形
	// MapElemType.FlowLevelFigureThree,// 流程等级图形
	// MapElemType.FlowLevelFigureFour, // 流程等级图形
	// MapElemType.MapNameText,// 流程图/流程地图/组织图名称文本框
	// MapElemType.KeyPointFigure,//关键控制点
	// MapElemType.RiskPointFigure//风险点
	// };

	/**
	 * 
	 * 流程元素维护使用的
	 * 
	 */
	public final static MapElemType[] flowElementSetType = { MapElemType.ParallelLines, // 横分割线
			// 1
			MapElemType.VerticalLine, // 竖分割线 2

			MapElemType.CommonLine, // 连接线 3
			MapElemType.ManhattanLine, // 连接线(带箭头) 4
			MapElemType.OneArrowLine, // 单向箭头 5
			MapElemType.TwoArrowLine, // 双向箭头 6
			MapElemType.DividingLine, // 分割线 7

			MapElemType.OvalSubFlowFigure, // 流程图：子流程 8
			MapElemType.Oval, // 流程地图：圆形 9

			MapElemType.Triangle, // 流程地图 三角形 10
			MapElemType.FunctionField, // 流程地图 矩形功能域 11
			MapElemType.Pentagon, // 五边形 12
			MapElemType.RightHexagon, // 箭头六边形 13
			MapElemType.ModelFigure, // 泳池 14
			MapElemType.DottedRect, // 协作框 15
			MapElemType.FreeText, // 自由文本框 16
			MapElemType.CommentText, // 注释框 17
			MapElemType.IconFigure, // 图标插入框 18
			MapElemType.InputHandFigure, // 手动输入 19
			MapElemType.SystemFigure, // 制度 20

			MapElemType.RoundRectWithLine, // 活动 21

			MapElemType.RoleFigure, // 角色 22
			MapElemType.CustomFigure, // 客户 23
			MapElemType.ReverseArrowhead, // 返入 24
			MapElemType.RightArrowhead, // 返出 25
			MapElemType.Rhombus, // 决策 26
			MapElemType.EventFigure, // 事件 27
			MapElemType.ImplFigure, // 接口 28
			MapElemType.FileImage, // 文档 29
			MapElemType.DataImage, // 信息系统 30
			MapElemType.ToFigure, // 跳转:转出 31
			MapElemType.FromFigure, // 跳转:转入 32
			MapElemType.EndFigure, // 结束 33
			MapElemType.PAFigure, // PA:问题区域 34
			MapElemType.KSFFigure, // KSF:关键成功因素 35
			MapElemType.KCPFigure, // KCP:关键控制点 36
			MapElemType.KCPFigureComp,// KCP(合规)
			MapElemType.XORFigure, // XOR:多种情况下只能选中一种 37
			MapElemType.ORFigure, // OR:多种情况下只能选中一种或多种 38
			MapElemType.FlowFigureStart, // 流程开始 39
			MapElemType.FlowFigureStop, // 流程结束 40
			MapElemType.AndFigure, // AND 41
			MapElemType.VirtualRoleFigure, // 虚拟角色 42

			// 以下画图工具没有
			MapElemType.PartitionFigure,// 阶段分隔符
			MapElemType.ITRhombus, // IT决策

			MapElemType.HDividingLine,// 横线
			MapElemType.VDividingLine,// 竖线
			MapElemType.DateFreeText,// 最后更改时间文本框
			MapElemType.IsoscelesTrapezoidFigure, // 等腰梯形
			MapElemType.FlowLevelFigureOne,// 流程等级图形
			MapElemType.FlowLevelFigureTwo,// 流程等级图形
			MapElemType.FlowLevelFigureThree,// 流程等级图形
			MapElemType.FlowLevelFigureFour, // 流程等级图形
			MapElemType.MapNameText,// 流程图/流程地图/组织图名称文本框

			MapElemType.Rect,// 组织
			MapElemType.PositionFigure, // 岗位

			MapElemType.KeyPointFigure,// 关键控制点
			MapElemType.RiskPointFigure,// 风险点
			MapElemType.MapRhombus, // 菱形
			MapElemType.ActivityImplFigure,// 活动-接口
			MapElemType.ActivityOvalSubFlowFigure,// 活动-子流程

			/** 【石化新增活动】 */
			MapElemType.ActiveFigureAR, // 活动
			MapElemType.StartFigureAR, // 开始/起点
			MapElemType.EndFigureAR, // 结束/终点
			MapElemType.RoleFigureAR, // 角色
			MapElemType.KCPFigureAR, // 关键控制点
			MapElemType.XORFigureAR, // XOR:多种情况下只能选中一种
			MapElemType.AndFigureAR, // 多种情况下 并行（必须同时发生）
			MapElemType.ORFigureAR, // 多种情况下只能选中一种或多种
			MapElemType.TermFigureAR, // 术语
			MapElemType.ExpertRhombusAR, // 专家决策
			MapElemType.MapFigureEnd,// 流程架构结束
			MapElemType.SystemDept, // 流程架构 信息部
			MapElemType.TrialReport,// 流程架构 试制报表
			MapElemType.StageSeparatorPentagon, // 阶段分隔符 五边形
			MapElemType.ReverseMapArrowhead, // 架构返入
			MapElemType.RightMapArrowhead, // 架构返出
			MapElemType.ERPImage,// 架构ERP
			MapElemType.InterfaceRightHexagon // 接口六边形
	};

	/**
	 * 
	 * 所有能添加到面板的图形类型
	 * 
	 */
	public final static MapElemType[] figrueType = { MapElemType.OvalSubFlowFigure, // 流程图：子流程
			// 8
			MapElemType.Oval, // 流程地图：圆形 9

			MapElemType.OneArrowLine, // 单向箭头 5
			MapElemType.TwoArrowLine, // 双向箭头 6

			MapElemType.Triangle, // 三角形 10
			MapElemType.FunctionField, // 矩形 11
			MapElemType.Pentagon, // 五边形 12
			MapElemType.RightHexagon, // 箭头六边形 13
			MapElemType.ModelFigure, // 泳池 14
			MapElemType.DottedRect, // 协作框 15
			MapElemType.FreeText, // 自由文本框 16
			MapElemType.CommentText, // 注释框 17
			MapElemType.IconFigure, // 图标插入框 18
			MapElemType.InputHandFigure, // 手动输入 19
			MapElemType.SystemFigure, // 制度 20

			MapElemType.RoundRectWithLine, // 活动 21
			MapElemType.RoleFigure, // 角色 22
			MapElemType.CustomFigure, // 客户 23
			MapElemType.ReverseArrowhead, // 返入 24
			MapElemType.RightArrowhead, // 返出 25
			MapElemType.Rhombus, // 决策 26
			MapElemType.EventFigure, // 事件 27
			MapElemType.ImplFigure, // 接口 28
			MapElemType.FileImage, // 文档 29
			MapElemType.DataImage, // 信息系统 30
			MapElemType.ToFigure, // 跳转:转出 31
			MapElemType.FromFigure, // 跳转:转入 32
			MapElemType.EndFigure, // 结束 33
			MapElemType.PAFigure, // PA:问题区域 34
			MapElemType.KSFFigure, // KSF:关键成功因素 35
			MapElemType.KCPFigure, // KCP:关键控制点 36
			MapElemType.KCPFigureComp,// KCP(合规)
			MapElemType.XORFigure, // XOR:多种情况下只能选中一种 37
			MapElemType.ORFigure, // OR:多种情况下只能选中一种或多种 38
			MapElemType.FlowFigureStart, // 流程开始 39
			MapElemType.FlowFigureStop, // 流程结束 40
			MapElemType.AndFigure, // AND 41
			MapElemType.VirtualRoleFigure,// 虚拟角色 42
			MapElemType.ITRhombus, // IT决策

			MapElemType.PositionFigure,// 岗位 43
			MapElemType.Rect, // 组织 44

			MapElemType.DateFreeText,// 最后更改时间文本框
			MapElemType.IsoscelesTrapezoidFigure, // 等腰梯形
			MapElemType.FlowLevelFigureOne,// 流程等级图形
			MapElemType.FlowLevelFigureTwo,// 流程等级图形
			MapElemType.FlowLevelFigureThree,// 流程等级图形
			MapElemType.FlowLevelFigureFour, // 流程等级图形
			MapElemType.MapNameText,// 流程图/流程地图/组织图名称文本框

			MapElemType.HDividingLine, // 横线
			MapElemType.VDividingLine, // 竖线

			MapElemType.KeyPointFigure,// 关键控制点
			MapElemType.RiskPointFigure,// 风险点
			MapElemType.MapRhombus, // 菱形
			MapElemType.ActivityImplFigure,// 活动-接口
			MapElemType.ActivityOvalSubFlowFigure,// 活动-子流程
			MapElemType.PartitionFigure, // 阶段分隔符

			/** 【石化新增活动】 */
			MapElemType.ActiveFigureAR, // 活动
			MapElemType.StartFigureAR, // 开始/起点
			MapElemType.EndFigureAR, // 结束/终点
			MapElemType.RoleFigureAR, // 角色
			MapElemType.KCPFigureAR, // 关键控制点
			MapElemType.XORFigureAR, // XOR:多种情况下只能选中一种
			MapElemType.AndFigureAR, // 多种情况下 并行（必须同时发生）
			MapElemType.ORFigureAR, // 多种情况下只能选中一种或多种
			MapElemType.TermFigureAR, // 术语
			MapElemType.ExpertRhombusAR, // 专家决策
			MapElemType.MapFigureEnd,// 流程架构结束
			MapElemType.SystemDept, // 流程架构 信息部
			MapElemType.TrialReport, // 流程架构 试制报表
			MapElemType.StageSeparatorPentagon, // 阶段分隔符 五边形
			MapElemType.ReverseMapArrowhead, // 架构返入
			MapElemType.RightMapArrowhead, // 架构返出
			MapElemType.ERPImage,// 架构ERP
			MapElemType.InterfaceRightHexagon // 接口六边形
	};

	/** 流程图：常用线 */
	public final static MapElemType[] partMapFavrLineFlowType = { MapElemType.ManhattanLine // 连接线(带箭头)
	};
	/** 流程地图：常用线 */
	public final static MapElemType[] totalMapFavrLineFlowType = { MapElemType.ManhattanLine // 连接线(带箭头)
	};
	/** 组织图:常用线 */
	public final static MapElemType[] orgLineType = { MapElemType.CommonLine, // 连接线
	};

	/** 流程图：常用图形 */
	public final static MapElemType[] partMapFavrFiguresFlowType = {
	/** 【石化新增活动】 */
	MapElemType.ActiveFigureAR, // 活动
			MapElemType.RoleFigureAR, // 角色
			MapElemType.KCPFigureAR, // 关键控制点
			MapElemType.XORFigureAR, // XOR:多种情况下只能选中一种
			MapElemType.AndFigureAR, // 多种情况下 并行（必须同时发生）
			MapElemType.ORFigureAR, // 多种情况下只能选中一种或多种
			MapElemType.StartFigureAR, // 开始/起点
			MapElemType.EndFigureAR, // 结束/终点
			MapElemType.TermFigureAR, // 术语MapElemType.RoleFigure,// 角色
			MapElemType.ExpertRhombusAR,// 专家决策
			MapElemType.VirtualRoleFigure,// 虚拟角色
			MapElemType.CustomFigure,// 客户
			MapElemType.ReverseArrowhead,// 返入
			MapElemType.RightArrowhead,// 返出
			MapElemType.Rhombus,// 决策
			MapElemType.DottedRect,// 协作框
			MapElemType.ImplFigure,// 接口
			MapElemType.FreeText,// 自由文本框
			MapElemType.CommentText, // 注释框
			MapElemType.PAFigure,// PA:问题区域
			MapElemType.KSFFigure // KSF:关键成功因素

	};
	/** 流程地图：常用图形 */
	public final static MapElemType[] totalMapFavrFigureFlowType = { MapElemType.Pentagon,// 五边形
			MapElemType.RightHexagon,// 箭头六边形
			MapElemType.FunctionField,// 矩形
			MapElemType.ModelFigure // 泳池
	};
	/**
	 * 
	 * 组织图:常用图形
	 * 
	 */
	public final static MapElemType[] orgFigureType = { MapElemType.Rect, // 组织44
			MapElemType.PositionFigure // 岗位 43
	};

	/**
	 * 
	 * 流程图：不常用线
	 * 
	 */
	public final static MapElemType[] partMapGenNoShowLineType = { MapElemType.CommonLine,// 连接线
			MapElemType.DividingLine,// 分割线
			MapElemType.ParallelLines,// 横分割线
			MapElemType.VerticalLine,// 竖分割线
			MapElemType.HDividingLine,// 横线
			MapElemType.VDividingLine, // 竖线
			MapElemType.PartitionFigure // 阶段分隔符
	};

	/**
	 * 
	 * 流程地图：不常用线
	 * 
	 */
	public final static MapElemType[] totalMapGenNoShowLineType = { MapElemType.CommonLine,// 连接线
			MapElemType.OneArrowLine,// 单向箭头
			MapElemType.TwoArrowLine,// 双向箭头
			MapElemType.DividingLine,// 分割线
			MapElemType.HDividingLine,// 横线
			MapElemType.VDividingLine // 竖线

	};

	/**
	 * 
	 * 组织图：不常用线
	 * 
	 */
	public final static MapElemType[] orgMapGenNoShowLineType = { MapElemType.ManhattanLine // 连接线(带箭头)
	};

	/** 流程图：不常用图形 */
	public final static MapElemType[] partMapGenNoShowFigureType = { MapElemType.FileImage,// 文档
			MapElemType.OvalSubFlowFigure,// 子流程
			MapElemType.DataImage,// 信息系统
			MapElemType.ToFigure,// 跳转:转出
			MapElemType.FromFigure,// 跳转:转入
			MapElemType.EndFigure,// 结束
			MapElemType.XORFigure,// XOR:多种情况下只能选中一种
			MapElemType.ORFigure,// OR:多种情况下只能选中一种或多种
			MapElemType.IconFigure,// 图标插入框
			MapElemType.FlowFigureStart,// 流程开始
			MapElemType.FlowFigureStop,// 流程结束
			MapElemType.AndFigure,// AND:多种情况下都要满足
			MapElemType.ITRhombus, // IT决策
			MapElemType.EventFigure, // 事件
			MapElemType.DateFreeText, // 最后更改时间文本框
			MapElemType.MapNameText,// 流程图/流程地图/组织图名称文本框

			MapElemType.KeyPointFigure,// 关键控制点
			MapElemType.RiskPointFigure, // 风险点
			MapElemType.RoundRectWithLine,// 活动
			MapElemType.KCPFigure,// KCPFigure
			MapElemType.KCPFigureComp,// KCP(合规)
			MapElemType.RoleFigure, // 角色
			MapElemType.ActivityImplFigure,// 活动-接口
			MapElemType.ActivityOvalSubFlowFigure, // 活动-子流程
			MapElemType.StageSeparatorPentagon, // 阶段分隔符 五边形
			MapElemType.InterfaceRightHexagon // 接口六边形
	};

	/** 流程地图：不常用图形 */
	public final static MapElemType[] totalMapGenNoShowFigureType = { MapElemType.Oval,// 圆形
			MapElemType.Triangle,// 三角形
			MapElemType.DottedRect,// 协作框
			MapElemType.FreeText,// 自由文本框
			MapElemType.CommentText,// 注释框
			MapElemType.IconFigure,// 图标插入框
			MapElemType.InputHandFigure,// 手动输入
			MapElemType.SystemFigure,// 制度
			MapElemType.DateFreeText,// 最后更改时间文本框
			MapElemType.IsoscelesTrapezoidFigure, // 等腰梯形
			MapElemType.FlowLevelFigureOne,// 流程等级图形
			MapElemType.FlowLevelFigureTwo,// 流程等级图形
			MapElemType.FlowLevelFigureThree,// 流程等级图形
			MapElemType.FlowLevelFigureFour, // 流程等级图形

			/************ 流程地图新增农夫新增 *************/
			MapElemType.RoleFigure,// 角色
			MapElemType.RoleFigureAR, // 角色AR
			MapElemType.VirtualRoleFigure,// 虚拟角色
			MapElemType.CustomFigure,// 客户
			MapElemType.FileImage,// 文档
			MapElemType.MapNameText,// 流程图/流程地图/组织图名称文本框
			MapElemType.MapRhombus, // 菱形
			MapElemType.MapFigureEnd,// 流程架构结束
			MapElemType.SystemDept, // 流程架构 信息部
			MapElemType.TrialReport, // 流程架构 试制报表
			MapElemType.ReverseMapArrowhead, // 架构返入
			MapElemType.RightMapArrowhead, // 架构返出
			MapElemType.ERPImage // 架构ERP
	};

	/** 组织图：不常用图形 */
	public final static MapElemType[] orgMapGenNoShowFigureType = { MapElemType.FreeText,// 自由文本框
			MapElemType.CommentText,// 注释框
			MapElemType.DottedRect,// 协作框
			MapElemType.IconFigure, // 图标插入框
			MapElemType.MapNameText
	// 流程图/流程地图/组织图名称文本框
	};

}
