package epros.designer.gui.rule;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import javax.swing.JLabel;
import javax.swing.JTree;

import org.apache.log4j.Logger;

import epros.designer.gui.common.JecnTextField;
import epros.designer.gui.system.CategoryCommon;
import epros.designer.gui.system.PersonliableSelectCommon;
import epros.designer.gui.system.ResDepartSelectCommon;
import epros.designer.tree.JecnTreeNode;
import epros.designer.util.JecnConfigTool;
import epros.designer.util.JecnProperties;
import epros.draw.gui.swing.JecnPanel;
import epros.draw.util.JecnUIUtil;

/***
 * 添加---制度基本信息
 * 
 * @author Administrator 2012-10-25
 * 
 *         修改人：chuhuanbo 时间：2014-10-21 描述：添加制度责任人可以选择岗位
 * 
 */
public class AddRuleBaseInfoPanel extends JecnPanel {
	private static Logger log = Logger.getLogger(AddRuleBaseInfoPanel.class);
	private JecnTreeNode pNode = null;
	private JTree jTree = null;
	/** 信息面板 */
	private JecnPanel infoPanel = new JecnPanel(430, 300);

	/** 制度名称Lab */
	protected JLabel ruleNameLab = new JLabel(JecnProperties.getValue("ruleNameC"));
	/** 制度名称Field */
	protected JecnTextField ruleNameFiled = new JecnTextField(315, 20);
	/** 制度名称必填提示符 */
	private JLabel ruleNameRequired = new JLabel(JecnProperties.getValue("required"));

	/** 制度编号Lab */
	private JLabel ruleNumLab = new JLabel(JecnProperties.getValue("ruleNumC"));
	/** 制度编号Field */
	protected JecnTextField ruleNumField = new JecnTextField(315, 20);

	/** 类别 */
	CategoryCommon categoryCommon = null;
	/** 流程责任人 */
	PersonliableSelectCommon personliableSelect = null;
	/** 流程责任部门 */
	ResDepartSelectCommon resDepartSelectCommon = null;

	/** 设置大小 */
	Dimension dimension = null;

	private AddRuleInfoDialog addRuleInfoDialog = null;

	public AddRuleBaseInfoPanel(JecnTreeNode pNode, JTree jTree, AddRuleInfoDialog addRuleInfoDialog) {
		this.pNode = pNode;
		this.jTree = jTree;
		this.addRuleInfoDialog = addRuleInfoDialog;
		infoPanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		ruleNameRequired.setForeground(Color.red);
		initLayout();
	}

	/**
	 * 布局
	 */
	private void initLayout() {
		this.setLayout(new BorderLayout());
		// 主面板

		// 信息面板=========================布局
		this.add(infoPanel, BorderLayout.NORTH);
		infoPanel.setLayout(new GridBagLayout());
		infoPanel.setPreferredSize(new Dimension(600, 150));
		// infoPanel.setBorder(BorderFactory.createTitledBorder(""));
		int rows = 0;
		Insets insets = new Insets(3, 5, 3, 5);
		// 制度名称
		GridBagConstraints c = new GridBagConstraints(0, rows, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
				GridBagConstraints.NONE, insets, 0, 0);
		infoPanel.add(ruleNameLab, c);
		c = new GridBagConstraints(1, rows, 2, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				insets, 0, 0);
		infoPanel.add(ruleNameFiled, c);
		c = new GridBagConstraints(3, rows, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE,
				new Insets(8, 0, 0, 0), 0, 0);
		infoPanel.add(ruleNameRequired, c);
		rows++;
		// 制度编号
		if (!JecnConfigTool.isAutoNumber()) {
			c = new GridBagConstraints(0, rows, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE,
					insets, 0, 0);
			infoPanel.add(ruleNumLab, c);
			c = new GridBagConstraints(1, rows, 2, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
					insets, 0, 0);
			infoPanel.add(ruleNumField, c);
			rows++;
		}

		if (JecnConfigTool.isShowRuleType()) {
			// 制度类别
			categoryCommon = new CategoryCommon(rows, infoPanel, insets, null, addRuleInfoDialog);
			rows++;
			this.categoryCommon.setVisible();
		}
		// 责任人
		personliableSelect = new PersonliableSelectCommon(rows, infoPanel, insets, null, -1, "", addRuleInfoDialog, 1,
				false);
		rows++;
		// 责任部门
		resDepartSelectCommon = new ResDepartSelectCommon(rows, infoPanel, insets, null, "", addRuleInfoDialog, false);
	}

	public boolean isUpdate() {
		// 基本信息
		if (!"".equals(ruleNameFiled.getText().trim())) {
			return true;
		}
		if (!"".equals(ruleNumField.getText().trim())) {
			return true;
		}
		if (categoryCommon != null && categoryCommon.isUpdate()) {
			return true;
		}
		if (personliableSelect.getSingleSelectCommon().isUpdate()) {
			return true;
		}
		if (resDepartSelectCommon.isUpdate()) {
			return true;
		}
		return false;
	}

}
