package com.jecn.epros.domain.process.inout;


import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * 流程文件专用
 */
public class JecnFlowInoutBean {
    private String id;
    private Long flowId;
    /**
     * 0输入
     * 1输出
     */
    private Integer type;
    private String name;
    private String posText;
    private String explain;
    private List<JecnFlowInoutPosBean> posAndGroups;

    public List<JecnFlowInoutPosBean> getPos() {
        return getPosAndGroupsMap().get(0);
    }

    public List<JecnFlowInoutPosBean> getGroups() {
        return getPosAndGroupsMap().get(1);
    }

    public Map<Integer, List<JecnFlowInoutPosBean>> getPosAndGroupsMap() {
        Map<Integer, List<JecnFlowInoutPosBean>> result = new HashMap<>();
        if (this.posAndGroups == null || this.posAndGroups.size() == 0) {
            return result;
        }
        result = this.posAndGroups.stream().collect(Collectors.groupingBy(JecnFlowInoutPosBean::getrType));
        return result;
    }

    //----------------


    public String getPosText() {
        return posText;
    }

    public void setPosText(String posText) {
        this.posText = posText;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Long getFlowId() {
        return flowId;
    }

    public void setFlowId(Long flowId) {
        this.flowId = flowId;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getExplain() {
        return explain;
    }

    public void setExplain(String explain) {
        this.explain = explain;
    }

    public List<JecnFlowInoutPosBean> getPosAndGroups() {
        return posAndGroups;
    }

    public void setPosAndGroups(List<JecnFlowInoutPosBean> posAndGroups) {
        this.posAndGroups = posAndGroups;
    }
}
