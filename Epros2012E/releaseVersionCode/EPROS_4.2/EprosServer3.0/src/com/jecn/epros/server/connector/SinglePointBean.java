package com.jecn.epros.server.connector;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Vector;

import net.sf.json.JSONArray;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import com.jecn.epros.server.action.web.login.LoginAction;
import com.jecn.epros.server.action.web.login.ad.guangzhouCsr.GuangZhouCrsTaskXmlBuilder;
import com.jecn.epros.server.common.JecnContants;
import com.jecn.epros.server.common.JecnConfigContents.ConfigItemPartMapMark;
import com.jecn.epros.server.webBean.WebLoginBean;
import com.jecn.epros.server.webBean.task.MyTaskBean;
import com.opensymphony.xwork2.ActionContext;

public class SinglePointBean extends AbstractSinglePointBean {
	private static final Logger log = Logger.getLogger(SinglePointBean.class);

	public SinglePointBean(LoginAction loginAction) {
		this.loginAction = loginAction;
	}

	/**
	 * 返回登录人待办总数
	 */
	public void getGtasksCount() throws Exception {
		WebLoginBean webLoginBean = (WebLoginBean) ActionContext.getContext().getSession().get("webLoginBean");
		int total = loginAction.getSearchService().getMyTaskCountBySearch(null,
				webLoginBean.getJecnUser().getPeopleId());
		String callbackFunc = loginAction.getRequest().getParameter("callback");

		if (StringUtils.isEmpty(callbackFunc)) {
			loginAction.outJeon("getGtasksCount() callbackFunc = ", callbackFunc);
			return;
		}
		loginAction.outJsonString(callbackFunc + "({\"taskTotal\":" + total + "})");
	}

	/***
	 * 只返回待办数字(包含你感染状态)
	 * 
	 * @throws Exception
	 */
	public void getNumberOfBacklog() throws Exception {
		WebLoginBean webLoginBean = (WebLoginBean) ActionContext.getContext().getSession().get("webLoginBean");
		Long peopleId = webLoginBean.getJecnUser().getPeopleId();
		int total = loginAction.getSearchService().getMyTaskCountNoDraftStateBySearch(null, peopleId);
		loginAction.outJsonString(String.valueOf(total));
	}

	/**
	 * 我的体型任务返回json串
	 * 
	 * @throws Exception
	 */
	public void getMyTaskJson() throws Exception {
		WebLoginBean webLoginBean = (WebLoginBean) ActionContext.getContext().getSession().get("webLoginBean");
		Long peopleId = webLoginBean.getJecnUser().getPeopleId();
		// 每页开始行
		int start = 0;
		// 每页最大行数
		int limit = 0;

		String startStr = loginAction.getRequest().getParameter("start");
		String limitStr = loginAction.getRequest().getParameter("limit");
		if (StringUtils.isEmpty(startStr)) {
			log.error("start = " + start);
			start = 0;
		} else {
			start = Integer.valueOf(startStr);
		}

		if (StringUtils.isEmpty(limitStr)) {
			log.error("limit = " + limit);
			limit = 0;
		} else {
			limit = Integer.valueOf(limitStr);
		}

		String callbackFunc = loginAction.getRequest().getParameter("callback");
		if (StringUtils.isEmpty(callbackFunc)) {
			loginAction.outJeon("getMyTaskJson() callbackFunc = ", callbackFunc);
			return;
		}
		// 根据查询条件获取登录人相关任务
		int total = loginAction.getSearchService().getMyTaskCountBySearch(null, peopleId);
		String basicEprosURL = JecnContants.sysPubItemMap.get(ConfigItemPartMapMark.basicEprosURL.toString());
		List<MyTaskBean> listTask = loginAction.getSearchService().getMyTaskBySearch(null, peopleId, start, limit);
		for (MyTaskBean myTaskBean : listTask) {
			myTaskBean.setOperation(getTaskOper(myTaskBean));
		}
		JSONArray jsonArray = JSONArray.fromObject(listTask);
		loginAction.callBackOutPage(callbackFunc, jsonArray.toString(), total);
	}

	/**
	 * 
	 * 只获取审批的任务信息（不包含查阅状态）
	 * 
	 * @throws Exception
	 */
	public void getToDOTasksJson() throws Exception {
		WebLoginBean webLoginBean = (WebLoginBean) ActionContext.getContext().getSession().get("webLoginBean");
		Long peopleId = webLoginBean.getJecnUser().getPeopleId();

		String callbackFunc = loginAction.getRequest().getParameter("callback");
		if (StringUtils.isEmpty(callbackFunc)) {
			loginAction.outJeon("getToDOTasksJson() callbackFunc = ", callbackFunc);
			return;
		}
		String basicEprosURL = JecnContants.sysPubItemMap.get(ConfigItemPartMapMark.basicEprosURL.toString());
		List<MyTaskBean> listTask = loginAction.getSearchService().getToDotasksBean(peopleId, 0, -1);
		List<Map<String, Object>> maps = new Vector<Map<String, Object>>();
		Map<String, Object> map = null;
		int total = listTask.size();
		for (MyTaskBean myTaskBean : listTask) {
			map = new HashMap<String, Object>();
			map.put("taskName", myTaskBean.getTaskName());
			map.put("taskHref", basicEprosURL + getToDoTaskHref(myTaskBean));
			maps.add(map);
		}
		JSONArray jsonArray = JSONArray.fromObject(maps);
		maps = null;
		listTask = null;
		loginAction.callBackOutPage(callbackFunc, jsonArray.toString(), total);
	}

	/**
	 * 获取
	 * 
	 * @throws Exception
	 */
	public void getToDOTasksJsonNoCallBack() throws Exception {
		try {
			WebLoginBean webLoginBean = (WebLoginBean) ActionContext.getContext().getSession().get("webLoginBean");
			Long peopleId = webLoginBean.getJecnUser().getPeopleId();

			String basicEprosURL = JecnContants.sysPubItemMap.get(ConfigItemPartMapMark.basicEprosURL.toString());
			List<MyTaskBean> listTask = loginAction.getSearchService().getToDotasksBean(peopleId, 0, -1);
			List<Map<String, Object>> maps = new Vector<Map<String, Object>>();
			Map<String, Object> map = null;
			int total = listTask.size();
			for (MyTaskBean myTaskBean : listTask) {
				map = new HashMap<String, Object>();
				map.put("taskName", myTaskBean.getTaskName());
				map.put("taskHref", basicEprosURL + "taskLogin.action?accessType=openApproveDomino&mailTaskId="
						+ myTaskBean.getTaskId() + "&isApp=" + loginAction.getIsApp());
				map.put("createTime", myTaskBean.getUpdateTime());
				map.put("createUser", myTaskBean.getCreateName());
				maps.add(map);
			}
			JSONArray jsonArray = JSONArray.fromObject(maps);
			maps = null;
			listTask = null;
			loginAction.outJsonPage(jsonArray.toString(), total);
		} catch (Exception e) {
			loginAction.outJsonPageError(null, -1);
			log.error("", e);
		}
	}

	/**
	 * 
	 * 只获取审批的任务信息（不包含查阅状态）
	 * 
	 * @throws Exception
	 */
	public void getToDotasksCount() throws Exception {
		WebLoginBean webLoginBean = (WebLoginBean) ActionContext.getContext().getSession().get("webLoginBean");
		int total = loginAction.getSearchService().getToDotasksCount(webLoginBean.getJecnUser().getPeopleId());
		String callbackFunc = loginAction.getRequest().getParameter("callback");

		if (StringUtils.isEmpty(callbackFunc)) {
			loginAction.outJeon("getToDotasksCount() callbackFunc = ", callbackFunc);
			return;
		}
		loginAction.outJsonString(callbackFunc + "({\"taskTotal\":" + total + "})");
	}

	public void getToDotasksCountNoCallBack() throws Exception {
		WebLoginBean webLoginBean = (WebLoginBean) ActionContext.getContext().getSession().get("webLoginBean");
		try {
			int total = loginAction.getSearchService().getToDotasksCount(webLoginBean.getJecnUser().getPeopleId());
			loginAction.outJsonPage(null, total);
		} catch (Exception e) {
			loginAction.outJsonPageError(null, -1);
			log.error("", e);
		}
	}

	/**
	 * 拼接当前任务的操作
	 * 
	 * @param myTaskBean
	 * @return
	 */
	private String getTaskOper(MyTaskBean myTaskBean) {
		int taskElseState = myTaskBean.getTaskElseState();
		int taskStage = myTaskBean.getTaskStage();
		long taskId = myTaskBean.getTaskId();
		int reJect = myTaskBean.getReJect();
		int taskType = myTaskBean.getTaskType();
		// 审批操作连接4
		String varApp = "";
		// 拟稿人操作
		String varState = "";
		String varDiv = "";

		// 参数url
		String appUrl = "taskId=" + taskId + "&taskType=" + taskType + "&taskStage=" + taskStage;
		// 查阅
		String varView = "<a target='_blank' href='taskLookUp.action?" + appUrl + "' class='a-operation'>" + "查阅"
				+ "</a>";

		myTaskBean.setTaskTypeName(getTaskTypeName(taskType));
		// // 删除
		// String varDelete =
		// "<a style='cursor: pointer;' onclick='deleteMyTask("
		// + taskId + "," + taskType + ");' class='a-operation'>" + "删除"
		// + "</a>";
		// String varReject = "";
		// // 撤回
		// if (reJect == 1) {// 撤回
		// varReject = "<a style='cursor: pointer;' onclick='reBackMyTask("
		// + taskId + "," + taskType + ",4);' class='a-operation'>"
		// + "撤回" + "</a>";
		// }

		// taskElseState :
		// 0是打回重新提交状态,1是评审意见整理状态,2为系统管理员;3:拟稿人无审批状态;4:拟稿人为当前阶段审批人
		if (taskStage == 0 || taskStage == 10 || taskElseState == 3 || taskElseState == 4) {// 拟稿人阶段或整理意见阶段
			if (taskElseState == 0) {// 重新提交状态
				varState = "<a target='_blank' href='loginTaskApprove.action?" + appUrl + "&taskOperation=" + 1
						+ "' class='a-operation'>" + "重新提交" + "</a>";
			} else if (taskElseState == 1) {// 评审意见整理
				varState = "<a target='_blank' href='loginTaskApprove.action?" + appUrl + "&taskOperation=" + 2
						+ "' class='a-operation'>" + "整理意见" + "</a>";
			}
			if (taskElseState == 3) {// 拟稿人不是审批人，只有查阅和删除权限
				varDiv = "<div >" + varView + "</div>";
				// varDiv = varDiv + "</a><a></a></div>";
			} else if (taskElseState == 4) {// 拟稿人是审批人
				varApp = "<a target='_blank' href='loginTaskApprove.action?" + appUrl + "' class='a-operation'>" + "审批"
						+ "</a>";
				varDiv = "<div >" + varApp + " | " + varView + "</div>";
			} else if (taskElseState == 5 && (taskStage == 10 || taskStage == 0)) {// 打回整理撤回操作，或打回操作
				// 我的任务显示撤回按钮，无拟稿人情况
				// varDiv = reCallNoDraft(varReject, varView);
				// varDiv = "<div >" + varView + "</a><a></a></div>";
				varDiv = "<div >" + varView + "</div>";
			} else {
				varDiv = "<div >" + varState + " | " + varView + "</div>";
			}
		} else if (taskStage != 5) {// 其他审核阶段按钮为审批 (当前审批阶段)
			if (reJect == 1) {// 如果存在驳回状态
				if (taskElseState == 3) {// 拟稿人，只有查阅和删除权限
					// 我的任务显示撤回按钮，拟稿人操作
					// varDiv = reCallIsDraft(varReject, varView);
					// varDiv = "<div >" + varView + "</a><a></a></div>";
					varDiv = "<div >" + varView + "</div>";
				} else {
					// 我的任务显示撤回按钮，无拟稿人情况
					// varDiv = reCallNoDraft(varReject, varView);
					// varDiv = "<div >" + varView + "</a><a></a></div>";
					varDiv = "<div >" + varView + "</div>";
				}
			} else {
				varApp = "<a target='_blank' href='loginTaskApprove.action?" + appUrl + "' class='a-operation'>" + "审批"
						+ "</a>";
				// 组织显示的按钮
				varDiv = "<div >" + varApp + " | " + varView + "</a><a></a></div>";
			}
		}
		return varDiv;
	}

	public static String getToDoTaskHref(MyTaskBean myTaskBean) {
		int taskElseState = myTaskBean.getTaskElseState();
		int taskStage = myTaskBean.getTaskStage();
		long taskId = myTaskBean.getTaskId();
		int taskType = myTaskBean.getTaskType();
		// 参数url
		String paraUrl = "taskId=" + taskId + "&taskType=" + taskType + "&taskStage=" + taskStage;

		String operationHref = "";
		if (taskStage == 0 || taskStage == 10 || taskElseState == 3 || taskElseState == 4) {// 拟稿人阶段或整理意见阶段
			if (taskElseState == 0) {// 重新提交状态
				operationHref = "loginTaskApprove.action?" + paraUrl + "&taskOperation=" + 1;
			} else if (taskElseState == 1) {// 评审意见整理
				operationHref = "loginTaskApprove.action?" + paraUrl + "&taskOperation=" + 2;
			}
			if (taskElseState == 4) {// 拟稿人是审批人
				operationHref = "loginTaskApprove.action?" + paraUrl;
			}
		} else if (taskStage != 5) {// 其他审核阶段按钮为审批 (当前审批阶段)
			// 组织显示的按钮
			return operationHref = "loginTaskApprove.action?" + paraUrl;
		}
		return operationHref;
	}

	public static String getLoginMailApproveDominoUrl(MyTaskBean myTaskBean) {
		return "loginMail.action?accessType=openApproveDomino&mailTaskId=" + myTaskBean.getTaskId();
	}

	private String getTaskTypeName(int value) {
		String taskTypeName = "";
		switch (value) {
		case 0:
			taskTypeName = "流程任务";// 
			break;
		case 4:
			taskTypeName = "地图任务";// 
			break;
		case 1:
			taskTypeName = "文件任务";// 
			break;
		case 2:// 制度模板文件
		case 3:// 制度文件
			taskTypeName = "制度任务";// 
			break;

		}
		return taskTypeName;
	}

	@Override
	public void getMyTaskXml() throws Exception {
		WebLoginBean webLoginBean = (WebLoginBean) ActionContext.getContext().getSession().get("webLoginBean");
		Long peopleId = webLoginBean.getJecnUser().getPeopleId();
		List<MyTaskBean> listTask = loginAction.getSearchService()
				.getMyTaskNoDraftStateBySearch(null, peopleId, -1, -1);
		loginAction.outXMLString(new GuangZhouCrsTaskXmlBuilder().build(listTask, peopleId));
	}
}
