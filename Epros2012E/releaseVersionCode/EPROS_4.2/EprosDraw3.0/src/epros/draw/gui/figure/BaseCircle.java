/*
 * Circle.java
 *圆形
 * Created on 2012年03月15日, 上午10:35
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */
package epros.draw.gui.figure;

import java.awt.BasicStroke;
import java.awt.Graphics;
import java.awt.Graphics2D;

import epros.draw.data.figure.JecnFigureData;
import epros.draw.util.JecnUIUtil;

/**
 * XORFigure、ToFigure、ORFigure、FromFigure、AndFigure 基类 圆形
 * 
 * @author Administrator
 * @date： 日期：Mar 23, 2012 时间：3:04:06 PM
 */
public class BaseCircle extends JecnBaseFigurePanel {

	public BaseCircle(JecnFigureData flowElementData) {
		super(flowElementData);
	}

	protected int x = 0;
	protected int y = 0;

	/**
	 * 画圆
	 * 
	 */
	public void paintComponent(Graphics g) {
		userWidth = this.getWidth() - 1;
		userHeight = this.getHeight() - 1;
		paintFigure(g);
	}

	/**
	 * 无填充
	 * 
	 */
	public void paintNoneFigure(Graphics2D g2d) {
		g2d.setPaint(flowElementData.getBodyColor());
		g2d.setStroke(flowElementData.getBasicStroke());
		g2d.drawOval(x, y, userWidth, userHeight);
	}

	/**
	 * 
	 * 阴影效果
	 */
	public void paintShadowsOnly(Graphics2D g2d) {
		// 阴影
		paintShadows(g2d, 4, 4);
	}

	/**
	 * 无阴影无3D 普通效果
	 * 
	 */
	public void paintGeneralFigurePart(Graphics2D g2d) {
		// 顶层 原图，榜值均为零
		paintCircleTop(g2d, 0);
	}

	/**
	 * 阴影 顶层填充
	 * 
	 */
	public void paintTop(Graphics2D g2d) {
		// 顶层 原图，榜值均为零
		paintCircleTop(g2d, 0);
	}

	/**
	 * 无阴影无3D 普通效果
	 * 
	 */
	protected void paintCircleTop(Graphics2D g2d, int shadowCount) {
		// 顶层 原图，榜值均为零
		g2d.fillOval(x, y, userWidth - shadowCount, userHeight - shadowCount);
		g2d.setPaint(flowElementData.getBodyColor());
		g2d.setStroke(flowElementData.getBasicStroke());
		g2d.drawOval(x, y, userWidth - shadowCount, userHeight - shadowCount);
	}

	/**
	 * 
	 * 填充背景区域
	 */
	public void paint3DLow(Graphics2D g2d) {
		paint3DCircleLow(g2d, 0);
	}

	/**
	 * 3D和阴影效果底层填充
	 * 
	 */
	public void paint3DAndShadowsLow(Graphics2D g2d) {
		paint3DCircleLow(g2d, 4);
	}

	private void paint3DCircleLow(Graphics2D g2d, int indent3Dvalue) {
		g2d.fillOval(x, y, userWidth - indent3Dvalue, userHeight - indent3Dvalue);
	}

	/**
	 * 3D效果填充线条
	 */
	public void paint3DPartLine(Graphics2D g2d) {
		paint3DCircleLine(g2d, 0);
	}

	/**
	 * 3D和阴影效果线条渐变后画边框
	 * 
	 * @param g2d
	 * @param indent3Dvalue
	 */
	private void paint3DCircleLine(Graphics2D g2d, int indent3Dvalue) {
		g2d.setStroke(flowElementData.getBasicStroke());
		g2d.drawOval(x, y, userWidth - indent3Dvalue, userHeight - indent3Dvalue);
	}

	/**
	 * 3D效果填充线条
	 */
	public void paint3DAndShadowsPartLine(Graphics2D g2d) {
		paint3DCircleLine(g2d, linevalue);
	}

	/**
	 * 顶层 颜色填充,渐变色区域
	 */
	protected void paint3DPartTop(Graphics2D g2d) {
		g2d.fillOval(x + 4, y + 4, userWidth - 4 - 4, userHeight - 4 - 4);
	}

	/**
	 * 既有3D又有阴影填充顶层
	 * 
	 * @param g2d
	 */
	protected void paint3DAndShadowsPartTop(Graphics2D g2d) {
		g2d.fillOval(x + 4, y + 4, userWidth - 4 - 4, userHeight - 4 - 4);
	}

	/**
	 * 圆形阴影效果
	 * 
	 * @param g2d
	 * @param x
	 * @param y
	 * @param localCount
	 *            有阴影时原图像缩进榜值
	 * @param content
	 *            有阴影时顶层图像缩进榜值
	 */
	private void paintShadows(Graphics2D g2d, int localCount, int comCount) {
		g2d.setPaint(flowElementData.getShadowColor());
		g2d.fillOval(x + localCount, y + localCount, userWidth - comCount, userHeight - comCount);
		g2d.setPaint(JecnUIUtil.getShadowBodyColor());
		g2d.setStroke(new BasicStroke());
		g2d.drawOval(x + localCount, y + localCount, userWidth - comCount, userHeight - comCount);
	}

}
