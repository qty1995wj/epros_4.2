package com.jecn.epros.server.webBean.rule;

import java.io.Serializable;

/**
 * @author yxw 2012-11-26
 * @description：制度查询条件封装
 */
public class RuleSearchBean implements Serializable{
	/**制度ID*/
	private long ruleId;
	/**制度名称*/
	private String ruleName;
	/**制度编号*/
	private String ruleMum;
	/**制度类别*/
	private long ruleType;
	/**责任部门*/
	private long orgId;
	/**责任部门名称*/
	private String orgName;
	/**查阅部门*/
	private long orgLookId;
	/**部门名称*/
	private String orgLookName;
	public Long getPeopleId() {
		return peopleId;
	}
	public void setPeopleId(Long peopleId) {
		this.peopleId = peopleId;
	}
	public boolean isAdmin() {
		return isAdmin;
	}
	public void setAdmin(boolean isAdmin) {
		this.isAdmin = isAdmin;
	}
	/**查阅岗位*/
	private long posLookId;
	/**查阅岗位名称*/
	private String posLookName;
	/**密级*/
	private long secretLevel;
	/**项目ID*/
	private long projectId;
	/**登陆人员Id**/
	private Long peopleId;
	/**是否是管理员**/
	private boolean isAdmin;
	public long getRuleId() {
		return ruleId;
	}
	public void setRuleId(long ruleId) {
		this.ruleId = ruleId;
	}
	public long getRuleType() {
		return ruleType;
	}
	public void setRuleType(long ruleType) {
		this.ruleType = ruleType;
	}
	public long getOrgId() {
		return orgId;
	}
	public void setOrgId(long orgId) {
		this.orgId = orgId;
	}
	public long getOrgLookId() {
		return orgLookId;
	}
	public void setOrgLookId(long orgLookId) {
		this.orgLookId = orgLookId;
	}
	public long getPosLookId() {
		return posLookId;
	}
	public void setPosLookId(long posLookId) {
		this.posLookId = posLookId;
	}
	public long getSecretLevel() {
		return secretLevel;
	}
	public void setSecretLevel(long secretLevel) {
		this.secretLevel = secretLevel;
	}
	public String getOrgLookName() {
		return orgLookName;
	}
	public void setOrgLookName(String orgLookName) {
		this.orgLookName = orgLookName;
	}
	public String getPosLookName() {
		return posLookName;
	}
	public void setPosLookName(String posLookName) {
		this.posLookName = posLookName;
	}
	public String getOrgName() {
		return orgName;
	}
	public void setOrgName(String orgName) {
		this.orgName = orgName;
	}
	public String getRuleName() {
		return ruleName;
	}
	public void setRuleName(String ruleName) {
		this.ruleName = ruleName;
	}
	public String getRuleMum() {
		return ruleMum;
	}
	public void setRuleMum(String ruleMum) {
		this.ruleMum = ruleMum;
	}
	public long getProjectId() {
		return projectId;
	}
	public void setProjectId(long projectId) {
		this.projectId = projectId;
	}
}
