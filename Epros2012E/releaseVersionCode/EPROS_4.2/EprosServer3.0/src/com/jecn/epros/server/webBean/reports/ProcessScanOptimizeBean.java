package com.jecn.epros.server.webBean.reports;

import java.util.ArrayList;
import java.util.List;

/**
 * 流程审视表bean
 * 
 * @author huoyl
 * 
 */

public class ProcessScanOptimizeBean {

	/** 责任部门下的流程审视的详细 */
	private List<ProcessScanWebListBean> allScanWebListBeans = new ArrayList<ProcessScanWebListBean>();
	/** 开始时间 */
	private String startTime;
	/** 结束时间 */
	private String endTime;
	/** 责任部门下完成的审视的流程数量 */
	private int scanCount = 0;
	/** 责任部门下未审视的流程数量 */
	private int notScanCount = 0;
	/** 责任部门下所有需要审视的流程数量 */
	private int allScanCount = 0;
	/** 流程审视比例 审视完成的除以所有的 */
	private int scanPercent = 0;
	/** 流程责任人是否显示 */
	private boolean guardianIsShow;
	private int type = 0;

	public List<ProcessScanWebListBean> getAllScanWebListBeans() {
		return allScanWebListBeans;
	}

	public void setAllScanWebListBeans(List<ProcessScanWebListBean> allScanWebListBeans) {
		this.allScanWebListBeans = allScanWebListBeans;
	}

	public String getStartTime() {
		return startTime;
	}

	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}

	public String getEndTime() {
		return endTime;
	}

	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}

	public int getScanCount() {
		return scanCount;
	}

	public void setScanCount(int scanCount) {
		this.scanCount = scanCount;
	}

	public int getNotScanCount() {
		return notScanCount;
	}

	public void setNotScanCount(int notScanCount) {
		this.notScanCount = notScanCount;
	}

	public int getAllScanCount() {
		return allScanCount;
	}

	public void setAllScanCount(int allScanCount) {
		this.allScanCount = allScanCount;
	}

	public int getScanPercent() {
		return scanPercent;
	}

	public void setScanPercent(int scanPercent) {
		this.scanPercent = scanPercent;
	}

	public boolean isGuardianIsShow() {
		return guardianIsShow;
	}

	public void setGuardianIsShow(boolean guardianIsShow) {
		this.guardianIsShow = guardianIsShow;
	}

	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}
	

}
