package com.jecn.epros.server.service.task.app.impl;

import java.util.ArrayList;
import java.util.List;

import com.jecn.epros.server.bean.popedom.JecnUser;
import com.jecn.epros.server.bean.task.JecnTaskBeanNew;
import com.jecn.epros.server.common.AbsBaseService;
import com.jecn.epros.server.dao.task.IJecnTaskTestRunDao;
import com.jecn.epros.server.emailnew.BaseEmailBuilder;
import com.jecn.epros.server.emailnew.EmailBasicInfo;
import com.jecn.epros.server.emailnew.EmailBuilderFactory;
import com.jecn.epros.server.emailnew.EmailBuilderFactory.EmailBuilderType;
import com.jecn.epros.server.service.task.TestRunCommon;
import com.jecn.epros.server.service.task.app.ITaskRunTestService;
import com.jecn.epros.server.util.JecnUtil;

/**
 * 任务试运行到期邮件提醒处理类
 * 
 * @author ZHANGXH
 * @date： 日期：Jul 1, 2013 时间：11:30:04 AM
 */
public class TaskRunTestServiceImpl extends AbsBaseService<JecnUser, Long>
		implements ITaskRunTestService {
	/**
	 * 
	 * 试运行到期数据接口
	 */
	private IJecnTaskTestRunDao taskTestRunDao;

	/**
	 * 发送试用行邮件
	 * 
	 * @throws DaoException
	 */
	@Override
	public void setEmailByDate() throws Exception {
		String queryString = "select a from JecnUser as a,JecnRoleInfo as r,UserRole as ur "
				+ "where a.peopleId = ur.peopleId and ur.roleId=r.roleId and r.filterId='admin'";
		List<JecnUser> adminUserList = taskTestRunDao.listHql(queryString);// 获得系统管理员
		if (adminUserList == null || adminUserList.size() == 0) {// 系统管理员不存在
			return;
		}
		/** 试运行到期提醒 */
		sendTestRunEndEmail(adminUserList);
		/** 发送试用行报告 */
		sendTestRunReportEmail(adminUserList);
	}

	/**
	 * 试运行到期提醒
	 * 
	 * @param adminUserList
	 */
	private void sendTestRunEndEmail(List<JecnUser> adminUserList)
			throws Exception {
		// 试运行到期提醒(提醒系统管理员)
		List<Object[]> listObjects = taskTestRunDao.sendTestRunEndE();
		if (listObjects.size() == 0) {
			return;
		}
		this.sendRunEmail(listObjects, 1, adminUserList);
	}

	/**
	 * 发送试用行报告
	 * 
	 * @param adminUserList
	 */
	private void sendTestRunReportEmail(List<JecnUser> adminUserList)
			throws Exception {
		// 获取试运行任务（只有流程任务）相关信息及用户信息
		List<Object[]> listObjects = taskTestRunDao.getTaskTestRun();
		if (listObjects.size() == 0) {
			return;
		}
		this.sendRunEmail(listObjects, 0, adminUserList);
	}

	/**
	 * 试运行邮件提醒
	 * 
	 * @param listObjects
	 * @param type
	 *            0填写试运行报告邮件提醒，1：试运行到期提醒
	 * @param adminUserList
	 */
	private void sendRunEmail(List<Object[]> listObjects, int type,
			List<JecnUser> adminUserList) {
		if (adminUserList == null || adminUserList.size() == 0) {
			return;
		}
		for (Object[] obj : listObjects) {
			List<JecnUser> listSendUser = new ArrayList<JecnUser>();
			JecnTaskBeanNew jecnTaskBeanNew = new JecnTaskBeanNew();
			TestRunCommon.getSendEmailObject(obj, adminUserList, listSendUser,
					jecnTaskBeanNew);
			if (jecnTaskBeanNew == null) {
				continue;
			}
			if (listSendUser.size() == 0) {// 拟稿人不存在
				listSendUser.addAll(adminUserList);
			}
			if(!listSendUser.isEmpty()){
				BaseEmailBuilder emailBuilder = EmailBuilderFactory.getEmailBuilder(EmailBuilderType.PROCESS_TEST_RUN);
				emailBuilder.setData(listSendUser,jecnTaskBeanNew, type);
				List<EmailBasicInfo> buildEmail = emailBuilder.buildEmail();
				//TODO :EMAIL
				JecnUtil.saveEmail(buildEmail);
			}

		}
	}

	public IJecnTaskTestRunDao getTaskTestRunDao() {
		return taskTestRunDao;
	}

	public void setTaskTestRunDao(IJecnTaskTestRunDao taskTestRunDao) {
		this.taskTestRunDao = taskTestRunDao;
	}

}
