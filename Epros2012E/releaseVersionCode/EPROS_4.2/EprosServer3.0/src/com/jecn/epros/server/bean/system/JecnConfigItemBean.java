package com.jecn.epros.server.bean.system;

import java.io.Serializable;
import java.util.Locale;

/**
 * 
 * 和数据库表（JECN_CONFIG_ITEM）对应Bean类
 * 
 * @author ZHOUXY
 * 
 */
public class JecnConfigItemBean implements Serializable {
	/** 主键ID */
	private int id = 0;
	/** 唯一标识 */
	private String mark = null;
	/** 名称 */
	private String name = null;
	/** 默认名称 */
	private String defaultName = null;

	/** 试运行周期、发送试运行报告周期 （天数））、制度版本号 */
	/** 是否显示 0：不显示 1：显示 */
	/** 主导发布类型：1：文控审核主导审批发布；0：正常审批发布（拟稿人主导发布）（默认） */
	private String value = null;
	private String defaultValue = null;

	/** 排序 (1、2、3、4...) */
	private Integer sort = 0;
	private Integer defaultSort = 0;

	/** 1：必填；0：不必填 */
	private Integer isEmpty = 0;
	private Integer defaultIsEmpty = 0;

	/** 大类：0：流程地图 1：流程图 2：制度 3：文件 4:其它 */
	private int typeBigModel = -1;
	/**
	 * 小类：0：审批环节配置; 1：任务界面显示项配置 ;2：基本配置 ;3：流程图建设规范; 4：流程文件; 5：邮箱配置 ;6：权限配置
	 * 17:系统配置，不在面板显示
	 */
	private int typeSmallModel = -1;

	/** 是否是表中数据：是：1；否0 （默认：否） */
	private int isTableData = 0;

	/** 英文名称 */
	private String enName = null;
	/** 英文默认名称 */
	private String enDefaultName = null;
	private String v1;
	private String v2;

	public String info() {
		return "需要更新数据:" + toString();
	}

	@Override
	public String toString() {
		return "JecnConfigItemBean [defaultIsEmpty=" + defaultIsEmpty + ", defaultName=" + defaultName
				+ ", defaultSort=" + defaultSort + ", defaultValue=" + defaultValue + ", enDefaultName="
				+ enDefaultName + ", enName=" + enName + ", id=" + id + ", isEmpty=" + isEmpty + ", isTableData="
				+ isTableData + ", mark=" + mark + ", name=" + name + ", sort=" + sort + ", typeBigModel="
				+ typeBigModel + ", typeSmallModel=" + typeSmallModel + ", v1=" + v1 + ", v2=" + v2 + ", value="
				+ value + "]";
	}

	/**
	 * 
	 * 是否必填，
	 * 
	 * @return bootean true：必填；false：非必填
	 */
	public boolean isNotEmpty() {
		return (isEmpty == null || isEmpty.intValue() == 0) ? false : true;
	}

	public String getName(Locale locale) {
		return locale == Locale.CHINESE ? name : enName;
	}

	public String getName(int type) {
		return type == 0 ? name : enName;
	}

	// --------------------------------

	public String getV1() {
		return v1;
	}

	public void setV1(String v1) {
		this.v1 = v1;
	}

	public String getV2() {
		return v2;
	}

	public void setV2(String v2) {
		this.v2 = v2;
	}

	public String getEnName() {
		return enName;
	}

	public void setEnName(String enName) {
		this.enName = enName;
	}

	public String getEnDefaultName() {
		return enDefaultName;
	}

	public void setEnDefaultName(String enDefaultName) {
		this.enDefaultName = enDefaultName;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getMark() {
		return mark;
	}

	public void setMark(String mark) {
		this.mark = mark;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDefaultName() {
		return defaultName;
	}

	public void setDefaultName(String defaultName) {
		this.defaultName = defaultName;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public String getDefaultValue() {
		return defaultValue;
	}

	public void setDefaultValue(String defaultValue) {
		this.defaultValue = defaultValue;
	}

	public Integer getSort() {
		return sort;
	}

	public void setSort(Integer sort) {
		this.sort = sort;
	}

	public Integer getDefaultSort() {
		return defaultSort;
	}

	public void setDefaultSort(Integer defaultSort) {
		this.defaultSort = defaultSort;
	}

	public Integer getIsEmpty() {
		return isEmpty;
	}

	public void setIsEmpty(Integer isEmpty) {
		this.isEmpty = isEmpty;
	}

	public Integer getDefaultIsEmpty() {
		return defaultIsEmpty;
	}

	public void setDefaultIsEmpty(Integer defaultIsEmpty) {
		this.defaultIsEmpty = defaultIsEmpty;
	}

	public int getTypeBigModel() {
		return typeBigModel;
	}

	public void setTypeBigModel(int typeBigModel) {
		this.typeBigModel = typeBigModel;
	}

	public int getTypeSmallModel() {
		return typeSmallModel;
	}

	public void setTypeSmallModel(int typeSmallModel) {
		this.typeSmallModel = typeSmallModel;
	}

	public int getIsTableData() {
		return isTableData;
	}

	public void setIsTableData(int isTableData) {
		this.isTableData = isTableData;
	}

}
