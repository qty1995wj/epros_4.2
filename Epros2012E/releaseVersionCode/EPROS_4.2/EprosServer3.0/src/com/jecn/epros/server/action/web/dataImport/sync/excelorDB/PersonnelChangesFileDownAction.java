package com.jecn.epros.server.action.web.dataImport.sync.excelorDB;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;

import org.apache.log4j.Logger;

import com.jecn.epros.server.common.BaseAction;
import com.jecn.epros.server.common.JecnCommon;
import com.jecn.epros.server.service.dataImport.sync.excelOrDb.buss.PersonnelChangesFileDownBuss;
import com.jecn.epros.server.service.dataImport.sync.excelOrDb.unit.SyncConstant;
import com.jecn.epros.server.service.dataImport.sync.excelOrDb.unit.SyncErrorInfo;

/**
 * 人员变更数据excel下载
 * @author zhangft
 * @date   2013-6-13 16:30	
 */
@SuppressWarnings("serial")
public class PersonnelChangesFileDownAction extends BaseAction {
	private static final Logger log = Logger.getLogger(PersonnelChangesFileDownAction.class);
	/** 对应的业务处理类 */
	private PersonnelChangesFileDownBuss personnelChangesFileDownBuss = null;
	
	/**
	 * 执行下载
	 * @return String 
	 */
	@Override
	public String execute() throws Exception {
		return checkFile();
	}

	/**
	 * 判断文件是否存在
	 * @return
	 * 
	 * ${basePath}jsp/systemManager/import/importMain.jsp;personImport
	 */
	private String checkFile() {
		File file = new File(personnelChangesFileDownBuss.getOutFilepath());
		String basePath = (String) this.getSession().getAttribute("basePath");
		String result_fail = basePath + "index.jsp?index=systemManage";
		if (file.exists()) {
			return SyncConstant.RESULT_SUCCESS;
		} else {
			ResultHtml(SyncErrorInfo.PERSONNEL_CHANGES_DOWN_FAIL, result_fail);
			return null;
		}
	}
	/**
	 * 
	 * 拼装前台界面提示页面
	 * 
	 * @param info
	 * @param href
	 */
	public void ResultHtml(String info, String href) {
		// 获取打印输出对象
		PrintWriter out = null;
		try {
			getResponse().setContentType("text/html;charset=UTF-8");
			out = getResponse().getWriter();
			if (isNullObj(out)) {
				return;
			}
			out.println("<script language='javascript'>");
			out.println("alert('" + info + "');");
			if (!JecnCommon.isNullOrEmtryTrim(href)) {
				out.println("window.location.href='" + href + "';");
			} 
			out.println("</script>");
			out.flush();
		} catch (IOException e) {
			log.error(e);
		} finally {
			if (isNullObj(out)) {
				return;
			} else {
				out.close();
			}
		}
	}
	
	/**
	 * 获得下载文件的内容，可以直接读入一个物理文件或从数据库中获取内容
	 * @return
	 */
	public InputStream getInputStream(){
		return personnelChangesFileDownBuss.getInputStream();
	}
	
	/**
	 * 对于配置中的 ${fileName}, 获得下载保存时的文件名
	 * @return
	 */
	public String getFileName(){
		return personnelChangesFileDownBuss.getFileName();
	}

	public PersonnelChangesFileDownBuss getPersonnelChangesFileDownBuss() {
		return personnelChangesFileDownBuss;
	}

	public void setPersonnelChangesFileDownBuss(
			PersonnelChangesFileDownBuss personnelChangesFileDownBuss) {
		this.personnelChangesFileDownBuss = personnelChangesFileDownBuss;
	}

}
