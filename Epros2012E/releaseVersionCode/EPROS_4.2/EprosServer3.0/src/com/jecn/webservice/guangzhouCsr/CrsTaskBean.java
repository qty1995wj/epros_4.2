package com.jecn.webservice.guangzhouCsr;

public class CrsTaskBean {
    
	/**任务ID*/
	private Long taskId;
	/**任务名称*/
	private String taskName;
	/**任务类型*/
	private int taskType;
	/**创建时间*/
	private String createTime;
	
	
	public Long getTaskId() {
		return taskId;
	}
	public void setTaskId(Long taskId) {
		this.taskId = taskId;
	}
	public String getTaskName() {
		return taskName;
	}
	public void setTaskName(String taskName) {
		this.taskName = taskName;
	}
	public int getTaskType() {
		return taskType;
	}
	public void setTaskType(int taskType) {
		this.taskType = taskType;
	}
	public String getCreateTime() {
		return createTime;
	}
	public void setCreateTime(String createTime) {
		this.createTime = createTime;
	}
	
	
	
	
	
	
	
}
