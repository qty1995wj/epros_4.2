package epros.draw.gui.workflow.aid;

import java.awt.Color;
import java.awt.FlowLayout;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.JPanel;

import epros.draw.constant.JecnToolBoxConstant.MapElemType;
import epros.draw.gui.designer.JecnDesignerProcess;
import epros.draw.util.DrawCommon;
import epros.draw.util.JecnUIUtil;

/**
 * 
 * 快捷面板：包括活动、角色、接口三个图形按钮
 * 
 * @author ZHOUXY
 * 
 */
public class JecnShortcutLinkFigurePanel extends JPanel {
	/** 快捷添加图形按钮宽 */
	private final int btnWidth = 35;
	/** 快捷添加图形按钮高 */
	private final int btnHeight = 25;
	/** 图形间水平间距 */
	private final int hSpace = 4;
	/** 图形间垂直间距 */
	private final int vSpace = 4;
	/** 图形个数 */
	private final int btnCount = 3;

	/** 按钮1：活动按钮 */
	private JecnShortcutLinkFigureButton roundRectWithLineBtn = null;
	/** 按钮2：决策按钮 */
	private JecnShortcutLinkFigureButton rhombusFigureBtn = null;
	/** 按钮3：接口按钮 */
	private JecnShortcutLinkFigureButton implFigureBtn = null;

	/** 按钮4：箭头六边形接口按钮 */
	private JecnShortcutLinkFigureButton implRightHexagonBtn = null;

	/** 快捷添加图形的主类 */
	private JecnLinkFigureMainProcess linkFigureMainProcess = null;
	/** 触发此组件显示的小三角形 */
	private JecnLinkTrianglesButton actionTriangleButton = null;

	public JecnShortcutLinkFigurePanel(JecnLinkFigureMainProcess linkFigureMainProcess) {
		this.linkFigureMainProcess = linkFigureMainProcess;
		initComponents();
	}

	/**
	 * 
	 * 初始化
	 * 
	 */
	private void initComponents() {

		// 活动按钮
		roundRectWithLineBtn = new JecnShortcutLinkFigureButton(DrawCommon.getCreateFlowActiveType(), this);
		// 决策按钮
		rhombusFigureBtn = new JecnShortcutLinkFigureButton(MapElemType.Rhombus, this);
		// 接口按钮
		implFigureBtn = new JecnShortcutLinkFigureButton(MapElemType.ImplFigure, this);

		// 箭头六边形接口元素
		implRightHexagonBtn = new JecnShortcutLinkFigureButton(MapElemType.InterfaceRightHexagon, this);
		// 布局
		this.setLayout(new FlowLayout(FlowLayout.CENTER, hSpace, vSpace));
		// 背景颜色
		this.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		// 边框
		this.setBorder(BorderFactory.createLineBorder(Color.LIGHT_GRAY));

		// 流程图显示节点
		List<MapElemType> showTypeList = JecnDesignerProcess.getDesignerProcess().getShowElementsType(0);
		// 快捷图形按钮
		this.add(roundRectWithLineBtn);
		this.add(rhombusFigureBtn);
		if (showTypeList.contains(MapElemType.ImplFigure)) {
			this.add(implFigureBtn);
		}

		if (showTypeList.contains(MapElemType.InterfaceRightHexagon)) {
			this.add(implRightHexagonBtn);
		}

		// 事件
		this.addMouseListener(linkFigureMainProcess);
	}

	/**
	 * 
	 * 设置快速添加流程元素的面板位置点或大小
	 * 
	 * @param figureButton
	 *            JecnLinkFigureButton
	 */
	public void resetLinkPanelLocationSize(JecnLinkTrianglesButton trianglesBtn) {
		// 记录触发此组件的小三角形
		actionTriangleButton = trianglesBtn;

		if (trianglesBtn == null) {
			return;
		}

		// 三角形到快捷图形的距离
		int space = 5;
		// 位置点坐标
		int x = 0;
		int y = 0;

		// 设置快速添加面板的位置点或大小
		switch (trianglesBtn.getEditPointType()) {
		case left:// 左
			// 大小
			this.setSize(getBtnWidth() + hSpace * 2, getBtnHeight() * btnCount + vSpace * (btnCount + 1));
			// 位置点
			x = trianglesBtn.getX() - space - this.getWidth();
			y = trianglesBtn.getY() - Math.abs((this.getHeight() - trianglesBtn.getHeight()) / 2);
			this.setLocation(x, y);
			break;
		case right:// 右
			// 大小
			this.setSize(getBtnWidth() + hSpace * 2, getBtnHeight() * btnCount + vSpace * (btnCount + 1));
			// 位置点
			x = trianglesBtn.getX() + trianglesBtn.getWidth() + space;
			y = trianglesBtn.getY() - Math.abs((this.getHeight() - trianglesBtn.getHeight()) / 2);
			this.setLocation(x, y);
			break;
		case top:// 上
			this.setSize(getBtnWidth() * btnCount + hSpace * (btnCount + 1), getBtnHeight() + vSpace * 2);
			// 位置点
			x = trianglesBtn.getX() - Math.abs((this.getWidth() - trianglesBtn.getWidth()) / 2);
			y = trianglesBtn.getY() - space - this.getHeight();
			this.setLocation(x, y);
			break;
		case bottom:// 下
			this.setSize(getBtnWidth() * btnCount + hSpace * (btnCount + 1), getBtnHeight() + vSpace * 2);
			// 位置点
			x = trianglesBtn.getX() - Math.abs((this.getWidth() - trianglesBtn.getWidth()) / 2);
			y = trianglesBtn.getY() + trianglesBtn.getHeight() + space;
			this.setLocation(x, y);
			break;
		default:// 不匹配时 选择左面
		}
		this.revalidate();
		this.repaint();
	}

	public int getBtnWidth() {
		return btnWidth;
	}

	public int getBtnHeight() {
		return btnHeight;
	}

	public JecnLinkFigureMainProcess getLinkFigureMainProcess() {
		return linkFigureMainProcess;
	}

	public JecnLinkTrianglesButton getActionTriangleButton() {
		return actionTriangleButton;
	}

}
