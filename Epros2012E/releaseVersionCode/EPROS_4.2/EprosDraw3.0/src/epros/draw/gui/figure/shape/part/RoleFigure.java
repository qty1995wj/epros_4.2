package epros.draw.gui.figure.shape.part;

import epros.draw.data.figure.JecnFigureData;
import epros.draw.data.figure.JecnRoleData;
import epros.draw.gui.figure.JecnBaseRoleFigure;

/**
 * 
 * 角色
 * 
 * @author Administrator
 */
public class RoleFigure extends JecnBaseRoleFigure {
	public RoleFigure(JecnFigureData figureData) {
		super(figureData);
	}

	public JecnRoleData getFlowElementData() {
		return (JecnRoleData) super.getFlowElementData();
	}
}
