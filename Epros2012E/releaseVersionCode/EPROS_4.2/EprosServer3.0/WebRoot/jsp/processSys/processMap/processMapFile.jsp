<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%
	response.setHeader("Pragma", "No-cache");
	response.setHeader("Cache-Control", "no-cache");
%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<script type="text/javascript">
document.body.onresize = function() {
	setShowProcessFileDivWH('processMapFileDiv');
}
</script>
	</head>

	<body>
		<div  align="center">
			<div id="processMapTitle"
				style="margin-top: 10px; margin-left: 15px; margin-right: 15px; margin-bottom: 15px; width: 95%">
				<table width="100%">
					<s:if test="processFileDownloadIsShow==true">
						<tr>
							<td align="right">
								<a
									href="printFlowDoc.action?flowId=<s:property value='processMapDownloadBean.flowId' />&isMap=true&isPub=${isPub}"
									style="cursor: pointer;"> <img
										src="${basePath}images/common/downLoadDocument.gif" border="0"></img>
								</a>
							</td>
						</tr>
					</s:if>
					<tr>
						<td align="center"
							style="background-color: #E4E4E4; height: 30px; font: bold 15px ;">
							<!-- 流程名称 -->
							<s:property value="processMapDownloadBean.flowName" />
						</td>
					</tr>
				</table>
			</div>
			<div id="processMapFileDiv"
				style="margin-top: 10px; margin-left: 15px; margin-right: 15px; margin-bottom: 15px; width: 95%; overflow-y: scroll;">
				<table width="98%">
					<s:set name="index" value="1" />
					<s:iterator value="processMapDownloadBean.jecnConfigItemBean">
						<!-- 目的 -->
						<s:if test='mark=="processMapPurpose" && value=="1"'>
							<tr
								style="background-color: #F8F8F8; font: bold 12px ; height: 20px;"
								align="left">
								<td>
									<s:property value="#index" />
									、
									<s:text name="objective" />
									<s:set name="index" value="#index+1" />
								</td>
							</tr>
							<tr>
								<td style="padding-left: 10px; word-break: break-all;"
									align="left">
									<s:if
										test="processMapDownloadBean.flowAim!=null && processMapDownloadBean.flowAim!=''">
										<s:property value="processMapDownloadBean.flowAim"
											escape="false" />
									</s:if>
									<s:else>
										<s:text name="none" />
									</s:else>
								</td>
							</tr>
						</s:if>
						<!-- 范围 -->
						<s:if test='mark=="processMapScope" && value=="1"'>
							<tr
								style="background-color: #F8F8F8; font: bold 12px ; height: 20px;"
								align="left">
								<td>
									<s:property value="#index" />
									、
									<s:text name="range" />
									<s:set name="index" value="#index+1" />
								</td>
							</tr>
							<tr>
								<td style="padding-left: 10px; word-break: break-all;"
									align="left">
									<s:if
										test="processMapDownloadBean.flowArea!=null && processMapDownloadBean.flowArea!=''">
										<s:property value="processMapDownloadBean.flowArea"
											escape="false" />
									</s:if>
									<s:else>
										<s:text name="none" />
									</s:else>
								</td>
							</tr>
						</s:if>
						<!-- 术语定义 -->
						<s:if test='mark=="processMapTerms" && value=="1"'>
							<tr
								style="background-color: #F8F8F8; font: bold 12px ; height: 20px;"
								align="left">
								<td>
									<s:property value="#index" />
									、
									<s:text name="isDefinition" />
									<s:set name="index" value="#index+1" />
								</td>
							</tr>
							<tr>
								<td style="padding-left: 10px; word-break: break-all;"
									align="left">
									<s:if
										test="processMapDownloadBean.flowNounDefine!=null && processMapDownloadBean.flowNounDefine!=''">
										<s:property value="processMapDownloadBean.flowNounDefine"
											escape="false" />
									</s:if>
									<s:else>
										<s:text name="none" />
									</s:else>
								</td>
							</tr>
						</s:if>
						<!-- 输入 -->
						<s:if test='mark=="processMapInput" && value=="1"'>
							<tr
								style="background-color: #F8F8F8; font: bold 12px ; height: 20px;"
								align="left">
								<td>
									<s:property value="#index" />
									、
									<s:text name="input" />
									<s:set name="index" value="#index+1" />
								</td>
							</tr>
							<tr>
								<td style="padding-left: 10px; word-break: break-all;"
									align="left">
									<s:if
										test="processMapDownloadBean.flowInput!=null && processMapDownloadBean.flowInput!=''">
										<s:property value="processMapDownloadBean.flowInput"
											escape="false" />
									</s:if>
									<s:else>
										<s:text name="none" />
									</s:else>
								</td>
							</tr>
						</s:if>
						<!-- 输出 -->
						<s:if test='mark=="processMapOutput" && value=="1"'>
							<tr
								style="background-color: #F8F8F8; font: bold 12px ; height: 20px;"
								align="left">
								<td>
									<s:property value="#index" />
									、
									<s:text name="output" />
									<s:set name="index" value="#index+1" />
								</td>
							</tr>
							<tr>
								<td style="padding-left: 10px; word-break: break-all;"
									align="left">
									<s:if
										test="processMapDownloadBean.flowOutput!=null && processMapDownloadBean.flowOutput!=''">
										<s:property value="processMapDownloadBean.flowOutput"
											escape="false" />
									</s:if>
									<s:else>
										<s:text name="none" />
									</s:else>
								</td>
							</tr>
						</s:if>
						<!-- 流程地图 -->
						<s:if test='mark=="processMap" && value=="1"'>
							<tr
								style="background-color: #F8F8F8; font: bold 12px ; height: 20px;"
								align="left">
								<td>
									<s:property value="#index" />
									、
									<s:text name="flowMap" />
									<s:set name="index" value="#index+1" />
								</td>
							</tr>
							<tr>
								<td style="padding-left: 10px; word-break: break-all;"
									align="left">
									<a
										href="process.action?type=processMap&flowId=<s:property value='processMapDownloadBean.flowId'/>&isPub=${isPub}"
										target='_blank' style="cursor: pointer;" class="table"> <s:if
											test="processMapDownloadBean.flowName!=null && processMapDownloadBean.flowName!=''">
											<s:property value="processMapDownloadBean.flowName" />
										</s:if> <s:else>
											<s:text name="none" />
										</s:else> </a>
								</td>
							</tr>
						</s:if>
						<!-- 流程步骤说明 -->
						<s:if test='mark=="processMapInstructions" && value=="1"'>
							<tr
								style="background-color: #F8F8F8; font: bold 12px ; height: 20px;"
								align="left">
								<td>
									<s:property value="#index" />
									、
									<s:text name="processStepsThat" />
									<s:set name="index" value="#index+1" />
								</td>
							</tr>
							<tr>
								<td style="padding-left: 10px; word-break: break-all;"
									align="left">
									<s:if
										test="processMapDownloadBean.flowShowStep!=null && processMapDownloadBean.flowShowStep!=''">
										<s:property value="processMapDownloadBean.flowShowStep"
											escape="false" />
									</s:if>
									<s:else>
										<s:text name="none" />
									</s:else>
								</td>
							</tr>
						</s:if>
						<!-- 附件 -->
						<s:if test='mark=="processMapAttachment" && value=="1"'>
							<tr
								style="background-color: #F8F8F8; font: bold 12px ; height: 20px;"
								align="left">
								<td>
									<s:property value="#index" />
									、
									<s:text name="accessor" />
									<s:set name="index" value="#index+1" />
								</td>
							</tr>
							<tr>
								<td style="padding-left: 10px; word-break: break-all;"
									align="left">
									<s:if
										test="processMapDownloadBean.fileName!=null && processMapDownloadBean.fileName!=''">
										<a
											href="file.action?from=processMap&reqType=public&visitType=downFile&fileId=<s:property value='processMapDownloadBean.fileId'/>&isPub=${isPub}"
											style="cursor: pointer;" target='_blank' class="table"> <s:property
												value="processMapDownloadBean.fileName" /> </a>
									</s:if>
									<s:else>
										<s:text name="none" />
									</s:else>
								</td>
							</tr>
						</s:if>
					</s:iterator>
					<tr></tr>
				</table>
			</div>
		</div>
	</body>
</html>