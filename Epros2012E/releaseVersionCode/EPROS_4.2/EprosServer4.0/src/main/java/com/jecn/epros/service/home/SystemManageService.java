package com.jecn.epros.service.home;

import com.jecn.epros.domain.JecnUser;
import com.jecn.epros.domain.system.UserWebSearchBean;

import java.util.List;

public interface SystemManageService {

    public List<JecnUser> fetchUsers();

    public List<JecnUser> fetchUsersByPage(int startPage, int pageSize);

    public List<JecnUser> fetchUsersForManage(UserWebSearchBean userWebSearchBean);

}
