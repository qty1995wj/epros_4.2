package com.jecn.epros.server.service.dataImport.sync.tempBean;

/**
 * 人员同步人员变更信息
 * 
 * @author Administrator
 * @date： 日期：May 17, 2013 时间：3:38:21 PM
 */
public class DataChangeInfoUser {
	/** 人员名称 */
	String oldUserName;
	String curUserName;
	/** 人员所属岗位名称 */
	String oldPosName;
	String curPosName;
	/** 真实姓名 */
	String oldTureName;
	String curTrueName;
	/** 邮箱类型* */
	String oldEmailType;
	String curEmailType;
	/** 邮箱地址 */
	String oldEmail;
	String curEmail;
	/** 联系方式 */
	String oldPhone;
	String curPhone;

	public String getOldUserName() {
		return oldUserName;
	}

	public void setOldUserName(String oldUserName) {
		this.oldUserName = oldUserName;
	}

	public String getCurUserName() {
		return curUserName;
	}

	public void setCurUserName(String curUserName) {
		this.curUserName = curUserName;
	}

	public String getOldPosName() {
		return oldPosName;
	}

	public void setOldPosName(String oldPosName) {
		this.oldPosName = oldPosName;
	}

	public String getCurPosName() {
		return curPosName;
	}

	public void setCurPosName(String curPosName) {
		this.curPosName = curPosName;
	}

	public String getOldTureName() {
		return oldTureName;
	}

	public void setOldTureName(String oldTureName) {
		this.oldTureName = oldTureName;
	}

	public String getCurTrueName() {
		return curTrueName;
	}

	public void setCurTrueName(String curTrueName) {
		this.curTrueName = curTrueName;
	}

	public String getOldEmailType() {
		return oldEmailType;
	}

	public void setOldEmailType(String oldEmailType) {
		this.oldEmailType = oldEmailType;
	}

	public String getCurEmailType() {
		return curEmailType;
	}

	public void setCurEmailType(String curEmailType) {
		this.curEmailType = curEmailType;
	}

	public String getOldEmail() {
		return oldEmail;
	}

	public void setOldEmail(String oldEmail) {
		this.oldEmail = oldEmail;
	}

	public String getCurEmail() {
		return curEmail;
	}

	public void setCurEmail(String curEmail) {
		this.curEmail = curEmail;
	}

	public String getOldPhone() {
		return oldPhone;
	}

	public void setOldPhone(String oldPhone) {
		this.oldPhone = oldPhone;
	}

	public String getCurPhone() {
		return curPhone;
	}

	public void setCurPhone(String curPhone) {
		this.curPhone = curPhone;
	}
}
