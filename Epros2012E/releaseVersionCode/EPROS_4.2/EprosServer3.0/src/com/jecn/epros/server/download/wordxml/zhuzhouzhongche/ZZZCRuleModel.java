package com.jecn.epros.server.download.wordxml.zhuzhouzhongche;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import wordxml.constant.Constants;
import wordxml.constant.Constants.Align;
import wordxml.constant.Constants.DocGridType;
import wordxml.constant.Constants.HeaderOrFooterType;
import wordxml.constant.Constants.LineRuleType;
import wordxml.constant.Constants.LineStyle;
import wordxml.constant.Constants.PStyle;
import wordxml.element.bean.common.PositionBean;
import wordxml.element.bean.page.DocGrid;
import wordxml.element.bean.page.SectBean;
import wordxml.element.bean.paragraph.ParagraphBean;
import wordxml.element.bean.paragraph.ParagraphLineRule;
import wordxml.element.bean.table.TableBean;
import wordxml.element.dom.graph.LineGraph;
import wordxml.element.dom.page.Header;
import wordxml.element.dom.page.Sect;
import wordxml.element.dom.table.Table;
import wordxml.element.dom.table.TableRow;

import com.jecn.epros.server.bean.download.RuleDownloadBean;
import com.jecn.epros.server.bean.task.JecnTaskHistoryFollow;
import com.jecn.epros.server.bean.task.JecnTaskHistoryNew;
import com.jecn.epros.server.download.wordxml.RuleModel;

public class ZZZCRuleModel extends RuleModel {
	/*** 段落行距 单倍行距 *****/
	private ParagraphLineRule vLine1 = new ParagraphLineRule(1F, LineRuleType.AUTO);
	/****** 段落行距固定值20磅 *********/
	private ParagraphLineRule lineRule = new ParagraphLineRule(18, LineRuleType.EXACT);

	private final String PROCESS_NUMBER_PREFIX = "Q/TEG";

	/**
	 * 默认模版
	 * 
	 * @param ruleDownloadBean
	 * @param path
	 * @param flowChartDirection
	 */
	public ZZZCRuleModel(RuleDownloadBean ruleDownloadBean, String path) {
		super(ruleDownloadBean, path, true);
		// 设置表格同一宽度
		getDocProperty().setNewTblWidth(15F);
		getDocProperty().setNewRowHeight(0.6F);
		getDocProperty().setFlowChartScaleValue(6F);
		setDocStyle(" ", textTitleStyle());
		setDefAscii("黑体");
	}

	@Override
	protected void initFirstSect(Sect firstSect) {
		// 添加前言节点数据
		firstSect.setSectBean(getSectBean());
		addQianYanSectContent(firstSect);
		// addFirstSectContent();
		createCommhdr(firstSect);
	}

	/**
	 * 添加第一节前面的数据
	 */
	private void addFirstSectContent() {
		// 制度名称
		String flowName = ruleDownloadBean.getRuleName();
		// 黑体 三号
		ParagraphBean pBean = new ParagraphBean(Align.center, "黑体", Constants.sanhao, false);
		ParagraphLineRule line = new ParagraphLineRule(1F, LineRuleType.AUTO);
		pBean.setSpace(1.5f, 1.5f, line);
		getDocProperty().getFirstSect().createParagraph(flowName, pBean);

	}

	private void addQianYanSectContent(Sect firstSect) {

		ParagraphBean pBean = new ParagraphBean(Align.center, "黑体", Constants.sanhao, false);
		ParagraphLineRule vLine = new ParagraphLineRule(4.5F, LineRuleType.AUTO);
		pBean.setSpace(0f, 0f, vLine);
		firstSect.createParagraph("前  言", pBean);

		String dutyOrg = nullToEmpty(ruleDownloadBean.getOrgName());
		String dutyUser = nullToEmpty(ruleDownloadBean.getDutyName());
		// 部门审核人
		String orgApprove = "";
		// 评审人会审
		String huiShengApprove = "";
		// IT
		String itApprove = "";
		// 批准人
		String piZhunApprove = "";
		// 事业部
		String shiYeBuApprove = "";
		// 分隔符
		String splitChar = "，";
		JecnTaskHistoryNew curHistory = ruleDownloadBean.getLastHistory();
		if (curHistory != null && curHistory.getListJecnTaskHistoryFollow() != null) {
			for (JecnTaskHistoryFollow approve : curHistory.getListJecnTaskHistoryFollow()) {

				Integer mark = approve.getStageMark();
				String approveName = approve.getName();
				if (StringUtils.isBlank(approveName)) {
					continue;
				}
				if (mark == 2) {
					orgApprove += splitChar + approveName;
				} else if (mark == 7) {
					itApprove += splitChar + approveName;
				} else if (mark == 4) {
					piZhunApprove += splitChar + approveName;
				} else if (mark == 8) {
					shiYeBuApprove += splitChar + approveName;
				} else if (mark == 3) {
					huiShengApprove += splitChar + approveName;
				}
			}
		}

		orgApprove = nullToEmpty(orgApprove.replaceFirst(splitChar, ""));
		itApprove = nullToEmpty(itApprove.replaceFirst(splitChar, ""));
		piZhunApprove = nullToEmpty(piZhunApprove.replaceFirst(splitChar, ""));
		shiYeBuApprove = nullToEmpty(shiYeBuApprove.replaceFirst(splitChar, ""));
		huiShengApprove = nullToEmpty(huiShengApprove.replaceFirst(splitChar, ""));
		String perVersion = "";
		if (ruleDownloadBean.getHistorys() != null && ruleDownloadBean.getHistorys().size() > 1) {
			perVersion = ruleDownloadBean.getHistorys().get(1).getVersionId();
		}
		// 制度编号+上个版本号
		String numberAndPerHistoryVersion = nullToEmpty(ruleDownloadBean.getRuleInputNum()) + " "
				+ nullToEmpty(perVersion);
		JecnTaskHistoryNew latestHistoryNew = ruleDownloadBean.getLastHistory();
		List<String> contents = new ArrayList<String>();
		contents.add("本制度按照GB/T 1.1-2009给出的规则起草。");
		contents.add("本制度代替" + numberAndPerHistoryVersion + "，本次修订的重点和主要变化如下：");
		contents.add("--" + nullToEmpty(latestHistoryNew == null ? "" : latestHistoryNew.getModifyExplain()));
		contents.add("本制度由" + dutyOrg + "提出并归口");
		contents.add("本制度的制度所有者：" + dutyUser);
		contents.add("本标准主要起草人：");
		contents.add("本制度的审核人：" + orgApprove + "  " + huiShengApprove);
		contents.add("本制度的会签人：" + itApprove + "  " + shiYeBuApprove);
		contents.add("本制度的审批人：" + piZhunApprove);

		pBean = new ParagraphBean(Align.left, "宋体", Constants.wuhao, false);
		vLine = new ParagraphLineRule(1F, LineRuleType.AUTO);
		pBean.setSpace(0f, 0.5f, vLine);

		firstSect.createParagraph(contents.toArray(new String[9]), pBean);

		// 添加分页符号
		// firstSect.addParagraph(new Paragraph(true));
	}

	@Override
	protected void initFlowChartSect(Sect flowChartSect) {
		SectBean sectBean = getSectBean();
		sectBean.setHorizontal(true);
		sectBean.setSize(29.7F, 21);
		flowChartSect.setSectBean(sectBean);
		createCommhdr(flowChartSect);
	}

	@Override
	protected void initSecondSect(Sect secondSect) {
		secondSect.setSectBean(getSectBean());
		createCommhdr(secondSect);
	}

	@Override
	protected void initTitleSect(Sect titleSect) {
		titleSect.setSectBean(getSectBean());
		// 添加封皮节点数据
		addTitleSectContent();
	}

	/***
	 * 创建通用页眉
	 * 
	 * @param sect
	 */
	private void createCommhdr(Sect sect) {
		ParagraphBean pBean = new ParagraphBean(Align.right, "黑体", Constants.wuhao, false);
		pBean.setSpace(0f, 0f, vLine1);
		Header header = sect.createHeader(HeaderOrFooterType.odd);
		String processNumber = assembleProcessNumber(ruleDownloadBean.getRuleInputNum());
		header.createParagraph(processNumber, pBean);
	}

	private String assembleProcessNumber(String flowInputNum) {
		String processNumber = "";
		if (flowInputNum == null) {
			return processNumber;
		}
		// processNumber = flowInputNum.startsWith(PROCESS_NUMBER_PREFIX) ?
		// processNumber
		// : (PROCESS_NUMBER_PREFIX + " " + flowInputNum);
		return processNumber;
	}

	/**
	 * 添加封面节点数据
	 */
	private void addTitleSectContent() {

		Sect titleSect = docProperty.getTitleSect();
		// 文件编号
		String flowInputNum = nullToEmpty(ruleDownloadBean.getRuleInputNum());
		// 文件版本
		String flowVersion = nullToEmpty(ruleDownloadBean.getVersion());
		// 　制度名称
		String flowName = nullToEmpty(ruleDownloadBean.getRuleName());
		// 公司名称
		String companyName = "株洲中车时代电气股份有限公司企业标准";
		// 宋体 粗号 居右对齐 单倍行距
		ParagraphBean pBean = new ParagraphBean(Align.right, "Times New Roman", Constants.sishiba, true);
		pBean.setSpace(0f, 0f, vLine1);
		titleSect.createParagraph("Q/TEG", pBean);

		// 宋体 一号 居中对齐 单倍行距
		pBean = new ParagraphBean(Align.center, "黑体", Constants.yihao, false);
		pBean.setSpace(0f, 0f, vLine1);
		titleSect.createParagraph(companyName, pBean);

		// Q/TEG 制度编号 黑体十四号
		pBean = new ParagraphBean(Align.right, "黑体", Constants.shisihao, false);
		docProperty.getTitleSect().createParagraph(flowInputNum, pBean);

		// 单线条
		LineGraph line = new LineGraph(LineStyle.single, 0.75F);
		line.setFrom(0.75F, 1.15F);
		line.setTo(495F, 1.15F);
		PositionBean positionBean = new PositionBean();
		positionBean.setMarginLeft(0);
		line.setPositionBean(positionBean);
		titleSect.createParagraph(line);

		titleSect.createMutilEmptyParagraph(5);

		// 竞争对手信息管理办法 黑体 26
		pBean = new ParagraphBean(Align.center, "黑体", 52, false);
		pBean.setSpace(0f, 0f, vLine1);
		titleSect.createParagraph(flowName, pBean);

		// （制度版本号V1.0）
		pBean = new ParagraphBean(Align.center, "黑体", 44, false);
		titleSect.createParagraph("（制度版本号" + flowVersion + "）", pBean);

		// 2016 - 04 - 11发布 黑体 14
		pBean = new ParagraphBean(Align.left, "黑体", 28, false);
		String pubDateStr = ruleDownloadBean.getPubDate();
		if (pubDateStr.length() > 10) {
			pubDateStr = pubDateStr.substring(0, 10);
		}
		// 2016 - 05 - 01实施 黑体 14
		String nextMonthFirstDay = "";
		if (StringUtils.isNotBlank(pubDateStr)) {

			try {
				// 下一月第一天
				SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
				Date pubDate = format.parse(pubDateStr);
				Calendar cal = Calendar.getInstance();
				cal.setTime(pubDate);
				cal.add(Calendar.MONTH, 1);
				cal.set(Calendar.DAY_OF_MONTH, 1);
				nextMonthFirstDay = format.format(cal.getTime());

			} catch (ParseException e) {
				log.error(e);
			}

		}

		// 换行
		titleSect.createMutilEmptyParagraph(21);

		// 14个\t
		String space = "                                         ";
		titleSect.createParagraph(pubDateStr + "发布" + space + nextMonthFirstDay + "实施", pBean);

		// 横线
		// 单线条
		line = new LineGraph(LineStyle.single, 0.75F);
		line.setFrom(0.75F, -5F);
		line.setTo(495F, -5F);
		positionBean = new PositionBean();
		positionBean.setMarginLeft(0);
		line.setPositionBean(positionBean);
		titleSect.createParagraph(line);

		// 株洲中车时代电气股份有限公司 发布 黑体14
		pBean = new ParagraphBean(Align.center, "黑体", 28, false);
		titleSect.createParagraph("株 洲 中 车 时 代 电 气 股 份 有 限 公 司        发   布", pBean);
	}

	/**
	 * 获取默认的节属性
	 * 
	 * @return
	 */
	private SectBean getSectBean() {
		SectBean sectBean = new SectBean();
		sectBean.setDocGrid(new DocGrid(DocGridType.LINES, 16.3F));
		// 设置页边距
		sectBean.setPage(1.5F, 2F, 1.5F, 1.7F, 1F, 1.2F);
		sectBean.setSize(21, 29.7F);
		return sectBean;
	}

	@Override
	public TableBean tblStyle() {
		TableBean tblStyle = new TableBean();// 默认表格样式
		// 所有边框 0.5 磅
		tblStyle.setBorder(0.5F);
		tblStyle.setCellMargin(0.1F, 0.2F, 0, 0.2F);
		tblStyle.setTableAlign(Align.center);
		return tblStyle;
	}

	@Override
	public ParagraphBean tblTitleStyle() {
		ParagraphBean tblTitileStyle = new ParagraphBean(Align.center, "宋体", Constants.wuhao, true);
		tblTitileStyle.setSpace(0f, 0f, vLine1);
		return tblTitileStyle;
	}

	@Override
	public ParagraphBean textContentStyle() {
		ParagraphBean textContentStyle = new ParagraphBean(Align.left, "宋体", Constants.wuhao, false);
		textContentStyle.setSpace(0f, 0f, vLine1);
		textContentStyle.setInd(0f, 0f, 0f, 0.7F);
		return textContentStyle;
	}

	@Override
	public ParagraphBean textTitleStyle() {
		ParagraphBean textTitleStyle = new ParagraphBean("黑体", Constants.wuhao, false);
		textTitleStyle.setSpace(1F, 1F, vLine1);
		textTitleStyle.setpStyle(PStyle.LVL1);
		return textTitleStyle;
	}

	@Override
	protected void beforeHandle() {

	}

	private void setHeight(Table table, float height) {
		for (TableRow row : table.getRows()) {
			row.setHeight(height);
		}
	}

	@Override
	public ParagraphBean tblContentStyle() {
		ParagraphBean tblContentStyle = new ParagraphBean("宋体", Constants.wuhao, false);
		tblContentStyle.setSpace(0f, 0f, vLine1);
		return tblContentStyle;
	}

}
