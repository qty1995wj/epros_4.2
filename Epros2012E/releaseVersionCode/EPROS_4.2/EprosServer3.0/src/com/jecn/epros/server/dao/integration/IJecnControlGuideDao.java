package com.jecn.epros.server.dao.integration;

import java.util.Set;

import com.jecn.epros.server.bean.integration.JecnControlGuide;
import com.jecn.epros.server.common.IBaseDao;

/**
 * 内控指引知识库Dao接口
 * @author Administrator
 *
 */
public interface IJecnControlGuideDao extends IBaseDao<JecnControlGuide, Long> {

	/**
	 * 删除
	 * @param setIds ID集合
	 * @throws Exception
	 */
	public void deleteControlGuide(Set<Long> setIds) throws Exception;
	
}
