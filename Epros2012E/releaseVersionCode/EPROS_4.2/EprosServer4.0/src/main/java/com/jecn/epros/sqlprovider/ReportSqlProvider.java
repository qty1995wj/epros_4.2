package com.jecn.epros.sqlprovider;

import com.jecn.epros.domain.EnumClass;
import com.jecn.epros.domain.report.Part;
import com.jecn.epros.domain.report.PrimaryActivitySearchParam;
import com.jecn.epros.sqlprovider.server3.CommonSqlTPath;
import com.jecn.epros.sqlprovider.server3.SqlCommon;
import com.jecn.epros.util.ConfigUtils;
import com.jecn.epros.util.Utils;
import org.apache.commons.lang3.StringUtils;
import org.apache.ibatis.jdbc.SQL;
import org.springframework.jdbc.core.SqlProvider;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class ReportSqlProvider {

    /**
     * 流程清单报表显示的图片
     *
     * @return
     */
    public String processDetailView(Map<String, Object> map) {

        List<Long> mapIds = (List<Long>) map.get("mapIds");
        List<Long> orgIds = (List<Long>) map.get("orgIds");
        boolean isMap = false;
        boolean isOrg = false;
        if (mapIds != null && mapIds.size() > 0) {
            isMap = true;
        }
        if (orgIds != null && orgIds.size() > 0) {
            isOrg = true;
        }

        boolean both = isOrg && isMap;

        String mapQuery = "select jfst.flow_id from jecn_flow_structure_t jfo"
                + "	inner JOIN jecn_flow_structure_t jfst ON jfst.t_path like "
                + SqlCommon.getJoinFunc("jfo")
                + "						AND jfst.flow_id <> jfo.flow_id and jfo.flow_id in " + SqlCommon.getIds(mapIds);

        String sql = "		WITH t1 AS (";
        if (isOrg) {
            sql += "					SELECT"
                    + "						MAX (h.publish_date) AS publish_date,"
                    + "						jfst.flow_id,"
                    + "						jfo.org_id id,"
                    + "						jfo.org_name name,"
                    + "						0 AS type"
                    + "					FROM"
                    + "						jecn_flow_org jfo"
                    + "					LEFT JOIN JECN_FLOW_RELATED_ORG_T jfrot ON jfo.org_id = jfrot.org_id"
                    + "					LEFT JOIN jecn_flow_structure_t jfst ON jfrot.flow_id = jfst.flow_id"
                    + "					AND jfst.del_state = 0"
                    + "					AND jfst.create_date <= #{endTime}"
                    + "                   and jfst.projectId=#{projectId}";
            if (isMap) {
                sql += " and jfst.flow_id in (" + mapQuery + ")";
            }
            sql += "					LEFT JOIN JECN_TASK_HISTORY_NEW h ON jfst.flow_id = h.RELATE_ID"
                    + "					AND h.Type = 0"
                    + "					AND h.publish_date <= #{endTime}"
                    + "					WHERE"
                    + "						jfo.org_id IN "
                    + SqlCommon.getIds(orgIds)
                    + "					GROUP BY"
                    + "						jfst.flow_id,"
                    + "						jfo.org_id,"
                    + "						jfo.org_name";
        }
        if (!both && isMap) {
            sql += "						SELECT" + "							MAX (h.publish_date) AS publish_date,"
                    + "							jfst.flow_id," + "							jfo.flow_id id,"
                    + "							jfo.flow_name NAME," + "							1 AS type"
                    + "						FROM" + "							jecn_flow_structure_t jfo"
                    + "						LEFT JOIN jecn_flow_structure_t jfst ON jfst.t_path like "
                    + SqlCommon.getJoinFunc("jfo") + "						AND jfst.flow_id <> jfo.flow_id"
                    + "						AND jfst.del_state = 0" + "						AND jfst.ISFLOW = 1"
                    + "						AND jfst.create_date <= #{endTime}"
                    + "                       and jfst.projectId=#{projectId}"
                    + "						LEFT JOIN JECN_TASK_HISTORY_NEW h ON jfst.flow_id = h.RELATE_ID"
                    + "						AND h.Type = 0" + "						AND h.publish_date <= #{endTime}"
                    + "						WHERE" + "							jfo.flow_id IN "
                    + SqlCommon.getIds(mapIds) + "						GROUP BY"
                    + "							jfst.flow_id," + "							jfo.flow_id,"
                    + "							jfo.flow_name ";
        }
        sql += "				)," + "				 t2 AS (" + "					SELECT"
                + "						CASE" + "					WHEN t1.publish_date IS NULL THEN"
                + "						x2.update_date" + "					ELSE"
                + "						t1.publish_date" + "					END AS publish_date,"
                + "					t1.flow_id," + "					t1.id," + "					t1.name,"
                + "					t1.type" + "				FROM" + "					t1"
                + "				LEFT JOIN JECN_FLOW_STRUCTURE x2 ON x2.flow_id = t1.flow_id"
                + "				AND x2.update_date <= #{endTime}" + "				)," + "				 t3 AS ("
                + "					SELECT" + "						t2.publish_date AS last_publish_date,"
                + "						t2.flow_id," + "						t2.id,"
                + "						t2.name," + "						t2.type,"
                + "						pubCount," + "						notPubCount" + "					FROM"
                + "						t2" + "					LEFT JOIN (" + "						SELECT"
                + "							SUM (" + "								CASE"
                + "								WHEN t2.publish_date IS NOT NULL THEN"
                + "									1" + "								ELSE"
                + "									0" + "								END"
                + "							) AS pubCount," + "							SUM ("
                + "								CASE" + "								WHEN t2.publish_date IS NULL"
                + "								AND t2.flow_id IS NOT NULL THEN"
                + "									1" + "								ELSE"
                + "									0" + "								END"
                + "							) AS notPubCount," + "							id"
                + "						FROM" + "							t2" + "						GROUP BY"
                + "							id" + "					) tt ON t2.id = tt.id"
                + "					LEFT JOIN jecn_task_history_new next_scan_time ON t2.flow_id = next_scan_time.relate_id"
                + "					AND next_scan_time.publish_date > t2.publish_date" + "					GROUP BY"
                + "						t2.publish_date," + "						t2.flow_id,"
                + "						t2.id," + "						t2.name," + "						t2.type,"
                + "						pubCount," + "						notPubCount" + "				) "
                + "				SELECT DISTINCT" + "					id," + "					name,"
                + "					type," + "					pubCount," + "					notPubCount,"
                + "					pubCount + notPubCount AS total,"
                + "					case when pubCount+notPubCount=0" + "					then 0"
                + "					else pubCount*100/(pubCount+notPubCount) end as percentage" + "				FROM"
                + "					t3 order by t3.id asc";

        return sql;
    }

    /**
     * 流程审视优化统计报表显示的图片
     *
     * @return
     */
    public String processOptimizeView(Map<String, Object> map) {

        int type = (Integer) map.get("type");
        String keyId = "";
        String typeCondition = "";
        if (type == 0) {
            keyId = "flow_id";
            typeCondition = " = 0";
        } else {
            keyId = "rule_id";
            typeCondition = " in (2,3)";
        }

        String sql = "		WITH tmp AS" + "		 (SELECT d.RELATE_ID," + "						 d.PUBLISH_DATE,"
                + "						 MAX(d.last_PUBLISH_DATE) AS last_PUBLISH_DATE"
                + "				FROM (SELECT a.PUBLISH_DATE," + "										 a.RELATE_ID,"
                + "										 b.PUBLISH_DATE AS last_PUBLISH_DATE,"
                + "										 b.RELATE_ID    AS last_RELATE_ID"
                + "								FROM JECN_TASK_HISTORY_NEW a"
                + "								LEFT JOIN JECN_TASK_HISTORY_NEW b"
                + "									ON b.type" + typeCondition
                + "								 AND b.expiry <> 0"
                + "								 AND b.type = a.type"
                + "								 AND b.relate_id = a.relate_id"
                + "								 AND b.publish_date < a.publish_date"
                + "							 WHERE a.type" + typeCondition
                + "								 AND b.publish_date IS NOT NULL) d"
                + "			 GROUP BY d.PUBLISH_DATE, d.RELATE_ID" + "			UNION ALL"
                + "			SELECT t4.relate_id, t4.publish_date, t4.last_publish_date"
                + "				FROM (SELECT t2.relate_id, t2.publish_date, t2.last_publish_date"
                + "								FROM (SELECT t1.relate_id,"
                + "														 MAX(t1.publish_date) AS publish_date,"
                + "														 MAX(t1.publish_date) AS last_publish_date"
                + "												FROM jecn_task_history_new t1"
                + "											 WHERE t1.type" + typeCondition
                + "											 GROUP BY t1.relate_id) t2"
                + "							 INNER JOIN jecn_task_history_new t3"
                + "									ON t2.relate_id = t3.relate_id"
                + "								 AND t2.publish_date = t3.publish_date"
                + "								 AND t3.expiry <> 0"
                + "								 AND t3.type" + typeCondition + ") t4),"
                + "		tmp1 AS" + "		 (";
        if (type == 0) {
            sql += getOrgOrMapSql(map);
        } else {
            sql += getOrgOrRuleDirSql(map);
        }
        sql += ")," + "		tmp2 AS" + "		 (SELECT tmp1.*," + "						 CASE"
                + "							 WHEN tmp.publish_date = tmp.last_PUBLISH_DATE THEN"
                + "								0" + "							 WHEN ";
        if (BaseSqlProvider.isOracle()) {
            sql += " add_months(tmp.last_PUBLISH_DATE,x.expiry) <="
                    + "										#{endTime} AND"
                    + " to_char(tmp.PUBLISH_DATE, 'YYYY/MM/DD')<=to_char(x.next_scan_date, 'YYYY/MM/DD')";
        } else if (BaseSqlProvider.isSQLServer()) {
            sql += " dateadd(MONTH, x.expiry, tmp.last_PUBLISH_DATE) <="
                    + "										#{endTime} AND"
                    + "										CONVERT(VARCHAR(10), tmp.PUBLISH_DATE, 23) <="
                    + "										CONVERT(VARCHAR(10), x.next_scan_date, 23) ";
        }

        sql += "THEN" + "								1" + "							 ELSE"
                + "								0" + "						 END finishNum,"
                + "						 1 AS totalnum" + "				FROM tmp1" + "			 INNER JOIN tmp"
                + "					ON tmp1." + keyId + " = tmp.RELATE_ID" + "			 INNER JOIN JECN_TASK_HISTORY_NEW x"
                + "					ON x.expiry <> 0"
                + "				 AND tmp.RELATE_ID = x.relate_id"
                + "				 AND x.TYPE" + typeCondition
                + "				 AND tmp.last_PUBLISH_DATE = x.publish_date"
                + "				 AND x.next_scan_date BETWEEN"
                + "						 #{startTime} AND #{endTime})"
                + "		select " + "		r.id,r.name,r.type,"
                + "		case when r.finishNum is null then 0 else r.finishNum end as finishNum,"
                + "		case when r.totalnum is null then 0 else r.totalnum end as totalNum,"
                + "		case when r.totalNum>0 then floor(r.finishNum*100/r.totalNum) else 0 end as percentage"
                + "		from (" + "		select s.id,s.name,s.type," + "		 sum(tmp2.finishNum) as finishNum,"
                + "		 sum(tmp2.totalnum) as totalNum"
                + "		from (select DISTINCT tmp1.id,tmp1.name,tmp1.type from tmp1)s"
                + "		left join tmp2 on s.id=tmp2.id and s.type=tmp2.type group by s.id,s.name,s.type" + "		)r order by r.id asc";

        return sql;
    }

    public String processKpiView(Map<String, Object> paramMap) {

        List<Long> mapIds = (List<Long>) paramMap.get("mapIds");
        List<Long> orgIds = (List<Long>) paramMap.get("orgIds");
        boolean isMap = false;
        boolean isOrg = false;
        if (mapIds != null && mapIds.size() > 0) {
            isMap = true;
        }
        if (orgIds != null && orgIds.size() > 0) {
            isOrg = true;
        }

        String sql = "		WITH flows AS" + "		 (";
        sql += getOrgOrMapSql(paramMap);
        sql += ") "
                + "SELECT f.flow_id,f.flow_name,f.id,f.name,f.type,KPI.KPI_AND_ID,KPI.KPI_NAME,KPI.KPI_TARGET,KV.KPI_AND_ID,KV.KPI_HOR_VALUE,KV.KPI_VALUE"
                + "			FROM flows F" + "			left JOIN JECN_FLOW_KPI_NAME KPI"
                + "				ON KPI.FLOW_ID = F.FLOW_ID" + "			left JOIN JECN_FLOW_KPI KV"
                + "				ON KV.KPI_AND_ID = KPI.KPI_AND_ID" + "			 AND KV.KPI_HOR_VALUE BETWEEN"
                + "					 #{startTime} AND" + "					 #{endTime} "
                + "		order by KV.KPI_HOR_VALUE asc";

        return sql;

    }

    /**
     * 角色饼状图
     *
     * @param paramMap
     * @return
     */
    public String processRoleNum(Map<String, Object> paramMap) {

        String sql = SqlCommon.getOrgOrMapFlow(paramMap);

        sql += "  select result.part,result.id,result.name,result.type,count(result.flow_id) as value"
                + "    from (select case when count(jfsi.figure_id) >= 0 and count(jfsi.figure_id) <= 3 then  '" + Part.roleRangeKeys.get(0) + "' "
                + "                      when count(jfsi.figure_id) >= 4 and count(jfsi.figure_id) <= 8 then  '" + Part.roleRangeKeys.get(1) + "' "
                + "                      when count(jfsi.figure_id) >= 9 then  '" + Part.roleRangeKeys.get(2) + "'"
                + "                 end as part, jfs.id,jfs.name,jfs.flow_id,jfs.type"
                + "            from  flows jfs left join jecn_flow_structure_image jfsi"
                + "              on jfsi.flow_id = jfs.flow_id  and jfsi.figure_type in " + SqlCommon.getRoleString()
                + "   group by jfs.id,jfs.name,jfs.flow_id,jfs.type) result"
                + "   group by result.part,result.id,result.name,result.type";

        return sql;

    }

    /**
     * 活动饼状图
     *
     * @param paramMap
     * @return
     */
    public String processActivityNum(Map<String, Object> paramMap) {

        String sql = SqlCommon.getOrgOrMapFlow(paramMap);

        sql += "  select result.part,result.id,result.name,result.type,count(result.flow_id) as value"
                + "    from (select case when count(jfsi.figure_id) >= 0 and count(jfsi.figure_id) <= 10 then '" + Part.activityRangeKeys.get(0) + "'"
                + "                      when count(jfsi.figure_id) >= 11 and count(jfsi.figure_id) <= 20 then '" + Part.activityRangeKeys.get(1) + "'"
                + "                      when count(jfsi.figure_id) >= 21 then  '" + Part.activityRangeKeys.get(2) + "'"
                + "                 end as part,jfs.id,jfs.name,jfs.flow_id,jfs.type"
                + "            from flows jfs left join jecn_flow_structure_image jfsi"
                + "              on jfsi.flow_id = jfs.flow_id  and jfsi.figure_type in "
                + SqlCommon.getActiveString() + "           group by jfs.id,jfs.name,jfs.flow_id,jfs.type) result"
                + "   group by result.part,result.id,result.name,result.type";

        return sql;

    }

    /**
     * 打开一个角色饼状图区域关联的流程的个数
     *
     * @param paramMap
     * @return
     */
    public String processRoleDetailNum(Map<String, Object> paramMap) {

        String sql = activityOrRoleRelatedFlowSql(paramMap, false, true);
        return sql;

    }

    /**
     * 打开一个活动饼状图区域关联的流程的个数
     *
     * @param paramMap
     * @return
     */
    public String processActivityDetailNum(Map<String, Object> paramMap) {
        String sql = activityOrRoleRelatedFlowSql(paramMap, true, true);
        return sql;
    }

    /**
     * 角色饼状图打开一个区域
     *
     * @param paramMap
     * @return
     */
    public String processRoleDetail(Map<String, Object> paramMap) {
        String sql = activityOrRoleRelatedFlowSql(paramMap, false, false);
        return sql;
    }

    /**
     * 活动饼状图打开一个区域
     *
     * @param paramMap
     * @return
     */
    public String processActivityDetail(Map<String, Object> paramMap) {

        String sql = activityOrRoleRelatedFlowSql(paramMap, true, false);

        return sql;
    }

    /**
     * 活动或者角色的关联流程sql
     *
     * @param paramMap
     * @param isActivity true活动 false角色
     * @param isCount    true详情 false数量
     * @return
     */
    private String activityOrRoleRelatedFlowSql(Map<String, Object> paramMap, boolean isActivity, boolean isCount) {
        String sql = SqlCommon.getOrgOrMapFlow(paramMap);

        String condition = "";
        if (isActivity) {
            condition = SqlCommon.getActiveString();
        } else {
            condition = SqlCommon.getRoleString();
        }
        if (isCount) {
            sql += " select count(1) from (";
        }
        sql += "	SELECT JFS.FLOW_ID," + "				 JFS.FLOW_NAME," + "				 JFS.FLOW_ID_INPUT,"
                + "				 JFO.ORG_NAME," + "				 CASE"
                + "					 WHEN JFBI.TYPE_RES_PEOPLE = 0 THEN" + "						JU.TRUE_NAME"
                + "					 WHEN JFBI.TYPE_RES_PEOPLE = 1 THEN" + "						JFOI.FIGURE_TEXT"
                + "				 END AS RES_PEOPLE" + "		FROM jecn_flow_structure JFS" + "	 INNER JOIN ("
                + "	SELECT COUNT(jfsi.figure_id) as num, jfsi.flow_id"
                + "								 FROM jecn_flow_structure_image jfsi"
                + "								WHERE jfsi.flow_id IN (SELECT jfs.flow_id FROM flows jfs)"
                + "								and jfsi.figure_type in " + condition
                + "								GROUP BY jfsi.flow_id"
                + "							 HAVING COUNT(jfsi.figure_id) >= #{start} ";
        if (paramMap.get("end") != null) {
            sql += "AND COUNT(jfsi.figure_id) <= #{end}";
        }
        sql += "	)a" + "			ON a.flow_id = jfs.flow_id" + "	 INNER JOIN JECN_FLOW_BASIC_INFO JFBI"
                + "			ON JFS.FLOW_ID = JFBI.FLOW_ID" + "		LEFT JOIN JECN_FLOW_RELATED_ORG JFRO"
                + "			ON JFRO.FLOW_ID = JFBI.FLOW_ID" + "		LEFT JOIN JECN_FLOW_ORG JFO"
                + "			ON JFO.ORG_ID = JFRO.ORG_ID" + "		 AND JFO.DEL_STATE = 0"
                + "		LEFT JOIN JECN_USER JU" + "			ON JFBI.TYPE_RES_PEOPLE = 0" + "		 AND JU.ISLOCK = 0"
                + "		 AND JU.PEOPLE_ID = JFBI.RES_PEOPLE_ID" + "		LEFT JOIN JECN_FLOW_ORG_IMAGE JFOI"
                + "			ON JFBI.TYPE_RES_PEOPLE = 1" + "		 AND JFOI.FIGURE_ID = JFBI.RES_PEOPLE_ID";
        if (isCount) {
            sql += " )a";
        }
        return sql;
    }

    /**
     * 信息系统饼状图
     *
     * @param paramMap
     * @return
     */
    public String processInformation(Map<String, Object> paramMap) {
        String sql = SqlCommon.getOrgOrMapFlow(paramMap);
        sql += ",tool as (select jfsi.figure_id as active_id,jfst.FLOW_SUSTAIN_TOOL_ID as tool_id,jfst.FLOW_SUSTAIN_TOOL_DESCRIBE as tool_name,flows.id,flows.name,flows.type from "
                + "	JECN_FLOW_STRUCTURE_IMAGE jfsi "
                + " inner join flows on flows.flow_id=jfsi.FLOW_ID"
                + "	left join JECN_ACTIVE_ONLINE jao on jfsi.figure_id = jao.active_id"
                + "	left join jecn_flow_sustain_tool jfst on jfst.FLOW_SUSTAIN_TOOL_ID=jao.TOOL_ID )"
                + " select t.id,t.name,t.tool_id as part,t.tool_name as partName, sum( case when t.active_id is not null and t.tool_id is not null then 1 else 0 end ) as value from tool t"
                + " group by t.id,t.name,t.tool_id,t.tool_name ";
        return sql;
    }

    public String processInformationDetailNum(Map<String, Object> paramMap) {
        String sql = SqlCommon.getOrgOrMapFlow(paramMap);
        sql += "select count(jfsi.figure_id) ";
        sql += informationBaseSql(paramMap);
        return sql;
    }

    /**
     * 活动信息关联的活动
     *
     * @param paramMap
     * @return
     */
    public String processInformationDetail(Map<String, Object> paramMap) {
        String sql = SqlCommon.getOrgOrMapFlow(paramMap);
        sql += " select jfsi.figure_id,jfsi.figure_text,jfsi.activity_id";
        sql += informationBaseSql(paramMap);
        return sql;
    }

    public String informationBaseSql(Map<String, Object> paramMap) {
        String sql = " from JECN_FLOW_STRUCTURE_IMAGE jfsi" + "	INNER JOIN flows ON flows.flow_id = jfsi.FLOW_ID"
                + "	INNER JOIN JECN_ACTIVE_ONLINE jao ON jfsi.FLOW_ID = jao.PROCESS_ID"
                + "	INNER JOIN jecn_flow_sustain_tool jfst ON jfst.FLOW_SUSTAIN_TOOL_ID = jao.TOOL_ID and jfst.FLOW_SUSTAIN_TOOL_ID=#{itId} where jfsi.figure_type in "
                + SqlCommon.getActiveString();
        return sql;
    }

    /**
     * 获得该流程架构或者责任部门下所有的未删除的流程(流程架构包括子节点的子节点，责任部门只包含当前的)
     *
     * @param paramMap
     * @return
     */
    private String getOrgOrMapSql(Map<String, Object> paramMap) {

        List<Long> mapIds = (List<Long>) paramMap.get("mapIds");
        List<Long> orgIds = (List<Long>) paramMap.get("orgIds");
        boolean isMap = false;
        boolean isOrg = false;
        if (mapIds != null && mapIds.size() > 0) {
            isMap = true;
        }
        if (orgIds != null && orgIds.size() > 0) {
            isOrg = true;
        }

        boolean bothExist = isMap && isOrg;

        String sql = "";
        if (isOrg) {
            sql += "SELECT jfst.flow_id,jfst.flow_name,jfo.org_id id, jfo.org_name name, 0 AS type"
                    + "				FROM jecn_flow_org jfo" + "				LEFT JOIN JECN_FLOW_RELATED_ORG_T jfrot"
                    + "					ON jfo.org_id = jfrot.org_id"
                    + "				LEFT JOIN jecn_flow_structure_t jfst"
                    + "					ON jfrot.flow_id = jfst.flow_id" + "				 AND jfst.del_state = 0";
            if (isMap) {
                sql += " and JFST.FLOW_ID IN " + getChildFlows(mapIds);
            }
            sql += " and jfst.projectId=#{projectId}" + "			 WHERE jfo.org_id IN "
                    + SqlCommon.getIds(orgIds);

        }
//        if (isOrg && isMap) {
//            sql += "			UNION";
//        }
        if (!bothExist && isMap) {
            sql += "			SELECT jfst.flow_id,jfst.flow_name,jfo.flow_id id, jfo.flow_name NAME, 1 AS type"
                    + "				FROM jecn_flow_structure_t jfo"
                    + "				LEFT JOIN jecn_flow_structure_t jfst" + "					ON jfst.t_path like "
                    + SqlCommon.getJoinFunc("jfo") + "				 AND jfst.flow_id <> jfo.flow_id"
                    + "				 AND jfst.del_state = 0" + "				 AND jfst.ISFLOW = 1"
                    + " and jfst.projectId=#{projectId}" + "			 WHERE jfo.flow_id IN "
                    + SqlCommon.getIds(mapIds);
        }

        return sql;

    }

    private String getOrgOrRuleDirSql(Map<String, Object> paramMap) {

        List<Long> mapIds = (List<Long>) paramMap.get("mapIds");
        List<Long> orgIds = (List<Long>) paramMap.get("orgIds");
        boolean isMap = false;
        boolean isOrg = false;
        if (mapIds != null && mapIds.size() > 0) {
            isMap = true;
        }
        if (orgIds != null && orgIds.size() > 0) {
            isOrg = true;
        }

        boolean bothExist = isMap && isOrg;

        String sql = "";
        if (isOrg) {
            sql += "SELECT jfst.id as rule_id,jfst.rule_name,jfo.org_id id, jfo.org_name name, 0 AS type"
                    + "				FROM jecn_flow_org jfo"
                    + "				LEFT JOIN jecn_rule_t jfst"
                    + "					ON jfo.org_id = jfst.org_id";
            if (isMap) {
                sql += " and JFST.ID IN " + getChildRules(mapIds);
            }
            sql += " and jfst.project_Id=#{projectId}" + "			 WHERE jfo.org_id IN "
                    + SqlCommon.getIds(orgIds);

        }
        if (!bothExist && isMap) {
            sql += "			SELECT jfst.id as rule_id,jfst.rule_name,jfo.id id, jfo.rule_name NAME, 1 AS type"
                    + "				FROM jecn_rule_t jfo"
                    + "				LEFT JOIN jecn_rule_t jfst" + "					ON jfst.t_path like "
                    + SqlCommon.getJoinFunc("jfo") + "				 AND jfst.id <> jfo.id"
                    + "				 AND jfst.is_dir <> 0 "
                    + " and jfst.project_Id=#{projectId}" + "			 WHERE jfo.id IN "
                    + SqlCommon.getIds(mapIds);
        }

        return sql;

    }

    private String getChildFlows(List<Long> mapIds) {
        String sql = "(SELECT" +
                "		c.flow_id" +
                "	FROM" +
                "		JECN_FLOW_STRUCTURE_T c" +
                "	INNER JOIN JECN_FLOW_STRUCTURE_T p ON c.T_PATH LIKE " + SqlCommon.getJoinFunc("p") +
                "	AND c.T_PATH IS NOT NULL" +
                "	AND p.T_PATH IS NOT NULL" +
                "	AND c.ISFLOW=1" +
                "	AND p.FLOW_ID in " + SqlCommon.getIds(mapIds) + ")A";
        return sql;
    }

    private String getChildRules(List<Long> dirIds) {
        String sql = "(SELECT" +
                "		c.id" +
                "	FROM" +
                "		jecn_rule_t c" +
                "	INNER JOIN jecn_rule_t p ON c.T_PATH LIKE " + SqlCommon.getJoinFunc("p") +
                "	AND c.T_PATH IS NOT NULL" +
                "	AND p.T_PATH IS NOT NULL" +
                "	AND c.is_dir in (2,3)" +
                "	AND p.id in " + SqlCommon.getIds(dirIds) + ")";
        return sql;
    }

    public String primaryManageNum(Map<String, Object> map) {
        String sql = "select count(*) from (" + primarySql(map) + ") a";
        return sql;
    }

    public String fetchPrimaryManage(Map<String, Object> map) {
        String sql = "select jfs.flow_id," +
                "       jfs.flow_name," +
                "       jfs.buss_type," +
                "       jfo.org_id," +
                "       jfo.org_name," +
                "       jfbi.flow_customfive as allow_rule," +
                "       jfs.num" +
                "  from (" + primarySql(map) + ") jfs" +
                " inner join jecn_flow_basic_info jfbi" +
                "    on jfs.flow_id = jfbi.flow_id" +
                "  left join jecn_flow_related_org jfsro" +
                "    on jfsro.flow_id = jfs.flow_id" +
                "  left join jecn_flow_org jfo" +
                "    on jfsro.org_id = jfo.org_id" +
                "  order by jfs.num desc,jfs.flow_id asc";
        return sql;
    }

    private String primarySql(Map<String, Object> map) {
        String condition = "";
        String fromSql = " select * from jecn_flow_structure where isflow=1 ";
        PrimaryActivitySearchParam param = (PrimaryActivitySearchParam) map.get("param");
        if (param != null) {
            if (param.getMapIds() != null && param.getMapIds().size() > 0) {
                Long mapId = param.getMapIds().get(0);
                fromSql = " select c.*" +
                        "  from jecn_flow_structure c" +
                        " inner join jecn_flow_structure p" +
                        "    on c.del_state = 0" +
                        "   and p.del_state = 0" +
                        "   and c.t_path like p.t_path" + BaseSqlProvider.getConcatChar() + "'%' " +
                        "   and p.flow_id=" + mapId +
                        "   where c.isflow=1";
            }
            if (StringUtils.isNotBlank(param.getOrgName())) {
                condition += " and jfo.org_name like '%" + param.getOrgName() + "%'";
            }
            // 业务类型
            if (param.getBussTypes() != null && param.getBussTypes().size() > 0) {
                condition += " and jfs.buss_type in " + param.getBussTypes().stream().collect(Collectors.joining(",", "(", ")"));
            }
            // 活动名称
            if (StringUtils.isNotBlank(param.getFigureText())) {
                condition += " and jfsi.figure_text like '%" + param.getFigureText() + "%'";
            }
            if (StringUtils.isNotBlank(param.getProcessName())) {
                condition += " and jfs.flow_name like '%" + param.getProcessName() + "%'";
            }
        }

        String sql = " select jfs.flow_id, jfs.flow_name, count(*) as num,jfs.buss_type" +
                "          from (" + fromSql + ") jfs" +
                "         inner join jecn_flow_structure_image jfsi" +
                "            on jfs.flow_id = jfsi.flow_id" +
                "         left join jecn_flow_related_org jfro on jfro.flow_id=jfs.flow_id" +
                "         left join jecn_flow_org jfo on jfro.org_id=jfo.org_id" +
                "         where jfs.del_state = 0 " + condition +
                "         and jfsi.figure_type in " + SqlCommon.getActiveString() +
                "         and jfsi.linecolor='4' " +
                "         group by jfs.flow_id, jfs.flow_name,jfs.buss_type";
        return sql;
    }

    public String primaryManageStatistic() {
        String sql = "select (select count(*)" +
                "          from (select distinct jfsi.figure_id" +
                "                  from jecn_flow_structure jfs" +
                "                 inner join jecn_flow_structure_image jfsi" +
                "                    on jfs.flow_id = jfsi.flow_id" +
                "                 where jfs.del_state = 0 " +
                "         and jfsi.figure_type in " + SqlCommon.getActiveString() +
                "         and jfsi.linecolor='4' " +
                ") t) as ELE_COUNT," +
                "       (select count(*)" +
                "          from (select distinct jfsi.flow_id" +
                "                  from jecn_flow_structure jfs" +
                "                 inner join jecn_flow_structure_image jfsi" +
                "                    on jfs.flow_id = jfsi.flow_id" +
                "                 where jfs.del_state = 0 " +
                "         and jfsi.figure_type in " + SqlCommon.getActiveString() +
                "         and jfsi.linecolor='4' " +
                ") t) as FLOW_COUNT," +
                "       (select count(*)" +
                "          from (select distinct jfo.org_id" +
                "                  from jecn_flow_structure jfs" +
                "                 inner join jecn_flow_structure_image jfsi" +
                "                    on jfs.flow_id = jfsi.flow_id" +
                "                 inner join jecn_flow_related_org jfro" +
                "                    on jfro.flow_id = jfs.flow_id" +
                "                 inner join jecn_flow_org jfo" +
                "                    on jfo.org_id = jfro.org_id" +
                "                 where jfs.del_state = 0 " +
                "         and jfsi.figure_type in " + SqlCommon.getActiveString() +
                "         and jfsi.linecolor='4' " +
                ") t) as ORG_COUNT";
        if (BaseSqlProvider.isOracle()) {
            sql += " from dual";
        }
        return sql;
    }

    public String reportPermissionsReports(Map<String, Object> map) {
        List<Long> ids = (List<Long>) map.get("ids");
        String orgId = map.get("orgId").toString();
        String type = map.get("type").toString();
        String sql = "";
        String idstr = (ids == null || ids.isEmpty()) ? "" : SqlCommon.getIds(ids);
        switch (type) {
            case "FLOW":
                sql = getFlowAuthReportSql(idstr, orgId);
                break;
            case "RULE":
                sql = getRuleAuthReportSql(idstr, orgId);
                break;
            case "FILE":
                sql = getFileAuthReportSql(idstr, orgId);
                break;
            default:
                break;
        }
        return sql;
    }


    public String getRuleAuthReportSql(String ids, String orgId) {
        String sql = "SELECT T.ID," +
                "      T.RULE_NAME NAME," +
                "      RES_ORG.ORG_ID," +
                "      RES_ORG.ORG_NAME," +
                "      ORG_VIEW.Org_Id AUTH_ORG_ID," +
                "      ORG_VIEW.ORG_NAME AUTH_ORG_NAME," +
                "      ORG.ACCESS_TYPE      AUTH_ORG_ACCESS_TYPE," +
                "      POSION.NAME          AUTH_GROUP_NAME," +
                "      POSG.ACCESS_TYPE     AUTH_GROUP_ACCESS_TYPE," +
                "      POS_VIEW.FIGURE_ID   AUTH_POS_ID," +
                "      POS_VIEW.FIGURE_TEXT AUTH_POS_NAME," +
                "      POS.ACCESS_TYPE      AUTH_POS_ACCESS_TYPE" +
                "  FROM JECN_RULE PT" +
                "  LEFT JOIN JECN_RULE T" +
                CommonSqlTPath.INSTANCE.getSubStringSqlByDBType("T", "PT") +
                "  LEFT JOIN JECN_FLOW_ORG RES_ORG" +
                "    ON T.ORG_ID = RES_ORG.ORG_ID" +
                "   AND RES_ORG.DEL_STATE = 0" +
                "  LEFT JOIN JECN_ORG_ACCESS_PERMISSIONS ORG" +
                "    ON T.ID = ORG.RELATE_ID" +
                "   AND ORG.TYPE = 3" +
                "  LEFT JOIN JECN_FLOW_ORG ORG_VIEW" +
                "    ON ORG_VIEW.ORG_ID = ORG.ORG_ID" +
                "   AND ORG_VIEW.DEL_STATE = 0" +
                "  LEFT JOIN JECN_GROUP_PERMISSIONS POSG" +
                "    ON T.ID = POSG.RELATE_ID" +
                "   AND POSG.TYPE = 3" +
                "  LEFT JOIN JECN_POSITION_GROUP POSION" +
                "    ON POSG.POSTGROUP_ID = POSION.ID" +
                "  LEFT JOIN JECN_ACCESS_PERMISSIONS POS" +
                "    ON T.ID = POS.RELATE_ID" +
                "   AND POS.TYPE = 3" +
                "  LEFT JOIN JECN_FLOW_ORG_IMAGE POS_VIEW" +
                "    ON POS.FIGURE_ID = POS_VIEW.FIGURE_ID" +
                " WHERE T.DEL_STATE = 0 AND T.IS_DIR <> 0";
        if (StringUtils.isNotBlank(ids)) {
            sql += " and PT.ID IN" + ids;
        }
        if (!"-1".equals(orgId)) {
            sql += "  and RES_ORG.ORG_ID = " + orgId;
        }
        sql += "  ORDER BY T.VIEW_SORT";
        return sql;
    }

    public String getFlowAuthReportSql(String ids, String orgId) {
        String sql = "SELECT T.FLOW_ID ID," +
                "       T.FLOW_NAME          NAME," +
                "       RES_ORG.ORG_ID," +
                "       RES_ORG.ORG_NAME," +
                "       ORG_VIEW.Org_Id      AUTH_ORG_ID," +
                "       ORG_VIEW.ORG_NAME    AUTH_ORG_NAME," +
                "       ORG.ACCESS_TYPE      AUTH_ORG_ACCESS_TYPE," +
                "       POSION.NAME          AUTH_GROUP_NAME," +
                "       POSG.ACCESS_TYPE     AUTH_GROUP_ACCESS_TYPE," +
                "       POS_VIEW.FIGURE_ID   AUTH_POS_ID," +
                "       POS_VIEW.FIGURE_TEXT AUTH_POS_NAME," +
                "       POS.ACCESS_TYPE      AUTH_POS_ACCESS_TYPE" +
                "  FROM JECN_FLOW_STRUCTURE PT" +
                "  LEFT JOIN JECN_FLOW_STRUCTURE T" +
                CommonSqlTPath.INSTANCE.getSubStringSqlByDBType("T", "PT") +
                "  LEFT JOIN JECN_FLOW_RELATED_ORG_T FLOW_ORG" +
                "    ON T.FLOW_ID = FLOW_ORG.FLOW_ID" +
                "  LEFT JOIN JECN_FLOW_ORG RES_ORG" +
                "    ON FLOW_ORG.ORG_ID = RES_ORG.ORG_ID" +
                "   AND RES_ORG.DEL_STATE = 0" +
                "  LEFT JOIN JECN_ORG_ACCESS_PERMISSIONS ORG" +
                "    ON T.FLOW_ID = ORG.RELATE_ID" +
                "   AND ORG.TYPE = 0" +
                "  LEFT JOIN JECN_FLOW_ORG ORG_VIEW" +
                "    ON ORG_VIEW.ORG_ID = ORG.ORG_ID" +
                "   AND ORG_VIEW.DEL_STATE = 0" +
                "  LEFT JOIN JECN_GROUP_PERMISSIONS POSG" +
                "    ON T.FLOW_ID = POSG.RELATE_ID" +
                "   AND POSG.TYPE = 0" +
                "  LEFT JOIN JECN_POSITION_GROUP POSION" +
                "    ON POSG.POSTGROUP_ID = POSION.ID" +
                "  LEFT JOIN JECN_ACCESS_PERMISSIONS POS" +
                "    ON T.FLOW_ID = POS.RELATE_ID" +
                "   AND POS.TYPE = 0" +
                "  LEFT JOIN JECN_FLOW_ORG_IMAGE POS_VIEW" +
                "    ON POS.FIGURE_ID = POS_VIEW.FIGURE_ID" +
                " WHERE T.DEL_STATE = 0 ";
        if (StringUtils.isNotBlank(ids)) {
            sql += " and PT.FLOW_ID IN" + ids;
        }
        if (!"-1".equals(orgId)) {
            sql += "  and RES_ORG.ORG_ID = " + orgId;
        }
        sql += "  ORDER BY T.VIEW_SORT";
        return sql;
    }

    public String getFileAuthReportSql(String ids, String orgId) {
        String sql = "SELECT T.FILE_ID ID," +
                "       T.FILE_NAME          NAME," +
                "       RES_ORG.ORG_ID," +
                "       RES_ORG.ORG_NAME," +
                "       ORG_VIEW.Org_Id      AUTH_ORG_ID," +
                "       ORG_VIEW.ORG_NAME    AUTH_ORG_NAME," +
                "       ORG.ACCESS_TYPE      AUTH_ORG_ACCESS_TYPE," +
                "       POSION.NAME          AUTH_GROUP_NAME," +
                "       POSG.ACCESS_TYPE     AUTH_GROUP_ACCESS_TYPE," +
                "       POS_VIEW.FIGURE_ID   AUTH_POS_ID," +
                "       POS_VIEW.FIGURE_TEXT AUTH_POS_NAME," +
                "       POS.ACCESS_TYPE      AUTH_POS_ACCESS_TYPE" +
                "  FROM JECN_FILE PT" +
                "  LEFT JOIN JECN_FILE T" +
                CommonSqlTPath.INSTANCE.getSubStringSqlByDBType("T", "PT") +
                "  LEFT JOIN JECN_FLOW_ORG RES_ORG" +
                "    ON T.ORG_ID = RES_ORG.ORG_ID" +
                "   AND RES_ORG.DEL_STATE = 0" +
                "  LEFT JOIN JECN_ORG_ACCESS_PERMISSIONS ORG" +
                "    ON T.FILE_ID = ORG.RELATE_ID" +
                "   AND ORG.TYPE = 1" +
                "  LEFT JOIN JECN_FLOW_ORG ORG_VIEW" +
                "    ON ORG_VIEW.ORG_ID = ORG.ORG_ID" +
                "   AND ORG_VIEW.DEL_STATE = 0" +
                "  LEFT JOIN JECN_GROUP_PERMISSIONS POSG" +
                "    ON T.FILE_ID = POSG.RELATE_ID" +
                "   AND POSG.TYPE = 1" +
                "  LEFT JOIN JECN_POSITION_GROUP POSION" +
                "    ON POSG.POSTGROUP_ID = POSION.ID" +
                "  LEFT JOIN JECN_ACCESS_PERMISSIONS POS" +
                "    ON T.FILE_ID = POS.RELATE_ID" +
                "   AND POS.TYPE = 1" +
                "  LEFT JOIN JECN_FLOW_ORG_IMAGE POS_VIEW" +
                "    ON POS.FIGURE_ID = POS_VIEW.FIGURE_ID" +
                " WHERE T.DEL_STATE = 0 AND T.IS_DIR <> 0";
        if (StringUtils.isNotBlank(ids)) {
            sql += " and PT.FILE_ID IN" + ids;
        }
        if (!"-1".equals(orgId)) {
            sql += "  and RES_ORG.ORG_ID = " + orgId;
        }
        sql += "  ORDER BY T.VIEW_SORT";
        return sql;
    }
}
