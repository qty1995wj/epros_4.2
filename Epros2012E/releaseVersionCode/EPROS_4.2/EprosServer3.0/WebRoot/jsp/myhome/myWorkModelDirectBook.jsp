<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>


<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	</head>

	<body>
		<div id="div" style="height: 100%;">
			<div id="search" class="divBorder" align="center">
				<div id="myWorkModelDirectBookSearchText" style="overflow: hidden;"
					class="minWidth">
					<table width="100%">
						<tr>
							<td height="25px" width="12%" align="right">
								<!-- 我参与的流程： -->
								<s:text name="myJoinProcessC"></s:text>
							</td>
							<td width="33%" align="left">
								<select style="width: 80%; margin-right: 20px;"
									id="myJoinProcessSelect_id"
									onchange="myJoinProcessSelectOnchange()"
									onfocus="myJoinProcessSelect()">
									<option value="-1">
										<s:text name="all" />
									</option>
								</select>
							</td>


							<td width="12%" align="right">
								<!--活动名称： -->
								<s:text name="activityTitleC"></s:text>
							</td>
							<td width="33%" align="left">
								<select style="width: 80%; margin-right: 20px;"
									id="activitySelect_id">
									<option value="-1">
										<s:text name="all" />
									</option>
								</select>
							</td>

							<td style="vertical-align: middle;" align="center">
								<a onClick="toggle(myWorkModelDirectBookSearchText)"
									style="cursor: pointer;" class="packUp"> <img id="packUp"
										src="${basePath}images/iParticipateInProcess/on.gif"
										align="middle" /> <!-- 收起 --> <span id="searchVal"><s:text
											name="packUp" /> </span> </a>
							</td>
						</tr>


						<tr>
							<td height="25px" align="right">
								<!--文件名称: -->
								<s:text name="fileNameC" />
							</td>
							<td width="35%" align="left">
								<input type="text" style="width: 80%" id="myWorkFileName_id"
									maxLength="32" />
							</td>

							<td></td>
							<td></td>

							<td></td>
						</tr>
					</table>
				</div>

				<div id="searchBut" align="center">
					<img style="cursor: pointer;"
						src="${basePath}images/iParticipateInProcess/search.gif"
						onclick="myWorkModelSearch()" />
					<img style="cursor: pointer;"
						src="${basePath}images/iParticipateInProcess/reset.gif"
						onclick="myWorkModelReset()" />
				</div>
			</div>

			<div id="myWorkModelDirectBookGrid_id">

			</div>
		</div>
	</body>
</html>