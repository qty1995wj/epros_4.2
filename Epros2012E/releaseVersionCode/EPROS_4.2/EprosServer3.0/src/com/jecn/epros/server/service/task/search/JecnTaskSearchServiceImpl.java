package com.jecn.epros.server.service.task.search;

import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Vector;

import jxl.Workbook;
import jxl.format.Alignment;
import jxl.format.Border;
import jxl.format.BorderLineStyle;
import jxl.format.Colour;
import jxl.format.UnderlineStyle;
import jxl.format.VerticalAlignment;
import jxl.write.Label;
import jxl.write.NumberFormats;
import jxl.write.WritableCellFormat;
import jxl.write.WritableFont;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
import jxl.write.WriteException;
import jxl.write.biff.RowsExceededException;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.transaction.annotation.Transactional;

import com.jecn.epros.server.bean.popedom.JecnUser;
import com.jecn.epros.server.bean.task.JecnTaskBeanNew;
import com.jecn.epros.server.common.AbsBaseService;
import com.jecn.epros.server.common.JecnCommon;
import com.jecn.epros.server.common.JecnCommonSql;
import com.jecn.epros.server.common.JecnConfigTool;
import com.jecn.epros.server.common.JecnContants;
import com.jecn.epros.server.common.JecnFinal;
import com.jecn.epros.server.common.JecnConfigContents.ConfigItemPartMapMark;
import com.jecn.epros.server.connector.SinglePointBean;
import com.jecn.epros.server.dao.popedom.IJecnRoleDao;
import com.jecn.epros.server.dao.system.IJecnConfigItemDao;
import com.jecn.epros.server.dao.task.search.IJecnTaskSearchDao;
import com.jecn.epros.server.service.task.buss.JecnTaskCommon;
import com.jecn.epros.server.util.JecnUtil;
import com.jecn.epros.server.webBean.task.MyTaskBean;
import com.jecn.epros.server.webBean.task.TaskSearchTempBean;

/**
 * 我的任务，任务计划 搜索、删除、显示记录 service实现类
 * 
 * @author ZHANGXIAOHU
 * @date： 日期：Nov 23, 2012 时间：10:13:23 AM
 */
public class JecnTaskSearchServiceImpl extends AbsBaseService<JecnTaskBeanNew, Long> implements IJecnTaskSearchService {
	/** 任务查询，。删除，显示全部记录DAO接口 */
	private IJecnTaskSearchDao taskSeachDao;
	/** 配置信息表（JECN_CONFIG_ITEM）操作类 */
	protected IJecnConfigItemDao configItemDao;

	protected IJecnRoleDao jecnRoleDao;
	private static final Logger LOGGER = LoggerFactory.getLogger(JecnTaskSearchServiceImpl.class);

	public IJecnConfigItemDao getConfigItemDao() {
		return configItemDao;
	}

	public void setConfigItemDao(IJecnConfigItemDao configItemDao) {
		this.configItemDao = configItemDao;
	}

	/**
	 * 我的任务获取登录人相关任务
	 * 
	 * @return List<MyTaskBean>登录人相关任务集合
	 * @param loginPeopleId
	 *            Long 登录
	 * @param start
	 *            每页开始行
	 * @param limit
	 *            每页最大行数
	 * @throws Exception
	 */
	@Override
	public List<MyTaskBean> getMyTaskBySearch(TaskSearchTempBean searchTempBean, Long loginPeopleId, int start,
			int limit) throws Exception {
		if (loginPeopleId == null) {
			throw new IllegalArgumentException("JecnTaskSeachServiceImpl中方法getMyTaskBySearch获取TaskSearchTempBean信息异常！");
		}

		String sql = "select " + " ALL_TASK.ID TASK_ID," + " ALL_TASK.TASK_NAME,"
				+ " ALL_TASK.STATE,"
				+ " ALL_TASK.TASK_ELSE_STATE,"
				+ " ALL_TASK.TASK_TYPE,"
				+ " ALL_TASK.TRUE_NAME,"
				+ " ALL_TASK.CREATE_PERSON_ID,"
				+ " ALL_TASK.UPDATE_TIME,"
				+ " ALL_TASK.START_TIME,"
				+ " ALL_TASK.END_TIME,"
				+ " ALL_TASK.FROM_PERSON_ID,"
				+ " ALL_TASK.UP_STATE,"
				+ "STAGE_NAME"
				+ " from (" // 下边的是主要的语句将所有数据作为子查询数据
				+ "         SELECT A.ID," + "                A.TASK_NAME," + "                A.STATE,"
				+ "                A.TASK_ELSE_STATE," + "                A.TASK_TYPE,"
				+ "                C.TRUE_NAME," + "                A.CREATE_PERSON_ID,"
				+ "                A.UPDATE_TIME," + "                A.start_time," + "                A.end_time,"
				+ "                A.FROM_PERSON_ID," + "                A.UP_STATE," + "				   JTS.STAGE_NAME,"
				+ "				   A.R_ID " + "           FROM JECN_TASK_BEAN_NEW A"
				+ "			 INNER JOIN JECN_TASK_STAGE JTS ON A.ID=JTS.TASK_ID AND A.STATE=JTS.STAGE_MARK"
				+ "          INNER JOIN JECN_TASK_PEOPLE_NEW B ON A.ID = B.TASK_ID"
				+ "                                           AND B.APPROVE_PID = "
				+ loginPeopleId
				+ "          INNER JOIN JECN_USER C ON A.CREATE_PERSON_ID = C.PEOPLE_ID"
				+ "          WHERE  A.IS_LOCK = 1"
				+ "            AND A.STATE <> 5"
				+ "         UNION"
				+ "         SELECT A.ID,"
				+ "                A.TASK_NAME,"
				+ "                A.STATE,"
				+ "                A.TASK_ELSE_STATE,"
				+ "                A.TASK_TYPE,"
				+ "                C.TRUE_NAME,"
				+ "                A.CREATE_PERSON_ID,"
				+ "                A.UPDATE_TIME,"
				+ "                A.start_time,"
				+ "                A.end_time,"
				+ "                A.FROM_PERSON_ID,"
				+ "                A.UP_STATE,"
				+ "				   JTS.STAGE_NAME,"
				+ "				   A.R_ID "
				+ "           FROM JECN_TASK_BEAN_NEW A"
				+ "			 INNER JOIN JECN_TASK_STAGE JTS ON A.ID=JTS.TASK_ID AND A.STATE=JTS.STAGE_MARK"
				+ "          INNER JOIN JECN_USER C ON A.CREATE_PERSON_ID = "
				+ loginPeopleId
				+ "                                AND C.PEOPLE_ID = A.CREATE_PERSON_ID"
				+ "          WHERE A.IS_LOCK = 1" + "            AND A.STATE <> 5";

		// 根据任务获取流程责任人类型，责任人ID
		sql = sql + ") ALL_TASK";

		List<Object[]> objectList = null;
		if (searchTempBean == null) {// 默认查询所有
			// sql = sql + "order by TMP_ID DESC";
			// Object[]
			LOGGER.info(" 查詢所有！");
			sql = sql + " order by ALL_TASK.update_time desc";
			// 0:任务主键ID，1：任务名称，2：任务阶段，3：任务操作状态，4：任务类型，5：创建人真实姓名，6:创建人主键ID
			objectList = taskSeachDao.listNativeSql(sql, start, limit);
		} else {
			// 拼装查询条件
			List<Object> listArgs = new ArrayList<Object>();
			// listArgs.add(loginPeopleId);
			// 拼装查询条件
			String searchSql = getSqlBytempBean(searchTempBean, sql, listArgs);
			sql = searchSql + " order by a.UPDATE_TIME DESC";
			// 分页查询满足一定条件的数据集
			objectList = taskSeachDao.listNativeSql(sql, start, limit, listArgs.toArray());
		}
		// 根据object数组获取MyTaskBean集合
		return getMyTaskBeanList(objectList, loginPeopleId, 0);
	}

	@Override
	public List<MyTaskBean> getToDotasksBean(Long loginPeopleId, int start, int limit) {
		String sql = "SELECT A.ID," + "       A.TASK_NAME," + "       A.STATE," + "       A.TASK_ELSE_STATE,"
				+ "       A.TASK_TYPE," + "       C.TRUE_NAME," + "       A.CREATE_PERSON_ID,"
				+ "       A.UPDATE_TIME," + "       A.start_time," + "       A.end_time," + "       A.FROM_PERSON_ID,"
				+ "       A.UP_STATE," + "       JTS.STAGE_NAME," + "       A.R_ID,A.CREATE_TIME"
				+ "  FROM JECN_TASK_BEAN_NEW A" + " INNER JOIN JECN_TASK_STAGE JTS" + "    ON A.ID = JTS.TASK_ID"
				+ "   AND A.STATE = JTS.STAGE_MARK" + " INNER JOIN JECN_TASK_PEOPLE_NEW B" + "    ON A.ID = B.TASK_ID"
				+ "   AND B.APPROVE_PID = " + loginPeopleId + " INNER JOIN JECN_USER C"
				+ "    ON A.CREATE_PERSON_ID = C.PEOPLE_ID" + " WHERE A.IS_LOCK = 1" + "   AND A.STATE <> 5";
		List<Object[]> objectList = null;
		if (limit == -1) {// 获取所有
			objectList = this.taskSeachDao.listNativeSql(sql);
		} else {
			objectList = this.taskSeachDao.listNativeSql(sql, start, limit);
		}
		return getMyToDotasksList(objectList, loginPeopleId, 0);
	}

	private List<MyTaskBean> getMyToDotasksList(List<Object[]> objectList, Long loginPeopleId, int type) {
		if (objectList == null || loginPeopleId == null) {
			throw new NullPointerException("getMyTaskBeanList获取任务记录异常！objectList == null || loginPeopleId == null");
		}

		// 获取当前审批人集合
		String hql = "SELECT T.taskId FROM JecnTaskPeopleNew T WHERE T.approvePid = " + loginPeopleId;
		// 获取当前审批人ID集合
		List<Long> listTaskIds = taskSeachDao.listHql(hql);
		// 记录登录人相关任务信息
		List<MyTaskBean> list = new ArrayList<MyTaskBean>();
		// 是否存在撤回
		boolean isAppMesTrue = JecnTaskCommon.isTrueMark(ConfigItemPartMapMark.taskOperationReCall.toString(),
				JecnTaskCommon.findBackItemBean(5, configItemDao));
		for (Object[] objects : objectList) {
			// 我的任务信息
			MyTaskBean myTaskBean = new MyTaskBean();
			Long taskId = JecnTaskCommon.getlongByObject(objects[0]);
			// 0任务主键ID
			myTaskBean.setTaskId(taskId);
			// 1任务名称
			myTaskBean.setTaskName(objects[1].toString());
			// 2任务阶段
			myTaskBean.setTaskStage(JecnTaskCommon.getIntByObject(objects[2]));

			// 操作状态
			int taskEleType = JecnTaskCommon.getIntByObject(objects[3]);
			// 3任务 拟稿人操作状态 0：拟稿人提交审批 4：打回
			// 5：提交意见（提交审批意见(评审-------->拟稿人) 6：二次评审
			// 拟稿人------>评审 7：拟稿人重新提交审批 8：完成 9:打回整理意见(批准------>拟稿人)）10：交办人提交
			if (type == 0) {// 我的任务
				if ((myTaskBean.getTaskStage() == 0 || myTaskBean.getTaskStage() == 10)
						&& JecnTaskCommon.isSameStr(loginPeopleId, objects[6])) {// 整理意见阶段或你搞阶段(拟稿人是当前阶段审批人)
					if (taskEleType == -1) {
						continue;
					}
					if (taskEleType == 4) {// 打回 (被打回，拟稿人不应该有撤回操作)
						// 0是驳回重新提交状态
						myTaskBean.setTaskElseState(0);
					} else {
						// 1是评审意见整理状态
						myTaskBean.setTaskElseState(1);
					}
				} else if (JecnTaskCommon.isSameStr(loginPeopleId, objects[6])) {// 当前登录人为拟稿人，并且在目标人集合中不存在，不是当前阶段审批人
					// 拟稿人无审批状态
					if (!isAppTask(listTaskIds, objects[0].toString())) {//
						// 登录人为拟稿人，3:拟稿人无审批状态
						myTaskBean.setTaskElseState(3);
					} else {// 拟稿人为当前阶段审批人
						myTaskBean.setTaskElseState(4);
					}
				}
			} else if (type == 2) {// 任务管理，登录人为管理员
				myTaskBean.setTaskElseState(2);

			}

			// 4任务类型
			myTaskBean.setTaskType(JecnTaskCommon.getIntByObject(objects[4]));
			// 当前为拟稿人操作
			if (isAppMesTrue && !isDraftCur(myTaskBean.getTaskElseState()) && objects.length > 11) {
				// 是否存在撤回状态
				if (JecnTaskCommon.isSameStr(loginPeopleId, objects[10])) {// true：撤回
					if (myTaskBean.getTaskStage() == 3 && taskEleType == 5) {// 评审人阶段
						if (!JecnTaskCommon.isSameStr(loginPeopleId, objects[6])) {// 拟稿人参与评审
							// 操作为5说明评审人已经审批过
							continue;
						}
					} else {
						// 只有拟稿人才会给3这个标识,拟稿人无审批功能一定有撤回功能 （上面的if条件已将评审结果略过）
						if (myTaskBean.getTaskElseState() == 3) {
							// 存在撤回状态
							myTaskBean.setReJect(1);

						} else {
							// 判断如果当前登录人员是当前任务的审批人(也就是登录人当前需要审批的任务集合listTaskIds包含taskId)那么他只有审批操作，不可能有撤回操作
							// true为可以撤回 false 为不可以撤回
							boolean canCancel = true;
							for (Long id : listTaskIds) {
								if (id.longValue() == taskId) {
									canCancel = false;
								}

							}
							// 可以撤回
							if (canCancel) { // 撤回标示 1为可以撤回
								myTaskBean.setReJect(1);
							}
							// 打回整理 taskEleType为上个阶段的操作
							if (taskEleType == 9) {
								// 如果操作是9说明是打回整理 0是打回重新提交状态,1是评审意见整理状态,2为系统管理员;
								// 3:拟稿人无审批状态;4:拟稿人为当前阶段审批人5：其他 （目前存在打回整理状态、和打回）
								myTaskBean.setTaskElseState(5);
							}
							// 打回
							if (taskEleType == 4) {
								myTaskBean.setTaskElseState(5);
							}
						}
					}

				}
			}

			// 5创建人真实姓名
			if (objects[5] != null) {
				myTaskBean.setCreateName(objects[5].toString());
			}

			if (objects[7] != null) {
				myTaskBean.setUpdateTime(objects[7].toString());
			}
			if (objects[8] != null) {
				myTaskBean.setStartTime(JecnUtil.valueToDateStr(objects[8]));
			}
			if (objects[9] != null) {
				myTaskBean.setEndTime(JecnUtil.valueToDateStr(objects[9]));
			}
			if (objects[12] != null) {
				myTaskBean.setStageName(objects[12].toString());
			}
			if (objects[14] != null) {
				myTaskBean.setCreateTime(JecnUtil.valueToDateStr(objects[14]));
			}
			list.add(myTaskBean);
		}
		return list;
	}

	/**
	 * 之获取任务待办总数
	 * 
	 * @param loginPeopleId
	 * @param start
	 * @param limit
	 * @return
	 */
	@Override
	public int getToDotasksCount(Long loginPeopleId) {
		String sql = "SELECT COUNT(A.ID)" + "  FROM JECN_TASK_BEAN_NEW A" + " INNER JOIN JECN_TASK_STAGE JTS"
				+ "    ON A.ID = JTS.TASK_ID" + "   AND A.STATE = JTS.STAGE_MARK"
				+ " INNER JOIN JECN_TASK_PEOPLE_NEW B" + "    ON A.ID = B.TASK_ID" + "   AND B.APPROVE_PID = "
				+ loginPeopleId + " INNER JOIN JECN_USER C" + "    ON A.CREATE_PERSON_ID = C.PEOPLE_ID"
				+ " WHERE  A.IS_LOCK = 1" + "   AND A.STATE <> 5";
		return this.taskSeachDao.countAllByParamsNativeSql(sql);
	}

	/**
	 * 我的任务获取登录人相关任务
	 * 
	 * @return List<MyTaskBean>登录人相关任务集合
	 * @param loginPeopleId
	 *            Long 登录
	 * @param start
	 *            每页开始行
	 * @param limit
	 *            每页最大行数
	 * @throws Exception
	 */
	@Override
	public List<MyTaskBean> getMyTaskNoDraftStateBySearch(TaskSearchTempBean searchTempBean, Long loginPeopleId,
			int start, int limit) throws Exception {
		if (loginPeopleId == null) {
			throw new IllegalArgumentException("JecnTaskSeachServiceImpl中方法getMyTaskBySearch获取TaskSearchTempBean信息异常！");
		}

		String sql = "select " + " ALL_TASK.ID TASK_ID," + " ALL_TASK.TASK_NAME," + " ALL_TASK.STATE,"
				+ " ALL_TASK.TASK_ELSE_STATE," + " ALL_TASK.TASK_TYPE," + " ALL_TASK.TRUE_NAME,"
				+ " ALL_TASK.CREATE_PERSON_ID," + " ALL_TASK.UPDATE_TIME," + " ALL_TASK.START_TIME,"
				+ " ALL_TASK.END_TIME," + " ALL_TASK.FROM_PERSON_ID," + " ALL_TASK.UP_STATE," + "STAGE_NAME,"
				+ getCaseResPeople()
				+ ",null publish_date"
				+ " from (" // 下边的是主要的语句将所有数据作为子查询数据
				+ "         SELECT A.ID,"
				+ "                A.TASK_NAME,"
				+ "                A.STATE,"
				+ "                A.TASK_ELSE_STATE,"
				+ "                A.TASK_TYPE,"
				+ "                C.TRUE_NAME,"
				+ "                A.CREATE_PERSON_ID,"
				+ "                A.UPDATE_TIME,"
				+ "                A.start_time,"
				+ "                A.end_time,"
				+ "                A.FROM_PERSON_ID,"
				+ "                A.UP_STATE,"
				+ "				   JTS.STAGE_NAME,"
				+ "				   A.R_ID "
				+ "           FROM JECN_TASK_BEAN_NEW A"
				+ "			 INNER JOIN JECN_TASK_STAGE JTS ON A.ID=JTS.TASK_ID AND A.STATE=JTS.STAGE_MARK"
				+ "          INNER JOIN JECN_TASK_PEOPLE_NEW B ON A.ID = B.TASK_ID"
				+ "                                           AND B.APPROVE_PID = "
				+ loginPeopleId
				+ "          INNER JOIN JECN_USER C ON A.CREATE_PERSON_ID = C.PEOPLE_ID"
				+ "          WHERE EXISTS (SELECT 1"
				+ "                   FROM JECN_USER_POSITION_RELATED U_P"
				+ "                  WHERE U_P.PEOPLE_ID = C.PEOPLE_ID)"
				+ "            AND A.IS_LOCK = 1"
				+ "            AND A.STATE <> 5";

		// 根据任务获取流程责任人类型，责任人ID
		sql = sql + ") ALL_TASK" + addResPeopleType();

		// 根据任务结果获取任务相关信息，责任人类型获取责任人
		sql = getTaskResPeoleSql(sql);

		List<Object[]> objectList = null;
		if (searchTempBean == null) {// 默认查询所有
			// sql = sql + "order by TMP_ID DESC";
			// Object[]
			sql = sql + "order by a.update_time desc";
			// 0:任务主键ID，1：任务名称，2：任务阶段，3：任务操作状态，4：任务类型，5：创建人真实姓名，6:创建人主键ID
			objectList = taskSeachDao.listNativeSql(sql, start, limit);
		} else {
			// 拼装查询条件
			List<Object> listArgs = new ArrayList<Object>();
			// listArgs.add(loginPeopleId);
			// 拼装查询条件
			String searchSql = getSqlBytempBean(searchTempBean, sql, listArgs);
			sql = searchSql + " order by a.UPDATE_TIME DESC";
			// 分页查询满足一定条件的数据集
			objectList = taskSeachDao.listNativeSql(sql, start, limit, listArgs.toArray());
		}
		// 根据object数组获取MyTaskBean集合
		return getMyTaskBeanList(objectList, loginPeopleId, 0);
	}

	/**
	 * 存在撤回操作，获取源人
	 * 
	 * @param loginPeopleId
	 * @return
	 */
	private String reBackString(long loginPeopleId) {
		return "         UNION" + "         SELECT A.ID,"
				+ "                A.TASK_NAME,"
				+ "                A.STATE,"
				+ "                A.TASK_ELSE_STATE,"
				+ "                A.TASK_TYPE,"
				+ "                C.TRUE_NAME,"
				+ "                A.CREATE_PERSON_ID,"
				+ "                A.UPDATE_TIME,"
				+ "                A.start_time,"
				+ "                A.end_time,"
				+ "                A.FROM_PERSON_ID,"
				+ "                A.UP_STATE,"
				+ "				   JTS.STAGE_NAME,"
				+ "				   A.R_ID "
				+ "           FROM JECN_TASK_BEAN_NEW A"
				+ "			 INNER JOIN JECN_TASK_STAGE JTS ON A.ID=JTS.TASK_ID AND A.STATE=JTS.STAGE_MARK"
				+ "          INNER JOIN JECN_USER C ON A.FROM_PERSON_ID = "
				+ loginPeopleId
				+ "                                AND C.PEOPLE_ID = A.CREATE_PERSON_ID"
				+ "          WHERE EXISTS (SELECT 1"
				+ "                   FROM JECN_USER_POSITION_RELATED U_P"
				+ "                  WHERE U_P.PEOPLE_ID = C.PEOPLE_ID)"
				+ "            AND A.IS_LOCK = 1"
				+ "            AND A.STATE <> 5 AND (A.STATE <> 10 or A.TASK_ELSE_STATE=9)"// 整理意见阶段的操作人只能为创建人
				// 审批记录MAX值唯一， 用in 子查询太慢，写的MAX
				+ "            AND A.FROM_PERSON_ID ="
				+ "                               (SELECT MAX(FR.FROM_PERSON_ID)"
				+ "                                  FROM JECN_TASK_FOR_RECODE_NEW FR"
				+ "                                 WHERE FR.UP_STATE < 10 and FR.UP_STATE<>3 AND FR.SORT_ID ="
				+ "                                       (SELECT MAX(TR.SORT_ID)"
				+ "                                          FROM JECN_TASK_FOR_RECODE_NEW TR"
				+ "                                         WHERE TR.TASK_ID = A.ID) AND FR.TASK_ID = A.ID)";
	}

	/**
	 * 拼装查询条件
	 * 
	 * @param searchTempBean
	 *            TaskSearchTempBean页面返回的查询信息
	 * @param sql
	 *            sql
	 * @param listArgs
	 *            查询值集合
	 * @return String
	 */
	public String getSqlBytempBean(TaskSearchTempBean searchTempBean, String sql, List<Object> listArgs) {
		if (searchTempBean == null || JecnCommon.isNullOrEmtryTrim(sql)) {
			return null;
		}
		if (searchTempBean != null) {
			if (searchTempBean.getTaskType() != -1) {// 任务类型
				if (searchTempBean.getTaskType() == 2) {// 制度任务查询
					sql = sql + " and  (A.TASK_TYPE = 2 or A.TASK_TYPE = 3)";
				} else {
					sql = sql + " and  A.TASK_TYPE = ?";
					listArgs.add(searchTempBean.getTaskType());
				}
			}
			if (searchTempBean.getTaskStage() != -1) {// 任务阶段
				sql = sql + " and  A.state = ?";
				listArgs.add(searchTempBean.getTaskStage());
			}
			if (!JecnCommon.isNullOrEmtryTrim(searchTempBean.getTaskName())) {// 任务名称模糊查询
				sql = sql + " and A.task_name like ?";
				listArgs.add("%" + searchTempBean.getTaskName() + "%");
			}
			if (searchTempBean.getCreatePeopleId() != null) {// 创建人模糊查询
				sql = sql + " and A.CREATE_PERSON_ID = ?";
				listArgs.add(searchTempBean.getCreatePeopleId());
			}
			if (JecnTaskCommon.isRightTime(searchTempBean.getStartTime(), searchTempBean.getEndTime())) {// 时间段查询

				// 字符串转换成时间拼接时、分、秒
				searchTempBean.setStartTime(searchTempBean.getStartTime() + " 00:00:00");
				searchTempBean.setEndTime(searchTempBean.getEndTime() + " 23:59:59");
				// 字符串转换日期类型 开始时间
				Date sDate = JecnCommon.getDateTimeByString(searchTempBean.getStartTime());
				// 字符串转换日期类型 结束时间
				Date eDate = JecnCommon.getDateTimeByString(searchTempBean.getEndTime());
				sql = sql + " and A.UPDATE_TIME>=?" + " and A.UPDATE_TIME<=?";
				// 添加到数组
				listArgs.add(sDate);
				listArgs.add(eDate);
			}
		}
		return sql;
	}

	/**
	 * 根据查询条件获取登录人相关任务
	 * 
	 * @param searchTempBean
	 *            TaskSearchTempBean页面返回的查询信息
	 * @param loginPeopleId
	 *            人员主键ID 登录人的
	 * @return int 根据查询条件返回的任务总数
	 * @throws Exception
	 */
	@Override
	public int getMyTaskCountBySearch(TaskSearchTempBean searchTempBean, Long loginPeopleId) throws Exception {

		String sql = "with TMP_TASK as (" // 下边的是主要的语句将所有数据作为子查询数据
				+ "         SELECT A.ID," + "                A.TASK_NAME,"
				+ "                A.STATE,"
				+ "                A.TASK_ELSE_STATE,"
				+ "                A.TASK_TYPE,"
				+ "                C.TRUE_NAME,"
				+ "                A.CREATE_PERSON_ID,"
				+ "                A.UPDATE_TIME,"
				+ "                A.start_time,"
				+ "                A.end_time,"
				+ "                A.FROM_PERSON_ID,"
				+ "                A.UP_STATE,"
				+ "				   JTS.STAGE_NAME,"
				+ "				   A.R_ID "
				+ "           FROM JECN_TASK_BEAN_NEW A"
				+ "			 INNER JOIN JECN_TASK_STAGE JTS ON A.ID=JTS.TASK_ID AND A.STATE=JTS.STAGE_MARK"
				+ "          INNER JOIN JECN_TASK_PEOPLE_NEW B ON A.ID = B.TASK_ID"
				+ "                                           AND B.APPROVE_PID = "
				+ loginPeopleId
				+ "          INNER JOIN JECN_USER C ON A.CREATE_PERSON_ID = C.PEOPLE_ID"
				+ "          WHERE  A.IS_LOCK = 1"
				+ "            AND A.STATE <> 5"
				+ "         UNION"
				+ "         SELECT A.ID,"
				+ "                A.TASK_NAME,"
				+ "                A.STATE,"
				+ "                A.TASK_ELSE_STATE,"
				+ "                A.TASK_TYPE,"
				+ "                C.TRUE_NAME,"
				+ "                A.CREATE_PERSON_ID,"
				+ "                A.UPDATE_TIME,"
				+ "                A.start_time,"
				+ "                A.end_time,"
				+ "                A.FROM_PERSON_ID,"
				+ "                A.UP_STATE,"
				+ "				   JTS.STAGE_NAME,"
				+ "				   A.R_ID "
				+ "           FROM JECN_TASK_BEAN_NEW A"
				+ "			 INNER JOIN JECN_TASK_STAGE JTS ON A.ID=JTS.TASK_ID AND A.STATE=JTS.STAGE_MARK"
				+ "          INNER JOIN JECN_USER C ON A.CREATE_PERSON_ID = "
				+ loginPeopleId
				+ "                                AND C.PEOPLE_ID = A.CREATE_PERSON_ID"
				+ "          WHERE A.IS_LOCK = 1" + "            AND A.STATE <> 5";

		sql = sql + ") select count(*) from TMP_TASK A where 1=1";

		if (searchTempBean == null) {// 不存在查询条件
			return taskSeachDao.countAllByParamsNativeSql(sql, loginPeopleId);
		} else {
			List<Object> listArgs = new ArrayList<Object>();
			// 拼装查询条件
			String searchSql = getSqlBytempBean(searchTempBean, sql, listArgs);

			// sql = searchSql + " order by a.UPDATE_TIME DESC";
			// 根据查询条件获取返回的总数
			return taskSeachDao.countAllByParamsNativeSql(searchSql, listArgs.toArray());
		}
	}

	/**
	 * 根据查询条件获取登录人相关任务
	 * 
	 * @param searchTempBean
	 *            TaskSearchTempBean页面返回的查询信息
	 * @param loginPeopleId
	 *            人员主键ID 登录人的
	 * @return int 根据查询条件返回的任务总数
	 * @throws Exception
	 */
	@Override
	public int getMyTaskCountNoDraftStateBySearch(TaskSearchTempBean searchTempBean, Long loginPeopleId)
			throws Exception {

		String sql = "with TMP_TASK as (" // 下边的是主要的语句将所有数据作为子查询数据
				+ "         SELECT A.ID,"
				+ "                A.TASK_NAME,"
				+ "                A.STATE,"
				+ "                A.TASK_ELSE_STATE,"
				+ "                A.TASK_TYPE,"
				+ "                C.TRUE_NAME,"
				+ "                A.CREATE_PERSON_ID,"
				+ "                A.UPDATE_TIME,"
				+ "                A.start_time,"
				+ "                A.end_time,"
				+ "                A.FROM_PERSON_ID,"
				+ "                A.UP_STATE,"
				+ "				   JTS.STAGE_NAME,"
				+ "				   A.R_ID "
				+ "           FROM JECN_TASK_BEAN_NEW A"
				+ "			 INNER JOIN JECN_TASK_STAGE JTS ON A.ID=JTS.TASK_ID AND A.STATE=JTS.STAGE_MARK"
				+ "          INNER JOIN JECN_TASK_PEOPLE_NEW B ON A.ID = B.TASK_ID"
				+ "                                           AND B.APPROVE_PID = "
				+ loginPeopleId
				+ "          INNER JOIN JECN_USER C ON A.CREATE_PERSON_ID = C.PEOPLE_ID"
				+ "          WHERE EXISTS (SELECT 1"
				+ "                   FROM JECN_USER_POSITION_RELATED U_P"
				+ "                  WHERE U_P.PEOPLE_ID = C.PEOPLE_ID)"
				+ "            AND A.IS_LOCK = 1"
				+ "            AND A.STATE <> 5";
		sql = sql + ") select count(*) from TMP_TASK A where 1=1";

		if (searchTempBean == null) {// 不存在查询条件
			return taskSeachDao.countAllByParamsNativeSql(sql, loginPeopleId);
		} else {
			List<Object> listArgs = new ArrayList<Object>();
			// 拼装查询条件
			String searchSql = getSqlBytempBean(searchTempBean, sql, listArgs);

			// sql = searchSql + " order by a.UPDATE_TIME DESC";
			// 根据查询条件获取返回的总数
			return taskSeachDao.countAllByParamsNativeSql(searchSql, listArgs.toArray());
		}
	}

	/**
	 * 根据查询条件获取登录人相关任务
	 * 
	 * @param searchTempBean
	 *            TaskSearchTempBean页面返回的查询信息
	 * @param loginPeopleId
	 *            人员主键ID 登录人的
	 * @return int 根据查询条件返回的任务总数
	 * @throws Exception
	 */
	@Override
	public int getApproveMyTaskCountBySearch(TaskSearchTempBean searchTempBean, Long loginPeopleId) throws Exception {

		String sql = "with TMP_TASK as (" // 下边的是主要的语句将所有数据作为子查询数据
				+ "         SELECT A.ID," + "                A.TASK_NAME,"
				+ "                A.STATE,"
				+ "                A.TASK_ELSE_STATE,"
				+ "                A.TASK_TYPE,"
				+ "                C.TRUE_NAME,"
				+ "                A.CREATE_PERSON_ID,"
				+ "                A.UPDATE_TIME,"
				+ "                A.start_time,"
				+ "                A.end_time,"
				+ "                A.FROM_PERSON_ID,"
				+ "                A.UP_STATE,"
				+ "				   JTS.STAGE_NAME,"
				+ "				   A.R_ID "
				+ "           FROM JECN_TASK_BEAN_NEW A"
				+ "			 INNER JOIN JECN_TASK_STAGE JTS ON A.ID=JTS.TASK_ID AND A.STATE=JTS.STAGE_MARK"
				+ "          INNER JOIN JECN_TASK_PEOPLE_NEW B ON A.ID = B.TASK_ID"
				+ "                                           AND B.APPROVE_PID = "
				+ loginPeopleId
				+ "          INNER JOIN JECN_USER C ON A.CREATE_PERSON_ID = C.PEOPLE_ID"
				+ "          WHERE EXISTS (SELECT 1"
				+ "                   FROM JECN_USER_POSITION_RELATED U_P"
				+ "                  WHERE U_P.PEOPLE_ID = C.PEOPLE_ID)"
				+ "            AND A.IS_LOCK = 1"
				+ "            AND A.STATE <> 5"
				+ "         UNION"
				+ "         SELECT A.ID,"
				+ "                A.TASK_NAME,"
				+ "                A.STATE,"
				+ "                A.TASK_ELSE_STATE,"
				+ "                A.TASK_TYPE,"
				+ "                C.TRUE_NAME,"
				+ "                A.CREATE_PERSON_ID,"
				+ "                A.UPDATE_TIME,"
				+ "                A.start_time,"
				+ "                A.end_time,"
				+ "                A.FROM_PERSON_ID,"
				+ "                A.UP_STATE,"
				+ "				   JTS.STAGE_NAME,"
				+ "				   A.R_ID "
				+ "           FROM JECN_TASK_BEAN_NEW A"
				+ "			 INNER JOIN JECN_TASK_STAGE JTS ON A.ID=JTS.TASK_ID AND A.STATE=JTS.STAGE_MARK"
				+ "          INNER JOIN JECN_USER C ON A.CREATE_PERSON_ID = "
				+ loginPeopleId
				+ "                                AND C.PEOPLE_ID = A.CREATE_PERSON_ID"
				+ "          WHERE EXISTS (SELECT 1"
				+ "                   FROM JECN_USER_POSITION_RELATED U_P"
				+ "                  WHERE U_P.PEOPLE_ID = C.PEOPLE_ID)"
				+ "            AND A.IS_LOCK = 1"
				+ "            AND A.STATE <> 5";
		sql = sql
				+ ") select count(*) from TMP_TASK A where 1=1  and  A.TASK_ELSE_STATE <>3 AND (TASK_ELSE_STATE<>5 and (A.STATE<>10 or A.STATE<>0)) AND A.STATE<>5";

		if (searchTempBean == null) {// 不存在查询条件
			return taskSeachDao.countAllByParamsNativeSql(sql, loginPeopleId);
		} else {
			List<Object> listArgs = new ArrayList<Object>();
			// 拼装查询条件
			String searchSql = getSqlBytempBean(searchTempBean, sql, listArgs);

			// sql = searchSql + " order by a.UPDATE_TIME DESC";
			// 根据查询条件获取返回的总数
			return taskSeachDao.countAllByParamsNativeSql(searchSql, listArgs.toArray());
		}
	}

	/**
	 * 
	 * 假删除
	 * 
	 * @param taskId
	 *            任务主键ID
	 * @throws Exception
	 */
	@Transactional(rollbackFor = Exception.class)
	@Override
	public void falseDeleteTask(Long taskId) throws Exception {
		taskSeachDao.falseDeleteTask(taskId);
	}

	/**
	 * 根据object数组获取MyTaskBean集合
	 * 
	 * @param objectList
	 *            List<Object[]>
	 * @param loginPeopleId
	 *            人员主键ID 登录人的
	 * @param type
	 *            0：我的任务，1：任务管理，登录人不是管理，2：任务管理，登录人为管理员
	 * @return List<MyTaskBean>登录人相关任务集合
	 */
	private List<MyTaskBean> getMyTaskBeanList(List<Object[]> objectList, Long loginPeopleId, int type) {
		if (objectList == null || loginPeopleId == null) {
			throw new NullPointerException("getMyTaskBeanList获取任务记录异常！objectList == null || loginPeopleId == null");
		}

		// 获取当前审批人集合
		String hql = "SELECT T.taskId FROM JecnTaskPeopleNew T WHERE T.approvePid = " + loginPeopleId;
		// 获取当前审批人ID集合
		List<Long> listTaskIds = taskSeachDao.listHql(hql);
		// 记录登录人相关任务信息
		List<MyTaskBean> list = new ArrayList<MyTaskBean>();
		// 是否存在撤回
		boolean isAppMesTrue = false;
		for (Object[] objects : objectList) {
			// 我的任务信息
			MyTaskBean myTaskBean = new MyTaskBean();
			Long taskId = JecnTaskCommon.getlongByObject(objects[0]);
			// 0任务主键ID
			myTaskBean.setTaskId(taskId);
			// 1任务名称
			myTaskBean.setTaskName(objects[1].toString());
			// 2任务阶段
			myTaskBean.setTaskStage(JecnTaskCommon.getIntByObject(objects[2]));

			// 操作状态
			int taskEleType = JecnTaskCommon.getIntByObject(objects[3]);
			// 3任务 拟稿人操作状态 0：拟稿人提交审批 4：打回
			// 5：提交意见（提交审批意见(评审-------->拟稿人) 6：二次评审
			// 拟稿人------>评审 7：拟稿人重新提交审批 8：完成 9:打回整理意见(批准------>拟稿人)）10：交办人提交
			if (type == 0) {// 我的任务
				if ((myTaskBean.getTaskStage() == 0 || myTaskBean.getTaskStage() == 10)
						&& JecnTaskCommon.isSameStr(loginPeopleId, objects[6])) {// 整理意见阶段或你搞阶段(拟稿人是当前阶段审批人)
					if (taskEleType == -1) {
						continue;
					}
					if (taskEleType == 4) {// 打回 (被打回，拟稿人不应该有撤回操作)
						// 0是驳回重新提交状态
						myTaskBean.setTaskElseState(0);
					} else {
						// 1是评审意见整理状态
						myTaskBean.setTaskElseState(1);
					}
				} else if (JecnTaskCommon.isSameStr(loginPeopleId, objects[6])) {// 当前登录人为拟稿人，并且在目标人集合中不存在，不是当前阶段审批人
					// 拟稿人无审批状态
					if (!isAppTask(listTaskIds, objects[0].toString())) {//
						// 登录人为拟稿人，3:拟稿人无审批状态
						myTaskBean.setTaskElseState(3);
					} else {// 拟稿人为当前阶段审批人
						myTaskBean.setTaskElseState(4);
					}
				} else {
					myTaskBean.setTaskElseState(6);
				}
			} else if (type == 2) {// 任务管理，登录人为管理员
				myTaskBean.setTaskElseState(2);

			}

			// 4任务类型
			myTaskBean.setTaskType(JecnTaskCommon.getIntByObject(objects[4]));
			// 当前为拟稿人操作
			if (isAppMesTrue && !isDraftCur(myTaskBean.getTaskElseState()) && objects.length > 11) {
				// 是否存在撤回状态
				if (JecnTaskCommon.isSameStr(loginPeopleId, objects[10])) {// true：撤回
					if (myTaskBean.getTaskStage() == 3 && taskEleType == 5) {// 评审人阶段
						if (!JecnTaskCommon.isSameStr(loginPeopleId, objects[6])) {// 拟稿人参与评审
							// 操作为5说明评审人已经审批过
							continue;
						}
					} else {
						// 只有拟稿人才会给3这个标识,拟稿人无审批功能一定有撤回功能 （上面的if条件已将评审结果略过）
						if (myTaskBean.getTaskElseState() == 3) {
							// 存在撤回状态
							myTaskBean.setReJect(1);

						} else {
							// 判断如果当前登录人员是当前任务的审批人(也就是登录人当前需要审批的任务集合listTaskIds包含taskId)那么他只有审批操作，不可能有撤回操作
							// true为可以撤回 false 为不可以撤回
							boolean canCancel = true;
							for (Long id : listTaskIds) {
								if (id.longValue() == taskId) {
									canCancel = false;
								}

							}
							// 可以撤回
							if (canCancel) { // 撤回标示 1为可以撤回
								myTaskBean.setReJect(1);
							}
							// 打回整理 taskEleType为上个阶段的操作
							if (taskEleType == 9) {
								// 如果操作是9说明是打回整理 0是打回重新提交状态,1是评审意见整理状态,2为系统管理员;
								// 3:拟稿人无审批状态;4:拟稿人为当前阶段审批人5：其他 （目前存在打回整理状态、和打回）
								myTaskBean.setTaskElseState(5);
							}
							// 打回
							if (taskEleType == 4) {
								myTaskBean.setTaskElseState(5);
							}
						}
					}

				}
			}

			// 5创建人真实姓名
			if (objects[5] != null) {
				myTaskBean.setCreateName(objects[5].toString());
			}

			if (objects[7] != null) {
				myTaskBean.setUpdateTime(objects[7].toString());
			}
			if (objects[8] != null) {
				myTaskBean.setStartTime(JecnUtil.valueToDateStr(objects[8]));
			}
			if (objects[9] != null) {
				myTaskBean.setEndTime(JecnUtil.valueToDateStr(objects[9]));
			}
			if (objects[12] != null) {
				myTaskBean.setStageName(objects[12].toString());
			}
			if (objects.length >= 14) {
				myTaskBean.setResPeopleName(JecnUtil.nullToEmpty(objects[13]));
			}
			if (objects.length >= 15 && objects[14] != null) {
				myTaskBean.setPublishTime(JecnUtil.nullToEmpty(objects[14]));
			}
			list.add(myTaskBean);
		}
		return list;
	}

	private boolean noCallBack(int state, int upState, int taskEleType) {

		if (state == 3 && taskEleType == 5) {
			return true;
		}

		// 自定义阶段
		if (upState >= 11 && upState <= 14 && state >= 11 && state <= 14) {
			return true;
		}

		return false;
	}

	/**
	 * 
	 * 0是打回重新提交状态,1是评审意见整理状态,2为系统管理员;3:拟稿人无审批状态;4:拟稿人为当前阶段审批人
	 * 
	 * @param taskElseType
	 * @return
	 */
	private boolean isDraftCur(int taskElseType) {
		if (taskElseType == 0 || taskElseType == 1 || taskElseType == 4) {
			return true;
		}
		return false;
	}

	/**
	 * 拟稿人操作
	 * 
	 * @param taskElseType
	 * @return boolean true:拟稿人操作
	 */
	private boolean isDraft(int taskElseType, String upState) {
		if (JecnCommon.isNullOrEmtryTrim(upState)) {
			return false;
		}
		if (taskElseType == 1 && "10".equals(upState)) {// 整理意见阶段,提交后，拟稿人有撤回操作权限
			return true;
		}
		switch (taskElseType) {
		case 7:// 重新提交
		case 6:// 二次评审
		case 0:// 拟稿人提交审批
			return true;
		}
		return false;
	}

	/**
	 * 验证当前ID在集合中是否存在
	 * 
	 * @param listTaskIds
	 * @param taskId
	 * @return true 存在
	 */
	private boolean isAppTask(List<Long> listTaskIds, String taskId) {
		if (listTaskIds == null || taskId == null) {
			return false;
		}
		for (Long objects : listTaskIds) {
			if (objects.toString().equals(taskId)) {// 存在相同ID
				return true;
			}
		}
		return false;
	}

	public IJecnTaskSearchDao getTaskSeachDao() {
		return taskSeachDao;
	}

	public void setTaskSeachDao(IJecnTaskSearchDao taskSeachDao) {
		this.taskSeachDao = taskSeachDao;
	}

	/**
	 * 根据查询条件获取任务管理相关任务集合
	 * 
	 * @param searchTempBean
	 *            TaskSearchTempBean页面返回的查询信息
	 * @param start
	 *            每页开始行
	 * @param limit
	 *            每页最大行数
	 * @return List<MyTaskBean>登录人相关任务集合
	 * @throws Exception
	 */
	@Override
	public List<MyTaskBean> getTaskManagementBySearch(TaskSearchTempBean searchTempBean, int start, int limit,
			Long loginPeopleId, boolean isAdmin) throws Exception {
		// 默认为管理员登录
		int type = 2;
		if (!isAdmin) {// 如果登录人是管理员，type类型等于2
			type = 1;
		}
		String sql = getAllTaskSql(isAdmin, loginPeopleId);

		// 添加流程责任人
		sql = sql + addResPeopleType();

		// 根据任务结果获取任务相关信息，责任人类型获取责任人
		sql = getTaskResPeoleSql(sql);

		List<Object[]> objectList = null;
		if (searchTempBean == null) {// 默认查询所有
			sql = sql + " order by a.update_time desc";
			// Object[]
			// 0:任务主键ID，1：任务名称，2：任务阶段，3：任务操作状态，4：任务类型，5：创建人真实姓名，6:创建人主键ID 7:开始时间
			// ：结束时间
			if (start == -1 || limit == -1) {
				objectList = taskSeachDao.listNativeSql(sql);
			} else {
				objectList = taskSeachDao.listNativeSql(sql, start, limit);
			}
		} else {// 带条件的查询

			// 如果搁置任务
			if (searchTempBean.isDelayTask()) {
				// 默认是Oracle数据库
				if (JecnCommon.isOracle()) {
					sql = "with tmp as(select distinct jts.task_id "
							+ " from jecn_task_stage jts where jts.is_selected_user=1 AND JTS.STAGE_MARK!=10"
							+ " and ( "
							+ " SELECT COUNT(JTPCN.id) FROM JECN_TASK_PEOPLE_CONN_NEW JTPCN"
							+ " ,jecn_user ju"
							+ " WHERE JTPCN.STAGE_ID=jts.id "
							+ "  and JTPCN.approve_pid=ju.people_id and ju.islock=0"
							+ " )=0 "
							+ " union "
							+ " select distinct jts.task_id "
							+ " from jecn_task_stage jts where jts.is_selected_user=1 AND JTS.STAGE_MARK=3"
							+ " and ( "
							+ " SELECT COUNT(distinct JTPCN.approve_pid) FROM JECN_TASK_PEOPLE_CONN_NEW JTPCN"
							+ " ,jecn_user ju"
							+ " WHERE JTPCN.STAGE_ID=jts.id "
							+ "  and JTPCN.approve_pid=ju.people_id and ju.islock=0"
							+ " )<>(select  COUNT(JTPCN.id) FROM JECN_TASK_PEOPLE_CONN_NEW JTPCN WHERE JTPCN.STAGE_ID=jts.id )"
							+ " union " // 当前操作人离职或者没有岗位
							+ " select distinct jtbn.id" + " from jecn_task_bean_new jtbn"
							+ " left join jecn_task_people_new jtpn " + " on jtbn.id =jtpn.task_id"
							+ " left join jecn_user u " + " on jtpn.approve_pid =u.people_id"
							+ " where (jtpn.id is null or u.people_id is null or u.islock=1)" + "  and jtbn.state <> 5"
							+ "  and jtbn.is_lock = 1" + " )" + " select " + " a.id," + " a.task_name," + " a.state,"
							+ " a.task_else_state," + " a.task_type," + " u.true_name," + " a.create_person_id,"
							+ " a.update_time ," + " a.start_time," + " a.end_time," + " a.up_state,"
							+ " a.from_person_id," + " JTS.STAGE_NAME," + " jthn.publish_date"
							+ "  from jecn_task_bean_new a" + "  inner join tmp on a.id=tmp.task_id"
							+ "	INNER JOIN JECN_TASK_STAGE JTS ON A.ID=JTS.TASK_ID AND A.STATE=JTS.STAGE_MARK"
							+ "  left join jecn_user u on a.create_person_id=u.people_id"
							+ " left join jecn_task_history_new jthn on jthn.task_id=a.id"
							+ "  where a.IS_LOCK=1 and a.State<>5 ";

					// 如果是SQLServer 数据库
				} else if (JecnCommon.isSQLServer()) {
					sql = "select  a.id," + " a.task_name," + " a.state, " + " a.task_else_state, " + " a.task_type, "
							+ " u.true_name, " + " a.create_person_id, " + " a.update_time ,"
							+ " convert(varchar(100), a.start_time, 23) as start_time, "
							+ " convert(varchar(100), a.end_time, 23) as end_time, " + " a.up_state,"
							+ " a.from_person_id," + " JTS.STAGE_NAME," + " jthn.publish_date"
							+ " from jecn_task_bean_new a " + " inner join (select jts.task_id "
							+ " from jecn_task_stage jts " + " where jts.is_selected_user = 1 "
							+ " AND JTS.STAGE_MARK != 10 " + " and (SELECT COUNT(JTPCN.id) "
							+ " FROM JECN_TASK_PEOPLE_CONN_NEW  JTPCN, " + " jecn_user                  ju "
							+ " WHERE JTPCN.STAGE_ID = jts.id " + " and JTPCN.approve_pid = ju.people_id "
							+ " and ju.islock = 0) = 0 " + " union "
							+ " select jts.task_id "
							+ " from jecn_task_stage jts "
							+ " where jts.is_selected_user = 1 "
							+ " AND JTS.STAGE_MARK = 3 "
							+ " and (SELECT COUNT(distinct JTPCN.approve_pid) "
							+ " FROM JECN_TASK_PEOPLE_CONN_NEW  JTPCN, "
							+ " jecn_user                  ju "
							+ " WHERE JTPCN.STAGE_ID = jts.id "
							+ " and JTPCN.approve_pid = ju.people_id "
							+ " and ju.islock = 0) <> "
							+ " (select COUNT(JTPCN.id) "
							+ " FROM JECN_TASK_PEOPLE_CONN_NEW JTPCN "
							+ " WHERE JTPCN.STAGE_ID = jts.id)"
							+ " union " // 最新的操作人没有或者没有岗位
							+ " select distinct jtbn.id" + " from jecn_task_bean_new jtbn"
							+ " left join jecn_task_people_new jtpn " + " on jtbn.id =jtpn.task_id"
							+ " left join jecn_user u " + " on jtpn.approve_pid =u.people_id"
							+ " where (jtpn.id is null or u.people_id is null or u.islock=1)" + "  and jtbn.state <> 5"
							+ "  and jtbn.is_lock = 1" + " )" + " tmp on a.id = tmp.task_id "
							+ "INNER JOIN JECN_TASK_STAGE JTS ON A.ID=JTS.TASK_ID AND A.STATE=JTS.STAGE_MARK"
							+ " left join jecn_user u on a.create_person_id = u.people_id "
							+ " left join jecn_task_history_new jthn on jthn.task_id=a.id "
							+ " where a.is_lock = 1 and a.state<>5 ";
				}
			}

			// 拼装查询条件
			List<Object> listArgs = new ArrayList<Object>();
			// 拼装查询条件
			String searchSql = getSqlBytempBean(searchTempBean, sql, listArgs);
			sql = searchSql + " order by a.update_time desc";
			// 分页查询满足一定条件的数据集
			if (start == -1 || limit == -1) {
				objectList = taskSeachDao.listNativeSql(sql, listArgs.toArray());
			} else {
				objectList = taskSeachDao.listNativeSql(sql, start, limit, listArgs.toArray());
			}

		}
		// 根据object数组获取MyTaskBean集合
		return getMyTaskBeanList(objectList, loginPeopleId, type);
	}

	private String getAllTaskSql(boolean isAdmin, Long loginPeopleId) {
		String pubDate = "";
		if (JecnCommon.isOracle()) {
			pubDate = "to_char(ALL_TASK.publish_date,'YYYY-MM-DD hh24:mi') as publish_date ";
		} else if (JecnCommon.isSQLServer()) {
			pubDate = "convert(varchar(16), ALL_TASK.publish_date, 20) as publish_date ";
		}
		return "SELECT ALL_TASK.ID TASK_ID," + "               ALL_TASK.TASK_NAME," + "               ALL_TASK.STATE,"
				+ "               ALL_TASK.TASK_ELSE_STATE," + "               ALL_TASK.TASK_TYPE,"
				+ "               ALL_TASK.TRUE_NAME," + "               ALL_TASK.CREATE_PERSON_ID,"
				+ "               ALL_TASK.UPDATE_TIME," + "               ALL_TASK.START_TIME,"
				+ "               ALL_TASK.END_TIME," + "               ALL_TASK.UP_STATE,"
				+ "               ALL_TASK.FROM_PERSON_ID," + " CASE WHEN ALL_TASK.STAGE_NAME IS NULL "
				+ " AND ALL_TASK.STATE = 5 THEN" + "                 '完成'" + "                ELSE"
				+ "                 ALL_TASK.STAGE_NAME" + "              END AS STAGE_NAME,"
				+ getCaseResPeople()
				+ " ,"
				+ pubDate
				+ "          FROM (SELECT A.ID,"
				+ "                       A.TASK_NAME,"
				+ "                       A.STATE,"
				+ "                       A.TASK_ELSE_STATE,"
				+ "                       A.TASK_TYPE,"
				+ "                       C.TRUE_NAME,"
				+ "                       A.CREATE_PERSON_ID,"
				+ "                       A.UPDATE_TIME,"
				+ "                       A.START_TIME,"
				+ "                       A.END_TIME,"
				+ "                       A.UP_STATE,"
				+ "                       A.FROM_PERSON_ID,"
				+ "                       JTS.STAGE_NAME,"
				+ "                       A.R_ID,"
				+ " A.update_time as publish_date"
				+ "                  FROM JECN_TASK_BEAN_NEW A"
				+ "                 LEFT JOIN JECN_TASK_STAGE JTS"
				+ "                    ON A.ID = JTS.TASK_ID"
				+ "                   AND A.STATE = JTS.STAGE_MARK"
				+ "                  LEFT JOIN JECN_USER C"
				+ "                    ON A.CREATE_PERSON_ID = C.PEOPLE_ID"
				+ " left join jecn_task_history_new jthn on jthn.task_id=a.id"
				+ "                 WHERE A.IS_LOCK = 1"
				+
				// 非管理员执行
				getCommonPeopleSql(isAdmin, loginPeopleId) + ") ALL_TASK";
	}

	/**
	 * 任务中相关责任人查询
	 * 
	 * @param sql
	 * @return
	 */
	private String getTaskResPeoleSql(String sql) {
		String resSql = "SELECT TASK_ID," + "       TASK_NAME," + "       STATE," + "       TASK_ELSE_STATE,"
				+ "       TASK_TYPE," + "       A.TRUE_NAME," + "       CREATE_PERSON_ID," + "       UPDATE_TIME,"
				+ JecnCommonSql.getSqlServerCONVERT("START_TIME")
				+ "       ,"
				+ JecnCommonSql.getSqlServerCONVERT("END_TIME")
				+ "       ,"
				+ "       FROM_PERSON_ID,"
				+ "       UP_STATE,"
				+ "       STAGE_NAME,"
				+ "       CASE"
				+ "         WHEN TYPE_RES_PEOPLE = 0 THEN"
				+ "          JU.TRUE_NAME"
				+ "         WHEN TYPE_RES_PEOPLE = 1 THEN"
				+ "          OI.FIGURE_TEXT"
				+ "       END FLOW_RES_PEOPLE"
				+ " ,publish_date"
				+ "  FROM ("
				+ sql
				+ ") A"
				+ "  LEFT JOIN JECN_USER JU"
				+ "    ON A.RES_PEOPLE_ID = JU.PEOPLE_ID"
				+ "  LEFT JOIN JECN_FLOW_ORG_IMAGE OI" + "    ON A.RES_PEOPLE_ID = OI.FIGURE_ID" + " WHERE 1 = 1";

		return resSql;
	}

	/**
	 * 查询责任人case判断
	 * 
	 * @return
	 */
	private String getCaseResPeople() {
		String value = JecnCommonSql.getSqlValueDefault();
		return "               CASE" + "                 WHEN TASK_TYPE = 0 THEN" + value + "(FBIT.RES_PEOPLE_ID, -1)"
				+ "                 WHEN TASK_TYPE = 2 THEN" + value + "(JRT.RES_PEOPLE_ID, -1)"
				+ "                 WHEN TASK_TYPE = 3 THEN" + value + "(JRT.RES_PEOPLE_ID, -1)"
				+ "                 ELSE" + "                  -1" + "               END RES_PEOPLE_ID,"
				+ "               CASE" + "                 WHEN TASK_TYPE = 0 THEN" + value
				+ "(FBIT.TYPE_RES_PEOPLE, -1)" + "                 WHEN TASK_TYPE = 2 THEN" + value
				+ "(JRT.TYPE_RES_PEOPLE, -1)" + "                 WHEN TASK_TYPE = 3 THEN" + value
				+ "(JRT.TYPE_RES_PEOPLE, -1)" + "                 ELSE" + "                  -1"
				+ "               END TYPE_RES_PEOPLE";
	}

	private String addResPeopleType() {
		return " LEFT JOIN JECN_FLOW_BASIC_INFO_T FBIT" + "           ON ALL_TASK.R_ID = FBIT.FLOW_ID"
				+ "         LEFT JOIN JECN_RULE_T JRT" + "           ON ALL_TASK.R_ID = JRT.ID"
				+ "         LEFT JOIN JECN_FILE_T JFT" + "           ON ALL_TASK.R_ID = JFT.FILE_ID";
	}

	/**
	 * 非管理员
	 * 
	 * @param loginPeopleId
	 * @return
	 */
	public String getCommonPeopleSql(boolean isAdmin, Long loginPeopleId) {
		if (isAdmin) {
			return "";
		}
		return "  AND EXISTS (SELECT 1 FROM JECN_TASK_FOR_RECODE_NEW FRN WHERE FRN.TASK_ID = a.ID AND FRN.TO_PERSON_ID = "
				+ loginPeopleId + ")";
	}

	public String getCreatePeopleSql(boolean isAdmin, Long loginPeopleId) {
		if (isAdmin) {
			return "";
		}
		return "  AND A.CREATE_PERSON_ID = " + loginPeopleId;
	}

	/**
	 * 任务管理任务总数
	 * 
	 * @param searchTempBean
	 * @param loginPeopleId
	 *            当前登录人人员主键ID
	 * @param isAdmin
	 *            true:系统管理员
	 * @return List<MyTaskBean>任务集合
	 * @throws Exception
	 */
	@Override
	public int getTaskManagementCountBySearch(TaskSearchTempBean searchTempBean, Long loginPeopleId, boolean isAdmin)
			throws Exception {
		String sql = " select count(*)  from  jecn_task_bean_new a where a.is_lock=1 ";
		// 非管理员执行
		sql = sql + getCommonPeopleSql(isAdmin, loginPeopleId);
		if (searchTempBean == null) {// 不存在查询条件

			return taskSeachDao.countAllByParamsNativeSql(sql);
		} else {
			List<Object> listArgs = new ArrayList<Object>();
			// 如果搁置任务
			if (searchTempBean.isDelayTask()) {

				sql = "with tmp as(select distinct jts.task_id "
						+ " from jecn_task_stage jts where jts.is_selected_user=1 AND JTS.STAGE_MARK!=10"
						+ " and ( "
						+ " SELECT COUNT(JTPCN.id) FROM JECN_TASK_PEOPLE_CONN_NEW JTPCN"
						+ " ,jecn_user ju"
						+ " WHERE JTPCN.STAGE_ID=jts.id "
						+ "  and JTPCN.approve_pid=ju.people_id and ju.islock=0"
						+ " )=0 "
						+ " union "
						+ " select distinct jts.task_id "
						+ " from jecn_task_stage jts where jts.is_selected_user=1 AND JTS.STAGE_MARK=3"
						+ " and ( "
						+ " SELECT COUNT(distinct JTPCN.approve_pid) FROM JECN_TASK_PEOPLE_CONN_NEW JTPCN"
						+ " ,jecn_user ju"
						+ " WHERE JTPCN.STAGE_ID=jts.id "
						+ "  and JTPCN.approve_pid=ju.people_id and ju.islock=0"
						+ " )<>(select  COUNT(JTPCN.id) FROM JECN_TASK_PEOPLE_CONN_NEW JTPCN WHERE JTPCN.STAGE_ID=jts.id )"
						+ " union " // 最新的操作人没有或者没有岗位
						+ " select distinct jtbn.id" + " from jecn_task_bean_new jtbn"
						+ " left join jecn_task_people_new jtpn " + " on jtbn.id =jtpn.task_id"
						+ " left join jecn_user u " + " on jtpn.approve_pid =u.people_id"
						+ " where (jtpn.id is null or u.people_id is null or u.islock=1)" + "  and jtbn.state <> 5"
						+ "  and jtbn.is_lock = 1" + " )" + "  select  count(a.id) from jecn_task_bean_new a"
						+ "  inner join tmp on a.id=tmp.task_id"
						+ "  left join jecn_user u on a.create_person_id=u.people_id"
						+ "  where a.IS_LOCK=1 and a.State<>5 ";
			}
			// 拼装查询条件
			String searchSql = getSqlBytempBean(searchTempBean, sql, listArgs);

			// 根据查询条件获取返回的总数
			return taskSeachDao.countAllByParamsNativeSql(searchSql, listArgs.toArray());

		}

	}

	@Override
	public JecnTaskBeanNew getJecnTaskBeanNew(Long taskId) throws Exception {

		return taskSeachDao.get(taskId);
	}

	/**
	 * 人员替换
	 * 
	 * @param fromPeopleId
	 *            来源人
	 * @param toPeopleId
	 *            替换人
	 * @throws Exception
	 * 
	 */
	@Transactional(rollbackFor = Exception.class)
	@Override
	public void replaceApprovePeople(String fromPeopleId, String toPeopleId) throws Exception {

		if (StringUtils.isBlank(fromPeopleId) || StringUtils.isBlank(toPeopleId)) {
			throw new IllegalArgumentException("fromPeopleId is blank || toPeopleId is blank");
		}
		// 人员替换
		taskSeachDao.updateTaskApprovePeople(fromPeopleId, toPeopleId);
	}

	@Override
	public byte[] downloadTaskManagementBySearch(List<MyTaskBean> listTask) throws Exception {
		byte[] tempDate = null;
		WritableWorkbook workBook = null;
		File file = null;
		try {
			// 获取Excel 临时文件路径
			String filePath = JecnContants.jecn_path + JecnFinal.saveExcelTempFilePath();
			// 创建导出到excel
			file = new File(filePath);
			workBook = Workbook.createWorkbook(file);
			// 写出文件
			writeExcelFile(workBook, listTask);
			// 后续处理--------------------------------------
			workBook.write();
			// 工作簿不关闭不会写出数据
			// workbook在调用close方法时，才向输出流中写入数据
			workBook.close();
			workBook = null;
			// 把一个文件转化为字节
			tempDate = JecnUtil.getByte(file);

		} catch (Exception e) {
			throw new Exception("任务下载异常", e);
		} finally {
			if (workBook != null) {
				try {
					workBook.close();
				} catch (Exception e) {
					throw new Exception("downloadTaskManagementBySearch关闭excel工作表异常", e);
				}
			}
			// 删除临时文件
			if (file != null && file.exists()) {
				file.delete();
			}
		}

		return tempDate;
	}

	/**
	 * 填写Excel具体的内容
	 * 
	 * @param workBook
	 * @param listTask
	 * @throws RowsExceededException
	 * @throws WriteException
	 */
	private void writeExcelFile(WritableWorkbook workBook, List<MyTaskBean> listTask) throws RowsExceededException,
			WriteException {

		WritableSheet wsheet = workBook.createSheet("task", 0);
		// 写出表头
		createTitle(wsheet);
		// 写出内容
		createContent(wsheet, listTask);
	}

	private void createContent(WritableSheet wsheet, List<MyTaskBean> listTask) throws WriteException {
		if (listTask == null || listTask.size() == 0) {
			return;
		}
		// 行
		int row = 0;
		// 列
		int col = 0;
		// 序号
		int index = 1;
		// 定义样式
		WritableCellFormat contentStyle = getSheetContentStyle();
		// 写出所有数据
		for (MyTaskBean webBean : listTask) {
			// 写入下一行
			row++;
			col = 0;
			wsheet.setRowView(row, 400);
			wsheet.addCell(new Label(col++, row, String.valueOf(index), contentStyle));
			wsheet.addCell(new Label(col++, row, webBean.getTaskName(), contentStyle));
			// 任务类型
			wsheet.addCell(new Label(col++, row, getTaskType(webBean.getTaskType()), contentStyle));
			// 创建人
			wsheet.addCell(new Label(col++, row, webBean.getCreateName(), contentStyle));
			// 责任人
			wsheet.addCell(new Label(col++, row, webBean.getResPeopleName(), contentStyle));
			// 阶段
			wsheet.addCell(new Label(col++, row, webBean.getStageName(), contentStyle));
			wsheet.addCell(new Label(col++, row, webBean.getStartTime(), contentStyle));
			wsheet.addCell(new Label(col++, row, webBean.getEndTime(), contentStyle));
			wsheet.addCell(new Label(col++, row, webBean.getPublishTime(), contentStyle));
			index++;
		}

	}

	private void createTitle(WritableSheet wsheet) throws RowsExceededException, WriteException {
		// 行
		int row = 0;
		// 列
		int col = 0;
		// 序号
		String indexStr = JecnUtil.getValue("index");
		// 任务名称
		String taskName = JecnUtil.getValue("taskName");
		// 任务类型
		String taskType = JecnUtil.getValue("taskType");
		// 创建人
		String createPeople = JecnUtil.getValue("createPeople");
		// 责任人
		String dutyPeople = JecnUtil.getValue("responsiblePersons");
		// 阶段
		String stage = JecnUtil.getValue("stage");
		// 开始时间
		String startTime = JecnUtil.getValue("startTime");
		// 预估结束时间
		String predictEndTime = JecnUtil.getValue("predictEndTime");
		// 实际结束时间
		String endTime = JecnUtil.getValue("realEndTime");

		// 表头的样式 背景为黄色 字体为红色
		WritableCellFormat titleStyleY = getSheetTitleStyleY();

		// ------------------------写出表头------------------------
		// -------------调整列宽
		// 索引
		wsheet.setColumnView(col++, 5);
		wsheet.setColumnView(col++, 28);
		wsheet.setColumnView(col++, 20);
		wsheet.setColumnView(col++, 20);
		wsheet.setColumnView(col++, 20);
		wsheet.setColumnView(col++, 20);
		wsheet.setColumnView(col++, 20);
		wsheet.setColumnView(col++, 20);
		wsheet.setColumnView(col++, 20);

		// -------------写出表头的内容
		col = 0;
		// 设置高度
		wsheet.setRowView(row, 400);
		// 表头
		wsheet.addCell(new Label(col++, row, indexStr, titleStyleY));
		wsheet.addCell(new Label(col++, row, taskName, titleStyleY));
		wsheet.addCell(new Label(col++, row, taskType, titleStyleY));
		wsheet.addCell(new Label(col++, row, createPeople, titleStyleY));
		wsheet.addCell(new Label(col++, row, dutyPeople, titleStyleY));
		wsheet.addCell(new Label(col++, row, stage, titleStyleY));
		wsheet.addCell(new Label(col++, row, startTime, titleStyleY));
		wsheet.addCell(new Label(col++, row, predictEndTime, titleStyleY));
		wsheet.addCell(new Label(col++, row, endTime, titleStyleY));

	}

	/**
	 * 设置表头的样式
	 */
	private WritableCellFormat getSheetTitleStyleY() throws WriteException {
		WritableFont wfc = new WritableFont(WritableFont.createFont("宋体"), 10, WritableFont.BOLD, false,
				UnderlineStyle.NO_UNDERLINE, Colour.RED);
		WritableCellFormat tempCellFormat = new WritableCellFormat(wfc);
		// 居中对齐
		tempCellFormat.setAlignment(Alignment.CENTRE);
		tempCellFormat.setVerticalAlignment(VerticalAlignment.CENTRE);
		tempCellFormat.setBackground(Colour.YELLOW);
		tempCellFormat.setBorder(Border.ALL, BorderLineStyle.THIN);
		tempCellFormat.setWrap(true);

		return tempCellFormat;
	}

	/**
	 * 设置内容样式
	 */
	private WritableCellFormat getSheetContentStyle() throws WriteException {
		WritableFont wfc = new WritableFont(WritableFont.createFont("宋体"), 10, WritableFont.NO_BOLD, false,
				UnderlineStyle.NO_UNDERLINE, Colour.BLACK);
		WritableCellFormat tempCellFormat = new WritableCellFormat(wfc, NumberFormats.TEXT);
		// 居中对齐
		tempCellFormat.setAlignment(Alignment.CENTRE);
		tempCellFormat.setVerticalAlignment(VerticalAlignment.CENTRE);
		tempCellFormat.setBorder(Border.ALL, BorderLineStyle.THIN);
		return tempCellFormat;
	}

	/**
	 * 根据任务类型获得对应的String
	 * 
	 * @param value
	 * @return
	 */
	private String getTaskType(int value) {
		String taskTypeName = "";
		switch (value) {
		case 0:
			taskTypeName = JecnUtil.getValue("processTask");// 流程任务
			break;
		case 4:
			taskTypeName = JecnUtil.getValue("mapTask");// 地图任务
			break;
		case 1:
			taskTypeName = JecnUtil.getValue("fileTask");// 文件任务
			break;
		case 2:// 制度模板文件
		case 3:// 制度文件
			taskTypeName = JecnUtil.getValue("ruleTask");// 制度任务
			break;

		}
		return taskTypeName;
	}

	@Override
	public int getSecondAdminTaskManagementCountBySearch(TaskSearchTempBean searchTempBean, Long loginPeopleId)
			throws Exception {
		Long roleId = jecnRoleDao.getSecondAdminRoleId(loginPeopleId);
		return taskSeachDao.countAllSecondAdminTaskNum(roleId, loginPeopleId, searchTempBean);

	}

	@Override
	public List<MyTaskBean> getSecondAdminTaskManagementBySearch(TaskSearchTempBean searchTempBean, int start,
			int limit, Long curPeopleId) throws Exception {
		Long roleId = jecnRoleDao.getSecondAdminRoleId(curPeopleId);
		List<MyTaskBean> tasks = this.taskSeachDao.listSecondAdminList(searchTempBean, start, limit, curPeopleId,
				roleId);

		return tasks;
	}

	public IJecnRoleDao getJecnRoleDao() {
		return jecnRoleDao;
	}

	public void setJecnRoleDao(IJecnRoleDao jecnRoleDao) {
		this.jecnRoleDao = jecnRoleDao;
	}

	@Override
	public List<Map<String, Object>> getTasksByUser(JecnUser jecnUser) {
		String basicEprosURL = JecnContants.sysPubItemMap.get(ConfigItemPartMapMark.basicEprosURL.toString());
		List<MyTaskBean> listTask = getToDotasksBean(jecnUser.getPeopleId(), 0, -1);
		List<Map<String, Object>> maps = new Vector<Map<String, Object>>();
		for (MyTaskBean myTaskBean : listTask) {
			maps.add(getTaskMapByLoginStatus(myTaskBean, basicEprosURL, jecnUser));
		}
		return maps;
	}

	private Map<String, Object> getTaskMapByLoginStatus(MyTaskBean bean, String basicEprosURL, JecnUser jecnUser) {
		// ProcessID 流程编号
		// ProcessType 流程类型（例如离职、报销）
		// ProcessName 流程名称
		// ProcessURL 流程链接
		// OriginatorName 申请人
		// StartDate 申请时间
		// Receiver 任务接收人（审批人）
		// ReceiverDate 任务接收时间
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("ProcessID", bean.getTaskId());
		map.put("ProcessType", JecnConfigTool.isLiBangLoginType() ? "制度管理" : "EPROS流程审批");
		map.put("ProcessName", bean.getTaskName());
		map.put("ProcessURL", basicEprosURL + SinglePointBean.getLoginMailApproveDominoUrl(bean));
		// 创建人
		map.put("OriginatorName", bean.getCreateName());
		// 申请时间
		map.put("StartDate", bean.getCreateTime());

		// 任务接收者
		map.put("Receiver", jecnUser.getTrueName());
		map.put("ReceiverDate", bean.getUpdateTime());
		return map;
	}

}
