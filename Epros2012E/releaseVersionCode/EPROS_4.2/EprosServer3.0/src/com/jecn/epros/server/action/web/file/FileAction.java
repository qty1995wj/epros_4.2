package com.jecn.epros.server.action.web.file;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import net.sf.json.JSONArray;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.jecn.epros.server.bean.file.FileOpenBean;
import com.jecn.epros.server.bean.file.FileWebBean;
import com.jecn.epros.server.bean.process.FlowFileContent;
import com.jecn.epros.server.bean.process.JecnFlowStructure;
import com.jecn.epros.server.bean.process.JecnFlowStructureT;
import com.jecn.epros.server.bean.rule.G020RuleFileContent;
import com.jecn.epros.server.bean.rule.Rule;
import com.jecn.epros.server.bean.rule.RuleT;
import com.jecn.epros.server.bean.task.JecnTaskHistoryFollow;
import com.jecn.epros.server.bean.task.JecnTaskHistoryNew;
import com.jecn.epros.server.bean.tree.JecnTreeBean;
import com.jecn.epros.server.common.BaseAction;
import com.jecn.epros.server.common.JecnCommon;
import com.jecn.epros.server.common.JecnConfigTool;
import com.jecn.epros.server.common.JecnContants;
import com.jecn.epros.server.common.JecnEnumUtil.BrowserName;
import com.jecn.epros.server.service.file.IDownLoadCountService;
import com.jecn.epros.server.service.file.IFileService;
import com.jecn.epros.server.service.log.IFileDownloadLogService;
import com.jecn.epros.server.service.popedom.IJecnRoleService;
import com.jecn.epros.server.service.process.IFlowStructureService;
import com.jecn.epros.server.service.process.IWebProcessService;
import com.jecn.epros.server.service.rule.IRuleService;
import com.jecn.epros.server.service.standard.IStandardService;
import com.jecn.epros.server.service.system.IJecnConfigItemService;
import com.jecn.epros.server.util.JecnUtil;
import com.jecn.epros.server.util.JecnTreeEnumConstant.TreeNodeType;
import com.jecn.epros.server.webBean.AttenTionSearchBean;
import com.jecn.epros.server.webBean.PublicFileBean;
import com.jecn.epros.server.webBean.PublicSearchBean;
import com.jecn.epros.server.webBean.WebLoginBean;
import com.jecn.epros.server.webBean.WebTreeBean;
import com.jecn.epros.server.webBean.file.FileBean;
import com.jecn.epros.server.webBean.file.FileDetailListWebBean;
import com.jecn.epros.server.webBean.file.FileDetailRelatedWebBean;
import com.jecn.epros.server.webBean.file.FileDetailWebBean;
import com.jecn.epros.server.webBean.file.FileSearchBean;
import com.jecn.epros.server.webBean.file.FileUseBean;
import com.jecn.viewdoc.convert.ViewDocsManager;
import com.jecn.viewdoc.convert.view.CommonView;
import com.jecn.viewdoc.convert.view.PageofficeView;
import com.jecn.viewdoc.convert.view.View;
import com.jecn.viewdoc.convert.view.ViewInfo;
import com.jecn.viewdoc.service.IViewDocService;
import com.opensymphony.xwork2.ActionContext;

@Component
public class FileAction extends BaseAction {
	/** 树加载传回的PID */
	private long node;
	/** 开始行数 */
	private int start;
	/** 每页显示条数 */
	private int limit;
	/** 文件ID */
	private long fileId;
	/** 文件名称 */
	private String fileName;
	/** 文件编号 */
	private String fileNum;
	/** 责任部门 */
	private long orgId = -1;
	/** 责任部门名称 */
	private String orgName;
	/** 查阅部门 */
	private long orgLookId = -1;
	/** 查阅部门名称 */
	private String orgLookName;
	/** 查阅岗位 */
	private long posLookId = -1;
	/** 查阅岗位名称 */
	private String posLookName;
	/** 密级 默认公开 -1全部, 0秘密, 1公开 */
	private int secretLevel = -1;
	/** 类型 -1全部 0是流程，1是文件，2是标准，3是制度 */
	private int type = -1;
	/** downFile:下载 infoFile:文件详情 */
	private String visitType;
	/** 1加入tab 0弹出 */
	private int callType;
	/** 文件service */
	private IFileService fileService;
	private IWebProcessService webProcessService;
	private IStandardService standardService;
	private IRuleService ruleService;
	private IJecnRoleService jecnRoleService;

	@Autowired
	private IFlowStructureService flowStructureService;
	@Autowired
	IDownLoadCountService downLoadCountService;
	@Autowired
	IJecnConfigItemService jecnConfigItemService;
	@Autowired
	IFileDownloadLogService fileDownloadLogService;

	@Resource
	private IViewDocService viewDocService;
	// 登陆人员ID
	Long peopleId = null;

	/** 下载统计是否成功 */
	boolean downLoadCount = false;

	/** 判断是否达到最大下载次数 */
	boolean downLoadNum = false;

	private static final Logger log = Logger.getLogger(FileAction.class);

	private FileBean fileBean;

	private List<FileUseBean> fileUseList;
	/** 文件bean */
	private FileOpenBean fileOpenBean;
	/** 文件名称 */
	private String downFileName;
	/** 查询标示 "ture"查询正式表 "false"查询临时表 */
	private String isPub;
	/** 标准清单 */
	private FileDetailListWebBean fileDetailListWebBean;
	/** 文件清单默认显示10条 true 显示所有 */
	private boolean showAll = false;
	/** 文件清单名 **/
	private String fileDetailListName;
	/** 文件清单流 **/
	private byte[] fileDetailListContent;
	/** 是否达到最大限制次数 true:是；false：否 */
	private boolean isDownLoadMax = false;

	/** 文控信息 */
	private List<JecnTaskHistoryNew> taskHistoryNewList;
	private JecnTaskHistoryNew taskHistoryNew;
	private Long historyId;
	/**
	 * 在线预览类型：文件(未发布)： 0； 文件(已发布)1 ；制度文件(未发布) 2；制度（已发布）；制度（未发布）3 ;
	 * 4、流程（文件）已发布，5：流程（文件）未发布;6 :标准附件; 7:流程图/架构图图片 已发布; 8:流程图/架构图图片 未发布
	 * 9：流程文件word 已发布;10：流程文件word 未发布;11:制度模板 已发布 ；12：制度模板未发布
	 **/
	private int viewType;
	private String downFileType;
	private byte[] downFileByte;

	/** true:文控版本信息 */
	private String isHistory;

	private boolean isp;
	private String isApp = null;

	private String isDownLoad = "false";

	public String isDownLoad() {
		return isDownLoad;
	}

	public void setDownLoad(String isDownLoad) {
		this.isDownLoad = isDownLoad;
	}

	/**
	 * 
	 * 文件清单下载
	 * 
	 * @return
	 */
	public String downLoadFileDetailList() {
		try {
			// 获取文件清单列表
			fileDetailListWebBean = fileService.getFileDetailList(fileId, true);
			// 获取excel 字节数组
			fileDetailListContent = fileService.downLoadFileDetailListExcel(fileDetailListWebBean);
			// 标准清单
			fileDetailListName = JecnUtil.getValue("fileDetailList") + ".xls";
		} catch (Exception e) {
			log.error("文件清单下载异常！", e);
		}
		return SUCCESS;
	}

	/**
	 * @author wdp
	 * @description 获取文件清单数据(ajax)
	 * @return
	 */
	public String obtainFileDetailList() {
		try {
			// 获取文件清单列表
			fileDetailListWebBean = fileService.getFileDetailList(fileId, showAll);
			// 转换数据为JSON
			String json = convertFileDetailListToJSON(fileDetailListWebBean);
			// 返回Json
			outJsonString(json);
		} catch (Exception e) {
			log.error("文件清单查询出错！", e);
		}
		return SUCCESS;
	}

	/**
	 * @author wdp
	 * @description 转换文件清单数据为Json 用于Ext(ColumnModel、JsonStore)
	 * @param fileDetailListWebBean
	 * @return
	 */
	private String convertFileDetailListToJSON(FileDetailListWebBean fileDetailListWebBean) {
		// 获取store数据的对应关系
		String stores = getStoresByFileDetailList(fileDetailListWebBean);
		// 获取列名和对应的store数据名
		String columns = getColumnsByFileDetailList(fileDetailListWebBean);
		// 获取数据
		String data = getDataByFileDetailList(fileDetailListWebBean);

		return JecnCommon.composeJson(stores, columns, data, 0, 0, 0, 0);
	}

	/***
	 * 递归查找当前文件的所有父级目录，并且拼装到JSON中
	 * 
	 * @param fileDetailWebBean
	 *            文件清单bean fileDetailListWebBean 文件清单集合类 bufer
	 * @author chehuanbo
	 * @since V3.06
	 */
	private void findDir(FileDetailWebBean fileDetailWebBean, FileDetailListWebBean fileDetailListWebBean,
			StringBuffer bufer) {

		FileDetailWebBean fileDetailWebBean1 = fileDetailWebBean;
		boolean isExit = false; // 判断是否存在父节点 ，默认：false 不存在
		for (FileDetailWebBean perFileDetailWebBean : fileDetailListWebBean.getFileDetailList()) {
			if (fileDetailWebBean1.getPerFileId().longValue() == perFileDetailWebBean.getFileId()) {
				// 风险名称 （将目录名称添加到Json中）
				bufer.append(JecnCommon.contentJson(perFileDetailWebBean.getLevel() + "fileName", perFileDetailWebBean
						.getFileName())
						+ ",");
				// 更改临时数据bean 主要用来下一次查找父级节点的查找
				fileDetailWebBean1 = perFileDetailWebBean;
				isExit = true;
				break;
			}
		}
		if (isExit) { // 如果存在父节点，继续递归查找
			findDir(fileDetailWebBean1, fileDetailListWebBean, bufer);
		}
	}

	/**
	 * @author weidp
	 * @description 获取JsonStore的Data
	 * @param fileDetailListWebBean
	 * @return 修改人：chehuanbo
	 * @since V3.06
	 * 
	 */
	private String getDataByFileDetailList(FileDetailListWebBean fileDetailListWebBean) {
		StringBuffer sb = new StringBuffer("[");
		// 获取数据条数
		int dataSize = fileDetailListWebBean.getFileDetailList().size();

		for (int i = 0; i < dataSize; i++) {

			FileDetailWebBean fileDetailBean = fileDetailListWebBean.getFileDetailList().get(i);
			// 目录
			if (fileDetailBean.getIsDir() == 0) {
				continue;
			}
			sb.append("{");

			// 递归查找文件的父目录
			findDir(fileDetailBean, fileDetailListWebBean, sb);
			// 文件名称
			sb.append(JecnCommon.contentJson("fileName", fileDetailBean.getHrefFileName()) + ",");
			// 文件编号
			sb.append(JecnCommon.contentJson("fileNumber", fileDetailBean.getDocNumber()) + ",");
			// 责任部门
			sb
					.append(JecnCommon.contentJson("responsibilityDepartment", fileDetailBean.getResponsibleDeptName())
							+ ",");
			// 创建人
			sb.append(JecnCommon.contentJson("creator", fileDetailBean.getCreator()) + ",");
			// 创建时间
			sb.append(JecnCommon.contentJson("createTime", fileDetailBean.getCreateTime()) + ",");
			// 更新时间
			sb.append(JecnCommon.contentJson("updateTime", fileDetailBean.getUpdateTime()) + ",");
			// 存储文件相关信息
			List<FileDetailRelatedWebBean> relateList = fileDetailBean.getFileDetailRelatedList();
			StringBuffer fileStr = new StringBuffer();
			for (FileDetailRelatedWebBean relateBean : relateList) {
				if (relateBean.getRelatedType().equals(TreeNodeType.process.toString())) {// 流程
					fileStr.append(relateBean.getHrefOfProcess()).append("[" + JecnUtil.getValue("flow") + "]");
				} else if (relateBean.getRelatedType().equals(// 流程地图
						TreeNodeType.processMap.toString())) {
					fileStr.append(relateBean.getHrefOfProcessMap()).append("[" + JecnUtil.getValue("flowMap") + "]");
				} else if (relateBean.getRelatedType().equals( // 制度文件
						TreeNodeType.ruleFile.toString())) {
					fileStr.append(relateBean.getHrefOfRuleFile()).append("[" + JecnUtil.getValue("ruleFile") + "]");
				} else if (relateBean.getRelatedType().equals( // 制度模板文件
						TreeNodeType.ruleModeFile.toString())) {
					fileStr.append(relateBean.getHrefOfRuleModeFile()).append(
							"[" + JecnUtil.getValue("ruleModeFile") + "]");
				} else if (relateBean.getRelatedType().equals( // 标准
						TreeNodeType.standard.toString())) {
					fileStr.append(relateBean.getHrefOfStandard()).append("[" + JecnUtil.getValue("standard") + "]");
				} else if (relateBean.getRelatedType().equals( // 岗位
						TreeNodeType.position.toString())) {
					fileStr.append(relateBean.getHrefOfPos()).append("[" + JecnUtil.getValue("posAccessory") + "]");
				}
				fileStr.append("<BR>");
			}
			sb.append(JecnCommon.contentJson("fileDetailRelatedList", fileStr.toString()));
			sb.append("},");
		}
		if (sb.toString().endsWith(",")) {
			sb.setLength(sb.length() - 1);
		}
		sb.append("]");
		return sb.toString();
	}

	/**
	 * @author weidp
	 * @description 获取ColumnModel
	 * @param fileDetailListWebBean
	 */
	private String getColumnsByFileDetailList(FileDetailListWebBean fileDetailListWebBean) {
		StringBuffer columnsBuf = new StringBuffer();
		columnsBuf.append("[new Ext.grid.RowNumberer(),");
		// 文件级别
		for (int i = 0; i < fileDetailListWebBean.getMaxLevel(); i++) {
			if (fileDetailListWebBean.getCurLevel() <= i) {
				columnsBuf.append(JecnCommon.columnsJson(i + JecnUtil.getValue("level"), i + "level", null) + ",");
			}
		}
		// 文件名称
		columnsBuf.append(JecnCommon.columnsJson(JecnUtil.getValue("fileName"), "fileName", null) + ",");
		// 文件编号
		columnsBuf.append(JecnCommon.columnsJson(JecnUtil.getValue("fileNumber"), "fileNumber", null) + ",");
		// 责任部门
		columnsBuf.append(JecnCommon.columnsJson(JecnUtil.getValue("responsibilityDepartment"),
				"responsibilityDepartment", null)
				+ ",");
		// 文件创建人
		columnsBuf.append(JecnCommon.columnsJson(JecnUtil.getValue("fileCreator"), "creator", null) + ",");
		// 创建时间
		columnsBuf.append(JecnCommon.columnsJson(JecnUtil.getValue("createTime"), "createTime", null) + ",");
		// 发布时间
		columnsBuf.append(JecnCommon.columnsJson("发布时间", "updateTime", null) + ",");
		// 文件使用情况
		columnsBuf.append(JecnCommon.columnsJson(JecnUtil.getValue("fileUse"), "fileDetailRelatedList", null) + "]");
		return columnsBuf.toString();
	}

	/**
	 * @author weidp
	 * @description 获取JsonStore的fields
	 * @param fileDetailListWebBean
	 */
	private String getStoresByFileDetailList(FileDetailListWebBean fileDetailListWebBean) {
		StringBuffer storesBuf = new StringBuffer();
		storesBuf.append("[");
		// 文件级别
		for (int i = 0; i < fileDetailListWebBean.getMaxLevel(); i++) {
			if (fileDetailListWebBean.getCurLevel() <= i) {
				storesBuf.append(JecnCommon.storesJson(i + "level", i + "fileName") + ",");
			}
		}
		storesBuf.append(JecnCommon.storesJson("fileName", "fileName") + ",");
		storesBuf.append(JecnCommon.storesJson("fileNumber", "fileNumber") + ",");
		storesBuf.append(JecnCommon.storesJson("responsibilityDepartment", "responsibilityDepartment") + ",");
		storesBuf.append(JecnCommon.storesJson("creator", "creator") + ",");
		storesBuf.append(JecnCommon.storesJson("createTime", "createTime") + ",");
		storesBuf.append(JecnCommon.storesJson("updateTime", "updateTime") + ",");
		storesBuf.append(JecnCommon.storesJson("fileDetailRelatedList", "fileDetailRelatedList") + "]");
		return storesBuf.toString();
	}

	/**
	 * @author yxw 2012-12-4
	 * @description:文件树加载
	 * @return
	 */
	public String getChildFiles() {
		try {

			// 需要添加三个参数
			List<JecnTreeBean> list = fileService.getChildFilesWeb(node, getProjectId(), getPeopleId(), isAdmin());
			if (list != null && list.size() > 0) {
				List<WebTreeBean> listWebTreeBean = new ArrayList<WebTreeBean>();
				for (JecnTreeBean jecnTreeBean : list) {
					WebTreeBean webTreeBean = new WebTreeBean();
					webTreeBean.setId(jecnTreeBean.getId().toString());
					webTreeBean.setText(jecnTreeBean.getName());
					webTreeBean.setLeaf(!jecnTreeBean.isChildNode());
					webTreeBean.setType(jecnTreeBean.getTreeNodeType().toString());
					webTreeBean.setIcon("images/treeNodeImages/" + jecnTreeBean.getTreeNodeType().toString() + ".gif");
					listWebTreeBean.add(webTreeBean);

				}
				JSONArray jsonArray = JSONArray.fromObject(listWebTreeBean);
				outJsonString(jsonArray.toString());
			}
		} catch (Exception e) {
			log.error("浏览端文件树加载报错", e);
		}
		return null;
	}

	/**
	 * @author yxw 2012-12-4
	 * @description:文件搜索
	 * @return
	 */
	public String getFileList() {

		FileSearchBean fileSearchBean = new FileSearchBean();
		try {
			BeanUtils.copyProperties(fileSearchBean, this);
			fileSearchBean.setAdmin(isAdmin());
			fileSearchBean.setPeopleId(getPeopleId());
			fileSearchBean.setProjectId(getProjectId());
			int total = fileService.getFileTotal(fileSearchBean);
			List<FileWebBean> list = fileService.getFileList(fileSearchBean, start, limit);
			JSONArray jsonArray = JSONArray.fromObject(list);
			outJsonPage(jsonArray.toString(), total);
		} catch (Exception e) {
			log.error("文件搜索报错", e);
		}
		return null;
	}

	/**
	 * @author yxw 2012-12-22
	 * @description:公共文件
	 * @return
	 */
	public String getPublicFileList() {
		PublicSearchBean publicSearchBean = new PublicSearchBean();
		publicSearchBean.setName(fileName);
		publicSearchBean.setNumber(fileNum);
		publicSearchBean.setOrgId(orgId);
		publicSearchBean.setOrgName(orgName);
		publicSearchBean.setSecretLevel(secretLevel);

		try {
			int total = 0;
			List<PublicFileBean> list = new ArrayList<PublicFileBean>();
			if (type == 0) {// 0是流程
				// 流程
				total = webProcessService.getTotalPublicProcess(publicSearchBean, getProjectId());
				if (total > 0) {
					list = webProcessService.getPublicProcess(publicSearchBean, start, limit, getProjectId());
				}

			} else if (type == 1) {// 1是文件
				total = fileService.getTotalPublicFile(publicSearchBean, getProjectId());
				if (total > 0) {
					list = fileService.getPublicFile(publicSearchBean, start, limit, getProjectId());
				}

			} else if (type == 2) {// 2是标准
				total = standardService.getTotalPublicStandard(publicSearchBean, getProjectId());
				if (total > 0) {
					list = standardService.getPublicStandard(publicSearchBean, start, limit, getProjectId());
				}

			} else if (type == 3) {// 3是制度
				total = ruleService.getTotalPublicRule(publicSearchBean, getProjectId());
				if (total > 0) {
					list = ruleService.getPublicRule(publicSearchBean, start, limit, getProjectId());
				}

			} else if (type == -1) {// TODO 全部 ,四组数据,5个条件,union合并数据 过于繁琐
				int process = webProcessService.getTotalPublicProcess(publicSearchBean, getProjectId()); // 流程
				int file = fileService.getTotalPublicFile(publicSearchBean, getProjectId()); // 文件
				int standard = standardService.getTotalPublicStandard(publicSearchBean, getProjectId()); // 标准
				int rule = ruleService.getTotalPublicRule(publicSearchBean, getProjectId()); // 制度

				List<PublicFileBean> processList = webProcessService.getPublicProcess(publicSearchBean, 0, process,
						getProjectId());
				List<PublicFileBean> fileList = fileService.getPublicFile(publicSearchBean, 0, file, getProjectId());
				List<PublicFileBean> standardList = standardService.getPublicStandard(publicSearchBean, 0, standard,
						getProjectId());
				List<PublicFileBean> ruleList = ruleService.getPublicRule(publicSearchBean, 0, rule, getProjectId());

				total = process + file + standard + rule;// 全部的文件数

				List<PublicFileBean> listPage = new ArrayList<PublicFileBean>();

				if (total > 0) { // 把所有的数据写一个集合，根据start,limit从全部的集合中取数据
					list.addAll(processList);
					list.addAll(fileList);
					list.addAll(standardList);
					list.addAll(ruleList);
					limit = (start + limit > total) ? total : (start + limit);// limit
					// =
					// start
					// +limit;
					for (int i = start; i < limit; i++) {
						if (null != list.get(i)) {
							listPage.add(list.get(i));
						} else {
							break;
						}
					}
				}
				list = listPage;// 重新赋值,方便通过list输出
			}
			JSONArray jsonArray = JSONArray.fromObject(list);
			outJsonPage(jsonArray.toString(), total);

		} catch (Exception e) {
			log.error("公共文件搜索报错", e);
		}
		return null;
	}

	public void setStandardService(IStandardService standardService) {
		this.standardService = standardService;
	}

	public void setRuleService(IRuleService ruleService) {
		this.ruleService = ruleService;
	}

	/**
	 * 文件基本信息和使用情况
	 * 
	 * @author fuzhh Dec 24, 2012
	 * @return
	 */
	public String findFileInformation() {
		try {
			boolean isp = true;
			if ("false".equals(isPub)) {
				isp = false;
			}
			fileBean = fileService.findFileBean(fileId, isp);
			if (fileBean != null) {
				SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
				String createDateString = formatter.format(fileBean.getCreateTime());
				fileBean.setCreateStrTime(createDateString);

				String updateDateString = formatter.format(fileBean.getUpdateTime());
				fileBean.setUpdateStrTime(updateDateString);
			}
		} catch (Exception e) {
			log.error("查询文件基本信息和使用情况异常！！！", e);
		}
		return SUCCESS;
	}

	public String attenTionFile() {
		AttenTionSearchBean attenTionSearchBean = new AttenTionSearchBean();
		attenTionSearchBean.setName(fileName);
		attenTionSearchBean.setNumber(fileNum);
		attenTionSearchBean.setOrgId(posLookId);
		attenTionSearchBean.setSecretLevel(secretLevel);
		attenTionSearchBean.setUserId(getPeopleId());
		attenTionSearchBean.setAdmin(isAdmin());
		attenTionSearchBean.setProjectId(getProjectId());
		try {
			int total = fileService.getTotalAttenTionFile(attenTionSearchBean);
			List<FileWebBean> list = fileService.getAttenTionFile(attenTionSearchBean, start, limit);
			JSONArray jsonArray = JSONArray.fromObject(list);
			outJsonPage(jsonArray.toString(), total);

		} catch (Exception e) {
			log.error("查询我关注文件报错！", e);
		}

		return null;
	}

	/**
	 * 文件下载
	 * 
	 * @author fuzhh Jan 21, 2013
	 * @return
	 */
	public String downloadFile() {
		// 解决弹出下载界面标题乱码问题。
		String oldEncod = getResponse().getCharacterEncoding();
		// 先将编码内容设为自己的编码格式
		getResponse().setCharacterEncoding("utf-8");
		try {
			// 初始化文件下载或在线预览所需参数
			initDownAndViewData();
			log.info("文件开始预览或者下载");
			isApp = this.getRequest().getParameter("isApp");
			isDownLoad = this.getRequest().getParameter("isDownLoad") == null ? "false" : this.getRequest()
					.getParameter("isDownLoad");
			getSession().setAttribute("isDowLoad", isDownLoad);
			// 判断是否支持在线预览 true:支持
			boolean viewIsSuport = isAllowedView();

			String result = "";
			log.info("viewIsSuport " + viewIsSuport);
			if ("true".equals(isApp) && isViewApp()) {
				result = viewOnlineAndResult();
				log.info("APP在线预览");
			} else if (viewIsSuport && !"true".equals(isApp)) {// 在线预览
				result = viewOnlineAndResult();
				log.info("在线预览");
			} else {
				result = download();
				log.info("本地下载");
			}
			return result;
		} catch (Exception e) {
			log.error("", e);
			return "fileAbnormal";
		} finally {
			// 将编码设置为浏览器识别的编码格式，防止出现弹出界面乱码
			getResponse().setCharacterEncoding(oldEncod);
		}
	}

	private boolean isAllowedView() throws Exception {
		return ViewDocsManager.enabledOnlineView() && isAllowedBrowser()
				&& ViewDocsManager.isAllowedExtension(downFileName);
	}

	private boolean isViewApp() {
		return ViewDocsManager.isAllowedExtension(downFileName);
	}

	/**
	 * 现在文件以及制度文件
	 * 
	 * @return
	 */
	public String downloadFileAndRuleFile() {
		String result = "";
		// 解决弹出下载界面标题乱码问题。
		String oldEncod = getResponse().getCharacterEncoding();
		// 先将编码内容设为自己的编码格式
		getResponse().setCharacterEncoding("utf-8");
		// 初始化文件下载所需参数
		try {
			initDownAndViewData();
			result = download();
			return result;
		} catch (Exception e) {
			log.error("", e);
			return "fileAbnormal";
		} finally {
			// 将编码设置为浏览器识别的编码格式，防止出现弹出界面乱码
			getResponse().setCharacterEncoding(oldEncod);
		}
	}

	private String download() {
		downLoadCountMax();
		if (isDownLoadMax) {// 判断下载次数
			return NONE;
		}
		// 下载文件获得字节流
		String result = downloadFileAndResult();
		// 获得当前登录信息
		WebLoginBean webLoginBean = (WebLoginBean) ActionContext.getContext().getSession().get("webLoginBean");
		if (!webLoginBean.isAdmin()) {
			// 下载次数统计
			downLoadCountLimit();
		}
		return result;
	}

	private String downloadFileAndResult() {
		// 解决弹出下载界面标题乱码问题。
		String oldEncod = getResponse().getCharacterEncoding();
		// 先将编码内容设为自己的编码格式
		getResponse().setCharacterEncoding("utf-8");
		String result = null;
		try {
			FileDownload fileDownload = getFileDownload();
			fileDownload.createDownloadFile();
			result = fileDownload.getResultPage();
			downFileByte = fileDownload.getDownFileByte();
			if (downFileByte == null) {
				throw new NullPointerException("download() 下载的字节数组为null");
			}
		} catch (Exception e) {
			log.error("下载文件异常！", e);
			this.setErrorInfo(this.getText("theDownloadFileError"));
			return "fileAbnormal";
		} finally {
			// 将编码设置为浏览器识别的编码格式，防止出现弹出界面乱码
			getResponse().setCharacterEncoding(oldEncod);
		}
		return result;
	}

	private FileDownload getFileDownload() {
		// Map<String, Object> param = new HashMap<String, Object>();
		// param.put("ruleService", ruleService);
		// param.put("standardService", standardService);
		// param.put("fileService", fileService);
		// param.put("flowStructureService", flowStructureService);
		// param.put("webProcessService", webProcessService);
		// param.put("viewType", viewType);
		// param.put("isRuleFileLocal", ruleFileFromLocalIsDownload());
		// param.put("fileId", fileId);
		// param.put("isPub", (StringUtils.isBlank(isPub) ||
		// "true".equals(isPub)) ? true : false);
		// param.put("historyId", historyId);
		// param.put("peopleId", peopleId);
		FileDownload fileDownload = new FileDownload(getViewInfo());
		return fileDownload;
	}

	private boolean ruleFileFromLocalIsDownload() {
		String downFileType = this.getRequest().getParameter("downFileType");
		// 默认是文件
		DownFileType fileType = getFileType(downFileType);
		return fileType.equals(DownFileType.ruleLocal);
	}

	enum DownFileType {
		file, // 文件
		ruleLocal, // 制度本地上传
		ruleWord, // 制度模板
		processFile,
		// 流程（文件本地上传）
		standardFile
		// 标准附件（本地上传）
		, processWord// 流程文件（dowd）
		, processImage
		// 流程图片
	}

	private String viewDocPath;

	private String viewOnlineAndResult() {
		View view = null;
		ViewInfo viewInfo = getViewInfo();
		if (JecnConfigTool.isLiBangLoginType()) {
			view = new PageofficeView(viewInfo);
			viewDocPath = view.getPath();
			return "pageoffice";
		} else {
			view = new CommonView(viewInfo);
			viewDocPath = view.getPath();
			return "online";
		}
	}

	private ViewInfo getViewInfo() {
		ViewInfo viewInfo = new ViewInfo();
		viewInfo.setFileService(fileService);
		viewInfo.setRuleService(ruleService);
		viewInfo.setStandardService(standardService);
		viewInfo.setFlowStructureService(flowStructureService);
		viewInfo.setType(viewType);
		viewInfo.setPub("false".equals(isPub) ? false : true);
		viewInfo.setRelatedId(fileId);
		viewInfo.setViewDocService(viewDocService);
		viewInfo.setApp("true".equals(isApp));
		viewInfo.setRequest(this.getRequest());
		viewInfo.setResponse(this.getResponse());
		viewInfo.setRelatedName(downFileName);
		viewInfo.setWebProcessService(webProcessService);
		viewInfo.sethId(historyId);
		viewInfo.setUserId(peopleId);
		viewInfo.setRuleFileLocal(ruleFileFromLocalIsDownload());
		return viewInfo;
	}

	/**
	 * 初始化文件下载或在线预览所需参数
	 * 
	 * @throws Exception
	 */
	private void initDownAndViewData() throws Exception {

		if ("false".equals(isPub)) {
			isp = false;
		} else {
			isp = true;
			isPub = "true";
		}

		String downFileType = this.getRequest().getParameter("downFileType");
		// 默认是文件
		DownFileType fileType = getFileType(downFileType);
		// 在线预览分类
		viewType = getViewTypeByFileType(fileType, isp);

		/*
		 * // 文控版本信息 isHistory = this.getRequest().getParameter("isHistory"); if
		 * ("true".equals(isHistory)) { initHistoryFileName(fileType); } else {
		 * // 初始化文件名称 initFileName(fileType, isp); }
		 */

		initFileName(fileType);

	}

	private DownFileType getFileType(String fileType) {
		if (StringUtils.isEmpty(fileType)) {
			return DownFileType.file;
		} else {
			return DownFileType.valueOf(fileType);
		}
	}

	// private void initFileName(DownFileType fileType, boolean isp) throws
	// Exception {
	// if (DownFileType.ruleLocal.equals(fileType)) {// 制度本地上传
	// if (isp) {// 发布
	// Rule rule = this.ruleService.getRule(fileId);
	// downFileName = rule.getRuleName();
	// } else {
	// RuleT ruleT = this.ruleService.get(fileId);
	// downFileName = ruleT.getRuleName();
	// }
	// } else if (DownFileType.processFile.equals(fileType)) {// 流程（文件）本地上传
	// Object[] obj = this.flowStructureService.getFlowFileContentName(fileId,
	// isp);
	// downFileName = obj[0].toString();
	// } else if (DownFileType.standardFile.equals(fileType)) {// 标准附件
	// FileOpenBean fileOpenBean = standardService.openFileContent(fileId);
	// downFileName = fileOpenBean.getName();
	// } else {
	// if (isp) {// 发布
	// JecnFileBean fileBean = this.fileService.findJecnFileBeanById(fileId);
	// downFileName = fileBean.getFileName();
	// } else {
	// JecnFileBeanT fileBeanT = this.fileService.get(fileId);
	// downFileName = fileBeanT.getFileName();
	// }
	// }
	// }

	/**
	 * 
	 * @param fileType
	 * @throws Exception
	 */
	private void initFileName(DownFileType fileType) throws Exception {
		switch (fileType) {
		case ruleLocal:
			G020RuleFileContent fileContent = ruleService.getG020RuleFileContent(fileId);
			downFileName = fileContent.getFileName();
			break;
		case ruleWord:
			if ("false".equals(isPub)) {
				RuleT ruleT = ruleService.get(fileId);
				downFileName = ruleT.getRuleName();
			} else {
				Rule rule = ruleService.getRule(fileId);
				downFileName = rule.getRuleName();
			}
			downFileName += ".doc";
			break;
		case processFile:
			FlowFileContent processFileContent = flowStructureService.getFlowFileContent(fileId);
			downFileName = processFileContent.getFileName();
			break;
		case standardFile:
			FileOpenBean fileOpenBean = standardService.openFileContent(fileId);
			downFileName = fileOpenBean.getName();
			break;
		case processWord:
		case processImage:
			if ("false".equals(isPub)) {
				JecnFlowStructureT flowStructureT = flowStructureService.get(fileId);
				downFileName = flowStructureT.getFlowName();
			} else {
				JecnFlowStructure flowStructureT = flowStructureService.findJecnFlowStructureById(fileId);
				downFileName = flowStructureT.getFlowName();
			}
			downFileName += DownFileType.processImage == fileType ? ".jpg" : ".doc";
			break;
		default:
			fileOpenBean = fileService.openVersion(null, fileId);
			downFileName = fileOpenBean.getName();
			break;
		}
	}

	private int getViewTypeByFileType(DownFileType fileType, boolean isp) {
		if (DownFileType.ruleLocal.equals(fileType)) {
			return isp == true ? 2 : 3;
		} else if (DownFileType.processFile.equals(fileType)) {
			return isp == true ? 4 : 5;
		} else if (DownFileType.standardFile.equals(fileType)) {
			return 6;
		} else if (DownFileType.processWord.equals(fileType)) {
			return isp == true ? 9 : 10;
		} else if (DownFileType.processImage.equals(fileType)) {
			return isp == true ? 7 : 8;
		} else if (DownFileType.ruleWord.equals(fileType)) {
			return isp == true ? 11 : 12;
		}
		// 默认是文件
		return isp == true ? 1 : 0;
	}

	/**
	 * 判断当前登录用户文件下载是否达到最大限制
	 * 
	 * @author CHB
	 * 
	 * */
	public void downLoadCountMax() {
		isDownLoadMax = false;

		// 获得当前登录信息
		WebLoginBean webLoginBean = (WebLoginBean) ActionContext.getContext().getSession().get("webLoginBean");
		// 解决弹出下载界面标题乱码问题。
		String oldEncod = getResponse().getCharacterEncoding();
		// 先将编码内容设为自己的编码格式
		getResponse().setCharacterEncoding("utf-8");
		// 判断是否管理员
		if (webLoginBean.isAdmin()) {
			return;
		}
		PrintWriter out = null;
		try {
			// 登陆人员ID
			peopleId = webLoginBean.getJecnUser().getPeopleId();
			// 判断是否启用下载限制 1：启用 0:不启用
			if (JecnContants.downLoadCountIsLimited) {
				if (downLoadCountService != null) {
					// 判断当前登录人的下载次数是否达到最大
					downLoadNum = downLoadCountService.downLoadNum(peopleId, JecnContants.downLoadCountValue);
					if (!downLoadNum) { // 已经达到最大下载次数
						out = getResponse().getWriter();
						String info = JecnUtil.getValue("downLoadCountMax");
						StringBuffer sb = new StringBuffer();
						sb
								.append(
										"<html><head><meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\">")
								.append("<title></title>").append("</head>")
								.append("<script type=\"text/javascript\">").append("alert('" + info + "');").append(
										"window.close();").append("</script></html>");
						out.println(sb.toString());
						out.print("downLoadMax");
						out.flush();
						out.close(); // 解决弹出下载界面，点击取消按钮报错问题
						out = null;
						isDownLoadMax = true;
					}
				}
			}
		} catch (Exception e) {
			log.error("判断下载次数是否达到最大值时出现出现错误！错误方法：downLoadCountMax()", e);
		} finally {
			if (out != null) {// 系统异常时候
				out.close();
			}
			// 将编码设置为浏览器识别的编码格式，防止出现弹出界面乱码
			getResponse().setCharacterEncoding(oldEncod);
		}
	}

	/***
	 * 文件下载统计
	 * 
	 * @author CHB
	 * 
	 * */
	private void downLoadCountLimit() {
		try {
			// 获得当前登录信息
			WebLoginBean webLoginBean = (WebLoginBean) ActionContext.getContext().getSession().get("webLoginBean");
			// 登陆人员ID
			peopleId = webLoginBean.getJecnUser().getPeopleId();
			// 判断是否启用下载限制 1：启用 0:不启用
			if (JecnContants.downLoadCountIsLimited) {
				if (downLoadCountService != null) {
					// 记录下载次数
					downLoadCountService.downLoadCount(peopleId, JecnContants.downLoadCountValue);
				}
			}
		} catch (Exception e) {
			log.error("记录下载次数出现错误！错误方法：downLoadCountLimit()", e);
		}
	}

	/**
	 * @author yxw 2014年3月19日
	 * @description:验证权限 用于applet
	 */
	public void filePurview() {
		WebLoginBean webLoginBean = (WebLoginBean) ActionContext.getContext().getSession()
				.get(JecnContants.SESSION_KEY);
		// 系统管理员和浏览管理员
		if (webLoginBean.isAdmin() || webLoginBean.isViewAdmin()) {
			outString("true");
		}
		try {
			if (jecnRoleService.isViewAuth(webLoginBean, "file", getRequest())) {
				outString("true");
			} else {
				outString("false");
			}
		} catch (Exception e) {
			log.error("applet（输入输出操作规范）下载文件验证权限", e);
		}

	}

	/**
	 * @author yxw 2013-3-27
	 * @description:
	 * @return
	 */
	public String file() {
		if ("downFile".equals(visitType)) {
			return "downFile";
		} else if ("infoFile".equals(visitType)) {
			return "infoFile";
		}
		return null;
	}

	/**
	 * 获得下载文件的内容,可以直接读入一个物理文件或从数据库中获取内容
	 * 
	 * @return
	 * @throws Exception
	 */
	public InputStream getInputStream() throws Exception {
		if (downFileByte == null) {
			throw new IllegalArgumentException("下载文件异常，downFileByte为空");
		}
		return new ByteArrayInputStream(downFileByte);
	}

	/**
	 * 获取文件清单下载内容
	 * 
	 * @return
	 * @throws Exception
	 */
	public InputStream getFileDetailInputStream() throws Exception {
		if (fileDetailListContent == null) {
			throw new IllegalArgumentException("getInputStream方法中参数为空!");
		}
		return new ByteArrayInputStream(fileDetailListContent);
	}

	/**
	 * 输出文件。。。
	 * 
	 * @author
	 */
	public void writefileByte() {
		try {
			boolean isp = true;
			if ("false".equals(isPub)) {
				isp = false;
			}
			fileOpenBean = fileService.openVersion(null, fileId);
			if (fileOpenBean != null && fileOpenBean.getFileByte() != null) {
				getResponse().getOutputStream().write(fileOpenBean.getFileByte());
			} else {
				log.info("文件或图片不存在");
			}

		} catch (Exception e) {
			log.error("输出文件异常！！！", e);
		}

	}

	public String fileListByPid() {
		try {

			int total = fileService.getFileTotalByPid(fileId, getProjectId(), getPeopleId(), isAdmin());
			if (total > 0) {
				List<FileWebBean> listFileInfoBean = fileService.getFileListByPid(fileId, getProjectId(),
						getPeopleId(), isAdmin(), start, limit);
				JSONArray jsonArray = JSONArray.fromObject(listFileInfoBean);
				outJsonPage(jsonArray.toString(), total);
			} else {
				outJsonPage("[]", total);
			}

		} catch (Exception e) {
			log.error("双击目录节点查询文件列表错误！", e);
		}
		return null;
	}

	public String findHistoryNew() {

		try {
			taskHistoryNewList = fileService.getTaskHistoryNew(fileId);
		} catch (Exception e) {
			log.error("获取文控信息错误！", e);
		}
		return SUCCESS;
	}

	/**
	 * @description:文控信息
	 * @return
	 */
	public String findFileHistoryNewDetail() {
		try {
			taskHistoryNew = fileService.getJecnTaskHistoryNew(historyId);
			// 将变更说明的换行改为br,使得可以在页面换行
			if (!JecnCommon.isNullOrEmtryTrim(taskHistoryNew.getModifyExplain())) {
				taskHistoryNew.setModifyExplain(taskHistoryNew.getModifyExplain().replaceAll("\n", "<br/>"));
			}
			if (taskHistoryNew != null && taskHistoryNew.getListJecnTaskHistoryFollow() != null) {
				for (JecnTaskHistoryFollow taskHistoryFollow : taskHistoryNew.getListJecnTaskHistoryFollow()) {
					if (taskHistoryFollow.getAppellation() != null
							&& !"".equals(taskHistoryFollow.getAppellation().trim())
							&& !taskHistoryFollow.getAppellation().endsWith("：")) {
						taskHistoryFollow.setAppellation(taskHistoryFollow.getAppellation() + "：");
					}
				}
			}
		} catch (Exception e) {
			log.error("查询文控详细信息错误！", e);
		}
		return SUCCESS;
	}

	public long getFileId() {
		return fileId;
	}

	public void setFileId(long fileId) {
		this.fileId = fileId;
	}

	public long getNode() {
		return node;
	}

	public void setNode(long node) {
		this.node = node;
	}

	public IFileService getFileService() {
		return fileService;
	}

	public void setFileService(IFileService fileService) {
		this.fileService = fileService;
	}

	public int getStart() {
		return start;
	}

	public void setStart(int start) {
		this.start = start;
	}

	public int getLimit() {
		return limit;
	}

	public void setLimit(int limit) {
		this.limit = limit;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getFileNum() {
		return fileNum;
	}

	public void setFileNum(String fileNum) {
		this.fileNum = fileNum;
	}

	public long getOrgId() {
		return orgId;
	}

	public void setOrgId(long orgId) {
		this.orgId = orgId;
	}

	public String getOrgName() {
		return orgName;
	}

	public void setOrgName(String orgName) {
		this.orgName = orgName;
	}

	public long getOrgLookId() {
		return orgLookId;
	}

	public void setOrgLookId(long orgLookId) {
		this.orgLookId = orgLookId;
	}

	public String getOrgLookName() {
		return orgLookName;
	}

	public void setOrgLookName(String orgLookName) {
		this.orgLookName = orgLookName;
	}

	public long getPosLookId() {
		return posLookId;
	}

	public void setPosLookId(long posLookId) {
		this.posLookId = posLookId;
	}

	public String getPosLookName() {
		return posLookName;
	}

	public void setPosLookName(String posLookName) {
		this.posLookName = posLookName;
	}

	public int getSecretLevel() {
		return secretLevel;
	}

	public void setSecretLevel(int secretLevel) {
		this.secretLevel = secretLevel;
	}

	public IWebProcessService getWebProcessService() {
		return webProcessService;
	}

	public void setWebProcessService(IWebProcessService webProcessService) {
		this.webProcessService = webProcessService;
	}

	public List<FileUseBean> getFileUseList() {
		return fileUseList;
	}

	public void setFileUseList(List<FileUseBean> fileUseList) {
		this.fileUseList = fileUseList;
	}

	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}

	public FileBean getFileBean() {
		return fileBean;
	}

	public void setFileBean(FileBean fileBean) {
		this.fileBean = fileBean;
	}

	public String getDownFileName() {
		return JecnCommon.getDownFileNameByEncoding(getResponse(), downFileName, isApp);
	}

	public String getIsApp() {
		return isApp;
	}

	public void setIsApp(String isApp) {
		this.isApp = isApp;
	}

	public void setDownFileName(String downFileName) {
		this.downFileName = downFileName;
	}

	public String getIsPub() {
		return isPub;
	}

	public void setIsPub(String isPub) {
		this.isPub = isPub;
	}

	public String getVisitType() {
		return visitType;
	}

	public void setVisitType(String visitType) {
		this.visitType = visitType;
	}

	public int getCallType() {
		return callType;
	}

	public void setCallType(int callType) {
		this.callType = callType;
	}

	public void setJecnRoleService(IJecnRoleService jecnRoleService) {
		this.jecnRoleService = jecnRoleService;
	}

	public boolean isShowAll() {
		return showAll;
	}

	public void setShowAll(boolean showAll) {
		this.showAll = showAll;
	}

	public String getFileDetailListName() {
		return JecnCommon.getDownFileNameByEncoding(getResponse(), fileDetailListName, isApp);
	}

	public List<JecnTaskHistoryNew> getTaskHistoryNewList() {
		return taskHistoryNewList;
	}

	public Long getHistoryId() {
		return historyId;
	}

	public void setHistoryId(Long historyId) {
		this.historyId = historyId;
	}

	public JecnTaskHistoryNew getTaskHistoryNew() {
		return taskHistoryNew;
	}

	public void setTaskHistoryNew(JecnTaskHistoryNew taskHistoryNew) {
		this.taskHistoryNew = taskHistoryNew;
	}

	public int getViewType() {
		return viewType;
	}

	public void setViewType(int viewType) {
		this.viewType = viewType;
	}

	public String getDownFileType() {
		return downFileType;
	}

	public void setDownFileType(String downFileType) {
		this.downFileType = downFileType;
	}

	public IDownLoadCountService getDownLoadCountService() {
		return downLoadCountService;
	}

	public void setDownLoadCountService(IDownLoadCountService downLoadCountService) {
		this.downLoadCountService = downLoadCountService;
	}

	public String getIsHistory() {
		return isHistory;
	}

	public void setIsHistory(String isHistory) {
		this.isHistory = isHistory;
	}

	public String getViewDocPath() {
		return viewDocPath;
	}

	/**
	 * 是否允许在线预览
	 * 
	 * @return
	 */
	private boolean isAllowedBrowser() throws Exception {
		if (JecnContants.otherLoginType == 38) {
			return true;
		}
		String agent = this.getRequest().getHeader("User-Agent").toLowerCase();
		// System.out.println(agent);
		BrowserName browserName = getBrowserName(agent);
		log.info("浏览器版本：browserName " + browserName);
		switch (browserName) {
		case ie11:
		case chrome:
		case firefox:
			return true;
		case Others:
			return false;
		default:
			return false;
		}
	}

	/**
	 * 获取浏览器版本信息
	 * 
	 * @Title: getBrowserName
	 * @param agent
	 * @return BrowserName
	 */
	public BrowserName getBrowserName(String agent) {
		if (agent.indexOf("msie 9") > 0) {
			return BrowserName.ie9;
		} else if (agent.indexOf("msie 10") > 0) {
			return BrowserName.ie10;
		} else if (agent.indexOf("gecko") > 0 && agent.indexOf("rv:11") > 0) {
			return BrowserName.ie11;
		} else if (agent.indexOf("chrome") > 0) {
			return BrowserName.chrome;
		} else if (agent.indexOf("firefox") > 0) {
			return BrowserName.firefox;
		} else {
			return BrowserName.Others;
		}
	}

}
