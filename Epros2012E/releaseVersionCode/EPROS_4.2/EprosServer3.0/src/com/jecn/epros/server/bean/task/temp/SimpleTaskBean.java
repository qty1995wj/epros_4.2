package com.jecn.epros.server.bean.task.temp;

import java.util.ArrayList;
import java.util.List;

import com.jecn.epros.server.bean.system.ProceeRuleTypeBean;
import com.jecn.epros.server.bean.task.JecnTaskApplicationNew;
import com.jecn.epros.server.bean.task.JecnTaskBeanNew;
import com.jecn.epros.server.bean.task.JecnTaskForRecodeNew;
import com.jecn.epros.server.common.JecnConfigTool;
import com.jecn.epros.server.util.JecnUtil;

/**
 * 任务信息
 * 
 * @author xiaohu
 * 
 */
public class SimpleTaskBean {
	/** 任务基本属性 */
	private JecnTaskBeanNew jecnTaskBeanNew;
	/** 当前任务状态名称 */
	private String stageName;
	/** 显示项 */
	private JecnTaskApplicationNew jecnTaskApplicationNew;
	/** 试运行 文件 */
	private String taskRunFileId;
	/** 试运行 文件 */
	private String taskRunFileName;
	/** 电话号码 */
	private String phone;
	/** 创建人userId */
	private String createUserId;
	/** 1：打回整理，0不显示打回整理 */
	private String callBack;
	/** 任务说明 */
	private String taskDescStr;
	/** 审批顺序 */
	private List<String> orderApproverList;
	/** 获取文控主导审批的审批人（文控审核人之后的审批人顺序） */
	private List<String> orderControlList;
	/** 重新提交审批顺序 */
	private String orderString;
	/** 上传文件 */
	private List<JecnTaskFileTemporary> uploadFileList;
	/** 更新文件 */
	private JecnTaskFileTemporary modifyFile;
	/** 提交人员名称信息 人员信息 */
	private List<TempAuditPeopleBean> listAuditPeopleBean;
	/** 页面显示人员名称信息 人员信息 */
	private List<TempAuditPeopleBean> listAudit;
	/** 页面显示任务走向 各阶段审批 */
	private List<TempAuditPeopleBean> listTaskViewAudit;
	/** 更新文件存储文件数据 Bean */
	private SimpleFileMassageBean fileMassageBean;
	/** 任务审批记录 */
	private List<JecnTaskForRecodeNew> listJecnTaskForRecodeNew;
	/** ******************提交审批文件 相关信息********************************** */
	/** PRF节点名称 */
	private String prfName;
	/** 父节节点名称 */
	private String prfPreName;
	/** 编号 */
	private String prfNumber;
	/** 类别 编号 */
	private String typeNumber;

	/** 术语定义 */
	private String definitionStr;
	/** PRF类别 */
	private List<ProceeRuleTypeBean> listTypeBean;
	/** PRF当前类别 */
	private String prfType;
	/** PRF当前类别prfTypeName */
	private String prfTypeName;
	/** ******************提交审批文件 相关信息********************************** */
	/** ********************* 权限 ********************************** */
	/** 密级 */
	private String strPublic;
	/** 查阅权限组织ID集合 ','分隔 */
	private String orgIds;
	/** 查阅权限岗位ID集合 ','分隔 */
	private String posIds;
	/** 查阅权限相关部门名称‘/’分离 */
	private String orgNames;
	/** 查阅权限相关岗位名称‘/’分离 */
	private String posNames;

	/** 任务主键ID */
	private String taskId = null;
	/** 显示的部门查阅权限名称 */
	private String tableOrgNames;
	/** 显示的岗位查阅权限名称 */
	private String tablePosNames;
	/** 查阅权限组岗位ID集合 ','分隔 */
	private String groupIds;
	/** 查阅权限组相关部门名称‘/’分离 */
	private String groupNames;
	/** 显示的岗位组查阅权限名称 */
	private String tableGroupNames;

	/** ****************二次评审，默认显示评审人*************************************** */
	/** 评审人 */
	private String reviewIds;
	/** 评审人名称 */
	private String reviewNames;

	/** 任务操作配置 */
	private TmpTaskOperationBean taskOperationBean;

	/** ********************* 权限 ********************************** */

	/** 保存是否存在错误信息 0:错误 1:正确 */
	private int errorState = 1;

	/** 二次评审时，默认二次评审人员 */

	/** 是否搁置 true搁置 */
	private boolean isDelay;
	/** 搁置原因 */
	private String delayReason;
	/** true:显示转批操作 */
	private String showOperationTransfer;
	/** true:显示交办操作 */
	private String showOperationAssigned;

	public boolean isDelay() {
		return isDelay;
	}

	public void setDelay(boolean isDelay) {
		this.isDelay = isDelay;
	}

	public String getDelayReason() {
		return delayReason;
	}

	public void setDelayReason(String delayReason) {
		this.delayReason = delayReason;
	}

	public JecnTaskBeanNew getJecnTaskBeanNew() {
		return jecnTaskBeanNew;
	}

	public void setJecnTaskBeanNew(JecnTaskBeanNew jecnTaskBeanNew) {
		this.jecnTaskBeanNew = jecnTaskBeanNew;
	}

	public JecnTaskApplicationNew getJecnTaskApplicationNew() {
		return jecnTaskApplicationNew;
	}

	public void setJecnTaskApplicationNew(JecnTaskApplicationNew jecnTaskApplicationNew) {
		this.jecnTaskApplicationNew = jecnTaskApplicationNew;
	}

	public String getPhone() {
		return phone;
	}

	public String getGroupIds() {
		return groupIds;
	}

	public void setGroupIds(String groupIds) {
		this.groupIds = groupIds;
	}

	public String getGroupNames() {
		return groupNames;
	}

	public void setGroupNames(String groupNames) {
		this.groupNames = groupNames;
	}

	public String getTableGroupNames() {
		this.tableGroupNames = rebackReplaceString(groupNames);
		return tableGroupNames;
	}

	public void setTableGroupNames(String tableGroupNames) {
		this.tableGroupNames = tableGroupNames;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public List<String> getOrderApproverList() {
		return orderApproverList;
	}

	public void setOrderApproverList(List<String> orderApproverList) {
		this.orderApproverList = orderApproverList;
	}

	public List<JecnTaskFileTemporary> getUploadFileList() {
		return uploadFileList;
	}

	public void setUploadFileList(List<JecnTaskFileTemporary> uploadFileList) {
		this.uploadFileList = uploadFileList;
	}

	public JecnTaskFileTemporary getModifyFile() {
		return modifyFile;
	}

	public void setModifyFile(JecnTaskFileTemporary modifyFile) {
		this.modifyFile = modifyFile;
	}

	public String getCallBack() {
		return callBack;
	}

	public void setCallBack(String callBack) {
		this.callBack = callBack;
	}

	public List<String> getOrderControlList() {
		return orderControlList;
	}

	public void setOrderControlList(List<String> orderControlList) {
		this.orderControlList = orderControlList;
	}

	public String getCreateUserId() {
		return createUserId;
	}

	public void setCreateUserId(String createUserId) {
		this.createUserId = createUserId;
	}

	public String getTaskDescStr() {
		return taskDescStr;
	}

	public void setTaskDescStr(String taskDescStr) {
		this.taskDescStr = taskDescStr;
	}

	public String getDefinitionStr() {
		return definitionStr;
	}

	public void setDefinitionStr(String definitionStr) {
		this.definitionStr = definitionStr;
	}

	public String getOrderString() {
		return orderString;
	}

	public void setOrderString(String orderString) {
		this.orderString = orderString;
	}

	public SimpleFileMassageBean getFileMassageBean() {
		return fileMassageBean;
	}

	public void setFileMassageBean(SimpleFileMassageBean fileMassageBean) {
		this.fileMassageBean = fileMassageBean;
	}

	public String getTaskId() {
		return taskId;
	}

	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}

	public String getStrPublic() {
		return strPublic;
	}

	public void setStrPublic(String strPublic) {
		this.strPublic = strPublic;
	}

	public String getOrgIds() {
		return orgIds;
	}

	public void setOrgIds(String orgIds) {
		this.orgIds = orgIds;
	}

	public String getPosIds() {
		return posIds;
	}

	public void setPosIds(String posIds) {
		this.posIds = posIds;
	}

	public String getOrgNames() {
		return orgNames;
	}

	public void setOrgNames(String orgNames) {
		this.orgNames = orgNames;
	}

	public String getPosNames() {
		return posNames;
	}

	public void setPosNames(String posNames) {
		this.posNames = posNames;
	}

	public String getPrfName() {
		return prfName;
	}

	public void setPrfName(String prfName) {
		this.prfName = prfName;
	}

	public String getPrfPreName() {
		return prfPreName;
	}

	public void setPrfPreName(String prfPreName) {
		this.prfPreName = prfPreName;
	}

	public String getPrfNumber() {
		return prfNumber;
	}

	public void setPrfNumber(String prfNumber) {
		this.prfNumber = prfNumber;
	}

	public List<ProceeRuleTypeBean> getListTypeBean() {
		return listTypeBean;
	}

	public void setListTypeBean(List<ProceeRuleTypeBean> listTypeBean) {
		this.listTypeBean = listTypeBean;
	}

	public String getPrfType() {
		return prfType;
	}

	public void setPrfType(String prfType) {
		this.prfType = prfType;
	}

	public String getStageName() {
		return stageName;
	}

	public void setStageName(String stageName) {
		this.stageName = stageName;
	}

	public List<TempAuditPeopleBean> getListAuditPeopleBean() {
		return listAuditPeopleBean;
	}

	public void setListAuditPeopleBean(List<TempAuditPeopleBean> listAuditPeopleBean) {
		this.listAuditPeopleBean = listAuditPeopleBean;
	}

	public List<JecnTaskForRecodeNew> getListJecnTaskForRecodeNew() {
		return listJecnTaskForRecodeNew;
	}

	public void setListJecnTaskForRecodeNew(List<JecnTaskForRecodeNew> listJecnTaskForRecodeNew) {
		this.listJecnTaskForRecodeNew = listJecnTaskForRecodeNew;
	}

	public String getPrfTypeName() {
		return prfTypeName;
	}

	public void setPrfTypeName(String prfTypeName) {
		this.prfTypeName = prfTypeName;
	}

	public String getTaskRunFileId() {
		return taskRunFileId;
	}

	public void setTaskRunFileId(String taskRunFileId) {
		this.taskRunFileId = taskRunFileId;
	}

	public String getTaskRunFileName() {
		return taskRunFileName;
	}

	public void setTaskRunFileName(String taskRunFileName) {
		this.taskRunFileName = taskRunFileName;
	}

	public int getErrorState() {
		return errorState;
	}

	public void setErrorState(int errorState) {
		this.errorState = errorState;
	}

	/**
	 * 提交人员名称信息 人员信息
	 * 
	 * @param listAuditPeopleBean
	 */
	public List<TempAuditPeopleBean> getListAudit() {
		if (listAuditPeopleBean != null) {
			listAudit = new ArrayList<TempAuditPeopleBean>();
			for (TempAuditPeopleBean tempAuditPeopleBean : listAuditPeopleBean) {
				if (tempAuditPeopleBean.getIsShow() == 1 && tempAuditPeopleBean.getIsSelectedUser() == 1) {
					if (isNullOrEmtryTrim(tempAuditPeopleBean.getAuditName())) {// 获取真实姓名
						listAudit.add(tempAuditPeopleBean.clone());
						continue;
					}
					TempAuditPeopleBean auditPeopleBean = tempAuditPeopleBean.clone();
					List<String[]> tableAuditIdNames = new ArrayList<String[]>();
					if (auditPeopleBean.getAuditId() != null && auditPeopleBean.getAuditName() != null) {
						String[] auditIds = auditPeopleBean.getAuditId().split(",");
						String[] auditNames = auditPeopleBean.getAuditName().split("\n");
						for (int i = 0; i < auditIds.length; i++) {
							String[] idAndName = new String[2];
							idAndName[0] = auditIds[i];
							idAndName[1] = auditNames[i];
							tableAuditIdNames.add(idAndName);

						}
					}
					auditPeopleBean.setTableAuditIdNames(tableAuditIdNames);

					listAudit.add(auditPeopleBean);
				}
			}
		}
		return listAudit;
	}

	public void setListAudit(List<TempAuditPeopleBean> listAudit) {
		this.listAudit = listAudit;
	}

	public String getTableOrgNames() {
		tableOrgNames = rebackReplaceString(orgNames);
		return tableOrgNames;
	}

	public void setTableOrgNames(String tableOrgNames) {
		this.tableOrgNames = tableOrgNames;
	}

	public String getTablePosNames() {
		tablePosNames = rebackReplaceString(posNames);
		return tablePosNames;
	}

	public void setTablePosNames(String tablePosNames) {
		this.tablePosNames = tablePosNames;
	}

	/**
	 * 
	 * 给定参数先去空再判断是否为null或空
	 * 
	 * @param str
	 * @return
	 */
	private boolean isNullOrEmtryTrim(String str) {
		return (str == null || "".equals(str.trim())) ? true : false;
	}

	/**
	 * 换行标识符替换
	 * 
	 * @param str
	 *            “带\n 的字符串”
	 * @return String
	 */
	private String rebackReplaceString(String str) {
		if (isNullOrEmtryTrim(str)) {
			return str;
		}
		return str.replaceAll("\n", "<br>");
	}

	/**
	 * 
	 * 获取
	 * 
	 * @return
	 */
	public List<TempAuditPeopleBean> getListTaskViewAudit() {
		listTaskViewAudit = new ArrayList<TempAuditPeopleBean>();
		// 执行状态
		// int isExecute = 0;
		for (TempAuditPeopleBean tempAuditPeopleBean : listAuditPeopleBean) {
			listTaskViewAudit.add(tempAuditPeopleBean);
			// // listTempAudit
			// if (tempAuditPeopleBean.getState() == 3
			// && !JecnCommon.isNullOrEmtryTrim(tempAuditPeopleBean
			// .getAuditName())) {// 评审阶段 添加整理意见阶段
			// TempAuditPeopleBean auditPeopleBean = new TempAuditPeopleBean();
			// // 记录当前审核阶段对应任务的状态
			// auditPeopleBean.setState(10);
			// // 审核阶段名称
			// auditPeopleBean.setAuditLable(JecnTaskCommon.FINISHING_VIEWS);
			// auditPeopleBean.setAuditName(JecnTaskCommon.FINISHING_VIEWS);
			// if ("1".equals(callBack) || jecnTaskBeanNew.getState() == 5) {//
			// 存在打回整理或任务审批完成
			// isExecute = 1;
			// } else if (jecnTaskBeanNew.getState() == 10) {// 整理意见阶段正在执行
			// isExecute = 2;
			// }
			// // 执行过 是否审批 isExecute 2:为当前任务 1：已审批，0 未审批
			// auditPeopleBean.setIsApproval(isExecute);
			// auditPeopleBean.setIsSelectedUser(1);
			// auditPeopleBean.setStageId(tempAuditPeopleBean.getStageId());
			// // 添加集合
			// listTaskViewAudit.add(auditPeopleBean);
			// }
		}
		return listTaskViewAudit;
	}

	public String getReviewIds() {
		return reviewIds;
	}

	public void setReviewIds(String reviewIds) {
		this.reviewIds = reviewIds;
	}

	public String getReviewNames() {
		return reviewNames;
	}

	public void setReviewNames(String reviewNames) {
		this.reviewNames = reviewNames;
	}

	public TmpTaskOperationBean getTaskOperationBean() {
		if (taskOperationBean == null) {
			taskOperationBean = new TmpTaskOperationBean();
		}
		return taskOperationBean;
	}

	public void setTaskOperationBean(TmpTaskOperationBean taskOperationBean) {
		this.taskOperationBean = taskOperationBean;
	}

	/**
	 * 根据任务类型获取对应的编号名称
	 * 
	 * @return
	 */
	public String getTypeNumber() {
		// 任务类型 0：流程任务，1：文件任务，2：制度模板任务，3：制度文件任务，4：流程地图任务
		switch (jecnTaskBeanNew.getTaskType()) {
		case 0:
			// 流程编号：
			typeNumber = JecnUtil.getValue("processNumberC");
			break;
		case 1:
			// 文件编号：
			typeNumber = JecnUtil.getValue("fileNumberC");
			break;
		case 2:
		case 3:
			// 制度编号：
			typeNumber = JecnUtil.getValue("ruleNumberC");
			break;
		case 4:
			// 地图编号：
			typeNumber = JecnUtil.getValue("map_Number");
			break;
		}
		return typeNumber;
	}

	public void setTypeNumber(String typeNumber) {
		this.typeNumber = typeNumber;
	}

	public String getShowOperationTransfer() {
		return showOperationTransfer;
	}

	public void setShowOperationTransfer(String showOperationTransfer) {
		this.showOperationTransfer = showOperationTransfer;
	}

	public String getShowOperationAssigned() {
		return showOperationAssigned;
	}

	public void setShowOperationAssigned(String showOperationAssigned) {
		this.showOperationAssigned = showOperationAssigned;
	}

}
