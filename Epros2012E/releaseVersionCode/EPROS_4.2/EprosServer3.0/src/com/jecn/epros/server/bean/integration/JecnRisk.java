package com.jecn.epros.server.bean.integration;

import java.util.Date;
import java.util.List;

/***
 * 风险表 2013-10-29
 * 
 */
public class JecnRisk implements java.io.Serializable {

	/** 主键ID */
	private Long id;
	/** 父ID */
	private Long parentId;
	/** 编号 */
	private String riskCode;
	/** 是否是目录：0：是，1：否 */
	private int isDir;
	/** 风险描述或目录名 */
	private String riskName;
	/** 等级 */
	private int grade;
	/** 序号 */
	private Integer sort;
	/** 标准化控制 */
	private String standardControl;
	/*** 创建人 */
	private Long createPersonId;
	/*** 创建日期 */
	private Date createTime;
	/*** 更新人 */
	private Long updatePersonId;
	/*** 更新日期 */
	private Date updateTime;
	/** 备注 */
	private String note;
	/** 内控指引ID */
	// private String guideId;
	// /**内控指引名称*/
	// private String guideName;
	private List<JecnControlGuide> listControlGuide;
	/** 控制目标集合 */
	private List<JecnControlTarget> listControlTarget;
	/** 项目ID */
	private Long projectId;
	private String tPath;// tpath 节点层级关系
	private int tLevel;// level 节点级别，默认0开始
	private String viewSort;

	private int riskType;

	public String getViewSort() {
		return viewSort;
	}

	public void setViewSort(String viewSort) {
		this.viewSort = viewSort;
	}

	public String gettPath() {
		return tPath;
	}

	public void settPath(String tPath) {
		this.tPath = tPath;
	}

	public int gettLevel() {
		return tLevel;
	}

	public void settLevel(int tLevel) {
		this.tLevel = tLevel;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getParentId() {
		return parentId;
	}

	public void setParentId(Long parentId) {
		this.parentId = parentId;
	}

	public String getRiskCode() {
		return riskCode;
	}

	public void setRiskCode(String riskCode) {
		this.riskCode = riskCode;
	}

	public int getIsDir() {
		return isDir;
	}

	public void setIsDir(int isDir) {
		this.isDir = isDir;
	}

	public String getRiskName() {
		return riskName;
	}

	public void setRiskName(String riskName) {
		this.riskName = riskName;
	}

	public int getGrade() {
		return grade;
	}

	public void setGrade(int grade) {
		this.grade = grade;
	}

	public Integer getSort() {
		return sort;
	}

	public void setSort(Integer sort) {
		this.sort = sort;
	}

	public String getStandardControl() {
		return standardControl;
	}

	public void setStandardControl(String standardControl) {
		this.standardControl = standardControl;
	}

	public Long getCreatePersonId() {
		return createPersonId;
	}

	public void setCreatePersonId(Long createPersonId) {
		this.createPersonId = createPersonId;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public Long getUpdatePersonId() {
		return updatePersonId;
	}

	public void setUpdatePersonId(Long updatePersonId) {
		this.updatePersonId = updatePersonId;
	}

	public Date getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	public List<JecnControlTarget> getListControlTarget() {
		return listControlTarget;
	}

	public void setListControlTarget(List<JecnControlTarget> listControlTarget) {
		this.listControlTarget = listControlTarget;
	}

	public List<JecnControlGuide> getListControlGuide() {
		return listControlGuide;
	}

	public void setListControlGuide(List<JecnControlGuide> listControlGuide) {
		this.listControlGuide = listControlGuide;
	}

	public Long getProjectId() {
		return projectId;
	}

	public void setProjectId(Long projectId) {
		this.projectId = projectId;
	}

	public int getRiskType() {
		return riskType;
	}

	public void setRiskType(int riskType) {
		this.riskType = riskType;
	}

}
