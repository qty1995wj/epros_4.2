package epros.draw.gui.workflow.dialog;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.ScrollPaneConstants;
import javax.swing.event.CaretEvent;
import javax.swing.event.CaretListener;

import epros.draw.constant.JecnToolbarConstant.ToolBarElemType;
import epros.draw.data.figure.JecnRoleData;
import epros.draw.gui.figure.JecnBaseRoleFigure;
import epros.draw.gui.swing.JecnOKCancelPanel;
import epros.draw.gui.swing.JecnPanel;
import epros.draw.gui.swing.JecnUserInfoTextArea;
import epros.draw.gui.top.dialog.JecnDialog;
import epros.draw.gui.top.dialog.listener.JecnDialogEvent;
import epros.draw.gui.top.dialog.listener.JecnDialogListener;
import epros.draw.gui.workflow.JecnDrawMainPanel;
import epros.draw.unredo.JecnUnRedoProcess;
import epros.draw.unredo.JecnUnRedoProcessFactory;
import epros.draw.unredo.data.JecnUndoRedoData;
import epros.draw.util.DrawCommon;
import epros.draw.util.JecnResourceUtil;
import epros.draw.util.JecnUIUtil;
import epros.draw.util.JecnUserCheckUtil;

/**
 * 
 * 角色明细
 * 
 * @author ZHOUXY
 * 
 */
public class JecnRoleDetailsDialog extends JecnDialog implements CaretListener, ActionListener {
	/** 角色对象 */
	protected JecnBaseRoleFigure roleFigure = null;

	/** 主面板的容器· */
	protected JTabbedPane tabbedPanel = null;
	/** 主面板 */
	protected JecnPanel basicInfoPanel = null;
	/** 主内容面板 */
	protected JecnPanel activeAtrrPanel = null;
	/** 确认取消按钮面板 */
	protected JecnOKCancelPanel okCancelPanel = null;

	/** 角色名称标签标签 */
	protected JLabel roleNameLabel = null;
	/** 角色名称输入框 */
	protected JTextArea roleNameTextArea = null;
	/** 角色名称输入框容器 */
	protected JScrollPane roleNameScrollPane = null;
	/** 角色名称提示信息框 */
	protected JecnUserInfoTextArea roleNameInfoTextArea = null;

	/** 角色职责标签 */
	protected JLabel roleResLabel = null;
	/** 角色职责输入区域 */
	protected JTextArea roleResTextArea = null;
	/** 角色职责输入区域容器 */
	protected JScrollPane roleResScrollPane = null;
	/** 角色职责提示信息框 */
	protected JecnUserInfoTextArea roleResInfoTextArea = null;

	public JecnRoleDetailsDialog(JecnBaseRoleFigure roleFigure) {
		this.roleFigure = roleFigure;
		intComponents();
		initLayout();
		initData();
	}

	/**
	 * 
	 * 初始化组件
	 * 
	 */
	protected void intComponents() {
		// 主面板的容器
		tabbedPanel = new JTabbedPane();
		// 主面板
		basicInfoPanel = new JecnPanel();

		// 角色信息
		activeAtrrPanel = new JecnPanel();

		// 名称标签
		roleNameLabel = new JLabel(JecnResourceUtil.getJecnResourceUtil().getValue("roleName"));
		// 名称输入框
		roleNameTextArea = new JTextArea();
		// 名称输入框容器
		roleNameScrollPane = new JScrollPane(roleNameTextArea);
		// 名称提示信息框
		roleNameInfoTextArea = new JecnUserInfoTextArea();

		// 角色职责标签
		roleResLabel = new JLabel(JecnResourceUtil.getJecnResourceUtil().getValue("roleRes"));
		// 角色职责输入框
		roleResTextArea = new JTextArea();
		// 角色职责输入区域容器
		roleResScrollPane = new JScrollPane(roleResTextArea);
		// 角色职责提示信息框
		roleResInfoTextArea = new JecnUserInfoTextArea();

		// 确认取消按钮面板
		okCancelPanel = new JecnOKCancelPanel();

		// 大小
		Dimension size = new Dimension(480, 404);
		Dimension preferredSize = new Dimension(600, 500);
		this.setSize(preferredSize);
		this.setMinimumSize(size);
		this.setModal(true);
		this.setResizable(true);
		this.setDefaultCloseOperation(JecnDialog.DISPOSE_ON_CLOSE);
		this.setLocationRelativeTo(JecnDrawMainPanel.getMainPanel());
		// 标题
		this.setTitle(JecnResourceUtil.getJecnResourceUtil().getValue(ToolBarElemType.roleDetails));

		// 角色信息
		activeAtrrPanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());

		// 角色名称输入框
		roleNameTextArea.setRows(1);
		roleNameTextArea.setLineWrap(true);
		roleNameTextArea.setWrapStyleWord(true);
		roleNameTextArea.setBorder(BorderFactory.createLineBorder(Color.gray));
		roleNameTextArea.setBorder(null);

		// 角色职责输入区域容器
		roleNameScrollPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		roleNameScrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED);

		// 角色职责输入区域容器
		roleResScrollPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		roleResScrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED);
		roleResScrollPane.setBorder(null);

		// 角色职责输入区域
		roleResTextArea.setRows(2);
		roleResTextArea.setLineWrap(true);
		roleResTextArea.setWrapStyleWord(true);
		roleResTextArea.setBorder(BorderFactory.createLineBorder(Color.gray));

		// 事件
		// 角色名称
		roleNameTextArea.addCaretListener(this);
		// 角色职责
		roleResTextArea.addCaretListener(this);
		// 确认按钮
		okCancelPanel.getOkBtn().addActionListener(this);
		// 取消按钮
		okCancelPanel.getCancelBtn().addActionListener(this);

		// 添加组件
		this.getContentPane().add(tabbedPanel);
		// 添加基本信息页
		tabbedPanel.addTab(JecnResourceUtil.getJecnResourceUtil().getValue("basicInfo"), basicInfoPanel);

		// 添加监听
		this.addJecnDialogListener(new JecnDialogListener() {
			@Override
			public boolean dialogCloseBefore(JecnDialogEvent e) {
				if (isUpdate()) {// true：有更新；false：没有更新
					if (dialogCloseBeforeMsgTip()) {// true：需要执行关闭 false：不要执行关闭
						return true;
					}
					return false;
				}
				return true;
			}
		});
	}

	/**
	 * 
	 * 角色信息
	 * 
	 */
	protected void initLayout() {

		int h = 7;
		int v = 3;

		/** 标签间距 */
		Insets insetsLabel = new Insets(7, 11, 7, 3);
		/** 内容间距 */
		Insets insetsContent = new Insets(7, 0, 7, 5);

		// 角色信息
		GridBagConstraints c = new GridBagConstraints(0, 0, 1, 1, 1.0, 1.0, GridBagConstraints.WEST,
				GridBagConstraints.BOTH, new Insets(v, h, 0, h), 0, 0);
		basicInfoPanel.add(activeAtrrPanel, c);
		// 确认取消按钮面板
		c = new GridBagConstraints(0, 2, 1, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				new Insets(0, h, 0, h), 0, 0);
		basicInfoPanel.add(okCancelPanel, c);

		// //////////////// 角色信息//////////////////
		// 角色名称标签
		c = new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, insetsLabel,
				0, 0);
		activeAtrrPanel.add(roleNameLabel, c);
		// 角色名称输入框
		c = new GridBagConstraints(1, 0, 1, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				insetsContent, 0, 0);
		activeAtrrPanel.add(roleNameScrollPane, c);
		// 角色名称信息提示
		c = new GridBagConstraints(1, 1, 1, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				JecnUIUtil.getInsets0(), 0, 0);
		activeAtrrPanel.add(roleNameInfoTextArea, c);

		// 角色职责标签
		c = new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, insetsLabel,
				0, 0);
		activeAtrrPanel.add(roleResLabel, c);
		// 角色职责输入框容器·
		c = new GridBagConstraints(1, 2, 1, 1, 1.0, 1.0, GridBagConstraints.WEST, GridBagConstraints.BOTH,
				insetsContent, 0, 0);
		activeAtrrPanel.add(roleResScrollPane, c);
		// 角色职责信息提示
		c = new GridBagConstraints(1, 3, 1, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				JecnUIUtil.getInsets0(), 0, 0);
		activeAtrrPanel.add(roleResInfoTextArea, c);

		// ////////////////角色信息//////////////////
	}

	/**
	 * 
	 * 初始化数据
	 * 
	 */
	protected void initData() {
		// 角色名称
		this.roleNameTextArea.setText(this.roleFigure.getFlowElementData().getFigureText());
		// 角色职责
		this.roleResTextArea.setText(this.roleFigure.getFlowElementData().getRoleRes());
	}

	/**
	 * 
	 * 退出对话框操作,提供给取消按钮使用
	 * 
	 */
	public void exitJecnDialog() {
		if (!isUpdate()) {// 没有更新
			this.setVisible(false);
		} else {
			if (dialogCloseBeforeMsgTip()) {// 信息提示：true：需要执行关闭 false：不要执行关闭
				this.setVisible(false);
			}
		}
	}

	/**
	 * 
	 * 是否有更新
	 * 
	 * @return boolean true：有更新；false：没有更新
	 */
	protected boolean isUpdate() {
		// 角色名称
		String roleName = this.roleNameTextArea.getText();
		// 角色职责
		String roleRes = this.roleResTextArea.getText();

		if (!isUpdate(roleName, roleRes)) {// 没有更新
			return false;
		}
		return true;
	}

	/**
	 * 
	 * 输入框的插入字符事件
	 * 
	 */
	public void caretUpdate(CaretEvent e) {
		caretUpdateProcess(e);
	}

	public void actionPerformed(ActionEvent e) {
		actionPerformedProcess(e);
	}

	/**
	 * 
	 * 下拉框事件或按钮点击事件
	 * 
	 * @param e
	 *            ActionEvent
	 */
	public void actionPerformedProcess(ActionEvent e) {
		if (e.getSource() == okCancelPanel.getOkBtn()) {// 确认按钮

			// boolean true:校验成功；false：校验失败
			boolean ret = okBtnCheck();
			if (!ret) {// 校验失败
				return;
			}
			// 校验成功修改角色属性值
			updateValuesToActionFigure();
			// 关闭对话框
			this.setVisible(false);

		} else if (e.getSource() == okCancelPanel.getCancelBtn()) {// 取消按钮
			// 关闭对话框
			exitJecnDialog();
		}
	}

	/**
	 * 
	 * 输入文本框插入字符事件
	 * 
	 * @param e
	 *            CaretEvent
	 */
	protected void caretUpdateProcess(CaretEvent e) {
		if (e.getSource() == roleNameTextArea) {// 角色名称输入框
			// 长度超过122个字符或61个汉字
			checkTextFieldLength(roleNameInfoTextArea, roleNameTextArea);
		} else if (e.getSource() == roleResTextArea) {// 角色职责输入框
			// 长度是否超过1200个字符或600个汉字
			checkTextAreaLength(roleResInfoTextArea, roleResTextArea);
		}
	}

	/**
	 * 
	 * 点击确认按钮提交前校验
	 * 
	 * @return boolean true:校验成功；false：校验失败
	 * 
	 */
	protected boolean okBtnCheck() {
		boolean ret = true;
		// 角色名称
		boolean nameRet = checkTextFieldLength(roleNameInfoTextArea, roleNameTextArea);
		if (!nameRet) {
			ret = nameRet;
		}
		// 角色职责
		boolean noteRet = checkTextAreaLength(roleResInfoTextArea, roleResTextArea);
		if (!noteRet) {
			ret = noteRet;
		}
		return ret;
	}

	/**
	 * 
	 * 校验JTextField的内容长度是否合法，不合法提示信息再infoTextArea中显示
	 * 
	 * @param infoTextArea
	 *            JecnUserInfoTextArea
	 * @param textField
	 *            JTextField
	 * @return boolean 校验成功或不需要校验返回true；校验失败返回false
	 */
	protected boolean checkTextFieldLength(JecnUserInfoTextArea infoTextArea, JTextField textField) {
		// 校验长度，返回提示信息
		String checkString = JecnUserCheckUtil.checkNameLength(textField.getText());
		// true:校验成功；false：校验失败
		return infoTextArea.checkInfo(checkString);
	}

	/**
	 * 
	 * 校验JTextArea的内容长度是否合法，不合法提示信息再infoTextArea中显示
	 * 
	 * @param infoTextArea
	 *            JecnUserInfoTextArea
	 * @param textArea
	 *            JTextArea
	 * @return boolean 校验成功或不需要校验返回true；校验失败返回false
	 */
	protected boolean checkTextFieldLength(JecnUserInfoTextArea infoTextArea, JTextArea textArea) {
		// 校验长度，返回提示信息
		String checkString = JecnUserCheckUtil.checkNameLength(textArea.getText());
		// true:校验成功；false：校验失败
		return infoTextArea.checkInfo(checkString);
	}

	/**
	 * 
	 * 校验JTextArea的内容长度是否合法，不合法提示信息再infoTextArea中显示
	 * 
	 * @param infoTextArea
	 *            JecnUserInfoTextArea
	 * @param textArea
	 *            JTextArea
	 * @return boolean 校验成功或不需要校验返回true；校验失败返回false
	 */
	protected boolean checkTextAreaLength(JecnUserInfoTextArea infoTextArea, JTextArea textArea) {
		if (!textArea.isEnabled()) {// 未启用就不用校验
			return true;
		}
		// 校验长度，返回提示信息
		String checkString = JecnUserCheckUtil.checkNoteLength(textArea.getText());
		// true:校验成功；false：校验失败
		return infoTextArea.checkInfo(checkString);
	}

	/**
	 * 
	 * 更新角色相关属性属性
	 * 
	 */
	protected void updateValuesToActionFigure() {
		// 角色名称
		String roleName = this.roleNameTextArea.getText();
		// 角色职责
		String roleRes = this.roleResTextArea.getText();

		if (!isUpdate(roleName, roleRes)) {// 没有更新
			return;
		}

		// 操作前数据
		JecnUndoRedoData undoData = new JecnUndoRedoData();
		// 操作后数据
		JecnUndoRedoData redoData = new JecnUndoRedoData();

		// 操作前
		undoData.recodeFlowElement(roleFigure);

		// 角色名称
		this.roleFigure.getFlowElementData().setFigureText(roleName);
		// 角色职责
		this.roleFigure.getFlowElementData().setRoleRes(roleRes);

		this.roleFigure.repaint();

		// 操作后
		redoData.recodeFlowElement(roleFigure);

		// *************************撤销恢复*************************//
		// 记录这次操作的数据集合
		JecnUnRedoProcess unredoProcess = JecnUnRedoProcessFactory.createEdit(undoData, redoData);
		JecnDrawMainPanel.getMainPanel().getWorkflow().getUndoRedoManager().addEdit(unredoProcess);
		// *************************撤销恢复*************************//
	}

	/**
	 * 
	 * 判断是否有更新，有返回true，没有返回false
	 * 
	 * @param newNum
	 *            String 角色名称
	 * @param newName
	 *            String 岗位名称
	 * @param newNote
	 *            String 角色职责
	 * @return boolean 有返回true，没有返回false
	 * 
	 */
	protected boolean isUpdate(String roleName, String roleRes) {
		JecnRoleData data = this.roleFigure.getFlowElementData();

		if (!DrawCommon.checkStringSame(roleName, data.getFigureText())
				|| !DrawCommon.checkStringSame(roleRes, data.getRoleRes())) {
			return true;
		}
		return false;
	}
}
