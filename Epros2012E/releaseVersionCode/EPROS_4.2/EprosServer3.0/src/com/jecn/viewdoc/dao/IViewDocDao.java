package com.jecn.viewdoc.dao;

import com.jecn.epros.server.common.IBaseDao;
import com.jecn.viewdoc.bean.ViewDocContentIdCache;


public interface IViewDocDao extends IBaseDao<ViewDocContentIdCache, Long> {

	ViewDocContentIdCache getCache(Long relatedId, int dataType);

}
