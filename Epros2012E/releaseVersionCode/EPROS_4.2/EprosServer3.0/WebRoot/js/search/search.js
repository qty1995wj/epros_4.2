/* 
text：输入框id, 
opid：保存人员的隐藏表单域的ID, 
div：div的ID  ,
type ：搜索类型(person:人员  org:部门  pos:岗位)
 */
function search(text, opid, div, type) {
	// 获取输入框
	var textValue = text.value;
	textValue = textValue.replace(/(^\s*)|(\s*$)/g, "");
	// 获取显示的div
	var url = "peopleSearch.action";
	// 获取输入框的宽度
	var width = text.offsetWidth;
	div.style.width = width;
	if (type == "person") { // 人员
		url = "peopleSearch.action";
	} else if (type == "org") { // 部门
		url = "orgSearch.action";
	} else if (type == "pos") { // 岗位
		url = "posSearch.action";
	} else if (type == "process") { // 流程
		url = "processSearch.action";
	} else if (type == "rule") {// 制度
		url = "ruleSearch.action";
	}
	var inputId = text.id;
	var outputDivId = div.id;
	var numberTextId = opid.id;
	divNode(div);
	if (textValue != null && textValue != "") {
		
		$.ajax( {
			type : 'post',//可选get
			url : url,//这里是接收数据的程序
			data : 'searchName=' + textValue,//传的数据，多个参数用&连接
			dataType : 'Json',//服务器返回的数据类型 可选XML ,Json jsonp script html text等
			success : function(msg) {
				//这里是ajax提交成功后，程序返回的数据处理函数。msg是返回的数据，数据类型在dataType参数里定义！
			if (msg != null && msg.length > 0) {
				div.style.display = "block";
				div.innerHTML = "";
				for ( var i = 0; i < msg.length; i++) {
					var s = '<div onmouseover="javascript:suggestOver(this);"';
					s += ' onmouseout="javascript:suggestOut(this);" ';
					s += ' style="width:' + width + ';cursor: pointer;"';
					s += ' onclick="javascript:setSearch(this.innerHTML,'
							+ msg[i].id + ',' + inputId + ',' + numberTextId
							+ ',' + outputDivId + ');" ';
					s += ' class="suggest_link">' + msg[i].name + '</div>';
					div.innerHTML += s;
				}
			} else {
				divNode(div);
			}
		},
		error : function() {
			//ajax提交失败的处理函数！
			divNode(div);
		}
		})
	} else {
		divNode(div);
	}
}

/**
 * 隐藏div
 */
function divNode(div) {
		document.onmouseup = function(e) {
		var event = e || window.event;
		var ele = event.srcElement || event.target;
		if (ele.parentNode == null) {
			//			alert("ele.parentNode==null");
			div.style.display = "none";
			document.onmouseup = null;
			return;
		} else if (ele.parentNode.id != div.id) {
			//			alert("ele.parentNode.id != div.id");
			div.style.display = "none";
			document.onmouseup = null;
			return;
		}
	}
}

function suggestOver(div_value) {
	div_value.className = "suggest_link_over";
}

function suggestOut(div_value) {
	div_value.className = "suggest_link";
}

function setSearch(peopleName, peopleId, inputId, numberTextId, outputDivId) {
	inputId.value = peopleName;
	numberTextId.value = peopleId;
	outputDivId.innerHTML = "";
	outputDivId.style.display = "none";
}

function tbblur() {
	var div = document.getElementById("search_suggest");
	//div.innerHTML = "";
	div.style.display = "none";
}
