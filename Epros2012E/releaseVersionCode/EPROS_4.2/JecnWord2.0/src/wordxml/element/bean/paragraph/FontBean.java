package wordxml.element.bean.paragraph;

import java.awt.Color;

import wordxml.constant.Constants;
import wordxml.element.bean.common.ShdBean;
import wordxml.element.bean.common.LineBean;

/**
 * 字体属性
 * 
 * @author admin
 * 
 */
public class FontBean {
	/** 字体名称 默认宋体 */
	private String fontFamily = "";
	/** 字体大小磅磅值 默认值 五号 */
	private int fontSize = Constants.wuhao;
	/**** 字体颜色 默认黑色 *****/
	private Color fontColor = Color.black;
	/** 字体加粗 默认false 不加粗 **/
	private boolean b = false;
	/*** 字体倾斜 默认false 不倾斜 ******/
	private boolean i = false;
	/** 字体下划线 **/
	private LineBean underlineBean;
	/** 绘制一条单线穿过文本 默认false 不绘制 **/
	private boolean strike = false;
	/** 绘制双线穿过文本 默认false 不绘制 **/
	private boolean dstrike = false;
	/*** 文本放大缩小 属性 ****/
	private int textScaleVal;

	/** 底纹 **/
	private ShdBean shdBean;

	public FontBean() {
	}

	public FontBean(String fontFamily, int fontSize, boolean b) {
		this.fontFamily = fontFamily;
		this.fontSize = fontSize;
		this.b = b;
	}

	public FontBean(int fontSize, boolean b) {
		this.fontSize = fontSize;
		this.b = b;
	}

	public FontBean(int fontSize) {
		this.fontSize = fontSize;
	}

	public FontBean(boolean b) {
		this.b = b;
	}

	public String getFontFamily() {
		return fontFamily;
	}

	public void setFontFamily(String fontFamily) {
		this.fontFamily = fontFamily;
	}

	public int getFontSize() {
		return fontSize;
	}

	public void setFontSize(int fontSize) {
		this.fontSize = fontSize;
	}

	public Color getFontColor() {
		return fontColor;
	}

	public void setFontColor(Color fontColor) {
		this.fontColor = fontColor;
	}

	public boolean isB() {
		return b;
	}

	public void setB(boolean b) {
		this.b = b;
	}

	public boolean isI() {
		return i;
	}

	public void setI(boolean i) {
		this.i = i;
	}

	public LineBean getUnderlineBean() {
		return underlineBean;
	}

	public void setUnderlineBean(LineBean underlineBean) {
		this.underlineBean = underlineBean;
	}

	public ShdBean getShdBean() {
		return shdBean;
	}

	public void setShdBean(ShdBean shdBean) {
		this.shdBean = shdBean;
	}

	public boolean isStrike() {
		return strike;
	}

	public void setStrike(boolean strike) {
		this.strike = strike;
	}

	public boolean isDstrike() {
		return dstrike;
	}

	public void setDstrike(boolean dstrike) {
		this.dstrike = dstrike;
	}

	public int getTextScaleVal() {
		return textScaleVal;
	}

	public void setTextScaleVal(int textScaleVal) {
		this.textScaleVal = textScaleVal;
	}

}
