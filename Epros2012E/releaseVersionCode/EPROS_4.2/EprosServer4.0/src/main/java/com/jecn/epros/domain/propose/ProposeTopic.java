package com.jecn.epros.domain.propose;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.jecn.epros.domain.JecnUser;
import com.jecn.epros.security.AuthenticatedUserUtil;
import com.jecn.epros.util.JecnProperties;
import org.apache.commons.lang3.StringUtils;

import java.util.Date;

/**
 * 合理化建议主题表
 *
 * @author user
 */
public class ProposeTopic {
    private static final String[] TYPES = {"流程",
            "制度",
            "流程架构",
            "文件",
            "风险",
            "标准"};
    private static final String[] EnTYPES = {"Process",
            "Institution",
            "Process Map ",
            "File",
            "Risk",
            "Standard"};

    private Long relatedId;
    private Integer type;
    private String name;
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+08")
    private Date createTime;
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+08")
    private Date updateTime;
    /*** 处理人**/
    @JsonIgnore
    private Long handlePeople;
    /*** 处理人名称*/
    @JsonIgnore
    private String handlePeopleName;

    public JecnUser getHandler() {
        if (handlePeople != null && StringUtils.isNotBlank(handlePeopleName)) {
            return new JecnUser(handlePeople, handlePeopleName);
        }
        return new JecnUser();
    }

    public String getTypeStr() {
        if (type == -1) {
            return "";
        }
        return AuthenticatedUserUtil.isLanguageType() == 0 ? TYPES[type] : EnTYPES[type];
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getRelatedId() {
        return relatedId;
    }

    public void setRelatedId(Long relatedId) {
        this.relatedId = relatedId;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public Long getHandlePeople() {
        return handlePeople;
    }

    public void setHandlePeople(Long handlePeople) {
        this.handlePeople = handlePeople;
    }

    public String getHandlePeopleName() {
        return handlePeopleName;
    }

    public void setHandlePeopleName(String handlePeopleName) {
        this.handlePeopleName = handlePeopleName;
    }
}
