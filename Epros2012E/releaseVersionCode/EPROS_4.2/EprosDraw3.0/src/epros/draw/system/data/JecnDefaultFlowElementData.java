package epros.draw.system.data;

import java.awt.Color;

import epros.draw.constant.JecnLogConstants;
import epros.draw.constant.JecnToolBoxConstant.MapElemType;
import epros.draw.constant.JecnWorkflowConstant.FillColorChangType;
import epros.draw.constant.JecnWorkflowConstant.FillType;
import epros.draw.data.JecnAbstractFlowElementData;
import epros.draw.error.ErrorInfoConstant;
import epros.draw.util.DrawCommon;

/**
 * 
 * 系统默认流程元素属性类
 * 
 * @author ZHOUXY
 * 
 */
public class JecnDefaultFlowElementData {
	// ******************** 老版配置文件 START ********************//
	/** (设计器)元素维护设置每个流程元素图形主键ID */
	private long flowElementId;
	/** 图形元素标识 */
	private MapElemType mapElemType = null;
	/** 边框颜色 */
	private Color bodyColor = null;
	/** 边框类型：0 1 2 3 */
	private int bodyStyle = 0;
	/** 图形宽 */
	private int figureSizeX = 0;
	/** 图形高 */
	private int figureSizeY = 01;
	/** 填充类型：无填充、单色（默认）、双色 */
	private FillType fillType = FillType.one;
	/** 填充颜色 */
	private Color fillColor = null;
	/** 填充颜色2 */
	private Color changeColor = null;
	/** 渐变方向类型(topTilt:向上倾斜（默认） bottom:向下倾斜 vertical:垂直 horizontal:水平) */
	private FillColorChangType fillColorChangType = FillColorChangType.topTilt;
	/** 字体颜色 */
	private Color fontColor = null;
	/** 字体位置 0:左上 1：正上 2：右上 3：左中 4：正中 5：右中 6：左下 7：正下 8：右下 */
	private int fontPosition = 4;
	/** 字体大小 */
	private int fontSize = 12;
	/** 阴影 0:无阴影 1：有阴影 */
	private int shadow = 0;
	/** 字体竖排 1:竖排文字，0：不是竖排文字 false:字体横排 true:字体竖排 */
	private int upRight = 0;
	// ******************** 老版配置文件 END ********************//
	/** 字形样式（0:普通 1:加粗、2:斜线） 目前支持加粗和普通 */
	private int fontStyle = 0;
	/** 字体名称 默认宋体 */
	private String fontName = null;
	/** 阴影颜色 */
	private Color shadowColor = null;
	/** 线条粗细 (1~10) */
	private int bodyWidth = 1;
	/** 3D效果 True：存在3D效果，False：不存在3D效果 */
	private boolean flag3D = false;
	/** 是否显示编辑四个点 true:显示 false：不显示 */
	private boolean eidtPoint = false;
	/** 是否支持双击：isDoubleClick */
	private boolean isDoubleClick = false;
	/** 是否支持快速添加图形：isSortAddFigure */
	private boolean isSortAddFigure = false;

	/** 是否保存到XML标识 true：是 false：否 */
	private boolean updateFlag = false;
	/** 图形默认名称 */
	private String figureText;

	/**
	 * 
	 * 复制对象
	 * 
	 */
	public JecnDefaultFlowElementData clone() {

		JecnDefaultFlowElementData data = new JecnDefaultFlowElementData();
		data.setMapElemType(this.getMapElemType());

		// ******************** 老版配置文件 START ********************//
		// 边框颜色
		data.setBodyColor(cloneColor(this.getBodyColor()));
		// 边框类型：0 1 2 3
		data.setBodyStyle(this.getBodyStyle());
		// 图形宽
		data.setFigureSizeX(this.getFigureSizeX());
		// 图形高
		data.setFigureSizeY(this.getFigureSizeY());

		// 填充类型
		data.setFillType(this.getFillType());
		// 填充颜色
		data.setFillColor(cloneColor(this.getFillColor()));
		// 填充颜色2
		data.setChangeColor(cloneColor(this.getChangeColor()));
		// 渐变方向类型
		data.setFillColorChangType(this.getFillColorChangType());

		// 字体颜色
		data.setFontColor(cloneColor(this.getFontColor()));
		// 字体位置 0:左上 1：正上 2：右上 3：左中 4：正中 5：右中 6：左下 7：正下 8：右下
		data.setFontPosition(this.getFontPosition());
		// 字体大小
		data.setFontSize(this.getFontSize());
		// 阴影 0:无阴影 1：有阴影
		data.setShadow(this.getShadow());
		// 字体竖排 1:竖排文字，0：不是竖排文字
		data.setUpRight(this.getUpRight());
		// ******************** 老版配置文件 END ********************//
		// 字形样式（0:普通 1:加粗、2:斜线） 目前支持加粗和普通
		data.setFontStyle(this.getFontStyle());
		// 字体名称 默认宋体
		data.setFontName(this.getFontName());
		// 阴影颜色
		data.setShadowColor(cloneColor(this.getShadowColor()));
		// 线条粗细 (1~10)
		data.setBodyWidth(this.getBodyWidth());
		// 3D效果 True：存在3D效果，False：不存在3D效果
		data.setFlag3D(this.getFlag3D());
		// 是否显示编辑点
		data.setEidtPoint(this.getEidtPoint());
		// 设置图形默认名称
		data.setFigureText(this.getFigureText());
		// 是否支持双击：isDoubleClick
		data.setDoubleClick(this.getDoubleClick());
		// 是否支持快速添加图形：isSortAddFigure
		data.setSortAddFigure(this.getSortAddFigure());
		return data;

	}

	/**
	 * 
	 * 复制颜色对象
	 * 
	 * @param color
	 *            Color 待复制的颜色
	 * @return Color 复制的Color或Null
	 */
	private Color cloneColor(Color color) {
		if (color == null) {
			return null;
		}
		return new Color(color.getRGB());
	}

	public Color getBodyColor() {
		return bodyColor;
	}

	public void setBodyColor(Color bodyColor) {
		this.bodyColor = bodyColor;
	}

	public int getBodyStyle() {
		return bodyStyle;
	}

	public void setBodyStyle(int bodyStyle) {
		if (bodyStyle <= -1 || bodyStyle >= 4) {
			JecnLogConstants.LOG_DEFAULT_FLOW_ELEMENT_DATA
					.error(" 参数值不是[0、1、2、3]其中一个. bodyStyle=" + bodyStyle
							+ ";mapElemType=" + mapElemType);
			throw new IllegalArgumentException(ErrorInfoConstant.PARA_OBJ_FAIL);
		}
		this.bodyStyle = bodyStyle;
	}

	public int getFigureSizeX() {
		return figureSizeX;
	}

	public void setFigureSizeX(int figureSizeX) {
		if (figureSizeX < 0) {
			JecnLogConstants.LOG_DEFAULT_FLOW_ELEMENT_DATA
					.error(" 参数必须大于0. figureSizeX=" + figureSizeX
							+ ";mapElemType=" + mapElemType);
			throw new IllegalArgumentException(ErrorInfoConstant.PARA_OBJ_FAIL);
		}
		this.figureSizeX = figureSizeX;
	}

	public int getFigureSizeY() {
		return figureSizeY;
	}

	public void setFigureSizeY(int figureSizeY) {
		if (figureSizeY < 0) {
			JecnLogConstants.LOG_DEFAULT_FLOW_ELEMENT_DATA
					.error(" 参数必须大于0. figureSizeY=" + figureSizeY
							+ ";mapElemType=" + mapElemType);
			throw new IllegalArgumentException(ErrorInfoConstant.PARA_OBJ_FAIL);
		}
		this.figureSizeY = figureSizeY;
	}

	public Color getFillColor() {
		return fillColor;
	}

	public void setFillColor(Color fillColor) {
		this.fillColor = fillColor;
	}

	public Color getFontColor() {
		return fontColor;
	}

	public void setFontColor(Color fontColor) {
		this.fontColor = fontColor;
	}

	public int getFontPosition() {
		return fontPosition;
	}

	public void setFontPosition(int fontPosition) {
		if (fontPosition <= -1 || fontPosition >= 9) {
			JecnLogConstants.LOG_DEFAULT_FLOW_ELEMENT_DATA
					.error(" 参数fontPosition超出[0~8]范围. fontPosition="
							+ fontPosition + ";mapElemType=" + mapElemType);
			throw new IllegalArgumentException(ErrorInfoConstant.PARA_OBJ_FAIL);
		}
		this.fontPosition = fontPosition;
	}

	public int getFontSize() {
		return fontSize;
	}

	public void setFontSize(int fontSize) {
		if (fontSize <= 0) {
			JecnLogConstants.LOG_DEFAULT_FLOW_ELEMENT_DATA
					.error(" 参数fontSize必须大于0. fontSize=" + fontSize
							+ ";mapElemType=" + mapElemType);
			throw new IllegalArgumentException(ErrorInfoConstant.PARA_OBJ_FAIL);
		}
		this.fontSize = fontSize;
	}

	public int getShadow() {
		return shadow;
	}

	public void setShadow(int shadow) {
		if (shadow != 0 && shadow != 1) {
			JecnLogConstants.LOG_DEFAULT_FLOW_ELEMENT_DATA
					.error(" 参数shadow必须是[0,1]其中一个. shadow=" + shadow
							+ ";mapElemType=" + mapElemType);
			throw new IllegalArgumentException(ErrorInfoConstant.PARA_OBJ_FAIL);
		}
		this.shadow = shadow;
	}

	public int getUpRight() {
		return upRight;
	}

	public void setUpRight(int upRight) {
		if (upRight != 0 && upRight != 1) {
			JecnLogConstants.LOG_DEFAULT_FLOW_ELEMENT_DATA
					.error(" 参数upRight必须是[0,1]其中一个. upRight=" + upRight
							+ ";mapElemType=" + mapElemType);
			throw new IllegalArgumentException(ErrorInfoConstant.PARA_OBJ_FAIL);
		}
		this.upRight = upRight;
	}

	public int getFontStyle() {
		return fontStyle;
	}

	public void setFontStyle(int fontStyle) {
		if (fontStyle != 0 && fontStyle != 1) {
			JecnLogConstants.LOG_DEFAULT_FLOW_ELEMENT_DATA
					.error(" 参数fontStyle必须是[0,1]其中一个. fontStyle=" + fontStyle
							+ ";mapElemType=" + mapElemType);
			throw new IllegalArgumentException(ErrorInfoConstant.PARA_OBJ_FAIL);
		}
		this.fontStyle = fontStyle;
	}

	public String getFontName() {
		return fontName;
	}

	public void setFontName(String fontName) {
		if (fontName == null) {
			JecnLogConstants.LOG_DEFAULT_FLOW_ELEMENT_DATA
					.error(" 参数fontName为null. fontName=" + fontName
							+ ";mapElemType=" + mapElemType);
			throw new IllegalArgumentException(ErrorInfoConstant.PARA_OBJ_FAIL);
		}
		this.fontName = fontName;
	}

	public Color getShadowColor() {
		return shadowColor;
	}

	public void setShadowColor(Color shadowColor) {
		this.shadowColor = shadowColor;
	}

	public int getBodyWidth() {
		return bodyWidth;
	}

	public void setBodyWidth(int bodyWidth) {
		if (bodyWidth < 0 || bodyWidth >= 11) {
			JecnLogConstants.LOG_DEFAULT_FLOW_ELEMENT_DATA
					.error(" 参数bodyWidth必须在[0~10]之间整数. bodyWidth=" + bodyWidth
							+ ";mapElemType=" + mapElemType);
			throw new IllegalArgumentException(ErrorInfoConstant.PARA_OBJ_FAIL);
		}
		this.bodyWidth = bodyWidth;
	}

	public boolean getFlag3D() {
		return flag3D;
	}

	public void setFlag3D(boolean flag3D) {
		this.flag3D = flag3D;
	}

	public boolean isUpdateFlag() {
		return updateFlag;
	}

	public void setUpdateFlag(boolean updateFlag) {
		this.updateFlag = updateFlag;
	}

	public MapElemType getMapElemType() {
		return mapElemType;
	}

	public void setMapElemType(MapElemType mapElemType) {
		if (mapElemType == null) {
			JecnLogConstants.LOG_DEFAULT_FLOW_ELEMENT_DATA
					.error(" 参数mapElemType为NULL. mapElemType=" + mapElemType);
			throw new IllegalArgumentException(ErrorInfoConstant.PARA_OBJ_FAIL);
		}
		this.mapElemType = mapElemType;
	}

	public String getFigureText() {
		return figureText;
	}

	public void setFigureText(String figureText) {
		this.figureText = figureText;
	}

	public Color getChangeColor() {
		return changeColor;
	}

	public void setChangeColor(Color changeColor) {
		this.changeColor = changeColor;
	}

	public FillColorChangType getFillColorChangType() {
		return fillColorChangType;
	}

	public void setFillColorChangType(FillColorChangType fillColorChangType) {
		if (fillColorChangType == null) {
			JecnLogConstants.LOG_DEFAULT_FLOW_ELEMENT_DATA
					.error(" 参数fillColorChangType为NULL. fillColorChangType="
							+ fillColorChangType + ";mapElemType="
							+ mapElemType);
			throw new IllegalArgumentException(ErrorInfoConstant.PARA_OBJ_FAIL);
		}
		this.fillColorChangType = fillColorChangType;
	}

	public FillType getFillType() {
		return fillType;
	}

	public void setFillType(FillType fillType) {
		if (fillType == null) {
			JecnLogConstants.LOG_DEFAULT_FLOW_ELEMENT_DATA
					.error(" 参数fillType为NULL. fillType=" + fillType
							+ ";mapElemType=" + mapElemType);
			throw new IllegalArgumentException(ErrorInfoConstant.PARA_OBJ_FAIL);
		}
		this.fillType = fillType;
	}

	public boolean getEidtPoint() {
		return eidtPoint;
	}

	public void setEidtPoint(boolean eidtPoint) {
		this.eidtPoint = eidtPoint;
	}

	public boolean getDoubleClick() {
		return isDoubleClick;
	}

	public void setDoubleClick(boolean isDoubleClick) {
		this.isDoubleClick = isDoubleClick;
	}

	public boolean getSortAddFigure() {
		return isSortAddFigure;
	}

	public void setSortAddFigure(boolean isSortAddFigure) {
		this.isSortAddFigure = isSortAddFigure;
	}

	/**
	 * 
	 * 通过流程元素默认数据来初始化流程元素对象数据
	 * 
	 * @param flowElementData
	 *            JecnAbstractFlowElementData 流程元素数据
	 * @param defaultFlowElementData
	 *            JecnDefaultFlowElementData 流程元素默认数据
	 */
	public void defaultDataToFlowData(
			JecnAbstractFlowElementData flowElementData,
			JecnDefaultFlowElementData defaultFlowElementData) {
		if (flowElementData == null || defaultFlowElementData == null) {
			return;
		}

		// ******************** 老版配置 START ********************//
		// 边框颜色
		flowElementData.setBodyColor(defaultFlowElementData.getBodyColor());
		// 边框类型：0 1 2 3
		flowElementData.setBodyStyle(defaultFlowElementData.getBodyStyle());
		// 流程元素宽
		flowElementData.setFigureSizeX(defaultFlowElementData.getFigureSizeX());
		// 流程元素高
		flowElementData.setFigureSizeY(defaultFlowElementData.getFigureSizeY());

		// 填充类型（填充类型：无填充、单色、双色）
		flowElementData.setFillType(defaultFlowElementData.getFillType());
		// 填充颜色
		flowElementData.setFillColor(defaultFlowElementData.getFillColor());
		// 填充颜色2
		flowElementData.setChangeColor(defaultFlowElementData.getChangeColor());
		// 渐变方向类型(topTilt:向上倾斜 bottom:向下倾斜 vertical:垂直 horizontal:水平)
		flowElementData.setFillColorChangType(defaultFlowElementData
				.getFillColorChangType());

		// 字体颜色
		flowElementData.setFontColor(defaultFlowElementData.getFontColor());
		// 字体位置 0:左上 1：正上 2：右上 3：左中 4：正中 5：右中 6：左下 7：正下 8：右下
		flowElementData.setFontPosition(defaultFlowElementData
				.getFontPosition());
		// 字体大小
		flowElementData.setFontSize(defaultFlowElementData.getFontSize());
		// 字体竖排 1:竖排文字，0：不是竖排文字
		flowElementData.setFontUpRight(DrawCommon
				.converIntToBoolean(defaultFlowElementData.getUpRight()));
		// ******************** 老版配置 END ********************//

		// 字形样式（0:普通 1:加粗、2:斜线） 目前支持加粗和普通
		flowElementData.setFontStyle(defaultFlowElementData.getFontStyle());
		// 字体名称 默认宋体
		flowElementData.setFontName(defaultFlowElementData.getFontName());
		// 阴影 0:无阴影 1：有阴影 True：存在阴影，False：不存在阴影
		flowElementData.setShadowsFlag(DrawCommon
				.converIntToBoolean(defaultFlowElementData.getShadow()));
		// 阴影颜色
		flowElementData.setShadowColor(defaultFlowElementData.getShadowColor());

		// 线条粗细 (1~10)
		flowElementData.setBodyWidth(defaultFlowElementData.getBodyWidth());
		// 3D效果 True：存在3D效果，False：不存在3D效果
		flowElementData.setFlag3D(defaultFlowElementData.getFlag3D());
		// 是否显示编辑点
		flowElementData.setEditPoint(defaultFlowElementData.getEidtPoint());
		// 设置流程元素默认名称
		flowElementData.setFigureText(defaultFlowElementData.getFigureText());
		// 是否支持双击：isDoubleClick
		flowElementData.setDoubleClick(defaultFlowElementData.getDoubleClick());
		// 是否支持快速添加图形：isSortAddFigure
		flowElementData.setSortAddFigure(defaultFlowElementData
				.getSortAddFigure());
		// 旋转角度值
		flowElementData.setCurrentAngle(0.0);
	}

	/**
	 * 
	 * 流程元素数据->流程元素默认数据
	 * 
	 * @param flowElementData
	 *            JecnAbstractFlowElementData 流程元素数据
	 * @param defaultFlowElementData
	 *            JecnDefaultFlowElementData 流程元素默认数据
	 */
	public void flowDataToDefaultData(
			JecnAbstractFlowElementData flowElementData,
			JecnDefaultFlowElementData defaultFlowElementData) {

		if (flowElementData == null || defaultFlowElementData == null) {
			return;
		}

		// ******************** 老版配置 START ********************//
		// 更新标识
		boolean updateFlag = false;

		// 边框颜色
		if (!DrawCommon.checkColorSame(flowElementData.getBodyColor(),
				defaultFlowElementData.getBodyColor())) {
			defaultFlowElementData.setBodyColor(flowElementData.getBodyColor());
			updateFlag = true;
		}

		// 边框类型：0 1 2 3
		if (flowElementData.getBodyStyle() != defaultFlowElementData
				.getBodyStyle()) {
			defaultFlowElementData.setBodyStyle(flowElementData.getBodyStyle());
			updateFlag = true;
		}

		// 流程元素宽
		if (flowElementData.getFigureSizeX() != defaultFlowElementData
				.getFigureSizeX()) {
			defaultFlowElementData.setFigureSizeX(flowElementData
					.getFigureSizeX());
			updateFlag = true;
		}

		// 流程元素高
		if (flowElementData.getFigureSizeY() != defaultFlowElementData
				.getFigureSizeY()) {
			defaultFlowElementData.setFigureSizeY(flowElementData
					.getFigureSizeY());
			updateFlag = true;
		}

		// 填充类型（填充类型：无填充、单色、双色）
		if (flowElementData.getFillType() != defaultFlowElementData
				.getFillType()) {
			defaultFlowElementData.setFillType(flowElementData.getFillType());
			updateFlag = true;
		}

		// 填充颜色
		if (!DrawCommon.checkColorSame(flowElementData.getFillColor(),
				defaultFlowElementData.getFillColor())) {
			defaultFlowElementData.setFillColor(flowElementData.getFillColor());
			updateFlag = true;
		}

		// 填充颜色2
		if (!DrawCommon.checkColorSame(flowElementData.getChangeColor(),
				defaultFlowElementData.getChangeColor())) {
			defaultFlowElementData.setChangeColor(flowElementData
					.getChangeColor());
			updateFlag = true;
		}

		// 渐变方向类型(topTilt:向上倾斜 bottom:向下倾斜 vertical:垂直 horizontal:水平)
		if (flowElementData.getFillColorChangType() != defaultFlowElementData
				.getFillColorChangType()) {
			defaultFlowElementData.setFillColorChangType(flowElementData
					.getFillColorChangType());
			updateFlag = true;
		}

		// 字体颜色
		if (!DrawCommon.checkColorSame(flowElementData.getFontColor(),
				defaultFlowElementData.getFontColor())) {
			defaultFlowElementData.setFontColor(flowElementData.getFontColor());
			updateFlag = true;
		}

		// 字体位置 0:左上 1：正上 2：右上 3：左中 4：正中 5：右中 6：左下 7：正下 8：右下
		if (flowElementData.getFontPosition() != defaultFlowElementData
				.getFontPosition()) {
			defaultFlowElementData.setFontPosition(flowElementData
					.getFontPosition());
			updateFlag = true;
		}

		// 字体大小
		if (flowElementData.getFontSize() != defaultFlowElementData
				.getFontSize()) {
			defaultFlowElementData.setFontSize(flowElementData.getFontSize());
			updateFlag = true;
		}

		// 字体竖排 1:竖排文字，0：不是竖排文字
		int fontUpRight = DrawCommon.converBooleanToInt(flowElementData
				.getFontUpRight());
		if (fontUpRight != defaultFlowElementData.getUpRight()) {
			defaultFlowElementData.setUpRight(fontUpRight);
			updateFlag = true;
		}
		// ******************** 老版配置 END ********************//

		// 字形样式（0:普通 1:加粗、2:斜线） 目前支持加粗和普通
		if (flowElementData.getFontStyle() != defaultFlowElementData
				.getFontStyle()) {
			defaultFlowElementData.setFontStyle(flowElementData.getFontStyle());
			updateFlag = true;
		}

		// 字体名称 默认宋体
		if (!DrawCommon.checkStringSame(flowElementData.getFontName(),
				defaultFlowElementData.getFontName())) {
			defaultFlowElementData.setFontName(flowElementData.getFontName());
			updateFlag = true;
		}

		// 阴影 0:无阴影 1：有阴影
		int isShadowsFlag = DrawCommon.converBooleanToInt(flowElementData
				.isShadowsFlag());
		if (isShadowsFlag != defaultFlowElementData.getShadow()) {
			defaultFlowElementData.setShadow(isShadowsFlag);
			updateFlag = true;
		}
		// 阴影颜色
		if (!DrawCommon.checkColorSame(flowElementData.getShadowColor(),
				defaultFlowElementData.getShadowColor())) {
			defaultFlowElementData.setShadowColor(flowElementData
					.getShadowColor());
			updateFlag = true;
		}

		// 线条粗细 (1~10)
		if (flowElementData.getBodyWidth() != defaultFlowElementData
				.getBodyWidth()) {
			defaultFlowElementData.setBodyWidth(flowElementData.getBodyWidth());
			updateFlag = true;
		}

		// 3D效果 True：存在3D效果，False：不存在3D效果
		if (flowElementData.isFlag3D() != defaultFlowElementData.getFlag3D()) {
			defaultFlowElementData.setFlag3D(flowElementData.isFlag3D());
			updateFlag = true;
		}

		// 是否显示编辑点
		if (flowElementData.getEditPoint() != defaultFlowElementData
				.getEidtPoint()) {
			defaultFlowElementData.setEidtPoint(flowElementData.getEditPoint());
			updateFlag = true;
		}

		// 设置流程元素默认名称
		if (!DrawCommon.checkStringSame(flowElementData.getFigureText(),
				defaultFlowElementData.getFigureText())) {
			defaultFlowElementData.setFigureText(flowElementData
					.getFigureText());
			updateFlag = true;
		}

		if (updateFlag) {// 更新标识
			defaultFlowElementData.setUpdateFlag(updateFlag);
		}
	}

	public long getFlowElementId() {
		return flowElementId;
	}

	public void setFlowElementId(long flowElementId) {
		this.flowElementId = flowElementId;
	}
}
