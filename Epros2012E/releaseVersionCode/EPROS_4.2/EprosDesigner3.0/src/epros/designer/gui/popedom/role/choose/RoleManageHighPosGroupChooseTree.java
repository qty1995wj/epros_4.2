package epros.designer.gui.popedom.role.choose;

import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.jecn.epros.server.bean.tree.JecnTreeBean;

import epros.designer.gui.common.JecnConstants;
import epros.designer.gui.popedom.positiongroup.PosGroupCommon;
import epros.designer.tree.JecnHighEfficiencyTree;
import epros.designer.tree.JecnTreeListener;
import epros.designer.tree.JecnTreeModel;
import epros.designer.util.ConnectionPool;

public class RoleManageHighPosGroupChooseTree extends JecnHighEfficiencyTree {
	private static Logger log = Logger.getLogger(RoleManageHighPosGroupChooseTree.class);
	private List<JecnTreeBean> list = new ArrayList<JecnTreeBean>();

	private RoleManagePosGropChooseDialog posGropChooseDialog = null;

	public RoleManageHighPosGroupChooseTree(RoleManagePosGropChooseDialog posGropChooseDialog) {
		this.posGropChooseDialog = posGropChooseDialog;
	}

	@Override
	public JecnTreeListener getTreeExpansionListener(JecnHighEfficiencyTree jTree) {
		return new RoleManagePosGroupChooseTreeListener(jTree);
	}

	@Override
	public JecnTreeModel getTreeModel() {
		try {
			list = ConnectionPool.getPosGroup().getChildPositionGroup(Long.valueOf(0), JecnConstants.projectId,JecnConstants.getUserId());
		} catch (Exception e) {
			log.error("HighPosGroupChooseTree getTreeModel is error！", e);
		}
		return PosGroupCommon.getPosGroupTreeModel(list);
	}

	@Override
	public boolean isSelectMutil() {
		return true;
	}

	@Override
	public void jTreeMousePressed(MouseEvent evt) {
	}

}
