package com.jecn.webservice.puyuan.bean;

/**
 * 元素连接线
 * 
 * @author chehuanbo
 * @date 2014-01-19
 */
public class FlowLink {
     
	/**连接线ID*/
	private Long linkId;
	/*** From节点 ******/
	private Long fromNodeId;
	/*** To节点 ******/
	private Long toNodeId;
	/** 连接线名称 */
	private String linkName;

	
	
	public String getLinkId() {
		if(linkId == null){
		  return "";	
		}
		return String.valueOf(linkId);
	}

	public void setLinkId(Long linkId) {
		this.linkId = linkId;
	}

	public void setFromNodeId(Long fromNodeId) {
		this.fromNodeId = fromNodeId;
	}

	public void setToNodeId(Long toNodeId) {
		this.toNodeId = toNodeId;
	}

	public String getFromNodeId() {
		if(fromNodeId == null){
			return "";
		}
		return String.valueOf(fromNodeId);
	}

	public void setFromNodeId(long fromNodeId) {
		this.fromNodeId = fromNodeId;
	}

	public String getToNodeId() {
		if(toNodeId == null){
			return "";
		}
		return String.valueOf(toNodeId);
	}

	public void setToNodeId(long toNodeId) {
		this.toNodeId = toNodeId;
	}

	public String getLinkName() {
		if(linkName == null){
			return "";
		}
		return linkName;
	}

	public void setLinkName(String linkName) {
		this.linkName = linkName;
	}

}
