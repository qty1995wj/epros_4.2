package com.jecn.epros.server.webBean;

import java.util.List;

import com.jecn.epros.server.webBean.process.ProcessActiveWebBean;

public class RoleBean {

	/** 角色ID */
	private long roleId;
	/** 角色名称 */
	private String roleName;
	/** 角色职责 */
	private String roleShow;
	/** 角色对应的活动的List */
	List<ProcessActiveWebBean> processActiveWebList;

	public long getRoleId() {
		return roleId;
	}

	public void setRoleId(long roleId) {
		this.roleId = roleId;
	}

	public String getRoleName() {
		return roleName;
	}

	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}

	public String getRoleShow() {
		return roleShow;
	}

	public void setRoleShow(String roleShow) {
		this.roleShow = roleShow;
	}

	public List<ProcessActiveWebBean> getProcessActiveWebList() {
		return processActiveWebList;
	}

	public void setProcessActiveWebList(
			List<ProcessActiveWebBean> processActiveWebList) {
		this.processActiveWebList = processActiveWebList;
	}
}
