package epros.draw.gui.workflow.util;

import java.awt.Point;
import java.util.Date;
import java.util.List;

import epros.draw.constant.JecnToolBoxConstant.MapElemType;
import epros.draw.constant.JecnWorkflowConstant.LineDirectionEnum;
import epros.draw.data.figure.JecnFigureData;
import epros.draw.data.line.JecnVHLineData;
import epros.draw.event.flowElement.JecnVHLineEventProcess;
import epros.draw.gui.figure.JecnBaseFigurePanel;
import epros.draw.gui.figure.shape.DateFreeText;
import epros.draw.gui.figure.shape.MapNameText;
import epros.draw.gui.figure.shape.part.CommentText;
import epros.draw.gui.figure.shape.total.ModelFigure;
import epros.draw.gui.figure.unit.JecnDraggedFiguresPaintLine;
import epros.draw.gui.figure.unit.JecnFigureRefManhattanLine;
import epros.draw.gui.figure.unit.JecnSelectFigureXY;
import epros.draw.gui.line.JecnBaseDividingLinePanel;
import epros.draw.gui.line.JecnBaseManhattanLinePanel;
import epros.draw.gui.line.JecnBaseVHLinePanel;
import epros.draw.gui.line.shape.DividingLine;
import epros.draw.gui.swing.JecnBaseFlowElementPanel;
import epros.draw.gui.workflow.JecnDrawMainPanel;
import epros.draw.gui.workflow.tabbedPane.JecnDrawDesktopPane;
import epros.draw.system.data.JecnSystemStaticData;
import epros.draw.unredo.JecnUnRedoProcess;
import epros.draw.unredo.JecnUnRedoProcessFactory;
import epros.draw.unredo.data.JecnUndoRedoData;
import epros.draw.util.DrawCommon;

/**
 * 面板添加流程元素处理类
 * 
 * @author ZHAGNXH
 * @date： 日期：May 31, 2012 时间：2:51:04 PM
 */
public class JecnAddFlowElementUnit {

	/**
	 * 添加流程元素到面板
	 * 
	 * @param point
	 *            Point 鼠标相对面板的位置点
	 * @param flowElement
	 *            JecnBaseFlowElementPanel流程元素
	 */
	public static void addFlowElement(Point point, JecnBaseFlowElementPanel flowElement) {
		if (flowElement == null || point == null) {
			return;
		}
		if (flowElement instanceof JecnBaseFigurePanel) {// 添加图形
			if (JecnSystemStaticData.isShowModel() && flowElement instanceof ModelFigure) {
				JecnSelectFigureXY selectFigureXY = JecnDrawMainPanel.getMainPanel().getWorkflow().getFlowMapData()
						.getModelFigureXY();
				if (selectFigureXY.getMaxX() != 0 && selectFigureXY.getMaxY() != 0) {
					double multiple = JecnDrawMainPanel.getMainPanel().getWorkflow().getWorkflowScale();
					point.setLocation(selectFigureXY.getMinX(), selectFigureXY.getMaxY());
					flowElement.getFlowElementData().setFigureSizeX(
							DrawCommon.convertDoubleToInt(selectFigureXY.getMaxX() / multiple
									- selectFigureXY.getMinX() / multiple));
				}
			}
			addFigure(point, (JecnBaseFigurePanel) flowElement);
		} else if (flowElement instanceof JecnBaseVHLinePanel) {// 添加横竖分割线
			addVHLine((JecnBaseVHLinePanel) flowElement, point);
		} else if (flowElement instanceof DividingLine) {// 添加分割线
			DividingLine dividingLine = (DividingLine) flowElement;
			addDividingLine(dividingLine);
		} else if (flowElement instanceof JecnBaseManhattanLinePanel) {// 添加连接线
			JecnBaseManhattanLinePanel linePanel = (JecnBaseManhattanLinePanel) flowElement;
			addManhattinLine(linePanel);
		} else if (flowElement instanceof JecnBaseDividingLinePanel) {
			JecnBaseDividingLinePanel baseVHDiviLinePanel = (JecnBaseDividingLinePanel) flowElement;
			addVHDiviLinePanel(baseVHDiviLinePanel, point);
		}
	}

	/**
	 * 点击流程元素库添分割线
	 * 
	 * @param baseVHDiviLinePanel
	 * @param point
	 */
	public static void addVHDiviLinePanel(JecnBaseDividingLinePanel baseVHDiviLinePanel, Point point) {
		if (JecnDrawMainPanel.getMainPanel().getWorkflow() == null || baseVHDiviLinePanel == null || point == null) {
			return;
		}
		// 根据位置点设置分割线位置及大小、创建克隆数据
		baseVHDiviLinePanel.setVHDiviLineBounds(point);
		// 获取当前面板
		JecnDrawDesktopPane drawDesktopPane = JecnDrawMainPanel.getMainPanel().getWorkflow();
		// 添加流程元素到面板集合
		drawDesktopPane.addJecnPanel(baseVHDiviLinePanel);
	}

	/**
	 * 全选
	 */
	public static void selectAllFigure() {
		// 获取当前面板
		JecnDrawDesktopPane drawDesktopPane = JecnDrawMainPanel.getMainPanel().getWorkflow();
		if (drawDesktopPane.getAidCompCollection().getEditScrollPane().isVisible()) {
			return;
		}
		// 获取所有的图形
		List<JecnBaseFlowElementPanel> allFigureList = drawDesktopPane.getPanelList();
		// 选中所有图形
		for (JecnBaseFlowElementPanel baseFlowElementPanel : allFigureList) {
			if (!(baseFlowElementPanel instanceof JecnBaseVHLinePanel)) {
				JecnWorkflowUtil.setCurrentSelect(baseFlowElementPanel);
			}
		}
	}

	/**
	 * 导入添加分割线
	 * 
	 * @param baseVHDiviLinePanel
	 */
	public static void importAddDiviLine(JecnBaseDividingLinePanel baseVHDiviLinePanel) {
		if (JecnDrawMainPanel.getMainPanel().getWorkflow() == null || baseVHDiviLinePanel == null) {
			return;
		}
		// 获取当前面板
		JecnDrawDesktopPane drawDesktopPane = JecnDrawMainPanel.getMainPanel().getWorkflow();
		// 添加流程元素到面板集合
		drawDesktopPane.addJecnPanel(baseVHDiviLinePanel);
		drawDesktopPane.updateUI();
	}

	/**
	 * 添加连接线到面板
	 * 
	 * @param linePanel
	 *            JecnBaseManhattanLinePanel需要添加的曼哈顿线
	 */
	public static void addManhattinLine(JecnBaseManhattanLinePanel linePanel) {
		if (JecnDrawMainPanel.getMainPanel().getWorkflow() == null || linePanel.getStartFigure() == null
				|| linePanel.getEndFigure() == null) {
			return;
		}
		// 获取当前面板
		JecnDrawDesktopPane workFlow = JecnDrawMainPanel.getMainPanel().getWorkflow();
		// 设置图形随机位移标识UUID
		if (DrawCommon.isNullOrEmtryTrim(linePanel.getFlowElementData().getFlowElementUUID())) {
			linePanel.getFlowElementData().setFlowElementUUID(DrawCommon.getUUID());
		}

		// 设置连接线的开始图形和结束图形与连接线的关系数据
		addRefManhattLineToFigure(linePanel);

		if (linePanel.isDrraged()) {
			JecnDraggedFiguresPaintLine.draggedFigureDrawLine(linePanel);
		} else {
			// 执行连接线默认曼哈顿算法
			linePanel.paintLineByRouter();
		}

		// 获取待添加的连接线开始图形和结束图形主键ID和UUID，并赋值给联系数据对象
		setLineDataByFigure(linePanel);
		// 添加连接下到图形元素集合
		workFlow.addJecnPanel(linePanel);
		workFlow.activeLine = null;
		workFlow.getTempDividingList().clear();
		workFlow.updateUI();
	}

	public static void addMapManhattinLine(JecnBaseManhattanLinePanel linePanel) {
		if (linePanel == null) {
			return;
		}
		// 获取当前面板
		JecnDrawDesktopPane workFlow = JecnDrawMainPanel.getMainPanel().getWorkflow();
		// 设置图形随机位移标识UUID
		if (DrawCommon.isNullOrEmtryTrim(linePanel.getFlowElementData().getFlowElementUUID())) {
			linePanel.getFlowElementData().setFlowElementUUID(DrawCommon.getUUID());
		}

		// 设置连接线的开始图形和结束图形与连接线的关系数据
		addRefManhattLineToFigure(linePanel);

		if (linePanel.isDrraged()) {
			JecnDraggedFiguresPaintLine.draggedFigureDrawLine(linePanel);
		} else {
			// 执行连接线默认曼哈顿算法
			linePanel.paintLineByRouter();
			// linePanel.paintDraggedLinePort(linePanel.getArrowStartPoint(),
			// linePanel.getFlowElementData()
			// .getStartEditPointType(), linePanel.getArrowEndPoint(),
			// linePanel.getFlowElementData()
			// .getEndEditPointType());
		}

		// 获取待添加的连接线开始图形和结束图形主键ID和UUID，并赋值给联系数据对象
		// setLineDataByFigure(linePanel);
		// 添加连接下到图形元素集合
		workFlow.addJecnPanel(linePanel);
		workFlow.activeLine = null;
		workFlow.getTempDividingList().clear();
		workFlow.updateUI();
	}

	/**
	 * 删除连接线撤销 添加连接线算法处理
	 * 
	 * @param linePanel
	 */
	public static void addDeleteUndoManLine(JecnBaseManhattanLinePanel linePanel) {
		// 获取当前面板
		JecnDrawDesktopPane workFlow = JecnDrawMainPanel.getMainPanel().getWorkflow();
		if (workFlow == null || linePanel.getStartFigure() == null || linePanel.getEndFigure() == null) {
			return;
		}
		linePanel.addArrowAndTextPanel();
		// 设置连接线的开始图形和结束图形与连接线的关系数据
		addRefManhattLineToFigure(linePanel);

		// 获取待添加的连接线开始图形和结束图形主键ID和UUID，并赋值给联系数据对象
		setLineDataByFigure(linePanel);
		// 添加连接下到图形元素集合
		workFlow.addJecnPanel(linePanel);
		workFlow.activeLine = null;
		workFlow.getTempDividingList().clear();
		workFlow.updateUI();
	}

	/**
	 * 获取待添加的连接线开始图形和结束图形主键ID和UUID，并赋值给联系数据对象
	 * 
	 * @param linePanel
	 *            待添加的连接线
	 */
	public static void setLineDataByFigure(JecnBaseManhattanLinePanel linePanel) {
		if (linePanel == null) {
			return;
		}
		// 开始图形UUID
		String startFigureUUID = linePanel.getStartFigure().getFlowElementData().getFlowElementUUID();
		// 结束图形UUID
		String endFigureUUID = linePanel.getEndFigure().getFlowElementData().getFlowElementUUID();

		// 设置图形UUID与连接线数据层关联
		linePanel.getFlowElementData().setStartFigureUUID(startFigureUUID);

		// 设置图形UUID与连接线数据层关联
		linePanel.getFlowElementData().setEndFigureUUID(endFigureUUID);

		// 设置开始图形主键ID关联到连接线数据层
		Long startId = linePanel.getStartFigure().getFlowElementData().getFlowElementId();
		if (startId != null) {
			linePanel.getFlowElementData().setStartId(startId);
		}
		// 设置结束图形主键ID关联到连接线数据层
		Long endId = linePanel.getEndFigure().getFlowElementData().getFlowElementId();
		if (endId != null) {
			linePanel.getFlowElementData().setEndId(endId);
		}
	}

	/**
	 * 设置连接线的开始图形和结束图形与连接线的关系数据
	 * 
	 * @param linePanel
	 *            连接线
	 * @param startEditPort
	 *            开始图形编辑点
	 * @param endEditPort
	 *            结束图形编辑点
	 */
	public static void addRefManhattLineToFigure(JecnBaseManhattanLinePanel linePanel) {
		// 设置连接线开始图形与线段的关联数据
		JecnFigureRefManhattanLine refStartManhattanLine = new JecnFigureRefManhattanLine(linePanel, linePanel
				.getFlowElementData().getStartEditPointType(), true);

		// 设置连接线结束图形与线段的关联数据
		linePanel.getStartFigure().getListFigureRefManhattanLine().add(refStartManhattanLine);

		// 设置图形端口关联线段
		JecnFigureRefManhattanLine refEndManhattanLine = new JecnFigureRefManhattanLine(linePanel, linePanel
				.getFlowElementData().getEndEditPointType(), false);

		// 添加关联线段数据到图形
		linePanel.getEndFigure().getListFigureRefManhattanLine().add(refEndManhattanLine);
	}

	/**
	 * 
	 * 添加图形
	 * 
	 * @param drawDesktopPane
	 *            JecnDrawDesktopPane 画图面板
	 * @param point
	 *            Point 图形位置点
	 * @param figure
	 *            JecnBaseFigurePanel 图形
	 * @return boolean false:不是图形或创建图形失败 true：创建图形成功
	 */
	public static boolean addFigure(Point point, JecnBaseFigurePanel figure) {
		if (JecnDrawMainPanel.getMainPanel().getWorkflow() == null || figure == null) {// 图形对象是NULL
			// 不添加
			return false;
		}
		addFigure(figure);
		// 设置图形大小和位置点
		figure.setSizeAndLocation(point);
		return true;
	}

	/**
	 * 添加图形,当图形时关键活动（PA,KSF,KCP）、协作框、泳池时默认置底
	 * 
	 * @param figure
	 *            图形
	 * @return boolean false:不是图形或创建图形失败 true：创建图形成功
	 */
	public static boolean addFigure(JecnBaseFigurePanel figure) {
		if (JecnDrawMainPanel.getMainPanel().getWorkflow() == null || figure == null) {// 图形对象是NULL
			// 不添加
			return false;
		}
		JecnDrawDesktopPane drawDesktopPane = JecnDrawMainPanel.getMainPanel().getWorkflow();
		// 图形数据对象
		JecnFigureData figureData = (JecnFigureData) figure.getFlowElementData();
		// 如果为图名称输入框 重置名称
		if (figure instanceof MapNameText) {
			figure.getFlowElementData().setFigureText(drawDesktopPane.getFlowMapData().getName());
		} else if (figure instanceof DateFreeText) {
			// 根据跟定日期转换字符串日期格式yyyy-MM-dd
			String strDate = JecnWorkflowUtil.getStringbyDate(new Date());
			figure.getFlowElementData().setFigureText(strDate);
		} else if (JecnWorkflowUtil.isAddLayoutBottom(figure)) {// 给定图形是否添加需要置底，添加需要置底的图形：关键活动（PA,KSF,KCP）、协作框、泳池
			if (figure.getFlowElementData().getFlowElementId() == -1) {
				// 把图形置于底层
				drawDesktopPane.getGraphicsLevel().setDownFigureLayerProcess(figure, new JecnUndoRedoData(),
						new JecnUndoRedoData());
			}
		}
		// 设置图形随机位移标识UUID
		if (DrawCommon.isNullOrEmtryTrim(figureData.getFlowElementUUID())) {
			figureData.setFlowElementUUID(DrawCommon.getUUID());
		}

		// 添加流程元素到面板集合
		drawDesktopPane.addJecnPanel(figure);

		// 设置阴影层级
		figure.reSetFlowElementZorder();

		if (figure instanceof CommentText) {
			CommentText commentText = (CommentText) figure;
			// commentText.addCommonLine();
			// 添加图形画图面板
			drawDesktopPane.add(commentText.commentLine);
			drawDesktopPane.setCommentLineList(commentText.commentLine);
		}
		return true;
	}

	/**
	 * 添加图形，层级不做特殊处理和addFigure方法一起满足不同情况下操作
	 * 
	 * @param figure
	 *            图形
	 * @return boolean false:不是图形或创建图形失败 true：创建图形成功
	 */
	// public static boolean openFigureByDB(JecnBaseFigurePanel figure) {
	// if (JecnDrawMainPanel.getMainPanel().getWorkflow() == null || figure ==
	// null) {// 图形对象是NULL
	// // 不添加
	// return false;
	// }
	// JecnDrawDesktopPane drawDesktopPane =
	// JecnDrawMainPanel.getMainPanel().getWorkflow();
	// // 图形数据对象
	// JecnFigureData figureData = (JecnFigureData) figure.getFlowElementData();
	// // 如果为图名称输入框 重置名称
	// if (figure instanceof MapNameText) {
	// figure.getFlowElementData().setFigureText(drawDesktopPane.getFlowMapData().getName());
	// } else if (figure instanceof DateFreeText) {
	// // 根据跟定日期转换字符串日期格式yyyy-MM-dd
	// String strDate = JecnWorkflowUtil.getStringbyDate(new Date());
	// figure.getFlowElementData().setFigureText(strDate);
	// }
	// // 设置图形随机位移标识UUID
	// if (DrawCommon.isNullOrEmtryTrim(figureData.getFlowElementUUID())) {
	// figureData.setFlowElementUUID(DrawCommon.getUUID());
	// }
	//
	// // 添加流程元素到面板集合
	// drawDesktopPane.addJecnPanel(figure);
	// if (figure instanceof CommentText) {
	// CommentText commentText = (CommentText) figure;
	// // commentText.addCommonLine();
	// // 添加图形画图面板
	// drawDesktopPane.add(commentText.commentLine);
	// drawDesktopPane.setCommentLineList(commentText.commentLine);
	// }
	//
	// return true;
	// }

	/**
	 * 
	 * 添加横竖分割线
	 * 
	 * @param drawDesktopPane
	 *            JecnDrawDesktopPane 画图面板 面板
	 * @param vhLine
	 *            JecnBaseVHLinePanel 横竖分割线
	 * @param point
	 *            Point 鼠标点击的点
	 * @return boolean false: 不是横竖分割线或创建横竖分割线失败 true：创建横竖分割线成功
	 */
	public static boolean addVHLine(JecnBaseVHLinePanel vhLine, Point point) {
		if (JecnDrawMainPanel.getMainPanel().getWorkflow() == null || vhLine == null || point == null) {
			return false;
		}
		// 设置图形随机位移标识UUID
		if (DrawCommon.isNullOrEmtryTrim(vhLine.getFlowElementData().getFlowElementUUID())) {
			vhLine.getFlowElementData().setFlowElementUUID(DrawCommon.getUUID());
		}
		JecnDrawDesktopPane drawDesktopPane = JecnDrawMainPanel.getMainPanel().getWorkflow();
		// 设置X、Y坐标
		vhLine.getFlowElementData().setVHXY(point);
		// 设置位置点和大小
		vhLine.setVHLineBounds();
		// 添加图形画图面板
		drawDesktopPane.addJecnPanel(vhLine);
		return true;
	}

	/**
	 * 面板添加分割线
	 * 
	 * @param drawDesktopPane
	 *            JecnDrawDesktopPane面板
	 * @param dividingLine
	 *            DividingLine 分割线
	 * @param point
	 *            Point 位置点
	 * @return true：添加分割线成功，false 添加失败
	 */
	public static boolean addDividingLine(JecnBaseDividingLinePanel dividingLine) {
		if (JecnDrawMainPanel.getMainPanel().getWorkflow() == null || dividingLine == null) {
			return false;
		}
		JecnDrawDesktopPane drawDesktopPane = JecnDrawMainPanel.getMainPanel().getWorkflow();

		// 添加图形画图面板
		drawDesktopPane.addJecnPanel(dividingLine);
		if (drawDesktopPane.activeLine != null) {
			drawDesktopPane.remove(drawDesktopPane.activeLine);
			// 删除虚拟连接线
			drawDesktopPane.activeLine = null;
			// 清空虚拟线段数组
			drawDesktopPane.getTempDividingList().clear();
		}
		drawDesktopPane.updateUI();
		return true;
	}

	/**
	 * 添加连接线到面板
	 * 
	 * @param linePanel
	 *            JecnBaseManhattanLinePanel需要添加的曼哈顿线
	 */
	public static void addImportManLine(JecnBaseManhattanLinePanel linePanel) {
		if (JecnDrawMainPanel.getMainPanel().getWorkflow() == null || linePanel.getStartFigure() == null
				|| linePanel.getEndFigure() == null) {
			return;
		}
		// 获取当前面板
		JecnDrawDesktopPane workFlow = JecnDrawMainPanel.getMainPanel().getWorkflow();

		// 设置连接线的开始图形和结束图形与连接线的关系数据
		addRefManhattLineToFigure(linePanel);
		// 导入、片段连接线画线
		linePanel.paintImportManLine(linePanel);
		// 开始图形UUID
		String startFigureUUID = linePanel.getStartFigure().getFlowElementData().getFlowElementUUID();
		// 结束图形UUID
		String endFigureUUID = linePanel.getEndFigure().getFlowElementData().getFlowElementUUID();

		// 设置图形UUID与连接线数据层关联
		linePanel.getFlowElementData().setStartFigureUUID(startFigureUUID);

		// 设置图形UUID与连接线数据层关联
		linePanel.getFlowElementData().setEndFigureUUID(endFigureUUID);

		// 添加连接下到图形元素集合
		workFlow.addJecnPanel(linePanel);
		workFlow.activeLine = null;
		workFlow.getTempDividingList().clear();
		workFlow.updateUI();
	}

	/**
	 * 添加泳道
	 */
	public static void addModelLineAction() {
		JecnDrawDesktopPane desktopPane = JecnDrawMainPanel.getMainPanel().getWorkflow();
		// 获取面板上分割线
		List<JecnBaseVHLinePanel> listVHLinePanel = desktopPane.getVhLinePanelList();

		int mY = desktopPane.getStartPoint().y;
		int mX = desktopPane.getStartPoint().x;

		int standardY = 0;
		int standardX = 0;

		int maxY = 0;
		int maxX = 0;

		int minY = 0;
		int minX = 0;
		Point movePoint = null;
		int moveWH = 0;
		LineDirectionEnum vhEnum = null;

		if (desktopPane.getFlowMapData().isHFlag()) { // 面板横向
			for (JecnBaseVHLinePanel vhLinePanel : listVHLinePanel) {
				if (vhLinePanel.getFlowElementData().getLineDirectionEnum() == LineDirectionEnum.vertical) {
					continue;
				}
				// 获取分割线
				JecnVHLineData vHLineData = (JecnVHLineData) vhLinePanel.getFlowElementData();
				if (maxY == 0) {
					maxY = (int) vHLineData.getVhY();
					minY = (int) vHLineData.getVhY();
				}
				// 循环判断小于角色位置点的分割线，并且离角色最近的一条
				if (mY > vHLineData.getVhY() && standardY < vHLineData.getVhY()) {
					standardY = (int) vHLineData.getVhY();
				}
				if (vHLineData.getVhY() > maxY) {
					maxY = (int) vHLineData.getVhY();
				}
				if (vHLineData.getVhY() < minY) {
					minY = (int) vHLineData.getVhY();
				}
			}
			if (mY > maxY) {
				standardY = maxY;
			}

			if (mY < minY) {// 鼠标点击位置为第一条分割线上方区域
				standardY = minY;
			}
			vhEnum = LineDirectionEnum.horizontal;
			movePoint = new Point(0, standardY);
			moveWH = 100;
		} else {// 纵向图
			for (JecnBaseVHLinePanel vhLinePanel : listVHLinePanel) {
				if (vhLinePanel.getFlowElementData().getLineDirectionEnum() == LineDirectionEnum.horizontal) {
					continue;
				}
				// 获取分割线
				JecnVHLineData vHLineData = (JecnVHLineData) vhLinePanel.getFlowElementData();
				if (maxX == 0) {
					maxX = (int) vHLineData.getVhX();
					minX = (int) vHLineData.getVhX();
				}
				// 循环判断小于角色位置点的分割线，并且离角色最近的一条
				if (mX > vHLineData.getVhX() && standardX < vHLineData.getVhX()) {
					standardX = (int) vHLineData.getVhX();
				}
				if (vHLineData.getVhX() > maxX) {
					maxX = (int) vHLineData.getVhX();
				}
				if (vHLineData.getVhX() < minX) {
					minX = (int) vHLineData.getVhX();
				}
			}
			if (mX > maxX) {
				standardX = maxX;
			}

			if (mX < minX) {// 鼠标点击位置为第一条分割线上方区域
				standardX = minX;
			}
			vhEnum = LineDirectionEnum.vertical;
			moveWH = 150;
			movePoint = new Point(standardX + 5, 0);
		}

		addModelAndMoveVHLineFigures(movePoint, vhEnum, moveWH, vhEnum == LineDirectionEnum.vertical ? standardX
				: standardY);

		// 记录这次操作的数据集合
		JecnUnRedoProcess unredoProcess = JecnUnRedoProcessFactory.createAddAndEdit(JecnVHLineEventProcess.lineUnit
				.getUndoData(), JecnVHLineEventProcess.lineUnit.getRedoData());
		desktopPane.getUndoRedoManager().addEdit(unredoProcess);

		desktopPane.getCurrDrawPanelSizeByPanelList();
	}

	private static void addModelAndMoveVHLineFigures(Point movePoint, LineDirectionEnum vhEnum, int moveWH,
			int standardY) {
		JecnDrawDesktopPane desktopPane = JecnDrawMainPanel.getMainPanel().getWorkflow();

		JecnBaseFlowElementPanel flowElement = JecnCreateFlowElement.getFlowEmelentByType(DrawCommon
				.getCreateFlowRoleType());
		JecnBaseFigurePanel baseFigure = (JecnBaseFigurePanel) flowElement;

		// 节点移动
		JecnVHLineEventProcess.initFigureList(desktopPane.getPanelList(), vhEnum, movePoint, desktopPane
				.getJecnTempFigurePanel());

		JecnVHLineEventProcess.lineUnit.setAddModeRole(true);

		JecnVHLineEventProcess.lineUnit.moveAllFiguresAndLines(vhEnum == LineDirectionEnum.horizontal ? movePoint.y
				: movePoint.x, moveWH);

		// 计算坐标
		Point point = null;
		if (vhEnum == LineDirectionEnum.horizontal) {
			point = new Point(30, standardY + 20);
		} else {
			point = new Point(standardY + 25, 30);
		}
		JecnAddFlowElementUnit.addFigure(point, baseFigure);

		// 添加图形：记录撤销恢复数据
		JecnVHLineEventProcess.lineUnit.getRedoData().recodeFlowElementByAddEdit(flowElement);

		Point linePoint = null;
		if (vhEnum == LineDirectionEnum.horizontal) {
			linePoint = new Point(0, standardY);
		} else {
			linePoint = new Point(standardY, 0);
		}
		JecnBaseFlowElementPanel pLinePanel = JecnCreateFlowElement
				.createFlowElementObj(vhEnum == LineDirectionEnum.horizontal ? MapElemType.ParallelLines
						: MapElemType.VerticalLine);
		if (pLinePanel == null) {
			return;
		}
		JecnAddFlowElementUnit.addFlowElement(linePoint, pLinePanel);

		// 添加图形：记录撤销恢复数据
		JecnVHLineEventProcess.lineUnit.getRedoData().recodeVHLineByAddEdit(pLinePanel);
	}
}
