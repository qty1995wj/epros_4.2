package epros.designer.gui.define;

import java.awt.event.InputEvent;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.swing.tree.TreePath;

import org.apache.log4j.Logger;

import com.jecn.epros.server.bean.define.TermDefinitionLibrary;
import com.jecn.epros.server.bean.tree.JecnTreeBean;
import com.jecn.epros.server.util.JecnTreeEnumConstant.TreeNodeType;

import epros.designer.gui.common.JecnConstants;
import epros.designer.tree.JecnTree;
import epros.designer.tree.JecnTreeCommon;
import epros.designer.tree.JecnTreeModel;
import epros.designer.tree.JecnTreeNode;
import epros.designer.util.JecnProperties;

public class JecnTermDefineCommon {
	private static Logger log = Logger.getLogger(JecnTermDefineCommon.class);

	/**
	 * 创建TermDefinitionLibrary
	 * 
	 * @param name
	 * @param pNode
	 * @return
	 */
	public static TermDefinitionLibrary getTermDefinitionLibrary(String name, JecnTreeNode pNode, int isDir,
			String instructions) {
		TermDefinitionLibrary termDefinitionLibrary = new TermDefinitionLibrary();
		// 文件名称
		termDefinitionLibrary.setName(name);
		// 创建人
		termDefinitionLibrary.setCreatePersonId(JecnConstants.getUserId());
		// 更新人
		termDefinitionLibrary.setUpdatePersonId(JecnConstants.getUserId());
		// 父ID
		termDefinitionLibrary.setParentId(pNode.getJecnTreeBean().getId());
		// 是否是目录 0：目录，1：文件
		termDefinitionLibrary.setIsDir(isDir);
		// 创建时间
		termDefinitionLibrary.setCreateTime(new Date());
		termDefinitionLibrary.setUpdateTime(new Date());
		termDefinitionLibrary.setInstructions(instructions);
		termDefinitionLibrary.setSortId(JecnTreeCommon.getMaxSort(pNode));
		return termDefinitionLibrary;
	}

	public static JecnTreeBean getJecnTreeBean(TermDefinitionLibrary termDefinitionLibrary) {
		JecnTreeBean jecnTreeBean = new JecnTreeBean();
		jecnTreeBean.setId(termDefinitionLibrary.getId());
		jecnTreeBean.setName(termDefinitionLibrary.getName());
		jecnTreeBean.setPid(termDefinitionLibrary.getParentId());
		jecnTreeBean.setPname("");
		jecnTreeBean.setTreeNodeType(termDefinitionLibrary.getIsDir() == 0 ? TreeNodeType.termDefineDir
				: TreeNodeType.termDefine);
		return jecnTreeBean;
	}

	/***************************************************************************
	 * 获得术语定义树
	 * 
	 * @param list
	 * @return
	 */
	public static JecnTreeModel getTermDefineTreeModel(List<JecnTreeBean> list) {
		// 根节点
		JecnTreeNode rootNode = JecnTreeCommon.createTreeRoot(TreeNodeType.termDefineRoot, JecnProperties
				.getValue("termDefineTree"));
		JecnTreeCommon.addNLevelNodes(list, rootNode);
		return new JecnTreeModel(rootNode);
	}

	/***************************************************************************
	 * 点击树节点的操作
	 * 
	 * @param evt
	 * @param jTree
	 * @param jecnManageDialog
	 */
	public static void treeMousePressed(MouseEvent evt, JecnTree jTree) {
		/** 点击左键 */
		if ((evt.getModifiersEx() & InputEvent.BUTTON1_DOWN_MASK) != 0) {
			// 双击左键
			if (evt.getClickCount() == 2) {
				TreePath[] treePath = jTree.getSelectionPaths();
				// if (treePath != null && treePath.length == 1) {
				// JecnTreeNode node = (JecnTreeNode)
				// treePath[0].getLastPathComponent();
				//
				// // 如果双击的节点 术语
				// if (node.getJecnTreeBean().getTreeNodeType() ==
				// TreeNodeType.termDefine) {// file:文件
				//
				// } else {
				// JecnTreeCommon.autoExpandNode(jTree, node);
				// }
				// }
			}
		}
		// 点击右键
		if ((evt.getModifiers() & InputEvent.BUTTON3_MASK) != 0) {
			/** 选中节点路径集合 */
			TreePath[] treePathArr = jTree.getSelectionPaths();
			if (treePathArr != null && treePathArr.length > 0) {
				/** 以最后一个选中为例 */
				TreePath treePathClick = treePathArr[treePathArr.length - 1];
				JecnTreeNode nodeClick = (JecnTreeNode) treePathClick.getLastPathComponent();
				TreeNodeType treeNodeType = nodeClick.getJecnTreeBean().getTreeNodeType();
				List<JecnTreeNode> listSelectNode = new ArrayList<JecnTreeNode>();
				List<TreePath> listTreePaths = new ArrayList<TreePath>();
				// 去掉不同类型
				for (TreePath treePath : treePathArr) {
					if (treePath != null) {
						JecnTreeNode nodeSelect = (JecnTreeNode) treePath.getLastPathComponent();
						if (treeNodeType == nodeSelect.getJecnTreeBean().getTreeNodeType()) {
							listSelectNode.add(nodeSelect);
							listTreePaths.add(treePath);
						}
					}
				}
				if (listSelectNode.size() > 0) {
					JecnTermDefineTreeMenu menu = null;
					if (listSelectNode.size() == 1) {
						jTree.setSelectionPath(listTreePaths.get(0));
						menu = new JecnTermDefineTreeMenu(jTree, listSelectNode.get(0));
						menu.show(evt.getComponent(), evt.getX(), evt.getY());
						return;
					}
					// 去掉子节点
					List<JecnTreeNode> listRemoveChild = JecnTreeCommon.repeatChildNodes(listSelectNode, jTree);
					if (listRemoveChild.size() > 0) {
						TreePath[] treePaths = new TreePath[listRemoveChild.size()];
						for (int i = 0; i < listRemoveChild.size(); i++) {
							JecnTreeNode node = listRemoveChild.get(i);
							for (TreePath treePath : listTreePaths) {
								JecnTreeNode nodeSelect = (JecnTreeNode) treePath.getLastPathComponent();
								if (node.getJecnTreeBean().getId().equals(nodeSelect.getJecnTreeBean().getId())) {
									treePaths[i] = treePath;
									break;
								}
							}
						}
						if (treePaths.length == 1) {
							jTree.setSelectionPath(treePaths[0]);
							menu = new JecnTermDefineTreeMenu(jTree, listRemoveChild.get(0));
						} else {
							menu = new JecnTermDefineTreeMenu(jTree, listRemoveChild, treeNodeType);
						}
						menu.show(evt.getComponent(), evt.getX(), evt.getY());
					}
				}

			}
		}
	}
}
