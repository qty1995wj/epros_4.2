package com.jecn.webservice.jinke;

import javax.jws.WebMethod;
import javax.jws.WebService;

import com.jecn.epros.bean.MessageResult;
import com.jecn.epros.bean.external.TaskParam;

/**
 * 金科 - 任务接口
 * 
 * @author hyl
 * 
 */
@WebService(endpointInterface = "com.jecn.webservice.jinke.IJKTaskService")
public interface IJKTaskService {

	@WebMethod(operationName = "handleTaskExternal")
	public MessageResult handleTaskExternal(TaskParam param);
}
