package com.jecn.epros.sqlprovider;

import com.jecn.epros.domain.EnumClass.TreeNodeType;
import com.jecn.epros.domain.risk.SearchRiskBean;
import com.jecn.epros.sqlprovider.server3.SqlCommon;
import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class RiskSqlProvider extends BaseSqlProvider {

    /**
     * 树展开
     *
     * @param map
     * @return
     */
    public String findChildRisks(Map<String, Object> map) {
        Long pId = Long.valueOf(map.get("pId").toString());
        Long projectId = Long.valueOf(map.get("projectId").toString());
        return "select distinct t.id,t.name,t.is_dir,t.sort,t.parent_id,"
                + " case when jr.id is null then 0 else 1 end as count " + " from JECN_RISK t"
                + " LEFT JOIN JECN_RISK JR ON JR.parent_id=T.ID" + " where t.parent_id=" + pId + " and  t.PROJECT_ID ="
                + projectId + " order by t.parent_id,t.sort,t.id";

    }

    /**
     * 风险相关控制目标
     *
     * @return
     */
    public String findRiskTargetDescriptions(Long riskId) {
        return "select jc.id, jc.description," + "       jcp.control_code," + "       jfsi.activity_show,"
                + "       jfsi.figure_id,jfsi.figure_text," + "       jfs.flow_name,jfs.flow_id,"
                + "       jfo.org_name, jfo.org_id," + "       jcp.method," + "       jcp.type,"
                + "       jcp.key_point," + "       jcp.frequency" + "  from jecn_controltarget jc"
                + "  INNER join jecn_control_point jcp on jc.id = jcp.target_id"
                + "  INNER join jecn_flow_structure_image jfsi on jcp.active_id ="
                + "                                              jfsi.figure_id"
                + "  INNER join jecn_flow_structure jfs on jfsi.flow_id = jfs.flow_id and jfs.del_state = 0"
                + "  left join jecn_flow_related_org jfro on jfsi.flow_id = jfro.flow_id"
                + "  left join jecn_flow_org jfo on jfro.org_id = jfo.org_id" + " where jc.risk_id = " + riskId;
    }

    /**
     * 风险相关内控指引
     *
     * @param riskId
     * @return
     */
    public String findRiskGuide(Long riskId) {
        return "select  JCG.ID,JCG.NAME,JCG.DESCRIPTION from JECN_RISK_GUIDE JRG,JECN_CONTROL_GUIDE JCG"
                + "   where JCG.ID=JRG.GUIDE_ID AND JRG.RISK_ID=" + riskId;
    }

    /**
     * 相关制度
     *
     * @return
     */
    public String findRelatedRules(Long riskId) {
        return "select r.id, r.rule_name,"
                + "        case when r.is_dir=2 and r.is_file_local=0 then jf.doc_id else r.rule_number END as rule_code"
                + "        ,o.org_name," + "        case when  r.TYPE_RES_PEOPLE=0 then ju.true_name"
                + "                       when r.type_res_people=1 then jfoi.figure_text         END AS RES_PEOPLE"
                + "                  from jecn_rule_risk rs ,JECN_RULE r"
                + "                  left join JECN_FLOW_ORG o on r.org_id = o.org_id and o.del_state=0"
                + "                  LEFT JOIN JECN_USER JU ON r.TYPE_RES_PEOPLE=0 AND JU.ISLOCK = 0  AND JU.PEOPLE_ID = r.RES_PEOPLE_ID"
                + "                  LEFT JOIN JECN_FLOW_ORG_IMAGE JFOI ON r.TYPE_RES_PEOPLE=1 AND JFOI.FIGURE_ID= r.RES_PEOPLE_ID"
                + "                  LEFT JOIN JECN_FILE JF ON r.is_dir=2 and r.is_file_local=0 and JF.FILE_ID= r.File_Id AND JF.DEL_STATE=0 "
                + "                  where rs.rule_id = r.id and r.del_state = 0 and rs.risk_id =" + riskId;
    }

    /**
     * 相关流程
     *
     * @return
     */
    public String findRelatedFlows(Long riskId) {
        return "	SELECT" +
                "		JFS.FLOW_ID," +
                "		JFS.FLOW_NAME," +
                "		JFS.FLOW_ID_INPUT," +
                "		JFO.ORG_NAME," +
                "		CASE" +
                "	WHEN JFBI.TYPE_RES_PEOPLE = 0 THEN" +
                "		JU.TRUE_NAME" +
                "	WHEN JFBI.TYPE_RES_PEOPLE = 1 THEN" +
                "		JFOI.FIGURE_TEXT" +
                "	END AS RES_PEOPLE" +
                "	FROM" +
                "		jecn_control_point jcp," +
                "		jecn_flow_structure_image jfsi," +
                "		jecn_flow_structure JFS," +
                "		JECN_FLOW_BASIC_INFO JFBI" +
                "	LEFT JOIN JECN_FLOW_RELATED_ORG JFRO ON JFRO.FLOW_ID = JFBI.FLOW_ID" +
                "	LEFT JOIN JECN_FLOW_ORG JFO ON JFO.ORG_ID = JFRO.ORG_ID" +
                "	AND JFO.DEL_STATE = 0" +
                "	LEFT JOIN JECN_USER JU ON JFBI.TYPE_RES_PEOPLE = 0" +
                "	AND JU.ISLOCK = 0" +
                "	AND JU.PEOPLE_ID = JFBI.RES_PEOPLE_ID" +
                "	LEFT JOIN JECN_FLOW_ORG_IMAGE JFOI ON JFBI.TYPE_RES_PEOPLE = 1" +
                "	AND JFOI.FIGURE_ID = JFBI.RES_PEOPLE_ID" +
                "	WHERE" +
                "		jcp.active_id = jfsi.figure_id" +
                "	AND jfsi.flow_id = jfs.flow_id" +
                "	AND JFS.FLOW_ID = JFBI.FLOW_ID AND JFS.DEL_STATE= 0" +
                "	AND jcp.risk_id = #{riskId}" +
                "	union " +
                "	select " +
                "		JFS.FLOW_ID," +
                "		JFS.FLOW_NAME," +
                "		JFS.FLOW_ID_INPUT," +
                "		JFO.ORG_NAME," +
                "		CASE" +
                "	WHEN JFBI.TYPE_RES_PEOPLE = 0 THEN" +
                "		JU.TRUE_NAME" +
                "	WHEN JFBI.TYPE_RES_PEOPLE = 1 THEN" +
                "		JFOI.FIGURE_TEXT" +
                "	END AS RES_PEOPLE " +
                "	from JECN_FLOW_RELATED_RISK jfrr" +
                "	inner join jecn_flow_structure JFS on jfrr.flow_id=jfs.flow_id AND JFS.DEL_STATE =0" +
                "	inner join JECN_FLOW_BASIC_INFO JFBI on JFS.flow_id=JFBI.flow_id" +
                "	LEFT JOIN JECN_FLOW_RELATED_ORG JFRO ON JFRO.FLOW_ID = JFBI.FLOW_ID" +
                "	LEFT JOIN JECN_FLOW_ORG JFO ON JFO.ORG_ID = JFRO.ORG_ID" +
                "	AND JFO.DEL_STATE = 0" +
                "	LEFT JOIN JECN_USER JU ON JFBI.TYPE_RES_PEOPLE = 0" +
                "	AND JU.ISLOCK = 0" +
                "	AND JU.PEOPLE_ID = JFBI.RES_PEOPLE_ID" +
                "	LEFT JOIN JECN_FLOW_ORG_IMAGE JFOI ON JFBI.TYPE_RES_PEOPLE = 1" +
                "	AND JFOI.FIGURE_ID = JFBI.RES_PEOPLE_ID" +
                "	where jfrr.RISK_ID=#{riskId}";
    }

    private String searchRiskCommon(Long projectId, SearchRiskBean searchRiskBean) {
        String sql = " from jecn_risk jr where JR.IS_DIR = 1 AND jr.project_id=" + projectId;
        // 名字
        if (StringUtils.isNotBlank(searchRiskBean.getName())) {
            sql = sql + " and jr.name like '%" + searchRiskBean.getName() + "%'";
        }
        // 编号
        if (StringUtils.isNotBlank(searchRiskBean.getCode())) {
            sql = sql + " and jr.risk_code like '%" + searchRiskBean.getCode() + "%'";
        }
        if (searchRiskBean.getGrade() != -1) {
            sql = sql + " and jr.grade=" + searchRiskBean.getGrade();
        }
        return sql;
    }

    /**
     * 风险搜索
     *
     * @param map
     * @return
     */
    public String searchRisk(Map<String, Object> map) {
        Long projectId = Long.valueOf(map.get("projectId").toString());
        SearchRiskBean searchBean = (SearchRiskBean) map.get("searchRiskBean");
        String sql = "select jr.id,jr.risk_code,jr.name,jr.grade ";
        return sql + searchRiskCommon(projectId, searchBean);
    }

    /**
     * 风险搜索 总数
     *
     * @param map
     * @return
     */
    public String searchRiskTotal(Map<String, Object> map) {
        Long projectId = Long.valueOf(map.get("projectId").toString());
        SearchRiskBean searchBean = (SearchRiskBean) map.get("searchRiskBean");
        String sql = "select count(jr.id) ";
        return sql + searchRiskCommon(projectId, searchBean);
    }

    /**
     * 风险清单
     *
     * @param map
     * @return
     */
    public String fetchInventory(Map<String, Object> map) {
        Long id = (Long) map.get("id");
        Long projectId = (Long) map.get("projectId");
        List<Long> listIds = new ArrayList<Long>();
        listIds.add(id);
        String sql = "with RISK AS (" +
                "" +InventorySqlProvider.getRisks(map)+
                ")";
        sql += "				select " +
                "				jr.id," +
                "				jr.PARENT_ID," +
                "				jr.RISK_CODE," +
                "				jr.IS_DIR," +
                "				jr.NAME," +
                "				jr.GRADE," +
                "				jr.STANDARDCONTROL," +
                "				jr.CREATE_PERSON," +
                "				jr.T_PATH," +
                "				jr.T_LEVEL," +
                //控制目标"
                "				jc.id as target_id," +
                "				jc.description," +
                //相关活动"
                "				jcp.ACTIVE_ID," +
                "				jfsi.figure_text as active_name," +
                "				jcp.CONTROL_CODE," +
                "				jfsi.ACTIVITY_SHOW," +
                "				jfsir.FIGURE_TEXT as role_name," +
                "				jfsi.FLOW_ID," +
                "				jfs.FLOW_NAME," +
                "				jfo.ORG_NAME," +
                "				jcp.KEY_POINT," +
                "				jcp.FREQUENCY," +
                "				jcp.METHOD," +
                "				jcp.TYPE,      dic.value RISK_TYPE" +
                "				from RISK jr" +
                "				left join JECN_CONTROLTARGET jc on jr.ID=jc.RISK_ID" +
                "				left join JECN_CONTROL_POINT jcp on jcp.target_id=jc.id" +
                "				left join JECN_FLOW_STRUCTURE_IMAGE jfsi on jfsi.figure_id=jcp.ACTIVE_ID" +
                "				left join JECN_ROLE_ACTIVE jra on jra.FIGURE_ACTIVE_ID=jfsi.figure_id" +
                "				left join JECN_FLOW_STRUCTURE_IMAGE jfsir on jfsir.figure_id=jra.FIGURE_ROLE_ID" +
                "				left join JECN_FLOW_STRUCTURE jfs on jfs.flow_id=jfsi.flow_id AND JFS.DEL_STATE= 0" +
                "				LEFT JOIN JECN_FLOW_RELATED_ORG jfro ON jfro.FLOW_ID = jfs.FLOW_ID" +
                "				LEFT JOIN JECN_FLOW_ORG jfo ON jfo.ORG_ID = jfro.ORG_ID" +
                "             LEFT JOIN JECN_DICTIONARY dic" +
                "    ON jr.RISK_TYPE = DIC.CODE" +
                "   and DIC.NAME = 'JECN_RISK_TYPE'"+
                " order by jr.view_sort,jcp.CONTROL_CODE ASC";

        return sql;
    }

    /**
     * 相关文件
     * @param id
     * @return
     */
    public String findRelatedFiles(Long id){
        return  "	select f.version_id as id,f.file_name as name from FILE_RELATED_RISK a" +
                "	INNER JOIN jecn_file f on a.file_id=f.file_id AND f.del_state = 0" +
                "	INNER join JECN_RISK jr on jr.id=a.risk_id where a.risk_id=" + id;
    }

    public String fetchInventoryParent(Map<String, Object> map){
        String sql="select p.id,p.parent_id,p.risk_code,p.is_dir,p.name,p.t_path " +
                "	from jecn_risk c" +
                "	inner join jecn_risk p on c.id=#{id} and c.id<>p.id and c.t_path is not null and p.t_path is not null and c.t_path like p.t_path"+BaseSqlProvider.getConcatChar()+"'%'";
        return sql;
    }


}
