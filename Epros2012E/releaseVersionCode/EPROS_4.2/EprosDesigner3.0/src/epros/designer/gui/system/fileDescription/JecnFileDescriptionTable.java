package epros.designer.gui.system.fileDescription;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.util.ArrayList;
import java.util.List;

import javax.swing.SwingConstants;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;

import epros.designer.gui.process.guide.explain.GuideJScrollPane;
import epros.designer.table.JecnDBClickTable;
import epros.designer.table.JecnTable;
import epros.designer.table.JecnTableModel;
import epros.draw.gui.swing.JecnPanel;
import epros.draw.util.JecnUIUtil;

/**
 * 操作说明包含一个Table时候的通用组件
 * 
 * @author user
 * 
 */
public abstract class JecnFileDescriptionTable extends JecnFileDescriptionComponent {
	protected JecnTable table;

	public JecnFileDescriptionTable(int index, String name, boolean isRequest, JecnPanel contentPanel, String tip) {
		super(index, name, isRequest, contentPanel, tip);
	}
	
	public JecnFileDescriptionTable(int index, String name, boolean isRequest, JecnPanel contentPanel) {
		this(index, name, isRequest, contentPanel, "");
	}

	public void initTable() {
		centerPanel.setLayout(new GridBagLayout());
		GridBagConstraints c = new GridBagConstraints(0, 0, 1, 1, 1.0, 1.0, GridBagConstraints.NORTHWEST,
				GridBagConstraints.BOTH, insets, 0, 0);

		table = new FileDescriptionTable();
		GuideJScrollPane scrollPanel = new GuideJScrollPane();
		scrollPanel.setViewportView(table);
		scrollPanel.getViewport().setBackground(JecnUIUtil.getDefaultBackgroundColor());
		this.centerPanel.add(scrollPanel, c);
	}
	

	protected abstract void dbClickMethod();

	protected abstract JecnTableModel getTableModel();

	protected abstract int[] gethiddenCols();

	protected abstract boolean isSelectMutil();

	class FileDescriptionTable extends JecnDBClickTable {

		@Override
		protected void dbClickMethod() {
			JecnFileDescriptionTable.this.dbClickMethod();
		}

		@Override
		public JecnTableModel getTableModel() {
			return JecnFileDescriptionTable.this.getTableModel();
		}

		@Override
		public int[] gethiddenCols() {
			return JecnFileDescriptionTable.this.gethiddenCols();
		}

		@Override
		public boolean isSelectMutil() {
			return JecnFileDescriptionTable.this.isSelectMutil();
		}

	}

	public JecnTable getTable() {
		return table;
	}

	public void setTable(JecnTable table) {
		this.table = table;
	}

	public List<Long> getResultListLong() {
		JecnTableModel jecnTableModel = (JecnTableModel) this.getTable().getModel();
		List<Long> list = new ArrayList<Long>();
		for (int i = 0; i < this.getTable().getRowCount(); i++) {
			list.add(Long.valueOf(jecnTableModel.getValueAt(i, 0).toString()));
		}
		return list;
	}
	
	public void setColumnToFixedCenter(int col) {
		TableColumn tableColumn = setColumnWidth(col);
		DefaultTableCellRenderer render = new DefaultTableCellRenderer();
		render.setHorizontalAlignment(SwingConstants.CENTER);
		tableColumn.setCellRenderer(render);
	}

	public void setColumnToFixedLeft(int col) {
		TableColumn tableColumn = setColumnWidth(col);
		DefaultTableCellRenderer render = new DefaultTableCellRenderer();
		render.setHorizontalAlignment(SwingConstants.LEFT);
		tableColumn.setCellRenderer(render);
	}
	
	private TableColumn setColumnWidth(int col) {
		TableColumnModel columnModel = table.getColumnModel();
		TableColumn tableColumn = columnModel.getColumn(col);
		tableColumn.setMinWidth(180);
		tableColumn.setMaxWidth(180);
		return tableColumn;
	}
	
	
	
	

}
