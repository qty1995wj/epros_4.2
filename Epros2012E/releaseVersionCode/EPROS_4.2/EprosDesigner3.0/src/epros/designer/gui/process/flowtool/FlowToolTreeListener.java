package epros.designer.gui.process.flowtool;

import java.util.List;

import javax.swing.event.TreeExpansionEvent;

import org.apache.log4j.Logger;

import com.jecn.epros.server.bean.tree.JecnTreeBean;

import epros.designer.tree.JecnHighEfficiencyTree;
import epros.designer.tree.JecnTreeCommon;
import epros.designer.tree.JecnTreeListener;
import epros.designer.tree.JecnTreeNode;
import epros.designer.util.ConnectionPool;

public class FlowToolTreeListener extends JecnTreeListener {

	private JecnHighEfficiencyTree jTree;
	private static Logger log = Logger.getLogger(FlowToolTreeListener.class);

	public FlowToolTreeListener(JecnHighEfficiencyTree jTree) {
		this.jTree = jTree;
	}
	
	/**
	 * 展开
	 */
	@Override
	public void treeExpanded(TreeExpansionEvent event) {
		if (!jTree.isAllowExpand()) {
			return;
		}
		JecnTreeNode node = (JecnTreeNode) event.getPath()
				.getLastPathComponent();
		if (node.getChildCount() == 0) {
			try {
				List<JecnTreeBean> list = ConnectionPool.getFlowTool()
						.getChildSustainTools(node.getJecnTreeBean().getId());
				JecnTreeCommon.expansionTreeNode(jTree, list, node);
			} catch (Exception e) {
				log.error("FlowToolTreeListener treeExpanded is error", e);
			}
		}
		JecnTreeCommon.selectNode(jTree, node);

	}
	/**
	 * 合拢
	 */
	@Override
	public void treeCollapsed(TreeExpansionEvent event) {
		if (!jTree.isAllowExpand()) {
			return;
		}
		JecnTreeNode node = (JecnTreeNode) event.getPath()
				.getLastPathComponent();
		JecnTreeCommon.selectNode(jTree, node);
	}

}
