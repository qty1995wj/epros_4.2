package com.jecn.epros.server.action.designer.popedom.impl;

import java.util.Date;
import java.util.List;
import java.util.Map;

import com.jecn.epros.server.action.designer.popedom.IJecnRoleAction;
import com.jecn.epros.server.bean.popedom.JecnRoleInfo;
import com.jecn.epros.server.bean.tree.JecnTreeBean;
import com.jecn.epros.server.bean.tree.JecnTreeDragBean;
import com.jecn.epros.server.service.popedom.IJecnRoleService;

public class JecnRoleActionImpl implements IJecnRoleAction {

	private IJecnRoleService jecnRoleService;

	public IJecnRoleService getJecnRoleService() {
		return jecnRoleService;
	}

	public void setJecnRoleService(IJecnRoleService jecnRoleService) {
		this.jecnRoleService = jecnRoleService;
	}

	@Override
	public Long addRole(JecnRoleInfo jecnRoleInfo, Map<Integer, String> mapIds, Long updatePersionId) throws Exception {
		return jecnRoleService.addRole(jecnRoleInfo, mapIds, updatePersionId);
	}

	@Override
	public Long addRoleSecondAdmin(JecnRoleInfo jecnRoleInfo, Map<Integer, String> mapIds, Long updatePersionId)
			throws Exception {
		return jecnRoleService.addRoleSecondAdmin(jecnRoleInfo, mapIds, updatePersionId);
	}

	@Override
	public Long addRoleDir(JecnRoleInfo jecnRoleInfo, Long updatePersionId) throws Exception {
		jecnRoleInfo.setCreateTime(new Date());
		jecnRoleInfo.setUpdateTime(new Date());
		return jecnRoleService.save(jecnRoleInfo);
	}

	@Override
	public void deleteRoles(List<Long> listIds, Long projectId, Long updatePersionId) throws Exception {
		jecnRoleService.deleteRoles(listIds, projectId, updatePersionId);

	}

	@Override
	public List<JecnTreeBean> getRoleDirsByNameMove(Long projectId, String name, List<Long> listIds) throws Exception {

		return null;
	}

	@Override
	public List<JecnTreeBean> getRolesByName(Long projectId, String name, Long peopleId) throws Exception {
		if (peopleId == null) {
			return jecnRoleService.getRolesByName(projectId, name);
		}
		return jecnRoleService.getSecondAdminRolesByName(projectId, name, peopleId);
	}

	@Override
	public List<JecnTreeBean> getAllRoleDirs(Long projectId) throws Exception {

		return null;
	}

	@Override
	public List<JecnTreeBean> getAllRoles(Long projectId) throws Exception {

		return jecnRoleService.getAllRoles(projectId);
	}

	@Override
	public List<JecnTreeBean> getChildRoleDirs(Long pId, Long projectId, Long secondAdminPeopleId) throws Exception {

		return jecnRoleService.getChildRoleDirs(pId, projectId, secondAdminPeopleId);
	}

	@Override
	public List<JecnTreeBean> getChildRoles(Long id, Long projectId, Long secondAdminPeopleId) throws Exception {
		return jecnRoleService.getChildRoles(id, projectId, secondAdminPeopleId);
	}

	@Override
	public List<JecnTreeBean> getDefaultRoles(boolean isAdmin) throws Exception {
		return jecnRoleService.getDefaultRoles(isAdmin);
	}

	@Override
	public List<JecnTreeBean> getSecondAdminTreeBean(Long preId) throws Exception {
		return jecnRoleService.getSecondAdminTreeBean(preId);
	}

	@Override
	public void moveRoles(List<Long> listIds, Long pId, Long updatePersionId) throws Exception {
		jecnRoleService.moveRoles(listIds, pId, updatePersionId);

	}

	@Override
	public void reRoleName(String newName, Long id, Long updatePersonId) throws Exception {
		jecnRoleService.updateName(newName, id, updatePersonId);

	}

	@Override
	public void updateSortRoles(List<JecnTreeDragBean> list, Long pId, Long projectId, Long updatePersionId)
			throws Exception {

		jecnRoleService.updateSortRoles(list, pId, projectId, updatePersionId);
	}

	@Override
	public Long updateRole(JecnRoleInfo jecnRoleInfo, Map<Integer, String> mapIds, Long updatePersionId)
			throws Exception {
		return jecnRoleService.updateRole(jecnRoleInfo, mapIds, updatePersionId);
	}

	@Override
	public Long updateRoleSecondAdmin(JecnRoleInfo jecnRoleInfo, Map<Integer, String> mapIds, Long updatePersionId)
			throws Exception {
		return jecnRoleService.updateRoleSecondAdmin(jecnRoleInfo, mapIds, updatePersionId);
	}

	@Override
	public JecnRoleInfo getJecnRoleInfoById(Long id) throws Exception {
		return jecnRoleService.getJecnRoleInfoById(id);
	}

	@Override
	public int getChildCount(Long id, Long projectId) throws Exception {
		return jecnRoleService.getChildCount(id, projectId);
	}

	@Override
	public boolean validateRepeatNameEidt(String newName, Long id, Long pid, Long projectId) throws Exception {
		return jecnRoleService.getRepeatNameEidt(newName, id, pid, projectId);
	}

	@Override
	public List<JecnTreeBean> getPnodes(Long id, Long projectId) throws Exception {
		return jecnRoleService.getPnodes(id, projectId);
	}

	@Override
	public List<Object[]> selectUserRoleByRoleId(Long roleId) throws Exception {
		return jecnRoleService.selectUserRoleByRoleId(roleId);
	}

	@Override
	public Long getSecondAdminRoleId(Long peopleId) throws Exception {
		return jecnRoleService.getSecondAdminRoleId(peopleId);
	}

	/**
	 * 用户是否存在登录设计器的角色权限：true 存在
	 * 
	 * @param peopleId
	 * @return boolean
	 * @throws Exception
	 */
	@Override
	public boolean isRoleAuthDesigner(Long peopleId) throws Exception {
		return jecnRoleService.isRoleAuthDesigner(peopleId);
	}
}
