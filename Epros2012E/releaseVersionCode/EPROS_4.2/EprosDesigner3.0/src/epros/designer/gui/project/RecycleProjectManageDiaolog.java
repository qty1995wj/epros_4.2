package epros.designer.gui.project;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.swing.ImageIcon;
import javax.swing.JCheckBox;

import org.apache.log4j.Logger;

import com.jecn.epros.server.util.JecnTreeEnumConstant.TreeNodeType;

import epros.designer.gui.common.JecnRecycleManageDialog;
import epros.designer.util.ConnectionPool;
import epros.designer.util.JecnProperties;

/**
 * 
 * 项目回收
 * 
 * @author Administrator
 *
 */
public class RecycleProjectManageDiaolog extends JecnRecycleManageDialog {
	private static Logger log = Logger
			.getLogger(RecycleProjectManageDiaolog.class);

	@Override
	public boolean deleteData(List<Long> listids, Long updatePeopleId) {
		try {
			Set<Long> set = new HashSet<Long>();
			set.addAll(listids);
			ConnectionPool.getProject().deleteIdsProject(set);
		} catch (Exception e) {
			log.error("RecycleProjectManageDiaolog deleteData is error", e);
			return false;
		}
		return true;
	}

	@Override
	public Object[][] getTableData() {
		try {
			List<Object[]> list = ConnectionPool.getProject().getDelsProject();
			Object[][] obj = new Object[list.size()][5];
			for (int i = 0; i < list.size(); i++) {
				Object[] objects = list.get(i);
				if (objects == null || objects[0] == null || objects[1] == null) {
					continue;
				}
				obj[i][0] = new JCheckBox();
				obj[i][1] = 3;
				obj[i][2] = objects[0].toString();
				obj[i][3] = objects[1].toString();
				obj[i][4] = new ImageIcon("images/treeNodeImages/"
						+ TreeNodeType.project.toString() + ".gif");
			}
			return obj;
		} catch (Exception e) {
			log.error("RecycleProjectManageDiaolog getTableData is error", e);
			;
		}
		return null;
	}

	@Override
	public String getTableType() {
		return JecnProperties.getValue("projectName");// projectName
	}

	@Override
	public boolean isViewPathButton() {
		return false;
	}

	@Override
	public boolean recyData(List<Long> ids) {
		try {
			ConnectionPool.getProject().updateRecoverDelProject(ids);
		} catch (Exception e) {
			log.error("RecycleProjectManageDiaolog recyData is error", e);
			return false;
		}
		return true;
	}
	
	@Override
	public String getRecycleTitle() {
		return JecnProperties.getValue("projectRecover");
	}

	@Override
	public void searchButAction() {
		
	}
}
