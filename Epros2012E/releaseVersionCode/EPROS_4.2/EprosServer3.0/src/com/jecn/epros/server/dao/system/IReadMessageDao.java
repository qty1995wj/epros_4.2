package com.jecn.epros.server.dao.system;

import java.util.List;

import com.jecn.epros.server.bean.system.JecnMessageReaded;
import com.jecn.epros.server.common.IBaseDao;

/***
 * 已读消息操作类
 * @author 
 * 2013-02-27
 *
 */
public interface IReadMessageDao  extends IBaseDao<JecnMessageReaded, Long> {

	/***
	 * 获取所有已读消息数据
	 * @return
	 */
//	public List<JecnMessageReaded> getAllReadMessage();
	
}
