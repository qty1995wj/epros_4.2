package epros.draw.gui.top.toolbar;

import java.awt.GridBagConstraints;

import javax.swing.SwingConstants;

import epros.draw.constant.JecnToolbarConstant.ToolBarElemType;
import epros.draw.gui.top.button.JecnToolbarContentButton;
import epros.draw.util.JecnResourceUtil;
import epros.draw.util.JecnUIUtil;

/**
 * 
 * 工具栏大类：帮助类
 * 
 * @author ZHOUXY
 * 
 */
public class JecnHelpToolBarItemPanel extends JecnAstractToolBarItemPanel {

	/** 关于 */
	private JecnToolbarContentButton aboutBtn = null;
	/** 用户指南 */
	private JecnToolbarContentButton userHelpBtn = null;
	/** 联系我们 */
	private JecnToolbarContentButton findJecnBtn = null;

	public JecnHelpToolBarItemPanel(JecnToolBarPanel toolBarPanel) {
		super(toolBarPanel);

		initchildComponents();
	}

	@Override
	protected void initTitleType() {
		this.titleType = ToolBarElemType.helpTitle;
	}

	@Override
	protected void createChildComponents() {

		// 关于
		aboutBtn = new JecnToolbarContentButton(JecnResourceUtil.getJecnResourceUtil().getValue(
				ToolBarElemType.helpAbout), ToolBarElemType.helpAbout);
		// 用户指南
		userHelpBtn = new JecnToolbarContentButton(JecnResourceUtil.getJecnResourceUtil().getValue(
				ToolBarElemType.helpUserManual), ToolBarElemType.helpUserManual);
		// 联系我们
		findJecnBtn = new JecnToolbarContentButton(JecnResourceUtil.getJecnResourceUtil().getValue(
				ToolBarElemType.helpFindJecn), ToolBarElemType.helpFindJecn);

		// 设置图片向上、内容向下，整体居中
		// 关于
		aboutBtn.setHorizontalTextPosition(SwingConstants.CENTER);
		aboutBtn.setVerticalTextPosition(SwingConstants.BOTTOM);
		// 用户指南
		userHelpBtn.setHorizontalTextPosition(SwingConstants.CENTER);
		userHelpBtn.setVerticalTextPosition(SwingConstants.BOTTOM);
		// 联系我们
		findJecnBtn.setHorizontalTextPosition(SwingConstants.CENTER);
		findJecnBtn.setVerticalTextPosition(SwingConstants.BOTTOM);

		// 边框
		this.setBorder(JecnUIUtil.getTootBarBorder());
	}

	@Override
	protected void layoutChildComponents() {
		// 关于
		GridBagConstraints c = new GridBagConstraints(0, 0, 1, 1, 0.0, 1.0, GridBagConstraints.NORTHWEST,
				GridBagConstraints.VERTICAL, JecnUIUtil.getInsets0(), 0, 0);
		this.add(aboutBtn.getJToolBar(), c);

		// 用户指南
		// c = new GridBagConstraints(1, 0, 1, 1, 0.0, 1.0,
		// GridBagConstraints.NORTHWEST, GridBagConstraints.VERTICAL,
		// JecnUIUtil.getInsets0(), 0, 0);
		// this.add(userHelpBtn.getJToolBar(), c);
		// 用户指南
		c = new GridBagConstraints(2, 0, 1, 1, 0.0, 1.0, GridBagConstraints.NORTHWEST, GridBagConstraints.VERTICAL,
				JecnUIUtil.getInsets0(), 0, 0);
		this.add(findJecnBtn.getJToolBar(), c);
	}
}
