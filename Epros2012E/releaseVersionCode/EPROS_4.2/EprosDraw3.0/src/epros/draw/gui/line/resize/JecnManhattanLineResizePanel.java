package epros.draw.gui.line.resize;

import epros.draw.constant.JecnLogConstants;
import epros.draw.constant.JecnWorkflowConstant.ReSizeLineType;
import epros.draw.error.ErrorInfoConstant;
import epros.draw.gui.line.JecnBaseManhattanLinePanel;
import epros.draw.gui.swing.JecnBaseResizePanel;
import epros.draw.util.JecnUIUtil;

/**
 * 
 * 连接线的首尾选中点，其具有改变连接线所关联的图形
 * 
 * @author ZHANGXH
 * 
 */
public class JecnManhattanLineResizePanel extends JecnBaseResizePanel {
	/** 线选中点类型 start 开始点,end 结束点 */
	private ReSizeLineType reSizeLineType = null;
	/** 端点关联的曼哈顿线 */
	private JecnBaseManhattanLinePanel manhattanLine = null;

	private JecnManhattanLineResizePanel(ReSizeLineType reSizeLineType,
			JecnBaseManhattanLinePanel manhattanLine) {
		if (reSizeLineType == null) {
			JecnLogConstants.LOG_JECN_BASE_RESIZE_PANEL
					.error("JecnManhattanLineResizePanel类构造函数：参数为null。reSizeLineType="
							+ reSizeLineType);
			throw new IllegalArgumentException(ErrorInfoConstant.PARA_OBJ_NULL);
		}
		// 显示点位置 start，center，end
		this.reSizeLineType = reSizeLineType;

		this.manhattanLine = manhattanLine;

		initComponts();
	}

	public void initComponts() {
		switch (reSizeLineType) {
		case start:
			// 默认背景色为绿色
			this.bodyColor = JecnUIUtil.getLineStartPointColor();
			this.setSize(8, 8);
			break;

		case end:
			// 默认背景色为绿色
			this.bodyColor = JecnUIUtil.getLineEndPointColor();
			this.setSize(8, 8);
			break;
		default:
			break;
		}
	}

	/**
	 * 
	 * 创建开始选中点
	 * 
	 * @return
	 */
	public static JecnManhattanLineResizePanel createStartManhattLineResizePanel(
			JecnBaseManhattanLinePanel manhattanLine) {
		return new JecnManhattanLineResizePanel(ReSizeLineType.start,
				manhattanLine);
	}

	/**
	 * 
	 * 创建开始选中点
	 * 
	 * @return
	 */
	public static JecnManhattanLineResizePanel createEndManhattLineResizePanel(
			JecnBaseManhattanLinePanel manhattanLine) {
		return new JecnManhattanLineResizePanel(ReSizeLineType.end,
				manhattanLine);
	}

	public ReSizeLineType getReSizeLineType() {
		return reSizeLineType;
	}

	public JecnBaseManhattanLinePanel getManhattanLine() {
		return manhattanLine;
	}

}
