package epros.designer.gui.common;

import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;
import java.util.Vector;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.table.DefaultTableModel;

import org.apache.log4j.Logger;

import com.jecn.epros.server.bean.popedom.DownloadPopedomBean;
import com.jecn.epros.server.bean.tree.JecnTreeBean;

import epros.designer.gui.choose.PosCompetenceTable;
import epros.designer.gui.popedom.position.PositionChooseDialog;
import epros.designer.tree.JecnTreeNode;
import epros.designer.util.ConnectionPool;
import epros.designer.util.JecnProperties;
import epros.draw.gui.swing.JecnPanel;
import epros.draw.gui.top.dialog.JecnDialog;
import epros.draw.gui.top.optionpane.JecnOptionPane;
import epros.draw.util.JecnUIUtil;
/**
 * @author hyl
 * @description：设置流程文件下载权限界面
 */
public class JecnFlowFileDownloadPermDialog extends JecnDialog {
	Logger log = Logger.getLogger(JecnFlowFileDownloadPermDialog.class);
	/** 主面板 */
	private JPanel mainPanel = new JecnPanel();
	/** 包括密级、岗位权限、组织权限 */
	private JecnPanel proPanel = new JecnPanel(); // 530,160

	/**权限面板*/
	private JecnPanel permPanel=new JecnPanel();

	/** 岗位查阅权限 */
	private JecnPanel posPanel = new JecnPanel();
	/** 岗位 权限设置是否子节点面板 */
	private JPanel posChoosePopemPanel = new JecnPanel();
	
	
	/** 岗位权限Lab */
	private JLabel posCompetenceLab = new JLabel("");
	/** 岗位权限滚动面板 */
	private JScrollPane posCompetenceScrollPane = new JScrollPane();
	private PosCompetenceTable posCompetenceTable = null;
	/** 岗位权限 选择按钮 */
	private JButton posBut = new JButton(JecnProperties.getValue("selectBtn"));


	/** 主面板底部按钮面板，包括确定、取消按钮 */
	private JecnPanel botButtonPanel = new JecnPanel();

	private List<JecnTreeBean> poslist = null;
	
	
	// 确定
	private JButton okBut = new JButton(JecnProperties.getValue("okBtn"));
	// 取消
	private JButton cancelBut = new JButton(
			JecnProperties.getValue("cancelBtn"));
	private JecnTreeNode node;

	public JecnFlowFileDownloadPermDialog(DownloadPopedomBean downloadPerm,
			JecnTreeNode node) {
		poslist = downloadPerm.getPosList();
		this.node = node;
		this.setSize(540, 340);
		this.setResizable(true);
		this.setModal(true);
		// 居中，设置大小后使用
		this.setLocationRelativeTo(null);
		initCompotents();
		initLayout();
		
		initPermPosShow();

	}


	private void initCompotents() {
		this.setTitle(JecnProperties.getValue("flowFileDownloadPer"));
		Dimension dimension = new Dimension(320, 145);
		posCompetenceScrollPane.setPreferredSize(dimension);
		posCompetenceScrollPane.setMinimumSize(dimension);
		posCompetenceTable = new PosCompetenceTable(poslist,false);
		posCompetenceScrollPane.setViewportView(posCompetenceTable);
		posCompetenceScrollPane.getViewport().setBackground(JecnUIUtil.getDefaultBackgroundColor());
		
		
		posPanel.setBorder(BorderFactory.createTitledBorder(
				BorderFactory.createEtchedBorder(), JecnProperties.getValue("postDownloadPer")));
		
		posBut.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {

				PositionChooseDialog positionChooseDialog = new PositionChooseDialog(
						poslist);
				positionChooseDialog.setVisible(true);
				if (positionChooseDialog.isOperation()) {
					initPermPosShow();
				}
			}
		});
		// 取消事件，关闭窗口
		cancelBut.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				JecnFlowFileDownloadPermDialog.this.dispose();
			}
		});
		okBut.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				okButAction();
			}
		});
		
	}
	
	
	private void initPermPosShow() {
		if (poslist != null && poslist.size() > 0) {
			posCompetenceTable.remoeAll();
			for (int i = 0; i < poslist.size(); i++) {
				JecnTreeBean jecnTreeBean = poslist.get(i);
				Vector vec = new Vector();
				vec.add(jecnTreeBean.getId());
				vec.add(jecnTreeBean.getName());
				((DefaultTableModel) posCompetenceTable.getModel())
						.addRow(vec);
			}
		} else {
			posCompetenceTable.remoeAll();
		}
	}
	

	private void initPermShow(){
		initPermPosShow();
	}
	
	private void initLayout() {
		Insets insets = new Insets(2, 1, 2, 1);
		GridBagConstraints c = new GridBagConstraints(0, 0, 1, 1, 1.0, 1.0,
				GridBagConstraints.EAST, GridBagConstraints.BOTH, insets, 0, 0);

		mainPanel.add(proPanel, c);
		c = new GridBagConstraints(0, 1, 1, 1, 1.0, 0.0,
				GridBagConstraints.EAST, GridBagConstraints.HORIZONTAL, insets,
				0, 0);

		mainPanel.add(botButtonPanel, c);
		c = new GridBagConstraints(0, 0, 1, 1, 1.0, 0.0,
				GridBagConstraints.NORTH, GridBagConstraints.HORIZONTAL,
				insets, 0, 0);
		proPanel.add(permPanel, c);
		c = new GridBagConstraints(0, 3, 1, 1, 1.0, 1.0,
				GridBagConstraints.NORTH, GridBagConstraints.BOTH, insets, 0, 0);
		proPanel.add(posPanel, c);
		


		// 岗位权限lab
		c = new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
				GridBagConstraints.NORTH, GridBagConstraints.NONE, insets, 0, 0);
		posPanel.add(posCompetenceLab, c);
		// 岗位权限area
		c = new GridBagConstraints(1, 0, 1, 1, 1.0, 1.0,
				GridBagConstraints.WEST, GridBagConstraints.BOTH, insets, 0, 0);
		posPanel.add(posCompetenceScrollPane, c);
		// 岗位权限 选择button
		c = new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0,
				GridBagConstraints.NORTHWEST, GridBagConstraints.NONE, insets,
				0, 0);
		posPanel.add(posBut, c);
		c = new GridBagConstraints(0, 1, 3, 1, 0.0, 0.0,
				GridBagConstraints.NORTHWEST, GridBagConstraints.NONE, insets,
				0, 0);
		posPanel.add(posChoosePopemPanel, c);
		
		

		botButtonPanel.setLayout(new FlowLayout(FlowLayout.RIGHT));
		botButtonPanel.add(okBut);
		botButtonPanel.add(cancelBut);
		this.getContentPane().add(mainPanel);
	}

	private void okButAction() {
		
		StringBuffer posIdsBuffer = new StringBuffer();
		
		if (poslist != null && poslist.size() > 0) {
			for (JecnTreeBean b : poslist) {
				posIdsBuffer.append(b.getId().toString() + ",");
			}
		}
		String posaIds = posIdsBuffer.length() > 0 ? posIdsBuffer.substring(0,
				posIdsBuffer.length() - 1) : "";
		try {
			ConnectionPool.getOrganizationAction()
					.saveOrUpdateFlowFileDownloadPermissions(
							node.getJecnTreeBean().getId(),posaIds);
			this.dispose();
		} catch (Exception e) {
			log.error("okButAction is error！", e);
			JecnOptionPane.showMessageDialog(null,JecnProperties.getValue("serverConnException"));
		}
	}
}
