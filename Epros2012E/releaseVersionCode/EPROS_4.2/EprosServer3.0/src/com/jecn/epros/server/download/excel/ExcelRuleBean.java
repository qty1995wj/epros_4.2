package com.jecn.epros.server.download.excel;



/***
 * 制度父级目录封装Bean
 * @author chehuanbo
 * @since V3.06
 */
public class ExcelRuleBean {
   
	
	private Long ruleNum;  //制度名称
	private String ruleName; //制度编号
	public Long getRuleNum() {
		return ruleNum;
	}
	public void setRuleNum(Long ruleNum) {
		this.ruleNum = ruleNum;
	}
	public String getRuleName() {
		return ruleName;
	}
	public void setRuleName(String ruleName) {
		this.ruleName = ruleName;
	}
}
