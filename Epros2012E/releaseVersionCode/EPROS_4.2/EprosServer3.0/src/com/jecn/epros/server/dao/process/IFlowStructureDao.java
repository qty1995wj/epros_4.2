package com.jecn.epros.server.dao.process;

import java.util.List;
import java.util.Set;

import com.jecn.epros.server.bean.define.TermDefinitionLibrary;
import com.jecn.epros.server.bean.file.FileOpenBean;
import com.jecn.epros.server.bean.integration.JecnActiveOnLineTBean;
import com.jecn.epros.server.bean.integration.JecnActiveStandardBeanT;
import com.jecn.epros.server.bean.integration.JecnControlPointT;
import com.jecn.epros.server.bean.integration.JecnRisk;
import com.jecn.epros.server.bean.process.FlowCriterionT;
import com.jecn.epros.server.bean.process.JecnActivityFileT;
import com.jecn.epros.server.bean.process.JecnFigureFileTBean;
import com.jecn.epros.server.bean.process.JecnFlowBasicInfo;
import com.jecn.epros.server.bean.process.JecnFlowDriver;
import com.jecn.epros.server.bean.process.JecnFlowDriverT;
import com.jecn.epros.server.bean.process.JecnFlowStructure;
import com.jecn.epros.server.bean.process.JecnFlowStructureT;
import com.jecn.epros.server.bean.process.JecnFlowSustainTool;
import com.jecn.epros.server.bean.process.JecnModeFileT;
import com.jecn.epros.server.bean.process.JecnRefIndicatorsT;
import com.jecn.epros.server.bean.process.JecnRoleActiveT;
import com.jecn.epros.server.bean.process.JecnTempletT;
import com.jecn.epros.server.bean.process.JecnToFromActiveT;
import com.jecn.epros.server.bean.process.ProcessInterface;
import com.jecn.epros.server.bean.process.StandardFlowRelationTBean;
import com.jecn.epros.server.bean.process.inout.JecnFlowInoutData;
import com.jecn.epros.server.bean.tree.JecnTreeBean;
import com.jecn.epros.server.common.IBaseDao;
import com.jecn.epros.server.webBean.WebLoginBean;
import com.jecn.epros.server.webBean.process.ProcessBaseInfoBean;

public interface IFlowStructureDao extends IBaseDao<JecnFlowStructureT, Long> {

	/**
	 * @author zhangchen Jul 26, 2012
	 * @description：流程下所有的活动的输入和操作规范 0是主键ID 1是活动主键Id 2是文件主键ID 3是文件名称
	 *                               4是文件类型（0是输入，1是操作手册） 5是文件编号
	 * @param flowId
	 * @return
	 * @throws Exception
	 */
	public List<Object[]> findAllActiveInputAndOperationByFlowId(Long flowId, boolean isPub) throws Exception;

	/**
	 * 
	 * 农夫山泉操作说明下载使用
	 * 
	 * @description：流程下所有的活动的输入和操作规范 0是主键ID 1是活动主键Id 2是文件主键ID 3是文件名称
	 *                               4是文件类型（0是输入，1是操作手册） 5是文件编号
	 * @param flowId
	 * @return
	 * @throws Exception
	 */
	public List<Object[]> findAllActiveInputAndOperationByFlowIdNFSQ(Long flowId, boolean isPub) throws Exception;

	/**
	 * @author zhangchen Jul 26, 2012
	 * @description：流程下所有的活动的输出 0是主键ID 1是活动主键Id 2是文件主键ID 3是文件名称 4是文件编号
	 * @param flowId
	 * @return
	 * @throws Exception
	 */
	public List<Object[]> findAllActiveOutputByFlowId(Long flowId, boolean isPub) throws Exception;

	/**
	 * @author zhangchen Jul 26, 2012
	 * @description：流程下所有的活动的样例（设计器）
	 * @param flowId
	 * @return
	 * @throws Exception
	 */
	public List<Object[]> findAllActiveTempletByFlowId(Long flowId, boolean isPub) throws Exception;

	/**
	 * @author zhangchen Jul 26, 2012
	 * @description：流程下所有的活动的指标（设计器）
	 * @param flowId
	 * @return
	 * @throws Exception
	 */
	public List<JecnRefIndicatorsT> findAllJecnRefIndicatorsTsByFlowIdT(Long flowId) throws Exception;

	/**
	 * @author zhangchen Jul 26, 2012
	 * @description：流程下所有的角色的相关岗位和岗位组
	 * @param flowId
	 * @return
	 * @throws Exception
	 */
	public List<Object[]> findAllRolePosByFlowIdT(Long flowId) throws Exception;

	/**
	 * @author zhangchen Jul 26, 2012
	 * @description：流程下所有的角色和活动关系表
	 * @param flowId
	 * @return
	 * @throws Exception
	 */
	public List<JecnRoleActiveT> findAllJecnRoleActiveTByFlowIdT(Long flowId) throws Exception;

	/**
	 * @author zhangchen Aug 6, 2012
	 * @description：删除流程
	 * @param setFlowIds
	 * @throws Exception
	 */
	public void deleteFlows(Set<Long> setFlowIds) throws Exception;

	/**
	 * @author zhangchen Aug 6, 2012
	 * @description：删除流程地图
	 * @param listMainFlowIds
	 * @throws Exception
	 */
	public void deleteMainFlows(Set<Long> listMainFlowIds) throws Exception;

	/**
	 * @author zhangchen Aug 7, 2012
	 * @description：删除项目下的流程
	 * @param projectId
	 * @throws Exception
	 */
	public void deleteFlowsByProjectId(Long projectId) throws Exception;

	/**
	 * 
	 * @author zhangchen Oct 11, 2012
	 * @description：获得流程下活动的所有输入
	 * @param flowId
	 * @return
	 * @throws Exception
	 */
	public List<JecnModeFileT> findJecnModeFileTByFlowId(Long flowId) throws Exception;

	/**
	 * 
	 * @author zhangchen Oct 11, 2012
	 * @description：获得流程下活动的所有样例
	 * @param flowId
	 * @return
	 * @throws Exception
	 */
	public List<JecnTempletT> findJecnTempletTByFlowId(Long flowId) throws Exception;

	/**
	 * 
	 * @author zhangchen Oct 11, 2012
	 * @description：获得流程下活动的所有输入和操作规范
	 * @param flowId
	 * @return
	 * @throws Exception
	 */
	public List<JecnActivityFileT> findJecnActivityFileTByFlowId(Long flowId) throws Exception;

	/**
	 * @author zhangchen Oct 19, 2012
	 * @description：发布流程的数据
	 * @param processId
	 * @throws Exception
	 */
	public void releaseProcessData(Long processId) throws Exception;

	/**
	 * @author zhangchen Oct 19, 2012
	 * @description：撤销流程的数据
	 * @param processId
	 * @throws Exception
	 */
	public void revokeProcessRelease(Long processId) throws Exception;

	/**
	 * @author zhangchen Oct 19, 2012
	 * @description：撤销流程地图的数据
	 * @param processId
	 * @throws Exception
	 */
	public void revokeProcessMapRelease(Long processId) throws Exception;

	/**
	 * @author zhangchen Oct 19, 2012
	 * @description：发布流程地图的数据
	 * @param processId
	 * @throws Exception
	 */
	public void releaseProcessMapData(Long processId) throws Exception;

	/**
	 * @author zhangchen Nov 1, 2012
	 * @description：获得流程中相关制度未发布的制度Id集合
	 * @param flowId
	 * @return
	 * @throws Exception
	 */
	public List<Object[]> getRelateRuleByFlowId(Long flowId) throws Exception;

	/**
	 * @author zhangchen Nov 1, 2012
	 * @description：获得流程地图中未发布制度（制度图标）
	 * @param flowId
	 * @return
	 * @throws Exception
	 */
	public List<Object[]> getRelateRuleByFlowMapId(Long flowId) throws Exception;

	/**
	 * @author zhangchen Nov 1, 2012
	 * @description：获得流程中相关制度
	 * @param flowId
	 * @return
	 * @throws Exception
	 */
	public List<FlowCriterionT> getFlowCriterionTByFlowId(Long flowId) throws Exception;

	/**
	 * @author zhangchen Nov 1, 2012
	 * @description：获得流程中相关标准
	 * @param flowId
	 * @return
	 * @throws Exception
	 */
	public List<StandardFlowRelationTBean> getStandardFlowRelationTBeanByFlowId(Long flowId) throws Exception;

	/**
	 * @author zhangchen Nov 1, 2012
	 * @description： 驱动规则的基本信息
	 * @param flowId
	 * @return
	 * @throws Exception
	 */
	public JecnFlowDriverT getJecnFlowDriverTByFlowId(Long flowId) throws Exception;

	/**
	 * @author zhangchen Nov 1, 2012
	 * @description： 驱动规则的基本信息
	 * @param flowId
	 * @return
	 * @throws Exception
	 */
	public JecnFlowDriver getJecnFlowDriverByFlowId(Long flowId) throws Exception;

	/*
	 * 0活动主键ID 1活动编号 2活动名称 3活动说明 4关键活动类型 5关键说明 6 活动输入 7 活动输出 8链接id 9链接类型
	 * 10办理时限目标值 11办理时限原始值
	 */
	public List<Object[]> getActiveShowByFlowId(Long flowId, boolean isPub) throws Exception;

	/**
	 * @author zhangchen Nov 1, 2012
	 * @description： 活动明细数据 0活动主键ID 1活动编号 2活动名称 3活动说明 4关键活动类型 5关键说明 6对应内控矩阵风险点
	 *               7对应标准条款
	 * @param flowId
	 * @return
	 * @throws Exception
	 */
	public List<Object[]> getActiveByFlowId(Long flowId, boolean isPub) throws Exception;

	/**
	 * @author zhangchen Nov 1, 2012
	 * @description： 角色明细数据 0是角色图形主键ID 1角色名称 2角色职责
	 * @param flowId
	 * @return
	 * @throws Exception
	 */
	public List<Object[]> getRoleShowByFlowId(Long flowId, boolean isPub) throws Exception;

	/**
	 * @author zhangchen Jul 26, 2012
	 * @description：流程下所有的角色和活动关系表 0是角色主键ID 1是活动主键ID
	 * @param flowId
	 * @param isPub
	 * @return
	 * @throws Exception
	 */
	public List<Object[]> findAllRoleAndActiveRelateByFlowId(Long flowId, boolean isPub) throws Exception;

	/**
	 * @author zhangchen Jul 26, 2012
	 * @description：流程下所有的角色和岗位关系表 0是角色主键ID 1是岗位主键ID 2是岗位名称
	 * @param flowId
	 * @param isPub
	 * @return
	 * @throws Exception
	 */
	public List<Object[]> finaAllRoleAndPosRelates(Long flowId, boolean isPub) throws Exception;

	/**
	 * @author zhangchen Jul 26, 2012
	 * @description：流程下所有的角色和岗位关系表 0是角色主键ID 1是岗位主键ID 2是岗位名称
	 * @param flowId
	 * @param isPub
	 * @return
	 * @throws Exception
	 */
	public List<Object[]> finaAllRoleAndPosGroupPosRelates(Long flowId, boolean isPub) throws Exception;

	/**
	 * @author zhangchen Jul 26, 2012
	 * @description：流程下所有的相关流程 0是linecolor 1是流程编号 2是流程名称
	 * @param flowId
	 * @param isPub
	 * @return
	 * @throws Exception
	 */
	public List<Object[]> findAllRelateFlows(Long flowId, boolean isPub) throws Exception;

	/**
	 * 农夫山泉操作说明下载专用
	 * 
	 * @description：流程下所有的相关流程 0是linecolor 1是流程编号 2是流程名称
	 * @param flowId
	 * @param isPub
	 * @return
	 * @throws Exception
	 */
	public List<Object[]> findAllRelateFlowsNFSQ(Long flowId, boolean isPub) throws Exception;

	/**
	 * @author zhangchen Jul 26, 2012
	 * @description：流程下所有的相关流程 0是linecolor 1是流程编号 2是流程名称
	 * @param flowId
	 * @param isPub
	 * @return
	 * @throws Exception
	 */
	public List<Object[]> findAllRelateFlowsWeb(Long flowId, boolean isPub) throws Exception;

	/**
	 * 获取流程地图操作说明数据
	 * 
	 * @author fuzhh Nov 9, 2012
	 * @param flowId
	 *            流程ID
	 * @param isPub
	 *            true为非临时表 false为临时表
	 * @return
	 * @throws Exception
	 */
	public Object[] findFlowMap(Long flowId, boolean isPub) throws Exception;

	/**
	 * 获得流程地图没有发布的文件
	 * 
	 * @param flowId
	 * @return
	 * @throws Exception
	 */
	public List<Long> findProcessMapNoPubFiles(Long flowId) throws Exception;

	/**
	 * 获得流程没有发布的文件
	 * 
	 * @param flowId
	 * @return
	 * @throws Exception
	 */
	public List<Long> findProcessNoPubFiles(Long flowId) throws Exception;

	/**
	 * 获得流程对象
	 * 
	 * @param flowId
	 * @return
	 * @throws Exception
	 */
	public JecnFlowStructure findJecnFlowStructureById(Long flowId) throws Exception;

	/***************************************************************************
	 * 获得流程基本信息对象
	 * 
	 * @param flowId
	 * @return
	 * @throws Exception
	 */
	public JecnFlowBasicInfo findJecnFlowBasicInfoById(Long flowId) throws Exception;

	/**
	 * 流程支持工具
	 * 
	 * @param flowId
	 * @return
	 * @throws Exception
	 */
	public List<String> getListTools(Long flowId, boolean isPub) throws Exception;

	/**
	 * 查询流程下指标
	 * 
	 * @author fuzhh Apr 26, 2013
	 * @param flowId
	 *            流程ID
	 * @param isPub
	 *            true为正式表 false 为临时表
	 * @return 0主键ID ， 1指标名 ， 2指标值 , 3活动ID
	 * @throws Exception
	 */
	public List<Object[]> getRefIndicators(Long flowId, boolean isPub) throws Exception;

	/**
	 * @author zhangchen Nov 1, 2012
	 * @description： 角色明细数据 0是角色主键ID 1角色名称 2角色职责 3图形X点 4图形Y点
	 * @param flowId
	 * @return
	 * @throws Exception
	 */
	public List<Object[]> getRoleByFlowId(Long flowId, boolean isPub) throws Exception;

	/**
	 * @author zhangchen Nov 1, 2012
	 * @description： 获取流程是横向还是纵向 0横 1纵
	 * @param flowId
	 * @return
	 * @throws Exception
	 */
	public String getFlowDirectionByFlowId(Long flowId, boolean isPub) throws Exception;

	/**
	 * 获取流程关联文件
	 * 
	 * @author fuzhh 2013-9-3
	 * @param flowId
	 * @param isPub
	 * @return
	 * @throws Exception
	 */
	public List<FileOpenBean> getAssociatedFileByProcessId(Long flowId, boolean isPub) throws Exception;

	/**
	 * 获取流程关联文件ID
	 * 
	 * @author fuzhh 2013-9-3
	 * @param flowId
	 * @param isPub
	 * @return
	 * @throws Exception
	 */
	public List<Long> getAssociatedFileIdByProcessId(Long flowId, Long projectId, Long peopleId, boolean isPub,
			boolean isAdmin) throws Exception;

	/**
	 * 获取流程制度附件
	 * 
	 * @param flowId
	 * @param projectId
	 * @param peopleId
	 * @param isPub
	 * @param isAdmin
	 * @return
	 * @throws Exception
	 */
	public List<Long> getProcessRuleFile(Long flowId, Long projectId, Long peopleId, boolean isPub, boolean isAdmin)
			throws Exception;

	/**
	 * 获取流程标准
	 * 
	 * @param flowId
	 * @param projectId
	 * @param peopleId
	 * @param isPub
	 * @param isAdmin
	 * @return
	 * @throws Exception
	 */
	public List<Long> getProcessStandFile(Long flowId, Long projectId, Long peopleId, boolean isPub, boolean isAdmin)
			throws Exception;

	/**
	 * 获取制度本地上传
	 * 
	 * @param flowId
	 * @param projectId
	 * @param peopleId
	 * @param isPub
	 * @param isAdmin
	 * @return
	 * @throws Exception
	 */
	public List<Object[]> getProcessLocalRuleFile(Long flowId, Long projectId, Long peopleId, boolean isPub,
			boolean isAdmin) throws Exception;

	/**
	 * 获取流程检错信息
	 * 
	 * @author fuzhh 2013-9-9
	 * @param processCheckType
	 *            检错类型
	 * @return
	 * @throws Exception
	 */
	public List<ProcessBaseInfoBean> getProcessCheckList(long processCheckType, int start, int limit, Long projectId)
			throws Exception;

	/**
	 * 获取流程检错信息大小
	 * 
	 * @author fuzhh 2013-9-9
	 * @param processCheckType
	 *            检错类型
	 * @return
	 * @throws Exception
	 */
	public int getProcessCheckCount(long processCheckType, Long projectId) throws Exception;

	/**
	 * 获取流程地图附件集合
	 * 
	 * @param flowId
	 * @return List<JecnFigureFileTBean>
	 */
	List<JecnFigureFileTBean> findJecnFigureFileTList(Long flowId) throws Exception;

	/**
	 * 获取流程所有活动的线上信息集合
	 * 
	 * @param flowId
	 * @return List<JecnActiveOnLineTBean>
	 * @throws Exception
	 */
	List<JecnActiveOnLineTBean> findActiveOnLineTBeans(Long flowId,boolean isPub) throws Exception;

	/**
	 * 活动相关标准集合
	 * 
	 * @param flowId
	 *            流程主键ID
	 * @return
	 * @throws Exception
	 */
	List<JecnActiveStandardBeanT> findActiveStandardBeanTs(Long flowId) throws Exception;

	/**
	 * 获取流程中活动相关控制点
	 * 
	 * @param flowId
	 * @return List<JecnControlPointT> 控制点集合
	 * @throws Exception
	 */
	List<JecnControlPointT> findControlPointTs(Long flowId) throws Exception;

	/**
	 * 获取活动对应的控制点相关信息
	 * 
	 * @param flowId
	 * @return
	 * @throws Exception
	 *             List<JecnControlPointT>
	 */
	List<JecnControlPointT> findListControlPointTAll(Long flowId) throws Exception;

	/**
	 * 根据流程ID查询对应风险
	 * 
	 * @author fuzhh 2013-11-27
	 * @param flowId
	 *            流程ID
	 * @return
	 * @throws Exception
	 */
	public List<JecnRisk> getRiskByFlowId(Long flowId, boolean isPub) throws Exception;

	/**
	 * 活动明细数据 0活动主键ID 1活动编号 2活动名称 3活动说明 4关键活动类型 5关键说明6流程ID
	 * 
	 * @author fuzhh 2013-12-4
	 * @param flowSet
	 * @param isPub
	 * @return
	 * @throws Exception
	 */
	public List<Object[]> getActiveShowBySet(Set<Long> flowSet, boolean isPub) throws Exception;

	/**
	 * @author yxw 2013年12月4日
	 * @description: 流程相关制度
	 * @param ids
	 *            流程ID集合
	 * @return
	 * @throws Exception
	 */
	public List<Object[]> getProcessRules(Set<Long> ids) throws Exception;

	/**
	 * @author yxw 2013年12月4日
	 * @description: 流程相关标准
	 * @param ids
	 *            流程ID集合
	 * @return
	 * @throws Exception
	 */
	public List<Object[]> getProcessStandards(Set<Long> ids) throws Exception;

	/**
	 * @author yxw 2013年12月5日
	 * @description:
	 * @param ids
	 * @return
	 * @throws Exception
	 */
	public List<Object[]> getProcessRisks(Set<Long> ids) throws Exception;

	/**
	 * 查询单个流程对应标准
	 * 
	 * @author fuzhh 2013-12-20
	 * @param flowId
	 * @param isPub
	 * @return
	 * @throws Exception
	 */
	public List<Object[]> getFlowStandards(Long flowId, boolean isPub) throws Exception;

	/**
	 * 
	 * 农夫三泉操作说明下载
	 * 
	 * 查询单个流程对应标准
	 * 
	 * @param flowId
	 * @param isPub
	 * @return
	 * @throws Exception
	 */
	public List<Object[]> getFlowStandardsNFSQ(Long flowId, boolean isPub) throws Exception;

	/**
	 * 获得选中的责任部门下的流程图与它的所有父节点
	 * 
	 * @author huoyl
	 * @param oIds
	 *            责任部门id的集合
	 * @param endTime
	 *            结束时间
	 * @param projectId
	 * @param orgHasChild 
	 * @return list
	 * @throws Exception
	 */
	public List<Object[]> getProcessDetailDutyOrgList(String oIds, long projectId, String endTime, boolean orgHasChild) throws Exception;

	/**
	 * 查询所有有选中责任部门下的所有有流程详细信息（发布日期在给定日期内的）
	 * 
	 * @param orgId
	 *            责任部门id的集合
	 * @param projectId
	 *            项目id
	 * @param endTime
	 * @param orgHasChild 
	 * @return
	 */
	public List<Object[]> getDutyOrgProcessesInfo(String orgId, long projectId, String endTime, boolean orgHasChild);

	/**
	 * 获得选中的流程地图下的流程与流程图
	 * 
	 * @author huoyl
	 * @param mIds
	 *            流程地图id的集合
	 * @param endTime
	 *            结束时间
	 * @return list
	 * @throws Exception
	 */
	public List<Object[]> getProcessDetailProcessMapList(String mIds, long projectId) throws Exception;

	/**
	 * 获得选中的流程地图下的所有流程的详细信息
	 * 
	 * @author huoyl
	 * @param processMapIds
	 *            流程地图ids
	 * @param projectId
	 *            项目id
	 * @param endTime
	 * @return list
	 */
	public List<Object[]> getProcessMapProcessesInfo(String[] processMapIds, long projectId, String endTime)
			throws Exception;

	/**
	 * 获得流程审视优化的详细信息
	 * 
	 * @author huoyl
	 * @param dutyOrgIds
	 * @param startTime
	 * @param endTime
	 * @param projectId
	 * @param processMapIds
	 * @param processDutyIds 
	 * @param orgHasChild 
	 * @return list
	 * @throws Exception
	 */
	public List<Object[]> getAllProcessScanOptimizeList(String dutyOrgIds, String startTime, String endTime,
			long projectId, String processMapIds, String processDutyIds, boolean orgHasChild) throws Exception;

	/**
	 * 
	 * 流程kpi跟踪表数据源
	 * 
	 * @param dutyOrgIds
	 *            String 部门ID集合（1,2,3,4...）
	 * @param projectId
	 *            long 主项目ID
	 * @param startTime
	 *            String 主项目ID（yyyy-MM-dd 00:00:00）
	 * @param endTime
	 *            String 主项目ID (yyyy-MM-dd 23:59:59)
	 * 
	 * @return List<Object[]>
	 * 
	 */
	public List<Object[]> getProcessKPIFollowList(String dutyOrgIds, long projectId, String startTime, String endTime)
			throws Exception;

	/**
	 * 获得下责任部门下两个月需要审视的流程的数量与部门id
	 * 
	 * @author huoyl
	 * @param dutyOrgIds
	 *            部门的ids
	 * @param projectId
	 *            项目id
	 * @param processMapIds
	 * @param processDutyIds 
	 * @param orgHasChild 
	 * @param startTime
	 *            开始时间
	 * @param endTime
	 *            结束时间
	 * @return list
	 */
	public List<Object[]> getNextTwoMonthScanList(String dutyOrgIds, String nextScanStartTime, String nextScanEndTime,
			long projectId, String processMapIds, String processDutyIds, boolean orgHasChild) throws Exception;

	/**
	 * 获得流程架构下流程审视信息
	 * 
	 * @param startTimeAccurate
	 *            开始的日期
	 * @param endTimeAccurate
	 *            结束的日期
	 * @param projectId
	 *            项目id
	 * @param processDutyIds 
	 * @return
	 */
	public List<Object[]> getAllMapProcessScanOptimizeList(List<Object[]> idAndNames, String startTimeAccurate,
			String endTimeAccurate, long projectId, String processDutyIds) throws Exception;

	/**
	 * 获得下流程架构下两个月需要审视的流程的数量与流程架构id
	 * 
	 * @author huoyl
	 * @param dutyOrgIds
	 *            部门的ids
	 * @param projectId
	 *            项目id
	 * @param processDutyIds 
	 * @param startTime
	 *            开始时间
	 * @param endTime
	 *            结束时间
	 * @return list
	 */
	public List<Object[]> getMapNextTwoMonthScanList(String processMapIds, String nextScanStartTime,
			String nextScanEndTime, long projectId, String processDutyIds) throws Exception;

	/**
	 * 获得选中的流程架构下的kpi信息
	 * 
	 * @param processMapIds
	 * @param processMapNames
	 * @param projectId
	 * @param startTime
	 * @param endTime
	 * @return
	 */
	public List<Object[]> getMapProcessKPIFollowList(String processMapIds, long projectId, String startTime,
			String endTime) throws Exception;

	/**
	 * 获得流程有效期维护信息
	 * 
	 * @param dutyOrgIds
	 * @param processMapIds
	 * @param projectId
	 * @param type
	 * @return
	 */
	public List<Object[]> getProcessExpiryMaintenanceDateList(Set<Long> dutyOrgIds, Set<Long> processMapIds,
			Long projectId, int type) throws Exception;

	public List<JecnFlowSustainTool> getSustainTool(long relatedId, int type) throws Exception;

	List<Object[]> getBadfRoleShowByFlowId(Long flowId, boolean isPub) throws Exception;

	List<Object[]> getChildFlows(Long flowMapId, Long projectId, boolean isAdmin) throws Exception;

	List<Object[]> getChildFlowsT(Long flowMapId, Long projectId, boolean isContainsDelNode) throws Exception;

	public List<JecnToFromActiveT> findToFromRelatedActiveT(long id) throws Exception;

	void insertEPROSBPM(Long flowId, String flowName, String version) throws Exception;

	int validateEPROSBPM(Long flowId, String version) throws Exception;

	List<Object[]> getProcessActiveShowByFlowId(Long flowId, boolean isPub) throws Exception;

	public List<TermDefinitionLibrary> getDefinitionsByFlowId(Long processId, boolean isPub) throws Exception;

	public List<ProcessInterface> findFlowInterfaces(Long id, boolean isPub) throws Exception;

	public List<Object[]> getParents(Long flowId, boolean isPub) throws Exception;

	List<Object[]> getTermDefineByFlowId(Long flowId, String isPub) throws Exception;

	Long getRelatedFileId(Long flowId) throws Exception;

	List<Object[]> getParentRelatedFileByPath(Long id) throws Exception;

	void updateParentRelatedFileId(Long flowId, Long relatedFileId) throws Exception;

	List<Long> getRelatedFileIdsByFlowIds(List<Long> ids) throws Exception;

	List<Object[]> getStandardizedFiles(Long flowId) throws Exception;

	public List<Object[]> getRelatedRiskList(Long flowId) throws Exception;

	/**
	 * 获得流程的第一个有岗位的角色的人员信息
	 * 
	 * @return
	 * @throws Exception
	 */
	public List<Object[]> getFirstRoleUsers(Set<Long> flowIdSet) throws Exception;

	/**
	 * 获得收藏的文件被更新的信息
	 * 
	 * @return
	 * @throws Exception
	 */
	public List<Object[]> listFavoriteUpdateInfo() throws Exception;

	Object[] getFlowFileContentName(Long id, String isPub) throws Exception;

	Object[] getFlowFileContent(Long id, String isPub) throws Exception;

	List<Object[]> getPathNameByIds(Set<Long> ids);

	List<Long> getActivityFileByFlowId(Long flowId, WebLoginBean loginBean, String isPub, int accessType) throws Exception;

	List<Long> getRuleFileIdsByFlowId(Long flowId, WebLoginBean loginBean, String isPub, int accessType) throws Exception;

	List<Long> getStandFileByFlowId(Long flowId, WebLoginBean loginBean, String isPub, int accessType) throws Exception;

	List<Object[]> getLocalRuleFileByFlowId(Long flowId, WebLoginBean loginBean, String isPub, int accessType) throws Exception;

	List<Long> getStandardizedFileIds(Long flowId, WebLoginBean loginBean, String isPub, int accessType) throws Exception;

	List<Long> getAcessStandardizedFileIds(Long flowId, Long projectId, Long peopleId, boolean isPub, boolean isAdmin);

	public List<String> getApplyOrgs(Long flowId, boolean isPub);

	public List<Object[]> getDriverUsers(Set<Long> ids);

	/**
	 * 获得流程审视优化的详细信息
	 * 
	 * @author huoyl
	 * @param dutyOrgIds
	 * @param startTime
	 * @param endTime
	 * @param projectId
	 * @param dirIds
	 * @param processDutyIds 
	 * @return list
	 * @throws Exception
	 */
	public List<Object[]> getAllRulesScanOptimizeList(String dutyOrgIds, String startTime, String endTime,
			long projectId, String dirIds,String processDutyIds, boolean orgHasChild) throws Exception;

	public List<Object[]> getRuleNextTwoMonthScanList(String dutyOrgIds, String nextScanStartTime,
			String nextScanEndTime, long projectId, String dirIds, String processDutyIds, boolean orgHasChild) throws Exception;

	public List<Object[]> getRuleDirProcessScanOptimizeList(String dirIds, String startTimeAccurate,
			String endTimeAccurate, long projectId, String processDutyIds);

	public List<Object[]> getRuleDirNextTwoMonthScanList(String processMapIds, String nextScanStartTime,
			String nextScanEndTime, long projectId, String processDutyIds);

	public List<Object[]> finaAllRoleAndPosGroupRelates(Long flowId, boolean isPub);

	public List<Object[]> getActivityRisks(Long flowId, boolean isPub);

	public List<Object[]> getActivityIts(Long flowId, boolean isPub);

	public List<JecnFlowSustainTool> getFlowIts(Long flowId, boolean isPub);

	public List<Long> getRelatedFileIds(List<Long> flowIds) throws Exception;
	public List<JecnTreeBean> getOrgAccessPermissions(Long relateId, int type, boolean isPub) throws Exception;

	public List<Object[]> getDutyOrgRulesInfo(String dutyOrgIds, Long projectId, String endTimeAccurate,
			boolean orgHasChild);

	public List<Object[]> getDirRulesInfo(String[] processMapIdArrs, Long projectId, String endTimeAccurate);

	public List<Object[]> getRulesDetailDutyOrgList(String dutyOrgIds, Long projectId, String endTimeAccurate, boolean orgHasChild);

	public List<Object[]> getRuleDetailDirList(String processMapIds, Long projectId);

	public List<JecnFlowInoutData> getFlowInoutsByType(int type, Long flowId, boolean isPub);

	List<Object[]> getChildFlowsT(List<Long> pids) throws Exception;

}
