<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>


<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	</head>

	<body>
		<div id="div" style="height: 100%;">
			<div id="search" class="divBorder" align="center">
				<div id="myActivitiesSearchText" style="overflow: hidden;"
					class="minWidth">
					<table width="100%">
						<tr>
							<td height="25px" align="right" width="10%">
								<!-- 活动名称： -->
								<s:text name="activityTitleC"></s:text>
							</td>
							<td width="35%" align="left">
								<input type="text" style="width: 80%;" id="activeName_id"
									maxLength="32" />
							</td>

							<td width="10%" align="right">
								<!-- 时间段： -->
								<s:text name="timeC"></s:text>
							</td>
							<td width="35%" align="left">
								<s:textfield readonly="true" cssClass="Wdate" id="startTime"
									onclick="WdatePicker();" theme="simple"
									cssStyle="width:36%;min-width: 90px;border-color: #7F9DB9;" />
								-
								<s:textfield readonly="true" cssClass="Wdate" id="endTime"
									onclick="WdatePicker();" theme="simple"
									cssStyle="width:36%;min-width: 90px;border-color: #7F9DB9;" />
							</td>

							<td style="vertical-align: middle; width: 10%;" align="center">
								<a onClick="toggle(myActivitiesSearchText)"
									style="cursor: pointer;" class="packUp"> <img id="packUp"
										src="${basePath}images/iParticipateInProcess/on.gif"
										align="middle" /> <!-- 收起 --> <span id="searchVal"><s:text
											name="packUp" /> </span> </a>
							</td>
						</tr>
						<tr>
							<td height="25px" align="right">
								<!-- 流程名称: -->
								<s:text name="processNameC" />
							</td>
							<td width="35%" align="left">
								<input type="text" style="width: 80%;" id="processName_id"
									maxLength="32" />
							</td>

							<td align="right">

								<!-- 流程编号: -->
								<s:text name="processNumberC" />
							</td>
							<td width="35%" align="left">
								<input type="text" style="width: 80%;" id="processNumber_id"
									maxLength="32" />
							</td>

							<td></td>
						</tr>
					</table>
				</div>

				<div id="searchBut" align="center">
					<img style="cursor: pointer;"
						src="${basePath}images/iParticipateInProcess/search.gif"
						onclick="myWorkActive()" />
					<img style="cursor: pointer;"
						src="${basePath}images/iParticipateInProcess/reset.gif"
						onclick="myActivitiesReset()" />
				</div>
			</div>

			<div id="myWorkActive_id">

			</div>
		</div>
	</body>
</html>