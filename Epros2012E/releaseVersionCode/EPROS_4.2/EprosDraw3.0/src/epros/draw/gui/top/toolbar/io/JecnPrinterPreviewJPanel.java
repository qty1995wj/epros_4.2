package epros.draw.gui.top.toolbar.io;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.geom.AffineTransform;
import java.util.ArrayList;
import javax.swing.JComponent;
import javax.swing.JPanel;

/**
 * 打印内容显示面板
 */
public class JecnPrinterPreviewJPanel extends JPanel {
	private JecnPrinterWorkflowJPanel workflowPanel;
	private int movX;
	private int movY;

	public JecnPrinterPreviewJPanel(int w, int h, int movX, int movY,
			JecnPrinterWorkflowJPanel workflowPanel) {
		this.workflowPanel = workflowPanel;
		this.movX = movX;
		this.movY = movY;
		this.setPreferredSize(new Dimension(w, h));
	}

	@Override
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		Graphics2D g2d = (Graphics2D) g;
		RenderingHints rhO = g2d.getRenderingHints();
		RenderingHints rh = new RenderingHints(RenderingHints.KEY_ANTIALIASING,
				RenderingHints.VALUE_ANTIALIAS_ON);
		g2d.setRenderingHints(rh);
		JComponent jcomponent = (JComponent) workflowPanel;

		if (!(movX == 0 && movY == 0)) {
			g2d.transform(AffineTransform.getTranslateInstance(-movX, -movY));
		}
		ArrayList<JComponent> dbcomponents = new ArrayList<JComponent>();
		updateDoubleBuffered(jcomponent, dbcomponents);
		jcomponent.paint(g2d);
		resetDoubleBuffered(dbcomponents);

		g2d.setRenderingHints(rhO);
		g2d.dispose();
	}

	private void updateDoubleBuffered(JComponent component,
			ArrayList<JComponent> dbcomponents) {
		if (component.isDoubleBuffered()) {
			dbcomponents.add(component);
			component.setDoubleBuffered(false);
		}
		for (int i = 0; i < component.getComponentCount(); i++) {
			Component c = component.getComponent(i);
			if (c instanceof JComponent) {
				updateDoubleBuffered((JComponent) c, dbcomponents);
			}
		}
	}

	private void resetDoubleBuffered(ArrayList<JComponent> dbcomponents) {
		for (JComponent component : dbcomponents) {
			component.setDoubleBuffered(true);
		}
	}

	public void setMovX(int movX) {
		this.movX = movX;
	}

	public void setMovY(int movY) {
		this.movY = movY;
	}
}
