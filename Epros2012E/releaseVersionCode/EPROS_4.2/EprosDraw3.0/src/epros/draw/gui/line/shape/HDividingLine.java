package epros.draw.gui.line.shape;

import java.awt.Point;

import epros.draw.constant.JecnWorkflowConstant.LineDirectionEnum;
import epros.draw.data.line.JecnBaseDivedingLineData;
import epros.draw.gui.line.JecnBaseDividingLinePanel;

/**
 * 横线 (横向分隔符)
 * 
 * @author ZHANGXH
 * @date： 日期：Jun 25, 2012 时间：11:36:46 AM
 */
public class HDividingLine extends JecnBaseDividingLinePanel {
	public HDividingLine(JecnBaseDivedingLineData flowElementData) {
		super(flowElementData);
		this.directionEnum = LineDirectionEnum.horizontal;
	}

	/**
	 * 获取克隆数据对象
	 */
	public HDividingLine clone() {
		HDividingLine dividingLine = (HDividingLine) this.currClone();
		dividingLine.directionEnum = this.directionEnum;
		return dividingLine;
	}

	/**
	 * 重设位置点
	 * 
	 */
	public void reSetLocation(Point startPoint, Point endPoint) {
		this.setBounds(startPoint, endPoint);
	}
}
