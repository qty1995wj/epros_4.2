package com.jecn.epros.domain.propose;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.util.Date;

public class Propose {

    //--------采纳拒绝等操作页面返回值提示信息标识
    public static final int SUCCESS = 0;
    public static final int HAS_DELETED = 1;
    public static final int HAS_ACCEPTED = 2;
    public static final int HAS_REFUSEED = 3;
    /**
     * 已经采纳不能够编辑
     **/
    public static final int HAS_ACCEPT_CANNOT_EDIT = 4;
    /**
     * 没有操作权限
     **/
    public static final int HAS_NO_PERM = 5;
    /**
     * 合理化建议的提交人不能够删除已被处理的合理化建议
     **/
    public static final int HAS_HANDLE_CAN_NOT_DEL = 6;
    //--------
    public static final int UNREAD = 0;
    public static final int READ = 1;
    public static final int ACCEPT = 2;
    public static final int REFUSE = 3;

    /***主键ID */
    private String id;
    /***关联制度或者流程的ID*/
    private Long relationId;
    /***建议内容*/
    private String proposeContent;
    /***回复内容*/
    private String replyContent;
    /***状态-1全部	0 未读 	1 已读	2 采纳 	 3拒绝*/
    private int state;
    /***回复人*/
    private Long replyPersonId;
    /***回复日期*/
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm", timezone = "GMT+08")
    private Date replyTime;
    /***创建人*/
    private Long createPersonId;
    /***创建日期*/
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm", timezone = "GMT+08")
    private Date createTime;
    /***更新人*/
    private Long updatePersonId;
    /***更新日期*/
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm", timezone = "GMT+08")
    private Date updateTime;
    /**
     * 合理化建议类型：0流程，1制度，2流程架构，3文件，4风险，5标准
     */
    private int type;
    /***附件建议表主键ID*/
    private String listFileIds;
    /***附件建议表文件名称*/
    private String listFileNames;
    /**
     * 创建/更新人员名称
     */
    private String createPeopoleName;
    /**
     * 回复人名称
     */
    private String replyName;
    /**
     * 是否删除合理化建议附件： 0： 否，1： 是
     */
    private int isDeleteFileId = 0;
    /**
     * 合理化建议附件编辑界面主键ID
     */
    private String editProposeFileId;
    /**
     * 页面按钮显示配置
     */
    private ProposeButtonShowConfig buttonShowConfig;
    /**
     * 创建人员删除状态：0：未删除；1：已删除
     */
    private int createPeopleState;
    /**
     * 回复人员删除状态：0：未删除；1：已删除
     */
    private int replyPeopleState;
    /***关联name*/
    private String relationName;
    /**
     * 附件
     **/
    private ProposeFile proposeFile;
    /**
     * 关联的：0流程，1制度，2流程架构，3文件，4风险，5标准
     * 以及该文件的责任人，责任部门等属性
     **/
    private ProposeRelated related;
    /**
     * 0设置的处理人可见1所有人可见
     **/
    private Integer showType = 0;
    /**
     * 文控id
     **/
    private Long historyId;

    private int queryProcessingState;//查询处理人状态 0:专员 1：责任人


    public Integer getShowType() {
        return showType;
    }

    public void setShowType(Integer showType) {
        this.showType = showType;
    }


    public Long getHistoryId() {
        return historyId;
    }

    public void setHistoryId(Long historyId) {
        this.historyId = historyId;
    }

    public ProposeRelated getRelated() {
        return related;
    }

    public void setRelated(ProposeRelated related) {
        this.related = related;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Long getRelationId() {
        return relationId;
    }

    public void setRelationId(Long relationId) {
        this.relationId = relationId;
    }

    public String getProposeContent() {
        return proposeContent;
    }

    public void setProposeContent(String proposeContent) {
        this.proposeContent = proposeContent;
    }

    public String getReplyContent() {
        return replyContent;
    }

    public void setReplyContent(String replyContent) {
        this.replyContent = replyContent;
    }

    public int getState() {
        return state;
    }

    public void setState(int state) {
        this.state = state;
    }

    public Long getReplyPersonId() {
        return replyPersonId;
    }

    public void setReplyPersonId(Long replyPersonId) {
        this.replyPersonId = replyPersonId;
    }

    public Date getReplyTime() {
        return replyTime;
    }

    public void setReplyTime(Date replyTime) {
        this.replyTime = replyTime;
    }

    public Long getCreatePersonId() {
        return createPersonId;
    }

    public void setCreatePersonId(Long createPersonId) {
        this.createPersonId = createPersonId;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Long getUpdatePersonId() {
        return updatePersonId;
    }

    public void setUpdatePersonId(Long updatePersonId) {
        this.updatePersonId = updatePersonId;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getListFileIds() {
        return listFileIds;
    }

    public void setListFileIds(String listFileIds) {
        this.listFileIds = listFileIds;
    }

    public String getListFileNames() {
        return listFileNames;
    }

    public void setListFileNames(String listFileNames) {
        this.listFileNames = listFileNames;
    }

    public String getCreatePeopoleName() {
        return createPeopoleName;
    }

    public void setCreatePeopoleName(String createPeopoleName) {
        this.createPeopoleName = createPeopoleName;
    }

    public String getReplyName() {
        return replyName;
    }

    public void setReplyName(String replyName) {
        this.replyName = replyName;
    }

    public int getIsDeleteFileId() {
        return isDeleteFileId;
    }

    public void setIsDeleteFileId(int isDeleteFileId) {
        this.isDeleteFileId = isDeleteFileId;
    }

    public String getEditProposeFileId() {
        return editProposeFileId;
    }

    public void setEditProposeFileId(String editProposeFileId) {
        this.editProposeFileId = editProposeFileId;
    }

    public ProposeButtonShowConfig getButtonShowConfig() {
        return buttonShowConfig;
    }

    public void setButtonShowConfig(ProposeButtonShowConfig buttonShowConfig) {
        this.buttonShowConfig = buttonShowConfig;
    }

    public int getCreatePeopleState() {
        return createPeopleState;
    }

    public void setCreatePeopleState(int createPeopleState) {
        this.createPeopleState = createPeopleState;
    }

    public int getReplyPeopleState() {
        return replyPeopleState;
    }

    public void setReplyPeopleState(int replyPeopleState) {
        this.replyPeopleState = replyPeopleState;
    }

    public String getRelationName() {
        return relationName;
    }

    public void setRelationName(String relationName) {
        this.relationName = relationName;
    }

    public ProposeFile getProposeFile() {
        return proposeFile;
    }

    public void setProposeFile(ProposeFile proposeFile) {
        this.proposeFile = proposeFile;
    }


    public int getQueryProcessingState() {
        return queryProcessingState;
    }

    public void setQueryProcessingState(int queryProcessingState) {
        this.queryProcessingState = queryProcessingState;
    }
}
