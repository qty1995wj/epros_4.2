package wordxml.element.bean.paragraph;

public class ProjectSignBean {

	/*** 项目符号级别 默认是0 ****/
	private int signLvl = 0;
	/*** 项目符号图标定义 ****/
	private int ilfo;
	/*** 项目符号字体 默认是 Wingdings ***/
	private String font = "Wingdings";

	public ProjectSignBean(int ilfo) {
		this.ilfo = ilfo;
	}

	public ProjectSignBean(int signLvl, int ilfo) {
		this.signLvl = signLvl;
		this.ilfo = ilfo;
	}

	public int getSignLvl() {
		return signLvl;
	}

	public void setSignLvl(int signLvl) {
		this.signLvl = signLvl;
	}

	public int getIlfo() {
		return ilfo;
	}

	public void setIlfo(int ilfo) {
		this.ilfo = ilfo;
	}

	public String getFont() {
		return font;
	}

	public void setFont(String font) {
		this.font = font;
	}

}
