package com.jecn.svg;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;

import com.jecn.svg.dataModel.SvgBaseGraph;
import com.jecn.svg.dataModel.SvgGraphStyle;
import com.jecn.svg.dataModel.epros.SvgEprosGraphData;
import com.jecn.svg.dataModel.epros.TimeLineBaseData;
import com.jecn.svg.dataModel.epros.TimeLineDataHandler;
import com.jecn.svg.dataModel.graphs.SvgTimeLine;

public class SvgSpecialGraphBuilder {

	/**
	 * 办理时限 时间轴 构造入口
	 * 
	 * @param graphDataList
	 * @return
	 */
	public static SvgBaseGraph buildTimeLineSvgGraph(List<SvgEprosGraphData> graphDataList) {
		SvgTimeLine timeLine = new SvgTimeLine();
		timeLine.setType("FlowTimeLine");
		SvgGraphStyle graphStyle = new SvgGraphStyle();
		graphStyle.setStroke("#000");
		graphStyle.setStrokeWidth("1");
		timeLine.setGraphStyle(graphStyle);
		Map<Long, TimeLineBaseData> sourceData = buildTimeLineBaseData(graphDataList);
		TimeLineDataHandler handler = new TimeLineDataHandler();

		// 设置页面基本 属性
		handler.getTimeLineNodes(sourceData, timeLine);
		if (timeLine.getNodes() == null) {
			return null;
		}
		return timeLine;
	}

	/**
	 * 组件时间轴数据层 按活动ID分组
	 * 
	 * @param graphDataList
	 * @return
	 */
	private static Map<Long, TimeLineBaseData> buildTimeLineBaseData(List<SvgEprosGraphData> graphDataList) {
		Map<Long, TimeLineBaseData> sourceData = new HashMap<Long, TimeLineBaseData>();
		for (SvgEprosGraphData graphData : graphDataList) {
			TimeLineBaseData baseData = new TimeLineBaseData();
			baseData.setId(graphData.getFigureId());
			baseData.setNumber(graphData.getActivityId());
			baseData.setType(graphData.getFigureType());
			baseData.setStartDataId(graphData.getStartFigure());
			baseData.setEndDataId(graphData.getEndFigure());
			baseData.setX(graphData.getPoLongx());
			baseData.setY(graphData.getPoLongy());
			baseData.setWidth(graphData.getWidth());
			baseData.setHeight(graphData.getHeight());
			baseData.setTargetValue(valueOfDouble(graphData.getTargetValue()));
			baseData.setStatusValue(valueOfDouble(graphData.getStatusValue()));
			sourceData.put(graphData.getFigureId(), baseData);
		}
		return sourceData;
	}

	private static double valueOfDouble(String value) {
		return StringUtils.isBlank(value) ? 0 : Double.valueOf(value);
	}
}
