package com.jecn.epros.server.service.dataImport.sync.excelOrDb.impl;

import java.io.IOException;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.jecn.epros.server.bean.dataimport.BasePosBean;
import com.jecn.epros.server.common.JecnCommon;
import com.jecn.epros.server.service.dataImport.sync.excelOrDb.AbstractImportUser;
import com.jecn.epros.server.service.dataImport.sync.excelOrDb.unit.SyncErrorInfo;
import com.jecn.epros.server.service.dataImport.sync.excelOrDb.unit.SyncTool;
import com.jecn.epros.server.webBean.dataImport.sync.excelOrDB.AbstractConfigBean;
import com.jecn.epros.server.webBean.dataImport.sync.excelOrDB.DBConfigBean;
import com.jecn.epros.server.webBean.dataImport.sync.excelOrDB.DeptBean;
import com.jecn.epros.server.webBean.dataImport.sync.excelOrDB.UserBean;

/**
 * 
 * 由于目前只有农夫山泉存在基准岗位同步业务，所以添加此类
 * 
 * 
 */
public class ImportBasePos extends AbstractImportUser {

	protected final Log log = LogFactory.getLog(this.getClass());
	/** 部门 */
	private List<DeptBean> deptBeanList = new ArrayList<DeptBean>();
	/** 人员 */
	private List<UserBean> userBeanList = new ArrayList<UserBean>();
	/** 基准岗位及与岗位关系集合 **/
	private List<BasePosBean> basePosBeanList = new ArrayList<BasePosBean>();
	/** 执行读取标识(true:表示执行过；false：标识没有执行过) */
	protected boolean readedFlag = false;

	public ImportBasePos(AbstractConfigBean configBean) {

		super(configBean);
	}

	/**
	 * 
	 * 获取部门数据集合
	 * 
	 * @return
	 */
	public List<DeptBean> getDeptBeanList() throws Exception {
		// 执行读取数据库数据
		readDB();

		return deptBeanList;
	}

	/**
	 * 
	 * 获取人员数据集合
	 * 
	 * @return
	 */
	public List<UserBean> getUserBeanList() throws Exception {
		// 执行读取数据库数据
		readDB();

		return userBeanList;
	}

	/**
	 * 获取基准岗位数据集合
	 * 
	 * @author cheshaowei
	 * 
	 * */
	public List<BasePosBean> getBasePosBeanList() throws Exception {

		readDB();

		return basePosBeanList;

	}

	/**
	 * @throws BsException
	 * @throws Exception
	 * 
	 * 
	 * 
	 */
	private void readDB() throws Exception {
		// 执行过就不执行
		if (readedFlag) {
			return;
		}

		// Excel文件读取，部门、岗位、人员数据都全部取出来
		try {
			selectDB((DBConfigBean) configBean);
		} catch (Exception e) {
			throw new IOException(SyncErrorInfo.SELECT_DB_FAIL);
		}
	}

	/**
	 * 
	 * 查询数据库返回查询结果
	 * 
	 * @param configBean
	 */
	private void selectDB(DBConfigBean configBean) throws Exception {
		// 部门查询返回记录集
		ResultSet resDept = null;
		// 人员查询返回记录集
		ResultSet resUser = null;
		// 基准岗位查询结果集
		ResultSet resBasePos = null;
		Statement stmt = null;
		Connection conn = null;

		CallableStatement callStmt = null;
		try {
			// 加载的驱动类
			Class.forName(configBean.getDriver());
			// 创建数据库连接
			conn = DriverManager.getConnection(configBean.getUrl(), configBean
					.getUsername(), configBean.getPassword());

			// 创建一个Statement
			stmt = conn.createStatement();
			// 执行查询，返回结果集
			// 部门
			resDept = stmt.executeQuery(configBean.getDeptSql());

			getDpetList(resDept);

			// 东航必须执行存储过程，否则没有权限访问视图
			if (StringUtils.isNotEmpty(configBean.getProcedureSql())) {
				callStmt = conn.prepareCall(configBean.getProcedureSql());
				Date curDate = JecnCommon.getDateByString(JecnCommon
						.getStringbyDate(new Date()));
				callStmt.setDate(1, new java.sql.Date(curDate.getTime()));
				callStmt.executeUpdate();
				log.info("存储过程执行成功！");
			}
			// 人员
			resUser = stmt.executeQuery(configBean.getUserSql());

			getUserList(resUser);

			// 获取基准岗位及与岗位关系
			resBasePos = stmt.executeQuery(configBean.getBasePosSql());
			getBasePosList(resBasePos);

			readedFlag = true;
		} catch (ClassNotFoundException e) {
			log.error("ImportUserByDB类的selectDB方法加驱动时出错：" + e);
			throw e;
		} catch (SQLException e) {
			log.error("ImportUserByDB类的selectDB方法创建连接时出错：" + e);
			throw e;
		} finally {
			if (resUser != null) {// 关闭记录集
				try {
					resUser.close();
				} catch (SQLException e) {
					log
							.error("ImportUserByDB类的selectDB方法关闭人员记录集（ResultSet）时出错："
									+ e);
					throw e;
				}
			}
			if (resDept != null) {// 关闭记录集
				try {
					resDept.close();
				} catch (SQLException e) {
					log
							.error("ImportUserByDB类的selectDB方法关闭部门记录集（ResultSet）时出错："
									+ e);
					throw e;
				}
			}
			if (stmt != null) { // 关闭声明
				try {
					stmt.close();
				} catch (SQLException e) {
					log.error("ImportUserByDB类的selectDB方法关闭声明（Statement）时出错："
							+ e);
					throw e;
				}
			}
			if (conn != null) { // 关闭连接对象
				try {
					conn.close();
				} catch (SQLException e) {
					log.error("ImportUserByDB类的selectDB方法关闭连接对象（Connection）时出错："
									+ e);
					throw e;
				}
			}
			if (callStmt != null) {
				callStmt.close();
			}
		}
	}

	/**
	 * 
	 * 把数据取出结果集存储到bean对象中
	 * 
	 * @param resDept
	 * @throws SQLException
	 */
	protected void getDpetList(ResultSet resDept) throws SQLException {
		while (resDept.next()) {
			DeptBean deptBean = new DeptBean();
			// 部门编号
			String deptNum = resDept.getString(1);
			// 部门名称
			String deptName = resDept.getString(2);
			// 上级部门编号
			String perDeptNum = resDept.getString(3);
			// 部门编号
			deptBean.setDeptNum(SyncTool.emtryToNull(deptNum));
			// 部门名称
			deptBean.setDeptName(SyncTool.emtryToNull(deptName));
			// 上级部门编号
			deptBean.setPerDeptNum(SyncTool.emtryToNull(perDeptNum));

			this.deptBeanList.add(deptBean);
		}
	}

	/**
	 * 
	 * 把数据取出结果集存储到bean对象中
	 * 
	 * @param resUser
	 * @throws SQLException
	 */
	protected void getUserList(ResultSet resUser) throws SQLException {
		while (resUser.next()) {
			UserBean userBean = new UserBean();

			// 下面的顺序不要轻易改变
			// 登录名称
			String userNum = resUser.getString(1);
			// 真实姓名
			String trueName = resUser.getString(2);
			// 任职岗位编号
			String posNum = resUser.getString(3);
			// 任职岗位名称
			String posName = resUser.getString(4);
			// 岗位所属部门编号
			String deptNum = resUser.getString(5);
			// 邮箱地址
			String email = resUser.getString(6);
			// 内外网邮件标识(内网:0 外网:1)
			String emailType = resUser.getString(7);
			// 联系电话
			String phone = resUser.getString(8);

			// 登录名称
			userBean.setUserNum(SyncTool.emtryToNull(userNum));
			// 真实姓名
			userBean.setTrueName(SyncTool.emtryToNull(trueName));
			// 任职岗位编号
			userBean.setPosNum(SyncTool.emtryToNull(posNum));
			// 任职岗位名称
			userBean.setPosName(SyncTool.emtryToNull(posName));
			// 岗位所属部门编号
			userBean.setDeptNum(SyncTool.emtryToNull(deptNum));
			// 邮箱地址
			userBean.setEmail(SyncTool.emtryToNull(email));
			// 邮箱、邮箱类型不等于空且是“0”，“1”
			if (SyncTool.isEmailTypeStr(emailType)) {
				// 内外网邮件标识(内网:0 外网:1)
				userBean.setEmailType(Integer.valueOf(emailType));
			}
			// 联系电话
			userBean.setPhone(SyncTool.emtryToNull(phone));

			this.userBeanList.add(userBean);
		}

	}
	

	
//	/**
//	 * 基准岗位岗位关系集合
//	 * 
//	 * @author cheshaowei
//	 * @param configBean
//	 *            配置文件bean
//	 * @return
//	 * */
//	public List<BasePosBean> basePosList(DBConfigBean configBean)
//			throws Exception {
//
//		ResultSet resBasePos = null; // 查询结果集
//		Statement st = null;
//		Connection conn = null;
//
//		try {
//			// 加载数据驱动
//			Class.forName(configBean.getDriver());
//			// 创建数据库连接
//			conn = DriverManager.getConnection(configBean.getUrl(), configBean
//					.getUsername(), configBean.getPassword());
//
//			st = conn.createStatement();
//			// 执行查询，并且返回查询结果集
//			resBasePos = st.executeQuery(configBean.getBasePosSql());
//
//			getBasePosList(resBasePos);
//			
//			return basePosBeanList;
//		} catch (Exception e) {
//			log.error("执行jdbc数据库查询出现错误! line 57 ImportBasePos basePosList()", e);
//			return null;
//		}
//	}

	/**
	 * 
	 * 将数据集合添加到BasePosBean中
	 * 
	 * @author cheshaowei
	 * @param ResultSet
	 *            jdbc查询结果集
	 * 
	 * 
	 * **/
	private void getBasePosList(ResultSet resBasePos) throws SQLException {

		try {
			while (resBasePos.next()) {
				// 基准岗位编号
				String basePosNum = resBasePos.getString(1);
				// 基准岗位名称
				String basePosName = resBasePos.getString(2);
				// 岗位编号
				String posNum = resBasePos.getString(4);
				// 岗位名称
				String posName = resBasePos.getString(5);
				// 基准岗位和岗位对应Bean
				BasePosBean basePosBean = new BasePosBean();
				basePosBean.setBasePosNum(basePosNum);
				basePosBean.setBasePosName(basePosName);
				basePosBean.setPosNum(posNum);
				basePosBean.setPosName(posName);
				this.basePosBeanList.add(basePosBean);
			}
		} catch (Exception e) {
			log.info("数据集转换发生错误! line98 ImportBasePos getBasePosList");
		}
	}
	
	

}
