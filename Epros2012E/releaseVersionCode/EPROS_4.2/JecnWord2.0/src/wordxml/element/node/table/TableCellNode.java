package wordxml.element.node.table;

import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.dom4j.Element;

import wordxml.element.bean.common.ShdBean;
import wordxml.element.bean.table.CellBean;
import wordxml.element.dom.IBodyElement;
import wordxml.element.dom.WordElementFactory;
import wordxml.element.dom.table.TableCell;
import wordxml.element.dom.table.TableRow;
import wordxml.element.node.common.CommonUtil;
import wordxml.exception.WordException;
import wordxml.util.Tools;
import wordxml.util.Unit;

public class TableCellNode {
	private static final Log log = LogFactory.getLog(TableCellNode.class);

	/***
	 * 创建表格行中的单个格子
	 * 
	 * @param tr
	 * @param tableCell
	 */
	public static void createTblCell(Element tr, TableCell tableCell) {
		if (tr == null || tableCell == null) {
			log.info("TableCellNode class createTblCell Argument is not valid");
			return;
		}
		Element tc = tr.addElement("w:tc");
		// 设置单个单元格属性
		setCellPr(tc, tableCell);
		setCellDate(tc, tableCell);
	}

	/**
	 * 设置单元格属性
	 */
	private static void setCellPr(Element tc, TableCell tableCell) {
		// 单个单元格属性
		Element tcPr = tc.addElement("w:tcPr");

		// 单元格宽，此值能影响到列宽
		tcPr.addElement("w:tcW").addAttribute("w:w",
				Unit.convertCmToTwip(tableCell.getWidth())).addAttribute(
				"w:type", "dxa");//
		// <w:gridSpan w:val="2"/> 一个cell占2列
		if (tableCell.getColspan() > 1) {
			tcPr.addElement("w:gridSpan").addAttribute("w:val",
					"" + tableCell.getColspan());
		}

		if (tableCell.getvAlign() != null) {
			// 文本垂直对齐方式
			tcPr.addElement("w:vAlign").addAttribute("w:val",
					tableCell.getvAlign().toString());
		}

		// 设置的跨行数超过1行时，启用跨行处理
		if (tableCell.getRowspan() > 1) {
			// 设置开始标识
			tableCell.setVmergeBeg(true);
			// 获取当前列所在表格内的所有行
			List<TableRow> rows = tableCell.getTableRow().getTable().getRows();
			// 获取起始行 （当前行+1）
			int start = rows.indexOf(tableCell.getTableRow()) + 1;
			// 获取结束行（起始行+跨行数-1）
			int end = start + (tableCell.getRowspan() - 1);
			// 获取要设置跨行的列的索引
			int cellIndex = tableCell.getTableRow().getCells().indexOf(
					tableCell);
			for (int i = start; i < rows.size() && i < end; i++) {
				// 设置[被跨行]标识
				rows.get(i).getCell(cellIndex).setVmerge(true);
			}
		}
		// <w:vmerge w:val="restart"/> 开始跨行标识
		if (tableCell.isVmergeBeg()) {
			tcPr.addElement("w:vmerge").addAttribute("w:val", "restart");
		}
		// <w:vmerge/> 被跨行标识
		if (tableCell.isVmerge()) {
			tcPr.addElement("w:vmerge");
		}

		CellBean cellBean = tableCell.getCellBean();
		if (cellBean == null) {
			return;
		}

		// 表格底纹
		// 底纹
		ShdBean shdBean = cellBean.getShdBean();
		if (shdBean != null) {
			// <w:shd w:val="pct-15" w:color="auto" w:fill="FFFFFF"
			// wx:bgcolor="D9D9D9"/>

			// <w:shd w:val="clear" w:color="auto" w:fill="E6E6E6"/>

			Element shd = tcPr.addElement("w:shd");
			shd.addAttribute("w:val", shdBean.getVal().getValue());
			shd.addAttribute("w:color", shdBean.getColor());
			shd.addAttribute("w:fill", Tools.convertColorToRGBStr(shdBean
					.getFill()));
			// shd.addAttribute("wx:bgcolor",
			// cellBean.getShdBean().getBgcolor());
		}
		// 单元格边框线
		if (bordersNotNull(cellBean)) {
			Element tcBorders = tcPr.addElement("w:tcBorders");
			if (cellBean.getBorderTop() != null) {
				Element borderEle = tcBorders.addElement("w:top");
				// 构建边框属性值
				CommonUtil.buildBorder(cellBean.getBorderTop(), borderEle);
			}
			if (cellBean.getBorderLeft() != null) {
				Element borderEle = tcBorders.addElement("w:left");
				CommonUtil.buildBorder(cellBean.getBorderLeft(), borderEle);
			}
			if (cellBean.getBorderBottom() != null) {
				Element borderEle = tcBorders.addElement("w:bottom");
				CommonUtil.buildBorder(cellBean.getBorderBottom(), borderEle);
			}
			if (cellBean.getBorderRight() != null) {
				Element borderEle = tcBorders.addElement("w:right");
				CommonUtil.buildBorder(cellBean.getBorderRight(), borderEle);
			}
		}
	}

	/**
	 * 单元格边框都为空
	 * 
	 * @author weidp
	 * @date 2014-10-22 上午10:51:53
	 * @param cellBean
	 * @return
	 */
	private static boolean bordersNotNull(CellBean cellBean) {
		return cellBean.getBorderTop() != null
				|| cellBean.getBorderLeft() != null
				|| cellBean.getBorderBottom() != null
				|| cellBean.getBorderRight() != null;
	}

	/**
	 * 添加单元格数据
	 * 
	 * @param tc
	 * @param list
	 * @throws WordException
	 */
	private static void setCellDate(Element tc, TableCell tableCell) {
		List<IBodyElement> list = tableCell.getBodyElements();
		for (IBodyElement bodyEle : list) {
			WordElementFactory.produceElement(bodyEle, tc);
		}

	}
}
