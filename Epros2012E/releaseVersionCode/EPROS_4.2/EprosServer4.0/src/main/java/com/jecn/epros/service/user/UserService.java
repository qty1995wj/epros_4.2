package com.jecn.epros.service.user;

import com.jecn.epros.domain.JecnUser;
import com.jecn.epros.domain.MyStar;
import com.jecn.epros.domain.UserResourceStatistics;
import com.jecn.epros.domain.ViewResultWithMsg;
import com.jecn.epros.domain.org.SearchPosResultBean;
import com.jecn.epros.domain.process.FlowSearchResultBean;
import com.jecn.epros.domain.process.map.DeptAerialData;
import com.jecn.epros.domain.report.base.CharItem;
import com.jecn.epros.domain.report.base.LineChartOptions;
import com.jecn.epros.domain.risk.SearchRiskResultBean;
import com.jecn.epros.domain.rule.SearchRuleResultBean;
import com.jecn.epros.domain.user.MyResponsibilityProcess;
import com.jecn.epros.domain.user.MyResponsibilityRule;
import com.jecn.epros.domain.user.PermissionsMenuInfo;
import com.jecn.epros.domain.user.TreeNodeAccess;

import java.util.List;
import java.util.Map;

public interface UserService {

    List<MyStar> getRecommendStars(Map<String, Object> map);

    List<MyStar> getMyStars(Map<String, Object> map);

    MyStar getStar(Map<String, Object> params);

    void deleteStar(Map<String, Object> map);

    void updateStar(Map<String, Object> map);

    void addStar(Map<String, Object> map);

    UserResourceStatistics getMyResources(Map<String, Object> paramMap);

    List<SearchRuleResultBean> getMyRules(Map<String, Object> paramMap);

    List<SearchRiskResultBean> getMyRisks(Map<String, Object> paramMap);

    List<FlowSearchResultBean> getMyProcesses(Map<String, Object> paramMap);

    List<TreeNodeAccess> getDefaultWebAccess();

    List<TreeNodeAccess> getWebAccessByPeopleId(Map<String, Object> paramMap);

    void updateDefaultWebAccess(Map<String, Object> paramMap);

    void updateWebAccess(Map<String, Object> paramMap);

    /**
     * 获取登录角色对应的权限菜单
     *
     * @param roleId
     * @return
     */
    public PermissionsMenuInfo getPermissionsMenuById(List<String> roleId,int languageType);

    int getMyStarsNum(Map<String, Object> params);

    LineChartOptions getVisitTopProcess(Map<String, Object> paramMap);

    List<CharItem> getProposeTopProcess(Map<String, Object> paramMap);

    List<CharItem> getProposeTopRule(Map<String, Object> paramMap);

    List<CharItem> getVisitTopProcessHistogram(Map<String, Object> paramMap);

    List<CharItem> getVisitTopRuleHistogram(Map<String, Object> paramMap);

    JecnUser getUser(Map<String, Object> paramMap);

    List<SearchPosResultBean> listUserOrgAndPosInfos(Map<String, Object> paramMap);

    List<MyResponsibilityProcess> findMyResponsibilityProcess(Map<String, Object> map);

    List<MyResponsibilityRule> findMyResponsibilityRule(Map<String, Object> map);

    ViewResultWithMsg alterUserPwd(Map<String, Object> paramMap);

    List<DeptAerialData> findDeptAerialMapsByPeople(Map<String, Object> map);

    Long getUserIdByLoginName(Map map);
}
