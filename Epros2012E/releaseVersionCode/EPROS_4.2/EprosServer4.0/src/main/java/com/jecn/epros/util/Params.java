package com.jecn.epros.util;

import com.jecn.epros.domain.Paging;

import java.util.HashMap;
import java.util.Map;

public class Params {

    public static final String PG = "paging";

    private Map<String, Object> container;

    public Params() {
        container = new HashMap<String, Object>();
    }

    public Params(String key, Object value) {
        this();
        add(key, value);
    }

    public Params add(String key, Object value) {
        this.container.put(key, value);
        return this;
    }

    public Params add(Paging paging) {
        this.container.put(PG, paging);
        return this;
    }

    public Map<String, Object> toMap() {
        return this.container;
    }
}
