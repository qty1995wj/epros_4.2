
package com.jecn.webservice.cetc29;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.meritit.ws.intfsservicewsxml package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _ImportEffectDataXMLResponse_QNAME = new QName("http://www.meritit.com/ws/IntfsServiceWSXML", "importEffectDataXMLResponse");
    private final static QName _ImportDataProCodeAvailXML_QNAME = new QName("http://www.meritit.com/ws/IntfsServiceWSXML", "importDataProCodeAvailXML");
    private final static QName _ImportEditingDataXML_QNAME = new QName("http://www.meritit.com/ws/IntfsServiceWSXML", "importEditingDataXML");
    private final static QName _ImportEditingDataXMLResponse_QNAME = new QName("http://www.meritit.com/ws/IntfsServiceWSXML", "importEditingDataXMLResponse");
    private final static QName _GetCodeInfoXML_QNAME = new QName("http://www.meritit.com/ws/IntfsServiceWSXML", "getCodeInfoXML");
    private final static QName _GetDicInfoXMLResponse_QNAME = new QName("http://www.meritit.com/ws/IntfsServiceWSXML", "getDicInfoXMLResponse");
    private final static QName _ImportDataProCodeAuditXML_QNAME = new QName("http://www.meritit.com/ws/IntfsServiceWSXML", "importDataProCodeAuditXML");
    private final static QName _GetDicInfoXML_QNAME = new QName("http://www.meritit.com/ws/IntfsServiceWSXML", "getDicInfoXML");
    private final static QName _ImportEffectDataXML_QNAME = new QName("http://www.meritit.com/ws/IntfsServiceWSXML", "importEffectDataXML");
    private final static QName _ProduceCodeEditXML_QNAME = new QName("http://www.meritit.com/ws/IntfsServiceWSXML", "produceCodeEditXML");
    private final static QName _ProduceCodeAuditXML_QNAME = new QName("http://www.meritit.com/ws/IntfsServiceWSXML", "produceCodeAuditXML");
    private final static QName _ImportFlowDataXMLResponse_QNAME = new QName("http://www.meritit.com/ws/IntfsServiceWSXML", "importFlowDataXMLResponse");
    private final static QName _GetCodeInfoXMLResponse_QNAME = new QName("http://www.meritit.com/ws/IntfsServiceWSXML", "getCodeInfoXMLResponse");
    private final static QName _GetModelDatasXMLResponse_QNAME = new QName("http://www.meritit.com/ws/IntfsServiceWSXML", "getModelDatasXMLResponse");
    private final static QName _ProduceCodeAvailXML_QNAME = new QName("http://www.meritit.com/ws/IntfsServiceWSXML", "produceCodeAvailXML");
    private final static QName _ImportDataProCodeEditXML_QNAME = new QName("http://www.meritit.com/ws/IntfsServiceWSXML", "importDataProCodeEditXML");
    private final static QName _ImportDataProCodeAvailXMLResponse_QNAME = new QName("http://www.meritit.com/ws/IntfsServiceWSXML", "importDataProCodeAvailXMLResponse");
    private final static QName _ProduceCodeAvailXMLResponse_QNAME = new QName("http://www.meritit.com/ws/IntfsServiceWSXML", "produceCodeAvailXMLResponse");
    private final static QName _GetModelDatasXML_QNAME = new QName("http://www.meritit.com/ws/IntfsServiceWSXML", "getModelDatasXML");
    private final static QName _ProduceCodeAuditXMLResponse_QNAME = new QName("http://www.meritit.com/ws/IntfsServiceWSXML", "produceCodeAuditXMLResponse");
    private final static QName _ImportFlowDataXML_QNAME = new QName("http://www.meritit.com/ws/IntfsServiceWSXML", "importFlowDataXML");
    private final static QName _ProduceCodeEditXMLResponse_QNAME = new QName("http://www.meritit.com/ws/IntfsServiceWSXML", "produceCodeEditXMLResponse");
    private final static QName _ImportDataProCodeEditXMLResponse_QNAME = new QName("http://www.meritit.com/ws/IntfsServiceWSXML", "importDataProCodeEditXMLResponse");
    private final static QName _ImportDataProCodeAuditXMLResponse_QNAME = new QName("http://www.meritit.com/ws/IntfsServiceWSXML", "importDataProCodeAuditXMLResponse");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.meritit.ws.intfsservicewsxml
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link GetCodeInfoXMLResponse }
     * 
     */
    public GetCodeInfoXMLResponse createGetCodeInfoXMLResponse() {
        return new GetCodeInfoXMLResponse();
    }

    /**
     * Create an instance of {@link ImportEditingDataXML }
     * 
     */
    public ImportEditingDataXML createImportEditingDataXML() {
        return new ImportEditingDataXML();
    }

    /**
     * Create an instance of {@link ProduceCodeEditXMLResponse }
     * 
     */
    public ProduceCodeEditXMLResponse createProduceCodeEditXMLResponse() {
        return new ProduceCodeEditXMLResponse();
    }

    /**
     * Create an instance of {@link ProduceCodeAvailXML }
     * 
     */
    public ProduceCodeAvailXML createProduceCodeAvailXML() {
        return new ProduceCodeAvailXML();
    }

    /**
     * Create an instance of {@link ImportDataProCodeAvailXML }
     * 
     */
    public ImportDataProCodeAvailXML createImportDataProCodeAvailXML() {
        return new ImportDataProCodeAvailXML();
    }

    /**
     * Create an instance of {@link ImportDataProCodeAvailXMLResponse }
     * 
     */
    public ImportDataProCodeAvailXMLResponse createImportDataProCodeAvailXMLResponse() {
        return new ImportDataProCodeAvailXMLResponse();
    }

    /**
     * Create an instance of {@link ImportDataProCodeEditXMLResponse }
     * 
     */
    public ImportDataProCodeEditXMLResponse createImportDataProCodeEditXMLResponse() {
        return new ImportDataProCodeEditXMLResponse();
    }

    /**
     * Create an instance of {@link ProduceCodeAvailXMLResponse }
     * 
     */
    public ProduceCodeAvailXMLResponse createProduceCodeAvailXMLResponse() {
        return new ProduceCodeAvailXMLResponse();
    }

    /**
     * Create an instance of {@link ImportFlowDataXMLResponse }
     * 
     */
    public ImportFlowDataXMLResponse createImportFlowDataXMLResponse() {
        return new ImportFlowDataXMLResponse();
    }

    /**
     * Create an instance of {@link GetDicInfoXMLResponse }
     * 
     */
    public GetDicInfoXMLResponse createGetDicInfoXMLResponse() {
        return new GetDicInfoXMLResponse();
    }

    /**
     * Create an instance of {@link ImportEffectDataXMLResponse }
     * 
     */
    public ImportEffectDataXMLResponse createImportEffectDataXMLResponse() {
        return new ImportEffectDataXMLResponse();
    }

    /**
     * Create an instance of {@link ProduceCodeAuditXML }
     * 
     */
    public ProduceCodeAuditXML createProduceCodeAuditXML() {
        return new ProduceCodeAuditXML();
    }

    /**
     * Create an instance of {@link ImportEditingDataXMLResponse }
     * 
     */
    public ImportEditingDataXMLResponse createImportEditingDataXMLResponse() {
        return new ImportEditingDataXMLResponse();
    }

    /**
     * Create an instance of {@link GetDicInfoXML }
     * 
     */
    public GetDicInfoXML createGetDicInfoXML() {
        return new GetDicInfoXML();
    }

    /**
     * Create an instance of {@link ProduceCodeEditXML }
     * 
     */
    public ProduceCodeEditXML createProduceCodeEditXML() {
        return new ProduceCodeEditXML();
    }

    /**
     * Create an instance of {@link GetCodeInfoXML }
     * 
     */
    public GetCodeInfoXML createGetCodeInfoXML() {
        return new GetCodeInfoXML();
    }

    /**
     * Create an instance of {@link ImportFlowDataXML }
     * 
     */
    public ImportFlowDataXML createImportFlowDataXML() {
        return new ImportFlowDataXML();
    }

    /**
     * Create an instance of {@link ImportDataProCodeAuditXMLResponse }
     * 
     */
    public ImportDataProCodeAuditXMLResponse createImportDataProCodeAuditXMLResponse() {
        return new ImportDataProCodeAuditXMLResponse();
    }

    /**
     * Create an instance of {@link ImportDataProCodeEditXML }
     * 
     */
    public ImportDataProCodeEditXML createImportDataProCodeEditXML() {
        return new ImportDataProCodeEditXML();
    }

    /**
     * Create an instance of {@link ImportEffectDataXML }
     * 
     */
    public ImportEffectDataXML createImportEffectDataXML() {
        return new ImportEffectDataXML();
    }

    /**
     * Create an instance of {@link ImportDataProCodeAuditXML }
     * 
     */
    public ImportDataProCodeAuditXML createImportDataProCodeAuditXML() {
        return new ImportDataProCodeAuditXML();
    }

    /**
     * Create an instance of {@link GetModelDatasXMLResponse }
     * 
     */
    public GetModelDatasXMLResponse createGetModelDatasXMLResponse() {
        return new GetModelDatasXMLResponse();
    }

    /**
     * Create an instance of {@link ProduceCodeAuditXMLResponse }
     * 
     */
    public ProduceCodeAuditXMLResponse createProduceCodeAuditXMLResponse() {
        return new ProduceCodeAuditXMLResponse();
    }

    /**
     * Create an instance of {@link GetModelDatasXML }
     * 
     */
    public GetModelDatasXML createGetModelDatasXML() {
        return new GetModelDatasXML();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ImportEffectDataXMLResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.meritit.com/ws/IntfsServiceWSXML", name = "importEffectDataXMLResponse")
    public JAXBElement<ImportEffectDataXMLResponse> createImportEffectDataXMLResponse(ImportEffectDataXMLResponse value) {
        return new JAXBElement<ImportEffectDataXMLResponse>(_ImportEffectDataXMLResponse_QNAME, ImportEffectDataXMLResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ImportDataProCodeAvailXML }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.meritit.com/ws/IntfsServiceWSXML", name = "importDataProCodeAvailXML")
    public JAXBElement<ImportDataProCodeAvailXML> createImportDataProCodeAvailXML(ImportDataProCodeAvailXML value) {
        return new JAXBElement<ImportDataProCodeAvailXML>(_ImportDataProCodeAvailXML_QNAME, ImportDataProCodeAvailXML.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ImportEditingDataXML }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.meritit.com/ws/IntfsServiceWSXML", name = "importEditingDataXML")
    public JAXBElement<ImportEditingDataXML> createImportEditingDataXML(ImportEditingDataXML value) {
        return new JAXBElement<ImportEditingDataXML>(_ImportEditingDataXML_QNAME, ImportEditingDataXML.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ImportEditingDataXMLResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.meritit.com/ws/IntfsServiceWSXML", name = "importEditingDataXMLResponse")
    public JAXBElement<ImportEditingDataXMLResponse> createImportEditingDataXMLResponse(ImportEditingDataXMLResponse value) {
        return new JAXBElement<ImportEditingDataXMLResponse>(_ImportEditingDataXMLResponse_QNAME, ImportEditingDataXMLResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetCodeInfoXML }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.meritit.com/ws/IntfsServiceWSXML", name = "getCodeInfoXML")
    public JAXBElement<GetCodeInfoXML> createGetCodeInfoXML(GetCodeInfoXML value) {
        return new JAXBElement<GetCodeInfoXML>(_GetCodeInfoXML_QNAME, GetCodeInfoXML.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetDicInfoXMLResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.meritit.com/ws/IntfsServiceWSXML", name = "getDicInfoXMLResponse")
    public JAXBElement<GetDicInfoXMLResponse> createGetDicInfoXMLResponse(GetDicInfoXMLResponse value) {
        return new JAXBElement<GetDicInfoXMLResponse>(_GetDicInfoXMLResponse_QNAME, GetDicInfoXMLResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ImportDataProCodeAuditXML }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.meritit.com/ws/IntfsServiceWSXML", name = "importDataProCodeAuditXML")
    public JAXBElement<ImportDataProCodeAuditXML> createImportDataProCodeAuditXML(ImportDataProCodeAuditXML value) {
        return new JAXBElement<ImportDataProCodeAuditXML>(_ImportDataProCodeAuditXML_QNAME, ImportDataProCodeAuditXML.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetDicInfoXML }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.meritit.com/ws/IntfsServiceWSXML", name = "getDicInfoXML")
    public JAXBElement<GetDicInfoXML> createGetDicInfoXML(GetDicInfoXML value) {
        return new JAXBElement<GetDicInfoXML>(_GetDicInfoXML_QNAME, GetDicInfoXML.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ImportEffectDataXML }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.meritit.com/ws/IntfsServiceWSXML", name = "importEffectDataXML")
    public JAXBElement<ImportEffectDataXML> createImportEffectDataXML(ImportEffectDataXML value) {
        return new JAXBElement<ImportEffectDataXML>(_ImportEffectDataXML_QNAME, ImportEffectDataXML.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ProduceCodeEditXML }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.meritit.com/ws/IntfsServiceWSXML", name = "produceCodeEditXML")
    public JAXBElement<ProduceCodeEditXML> createProduceCodeEditXML(ProduceCodeEditXML value) {
        return new JAXBElement<ProduceCodeEditXML>(_ProduceCodeEditXML_QNAME, ProduceCodeEditXML.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ProduceCodeAuditXML }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.meritit.com/ws/IntfsServiceWSXML", name = "produceCodeAuditXML")
    public JAXBElement<ProduceCodeAuditXML> createProduceCodeAuditXML(ProduceCodeAuditXML value) {
        return new JAXBElement<ProduceCodeAuditXML>(_ProduceCodeAuditXML_QNAME, ProduceCodeAuditXML.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ImportFlowDataXMLResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.meritit.com/ws/IntfsServiceWSXML", name = "importFlowDataXMLResponse")
    public JAXBElement<ImportFlowDataXMLResponse> createImportFlowDataXMLResponse(ImportFlowDataXMLResponse value) {
        return new JAXBElement<ImportFlowDataXMLResponse>(_ImportFlowDataXMLResponse_QNAME, ImportFlowDataXMLResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetCodeInfoXMLResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.meritit.com/ws/IntfsServiceWSXML", name = "getCodeInfoXMLResponse")
    public JAXBElement<GetCodeInfoXMLResponse> createGetCodeInfoXMLResponse(GetCodeInfoXMLResponse value) {
        return new JAXBElement<GetCodeInfoXMLResponse>(_GetCodeInfoXMLResponse_QNAME, GetCodeInfoXMLResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetModelDatasXMLResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.meritit.com/ws/IntfsServiceWSXML", name = "getModelDatasXMLResponse")
    public JAXBElement<GetModelDatasXMLResponse> createGetModelDatasXMLResponse(GetModelDatasXMLResponse value) {
        return new JAXBElement<GetModelDatasXMLResponse>(_GetModelDatasXMLResponse_QNAME, GetModelDatasXMLResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ProduceCodeAvailXML }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.meritit.com/ws/IntfsServiceWSXML", name = "produceCodeAvailXML")
    public JAXBElement<ProduceCodeAvailXML> createProduceCodeAvailXML(ProduceCodeAvailXML value) {
        return new JAXBElement<ProduceCodeAvailXML>(_ProduceCodeAvailXML_QNAME, ProduceCodeAvailXML.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ImportDataProCodeEditXML }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.meritit.com/ws/IntfsServiceWSXML", name = "importDataProCodeEditXML")
    public JAXBElement<ImportDataProCodeEditXML> createImportDataProCodeEditXML(ImportDataProCodeEditXML value) {
        return new JAXBElement<ImportDataProCodeEditXML>(_ImportDataProCodeEditXML_QNAME, ImportDataProCodeEditXML.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ImportDataProCodeAvailXMLResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.meritit.com/ws/IntfsServiceWSXML", name = "importDataProCodeAvailXMLResponse")
    public JAXBElement<ImportDataProCodeAvailXMLResponse> createImportDataProCodeAvailXMLResponse(ImportDataProCodeAvailXMLResponse value) {
        return new JAXBElement<ImportDataProCodeAvailXMLResponse>(_ImportDataProCodeAvailXMLResponse_QNAME, ImportDataProCodeAvailXMLResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ProduceCodeAvailXMLResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.meritit.com/ws/IntfsServiceWSXML", name = "produceCodeAvailXMLResponse")
    public JAXBElement<ProduceCodeAvailXMLResponse> createProduceCodeAvailXMLResponse(ProduceCodeAvailXMLResponse value) {
        return new JAXBElement<ProduceCodeAvailXMLResponse>(_ProduceCodeAvailXMLResponse_QNAME, ProduceCodeAvailXMLResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetModelDatasXML }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.meritit.com/ws/IntfsServiceWSXML", name = "getModelDatasXML")
    public JAXBElement<GetModelDatasXML> createGetModelDatasXML(GetModelDatasXML value) {
        return new JAXBElement<GetModelDatasXML>(_GetModelDatasXML_QNAME, GetModelDatasXML.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ProduceCodeAuditXMLResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.meritit.com/ws/IntfsServiceWSXML", name = "produceCodeAuditXMLResponse")
    public JAXBElement<ProduceCodeAuditXMLResponse> createProduceCodeAuditXMLResponse(ProduceCodeAuditXMLResponse value) {
        return new JAXBElement<ProduceCodeAuditXMLResponse>(_ProduceCodeAuditXMLResponse_QNAME, ProduceCodeAuditXMLResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ImportFlowDataXML }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.meritit.com/ws/IntfsServiceWSXML", name = "importFlowDataXML")
    public JAXBElement<ImportFlowDataXML> createImportFlowDataXML(ImportFlowDataXML value) {
        return new JAXBElement<ImportFlowDataXML>(_ImportFlowDataXML_QNAME, ImportFlowDataXML.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ProduceCodeEditXMLResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.meritit.com/ws/IntfsServiceWSXML", name = "produceCodeEditXMLResponse")
    public JAXBElement<ProduceCodeEditXMLResponse> createProduceCodeEditXMLResponse(ProduceCodeEditXMLResponse value) {
        return new JAXBElement<ProduceCodeEditXMLResponse>(_ProduceCodeEditXMLResponse_QNAME, ProduceCodeEditXMLResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ImportDataProCodeEditXMLResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.meritit.com/ws/IntfsServiceWSXML", name = "importDataProCodeEditXMLResponse")
    public JAXBElement<ImportDataProCodeEditXMLResponse> createImportDataProCodeEditXMLResponse(ImportDataProCodeEditXMLResponse value) {
        return new JAXBElement<ImportDataProCodeEditXMLResponse>(_ImportDataProCodeEditXMLResponse_QNAME, ImportDataProCodeEditXMLResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ImportDataProCodeAuditXMLResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.meritit.com/ws/IntfsServiceWSXML", name = "importDataProCodeAuditXMLResponse")
    public JAXBElement<ImportDataProCodeAuditXMLResponse> createImportDataProCodeAuditXMLResponse(ImportDataProCodeAuditXMLResponse value) {
        return new JAXBElement<ImportDataProCodeAuditXMLResponse>(_ImportDataProCodeAuditXMLResponse_QNAME, ImportDataProCodeAuditXMLResponse.class, null, value);
    }

}
