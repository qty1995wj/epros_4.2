package com.jecn.epros.server.action.web.login.ad.fenghuo;

import java.net.URLDecoder;

import org.apache.commons.lang.StringUtils;
import org.jasig.cas.client.util.AssertionHolder;
import org.jasig.cas.client.validation.Assertion;

import sun.misc.BASE64Decoder;

import com.jecn.epros.server.action.web.login.LoginAction;
import com.jecn.epros.server.action.web.login.ad.JecnAbstractADLoginAction;
import com.jecn.epros.server.bean.popedom.JecnUser;
import com.jecn.epros.server.common.JecnConfigTool;

public class FengHuoLoginAction extends JecnAbstractADLoginAction {
	/** 邮箱后缀 */
	private static String MAIL_SUFFIX = null;
	private static String USER_CODE = "userCode";
	private static String IS_APP = "isApp";

	public FengHuoLoginAction(LoginAction loginAction) {
		super(loginAction);
	}

	static {
		String email = JecnConfigTool.getEmailInnerAdd();
		MAIL_SUFFIX = email.substring(email.indexOf("@"), email.length());
	}

	@Override
	public String login() {
		Assertion assertion = AssertionHolder.getAssertion();
		String loginName = "";
		if (assertion != null) {
			loginName = assertion.getPrincipal().getName();
		}
		String queryString = loginAction.getRequest().getQueryString();
		log.info("queryString = " + queryString);
		if (StringUtils.isNotBlank(queryString) && queryString.contains(USER_CODE) && queryString.contains(IS_APP)) {
			loginName = getAppLoginName(queryString);
		}
		log.info("loginName + MAIL_SUFFIX = " + loginName + MAIL_SUFFIX);
		// 根据邮箱获取用户信息
		JecnUser jecnUser = null;
		try {
			jecnUser = loginAction.getPersonService().getJecnUserByEmail(loginName.toLowerCase().trim() + MAIL_SUFFIX);
		} catch (Exception e) {
			e.printStackTrace();
			log.error("获取用户失败！user.name = " + loginName, e);
		}
		log.info("jecnUser = " + jecnUser);
		if (jecnUser == null) {
			return LoginAction.INPUT;
		}
		return loginByLoginName(jecnUser.getLoginName());
	}

	private String getAppLoginName(String queryString) {
		String[] str = queryString.split("&");

		String userCode = getUserCodeByQueryString(str);
		log.info("解密前userCode = " + userCode);
		if (StringUtils.isNotBlank(userCode)) {
			try {
				String decoder = getLoginNameByBase64(userCode);
				log.info("解密后userCode = " + decoder);
				return decoder.trim();
			} catch (Exception e) {
				e.printStackTrace();
				log.error("获取userCode异常！ userCode = " + userCode);
			}
		}
		return null;
	}

	private String getUserCodeByQueryString(String[] str) {
		String userCode = null;
		for (String string : str) {
			if (IS_APP.equals(string.substring(0, IS_APP.length()))) {
				loginAction.setIsApp("true".equals(string.substring(IS_APP.length() + 1, string.length())));
			}
			if (USER_CODE.equals(string.substring(0, USER_CODE.length()))) {
				userCode = string.substring(USER_CODE.length() + 1, string.length());
			}
		}
		return userCode;
	}

	private String getLoginNameByBase64(String userCode) throws Exception {
		String urlDecode = URLDecoder.decode(userCode, "UTF-8");
		BASE64Decoder base64Decoder = new BASE64Decoder();
		return new String(base64Decoder.decodeBuffer(urlDecode), "UTF-8");
	}
	
//	public static void main(String[] args) {
//		try {
//			String urlDecode = URLEncoder.encode("admin", "UTF-8");
//			BASE64Encoder base64Encoder = new BASE64Encoder();
//			System.out.println(base64Encoder.encode(urlDecode.getBytes("UTF-8")));
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//		
//	}

}
