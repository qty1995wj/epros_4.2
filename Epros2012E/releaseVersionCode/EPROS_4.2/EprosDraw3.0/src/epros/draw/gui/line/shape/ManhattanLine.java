/*
 * ArrowLine.java
 *曼哈顿线  带箭头:
 * Created on 2008年11月12日, 下午4:09
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */
package epros.draw.gui.line.shape;

import java.awt.Point;

import epros.draw.constant.JecnLogConstants;
import epros.draw.constant.JecnWorkflowConstant.EditPointType;
import epros.draw.data.line.JecnManhattanLineData;
import epros.draw.gui.line.JecnBaseManhattanLinePanel;
import epros.draw.gui.workflow.JecnDrawMainPanel;
import epros.draw.gui.workflow.tabbedPane.JecnDrawDesktopPane;
import epros.draw.util.DrawCommon;

/**
 * 连接线 带箭头
 * 
 * @author ZHOUXY
 */
public class ManhattanLine extends JecnBaseManhattanLinePanel {
	/** 带箭头连接线的箭头 */
	public ArrowPanel arrowPanel = null;
	/** 开始点箭头 */
	public ArrowPanel startArrow = null;

	public ManhattanLine(JecnManhattanLineData flowElementData) {
		super(flowElementData);
		initComponents();
	}

	private void initComponents() {

	}

	protected void addArrowPanel() {
		// 带箭头连接线的箭头
		arrowPanel = new ArrowPanel(this.getFlowElementData().getBodyColor(), this.getFlowElementData()
				.getEndEditPointType());
		JecnDrawDesktopPane desktopPane = JecnDrawMainPanel.getMainPanel().getWorkflow();
		// 添加箭头到画板
		addTwoArrowLine();
		// 设置箭头层级
		// 添加箭头到面板
		reDrawArrow();
		// 添加箭头到画板
		desktopPane.add(arrowPanel);
		desktopPane.setLayer(arrowPanel, this.getFlowElementData().getZOrderIndex());
	}

	/**
	 * 添加箭头到画板
	 */
	private void addTwoArrowLine() {
		if (this.getFlowElementData().isTwoArrow()) {
			addStartTwoArrowPanel();
		} else {// 删除开始箭头
			deleteStartArrowPanel();
		}
	}

	private void addStartTwoArrowPanel() {
		JecnDrawDesktopPane desktopPane = JecnDrawMainPanel.getMainPanel().getWorkflow();
		if (startArrow != null) {
			return;
		}
		startArrow = new ArrowPanel(this.getFlowElementData().getBodyColor(), this.getFlowElementData()
				.getStartEditPointType());
		// 添加箭头到画板
		desktopPane.add(startArrow);
		desktopPane.setLayer(startArrow, this.getFlowElementData().getZOrderIndex());
	}

	public void deleteArrowPanel() {
		if (arrowPanel != null) {
			JecnDrawMainPanel.getMainPanel().getWorkflow().remove(arrowPanel);
			arrowPanel = null;
		}
		deleteStartArrowPanel();
	}

	private void deleteStartArrowPanel() {
		if (startArrow != null) {
			JecnDrawMainPanel.getMainPanel().getWorkflow().remove(startArrow);
			startArrow = null;
		}
	}

	/**
	 * 
	 * 重画箭头
	 * 
	 */
	public void reDrawArrow() {
		if (lineSegments == null || lineSegments.size() == 0) {
			JecnLogConstants.LOG_BASE_FLOW_ELEMENT_PANEL.info("ManhattanLine类reDrawArrow方法：lineSegments中没有元素即没有小线段");
			return;
		}
		drawEndArrow(this.getFlowElementData().getEndEditPointType(), this.getFlowElementData().getZoomEndPoint(),
				arrowPanel);

		// 是否添加双箭头
		addTwoArrowLine();
		if (startArrow == null) {
			return;
		}
		drawEndArrow(this.getFlowElementData().getStartEditPointType(), this.getFlowElementData().getZoomStartPoint(),
				startArrow);
	}

	private void drawEndArrow(EditPointType editPointType, Point point, ArrowPanel arrowPanel) {
		// 获取面板当前放大缩小倍数
		double scale = JecnDrawMainPanel.getMainPanel().getCurrWorkflowScale();
		// 结束点当前大小X坐标
		double endPointX = point.getX();
		// 结束点当前大小Y坐标
		double endPointY = point.getY();
		double lineWidth = 1;
		if (this.getFlowElementData().getBodyWidth() != 1) {
			lineWidth = this.getFlowElementData().getBodyWidth() / 2.0;
		}

		// 获取当前面板下箭头对应的高度和宽度
		int arrowCurW = DrawCommon.convertDoubleToInt(arrowW * scale * lineWidth);
		int arrowCurH = DrawCommon.convertDoubleToInt(arrowH * scale * lineWidth);

		// 循环输出编辑点端口 获取箭头位置
		switch (editPointType) {
		case left:
			// 设置箭头位置
			arrowPanel.setBounds(DrawCommon.convertDoubleToInt(endPointX - arrowCurW), DrawCommon
					.convertDoubleToInt(endPointY - arrowCurH / 2), arrowCurW, arrowCurH);
			break;
		case top:
			// 设置箭头位置
			arrowPanel.setBounds(DrawCommon.convertDoubleToInt(endPointX - arrowCurH / 2), DrawCommon
					.convertDoubleToInt(endPointY - arrowCurW), arrowCurH, arrowCurW);
			break;
		case right:
			// 设置箭头位置
			arrowPanel.setBounds(DrawCommon.convertDoubleToInt(endPointX), DrawCommon.convertDoubleToInt(endPointY
					- arrowCurH / 2), arrowCurW, arrowCurH);
			break;
		case bottom:
			// 设置箭头位置
			arrowPanel.setBounds(DrawCommon.convertDoubleToInt(endPointX - arrowCurH / 2), DrawCommon
					.convertDoubleToInt(endPointY), arrowCurH, arrowCurW);
			break;
		}
	}

	/**
	 * 
	 * 把源图形属性赋值给目标图形属性
	 * 
	 * @param srcLinePanel
	 *            源属性
	 * @param destlinePanel
	 *            目标属性
	 */
	public void setFigureAttributes(JecnBaseManhattanLinePanel srcLinePanel, JecnBaseManhattanLinePanel destlinePanel) {
		if (srcLinePanel == null || destlinePanel == null) {
			return;
		}
		super.setFigureAttributes(srcLinePanel, destlinePanel);
		ManhattanLine destLine = (ManhattanLine) destlinePanel;

		destLine.arrowPanel = new ArrowPanel(destLine.getFlowElementData().getBodyColor(), destLine
				.getFlowElementData().getEndEditPointType());
		destLine.startArrow = new ArrowPanel(destLine.getFlowElementData().getBodyColor(), destLine
				.getFlowElementData().getStartEditPointType());
	}

	public void setArrowColor() {
		if (arrowPanel != null) {
			arrowPanel.setColor(this.getFlowElementData().getBodyColor());
		}
		if (startArrow != null) {
			startArrow.setColor(this.getFlowElementData().getBodyColor());
		}
	}
}
