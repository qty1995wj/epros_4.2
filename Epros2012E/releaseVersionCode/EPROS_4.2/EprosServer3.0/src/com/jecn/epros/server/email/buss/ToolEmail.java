package com.jecn.epros.server.email.buss;

import java.net.URISyntaxException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Date;

import javax.mail.Address;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.jecn.epros.server.action.web.login.LoginAction.MailLoginEnum;
import com.jecn.epros.server.bean.task.JecnTaskBeanNew;
import com.jecn.epros.server.bean.task.JecnTaskHistoryNew;
import com.jecn.epros.server.common.JecnCommon;
import com.jecn.epros.server.common.JecnConfigTool;
import com.jecn.epros.server.common.JecnContants;
import com.jecn.epros.server.service.task.TestRunCommon;
import com.jecn.epros.server.service.task.buss.JecnTaskCommon;
import com.jecn.epros.server.util.JecnUtil;

/**
 * 邮件数据处理
 * 
 * @author Administrator
 * 
 */
public class ToolEmail {
	public static final Log log = LogFactory.getLog(ToolEmail.class);
	public static String TASK_DESC = "任务说明：";
	public static String IS_FINISH_PUB = "已完成并发布, 请查阅！";
	public static String TASK_CREATE_PEOPLE = "任务创建人：";
	public static String TASK_CREATE_DATE = "任务创建时间：";
	public static String TASK_APPROVE = "已提交，请审批！";
	public static String TASK_NO_APPROVE = "已打回，请重新提交！";
	public static String TASK_TITLE = "任务：";
	public static String task_stage = "任务进度：";
	public static String IS_FINISH = "已完成！";
	/** 试运行已发布，请填写试运行报告！ */
	public static String TEST_RUN_IS_PUFLIC = "试运行已发布，请填写试运行报告！";
	/** 任务试运行已到期！ */
	public static String TEST_RUN_IS_EXPIRED = "任务试运行已到期！";
	/** *试运行已结束！ */
	public static String TEST_RUN_IS_END = "试运行已结束！";

	/** 请填写试运行报告！ */
	public static String PLEASE_FILL_REPORT = "请填写试运行报告！";

	/** 试运行文件路径 */
	public static String testRunFilePath = null;
	/** 试运行文件名称 */
	public static String TASK_RUN_FILE = "流程试运行总结表.xls";
	public static String TASK_ON_APPROVE = "正在审批！";

	/**
	 * 多邮件处理
	 * 
	 * @param listStr
	 * @return
	 * @throws AddressException
	 */
	public static Address[] getArrayAddress(ArrayList<String> listStr) throws AddressException {
		Address[] adds = new InternetAddress[listStr.size()];
		for (int i = 0; i < listStr.size(); i++) {
			adds[i] = new InternetAddress(listStr.get(i));
		}
		return adds;
	}

	/**
	 * 发送邮件拼装邮件信息
	 * 
	 * @param taskId
	 *            任务主键ID
	 * 
	 * @param toPeopleId
	 *            打开邮件操作人ID
	 * @param content
	 *            任务拼装的邮件信息
	 * 
	 * @param subject
	 *            邮件标题
	 * @param type
	 *            0:任务审批；1：试运行
	 * @return SimpleUserEmailBean邮件信息
	 */
	public static SimpleUserEmailBean getSimpleUserEmailBean(Long taskId, Long toPeopleId, String content,
			String subject, int type) {
		// 获取任务审批 指定下一个操作人的连接地址
		String httpUrl = null;
		if (type == 0) {
			httpUrl = getTaskHttpUrl(taskId, toPeopleId);
		} else {
			httpUrl = getTestRunHttpUrl();
		}
		// 每个流程参与者都要接收邮件，点击邮件连接地址直接查看流程
		SimpleUserEmailBean simpleUserEmailBean = new SimpleUserEmailBean();

		if (subject != null) {
			// 设置邮件标题
			simpleUserEmailBean.setEmailSubject(subject);
		}
		if (content != null) {
			// 设置邮件正文
			simpleUserEmailBean.setTaskContent(content);
		}
		simpleUserEmailBean.setHttpUrl(httpUrl);
		return simpleUserEmailBean;
	}

	/**
	 * 获取邮箱信息
	 * 
	 * @param emailType
	 *            邮箱内外网标识
	 * @param email
	 *            邮箱地址
	 * @param simpleUserEmailBean
	 */
	public static void setEmailInfo(String emailType, String email, SimpleUserEmailBean simpleUserEmailBean) {
		UserEmailBean userEmailBean = new UserEmailBean();
		if ("inner".equals(emailType)) {// 内网邮箱
			userEmailBean.setEmailAdderss(email);
			userEmailBean.setEmailType(emailType);
			// 邮件接收者
			userEmailBean.setRecipient("0");
			simpleUserEmailBean.getListInnerEmail().add(userEmailBean);
		} else {
			// 邮件地址
			userEmailBean.setEmailAdderss(email);
			// 邮件类型
			userEmailBean.setEmailType(emailType);
			// 邮件接收者
			userEmailBean.setRecipient("0");
			simpleUserEmailBean.getListOutterEmail().add(userEmailBean);
		}
	}

	/**
	 * 获取邮件地址
	 * 
	 * @param objects
	 *            Object[] // 0:内外网；1：邮件地址;2:人员主键ID
	 * @param simpleUserEmailBean
	 *            邮件信息对象
	 */
	public static void getEmailByUser(Object[] objects, SimpleUserEmailBean simpleUserEmailBean) {
		setEmailInfo(objects[0].toString(), objects[1].toString(), simpleUserEmailBean);
	}

	/**
	 * 获取任务正文
	 * 
	 * @param state
	 * @return
	 */
	public static String getTaskDesc(JecnTaskBeanNew taskBeanNew, TaskEmailParams taskParams) {
		boolean isLibang = JecnConfigTool.isLiBangLoginType();
		String str = "";
		if (isLibang && taskBeanNew.getTaskType() == 0) {
			str = liBangTaskDesc(taskBeanNew, taskParams);
		} else {
			str = getTaskDesc(taskBeanNew);
		}
		return str;
	}

	private static String liBangTaskDesc(JecnTaskBeanNew taskBeanNew, TaskEmailParams taskParams) {
		String taskDesc = "";
		if (taskBeanNew.getTaskDesc() != null && !"".equals(taskBeanNew.getTaskDesc())) {
			taskDesc = taskBeanNew.getTaskDesc().replaceAll("\n", "<br>");
		}
		// 更新时间
		String updateTime = JecnCommon.getStringbyDate(taskBeanNew.getUpdateTime());
		// 文件名称
		String rname = taskBeanNew.getRname();
		String str = JecnUtil.getValue("fileNameC") + rname + "<br>" + JecnUtil.getValue("updateTimeC") + updateTime
				+ "<br>" + "发布附言栏：" + "<br>" + "&nbsp;&nbsp;1.发布部门："
				+ nTobr(taskParams.getProcessAttribute().getDutyOrgName()) + "<br>" + "&nbsp;&nbsp;2.目的："
				+ nTobr(taskParams.getFlowPurpose()) + "<br>" + "&nbsp;&nbsp;3.范围："
				+ nTobr(taskParams.getApplicability()) + "<br>" + "&nbsp;&nbsp;4.变更说明：" + taskDesc;
		return str;
	}

	/**
	 * 获取任务正文
	 * 
	 * @param state
	 * @return
	 */
	public static String getTaskDescNeedApprove(JecnTaskBeanNew taskBeanNew, TaskEmailParams taskParams) {
		boolean isLibang = JecnConfigTool.isLiBangLoginType();
		String str = "";
		if (isLibang && taskBeanNew.getTaskType() == 0) {
			str = liBangTaskDesc(taskBeanNew, taskParams);
		} else {
			str = getTaskDescNeedApprove(taskBeanNew);
		}
		return str;
	}

	private static String getTaskDescNeedApprove(JecnTaskBeanNew taskBeanNew) {
		String platName = JecnConfigTool.getItemValue("mailPlatName");
		String tip = "您好！<br>" + "您有1条新的审批待办项，请通过" + platName + "中的我的主页-待办任务项进行审批，为不影响流程工作，请您及时处理。<br>";
		return getTaskDesc(taskBeanNew, tip);
	}

	public static String getTaskDesc(JecnTaskBeanNew taskBeanNew) {
		return getTaskDesc(taskBeanNew, "");
	}

	/**
	 * 获取任务正文
	 * 
	 * @param state
	 * @return
	 */
	private static String getTaskDesc(JecnTaskBeanNew taskBeanNew, String tip) {
		String taskDesc = "";
		if (taskBeanNew.getTaskDesc() != null && !"".equals(taskBeanNew.getTaskDesc())) {
			taskDesc = taskBeanNew.getTaskDesc().replaceAll("\n", "<br>");
		}

		// 更新时间
		String updateTime = JecnCommon.getStringbyDate(taskBeanNew.getUpdateTime());

		// 文件名称
		String rname = taskBeanNew.getRname();

		String str = "";
		if (JecnConfigTool.isLiBangLoginType()) {
			str = JecnUtil.getValue("fileNameC") + rname + "<br>" + JecnUtil.getValue("updateTimeC") + updateTime
					+ "<br>" + "发布附言栏：" + "<br>" + taskDesc;
		} else {
			// 如果任务时结束状态
			if (taskBeanNew.getState() == 5) {
				taskBeanNew.setStateMark(JecnTaskCommon.FINISH);
			}
			// 任务状态
			String stateStr = task_stage + taskBeanNew.getStateMark();
			String createName = "";
			if (taskBeanNew.getCreatePersonTemporaryName() != null
					&& !"".equals(taskBeanNew.getCreatePersonTemporaryName())) {
				createName = TASK_CREATE_PEOPLE + taskBeanNew.getCreatePersonTemporaryName() + "<br>";
			}

			str = tip + JecnUtil.getValue("fileNameC") + rname + "<br>" + JecnUtil.getValue("updateTimeC") + updateTime
					+ "<br>" + JecnUtil.getValue("changeThatC") + taskDesc + "<br>" + createName + TASK_CREATE_DATE
					+ JecnCommon.getStringbyDate(taskBeanNew.getCreateTime()) + "<br>" + stateStr;
		}
		return str;
	}

	/**
	 * 获取任务审批 指定下一个操作人的连接地址
	 * 
	 * @param taskId
	 *            任务主键ID
	 * @param peopleId
	 *            下一个操作人人员主键ID（目标人）
	 * @return String指定下一个操作人的连接地址
	 */
	public static String getTaskHttpUrl(Long taskId, Long peopleId) {
		// 拼装邮件连接地址
		return "/loginMail.action?mailTaskId=" + taskId + "&mailPeopleId=" + peopleId + "&accessType="
				+ MailLoginEnum.mailApprove.toString();
	}

	/**
	 * 获取任务试运行连接地址
	 * 
	 * @return String指定下一个操作人的连接地址
	 */
	public static String getTestRunHttpUrl() {
		// 拼装邮件连接地址
		return "/login.jsp";
	}

	/**
	 * 获取发布流程的邮件地址
	 * 
	 * @param flowId
	 * @param peopleId
	 *            邮件接收者人员ID
	 * @return
	 */
	public static String getHttpUrl(Long flowId, String prfType, Long peopleId) {
		// 拼装邮件连接地址process.action?type=process&flowId=362
		String ipAddressUrl = "/loginMail.action?mailFlowType=" + prfType + "&mailFlowId=" + flowId + "&mailPeopleId="
				+ peopleId + "&accessType=" + MailLoginEnum.mailPubApprove.toString();
		return ipAddressUrl;
	}

	/**
	 * 
	 * 获取sync目录路径(../WebRoot/WEB-INF/classes/com/jecn/epros/server/service/task/
	 * doc/)
	 * 
	 * @return
	 */
	public static String getSyncPath() {
		URL url = TestRunCommon.class.getResource("");
		String path = "";
		try {
			path = url.toURI().getPath();
		} catch (URISyntaxException e) {
			// 这儿一般是不进入此分支的
			LogFactory.getLog(TestRunCommon.class).error("Tool类getSyncPath方法：", e);
			path = url.getPath().replaceAll("%20", " ");
		}
		if (JecnCommon.isNullOrEmtryTrim(path)) {
			return null;
		}
		path = path + "doc/";
		// 文件路径 +名称
		return path + TASK_RUN_FILE;
	}

	/**
	 * 根据任务操作获取对应的邮件标题
	 * 
	 * @param jecnTaskBeanNew
	 * @return
	 */
	public static String getTaskSubject(JecnTaskBeanNew jecnTaskBeanNew) {
		String subject = "";
		if (jecnTaskBeanNew.getTaskElseState() == 4) {// 打回
			subject = ToolEmail.TASK_TITLE + '"' + jecnTaskBeanNew.getTaskName() + '"' + ToolEmail.TASK_NO_APPROVE;
		} else if (jecnTaskBeanNew.getState() == 5)// 任务完成
		{
			subject = ToolEmail.TASK_TITLE + '"' + jecnTaskBeanNew.getTaskName() + '"' + ToolEmail.IS_FINISH;

		} else {
			subject = ToolEmail.TASK_TITLE + '"' + jecnTaskBeanNew.getTaskName() + '"' + ToolEmail.TASK_APPROVE;
		}

		return subject;
	}

	/**
	 * 根据任务操作获取对应的邮件标题(给拟稿人发送的邮件)
	 * 
	 * @param jecnTaskBeanNew
	 * @param toPeopleType
	 *            0、为审批人员 1、为拟稿人
	 * @return
	 */
	public static String getTaskSubjectForPeopleMake(JecnTaskBeanNew jecnTaskBeanNew) {
		String subject = "";
		if (jecnTaskBeanNew.getState() == 5)// 任务完成
		{
			subject = ToolEmail.TASK_TITLE + '"' + jecnTaskBeanNew.getTaskName() + '"' + "," + ToolEmail.IS_FINISH;

		} else {
			subject = ToolEmail.TASK_TITLE + '"' + jecnTaskBeanNew.getTaskName() + '"' + ","
					+ ToolEmail.TASK_ON_APPROVE;
		}

		return subject;

	}

	/**
	 * 获取任务审批超时提醒的邮件基本信息
	 * 
	 * @author weidp
	 * @date： 日期：2014-5-16 时间：上午11:26:00
	 * @param user
	 * @return
	 */
	protected static SimpleUserEmailBean getTaskTipSimpleUserEmailBean(Long taskId, Long toPeopleId) {
		// 获取登录请求地址（/login.jsp）
		String httpUrl = getTaskHttpUrl(taskId, toPeopleId);
		String subject = JecnUtil.getValue("taskApproveTipEmailSubject");
		String content = JecnUtil.getValue("taskApproveTipEmailContent");
		// 每个流程参与者都要接收邮件，点击邮件连接地址直接查看流程
		SimpleUserEmailBean simpleUserEmailBean = new SimpleUserEmailBean();
		// 设置邮件标题
		simpleUserEmailBean.setEmailSubject(subject);
		// 设置邮件正文
		simpleUserEmailBean.setTaskContent(content);
		simpleUserEmailBean.setHttpUrl(httpUrl);
		return simpleUserEmailBean;
	}

	/**
	 * 获取搁置任务提醒的邮件基本信息
	 * 
	 * @author huoyl
	 * @param user
	 * @return
	 */
	protected static SimpleUserEmailBean getTaskDelayAdminUserEmailBean(Long toPeopleId) {
		// TODO
		// 管理员点击页面的链接直接打开任务管理页面
		// loginMail.action?accessType=mailOpenTaskManagement&mailPeopleId=
		String httpUrl = "/loginMail.action?accessType=mailOpenTaskManagement&mailPeopleId=" + toPeopleId;
		String subject = JecnUtil.getValue("delayTaskEmailTitle");
		String content = JecnUtil.getValue("delayTaskEmailContent");
		SimpleUserEmailBean simpleUserEmailBean = new SimpleUserEmailBean();
		// 设置邮件标题
		simpleUserEmailBean.setEmailSubject(subject);
		// 设置邮件正文
		simpleUserEmailBean.setTaskContent(content);
		simpleUserEmailBean.setHttpUrl(httpUrl);
		return simpleUserEmailBean;
	}

	/**
	 * 获得邮件bean
	 * 
	 * @author huoyl
	 * @param historyNew
	 *            文控信息
	 * @param subject
	 *            标题
	 * @param objects
	 *            Object[] 0内外网 1邮件地址 2收件人id
	 * @return
	 */
	public static SimpleUserEmailBean getPubRecordPublicSimpleUserEmailBean(JecnTaskHistoryNew historyNew,
			String subject, Object[] objects) {

		/**
		 * /**rId
		 * type=0时，表示流程ID，type=1时，表示文件ID，type=2时，表示制度模板ID,3:制度文件ID，4：流程地图ID
		 */
		int subType = historyNew.getType();
		// 关联id
		String rId = String.valueOf(historyNew.getRelateId());
		String peopleId = objects[2].toString();
		String httpUrl = getUrlBySubFileType(subType, rId, peopleId);
		SimpleUserEmailBean simpleUserEmailBean = new SimpleUserEmailBean();

		// 设置查阅链接
		simpleUserEmailBean.setHttpUrl(httpUrl);
		// 设置标题
		simpleUserEmailBean.setEmailSubject(subject);
		// 设置内容
		simpleUserEmailBean.setPubContent(getPubRecordContent(historyNew));

		return simpleUserEmailBean;
	}

	public static String getUrlBySubFileType(int subType, String rId, String peopleId) {
		String httpUrl = "/login.jsp";
		// 如果发布流程
		if (subType == 0) {
			httpUrl = getHttpUrl(rId, "process", peopleId);
		} else if (subType == 2)// 如果发布制度模板
		{
			httpUrl = getHttpUrl(rId, "ruleModeFile", peopleId);
		} else if (subType == 3)// 制度文件
		{
			httpUrl = getHttpUrl(rId, "ruleFile", peopleId);
		} else if (subType == 4)// 流程地图
		{
			httpUrl = getHttpUrl(rId, "processMap", peopleId);
		} else if (subType == 1)// 文件
		{
			httpUrl = getHttpUrl(rId, "infoFile", peopleId);
		}
		return httpUrl;
	}

	/**
	 * @author huoyl 直接发布（记录文控）获得邮件正文内容
	 * @param historyNew
	 *            JecnTaskHistoryNew 文控信息
	 * @return String 邮件正文内容
	 */
	private static String getPubRecordContent(JecnTaskHistoryNew historyNew) {

		String desc = "";
		if (historyNew.getModifyExplain() != null && !"".equals(historyNew.getModifyExplain().trim())) {
			desc = historyNew.getModifyExplain().replaceAll("\n", "<br>");
		}
		// 更新时间
		String updateTime = JecnCommon.getStringbyDate(new Date());

		// 文件名称
		String rname = historyNew.getRelateName();

		String str = JecnUtil.getValue("fileNameC") + rname + "<br>" + JecnUtil.getValue("updateTimeC") + updateTime
				+ "<br>" + JecnUtil.getValue("changeThatC") + desc + "<br>" + JecnUtil.getValue("peopleMakeADraftC")
				+ historyNew.getDraftPerson();
		return str;
	}

	/**
	 * 直接发布拼装链接到详细信息界面
	 * 
	 * @param id
	 *            流程或者流程地图或者制度或者文件的id
	 * @param prfType
	 *            文件类型
	 * @param peopleId
	 *            人员id
	 * @return String 直接打开文件信息链接地址
	 */
	public static String getHttpUrl(String id, String prfType, String peopleId) {

		String ipAddressUrl = "/login.jsp";
		// 拼装邮件连接地址process.action?type=process&flowId=362
		if (prfType.equals("process")) {
			ipAddressUrl = "/loginMail.action?mailFlowType=" + prfType + "&mailFlowId=" + id + "&mailPeopleId="
					+ peopleId + "&accessType=" + MailLoginEnum.mailPubApprove.toString() + "&isPub=true";
		} else if (prfType.equals("processMap")) {
			ipAddressUrl = "/loginMail.action?mailFlowType=" + prfType + "&mailFlowId=" + id + "&mailPeopleId="
					+ peopleId + "&accessType=" + MailLoginEnum.mailPubApprove.toString() + "&isPub=true";
		} else if (prfType.equals("ruleFile"))// 制度文件
		{
			ipAddressUrl = "/loginMail.action?mailRuleType=" + prfType + "&mailRuleId=" + id + "&mailPeopleId="
					+ peopleId + "&accessType=" + MailLoginEnum.mailOpenRule.toString() + "&isPub=true";
		} else if (prfType.equals("ruleModeFile"))// 制度模板文件
		{
			ipAddressUrl = "/loginMail.action?mailRuleType=" + prfType + "&mailRuleId=" + id + "&mailPeopleId="
					+ peopleId + "&accessType=" + MailLoginEnum.mailOpenRule.toString() + "&isPub=true";
		} else if (prfType.equals("infoFile"))// 文件任务
		{
			ipAddressUrl = "/loginMail.action?mailFileId=" + id + "&mailPeopleId=" + peopleId + "&accessType="
					+ MailLoginEnum.mailOpenFile.toString() + "&isPub=true";
		}

		return ipAddressUrl;
	}

	public static String nTobr(String text) {
		if (StringUtils.isBlank(text)) {
			return "";
		} else {
			return text.replace("\n", "<br>");
		}
	}

}
