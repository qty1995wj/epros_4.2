package epros.designer.gui.process.activitiesProperty;

import javax.swing.event.CaretEvent;

import epros.designer.util.JecnProperties;
import epros.draw.designer.JecnRefIndicatorsT;
import epros.draw.gui.operationConfig.OperationConfigUtil;
import epros.draw.gui.top.dialog.JecnDialog;
import epros.draw.util.DrawCommon;

/*******************************************************************************
 * 编辑指标
 * 
 * @author 2012-08-17
 * 
 */
public class EditActiveTargetDialog extends ActiveTargetDialog {

	public EditActiveTargetDialog(JecnDialog jecnDialog) {
		super(jecnDialog);
	}

	@Override
	public String getTargetTitle() {
		return JecnProperties.getValue("editActiveTarget");
	}

	@Override
	public void caretUpdate(CaretEvent e) {
		// 活动KPI
		isActiveKPI = OperationConfigUtil.checkInOutContent(activeKPIField.getText(), activeTargetVerfLab);
		if (isActiveKPI) {
			// 目标值
			isActiveKPIValue = OperationConfigUtil
					.checkInOutContent(activeKPIValueField.getText(), activeTargetVerfLab);
		}
	}

	/***************************************************************************
	 * 确定
	 */
	@Override
	public void okButPerformed() {
		if (isActiveKPI && isActiveKPIValue) {
			// 验证活动KPI不能为空
			if (DrawCommon.isNullOrEmtryTrim(this.activeKPIField.getText())) {
				this.activeTargetVerfLab.setText(JecnProperties.getValue("activeKPINotnull"));
				return;
			} else if (DrawCommon.checkNameMaxLength(this.activeKPIField.getText())) {
				// 名称不能超过122个字符或61个汉字
				this.activeTargetVerfLab.setText(JecnProperties.getValue("activeKPIC").substring(0,
						JecnProperties.getValue("activeKPIC").length() - 1)
						+ JecnProperties.getValue("lengthNotBaiOut"));
				return;
			}

			// 验证目标值不能为空
			if (DrawCommon.isNullOrEmtryTrim(this.activeKPIValueField.getText())) {
				this.activeTargetVerfLab.setText(JecnProperties.getValue("activeKPIValueNotnull"));
				return;
			} else if (DrawCommon.checkNameMaxLength(this.activeKPIValueField.getText())) {
				// 名称不能超过122个字符或61个汉字
				this.activeTargetVerfLab.setText(JecnProperties.getValue("activeKPIValuC").substring(0,
						JecnProperties.getValue("activeKPIValuC").length() - 1)
						+ JecnProperties.getValue("lengthNotBaiOut"));
				return;
			}
			this.activeTargetVerfLab.setText("");
			jecnRefIndicatorsT = new JecnRefIndicatorsT();
			jecnRefIndicatorsT.setIndicatorName(activeKPIField.getText());
			jecnRefIndicatorsT.setIndicatorValue(activeKPIValueField.getText());
			isOperation = true;
			this.dispose();
		}
	}
}
