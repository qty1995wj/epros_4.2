package epros.designer.autoUpdate;

import java.awt.Component;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.Properties;

import javax.swing.JOptionPane;

import jecntool.cmd.JecnCmd;
import jecntool.cmd.ResultBean;

import org.apache.log4j.Logger;

import com.jecn.epros.server.action.designer.update.JecnUpdateFileAction;
import com.jecn.epros.server.bean.system.JecnVersionSys;

import epros.designer.util.ConnectionPool;
import epros.designer.util.JecnLoginProperties;
import epros.designer.util.UnzipAFile;
import epros.draw.gui.top.optionpane.JecnOptionPane;
import epros.draw.gui.workflow.aid.wait.JecnLoading;
import epros.draw.util.DrawCommon;

/**
 * 版本自动更新类
 * 
 * @author yxw 2013-4-9
 * @description：
 */
public class VersionValidate {
	private static Logger log = Logger.getLogger(VersionValidate.class);

	/**
	 * @author yxw 2013-4-9
	 * @description:更新方法
	 * @param c
	 */
	public static boolean validate(Component c) {
		Properties props = new Properties();
		FileInputStream fileInput = null;
		String sysInfoPath = null;
		String dir = null;
		try {
			sysInfoPath = VersionValidate.class.getResource("/").toURI()
					.getPath()
					+ "Sysinfo.properties";
		} catch (URISyntaxException e) {
			log.error("", e);
			return false;
		}
		try {
			fileInput = new FileInputStream(sysInfoPath);
		} catch (FileNotFoundException e) {
			log.error("validate is error", e);
			dir = getUpateDir();
			if (dir != null) {
				return exeUpdate(dir);
			} else {
				return false;
			}

			
		}
		try {
			props.load(fileInput);
		} catch (IOException e) {
			log.error("ysinfo.properties is error", e);
			return false;
		} finally {
			if (fileInput != null) {
				try {
					fileInput.close();
				} catch (IOException e) {
					log.error("sysinfo is error", e);
				}
			}
		}
		JecnVersionSys versionSys = null;
		try {
			versionSys = ConnectionPool.getUpdateFileAction().readProperties();
		} catch (Exception e) {
			log.error("validate is error", e);
			return false;
		}
		if (versionSys != null) {
			// 设计器更新版本序号
			String updateNumDesignerString = props.getProperty("updateNum");
			int updateNumDesignerInt = 0;
			try {
				updateNumDesignerInt = Integer.parseInt(props
						.getProperty("updateNum"));
			} catch (Exception e) {
				dir = getUpateDir();
				return exeUpdate(dir);
			}

			// 服务器更新版本序号
			String updateNumServerString = versionSys.getUpdateNum();
			if (!DrawCommon.isNullOrEmtryTrim(updateNumDesignerString)) {
				int updateNumServerInt = Integer
						.parseInt(updateNumServerString);
				if (updateNumServerInt != updateNumDesignerInt) {
					// 确认提示框 版本有更新,是否更新!
					int option = JecnOptionPane.showConfirmDialog(c,
							JecnLoginProperties.getValue("loadingVersionHaveNew"),
							null, JecnOptionPane.YES_NO_OPTION,
							JecnOptionPane.INFORMATION_MESSAGE);
					if (option == JecnOptionPane.NO_OPTION) {
						System.exit(0);
					}
					dir = getUpateDir();
					// 正在下载更新文件...
					JecnLoading.setShowText(JecnLoginProperties
							.getValue("loadingDownLoadUpdate"));
					if (zipUpdate(dir, c)) {
						updateSysInfoProperties(sysInfoPath, props, versionSys);
						System.exit(0);
					} else {
						return false;
					}
				}
			}
		}
		return true;
	}

	/**
	 * @author yxw 2013-4-9
	 * @description:
	 * @return
	 */
	public static String getUpateDir() {
		String dir = null;
		try {
			dir = VersionValidate.class.getResource("/").toURI().getPath()
					+ "update/updatefile/";
		} catch (URISyntaxException e) {
			log.error("getUpateDir is error", e);
			return null;
		}
		dir.replaceAll("%20", " ");
		File fileDir = new File(dir);
		if (!fileDir.exists()) {
			fileDir.mkdirs();
		}
		return dir;
	}

	/**
	 * @author yxw 2013-4-9
	 * @description:更新版本序号只差1时调用
	 */
	public static boolean zipUpdate(String dir, Component c) {

		byte[] updateZip = null;
		try {
			updateZip = ConnectionPool.getUpdateFileAction().readUpdateZip();
		} catch (Exception e) {
			log.error("zipUpdate is error", e);
			return false;
		}
		// 下载完成,正在更新...
		JecnLoading.setShowText(JecnLoginProperties.getValue("loadingUpadteing"));
		String zipPath = dir + JecnUpdateFileAction.zipName;
		FileOutputStream fos = null;
		File file = new File(zipPath);
		try {
			fos = new FileOutputStream(file);
			fos.write(updateZip);
			fos.flush();

		} catch (Exception e) {
			log.error("zipUpdate is error", e);
			return false;
		} finally {
			if (fos != null) {
				try {
					fos.close();
				} catch (IOException e) {
					log.error("zipUpdate is error", e);
					return false;
				}
			}
		}

		try {
			UnzipAFile.UnZip(VersionValidate.class.getResource("/").toURI()
					.getPath(), zipPath);
		} catch (Exception e) {
			log.error("zipUpdate is error", e);
			return false;
		}

		try {
			JecnLoading.stop();

			// 更新完成,请重启设计器!
			// 由于加载图片出错，所以使用系统的提示框
			JOptionPane.showMessageDialog(c, JecnLoginProperties
					.getValue("updatedRestartDesigner"));
			JecnLoading.initShowDefaultText();
		} catch (Exception e) {
			log.error("zipUpdate is error", e);
		}
		return true;

	}

	/**
	 * @author yxw 2013-4-9
	 * @description:更新版本序号相差1以上过验证设计器版本号出错时调用
	 */
	public static boolean exeUpdate(String dir) {

		byte[] updateExe = null;
		try {
			updateExe = ConnectionPool.getUpdateFileAction().readUpdateExe();
		} catch (Exception e1) {
			log.error("exeUpdate is error", e1);
			return false;
		}
		String exePath = dir + JecnUpdateFileAction.exeName;
		FileOutputStream fos = null;
		File file = new File(exePath);
		try {
			fos = new FileOutputStream(file);
			fos.write(updateExe);
			fos.flush();
			fos.close();
		} catch (Exception e) {
			log.error("exeUpdate is error", e);
			return false;
		}

		// 自动更新执行exe报错
		ResultBean retBean = JecnCmd.cmdExec(exePath);
		if (retBean.getResultState() != 0) {// 执行失败
			return false;
		}
		// try {
		// ConnectionPool.getPersonAction()
		// .loginOut(JecnConstants.getUserId());
		System.exit(0);
		// } catch (Exception e) {
		// log.error("自动更新exe用户退出报错", e);
		// return false;
		// }
		return true;

	}

	public static void updateSysInfoProperties(String path, Properties pro,
			JecnVersionSys versionSys) {
		FileOutputStream out = null;
		try {
			out = new FileOutputStream(path);
			pro.setProperty("version", versionSys.getVersion());
			pro.setProperty("pubtime", versionSys.getPubtime());
			pro.setProperty("updateNum", versionSys.getUpdateNum());

		} catch (FileNotFoundException e) {
			log.error("updateSysInfoProperties is error", e);
			try {
				out.close();
			} catch (IOException e1) {
				log.error("updateSysInfoProperties is error", e);
			}
			return;
		}
		try {
			pro.store(out, "");
			out.close();
		} catch (IOException e) {
			log.error("updateSysInfoProperties is error", e);
		}
	}
}
