function onloadSingleRulePropose(ruleId,isPop,ruleName) {
	//将名称中的空格替换为&nbsp;
	$("#allStateProposeId").children().remove();
	$("#allStateProposeId")
			.load('allStateProposeJsp.action',
				{
					"flowId":ruleId,
					"pagerMethod":"first",
					"isFlowRtnl":"1",
					"processName":ruleName,
					"_totalRows":$("#allProId").text()
				},
					function() {
						setAllStateProposeDivWH('flowAllProposeJspId', isPop);
						onloadPage(document.getElementById("totalPages").value);
					});
}
