package com.jecn.epros.server.service.autoCode;

import java.util.List;

import org.apache.commons.lang.StringUtils;

import com.jecn.epros.server.bean.autoCode.AutoCodeNodeData;
import com.jecn.epros.server.bean.autoCode.AutoCodeResultData;
import com.jecn.epros.server.common.JecnContants;

public enum AutoCodeEnum {
	INSTANCE;

	/**
	 * 
	 * @param parentCodes
	 * @param codeData
	 * @param relatedType
	 *            文件类型： 0:流程、1：文件、2：制度
	 */
	public void getAutoCodeBy(List<String> parentCodes, AutoCodeResultData codeData, AutoCodeNodeData codeNodeData) {
		switch (JecnContants.otherLoginType) {
		case 23:// 株洲时代电气
			getCRRCTimesDQFlowAutoCode(codeNodeData, codeData);
			break;
		case 39:// 株洲时代新材
			getCRRCTimesXCFlowAutoCode(codeNodeData, codeData);
			break;
		case 19:// 烽火
			getFiberhomeAutoCode(parentCodes, codeData, codeNodeData);
			break;
		default:
			getDefaultAutoCode(parentCodes, codeData, codeNodeData);
			break;
		}
	}

	private void getDefaultAutoCode(List<String> parentCodes, AutoCodeResultData codeData, AutoCodeNodeData codeNodeData) {
		String parentCode = getDefaultParentCode(parentCodes);
		codeData.setCodeTotal(codeData.getCodeTotal() + 1);
		codeData.setCode(parentCode + "-" + getCodeString(codeData.getCodeTotal()));
	}

	private String FIBER_HOME = "FH";

	private void getFiberhomeAutoCode(List<String> parentCodes, AutoCodeResultData codeData,
			AutoCodeNodeData codeNodeData) {
		if (parentCodes == null || parentCodes.isEmpty()) {
			return;
		}
		int size = parentCodes.size();
		String parentCode = parentCodes.get(size - 1);
		codeData.setCodeTotal(codeData.getCodeTotal() + 1);
		codeData.setCode(FIBER_HOME + "-" + parentCode + "-" + getFHCodeString(codeData.getCodeTotal()));
	}

	private String getDefaultParentCode(List<String> parentCodes) {
		String parentCode = "";
		for (int i = 0; i < parentCodes.size(); i++) {
			if (StringUtils.isBlank(parentCode)) {
				parentCode = parentCodes.get(i);
			} else {
				parentCode += "-" + parentCodes.get(i);
			}
		}
		return parentCode;
	}

	/**
	 * 文件类型： 0:流程、1：文件、2：制度
	 * 
	 * @param relatedType
	 * @param codeData
	 */
	private void getCRRCTimesDQFlowAutoCode(AutoCodeNodeData codeNodeData, AutoCodeResultData codeData) {
		String code = "Q/TEG ";
		// 1、"Q/TEG "+ (流程 2001-无限、制度 流水号：1-2000)
		int codeTotal = codeData.getCodeTotal();
		if (codeNodeData.getNodeType() == 1) {// 文件自动编号
			code = codeNodeData.getStandardizedFileCode() + ".T";
			if (StringUtils.isBlank(code)) {
				codeData.setError("该文件所属目录编号规则为空！");
				throw new IllegalArgumentException("getCRRCTimesDQFlowAutoCode is error!");
			}
		} else if (codeNodeData.getNodeType() == 0 && codeTotal == 0) {// 流程节点自动编号
			codeTotal = 2000;
		}
		codeData.setCodeTotal(codeTotal + 1);
		codeData.setDirCode(code);
		codeData.setCode(code + getCodeString(codeData.getCodeTotal()));
	}

	private void getCRRCTimesXCFlowAutoCode(AutoCodeNodeData codeNodeData, AutoCodeResultData codeData) {
		String code = "BP ";
		// 1、"BP "+ (BP 0001)
		int codeTotal = codeData.getCodeTotal();
		if (codeNodeData.getNodeType() == 1) {// 文件自动编号
			code = "TM ";
			// "TM 0001-01"
			if (StringUtils.isBlank(codeNodeData.getStandardizedFileCode())) {
				codeData.setError("该文件所属目录编号规则为空！");
				throw new IllegalArgumentException("getCRRCTimesDQFlowAutoCode is error!");
			} else {
				String str[] = codeNodeData.getStandardizedFileCode().split(" ");
				code += str[1] + "-";
			}
		}
		codeData.setCodeTotal(codeTotal + 1);
		codeData.setDirCode(code);
		if (codeNodeData.getNodeType() == 1) {// 文件编号
			codeData.setCode(code + getCodeString(codeData.getCodeTotal()));
		} else {
			codeData.setCode(code + getCRRCXCCodeString(codeData.getCodeTotal()));
		}
	}

	private String getCRRCXCCodeString(int codeTotal) {
		if (codeTotal < 10) {
			return "000" + codeTotal;
		} else if (codeTotal < 100) {
			return "00" + codeTotal;
		} else if (codeTotal < 1000) {
			return "0" + codeTotal;
		}
		return codeTotal + "";
	}

	private String getCodeString(int codeTotal) {
		return codeTotal < 10 ? "0" + codeTotal : codeTotal + "";
	}

	private String getFHCodeString(int codeTotal) {
		if (codeTotal < 10) {
			return "00" + codeTotal;
		} else if (codeTotal < 100) {
			return "0" + codeTotal;
		}
		return codeTotal + "";
	}

	public AutoCodeResultData getStandardizedAutoCodeResultData(Long nodeId, int nodeType,
			IStandardizedAutoCodeService autoCodeService) throws Exception {
		AutoCodeNodeData codeNodeData = new AutoCodeNodeData();
		codeNodeData.setNodeId(nodeId);
		codeNodeData.setNodeType(nodeType);
		AutoCodeResultData resultData = autoCodeService.getStandardizedAutoCode(codeNodeData);
		autoCodeService.updateStandardizedAutoCode(nodeId, nodeType, resultData.getCodeTotal());
		return resultData;
	}

}
