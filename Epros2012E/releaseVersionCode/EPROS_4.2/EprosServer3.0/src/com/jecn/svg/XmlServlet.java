package com.jecn.svg;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.jecn.epros.server.bean.process.JecnFlowStructureT;
import com.jecn.epros.server.service.process.IFlowStructureService;
import com.jecn.epros.server.service.process.IViewAppletService;
import com.jecn.epros.server.util.ApplicationContextUtil;
import com.jecn.epros.server.webBean.WebLoginBean;
import com.jecn.svg.dataModel.SvgPanel;

public class XmlServlet extends HttpServlet {
	private final Logger log = Logger.getLogger(XmlServlet.class);

	@SuppressWarnings("all")
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setCharacterEncoding("utf-8");
		PrintWriter out = response.getWriter();
		Long mapID = Long.valueOf(request.getParameter("mapID"));
		// 流程类型 ：processMap：流程架构；process：流程图
		String mapType = request.getParameter("mapType");
		if (StringUtils.isBlank(mapType)) {
			IFlowStructureService service = (IFlowStructureService) ApplicationContextUtil.getContext().getBean(
					"FlowStructureServiceImpl");
			JecnFlowStructureT structureT = service.get(mapID);
			mapType = structureT.getIsFlow() == 1 ? "process" : "processMap";
		}
		String isPub = request.getParameter("isPub");
		try {
			SvgPanel svg = getSvgPanel(request, mapID, mapType, isPub);
			ObjectMapper mapper = new ObjectMapper();
			mapper.setSerializationInclusion(Include.NON_NULL);
			out.print(mapper.writeValueAsString(svg));
			out.flush();
			out.close();
		} catch (Exception e) {
			log.error("获取数据异常：", e);
		}
	}

	/**
	 * 
	 * @param request
	 * @param flowId
	 * @param flowType
	 *            :process:流程；processMap：流程架构
	 * @param isPub
	 *            : true:发布；false：未发布
	 * @return
	 * @throws Exception
	 */
	private SvgPanel getSvgPanel(HttpServletRequest request, Long flowId, String flowType, String isPub)
			throws Exception {
		SvgPanel svg = null;
		IViewAppletService appletService = (IViewAppletService) ApplicationContextUtil.getContext().getBean(
				"JecnViewAppletServiceImpl");
		WebLoginBean loginBean = (WebLoginBean) request.getSession().getAttribute("webLoginBean");
		if ("processMap".equals(flowType)) {
			if (!"true".equals(isPub)) {
				svg = appletService.getSvgMapBeanT(flowId, loginBean.getJecnUser().getPeopleId(), loginBean
						.getProjectId(),null,0);
			} else {
				svg = appletService.getSvgMapBean(flowId, loginBean.getJecnUser().getPeopleId(), loginBean
						.getProjectId(),0);
			}
		} else if ("process".equals(flowType)) {
			if (!"true".equals(isPub)) {
				svg = appletService
						.getSvgBeanT(flowId, loginBean.getJecnUser().getPeopleId(), loginBean.getProjectId(),null,0);
			} else {
				svg = appletService.getSvgBean(flowId, loginBean.getJecnUser().getPeopleId(), loginBean.getProjectId(),0);
			}
		} else if("organization".equals(flowType)){
			svg=appletService.getSvgOrg(flowId,loginBean.getJecnUser().getPeopleId(), loginBean.getProjectId(),0);
		}
		svg.setFlowId(flowId.toString());
		return svg;
	}
}
