package com.jecn.epros.server.dao.project.impl;

import java.util.List;

import com.jecn.epros.server.bean.project.JecnCurProject;
import com.jecn.epros.server.common.AbsBaseDao;
import com.jecn.epros.server.dao.project.ICurProjectDao;

public class CurProjectDaoImpl extends AbsBaseDao<JecnCurProject, Long>
		implements ICurProjectDao {

	@Override
	public JecnCurProject getJecnCurProjectById() throws Exception {
		String hql = "from JecnCurProject ";// where cp.curProjectId = "+projectId;
		List<JecnCurProject> jecnCurProjectList = this.listHql(hql);
		if(jecnCurProjectList.size()==1){
			return jecnCurProjectList.get(0);
		}
		return null;
	}

}
