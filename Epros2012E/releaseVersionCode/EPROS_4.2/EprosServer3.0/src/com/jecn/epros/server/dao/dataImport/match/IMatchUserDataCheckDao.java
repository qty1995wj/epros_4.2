package com.jecn.epros.server.dao.dataImport.match;

import java.util.List;

import com.jecn.epros.server.common.IBaseDao;
import com.jecn.epros.server.webBean.dataImport.match.DeptMatchBean;
import com.jecn.epros.server.webBean.dataImport.match.PosMatchBean;
import com.jecn.epros.server.webBean.dataImport.match.UserMatchBean;
import com.jecn.epros.server.webBean.dataImport.sync.excelOrDB.JecnTmpPosAndUserBean;

/**
 * 岗位匹配人员同步 验证DAO
 * 
 * @author ZHAGNXIAOHU
 * @date： 日期：Jun 3, 2013 时间：2:04:11 PM
 */
public interface IMatchUserDataCheckDao extends IBaseDao<String, String> {

	/** *****************【人员行间校验】********************************** */
	/**
	 * 
	 * 人员数据行间校验，是否存在重复数据
	 */
	List<UserMatchBean> userSameDataCheck();

	/**
	 * 人员登录名称相同数据验证 登录名称相同，存在岗位编号为空
	 * 
	 * @return
	 */
	List<UserMatchBean> loginNameSameAndPosNullCheck();

	/**
	 * 一人多岗位清空，岗位编号相同 异常
	 * 
	 * @return
	 */
	List<UserMatchBean> sameLoginNameAndSamePos();

	/**
	 * 一人多岗位情况 ，真实姓名不同
	 * 
	 * @return
	 */
	List<UserMatchBean> trueNameNoSameCheck();

	/**
	 * 一人多岗位情况 ，邮箱不同
	 * 
	 * @return
	 */
	List<UserMatchBean> emailNoSameCheck();

	/**
	 * 一人多岗位情况 ，联系方式不同
	 * 
	 * @return
	 */
	List<UserMatchBean> phoneNoSameCheck();

	/** *********************【岗位行间校验】*************************************** */

	/**
	 * 岗位编号不唯一
	 * 
	 * @return List<UserMatchBean>
	 */
	List<PosMatchBean> posNumNotOnly();

	/**
	 * 岗位所属部门编号在部门集合中不存在
	 * 
	 * @return
	 * @return List<UserMatchBean>
	 */
	List<PosMatchBean> posNumRefDeptNumNoExists();

	/** ***************【部门行间校验】******************************* */

	/**
	 * 部门行间校验，部门编号不唯一
	 * 
	 * @return List<DeptMatchBean>
	 */
	List<DeptMatchBean> hasSameDeptNum();

	/**
	 * 同一部门下部门名称不能重复
	 * 
	 * @return
	 */
	List<DeptMatchBean> hasSameDeptName();

	/**
	 * 部门内不能出现重复岗位
	 * 
	 * @return List<UserMatchBean>
	 */
	List<PosMatchBean> deptHashSamePosName();

	List<PosMatchBean> posNameNotSameList();

	/**
	 * 岗位编号相同，部门编号必须相同
	 * 
	 * @return
	 */
	List<PosMatchBean> posNumSameAndDeptSameList();
}
