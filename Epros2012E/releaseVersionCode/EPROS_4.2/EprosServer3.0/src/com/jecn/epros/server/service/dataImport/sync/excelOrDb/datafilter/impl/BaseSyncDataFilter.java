package com.jecn.epros.server.service.dataImport.sync.excelOrDb.datafilter.impl;

import com.jecn.epros.server.service.dataImport.sync.excelOrDb.datafilter.AbstractSyncDataFilter;
import com.jecn.epros.server.webBean.dataImport.sync.excelOrDB.BaseBean;

public class BaseSyncDataFilter extends AbstractSyncDataFilter {

	public BaseSyncDataFilter(BaseBean baseBean) {
		super(baseBean);
	}

	@Override
	protected void addedSpecialDataToSyncData() {
	}

	@Override
	public void filterDeptDataBeforeCheck() {
	}

	@Override
	public void removeUnusefulDataAfterCheck() {
	}

	@Override
	public void filterUserPosDataBeforeCheck() {
	}
}
