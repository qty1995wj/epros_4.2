package com.jecn.epros.domain.propose;

import java.util.List;

public class ProposeTotalChart {
    /**
     * 纵轴 最小值
     **/
    private int minVertical = 0;
    /**
     * 纵轴 最大值
     **/
    private int maxVertical = 0;
    /**
     * 横轴 集合
     */
    private List<String> horizontals;

    /**
     * 内容
     */
    private List<ProposeTotalContentChart> proposeTotalContent;

    public int getMinVertical() {
        return minVertical;
    }

    public void setMinVertical(int minVertical) {
        this.minVertical = minVertical;
    }

    public int getMaxVertical() {
        return maxVertical;
    }

    public void setMaxVertical(int maxVertical) {
        this.maxVertical = maxVertical;
    }

    public List<String> getHorizontals() {
        return horizontals;
    }

    public void setHorizontals(List<String> horizontals) {
        this.horizontals = horizontals;
    }

    public List<ProposeTotalContentChart> getProposeTotalContent() {
        return proposeTotalContent;
    }

    public void setProposeTotalContent(List<ProposeTotalContentChart> proposeTotalContent) {
        this.proposeTotalContent = proposeTotalContent;
    }
}
