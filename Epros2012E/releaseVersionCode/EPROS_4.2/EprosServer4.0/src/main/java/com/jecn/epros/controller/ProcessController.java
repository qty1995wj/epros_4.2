package com.jecn.epros.controller;

import com.jecn.epros.bean.ByteFile;
import com.jecn.epros.configuration.rpc.RPCServiceConfiguration;
import com.jecn.epros.domain.Attachment;
import com.jecn.epros.domain.Paging;
import com.jecn.epros.domain.Resource;
import com.jecn.epros.domain.process.*;
import com.jecn.epros.domain.process.kpi.*;
import com.jecn.epros.domain.process.processFile.OperationFile;
import com.jecn.epros.exception.domain.EprosBussinessException;
import com.jecn.epros.security.AuthenticatedUserUtil;
import com.jecn.epros.service.IChartRPCService;
import com.jecn.epros.service.IDocRPCService;
import com.jecn.epros.service.IProcessRPCService;
import com.jecn.epros.service.process.ProcessService;
import com.jecn.epros.sqlprovider.server3.ConfigContents;
import com.jecn.epros.util.*;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.util.*;
import java.util.List;

@RestController
@Api(value = "/process", description = "流程体系", position = 2)
public class ProcessController {

    @Autowired
    private ProcessService processService;

    @Autowired
    private IDocRPCService docRPCService;

    @Autowired
    private IChartRPCService chartRPCService;

    @Autowired
    private IProcessRPCService processRPCService;

    @ApiOperation(value = "流程-搜索")
    @RequestMapping(value = "processes", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<FlowViewBean> search(@ModelAttribute SearchBean searchBean, @ModelAttribute Paging paging) {
        Map<String, Object> map = new HashMap<>();
        // 项目ID
        map.put("projectId", AuthenticatedUserUtil.getProjectId());
        map.put("peopleId", AuthenticatedUserUtil.getPeopleId());
        // true：系统管理员,浏览管理员
        map.put("isAdmin", AuthenticatedUserUtil.isAdmin());
        // 岗位查阅权限
        map.put("listFigureIds", AuthenticatedUserUtil.getPosIds());
        // 部门查阅权限
        map.put("listOrgIds", AuthenticatedUserUtil.getOrgIds());
        map.put("searchBean", searchBean);
        map.put("paging", paging);

        FlowViewBean viewBean = new FlowViewBean();
        int total = processService.searchFlowTotal(map);
        List<FlowSearchResultBean> listSearchFlowResultBean = processService.searchFlow(map);
        viewBean.setTotal(total);
        viewBean.setData(listSearchFlowResultBean);
        return ResponseEntity.ok(viewBean);
    }

    @ApiOperation(value = "流程-树节点展开，第一次展开时")
    @RequestMapping(value = "/processes/{id}/children", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<TreeNode>> fetchChildren(
            @ApiParam(value = "父节点Id", required = true) @PathVariable Long id) {
        Map<String, Object> map = new HashMap<>();
        // 项目ID
        map.put("projectId", AuthenticatedUserUtil.getProjectId());
        // 父节点ID
        map.put("pid", id);
        // true：系统管理员
        map.put("isAdmin", AuthenticatedUserUtil.isAdmin());
        // 岗位查阅权限
        map.put("listFigureIds", AuthenticatedUserUtil.getPosIds());
        // 部门查阅权限
        map.put("listOrgIds", AuthenticatedUserUtil.getOrgIds());
        map.put("peopleId", AuthenticatedUserUtil.getPeopleId());

        List<TreeNode> result = new ArrayList<>();
        // 真实子节点
        result.addAll(getProcessNodes(map));
        // 虚拟架构
        result.addAll(getInventedProcessNodes(map));
        return ResponseEntity.ok(result);
    }

    private List<TreeNode> getInventedProcessNodes(Map<String, Object> map) {
        return processService.findInventedFlows(map);
    }

    private List<TreeNode> getProcessNodes(Map<String, Object> map) {
        return processService.findChildFlows(map);
    }

    @PreAuthorize("hasAuthority('processChart')")
    @ApiOperation(value = "流程-图")
    @RequestMapping(value = "/processes/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> getProcessChart(@PathVariable Long id,
                                                  @RequestParam(defaultValue = "1", name = "pub") String pub,
                                                  @RequestParam(required = false) Long historyId) {
        Params params = new Params("projectId", AuthenticatedUserUtil.getProjectId());
        if ("1".equals(pub)) {
            params.add("isPub", true);
        } else {
            params.add("isPub", false);
        }
        Map<String, Object> map = params.toMap();
        map.put("httpURL", RPCServiceConfiguration.DEF_RPC_SERVER_HTTPURL);
        map.put("historyId", historyId);
        /**
         * 语言类型 0 中文  1英文
         */
        map.put("languageType", AuthenticatedUserUtil.getPrincipal().getDataAccessAuthorityBean().getLanguage());
        String chartJSON = chartRPCService.getChartJSON(id, "process", AuthenticatedUserUtil.getPeopleId(), map);
        return ResponseEntity.ok(chartJSON);
    }

    @PreAuthorize(" hasAuthority('processInfo')")
    @ApiOperation(value = "流程-流程属性")
    @RequestMapping(value = "/processes/{id}/info", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ProcessBaseAttribute> findBaseInfo(@PathVariable Long id, @RequestParam int isPublish, @RequestParam(required = false) Long historyId, @RequestParam boolean isApp) {
        Map<String, Object> map = getProcessRequestMap(id, isPublish);
        map.put("historyId", historyId);
        map.put("isApp", isApp);
        return ResponseEntity.ok(processService.findBaseInfoBean(map));
    }

    @PreAuthorize(" hasAuthority('processKpi')")
    @ApiOperation(value = "流程-KPI：获取流程下的KPI")
    @RequestMapping(value = "/processes/{id}/kpi", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<FlowKPIBean>> fetchKpi(@PathVariable Long id, @RequestParam int isPublish) {
        Map<String, Object> map = getProcessRequestMap(id, isPublish);
        return ResponseEntity.ok(processService.findKpiNames(map));
    }

    @ApiOperation(value = "流程-KPI：查询获取KPI值,并显示KPI曲线图")
    @RequestMapping(value = "/processes/{id}/kpi/{kpiId}/values", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<KpiSearchResultData> fetchKpiValues(@PathVariable Long id, @PathVariable Long kpiId,
                                                              @ModelAttribute KPISearchBean kpiSearchBean) {
        Map<String, Object> map = new HashMap<>();
        map.put("kpiAndId", kpiId);
        map.put("startTime", kpiSearchBean.getStartTime());
        map.put("endTime", kpiSearchBean.getEndTime());
        return ResponseEntity.ok(processService.findKpiValuesByDate(map));
    }

    @ApiOperation(value = "流程-导入流程KPI值")
    @RequestMapping(value = "/processes/{id}/kpi/{kpiId}", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> kpiValuesExport(@PathVariable Long id, @PathVariable Long kpiId,
                                                  @RequestBody List<KPIValuesBean> kpiValuesBeans) {
        Map<String, Object> map = new HashMap<>();
        map.put("kpiAndId", kpiId);
        map.put("peopleId", AuthenticatedUserUtil.getPeopleId());
        // 保存KPI
        processService.saveKpiValues(map, kpiValuesBeans);
        return ResponseEntity.ok(null);
    }

    @ApiOperation(value = "流程-更新KPI值")
    @RequestMapping(value = "/processes/{id}/updateKpi/{kpiId}", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> updateKpiValue(@PathVariable Long id, @PathVariable Long kpiId, @RequestBody KPIValuesBean kpiValuesBean) {
        Map<String, Object> map = new HashMap<>();
        map.put("kpiAndId", kpiId);
        map.put("peopleId", AuthenticatedUserUtil.getPeopleId());
        processService.updateKpiValue(map, kpiValuesBean);
        return ResponseEntity.ok(null);
    }

    @ApiOperation(value = "流程-KPI详情")
    @RequestMapping(value = "/processes/kpi/{kpiId}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<KPIInfo>> findKpiDetail(@PathVariable Long kpiId) {
        Map<String, Object> map = new HashMap<>();
        map.put("kpiAndId", kpiId);
        return ResponseEntity.ok(processService.findKpiInfos(map));
    }

    @ApiOperation(value = "流程-KPI Excel导入")
    @RequestMapping(value = "/processes/{id}/kpi/{kpiId}/import", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<KPIValuesBean>> kpiExcelImport(@PathVariable Long id, @PathVariable Long kpiId,
                                                              @RequestParam(value = "file", required = true) MultipartFile file, @ModelAttribute KPISearchBean kpiSearchBean) {
        Map<String, Object> map = new HashMap<>();
        map.put("kpiAndId", kpiId);
        map.put("flowId", id);

        map.put("startTime", kpiSearchBean.getStartTime());
        map.put("endTime", kpiSearchBean.getEndTime());

        List<KPIValuesBean> kpiValues = null;
        map.put("file", getFile(file));
        FlowKpiName flowKpi = processService.getKpiNameByMap(map);
        map.put("kpiHorType", flowKpi.getKpiHorizontal());
        map.put("userId", AuthenticatedUserUtil.getPeopleId());
        // 上传文件
        String str = processRPCService.excelImporteFlowKpiImage(map);
        if ("success".equalsIgnoreCase(str)) {
            // 刷新页面，重新获取导入的流程KPI
            KpiSearchResultData searchResultData = processService.findKpiValuesByDate(map);
            kpiValues = searchResultData.getKpiValues();
        }
        return ResponseEntity.ok(kpiValues);
    }

    private ByteFile getFile(MultipartFile file) {
        ByteFile byteFile = new ByteFile();
        byteFile.setFileName(file.getName());
        try {
            byteFile.setBytes(file.getBytes());
        } catch (IOException e) {
            throw new EprosBussinessException("ProcessController getFile is error", e);
        }
        return byteFile;
    }

    @PreAuthorize(" hasAuthority('processDocument')")
    @ApiOperation(value = "流程文件")
    @RequestMapping(value = "/processes/{id}/document", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<OperationFile> findProcessDocument(@PathVariable Long id, @RequestParam int isPublish, @RequestParam int historyId) {
        Map<String, Object> map = getProcessRequestMap(id, isPublish);
        map.put("historyId", historyId);
        return ResponseEntity.ok(processService.getShowProcessFile(map));
    }

    @ApiOperation(value = "流程-文档-DOC下载")
    @RequestMapping(value = "/processes/{id}/document/doc", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ByteFile> fetchProcessesWord(
            @ApiParam(value = "流程Id", required = true) @PathVariable String id,
            @ApiParam(value = "是否为流程架构", required = true) @RequestParam boolean isMap,
            @ApiParam(value = "版本id", required = false) @RequestParam Long historyId) {
        Map<String, Object> paramMap = new HashMap<>();
        paramMap.put("isMap", String.valueOf(isMap));
        paramMap.put("isPub", "true");
        paramMap.put("flowId", id);
        paramMap.put("curPeopleId", AuthenticatedUserUtil.getPeopleId());
        paramMap.put("historyId", historyId);
        ByteFile downloadDoc = docRPCService.downloadDoc(paramMap);
        return ResponseEntity.ok(downloadDoc);
    }

    @ApiOperation(value = "流程-文档-XLS下载")
    @RequestMapping(value = "/processes/{id}/document/xls", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ByteFile> fetchProcessesExcel(
            @ApiParam(value = "流程Id", required = true) @PathVariable String id) {
        Map<String, Object> paramMap = new HashMap<>();
        paramMap.put("isPub", "true");
        paramMap.put("flowId", id);
        paramMap.put("curPeopleId", AuthenticatedUserUtil.getPeopleId());
        ByteFile downloadDoc = docRPCService.downloadExcel(paramMap);
        return ResponseEntity.ok(downloadDoc);
    }

    @ApiOperation("下载流程图片")
    @RequestMapping(value = "/processes/downloadMap", method = RequestMethod.GET)
    public ResponseEntity<?> downloadMap(String mapIconPath, String mapName, HttpServletRequest request) {
        String mark = "";
        //获取文件存储盘符
        mark = ConfigUtils.getValue(ConfigContents.ConfigItemPartMapMark.systemLocalFileProfix.toString())
                //根据传入地址获取图片路径
                + mapIconPath;
        byte[] s;
        //获取文件字节  异常直接返回
        File outFile = null;
        try {
            s = Utils.toByteArray(mark.replace("/", "\\"));
            // 添加水印
            String outImgPath = mark.substring(0, mark.lastIndexOf("/")) + File.separator + UUID.randomUUID().toString() + ".jpg";
            IMGWaterMarkUtils.mark(new File(mark), outImgPath, Color.GRAY, AuthenticatedUserUtil.getTrueName());
            //返回文件
            outFile = new File(outImgPath);
            if (!outFile.exists()) {
                outFile.createNewFile();
            }
            Attachment attachment = new Attachment();
            attachment.setContent(Utils.getByte(outFile));
            attachment.setName(mapName + mapIconPath.substring(mapIconPath.lastIndexOf(".")));
            return AttachmentDownloadUtils.newResponseEntity(request, attachment);
        } catch (IOException e) {
            return ResponseEntity.ok(null);
        } finally {
            if (outFile != null) {
                outFile.delete();
            }
        }
        //return ResponseEntity.ok(downloadMap);
    }

    @ApiOperation(value = "流程-活动明细")
    @RequestMapping(value = "/processes/{id}/activities/{activityId}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<FlowActiveDetainBean> findActiveDetail(@PathVariable Long id, @PathVariable Long activityId,
                                                                 @RequestParam int isPublish, @RequestParam(required = false) Long historyId) {
        Map<String, Object> map = new HashMap<>();
        map.put("isPub", isPublish == 1 ? "" : "_T");
        // 岗位查阅权限
        map.put("activeId", activityId);
        map.put("historyId", historyId);
        return ResponseEntity.ok(processService.findFlowActiveDetainBean(map));
    }

    @ApiOperation(value = "流程-操作模板")
    @RequestMapping(value = "/processes/{id}/templates", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<FlowOperationModel>> findProcessTemplates(@PathVariable Long id,
                                                                         @RequestParam int isPublish) {
        Map<String, Object> map = getProcessRequestMap(id, isPublish);
        return ResponseEntity.ok(processService.getProcessActiveBeanList(map));
    }

    @PreAuthorize(" hasAuthority('processSupports')")
    @ApiOperation(value = "流程-支撑文件")
    @RequestMapping(value = "/processes/{id}/supports", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<Resource>> fetchProcessesSupportResources(
            @ApiParam(value = "流程Id", required = true) @PathVariable String id, @RequestParam int isPublish, @RequestParam Long historyId) {
        Map<String, Object> paramMap = new HashMap<>();
        paramMap.put("projectId", AuthenticatedUserUtil.getProjectId());
        paramMap.put("id", id);
        paramMap.put("isPub", isPublish == 0 ? false : true);
        paramMap.put("historyId", historyId);
        List<Resource> resources = processService.fetchProcessesSupportResources(paramMap);
        return ResponseEntity.ok(resources);
    }

    @ApiOperation(value = "导出流程kpi异常")
    @RequestMapping(value = "/processes/kpi/outputKpi", method = RequestMethod.GET)
    public ResponseEntity<?> outputKpi(@RequestParam("selectedKPI") Long kpiId,
                                       @RequestParam("startTime") String startTime,
                                       @RequestParam("endTime") String endTime,
                                       HttpServletRequest request) {
        Map<String, Object> params = new HashMap<>();
        params.put("kpiId", kpiId);
        params.put("startTime", startTime);
        params.put("endTime", endTime);
        ByteFile byteFile = processRPCService.exportKpi(params);
        if (byteFile == null) {
            throw new EprosBussinessException("导出流程kpi异常");
        }
        return AttachmentDownloadUtils.newResponseEntity(request, Utils.byteFileToAttachment(byteFile));
    }

    @ApiOperation("下载流程贯通性检查表")
    @RequestMapping(value = "/processes/downloadCheckout/{flowId}", method = RequestMethod.GET)
    public ResponseEntity<?> downloadCheckout(@PathVariable(value = "flowId") Long flowId, HttpServletRequest request) {
        Map<String, Object> param = new HashMap<>();
        param.put("flowId", flowId);
        ByteFile byteFile = processRPCService.downLoadCheckout(param);
        Attachment attachment = Utils.byteFileToAttachment(byteFile);
        return AttachmentDownloadUtils.newResponseEntity(request, attachment);
    }

    @ApiOperation("下载角色规范性检查表")
    @RequestMapping(value = "/processes/downloadCheckoutRole/{flowId}", method = RequestMethod.GET)
    public ResponseEntity<?> downloadCheckoutRole(@PathVariable(value = "flowId") Long flowId, HttpServletRequest request) {
        Map<String, Object> param = new HashMap<>();
        param.put("flowId", flowId);
        ByteFile byteFile = processRPCService.downLoadCheckoutRole(param);
        Attachment attachment = Utils.byteFileToAttachment(byteFile);
        return AttachmentDownloadUtils.newResponseEntity(request, attachment);
    }

    @ApiOperation("查阅流程贯通性检查表")
    @RequestMapping(value = "/processes/showCheckout/{flowId}", method = RequestMethod.GET)
    public ResponseEntity<?> showCheckout(@PathVariable(value = "flowId") Long flowId) {
        return ResponseEntity.ok(processService.showCheckout(flowId));
    }

    @ApiOperation("查阅角色规范性检查表")
    @RequestMapping(value = "/processes/showCheckoutRole/{flowId}", method = RequestMethod.GET)
    public ResponseEntity<?> showCheckoutRole(@PathVariable(value = "flowId") Long flowId) {
        return ResponseEntity.ok(processService.showCheckoutRole(flowId));
    }


    private Map<String, Object> getProcessRequestMap(Long flowId, int isPublish) {
        Map<String, Object> map = new HashMap<>();
        map.put("flowId", flowId);
        map.put("isPub", isPublish == 1 ? "" : "_T");
        return map;
    }
}
