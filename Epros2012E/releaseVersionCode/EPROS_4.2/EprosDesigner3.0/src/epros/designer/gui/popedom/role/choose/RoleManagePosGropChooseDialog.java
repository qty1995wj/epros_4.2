package epros.designer.gui.popedom.role.choose;

import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import org.apache.log4j.Logger;

import com.jecn.epros.server.bean.tree.JecnTreeBean;

import epros.designer.gui.common.JecnConstants;
import epros.designer.gui.common.JecnDesignerCommon;
import epros.designer.gui.common.JecnSelectChooseDialog;
import epros.designer.tree.JecnTree;
import epros.designer.util.ConnectionPool;
import epros.designer.util.JecnProperties;
import epros.draw.gui.top.dialog.JecnDialog;

/*******************************************************************************
 * 岗位组选择框
 * 
 * @author 2012-08-15
 * 
 */
public class RoleManagePosGropChooseDialog extends JecnSelectChooseDialog {
	private static Logger log = Logger.getLogger(RoleManagePosGropChooseDialog.class);

	public RoleManagePosGropChooseDialog(List<JecnTreeBean> list) {
		super(list, 8);
		// 岗位组选择
		this.setTitle(JecnProperties.getValue("positionGroupSelect"));
	}
	
	public RoleManagePosGropChooseDialog(List<JecnTreeBean> list,int type) {
		super(list, type);
		// 岗位组选择
		this.setTitle(JecnProperties.getValue("positionGroupSelect"));
	}

	public RoleManagePosGropChooseDialog(List<JecnTreeBean> list, JecnDialog jecnDialog) {
		super(list, 8, jecnDialog);
		// 岗位组选择
		this.setTitle(JecnProperties.getValue("positionGroupSelect"));
	}

	@Override
	public Vector<String> convertRowData(JecnTreeBean jecnTreeBean) {
		Vector<String> data = new Vector<String>();
		data.add(jecnTreeBean.getId().toString());
		data.add(jecnTreeBean.getName());
		data.add(jecnTreeBean.getPname());
		return data;
	}

	@Override
	public Vector<Vector<String>> getContent(List<JecnTreeBean> list) {
		Vector<Vector<String>> content = new Vector<Vector<String>>();
		if (list != null) {
			for (JecnTreeBean jecnTreeBean : list) {
				Vector<String> data = new Vector<String>();
				data.add(jecnTreeBean.getId().toString());
				data.add(jecnTreeBean.getName());
				data.add(jecnTreeBean.getPname());
				content.add(data);
			}
		}
		return content;
	}

	@Override
	public JecnTree getJecnTree() {
		return new RoleManageHighPosGroupChooseTree(this);
	}

	@Override
	public String getSearchLabelName() {
		return JecnProperties.getValue("positionGroupNameC");
	}

	@Override
	public Vector<String> getTableTitle() {
		Vector<String> title = new Vector<String>();
		title.add("id");
		/*
		 * 岗位组名称
		 */
		title.add(JecnProperties.getValue("positionGroupName"));
		/*
		 * 岗位组目录
		 */
		title.add(JecnProperties.getValue("positionGroupCatalog"));
		return title;
	}

	@Override
	public List<JecnTreeBean> searchByName(String name) {
		try {
			return ConnectionPool.getPosGroup().searchPositionGroupsByName(JecnConstants.projectId, name,JecnDesignerCommon.getSecondAdminUserId());
		} catch (Exception e) {
			log.error("PosGropChooseDialog searchByName is error！", e);
			return new ArrayList<JecnTreeBean>();
		}
	}

	public int[] hiddenCols() {
		return new int[] { 0, 2 };
	}
}
