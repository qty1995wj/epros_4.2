/**
 * 风险体系
 */
function riskSystem() {

	//根节点配置
	var riskRoot = new Ext.tree.AsyncTreeNode( {
		text : i_riskSys,//该节点所显示的文本
		expanded : true,//判断节点是否展开
		id : "0"//节点ID
	})

	//节点使用的Loader
	var riskLoader = new Ext.tree.TreeLoader( {
		dataUrl : "getChildRisks.action"
	})

	//风险体系Tab
	var riskTabPanel = new Ext.TabPanel( {
		border : false,//body元素的边框，false则隐藏
		activeTab : 0,//一个字符串或数字(索引)表示，渲染后就活动的那个tab（默认为没有）
		height : Ext.getCmp("main-panel").getInnerHeight() + 58,
		items : [ {//默认Tab选项内容
			title : i_search,// 搜索
			id : "riskSearchTab_id",
			autoLoad : {//面板会尝试在渲染后立即加载内容
				url : basePath + "jsp/riskSystem/riskSystemSeareh.jsp",
				scripts : false,
				callback : function(el, success, response) {
					riskSystemGrid();
				}
			}
		} ],
		listeners : {
			'tabchange' : function(tabPanel, panel) {
				if (panel.getId() == 'riskSearchTab_id') {
					if (typeof (riskSysGridGridPanel) != "undefined") {
						riskSysGridGridPanel.setSize(tabPanel.getWidth() - 2,
								tabPanel.getHeight() - 190);
					}
				} else if (panel.getId() == 'riskSys_riskList_id') {
					if (typeof (riskSysGridGridPanel) != "undefined") {
						riskSysGridGridPanel.setSize(tabPanel.getWidth() - 2,
								tabPanel.getHeight() - 28);
					}
				}
			},
			'bodyresize' : function(p, width, height) {
				if (p.getActiveTab() == null) {
					return;
				}
				if (p.getActiveTab().getId() == 'riskSys_riskList_id') {
					Ext.getCmp('riskListGridGridPanel_id').setSize(width - 2,
							height);
				} else if (p.getActiveTab().getId() == 'riskInventoryTabId') {
					Ext.getCmp('riskInventoryGridPanel_id').setSize(width - 2,
							height - 38);
				}

			}
		}
	});

	//树面板配置
	var riskTree = new Ext.tree.TreePanel( {
		root : riskRoot,//root根节点的对象
		height : Ext.getCmp("main-panel").getInnerHeight() - 25,//树面板高度
		loader : riskLoader,//加载风险信息
		useArrows : true,//是否在树中使用Vista样式箭头，默认为false
		autoScroll : true,//面板body元素上，设置overflow
		rootVisible : false,//隐藏根节点
		animate : true,// 开启动画效果
		enableDD : false,// 不允许子节点拖动
		border : false,
		listeners : {
			"dblclick" : function(node, e) {
				var length = riskTabPanel.items.length;
				if (node.attributes.type == "riskPoint") {
					for ( var i = length - 2; i >= 0; i--) {
						var p = riskTabPanel.getComponent(i);
						riskTabPanel.remove(p, true);
					}
					riskTabPanel.insert(0, {
						title : i_riskDetail,//风险详情
						id : "riskSys_riskDetail_id",
						autoLoad : {
							url : basePath + "getRiskDetailById.action?id="
									+ node.id,
							callback : function(el, success, response) {
								//								getRelatedProcessDiv(node.id);
							//								getRelatedRuleDiv(node.id);
						}
						}
					});
					riskTabPanel.setActiveTab(0);
				} else if (node.attributes.type == "riskDir") {
					for ( var i = length - 2; i >= 0; i--) {
						var p = riskTabPanel.getComponent(i);
						riskTabPanel.remove(p, true);
					}
					riskTabPanel.insert(0, {
						title : i_riskList,// 风险列表
						border : false,
						id : "riskSys_riskList_id",
						autoLoad : {
							url : basePath
									+ "jsp/riskSystem/riskSystemList.jsp",
							callback : function() {
								riskListByPid(node.id);
							}
						}
					});
					riskTabPanel.insert(1, {// 风险清单
								title : i_riskInventory,
								id : "riskInventoryTabId",
								autoLoad : {
									url : basePath
											+ "jsp/riskSystem/riskList.jsp",
									callback : function() {
										riskInventoryJson(node.id, false);
									}
								}
							});
					if (isShowDirList) {
						riskTabPanel.remove(Ext.getCmp("riskSys_riskList_id"));
					}
					riskTabPanel.setActiveTab(0);
				}
			}
		}
	});

	//风险主页面设置
	var riskPanel = new Ext.Panel(
			{
				renderTo : "riskSys_id",//渲染到风险主页面
				border : false,
				id : 'riskSystem_index_id',
				layout : "border",
				height : Ext.getCmp("main-panel").getInnerHeight(),
				items : [
						{
							region : "west",
							border : 0,
							title : "<div  style='text-align:center;font-weight:bold'>"
									+ i_riskSys + "</div>",
							split : true,
							collapseMode : "mini",
							width : 300,
							items : [ riskTree ]
						},
						{
							region : "center",
							border : 0,
							id : "risk_center_panel_id",
							items : [ riskTabPanel ],
							listeners : {
								'bodyresize' : function(p, width, height) {
									if (riskTabPanel.getActiveTab() == null) {
										return;
									}
									riskTree.setHeight(height - 5);
									riskTabPanel.setSize(width, height);
									if (riskTabPanel.getActiveTab().getId() == 'riskSearchTab_id') {
										riskSysGridGridPanel.setSize(width,
												height - 138);
									} else if (riskTabPanel.getActiveTab()
											.getId() == '') {
										riskSysGridGridPanel.setSize(width - 2,
												height - 28);
									}
								}
							}
						} ]
			});
}

/**
 * 风险Record封装
 */
function riskSystemGrid() {
	riskSysGridStore = new Ext.data.Store( {
		//		autoLoad : {//如果有值传入，那么store的load会自动调用，发生在autoLoaded对象创建之后
		//			params : {
		//				start : 0,
		//				limit : pageSize
		//			}
		//		},
		proxy : new Ext.data.HttpProxy( {//用于访问数据对象
					url : "getRiskList.action"
				}),
		reader : new Ext.data.JsonReader(//表示store初始化后，要加载的内联数据
				{
					totalProperty : "totalProperty",
					root : "root",
					id : "id"
				}, [ {
					name : "riskId",
					mapping : "id"
				}, {
					name : "riskCode",
					mapping : "riskCode"
				}, {
					name : "name",
					mapping : "name"
				}, {
					name : "gradeStr",
					mapping : "gradeStr"
				} ])
	});

	riskSysGridColumnModel = new Ext.grid.ColumnModel(//Grid的列模型（ColumnModel）的默认实现
			[ new Ext.grid.RowNumberer(), {
				header : i_riskNumber,//头部显示的名称
				dataIndex : "name",//要绑定的Store之Record字段名
				align : "center",
				menuDisabled : true,//隐藏列显示
				sortable : true,
				renderer : riskDetailRenderer
			// 可排序
					}, {
						header : i_riskName,
						dataIndex : "riskCode",
						align : "center",
						menuDisabled : true,//隐藏列显示
						sortable : true
					// 可排序
					}, {
						header : i_riskGrade,
						dataIndex : "gradeStr",
						align : "center",
						menuDisabled : true,//隐藏列显示
						sortable : true
					// 可排序
					}
			//					, {
			//						header : i_operation,//操作
			//						align : "center",
			//						menuDisabled : true,
			//						renderer : riskDetailRenderer
			//					} 
			]);

	riskSysGridBbar = new Ext.PagingToolbar( {//自动提供翻页控制的工具栏
				pageSize : pageSize,
				store : riskSysGridStore,
				displayInfo : true,
				displayMsg : "{0}--{1} " + i_twig + i_together + " {2}"
						+ i_twig,
				emptyMsg : i_noData
			});

	riskSysGridGridPanel = new Ext.grid.GridPanel( {
		id : "riskSysGridGridPanel_id",
		renderTo : 'riskSearchGrid_id',
		height : Ext.getCmp("main-panel").getInnerHeight() - 135,
		autoScroll : true,// 滚动条
		stripeRows : true,// 显示行的分隔符
		viewConfig : {
			forceFit : true
		// 让列的宽度和grid一样
		},
		frame : false,
		bodyStyle : 'border-left: 0px;',
		store : riskSysGridStore,
		colModel : riskSysGridColumnModel,
		bbar : riskSysGridBbar
	})
}

/**
 * 风险搜索
 */
function riskSearch() {
	//风险名称
	var riskName = document.getElementById("riskNum_id").value;
	var namelength = 0;
	var riskNames = riskName.split(" ");
	for ( var f = 0, len = riskNames.length; f < len; f++) {
		if (riskNames[f] != "") {
			namelength++;
		}
	}
	if (namelength > 3) {
		alert("风险名称最多输入三个！");
		return;
	}

	// 责任部门名称
	var orgDutyName = document.getElementById("orgDutyName_risk_id").value;
	// 责任部门ID
	var orgDutyId = document.getElementById("orgDutyId_risk_id").value;
	//风险编号
	var riskNum = document.getElementById("riskName_id").value;
	var numlength = 0;
	var riskNums = riskNum.split(" ");
	for ( var n = 0, fnum = riskNums.length; n < fnum; n++) {
		if (riskNums[n] != "") {
			numlength++;
		}
	}
	if (numlength > 3) {
		alert("风险编号最多输入三个！");
		return;
	}

	riskSysGridStore.setBaseParam("name", riskName);
	riskSysGridStore.setBaseParam("riskCode", riskNum);
	riskSysGridStore.setBaseParam("orgName", orgDutyName);
	if (orgDutyId && orgDutyId != "") {
		riskSysGridStore.setBaseParam("orgId", orgDutyId);
	}

	riskSysGridStore.load( {
		params : {
			start : 0,
			limit : pageSize
		}
	});
}

/**
 * 风险搜索重置
 */
function riskSearchReset() {
	document.getElementById("riskName_id").value = "";
	document.getElementById("riskNum_id").value = "";
	document.getElementById("orgDutyId_risk_id").value = "-1";
	document.getElementById("orgDutyName_risk_id").value = "";
}

/**
 * 根据目录ID查询该目录下所有子节点
 */
function riskListByPid(id) {
	riskListGridStore = new Ext.data.Store( {
		autoLoad : {
			params : {
				start : 0,
				limit : pageSize
			}
		},
		baseParams : {
			riskId : id
		},
		proxy : new Ext.data.HttpProxy( {
			url : "getRiskListByPid.action"
		}),
		reader : new Ext.data.JsonReader( {
			totalProperty : "totalProperty",
			root : "root",
			id : "id"
		}, [ {
			name : "riskId",
			mapping : "id"
		}, {
			name : "riskCode",
			mapping : "riskCode"
		}, {
			name : "name",
			mapping : "name"
		}, {
			name : "gradeStr",
			mapping : "gradeStr"
		} ])
	});

	riskListGridColumnModel = new Ext.grid.ColumnModel( [
			new Ext.grid.RowNumberer(), {
				header : i_riskNumber,
				dataIndex : "riskCode",
				align : "center",
				menuDisabled : true,
				renderer : riskDetailRendererA
			}, {
				header : i_riskName,
				dataIndex : "name",
				align : "center",
				menuDisabled : true,
				sortable : true
			}, {
				header : i_riskGrade,
				dataIndex : "gradeStr",
				align : "center",
				menuDisabled : true
			}
	//			, {
			//				header : i_operation,
			//				align : "center",
			//				menuDisabled : true,
			//				renderer : riskDetailRenderer
			//			} 
			]);

	riskListGridBbar = new Ext.PagingToolbar( {
		pageSize : pageSize,
		store : riskListGridStore,
		displayInfo : true,
		displayMsg : "{0}--{1} " + i_twig + i_together + " {2}" + i_twig,
		emptyMsg : i_noData
	});

	riskListGridGridPanel = new Ext.grid.GridPanel( {
		id : "riskListGridGridPanel_id",
		renderTo : 'riskGrid_id',
		height : Ext.getCmp("main-panel").getInnerHeight() - 28,
		autoScroll : true,// 滚动条
		stripeRows : true,// 显示行的分隔符
		viewConfig : {
			forceFit : true
		// 让列的宽度和grid一样
		},
		frame : false,
		border : false,
		store : riskListGridStore,
		colModel : riskListGridColumnModel,
		bbar : riskListGridBbar
	})
}

/**
 * 双击目录查询风险信息，根据风险ID查询风险详细信息
 * @param {Object} value
 * @param {Object} cellmeta
 * @param {Object} record
 * @return {TypeName} 
 */
function riskDetailRenderer(value, cellmeta, record) {
	return "<div><a target='_blank' class='a-operation' href='getRiskById.action?"
			+ "riskDetailId="
			+ record.data['riskId']
			+ "' >"
			+ record.data['name'] + "</a>";
}

function riskDetailRendererA(value, cellmeta, record) {
	return "<div><a target='_blank' class='a-operation' href='getRiskById.action?"
			+ "riskDetailId="
			+ record.data['riskId']
			+ "' >"
			+ record.data['riskCode'] + "</a>";
}
