package com.jecn.epros.server.download.word;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import com.jecn.epros.server.action.web.login.ad.iflytek.JecnIflytekSyncUserEnum;
import com.jecn.epros.server.bean.download.FlowDriverBean;
import com.jecn.epros.server.bean.download.KeyActivityBean;
import com.jecn.epros.server.bean.download.ProcessDownloadBaseBean;
import com.jecn.epros.server.bean.download.ProcessDownloadBean;
import com.jecn.epros.server.bean.download.RelatedProcessBean;
import com.jecn.epros.server.bean.process.JecnFlowBasicInfo;
import com.jecn.epros.server.bean.process.JecnFlowBasicInfoT;
import com.jecn.epros.server.bean.process.JecnFlowDriver;
import com.jecn.epros.server.bean.process.JecnFlowDriverT;
import com.jecn.epros.server.bean.process.ProcessAttributeBean;
import com.jecn.epros.server.bean.task.JecnTaskHistoryFollow;
import com.jecn.epros.server.bean.task.JecnTaskHistoryNew;
import com.jecn.epros.server.common.JecnCommon;
import com.jecn.epros.server.common.JecnConfigTool;
import com.jecn.epros.server.dao.control.IJecnDocControlDao;
import com.jecn.epros.server.dao.process.IFlowStructureDao;
import com.jecn.epros.server.dao.process.IProcessBasicInfoDao;
import com.jecn.epros.server.email.buss.TaskEmailParams;
import com.jecn.epros.server.util.JecnUtil;

public class ProcessDownDataUtil {
	private static final Logger log = Logger.getLogger(ProcessDownDataUtil.class);

	/**
	 * 活动明显 活动明细数据 0:活动编号 1执行角色 2活动名称 3活动说明 4输入 5 输出 6办理时限目标值 7办理时限原始值 8活动id
	 * 9角色id
	 * 
	 * @param listActiveShow
	 *            0活动主键ID 1活动编号 2活动名称 3活动说明 4关键活动类型 5关键说明 6 活动输入 7 活动输出 8链接id
	 *            9链接类型 10办理时限目标值 11办理时限原始值
	 * @param listRoleShow
	 *            角色明细数据 0是角色主键ID 1角色名称 2角色职责
	 * @param listRoleAndActiveRelate
	 *            流程下所有的角色和活动关系表 0是角色主键ID 1是活动主键ID
	 * @param listInputFiles
	 *            0是主键ID 1是活动主键Id 2是文件主键ID 3是文件名称 4是文件类型（0是输入，1是操作手册） 5是文件编号 7
	 *            活动文本输入
	 * @param listOutFiles
	 *            流程下所有的活动的输出 0是主键ID 1是活动主键Id 2是文件主键ID 3是文件名称 4是文件编号 5活动文本输出
	 * @param listRefIndicators
	 *            0主键ID ， 1指标名 ， 2指标值 , 3活动ID
	 * @return
	 */
	public static List<Object[]> getActiveShow(List<Object[]> listActiveShow, List<Object[]> listRoleShow,
			List<Object[]> listRoleAndActiveRelate, List<Object[]> listInputFiles, List<Object[]> listOutFiles,
			List<Object[]> listRefIndicators) {
		boolean useOldInout = !JecnConfigTool.useNewInout();
		List<Object[]> listResult = new ArrayList<Object[]>();
		StringBuilder str = new StringBuilder();
		for (Object[] objActives : listActiveShow) {
			str.setLength(0);
			if (objActives == null || objActives[0] == null) {
				continue;
			}
			Object[] obj = new Object[10];
			// 活动编号
			obj[0] = objActives[1];
			obj[8] = objActives[0];
			// 执行角色
			for (Object[] objRoleAndActiveRelate : listRoleAndActiveRelate) {
				if (objRoleAndActiveRelate == null || objRoleAndActiveRelate[0] == null
						|| objRoleAndActiveRelate[1] == null) {
					continue;
				}
				if (objActives[0].equals(objRoleAndActiveRelate[1])) {
					for (Object[] objRoleShow : listRoleShow) {
						if (objRoleShow == null || objRoleShow[0] == null) {
							continue;
						}
						if (objRoleAndActiveRelate[0].equals(objRoleShow[0])) {
							obj[1] = objRoleShow[1];
							obj[9] = objRoleShow[0];
							break;
						}
					}
					break;
				}
			}
			// 活动名称
			obj[2] = objActives[2];
			// 活动说明
			String content = "";
			if (objActives[3] != null) {
				content = objActives[3].toString();
			}
			obj[3] = content;
			if (useOldInout) {
				// 输入
				str.setLength(0);
				// 活动文本输入
				if (objActives[6] != null) {
					str.append(objActives[6].toString()).append("\n");
				}
				for (Object[] objInputFiles : listInputFiles) {
					if (objInputFiles == null || objInputFiles[0] == null || objInputFiles[1] == null
							|| objInputFiles[2] == null || objInputFiles[4] == null) {
						continue;
					}
					// 操作规范
					if ("1".equals(objInputFiles[4].toString())) {
						continue;
					}
					// 不是这个活动的输入
					if (!objActives[0].toString().equals(objInputFiles[1].toString())) {
						continue;
					}
					str.append(objInputFiles[3].toString()).append("\n");
				}
				if (!"".equals(str.toString())) {
					obj[4] = str.toString().substring(0, str.toString().length() - 1);
				} else {
					obj[4] = "";
				}
				// 输出
				str.setLength(0);
				// 活动文本输出
				if (objActives[7] != null) {
					str.append(objActives[7].toString()).append("\n");
				}
				for (Object[] objOutFiles : listOutFiles) {
					if (objOutFiles == null || objOutFiles[0] == null || objOutFiles[1] == null
							|| objOutFiles[2] == null) {
						continue;
					}
					// 不是这个活动的输出
					if (!objActives[0].toString().equals(objOutFiles[1].toString())) {
						continue;
					}
					str.append(objOutFiles[3].toString()).append("\n");
				}
				if (!"".equals(str.toString())) {
					obj[5] = str.toString().substring(0, str.toString().length() - 1);
				} else {
					obj[5] = "";
				}
			} else {
				obj[4] = "";
				obj[5] = "";
			}

			obj[6] = JecnUtil.objToNumberStr(objActives[10]);
			obj[7] = JecnUtil.objToNumberStr(objActives[11]);

			listResult.add(obj);
		}
		return getList(listResult);
	}

	/**
	 * 活动明显 活动明细数据 0:活动编号 1执行角色 2活动名称 3活动说明 4输入 5 输出
	 * 
	 * @param listActiveShow
	 *            0活动主键ID 1活动编号 2活动名称 3活动说明 4关键活动类型 5关键说明
	 * @param listRoleShow
	 *            角色明细数据 0是角色主键ID 1角色名称 2角色职责
	 * @param listRoleAndActiveRelate
	 *            流程下所有的角色和活动关系表 0是角色主键ID 1是活动主键ID
	 * @param listInputFiles
	 *            0是主键ID 1是活动主键Id 2是文件主键ID 3是文件名称 4是文件类型（0是输入，1是操作手册） 5是文件编号
	 * @param listOutFiles
	 *            流程下所有的活动的输出 0是主键ID 1是活动主键Id 2是文件主键ID 3是文件名称 4是文件编号
	 * @param listRefIndicators
	 *            0主键ID ， 1指标名 ， 2指标值 , 3活动ID
	 * @return
	 */
	public static List<Object[]> nongfushanquan_getActiveShow(List<Object[]> listActiveShow,
			List<Object[]> listRoleShow, List<Object[]> listRoleAndActiveRelate, List<Object[]> listInputFiles,
			List<Object[]> listOutFiles, List<Object[]> listRefIndicators) {
		List<Object[]> listResult = new ArrayList<Object[]>();
		for (Object[] objActives : listActiveShow) {
			if (objActives == null || objActives[0] == null) {
				continue;
			}
			Object[] obj = new Object[9];
			// 关键说明类型
			obj[8] = objActives[4];
			// 关键说明
			obj[7] = objActives[5];
			// 活动ID
			obj[6] = objActives[0];
			// 活动编号
			obj[0] = objActives[1];
			// 执行角色
			for (Object[] objRoleAndActiveRelate : listRoleAndActiveRelate) {
				if (objRoleAndActiveRelate == null || objRoleAndActiveRelate[0] == null
						|| objRoleAndActiveRelate[1] == null) {
					continue;
				}
				if (objActives[0].equals(objRoleAndActiveRelate[1])) {
					for (Object[] objRoleShow : listRoleShow) {
						if (objRoleShow == null || objRoleShow[0] == null) {
							continue;
						}
						if (objRoleAndActiveRelate[0].equals(objRoleShow[0])) {
							obj[1] = objRoleShow[1];
							break;
						}
					}
					break;
				}
			}
			// 活动名称
			obj[2] = objActives[2];
			// 活动说明
			String content = "";
			if (objActives[3] != null) {
				content = objActives[3].toString();
			}
			obj[3] = content;
			// 输入
			StringBuffer str = new StringBuffer();
			for (Object[] objInputFiles : listInputFiles) {
				if (objInputFiles == null || objInputFiles[0] == null || objInputFiles[1] == null
						|| objInputFiles[2] == null || objInputFiles[4] == null) {
					continue;
				}
				// 操作规范
				if ("1".equals(objInputFiles[4].toString())) {
					continue;
				}
				// 不是这个活动的输入
				if (!objActives[0].toString().equals(objInputFiles[1].toString())) {
					continue;
				}
				str.append(objInputFiles[3].toString()).append("\n");
			}
			if (!"".equals(str.toString())) {
				obj[4] = str.toString().substring(0, str.toString().length() - 1);
			} else {
				obj[4] = "";
			}
			// 输出
			str = new StringBuffer();
			for (Object[] objOutFiles : listOutFiles) {
				if (objOutFiles == null || objOutFiles[0] == null || objOutFiles[1] == null || objOutFiles[2] == null) {
					continue;
				}
				// 不是这个活动的输出
				if (!objActives[0].toString().equals(objOutFiles[1].toString())) {
					continue;
				}
				str.append(objOutFiles[3].toString()).append("\n");
			}
			if (!"".equals(str.toString())) {
				obj[5] = str.toString().substring(0, str.toString().length() - 1);
			} else {
				obj[5] = "";
			}

			listResult.add(obj);
		}
		return getList(listResult);
	}

	private static List<Object[]> getList(List<Object[]> listResult) {
		// 外循环控制比较的次数
		for (int i = 0; i < listResult.size(); i++) {
			// 内循环控制比较后移位
			for (int j = listResult.size() - 1; j > i; j--) {
				Object[] oneObjActives = listResult.get(j - 1);
				Object[] twoObjActives = listResult.get(j);
				Object[] objActives = oneObjActives;
				if (oneObjActives[0] == null || twoObjActives[0] == null) {
					continue;
				}
				String oneActivesNumber = oneObjActives[0].toString();
				String twoActivesNumber = twoObjActives[0].toString();

				if (oneActivesNumber.indexOf("-") > 0) {
					oneActivesNumber = oneActivesNumber.substring(oneActivesNumber.lastIndexOf("-") + 1,
							oneActivesNumber.length());
				}

				if (twoActivesNumber.indexOf("-") > 0) {
					twoActivesNumber = twoActivesNumber.substring(twoActivesNumber.lastIndexOf("-") + 1,
							twoActivesNumber.length());
				}
				if (!oneActivesNumber.matches("[0-9]+") || !twoActivesNumber.matches("[0-9]+")) {
					continue;
				}

				if (Long.valueOf(oneActivesNumber).longValue() > Long.valueOf(twoActivesNumber).longValue()) {
					listResult.set(j - 1, twoObjActives);
					listResult.set(j, objActives);
				}
			}
		}
		return listResult;
	}

	/**
	 * 获取时间驱动
	 * 
	 * @author fuzhh Dec 7, 2012
	 * @param jecnFlowBasicInfoT
	 * @return
	 * @throws Exception
	 */
	public static FlowDriverBean getFlowDriverBean(Long flowId, JecnFlowBasicInfoT jecnFlowBasicInfoT,
			JecnFlowBasicInfo jecnFlowBasicInfo, IFlowStructureDao flowStructureDao) throws Exception {
		FlowDriverBean flowDriverBean = new FlowDriverBean();
		if (jecnFlowBasicInfo != null) {
			flowDriverBean.setDriveType(jecnFlowBasicInfo.getDriveType());
			// 驱动规则
			if (jecnFlowBasicInfo.getDriveRules() != null) {
				flowDriverBean.setDriveRules(jecnFlowBasicInfo.getDriveRules());
			} else {
				flowDriverBean.setDriveRules("");
			}
			// 时间驱动
			JecnFlowDriver jecnFlowDriver = flowStructureDao.getJecnFlowDriverByFlowId(flowId);
			if (jecnFlowDriver != null) {
				// 频率
				if (jecnFlowDriver.getFrequency() != null) {
					flowDriverBean.setFrequency(jecnFlowDriver.getFrequency());
				} else {
					flowDriverBean.setFrequency("");
				}
				// 数量
				if (jecnFlowDriver.getQuantity() != null) {
					flowDriverBean.setQuantity(jecnFlowDriver.getQuantity());
					if ("1".equals(jecnFlowDriver.getQuantity())) { // 日
						// 开始时间
						StringBuffer startTime = new StringBuffer();
						if (jecnFlowDriver.getStartTime() != null) {
							String[] startStr = jecnFlowDriver.getStartTime().split(",");
							for (int i = 0; i < startStr.length; i++) {
								String str = startStr[i];
								if (i == 0) {
									startTime.append(str + JecnUtil.getValue("when"));
								} else if (i == 1) {
									startTime.append(str + JecnUtil.getValue("points"));
								} else if (i == 2) {
									startTime.append(str + JecnUtil.getValue("seconds"));
								}
							}
							flowDriverBean.setStartTime(startTime.toString());
						} else {
							flowDriverBean.setStartTime("");
						}
						// 结束时间
						StringBuffer endTime = new StringBuffer();
						if (jecnFlowDriver.getEndTime() != null) {
							String[] endStr = jecnFlowDriver.getEndTime().split(",");
							for (int i = 0; i < endStr.length; i++) {
								String str = endStr[i];
								if (i == 0) {
									endTime.append(str + JecnUtil.getValue("when"));
								} else if (i == 1) {
									endTime.append(str + JecnUtil.getValue("points"));
								} else if (i == 2) {
									endTime.append(str + JecnUtil.getValue("seconds"));
								}
							}
							flowDriverBean.setEndTime(endTime.toString());
						} else {
							flowDriverBean.setEndTime("");
						}
					} else if ("2".equals(jecnFlowDriver.getQuantity())) { // 周
						// 开始时间
						String startTime = "";
						if (jecnFlowDriver.getStartTime() != null) {
							if ("0".equals(jecnFlowDriver.getStartTime())) {
								startTime = JecnUtil.getValue("onMonday");
							} else if ("1".equals(jecnFlowDriver.getStartTime())) {
								startTime = JecnUtil.getValue("onTuesday");
							} else if ("2".equals(jecnFlowDriver.getStartTime())) {
								startTime = JecnUtil.getValue("onWednesda");
							} else if ("3".equals(jecnFlowDriver.getStartTime())) {
								startTime = JecnUtil.getValue("onThursday");
							} else if ("4".equals(jecnFlowDriver.getStartTime())) {
								startTime = JecnUtil.getValue("onFriday");
							} else if ("5".equals(jecnFlowDriver.getStartTime())) {
								startTime = JecnUtil.getValue("onSaturday");
							} else if ("6".equals(jecnFlowDriver.getStartTime())) {
								startTime = JecnUtil.getValue("onSunday");
							}

						}
						flowDriverBean.setStartTime(startTime);
						// 结束时间
						String endTime = "";
						if (jecnFlowDriver.getEndTime() != null) {
							if ("0".equals(jecnFlowDriver.getEndTime())) {
								endTime = JecnUtil.getValue("onMonday");
							} else if ("1".equals(jecnFlowDriver.getEndTime())) {
								endTime = JecnUtil.getValue("onTuesday");
							} else if ("2".equals(jecnFlowDriver.getEndTime())) {
								endTime = JecnUtil.getValue("onWednesda");
							} else if ("3".equals(jecnFlowDriver.getEndTime())) {
								endTime = JecnUtil.getValue("onThursday");
							} else if ("4".equals(jecnFlowDriver.getEndTime())) {
								endTime = JecnUtil.getValue("onFriday");
							} else if ("5".equals(jecnFlowDriver.getEndTime())) {
								endTime = JecnUtil.getValue("onSaturday");
							} else if ("6".equals(jecnFlowDriver.getEndTime())) {
								endTime = JecnUtil.getValue("onSunday");
							}

						}
						flowDriverBean.setEndTime(endTime);
					} else if ("3".equals(jecnFlowDriver.getQuantity())) { // 月
						// 开始时间
						String startTime = "";
						if (jecnFlowDriver.getStartTime() != null) {
							startTime = (Integer.valueOf(jecnFlowDriver.getStartTime()) + 1) + JecnUtil.getValue("day");
						}
						flowDriverBean.setStartTime(startTime);
						// 结束时间
						String endTime = "";
						if (jecnFlowDriver.getEndTime() != null) {
							endTime = (Integer.valueOf(jecnFlowDriver.getEndTime()) + 1) + JecnUtil.getValue("day");
						}
						flowDriverBean.setEndTime(endTime);
					} else if ("4".equals(jecnFlowDriver.getQuantity())) { // 季
						// 开始时间
						String startTime = "";
						if (jecnFlowDriver.getStartTime() != null) {
							startTime = (Integer.valueOf(jecnFlowDriver.getStartTime()) + 1) + JecnUtil.getValue("day");
						}
						flowDriverBean.setStartTime(startTime);
						// 结束时间
						String endTime = "";
						if (jecnFlowDriver.getEndTime() != null) {
							endTime = (Integer.valueOf(jecnFlowDriver.getEndTime()) + 1) + JecnUtil.getValue("day");
						}
						flowDriverBean.setEndTime(endTime);
					} else if ("5".equals(jecnFlowDriver.getQuantity())) { // 年
						// 开始时间
						StringBuffer startTime = new StringBuffer();
						if (jecnFlowDriver.getStartTime() != null) {
							String[] startStr = jecnFlowDriver.getStartTime().split(",");
							for (int i = 0; i < startStr.length; i++) {
								String str = startStr[i];
								if (i == 0) {
									startTime.append((Integer.valueOf(str) + 1) + JecnUtil.getValue("month"));
								} else if (i == 1) {
									startTime.append((Integer.valueOf(str) + 1) + JecnUtil.getValue("day"));
								}
							}
							flowDriverBean.setStartTime(startTime.toString());
						} else {
							flowDriverBean.setStartTime("");
						}
						// 结束时间
						StringBuffer endTime = new StringBuffer();
						if (jecnFlowDriver.getEndTime() != null) {
							String[] endStr = jecnFlowDriver.getEndTime().split(",");
							for (int i = 0; i < endStr.length; i++) {
								String str = endStr[i];
								if (i == 0) {
									endTime.append((Integer.valueOf(str) + 1) + JecnUtil.getValue("month"));
								} else if (i == 1) {
									endTime.append((Integer.valueOf(str) + 1) + JecnUtil.getValue("day"));
								}
							}
							flowDriverBean.setEndTime(endTime.toString());
						} else {
							flowDriverBean.setEndTime("");
						}
					}
				} else {
					flowDriverBean.setQuantity("");
				}
			}
		} else {
			flowDriverBean.setDriveType(jecnFlowBasicInfoT.getDriveType());
			// 驱动规则
			if (jecnFlowBasicInfoT.getDriveRules() != null) {
				flowDriverBean.setDriveRules(jecnFlowBasicInfoT.getDriveRules());
			} else {
				flowDriverBean.setDriveRules("");
			}
			// 时间驱动
			JecnFlowDriverT jecnFlowDriverT = flowStructureDao.getJecnFlowDriverTByFlowId(flowId);
			if (jecnFlowDriverT != null) {
				// 频率
				if (jecnFlowDriverT.getFrequency() != null) {
					flowDriverBean.setFrequency(jecnFlowDriverT.getFrequency());
				} else {
					flowDriverBean.setFrequency("");
				}
				// 数量
				if (jecnFlowDriverT.getQuantity() != null) {
					flowDriverBean.setQuantity(jecnFlowDriverT.getQuantity());
					if ("1".equals(jecnFlowDriverT.getQuantity())) { // 日
						// 开始时间
						StringBuffer startTime = new StringBuffer();
						if (jecnFlowDriverT.getStartTime() != null) {
							String[] startStr = jecnFlowDriverT.getStartTime().split(",");
							for (int i = 0; i < startStr.length; i++) {
								String str = startStr[i];
								if (i == 0) {
									startTime.append(str + JecnUtil.getValue("when"));
								} else if (i == 1) {
									startTime.append(str + JecnUtil.getValue("points"));
								} else if (i == 2) {
									startTime.append(str + JecnUtil.getValue("seconds"));
								}
							}
							flowDriverBean.setStartTime(startTime.toString());
						} else {
							flowDriverBean.setStartTime("");
						}
						// 结束时间
						StringBuffer endTime = new StringBuffer();
						if (jecnFlowDriverT.getEndTime() != null) {
							String[] endStr = jecnFlowDriverT.getEndTime().split(",");
							for (int i = 0; i < endStr.length; i++) {
								String str = endStr[i];
								if (i == 0) {
									endTime.append(str + JecnUtil.getValue("when"));
								} else if (i == 1) {
									endTime.append(str + JecnUtil.getValue("points"));
								} else if (i == 2) {
									endTime.append(str + JecnUtil.getValue("seconds"));
								}
							}
							flowDriverBean.setEndTime(endTime.toString());
						} else {
							flowDriverBean.setEndTime("");
						}
					} else if ("2".equals(jecnFlowDriverT.getQuantity())) { // 周
						// 开始时间
						String startTime = "";
						if (jecnFlowDriverT.getStartTime() != null) {
							if ("0".equals(jecnFlowDriverT.getStartTime())) {
								startTime = JecnUtil.getValue("onMonday");
							} else if ("1".equals(jecnFlowDriverT.getStartTime())) {
								startTime = JecnUtil.getValue("onTuesday");
							} else if ("2".equals(jecnFlowDriverT.getStartTime())) {
								startTime = JecnUtil.getValue("onWednesda");
							} else if ("3".equals(jecnFlowDriverT.getStartTime())) {
								startTime = JecnUtil.getValue("onThursday");
							} else if ("4".equals(jecnFlowDriverT.getStartTime())) {
								startTime = JecnUtil.getValue("onFriday");
							} else if ("5".equals(jecnFlowDriverT.getStartTime())) {
								startTime = JecnUtil.getValue("onSaturday");
							} else if ("6".equals(jecnFlowDriverT.getStartTime())) {
								startTime = JecnUtil.getValue("onSunday");
							}

						}
						flowDriverBean.setStartTime(startTime);
						// 结束时间
						String endTime = "";
						if (jecnFlowDriverT.getEndTime() != null) {
							if ("0".equals(jecnFlowDriverT.getEndTime())) {
								endTime = JecnUtil.getValue("onMonday");
							} else if ("1".equals(jecnFlowDriverT.getEndTime())) {
								endTime = JecnUtil.getValue("onTuesday");
							} else if ("2".equals(jecnFlowDriverT.getEndTime())) {
								endTime = JecnUtil.getValue("onWednesda");
							} else if ("3".equals(jecnFlowDriverT.getEndTime())) {
								endTime = JecnUtil.getValue("onThursday");
							} else if ("4".equals(jecnFlowDriverT.getEndTime())) {
								endTime = JecnUtil.getValue("onFriday");
							} else if ("5".equals(jecnFlowDriverT.getEndTime())) {
								endTime = JecnUtil.getValue("onSaturday");
							} else if ("6".equals(jecnFlowDriverT.getEndTime())) {
								endTime = JecnUtil.getValue("onSunday");
							}

						}
						flowDriverBean.setEndTime(endTime);
					} else if ("3".equals(jecnFlowDriverT.getQuantity())) { // 月
						// 开始时间
						String startTime = "";
						if (jecnFlowDriverT.getStartTime() != null) {
							startTime = (Integer.valueOf(jecnFlowDriverT.getStartTime()) + 1)
									+ JecnUtil.getValue("day");
						}
						flowDriverBean.setStartTime(startTime);
						// 结束时间
						String endTime = "";
						if (jecnFlowDriverT.getEndTime() != null) {
							endTime = (Integer.valueOf(jecnFlowDriverT.getEndTime()) + 1) + JecnUtil.getValue("day");
						}
						flowDriverBean.setEndTime(endTime);
					} else if ("4".equals(jecnFlowDriverT.getQuantity())) { // 季
						// 开始时间
						String startTime = "";
						if (jecnFlowDriverT.getStartTime() != null) {
							startTime = (Integer.valueOf(jecnFlowDriverT.getStartTime()) + 1)
									+ JecnUtil.getValue("day");
						}
						flowDriverBean.setStartTime(startTime);
						// 结束时间
						String endTime = "";
						if (jecnFlowDriverT.getEndTime() != null) {
							endTime = (Integer.valueOf(jecnFlowDriverT.getEndTime()) + 1) + JecnUtil.getValue("day");
						}
						flowDriverBean.setEndTime(endTime);
					} else if ("5".equals(jecnFlowDriverT.getQuantity())) { // 年
						// 开始时间
						StringBuffer startTime = new StringBuffer();
						if (jecnFlowDriverT.getStartTime() != null) {
							String[] startStr = jecnFlowDriverT.getStartTime().split(",");
							for (int i = 0; i < startStr.length; i++) {
								String str = startStr[i];
								if (i == 0) {
									startTime.append((Integer.valueOf(str) + 1) + JecnUtil.getValue("month"));
								} else if (i == 1) {
									startTime.append((Integer.valueOf(str) + 1) + JecnUtil.getValue("day"));
								}
							}
							flowDriverBean.setStartTime(startTime.toString());
						} else {
							flowDriverBean.setStartTime("");
						}
						// 结束时间
						StringBuffer endTime = new StringBuffer();
						if (jecnFlowDriverT.getEndTime() != null) {
							String[] endStr = jecnFlowDriverT.getEndTime().split(",");
							for (int i = 0; i < endStr.length; i++) {
								String str = endStr[i];
								if (i == 0) {
									endTime.append((Integer.valueOf(str) + 1) + JecnUtil.getValue("month"));
								} else if (i == 1) {
									endTime.append((Integer.valueOf(str) + 1) + JecnUtil.getValue("day"));
								}
							}
							flowDriverBean.setEndTime(endTime.toString());
						} else {
							flowDriverBean.setEndTime("");
						}
					}
				} else {
					flowDriverBean.setQuantity("");
				}
			}
		}
		return flowDriverBean;
	}

	/**
	 * 获取文控信息
	 * 
	 * @author fuzhh Nov 9, 2012
	 * @param objList
	 *            存文控信息的集合
	 * @param id
	 *            文控ID
	 * @param docControlDao
	 * @return
	 * @throws Exception
	 */
	public static String getJecnTaskHistoryNew(List<Object[]> objList, Long id, IJecnDocControlDao docControlDao)
			throws Exception {
		// 流程文控信息
		if (id == null) {
			return null;
		}
		JecnTaskHistoryNew jecnTaskHistoryNew = docControlDao.getJecnTaskHistoryNew(id);
		// 评审人数据集合
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String pubTime = "";
		if (jecnTaskHistoryNew != null) {
			/*
			 * if (jecnTaskHistoryNew.getPublishDate() != null) { pubTime =
			 * dateFormat.format(jecnTaskHistoryNew.getPublishDate()); Object[]
			 * objArr1 = new Object[4]; objArr1[0] = "拟稿人"; objArr1[1] =
			 * jecnTaskHistoryNew.getDraftPerson(); objArr1[2] = pubTime;
			 * objArr1[3] = 0; objList.add(objArr1); }
			 */
			for (JecnTaskHistoryFollow jecnTaskHistoryFollow : jecnTaskHistoryNew.getListJecnTaskHistoryFollow()) {
				if (JecnCommon.isNullOrEmtryTrim(jecnTaskHistoryFollow.getName())) {
					continue;
				}
				Object[] objArr = new Object[4];
				// 审核阶段名称
				if (jecnTaskHistoryFollow.getAppellation() != null) {
					objArr[0] = jecnTaskHistoryFollow.getAppellation();
				} else {
					objArr[0] = "";
				}
				// 审核人名称
				if (jecnTaskHistoryFollow.getName() != null) {
					objArr[1] = jecnTaskHistoryFollow.getName();
				} else {
					objArr[1] = "";
				}
				// 日期
				if (jecnTaskHistoryFollow.getApprovalTime() != null) {
					String sDate = dateFormat.format(jecnTaskHistoryFollow.getApprovalTime());
					objArr[2] = sDate;
				} else {
					objArr[2] = pubTime;
				}
				// 审批阶段标识
				if (jecnTaskHistoryFollow.getStageMark() != null) {
					objArr[3] = jecnTaskHistoryFollow.getStageMark();
				} else {
					objArr[3] = "";
				}
				objList.add(objArr);
			}
			return jecnTaskHistoryNew.getVersionId();
		}
		return null;
	}

	/**
	 * 流程相关制度
	 * 
	 * @param list
	 * @return
	 */
	public static List<Object[]> getRelateRules(List<Object[]> list) {
		List<Object[]> listResult = new ArrayList<Object[]>();
		for (Object[] obj : list) {
			if (obj == null || obj[0] == null || obj[1] == null) {
				continue;
			}
			Object[] ruleObj = new Object[3];
			ruleObj[0] = obj[1].toString();
			if (obj[4] != null) {
				ruleObj[1] = JecnCommon.ruleTypeVal(Integer.valueOf(obj[4].toString()));
			}
			if (obj[2] != null) {
				ruleObj[2] = obj[2].toString();
			}
			listResult.add(ruleObj);
		}
		return listResult;
	}

	/**
	 * 流程相关标准
	 * 
	 * @param list
	 * @return
	 */
	public static List<Object[]> getRelateStandards(List<Object[]> list) {
		List<Object[]> listResult = new ArrayList<Object[]>();
		for (Object[] obj : list) {
			if (obj == null || obj[0] == null || obj[1] == null) {
				continue;
			}
			Object[] standardObj = new Object[2];
			standardObj[0] = obj[1].toString();
			// standardObj[1] =
			// JecnCommon.standardTypeVal(Integer.valueOf(obj[3].toString()));
			standardObj[1] = obj[4].toString();
			listResult.add(standardObj);
		}
		return listResult;
	}

	/**
	 * 关键活动明显
	 * 
	 * @param listActiveShow
	 * @return
	 */
	public static List<KeyActivityBean> getListKeyActivityBean(List<Object[]> listActiveShow) {
		List<KeyActivityBean> listKeyActivityBean = new ArrayList<KeyActivityBean>();
		for (Object[] objActives : listActiveShow) {
			if (objActives == null || objActives[0] == null || objActives[4] == null) {
				continue;
			}
			if ("1".equals(objActives[4].toString()) || "2".equals(objActives[4].toString())
					|| "3".equals(objActives[4].toString()) || "4".equals(objActives[4].toString())) {
				KeyActivityBean keyActivityBean = new KeyActivityBean();
				keyActivityBean.setId(Long.valueOf(objActives[0].toString()));
				// 关键活动类型：1为PA,2为KSF,3为KCP，4为KCP(合规)
				keyActivityBean.setActiveKey(objActives[4].toString());
				// 活动编号
				if (objActives[1] != null) {
					keyActivityBean.setActivityNumber(objActives[1].toString());
				} else {
					keyActivityBean.setActivityNumber("");
				}
				// 活动名称
				if (objActives[2] != null) {
					keyActivityBean.setActivityName(objActives[2].toString());
				} else {
					keyActivityBean.setActivityName("");
				}
				// 活动说明
				if (objActives[3] != null) {
					keyActivityBean.setActivityShow(objActives[3].toString());
				} else {
					keyActivityBean.setActivityShow("");
				}
				// 关键说明
				if (objActives[5] != null) {
					keyActivityBean.setActivityShowControl(objActives[5].toString());
				} else {
					keyActivityBean.setActivityShowControl("");
				}
				listKeyActivityBean.add(keyActivityBean);
			}
		}
		return listKeyActivityBean;
	}

	/**
	 * 农夫山泉关键活动明细
	 * 
	 * @param listActiveShow
	 * @return
	 */
	public static List<KeyActivityBean> nongfushanquan_getListKeyActivityBean(List<Object[]> listActiveShow,
			List<Object[]> listRoleAndActiveRelate, List<Object[]> listRoleShow) {
		List<KeyActivityBean> listKeyActivityBean = new ArrayList<KeyActivityBean>();
		for (Object[] objActives : listActiveShow) {
			if (objActives == null || objActives[0] == null || objActives[4] == null) {
				continue;
			}
			if ("1".equals(objActives[4].toString()) || "2".equals(objActives[4].toString())
					|| "3".equals(objActives[4].toString()) || "4".equals(objActives[4].toString())) {
				KeyActivityBean keyActivityBean = new KeyActivityBean();
				keyActivityBean.setId(Long.valueOf(objActives[0].toString()));
				// 对应的活动信息
				keyActivityBean.setActiveInfo(objActives);
				// 关键活动类型：1为PA,2为KSF,3为KCP
				keyActivityBean.setActiveKey(objActives[4].toString());
				// 活动编号
				// 1.农夫山泉把活动编号设置成了执行角色数据
				// 2.活动编号取活动详细的数据
				if (objActives[1] != null && objActives[0] != null) {
					// 0是角色主键ID 1是活动主键ID
					String ruleName = nongfushanquan_getActExcuteRule(listRoleAndActiveRelate, listRoleShow,
							objActives[0].toString());
					keyActivityBean.setActivityNumber(ruleName);
				} else {
					keyActivityBean.setActivityNumber("");
				}
				// 活动名称
				if (objActives[2] != null) {
					keyActivityBean.setActivityName(objActives[2].toString());
				} else {
					keyActivityBean.setActivityName("");
				}
				// 活动说明
				if (objActives[3] != null) {
					keyActivityBean.setActivityShow(objActives[3].toString());
				} else {
					keyActivityBean.setActivityShow("");
				}
				// 关键说明
				if (objActives[5] != null) {
					keyActivityBean.setActivityShowControl(objActives[5].toString());
				} else {
					keyActivityBean.setActivityShowControl("");
				}
				listKeyActivityBean.add(keyActivityBean);
			}
		}
		return listKeyActivityBean;
	}

	/**
	 * 获取活动的执行角色
	 * 
	 * @param listRoleAndActiveRelate
	 *            流程下所有的角色和活动关系表 0是角色主键ID 1是活动主键ID
	 * @param listRoleShow
	 *            // 角色明细数据 0是角色主键ID 1角色名称 2角色职责
	 * @param activesNumber
	 *            活动编号
	 * @return
	 */
	private static String nongfushanquan_getActExcuteRule(List<Object[]> listRoleAndActiveRelate,
			List<Object[]> listRoleShow, String activesNumber) {
		String ruleName = "";
		// 流程下所有的角色和活动关系表 0是角色主键ID 1是活动主键ID
		for (Object[] obj : listRoleAndActiveRelate) {
			if (activesNumber.equals(String.valueOf(obj[1]))) {
				String ruleId = String.valueOf(obj[0]);
				// 角色明细数据 0是角色主键ID 1角色名称 2角色职责
				for (Object[] objRule : listRoleShow) {
					if (ruleId.equals(String.valueOf(objRule[0]))) {
						ruleName = String.valueOf(objRule[1]);
					}
				}
			}
		}
		return ruleName;
	}

	/**
	 * 获得流程记录和流程操作规范
	 * 
	 * @param processRecordList
	 * @param operationTemplateList
	 * @param listInputFiles
	 * @param listOutFiles
	 */
	public static void getFlowFiles(List<Object[]> processRecordList, List<Object[]> operationTemplateList,
			List<Object[]> listInputFiles, List<Object[]> listOutFiles) {
		// 检查有不有重复 输入和输出文件
		List<Long> listRecordListIds = new ArrayList<Long>();
		// 检查有不有重复 操作规范文件
		List<Long> listoperationTemplateListIds = new ArrayList<Long>();

		for (Object[] objOutFiles : listOutFiles) {
			if (objOutFiles == null || objOutFiles[0] == null || objOutFiles[1] == null || objOutFiles[2] == null) {
				continue;
			}
			if (!listRecordListIds.contains(Long.valueOf(objOutFiles[2].toString()))) {
				listRecordListIds.add(Long.valueOf(objOutFiles[2].toString()));
				Object[] obj = new Object[2];
				// 文件编号
				obj[0] = objOutFiles[4];
				// 文件名称
				obj[1] = objOutFiles[3];
				processRecordList.add(obj);
			}
		}

		for (Object[] objInputFiles : listInputFiles) {
			if (objInputFiles == null || objInputFiles[0] == null || objInputFiles[1] == null
					|| objInputFiles[2] == null || objInputFiles[4] == null) {
				continue;
			}

			// 操作规范
			if ("1".equals(objInputFiles[4].toString())) {
				if (!listoperationTemplateListIds.contains(Long.valueOf(objInputFiles[2].toString()))) {
					listoperationTemplateListIds.add(Long.valueOf(objInputFiles[2].toString()));
					Object[] obj = new Object[2];
					// 文件编号
					obj[0] = objInputFiles[5];
					// 文件名称
					obj[1] = objInputFiles[3];
					operationTemplateList.add(obj);
				}
			} else if ("0".equals(objInputFiles[4].toString())) {
				if (!listRecordListIds.contains(Long.valueOf(objInputFiles[2].toString()))) {
					listRecordListIds.add(Long.valueOf(objInputFiles[2].toString()));
					Object[] obj = new Object[2];
					// 文件编号
					obj[0] = objInputFiles[5];
					// 文件名称
					obj[1] = objInputFiles[3];
					processRecordList.add(obj);
				}
			}
		}
	}

	/**
	 * 获得流程记录和流程操作规范
	 * 
	 * @param processRecordList
	 * @param operationTemplateList
	 * @param listInputFiles
	 *            0是主键ID 1是活动主键Id 2是文件主键ID 3是文件名称 4是文件类型（0是输入，1是操作手册） 5是文件编号
	 * @param listOutFiles
	 *            0是主键ID 1是活动主键Id 2是文件主键ID 3是文件名称 4是文件编号
	 * @param listActiveShow
	 *            0活动主键ID 1活动编号 2活动名称 3活动说明 4关键活动类型 5关键说明
	 */
	public static void guangji_getFlowFiles(List<Object[]> processRecordList, List<Object[]> operationTemplateList,
			List<Object[]> listInputFiles, List<Object[]> listOutFiles, List<Object[]> listActiveShow) {
		// 检查有不有重复 输入和输出文件
		List<Long> listRecordListIds = new ArrayList<Long>();
		// 检查有不有重复 操作规范文件
		List<Long> listoperationTemplateListIds = new ArrayList<Long>();
		for (Object[] activeObj : listActiveShow) {
			if (activeObj == null || activeObj[0] == null) {
				continue;
			}

			// 输入和操作规范
			for (Object[] objInputFiles : listInputFiles) {
				if (objInputFiles == null || objInputFiles[0] == null || objInputFiles[1] == null
						|| objInputFiles[2] == null || objInputFiles[4] == null) {
					continue;
				}
				if (activeObj[0].toString().equals(objInputFiles[1].toString())) {
					// 操作规范
					if ("1".equals(objInputFiles[4].toString())) {
						if (!listoperationTemplateListIds.contains(Long.valueOf(objInputFiles[2].toString()))) {
							listoperationTemplateListIds.add(Long.valueOf(objInputFiles[2].toString()));
							Object[] obj = new Object[2];
							// 文件编号
							obj[0] = objInputFiles[5];
							// 文件名称
							obj[1] = objInputFiles[3];
							operationTemplateList.add(obj);
						}
					} else if ("0".equals(objInputFiles[4].toString())) {
						if (!listRecordListIds.contains(Long.valueOf(objInputFiles[2].toString()))) {
							listRecordListIds.add(Long.valueOf(objInputFiles[2].toString()));
							Object[] obj = new Object[2];
							// 文件编号
							obj[0] = objInputFiles[5];
							// 文件名称
							obj[1] = objInputFiles[3];
							processRecordList.add(obj);
						}
					}
				}
			}
			// 输出
			for (Object[] objOutFiles : listOutFiles) {
				if (objOutFiles == null || objOutFiles[0] == null || objOutFiles[1] == null || objOutFiles[2] == null) {
					continue;
				}
				if (activeObj[0].toString().equals(objOutFiles[1].toString())) {
					if (!listRecordListIds.contains(Long.valueOf(objOutFiles[2].toString()))) {
						listRecordListIds.add(Long.valueOf(objOutFiles[2].toString()));
						Object[] obj = new Object[2];
						// 文件编号
						obj[0] = objOutFiles[4];
						// 文件名称
						obj[1] = objOutFiles[3];
						processRecordList.add(obj);
					}
				}
			}
		}

	}

	/**
	 * 获得流程记录和流程操作规范
	 * 
	 * @param processRecordList
	 * @param operationTemplateList
	 * @param listInputFiles
	 * @param listOutFiles
	 */
	public static void nongfushanquan_getFlowFiles(List<Object[]> processRecordList,
			List<Object[]> operationTemplateList, List<Object[]> listInputFiles, List<Object[]> listOutFiles,
			ProcessDownloadBaseBean processDownloadBean) {
		// 检查有不有重复 输入和输出文件
		List<Long> listRecordListIds = new ArrayList<Long>();
		// 检查有不有重复 操作规范文件
		List<Long> listoperationTemplateListIds = new ArrayList<Long>();

		for (Object[] objOutFiles : listOutFiles) {
			if (objOutFiles == null || objOutFiles[0] == null || objOutFiles[1] == null || objOutFiles[2] == null) {
				continue;
			}
			if (!listRecordListIds.contains(Long.valueOf(objOutFiles[2].toString()))) {
				listRecordListIds.add(Long.valueOf(objOutFiles[2].toString()));
				Object[] obj = new Object[2];
				// 文件编号
				obj[0] = objOutFiles[4];
				// 文件名称
				obj[1] = objOutFiles[3];
				processRecordList.add(obj);
			}
		}

		for (Object[] objInputFiles : listInputFiles) {
			if (objInputFiles == null || objInputFiles[0] == null || objInputFiles[1] == null
					|| objInputFiles[2] == null || objInputFiles[4] == null) {
				continue;
			}

			// 操作规范
			if ("1".equals(objInputFiles[4].toString())) {
				Object[] obj = new Object[4];
				// 文件编号
				obj[0] = objInputFiles[5];
				// 文件名称
				obj[1] = objInputFiles[3];
				// FIXME 农夫山泉 对应的活动ID
				obj[2] = objInputFiles[1];
				// 责任部门
				obj[3] = objInputFiles[6];
				processDownloadBean.addOperationTemplateListNF(obj);
				if (!listoperationTemplateListIds.contains(Long.valueOf(objInputFiles[2].toString()))) {
					listoperationTemplateListIds.add(Long.valueOf(objInputFiles[2].toString()));
					operationTemplateList.add(obj);
				}
			} else if ("0".equals(objInputFiles[4].toString())) {
				if (!listRecordListIds.contains(Long.valueOf(objInputFiles[2].toString()))) {
					listRecordListIds.add(Long.valueOf(objInputFiles[2].toString()));
					Object[] obj = new Object[2];
					// 文件编号
					obj[0] = objInputFiles[5];
					// 文件名称
					obj[1] = objInputFiles[3];
					processRecordList.add(obj);
				}
			}
		}
	}

	/**
	 * 角色信息集合 0 角色名称 1 岗位名称 2 角色职责
	 * 
	 * @param listRoleShow
	 * @param roleRelatePos
	 * @param roleRelatePosGroup
	 * @return
	 */
	public static List<Object[]> getRoleShow(List<Object[]> listRoleShow, List<Object[]> roleRelatePos,
			List<Object[]> roleRelatePosGroup) {

		List<Object[]> roleList = new ArrayList<Object[]>();
		for (Object[] objRoleShow : listRoleShow) {
			if (objRoleShow == null || objRoleShow[0] == null) {
				continue;
			}

			Object[] obj = new Object[3];
			// 角色名称
			obj[0] = objRoleShow[1];
			// 岗位名称
			List<Long> listIds = new ArrayList<Long>();
			StringBuffer str = new StringBuffer();
			for (Object[] objPos : roleRelatePos) {
				if (objPos == null || objPos[0] == null || objPos[1] == null) {
					continue;
				}
				if (objRoleShow[0].toString().equals(objPos[0].toString())) {
					if (!listIds.contains(Long.valueOf(objPos[1].toString()))) {
						listIds.add(Long.valueOf(objPos[1].toString()));
					}
					str.append(objPos[2].toString()).append("/");
				}
			}
			if (JecnConfigTool.processFileShowPostionType()) {// 显示岗位名称-（岗位组显示岗位组名称）
				for (Object[] objPos : roleRelatePosGroup) {
					if (objPos == null || objPos[0] == null || objPos[1] == null) {
						continue;
					}
					if (objRoleShow[0].toString().equals(objPos[0].toString())) {
						str.append(objPos[1].toString()).append("/");
					}
				}
			} else {
				for (Object[] objPos : roleRelatePosGroup) {
					if (objPos == null || objPos[0] == null || objPos[1] == null) {
						continue;
					}
					if (objRoleShow[0].toString().equals(objPos[0].toString())) {
						if (!listIds.contains(Long.valueOf(objPos[1].toString()))) {
							listIds.add(Long.valueOf(objPos[1].toString()));
						}
						str.append(objPos[2].toString()).append("/");
					}
				}
			}

			if (!"".equals(str.toString())) {
				obj[1] = str.toString().substring(0, str.toString().length() - 1);
			} else {
				obj[1] = "";
			}
			// 角色职责
			obj[2] = objRoleShow[2];
			roleList.add(obj);
		}
		return roleList;
	}

	/**
	 * 获得相关流程
	 * 
	 * @param listRelateFlows
	 * @return
	 */
	public static List<RelatedProcessBean> getRelatedProcessBeanList(List<Object[]> listRelateFlows) {
		List<RelatedProcessBean> listResult = new ArrayList<RelatedProcessBean>();
		for (Object[] obj : listRelateFlows) {
			if (obj == null || obj[0] == null) {
				continue;
			}
			RelatedProcessBean relatedProcessBean = new RelatedProcessBean();
			// 接口流程类型
			relatedProcessBean.setLinecolor(obj[0].toString());
			// 流程编号
			if (obj[1] != null) {
				relatedProcessBean.setFlowNumber(obj[1].toString());
			} else {
				relatedProcessBean.setFlowNumber("");
			}
			// 流程名称
			if (obj[2] != null) {
				relatedProcessBean.setFlowName(obj[2].toString());
			} else {
				relatedProcessBean.setFlowName("");
			}
			listResult.add(relatedProcessBean);
		}
		return listResult;
	}

	/**
	 * 
	 * 农夫山泉操作说明下载专用
	 * 
	 * 获得相关流程
	 * 
	 * @param listRelateFlows
	 * @return
	 */
	public static List<RelatedProcessBean> getRelatedProcessBeanListNFSQ(List<Object[]> listRelateFlows) {
		List<RelatedProcessBean> listResult = new ArrayList<RelatedProcessBean>();
		for (Object[] obj : listRelateFlows) {
			// 类型、编号、名称、责任部门
			if (obj == null || obj.length != 4) {
				continue;
			}
			RelatedProcessBean relatedProcessBean = new RelatedProcessBean();
			// 接口流程类型
			relatedProcessBean.setLinecolor(obj[0].toString());
			// 流程编号
			if (obj[1] != null) {
				relatedProcessBean.setFlowNumber(obj[1].toString());
			} else {
				relatedProcessBean.setFlowNumber("");
			}
			// 流程名称
			if (obj[2] != null) {
				relatedProcessBean.setFlowName(obj[2].toString());
			} else {
				relatedProcessBean.setFlowName("");
			}
			// 责任部门
			if (obj[3] != null) {
				relatedProcessBean.setDutyOrg(obj[3].toString());
			} else {
				relatedProcessBean.setDutyOrg("");
			}

			listResult.add(relatedProcessBean);
		}
		return listResult;
	}

	/**
	 * 保隆操作说明下载 数据添加
	 * 
	 * @author fuzhh 2013-6-24
	 * @param flowId
	 * @param processDownload
	 * @param docControlDao
	 * @param processBasicDao
	 */
	public static void blProcessDownloadBean(Long flowId, ProcessDownloadBean processDownload,
			IJecnDocControlDao docControlDao, IProcessBasicInfoDao processBasicDao) {
		// 获取文控信息
		List<JecnTaskHistoryNew> taskHistoryNewList;
		List<Object[]> documentList = new ArrayList<Object[]>();
		// 流程拟制人名称
		String fictionPeopleName = "";
		try {
			// 流程属性
			ProcessAttributeBean processAttributeBean = new ProcessAttributeBean();
			// 责任人ID
			Object[] obj = processBasicDao.getFlowRelateOrgIds(flowId, false);
			if (obj != null && obj[0] != null && obj[1] != null) {
				// 责任部门Id
				processAttributeBean.setDutyOrgId(Long.valueOf(obj[0].toString()));
				// 责任部门
				processAttributeBean.setDutyOrgName(obj[1].toString());
			}

			// 流程责任人
			if (processDownload.getDutyUserType() != null && processDownload.getDutyUserId() != null) {
				// 责任人类型
				processAttributeBean.setDutyUserType(processDownload.getDutyUserType().intValue());
				// 责任人ID
				processAttributeBean.setDutyUserId(processDownload.getDutyUserId());
				// 类型为0是用户 1为岗位
				if (processDownload.getDutyUserType().intValue() == 0) {
					// 获取用户名称
					String userName = processBasicDao.getUserName(processDownload.getDutyUserId());
					processAttributeBean.setDutyUserName(userName);
				} else if (processDownload.getDutyUserType().intValue() == 1) {
					// 获取岗位名称
					String posName = processBasicDao.getPosName(processDownload.getDutyUserId());
					processAttributeBean.setDutyUserName(posName);
				}
			}
			// 流程监护人
			if (processDownload.getGuardianId() != null) {
				// 监护人ID
				processAttributeBean.setGuardianId(processDownload.getGuardianId());
				// 获取用户名称
				String userName = processBasicDao.getUserName(processDownload.getGuardianId());
				processAttributeBean.setGuardianName(userName);
			}
			// 流程拟制人
			if (processDownload.getFictionPeopleId() != null) {
				// 拟制人ID
				processAttributeBean.setFictionPeopleId(processDownload.getFictionPeopleId());
				// 获取用户名称
				fictionPeopleName = processBasicDao.getUserName(processDownload.getFictionPeopleId());
				processAttributeBean.setFictionPeopleName(fictionPeopleName);
			}
			processDownload.setResponFlow(processAttributeBean);

			taskHistoryNewList = docControlDao.getTaskHistoryNewListById(flowId);
			// 最大的发布日期
			Date releaseDate = null;
			for (JecnTaskHistoryNew taskHistoryNew : taskHistoryNewList) {
				List<JecnTaskHistoryFollow> JecnTaskHistoryFollow = docControlDao
						.getJecnTaskHistoryFollowList(taskHistoryNew.getId());
				// 阶段名+各阶段评审人
				String historyFollo = "";
				for (JecnTaskHistoryFollow taskHistoryFollow : JecnTaskHistoryFollow) {
					if (!JecnCommon.isNullOrEmtryTrim(taskHistoryFollow.getName())) {
						historyFollo += taskHistoryFollow.getAppellation() + taskHistoryFollow.getName() + "\n";
					}
				}
				SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss ");
				String date = formatter.format(taskHistoryNew.getPublishDate());
				if (releaseDate == null) {
					releaseDate = taskHistoryNew.getPublishDate();
				} else {
					boolean flag = releaseDate.before(taskHistoryNew.getPublishDate());
					if (flag) {
						releaseDate = taskHistoryNew.getPublishDate();
					}
				}
				Object[] object = new Object[] { taskHistoryNew.getVersionId(), taskHistoryNew.getModifyExplain(),
						date, fictionPeopleName, historyFollo };
				documentList.add(object);
			}
			// TODO
			// 发布日期 & 生效日期
			if (releaseDate != null) {
				SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm");
				String dateString = formatter.format(releaseDate);
				processDownload.setReleaseDate(dateString);

				// 使用默认时区和语言环境获得一个日历。
				Calendar cal = Calendar.getInstance();
				cal.setTime(releaseDate);
				cal.add(Calendar.DATE, 15);
				Date effectiveDate = cal.getTime();
				String effectiveStr = formatter.format(effectiveDate);
				processDownload.setEffectiveDate(effectiveStr);
			} else {
				processDownload.setReleaseDate("");
				processDownload.setEffectiveDate("");
			}
			processDownload.setDocumentList(documentList);
		} catch (Exception e) {
			log.error("保隆下载获取文控信息错误！", e);
		}
	}

	/**
	 * 操作说明下载 数据添加
	 * 
	 * @author fuzhh 2013-6-24
	 * @param flowId
	 * @param processDownload
	 * @param docControlDao
	 * @param processBasicDao
	 */
	public static void processDownloadBean(Long flowId, ProcessDownloadBean processDownload,
			IJecnDocControlDao docControlDao, IProcessBasicInfoDao processBasicDao) {
		// 获取文控信息
		List<Object[]> documentList = new ArrayList<Object[]>();
		try {
			processDownload.setResponFlow(getProcessAttributeBean(flowId, processDownload.getDutyUserType(),
					processDownload.getDutyUserId(), processDownload.getGuardianId(), processDownload
							.getFictionPeopleId(), processDownload.getCreateName(), processBasicDao));

			Date releaseDate = getTaskHistoryNewList(flowId, docControlDao, documentList, false);
			processDownload.setDocumentList(documentList);
			// TODO
			// 发布日期 & 生效日期
			if (releaseDate != null) {
				SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm");
				String dateString = formatter.format(releaseDate);
				processDownload.setReleaseDate(dateString);

				// 使用默认时区和语言环境获得一个日历。
				Calendar cal = Calendar.getInstance();
				cal.setTime(releaseDate);
				cal.add(Calendar.DATE, 15);
				Date effectiveDate = cal.getTime();
				String effectiveStr = formatter.format(effectiveDate);
				processDownload.setEffectiveDate(effectiveStr);
			} else {
				processDownload.setReleaseDate("");
				processDownload.setEffectiveDate("");
			}
		} catch (Exception e) {
			log.error("下载获取文控信息错误！", e);
		}
	}

	/**
	 * 获取流程责任属性
	 * 
	 * @author fuzhh 2014-1-14
	 * @param flowId
	 * @param dutyUserType
	 *            责任人类型
	 * @param dutyUserId
	 *            责任人id
	 * @param guardianId
	 *            监护人ID
	 * @param createName
	 *            流程创建人
	 * @param processBasicDao
	 * @throws Exception
	 */
	public static ProcessAttributeBean getProcessAttributeBean(Long flowId, Long dutyUserType, Long dutyUserId,
			Long guardianId, Long fictionPeopleId, String createName, IProcessBasicInfoDao processBasicDao)
			throws Exception {
		// 流程属性
		ProcessAttributeBean processAttributeBean = new ProcessAttributeBean();
		Object[] obj = processBasicDao.getFlowRelateOrgIds(flowId, false);
		if (obj != null && obj[0] != null && obj[1] != null) {
			// 责任部门Id
			processAttributeBean.setDutyOrgId(Long.valueOf(obj[0].toString()));
			// 责任部门
			processAttributeBean.setDutyOrgName(obj[1].toString());
		}

		// 流程责任人
		if (dutyUserType != null && dutyUserId != null) {
			// 责任人类型
			processAttributeBean.setDutyUserType(dutyUserType.intValue());
			// 责任人ID
			processAttributeBean.setDutyUserId(dutyUserId);
			// 类型为0是用户 1为岗位
			if (dutyUserType.intValue() == 0) {
				// 获取用户名称
				String userName = processBasicDao.getUserName(dutyUserId);
				List<String> list = processBasicDao.getPosNameByUserId(dutyUserId);
				String posName = "";
				for (String str : list) {
					posName = posName + str + "/";
				}
				if (posName.indexOf("/") != -1) {
					posName = posName.substring(0, posName.lastIndexOf("/"));
				}
				processAttributeBean.setDutyUserName(userName);
				processAttributeBean.setDutuUserPosName(posName);
			} else if (dutyUserType.intValue() == 1) {
				// 获取岗位名称
				String posName = processBasicDao.getPosName(dutyUserId);
				processAttributeBean.setDutyUserName(posName);
				processAttributeBean.setDutuUserPosName(posName);
			}
		}
		// 流程监护人
		if (guardianId != null) {
			// 监护人ID
			processAttributeBean.setGuardianId(guardianId);
			// 获取用户名称
			String userName = processBasicDao.getUserName(guardianId);
			processAttributeBean.setGuardianName(userName);
		}
		// 流程拟制人
		if (fictionPeopleId != null) {
			// 拟制人ID
			processAttributeBean.setFictionPeopleId(fictionPeopleId);
			// 获取用户名称
			String fictionPeopleName = processBasicDao.getUserName(fictionPeopleId);
			// 获取岗位名称
			List<String> list = processBasicDao.getPosNameByUserId(fictionPeopleId);
			String posName = "";
			for (String str : list) {
				posName = posName + str + "/";
			}
			if (posName.indexOf("/") != -1) {
				posName = posName.substring(0, posName.lastIndexOf("/"));
			}
			processAttributeBean.setFictionPeopleName(fictionPeopleName);
			processAttributeBean.setFictionPeoplePosName(posName);
		}
		// 科大讯飞 特殊处理
		iflytekUsers(processAttributeBean);
		
		return processAttributeBean;
	}
	
	private static void iflytekUsers(ProcessAttributeBean processAttributeBean) {
		if (!JecnConfigTool.isIflytekLogin()) {
			return;
		}

		List<String> loginNames = new ArrayList<String>();
		if (StringUtils.isNotBlank(processAttributeBean.getFictionPeopleName())) {
			loginNames.add(processAttributeBean.getFictionPeopleName());
		}
		if (StringUtils.isNotBlank(processAttributeBean.getGuardianName())) {
			loginNames.add(processAttributeBean.getGuardianName());
		}
		if (StringUtils.isNotBlank(processAttributeBean.getDutyUserName())) {
			loginNames.add(processAttributeBean.getDutyUserName());
		}

		if (loginNames.isEmpty()) {
			return;
		}
		Map<String, String> userMap = JecnIflytekSyncUserEnum.INSTANCE.getMapUserName(loginNames);
		// 流程拟制人
		String fictionPeopleName = userMap.get(processAttributeBean.getFictionPeopleName());
		processAttributeBean.setFictionPeopleName(StringUtils.isBlank(fictionPeopleName) ? processAttributeBean
				.getFictionPeopleName() : fictionPeopleName);

		// 流程监护人
		String guardianName = userMap.get(processAttributeBean.getGuardianName());
		processAttributeBean.setGuardianName(StringUtils.isBlank(guardianName) ? processAttributeBean.getGuardianName()
				: guardianName);

		// 流程责任人
		String dutyUserName = userMap.get(processAttributeBean.getDutyUserName());
		processAttributeBean.setDutyUserName(StringUtils.isBlank(dutyUserName) ? processAttributeBean.getDutyUserName()
				: dutyUserName);
	}

	/**
	 * 获取文控信息
	 * 
	 * @author fuzhh 2014-1-14
	 * @param flowId
	 *            流程IDs
	 * @param docControlDao
	 * @param isWrap
	 *            true 为页面显示换行
	 * @throws Exception
	 */
	public static Date getTaskHistoryNewList(Long flowId, IJecnDocControlDao docControlDao,
			List<Object[]> documentList, boolean isWrap) throws Exception {
		List<JecnTaskHistoryNew> taskHistoryNewList = docControlDao.getTaskHistoryNewListById(flowId);
		// 最大的发布日期
		Date releaseDate = null;
		for (JecnTaskHistoryNew taskHistoryNew : taskHistoryNewList) {
			List<JecnTaskHistoryFollow> JecnTaskHistoryFollow = docControlDao
					.getJecnTaskHistoryFollowList(taskHistoryNew.getId());
			// 阶段名+各阶段评审人
			String historyFollo = "";
			for (JecnTaskHistoryFollow taskHistoryFollow : JecnTaskHistoryFollow) {
				if (!JecnCommon.isNullOrEmtryTrim(taskHistoryFollow.getName())) {
					if (isWrap) {
						historyFollo += taskHistoryFollow.getAppellation() + ":" + taskHistoryFollow.getName() + "<br>";
					} else {
						historyFollo += taskHistoryFollow.getAppellation() + ":" + taskHistoryFollow.getName() + "\n";
					}
				}
			}
			SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss ");
			String date = formatter.format(taskHistoryNew.getPublishDate());
			if (releaseDate == null) {
				releaseDate = taskHistoryNew.getPublishDate();
			} else {
				boolean flag = releaseDate.before(taskHistoryNew.getPublishDate());
				if (flag) {
					releaseDate = taskHistoryNew.getPublishDate();
				}
			}
			Object[] object = null;
			if (isWrap) {
				object = new Object[] {
						taskHistoryNew.getVersionId(),
						taskHistoryNew.getModifyExplain() == null ? "" : taskHistoryNew.getModifyExplain().replaceAll(
								"\n", "<br>").replaceAll(" ", "&nbsp;"), date, taskHistoryNew.getDraftPerson(),
						historyFollo };
			} else {
				object = new Object[] { taskHistoryNew.getVersionId(), taskHistoryNew.getModifyExplain(), date,
						taskHistoryNew.getDraftPerson(), historyFollo };
			}
			documentList.add(object);
		}
		return releaseDate;
	}

	/**
	 * 南京石化活动明细数据
	 * 
	 * @author fuzhh 2013-6-24
	 * @param listActiveShow
	 *            0活动主键ID 1活动编号 2活动名称 3活动说明 4关键活动类型 5关键说明 6对应内控矩阵风险点 7对应标准条款
	 * @param listRoleShow
	 *            角色明细数据 0是角色主键ID 1角色名称 2角色职责
	 * @param listRoleAndActiveRelate
	 *            流程下所有的角色和活动关系表 0是角色主键ID 1是活动主键ID
	 */
	public static List<Object[]> njshProcessDownloadBean(List<Object[]> listActiveShow, List<Object[]> listRoleShow,
			List<Object[]> listRoleAndActiveRelate) {
		List<Object[]> listResult = new ArrayList<Object[]>();
		for (Object[] objActives : listActiveShow) {
			if (objActives == null || objActives[0] == null) {
				continue;
			}
			Object[] obj = new Object[8];
			// 活动编号
			obj[0] = objActives[1];
			// 执行角色
			for (Object[] objRoleAndActiveRelate : listRoleAndActiveRelate) {
				if (objRoleAndActiveRelate == null || objRoleAndActiveRelate[0] == null
						|| objRoleAndActiveRelate[1] == null) {
					continue;
				}
				if (objActives[0].equals(objRoleAndActiveRelate[1])) {
					for (Object[] objRoleShow : listRoleShow) {
						if (objRoleShow == null || objRoleShow[0] == null) {
							continue;
						}
						if (objRoleAndActiveRelate[0].equals(objRoleShow[0])) {
							obj[1] = objRoleShow[1];
							break;
						}
					}
					break;
				}
			}
			// 活动名称
			obj[2] = objActives[2];
			// 活动说明
			String content = "";
			if (objActives[3] != null) {
				content = objActives[3].toString();
			}
			obj[3] = content;

			// 对应内控矩阵风险点
			String innerControlRisk = "";
			if (objActives[6] != null) {
				innerControlRisk = objActives[6].toString();
			}
			obj[4] = innerControlRisk;

			// 对应标准条款
			String standardConditions = "";
			if (objActives[7] != null) {
				standardConditions = objActives[7].toString();
			}
			obj[5] = standardConditions;
			obj[6] = objActives[10];
			obj[7] = objActives[11];
			listResult.add(obj);
		}
		return getList(listResult);
	}

	public static TaskEmailParams getTaskEmailParams(boolean isPub, Long flowId, IFlowStructureDao flowStructureDao,
			IProcessBasicInfoDao processBasicDao) throws Exception {

		TaskEmailParams taskEmailParams = new TaskEmailParams();
		if (isPub) {
			JecnFlowBasicInfo jecnFlowBasicInfo = flowStructureDao.findJecnFlowBasicInfoById(flowId);
			taskEmailParams.setFlowPurpose(jecnFlowBasicInfo.getFlowPurpose());
			taskEmailParams.setApplicability(jecnFlowBasicInfo.getApplicability());
		} else {
			JecnFlowBasicInfoT jecnFlowBasicInfo = processBasicDao.get(flowId);
			taskEmailParams.setFlowPurpose(jecnFlowBasicInfo.getFlowPurpose());
			taskEmailParams.setApplicability(jecnFlowBasicInfo.getApplicability());
		}

		ProcessAttributeBean processAttributeBean = new ProcessAttributeBean();
		taskEmailParams.setProcessAttribute(processAttributeBean);
		Object[] obj = processBasicDao.getFlowRelateOrgIds(flowId, isPub);
		if (obj != null && obj[0] != null && obj[1] != null) {
			// 责任部门Id
			processAttributeBean.setDutyOrgId(Long.valueOf(obj[0].toString()));
			// 责任部门
			processAttributeBean.setDutyOrgName(obj[1].toString());
		}
		return taskEmailParams;
	}

}
