package com.jecn.webservice.cetc10;

public class Cetc10TaskBean {
	/** 任务名称 +审批 */
	private String title;
	private String url;
	private String time;
	private String source = "EPROS";
	private String name;
	private String loginName;
	private String isProcessed;
	/** 任务说明 */
	private String summery;
	private String taskid;

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getTime() {
		return time;
	}

	public void setTime(String time) {
		this.time = time;
	}

	public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = source;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLoginName() {
		return loginName;
	}

	public void setLoginName(String loginName) {
		this.loginName = loginName;
	}

	public String getIsProcessed() {
		return isProcessed;
	}

	public void setIsProcessed(String isProcessed) {
		this.isProcessed = isProcessed;
	}

	public String getSummery() {
		return summery;
	}

	public void setSummery(String summery) {
		this.summery = summery;
	}

	public String getTaskid() {
		return taskid;
	}

	public void setTaskid(String taskid) {
		this.taskid = taskid;
	}

	@Override
	public String toString() {
		return "title = " + title + " loginName = " + loginName + " name = " + name + " url = " + url
				+ " isProcessed = " + isProcessed + " taskid = " + taskid + " summery = " + summery + " time = " + time
				+ " source = " + source;
	}

}
