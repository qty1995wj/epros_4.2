package com.jecn.epros.bean.authetication;

import java.io.Serializable;

public class AuthenticationRequest implements Serializable{

    private String username;

    public AuthenticationRequest() {
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}
