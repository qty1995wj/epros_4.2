package com.jecn.epros.server.dao.define.impl;

import java.util.List;

import com.jecn.epros.server.bean.define.TermDefinitionLibrary;
import com.jecn.epros.server.common.AbsBaseDao;
import com.jecn.epros.server.common.JecnCommonSqlTPath;
import com.jecn.epros.server.dao.define.ITermDefinitionDao;

public class TermDefinitionDaoImpl extends AbsBaseDao<TermDefinitionLibrary, Long> implements ITermDefinitionDao {

	@Override
	public List<Object[]> getRoleAuthChilds(long pId, Long projectId, long userId) {
		String sql = "SELECT DISTINCT T.ID,T.NAME,T.PARENT_ID,T.IS_DIR,"
				+ "    CASE WHEN SUB.ID IS NULL THEN 0 ELSE 1 END AS COUNT," + "T.SORT_ID,T.INSTRUCTIONS"
				+ "    FROM TERM_DEFINITION_LIBRARY T"
				+ "    LEFT JOIN TERM_DEFINITION_LIBRARY SUB ON SUB.PARENT_ID=T.ID";
		sql += JecnCommonSqlTPath.INSTANCE.getRoleAuthChildsSqlCommon(8, userId, projectId);
		sql += "    WHERE T.PARENT_ID=? ORDER BY T.SORT_ID";
		List<Object[]> list = this.listNativeSql(sql, pId);
		return list;
	}

}
