package com.jecn.epros.security;

import com.jecn.epros.domain.EnumClass;
import com.jecn.epros.domain.security.AuthorityNodesData;
import com.jecn.epros.service.TreeNodeUtils;

import java.util.Set;

/**
 * 权限验证处理类
 * Created by admin on 2017/5/31.
 */
public class SecurityValidatorData {
    public static boolean process(Long id) {
        if (AuthenticatedUserUtil.isAdmin()) {
            return true;
        }
        if (AuthenticatedUserUtil.isFlowAdmin()) {// 流程节点下admin权限
            return true;
        }
        if (isOperationNode(id, EnumClass.ResourceNodeEnum.FLOW)) {//有操作权限
            return true;
        }
        return false;
    }


    public static boolean processMap(Long id) {
        if (AuthenticatedUserUtil.isAdmin()) {
            return true;
        }
        if (AuthenticatedUserUtil.isFlowMapAdmin()) {// 流程节点下admin权限
            return true;
        }
        if (isOperationNode(id, EnumClass.ResourceNodeEnum.FLOW)) {//有操作权限
            return true;
        }
        return false;
    }


    public static boolean org(Long id) {
        if (AuthenticatedUserUtil.isAdmin()) {
            return true;
        }
        if (AuthenticatedUserUtil.isOrgAdmin()) {// 流程节点下admin权限
            return true;
        }

        if (isOperationNode(id, EnumClass.ResourceNodeEnum.ORGANIZATION)) {//有操作权限
            return true;
        }
        return false;
    }

    public static boolean position(Long id) {
        if (AuthenticatedUserUtil.isAdmin()) {
            return true;
        }
        if (AuthenticatedUserUtil.isStandardAdmin()) {// 流程节点下admin权限
            return true;
        }
        if (isOperationNode(id, EnumClass.ResourceNodeEnum.POST)) {//有操作权限
            return true;
        }
        return false;
    }

    public static boolean rule(Long id) {
        if (AuthenticatedUserUtil.isAdmin()) {
            return true;
        }
        // 是不是查阅所有文件的管理员
        if (AuthenticatedUserUtil.isRuleAdmin()) {
            return true;
        }
        if (isOperationNode(id, EnumClass.ResourceNodeEnum.RULE)) {//有操作权限
            return true;
        }
        return false;
    }

    public static boolean standard(Long id) {
        if (AuthenticatedUserUtil.isAdmin()) {
            return true;
        }
        // 是不是查阅所有文件的管理员
        if (AuthenticatedUserUtil.isStandardAdmin()) {
            return true;
        }
        if (isOperationNode(id, EnumClass.ResourceNodeEnum.STANDARD)) {//有操作权限
            return true;
        }
        return false;
    }

    public static boolean file(Long id) {
        if (AuthenticatedUserUtil.isAdmin()) {
            return true;
        }
        // 是不是查阅所有文件的管理员
        if (AuthenticatedUserUtil.isFileAdmin()) {
            return true;
        }
        if (isOperationNode(id, EnumClass.ResourceNodeEnum.FILE)) {//有操作权限
            return true;
        }
        return false;
    }

    /**
     * 验证节点是否有操作权限
     *
     * @param id
     * @param nodeEnum
     * @return true:有该节点的操作权限
     */
    private static boolean isOperationNode(Long id, EnumClass.ResourceNodeEnum nodeEnum) {
        AuthorityNodesData nodesData = AuthenticatedUserUtil.getPrincipal().getDataAccessAuthorityBean().getNodesData();
        if (nodesData == null) {
            return false;
        }
        Set<Long> operationIds = getOperationIdsByResourceNode(nodeEnum, nodesData);
        if (operationIds.contains(id)) {
            return true;
        }
        return false;
    }

    /**
     * 验证节点是否有下载权限
     *
     * @param id
     * @param nodeEnum
     * @return true:有该节点的操作权限
     */
    public static boolean isDownLoadNode(Long id, EnumClass.ResourceNodeEnum nodeEnum) {
        AuthorityNodesData nodesData = AuthenticatedUserUtil.getPrincipal().getDataAccessAuthorityBean().getNodesData();
        if (nodesData == null) {
            return false;
        }
        Set<Long> downLoadIds = getDownLoadIdsByResourceNode(nodeEnum, nodesData);
        if (downLoadIds.contains(id)) {
            return true;
        }
        return false;
    }

    /**
     * 权限控制
     *
     * @param id
     * @param type
     * @return
     */
    public static boolean resource(Long id, TreeNodeUtils.NodeType type, String from) {
        if (AuthenticatedUserUtil.isAdmin()) {
            return true;
        }
        switch (type) {
            case process:
                return process(id);
            case processFile:
            case processMap:
                return processMap(id);

            case ruleFile:
            case ruleModelFile:
                if (isOpenValidteByFromType(type, from)) {// 判断文件所属资源，是否公开相关文件
                    return true;
                }
                return rule(id);
            case file:
                if (isOpenValidteByFromType(type, from)) {// 判断文件所属资源，是否公开相关文件
                    return true;
                }
                return file(id);
            case standard:
                //case standardClause:
                //case standardClauseRequire:
                //case standardProcess:
                //case standardProcessMap:
                if (isOpenValidteByFromType(type, from)) {// 判断文件所属资源，是否公开相关文件
                    return true;
                }
                return standard(id);
            case organization:
                return org(id);
            case position:
                return position(id);
            default:
                return true;
        }
    }

    /**
     * 流程、架构、制度：相关文件是否公开
     *
     * @param type 访问的资源
     * @param from (流程、架构、制度) 访问资源起始位置
     * @return
     */
    private static boolean isOpenValidteByFromType(TreeNodeUtils.NodeType type, String from) {
        switch (type) {
            case ruleFile:
            case ruleModelFile:
                if ("process".equals(from)) {
                    return JecnContants.isOpenFlowRelatedRule;
                } else if ("processMap".equals(from)) {
                    return JecnContants.isOpenArchitectureRelatedRule;
                }
                return false;
            case file:
                if ("process".equals(from)) {
                    return JecnContants.isOpenFlowRelatedFile;
                } else if ("ruleFile".equals(from)) {
                    return JecnContants.isOpenRuleRelatedFile;
                } else if ("processMap".equals(from)) {
                    return JecnContants.isOpenArchitectureRelatedFile;
                }
                return false;
            case standard:
                //case standardClause:
                //case standardClauseRequire:
                if ("process".equals(from)) {
                    return JecnContants.isOpenFlowRelatedStandard;
                } else if ("ruleFile".equals(from)) {
                    return JecnContants.isOpenRuleRelatedStandard;
                }
                return false;
            default:
                return false;
        }
    }

    private static Set<Long> getOperationIdsByResourceNode(EnumClass.ResourceNodeEnum nodeEnum, AuthorityNodesData nodesData) {
        Set<Long> operationIds = null;
        switch (nodeEnum) {
            case FLOW:
                operationIds = nodesData.getProcessNode().getOperationIds();
                break;
            case RULE:
                operationIds = nodesData.getRuleNode().getOperationIds();
                break;
            case FILE:
                operationIds = nodesData.getFileNode().getOperationIds();
                break;
            case STANDARD:
                operationIds = nodesData.getStandardNode().getOperationIds();
                break;
            case ORGANIZATION:
                operationIds = AuthenticatedUserUtil.getListSubOrgIds();
                break;
            case POST:
                operationIds = AuthenticatedUserUtil.getPosIds();
                break;
            default:
                break;
        }
        return operationIds;
    }

    private static Set<Long> getDownLoadIdsByResourceNode(EnumClass.ResourceNodeEnum nodeEnum, AuthorityNodesData nodesData) {
        Set<Long> downLoadIds = null;
        switch (nodeEnum) {
            case FLOW:
                downLoadIds = nodesData.getProcessNode().getDownLoadIds();
                break;
            case RULE:
                downLoadIds = nodesData.getRuleNode().getDownLoadIds();
                break;
            case FILE:
                downLoadIds = nodesData.getFileNode().getDownLoadIds();
                break;
            default:
                break;
        }
        return downLoadIds;
    }
}
