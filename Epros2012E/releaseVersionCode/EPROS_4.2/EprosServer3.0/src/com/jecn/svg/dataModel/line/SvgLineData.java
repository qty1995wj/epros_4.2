package com.jecn.svg.dataModel.line;

import java.util.ArrayList;
import java.util.List;

public class SvgLineData {

	private Long startX;
	private Long startY;
	private Long endX;
	private Long endY;
	/** 记录相交线断的焦点 */
	private List<int[]> crossOverPoints = new ArrayList<int[]>();

	public List<int[]> getCrossOverPoints() {
		return crossOverPoints;
	}

	public void addCrossOverPoints(List<int[]> xyPoint) {
		if (xyPoint == null) {
			return;
		}
		this.crossOverPoints.addAll(xyPoint);
	}

	public Long getStartX() {
		return startX;
	}

	public void setStartX(Long startX) {
		this.startX = startX;
	}

	public Long getStartY() {
		return startY;
	}

	public void setStartY(Long startY) {
		this.startY = startY;
	}

	public Long getEndX() {
		return endX;
	}

	public void setEndX(Long endX) {
		this.endX = endX;
	}

	public Long getEndY() {
		return endY;
	}

	public void setEndY(Long endY) {
		this.endY = endY;
	}

}