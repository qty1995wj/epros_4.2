package com.jecn.epros.domain.process.kpi;

/**
 * 流程KPI Bean
 *
 * @author admin
 */
public class FlowKPIBean {
    private Long id;
    private String name;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
