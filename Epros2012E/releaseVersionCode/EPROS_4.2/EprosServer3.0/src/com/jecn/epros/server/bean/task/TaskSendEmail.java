package com.jecn.epros.server.bean.task;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.log4j.Logger;
import org.hibernate.Session;

import com.jecn.epros.server.bean.popedom.JecnUser;
import com.jecn.epros.server.common.JecnCommon;
import com.jecn.epros.server.common.JecnCommonSql;
import com.jecn.epros.server.common.JecnConfigContents.ConfigItemPartMapMark;
import com.jecn.epros.server.dao.popedom.IPersonDao;
import com.jecn.epros.server.dao.process.IFlowStructureDao;
import com.jecn.epros.server.dao.process.IProcessBasicInfoDao;
import com.jecn.epros.server.dao.system.IJecnConfigItemDao;
import com.jecn.epros.server.download.word.ProcessDownDataUtil;
import com.jecn.epros.server.email.buss.TaskEmailParams;
import com.jecn.epros.server.emailnew.BaseEmailBuilder;
import com.jecn.epros.server.emailnew.EmailBasicInfo;
import com.jecn.epros.server.emailnew.EmailBuilderFactory;
import com.jecn.epros.server.emailnew.EmailBuilderFactory.EmailBuilderType;
import com.jecn.epros.server.service.task.app.impl.JecnCommonTaskService;
import com.jecn.epros.server.service.task.buss.JecnTaskCommon;
import com.jecn.epros.server.util.JecnUtil;

public class TaskSendEmail {
	protected Logger log = Logger.getLogger(JecnCommonTaskService.class);
	private IJecnConfigItemDao configItemDao;
	private IPersonDao personDao;
	private IFlowStructureDao flowStructureDao;
	private IProcessBasicInfoDao processBasicDao;
	private Session session;

	public TaskSendEmail(IJecnConfigItemDao configItemDao, IPersonDao personDao, IFlowStructureDao flowStructureDao,
			IProcessBasicInfoDao processBasicDao) {
		this.configItemDao = configItemDao;
		this.personDao = personDao;
		this.flowStructureDao = flowStructureDao;
		this.processBasicDao = processBasicDao;
		this.session = configItemDao.getSession();
	}

	/**
	 * 任务审批完成，邮件
	 * 
	 * @param jecnTaskBeanNew
	 * @throws Exception
	 */
	public void finishSendEmail(JecnTaskBeanNew jecnTaskBeanNew) throws Exception {
		try {
			long taskId = jecnTaskBeanNew.getId();
			// 任务审批完成给任务审批人发送邮件
			boolean isTrue = JecnTaskCommon.isTrueMark(ConfigItemPartMapMark.emailPulishApprover.toString(),
					JecnTaskCommon.selectMessageAndEmailItemBean(configItemDao));
			Set<Long> peopleList = new HashSet<Long>();
			if (isTrue) {
				peopleList.addAll(getPubRefApp(taskId));
			}

			/** *****拟稿人邮件消息处理********** */
			// 任务审批完成发布邮件
			boolean isDrafEmailTrue = JecnTaskCommon.isTrueMark(
					ConfigItemPartMapMark.emailPulishDraftPeople.toString(), JecnTaskCommon
							.selectMessageAndEmailItemBean(configItemDao));
			if (isDrafEmailTrue) {// 拟稿人邮件处理
				peopleList.add(jecnTaskBeanNew.getCreatePersonId());
			}
			if (peopleList != null && peopleList.size() > 0) {
				finishSendEmail(peopleList, jecnTaskBeanNew);
			}
		} catch (Exception e) {
			log.error("", e);
			throw e;
		}
	}

	private void finishSendEmail(Set<Long> peopleList, JecnTaskBeanNew jecnTaskBeanNew) throws Exception {
		if (peopleList == null || peopleList.size() == 0) {
			return;
		}
		// 获取任务创建人名称
		JecnUser createJecnUser = personDao.get(jecnTaskBeanNew.getCreatePersonId());
		jecnTaskBeanNew.setCreatePersonTemporaryName(createJecnUser.getTrueName());
		List<JecnUser> userObject = JecnTaskCommon.getJecnUserList(peopleList, personDao);
		if (!userObject.isEmpty()) {
			TaskEmailParams emailParams = null;
			if (jecnTaskBeanNew.getTaskType() == 0) {
				emailParams = ProcessDownDataUtil.getTaskEmailParams(true, jecnTaskBeanNew.getRid(), flowStructureDao,
						processBasicDao);
			}
			BaseEmailBuilder emailBuilder = EmailBuilderFactory.getEmailBuilder(EmailBuilderType.TASK_FINISH);
			emailBuilder.setData(userObject, jecnTaskBeanNew, emailParams);
			List<EmailBasicInfo> buildEmail = emailBuilder.buildEmail();
			JecnUtil.saveEmail(buildEmail);
		}
	}

	/**
	 * 获取任务记录中相关人员
	 * 
	 * @param taskId
	 * @return
	 * @throws Exception
	 */
	private Set<Long> getPubRefApp(long taskId) throws Exception {
		// 获取任务记录
		List<JecnTaskForRecodeNew> list = getJecnTaskForRecodeNewList(taskId);
		Set<Long> set = new HashSet<Long>();
		for (JecnTaskForRecodeNew jecnTaskForRecodeNew : list) {
			set.add(jecnTaskForRecodeNew.getFromPeopleId());
		}
		// 获取审批记录
		return set;
	}

	/**
	 * 获得任务的审批记录
	 * 
	 * @param taskId
	 * @return
	 * @throws DaoException
	 */
	private List<JecnTaskForRecodeNew> getJecnTaskForRecodeNewList(Long taskId) throws Exception {
		// 获取任务审批记录
		String hql = "from JecnTaskForRecodeNew where taskId =?  order by sortId";
		// 获取当前任务下所有日志记录
		List<JecnTaskForRecodeNew> list = flowStructureDao.listHql(hql, taskId);
		// 记录所有日志下所有人员的人员ID集合
		Set<Long> setPeopleId = new HashSet<Long>();
		for (JecnTaskForRecodeNew taskRecord : list) {
			if (taskRecord.getCreatePersonId() != null) {
				setPeopleId.add(taskRecord.getCreatePersonId());
			}
			if (taskRecord.getFromPeopleId() != null) {
				setPeopleId.add(taskRecord.getFromPeopleId());
			}
			if (taskRecord.getToPeopleId() != null) {
				setPeopleId.add(taskRecord.getToPeopleId());
			}
			if (!JecnCommon.isNullOrEmtryTrim(taskRecord.getOpinion())) {// 评审意见换行符替换
				taskRecord.setOpinion(JecnTaskCommon.replaceAll(taskRecord.getOpinion()));
			}
		}
		if (setPeopleId.size() > 0) {
			hql = "select peopleId,trueName,isLock from JecnUser where peopleId in"
					+ JecnCommonSql.getIdsSet(setPeopleId);
			// Object[] 0:jecnUser主键ID；1：人员真实姓名;2:是否删除isLock=1已删除
			List<Object[]> listObj = flowStructureDao.listHql(hql);
			for (JecnTaskForRecodeNew jecnTaskForRecodeNew : list) {
				// 通过人员id获得人员的姓名
				if (jecnTaskForRecodeNew.getCreatePersonId() != null) {
					jecnTaskForRecodeNew.setCreatePersonTemporaryName(JecnTaskCommon.getTrueNameByPeopleId(listObj,
							jecnTaskForRecodeNew.getCreatePersonId()));
				}
				if (jecnTaskForRecodeNew.getFromPeopleId() != null) {
					jecnTaskForRecodeNew.setFromPeopleTemporaryName(JecnTaskCommon.getTrueNameByPeopleId(listObj,
							jecnTaskForRecodeNew.getFromPeopleId()));
				}
				if (jecnTaskForRecodeNew.getToPeopleId() != null) {
					jecnTaskForRecodeNew.setToPeopleTemporaryName(JecnTaskCommon.getTrueNameByPeopleId(listObj,
							jecnTaskForRecodeNew.getToPeopleId()));
				}
			}
		}
		return list;
	}

}
