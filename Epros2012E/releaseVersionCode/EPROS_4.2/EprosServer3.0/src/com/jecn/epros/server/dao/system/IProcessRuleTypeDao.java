package com.jecn.epros.server.dao.system;

import com.jecn.epros.server.bean.system.ProceeRuleTypeBean;
import com.jecn.epros.server.common.IBaseDao;

public interface IProcessRuleTypeDao extends IBaseDao<ProceeRuleTypeBean, Long> {

}
