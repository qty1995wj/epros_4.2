package epros.draw.gui.box;

import java.awt.Dimension;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.ImageIcon;
import javax.swing.JToggleButton;
import javax.swing.SwingConstants;
import javax.swing.ToolTipManager;

import epros.draw.constant.JecnLogConstants;
import epros.draw.constant.JecnToolBoxConstant.MapElemType;
import epros.draw.constant.JecnWorkflowConstant.EprosType;
import epros.draw.error.ErrorInfoConstant;
import epros.draw.event.JecnTabbedEventProcess;
import epros.draw.gui.designer.JecnDesigner;
import epros.draw.gui.figure.JecnTemplateData;
import epros.draw.gui.swing.JecnToolBar;
import epros.draw.gui.workflow.JecnDrawMainPanel;
import epros.draw.gui.workflow.popup.JecnTemplatePopupMenu;
import epros.draw.gui.workflow.tabbedPane.JecnDrawDesktopPane;
import epros.draw.gui.workflow.util.JecnWorkflowUtil;
import epros.draw.system.JecnSystemData;
import epros.draw.system.data.JecnSystemStaticData;
import epros.draw.util.JecnUIUtil;

/**
 * 
 * 模板按钮
 * 
 * 
 * @author fuzhh
 * 
 */
public class JecnTemplateBoxButton extends JToggleButton {
	/** 装载按钮容器 */
	private JecnToolBar toolBar = null;

	/** 流程元素库主面板 */
	private JecnToolBoxMainPanel toolBoxMainPanel = null;

	/** 模板数据 */
	private JecnTemplateData templateData = null;

	/**
	 * * 构造函数
	 * 
	 * @param toolBoxMainPanel
	 *            JecnToolBoxMainPanel 流程元素库面板
	 * @param templateData
	 *            模板数据
	 * @param templateType
	 *            模板标识
	 */
	public JecnTemplateBoxButton(JecnToolBoxMainPanel toolBoxMainPanel,
			JecnTemplateData templateData) {
		if (toolBoxMainPanel == null || templateData == null) {
			JecnLogConstants.LOG_TOOLBOX_BUTTON
					.error("JecnToolBoxButton类构造函数：参数为null或“”.toolBoxMainPanel="
							+ toolBoxMainPanel
							+ ";templateData="
							+ templateData);
			throw new IllegalArgumentException(ErrorInfoConstant.PARA_OBJ_NULL);
		}
		this.toolBoxMainPanel = toolBoxMainPanel;
		this.templateData = templateData;
		initComponents();

	}

	/**
	 * 
	 * 初始化
	 * 
	 * @param text
	 * @param btnActionCommand
	 */
	private void initComponents() {
		// 实例化工具栏
		toolBar = new JecnToolBar();
		toolBar.add(this);

		// 不显示焦点状态
		this.setFocusPainted(false);

		// 设置背景颜色·
		this.setBackground(JecnUIUtil.getDefaultBackgroundColor());

		// 事件
		this.addMouseListener(new MouseAdapter() {
			public void mouseReleased(MouseEvent e) {
				getMouseReleased(e);
			}

			public void mouseEntered(MouseEvent e) {
				// 提示信息
				setTemplateBoxButtonToolTipText();
			}

			public void mouseExited(MouseEvent paramMouseEvent) {
				// 返回初始延迟值
				int intD = ToolTipManager.sharedInstance().getInitialDelay();
				// 取消工具提示的延迟值
				ToolTipManager.sharedInstance().setDismissDelay(intD);
			}
		});

		// 默认图片上内容下布局
		setButtonShowType();
	}

	/**
	 * 
	 * 提示信息
	 * 
	 */
	private void setTemplateBoxButtonToolTipText() {
		// 指定取消工具提示的延迟值 设置为一小时 3600秒
		ToolTipManager.sharedInstance().setDismissDelay(3600 * 1000);
		StringBuffer sbuf = new StringBuffer();
		sbuf.append("<html>");
		sbuf
				.append("<div style= 'white-space:normal; display:block;width:150px; word-break:break-all'>");
		sbuf.append(templateData.getTemplateName());
		sbuf.append("</div></html>");
		setToolTipText(sbuf.toString());
	}

	void setButtonShowType() {
		JecnToolBoxModelContainPanel toolBoxModelContainPanel = toolBoxMainPanel
				.getModelContainPanel();
		if (toolBoxModelContainPanel == null) {
			return;
		}
		switch (toolBoxModelContainPanel.getButtonShowType()) {
		case leftRight:// 图片左内容右
			leftRight();
			break;
		case topBotton: // 图片上内容下
			topButton();
			break;
		}
	}

	/**
	 * 
	 * 图上内容下布局
	 * 
	 */
	private void topButton() {
		this.removeAll();
		// 布局管理器
		this.setLayout(null);
		// 图片
		this.setIcon(new ImageIcon(templateData.getImagerPath()));
		// 设置内容
		this.setText(templateData.getTemplateName());
		// 大小
		this.setPreferredSize(new Dimension(105, 85));
		// 最小大小
		this.setMinimumSize(new Dimension(105, 85));
		// 图片内容显示方式：居中
		this.setHorizontalAlignment(SwingConstants.CENTER);
		// 上下分布
		this.setHorizontalTextPosition(SwingConstants.CENTER);
		this.setVerticalTextPosition(SwingConstants.BOTTOM);
	}

	/**
	 * 
	 * 图左内容右布局
	 * 
	 */
	public void leftRight() {
		this.removeAll();
		// 布局管理器
		this.setLayout(null);
		// 图片
		this.setIcon(new ImageIcon(templateData.getImagerPath()));
		// 设置内容
		this.setText(templateData.getTemplateName());
		// 大小
		this.setPreferredSize(new Dimension(160, 65));
		// 最小大小
		this.setMinimumSize(new Dimension(160, 65));
		// 图片内容显示方式：靠左
		this.setHorizontalAlignment(SwingConstants.LEFT);
		// 左右分布
		this.setHorizontalTextPosition(SwingConstants.RIGHT);
		this.setVerticalTextPosition(SwingConstants.CENTER);
	}

	/**
	 * 鼠标的点击事件
	 * 
	 * @param e
	 */
	public void getMouseReleased(MouseEvent e) {
		// 情况格式刷
		JecnWorkflowUtil.clearFormatPainter();
		if (e.getSource() instanceof JToggleButton) {
			if (e.getButton() == MouseEvent.BUTTON1) {
				if (JecnDrawMainPanel.getMainPanel().getWorkflow() == null) {
					return;
				}
				// 查看是否在编辑
				JecnDrawDesktopPane workflow = JecnDrawMainPanel.getMainPanel()
						.getWorkflow();
				if (workflow != null && !workflow.isEditFail()) {// 正在编辑且内容不正确
					toolBoxMainPanel.setSelectedBtnType(MapElemType.guide);
					return;
				}

				// 删除拖动生成的虚拟图形
				JecnTabbedEventProcess.removeTempFigure();
				// 拖动端点临时线
				JecnWorkflowUtil.disposeLinesAndEidtPoint();
				// 添加完成后，选中指针
				// JecnDrawMainPanel.getMainPanel().getBoxPanel().initGuide();
				// 把ID添加到面板中
				JecnDrawMainPanel.getMainPanel().getWorkflow().setTemplateId(
						templateData.getTemplateId());
				// 清空面板选中点
				JecnWorkflowUtil.hideAllResizeHandle();
				workflow.repaint();
			} else if (e.getButton() == MouseEvent.BUTTON3) {
				// 判断是否为设计器
				if (EprosType.eprosDesigner == JecnSystemStaticData
						.getEprosType()) {
					// 如果为管理员
					if (JecnDesigner.isAdmin) {
						// 获取右键菜单
						JecnTemplatePopupMenu templatePopupMenu = new JecnTemplatePopupMenu();
						// 添加右键菜单
						templatePopupMenu.show(this, e.getX(), e.getY());
						// 设置模板数据
						JecnSystemData.setTemplateData(templateData);
					}
				} else {
					// 获取右键菜单
					JecnTemplatePopupMenu templatePopupMenu = new JecnTemplatePopupMenu();
					// 添加右键菜单
					templatePopupMenu.show(this, e.getX(), e.getY());
					// 设置模板数据
					JecnSystemData.setTemplateData(templateData);
				}
			}

		}
	}

	/**
	 * 
	 * 获取按钮容器
	 * 
	 * @return
	 */
	public JecnToolBar getJToolBar() {
		return toolBar;
	}

	public JecnTemplateData getTemplateData() {
		return templateData;
	}

}