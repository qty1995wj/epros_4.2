package com.jecn.epros.server.action.web.login.ad.cetc29;

import com.jecn.epros.server.action.web.login.LoginAction;
import com.jecn.epros.server.action.web.login.ad.JecnAbstractADLoginAction;
import com.jecn.epros.server.common.JecnCommon;

/**
 * 29所登录类
 * URL(http://192.168.1.168:8080/login.action?swieeSSO=swiee&loginName=admin)
 * loginName参数名通过配置文件获取
 * this.loginAction.getRequest().getParameter(JecnSwieeAfterItem.LOGIN_NAME);
 * 
 * @author jecn-123456
 * 
 */
public class Cetc29LoginAction extends JecnAbstractADLoginAction {
	/** 单点登录标识 */
	private String swieeSSO = "";

	public Cetc29LoginAction(LoginAction loginAction) {
		super(loginAction);
		swieeSSO = this.loginAction.getRequest().getParameter("swieeSSO");
	}

	@Override
	public String login() {
		try {
			// 29所单点登录
			if (getLogin()) {
				// 设置单点登录标识
				return doSwieeSSO();
			}
			// 普通登录
			return loginAction.loginGen();
		} catch (Exception e) {
			log.error("获取用户信息异常", e);
			return LoginAction.INPUT;
		}
	}

	/**
	 * 登录操作
	 * 
	 * @author weidp
	 * @date 2014-8-27 下午02:56:51
	 * @param loginName
	 *            1、单点登录传递过来的登录名称
	 * @return
	 */
	private String doEprosLogin(String loginName) {
		log.info("用户名：" + loginName);
		if (JecnCommon.isNullOrEmtryTrim(loginName)) {
			return LoginAction.INPUT;
		}
		loginAction.setLoginName(loginName);
		// 不验证密码登录
		return loginAction.userLoginGen(false);
	}

	/**
	 * 单点登录
	 * 
	 * @author weidp
	 * @date 2014-8-27 下午02:55:25
	 * @return
	 */
	private String doSwieeSSO() {
		log.info("进入单点登录");
		// 获取单点登录用户名称
		String loginName = this.loginAction.getRequest().getParameter(JecnCetc29AfterItem.LOGIN_NAME);
		return doEprosLogin(loginName);
	}

	/**
	 * 是否进行单点登录
	 * 
	 * @author weidp
	 * @date 2014-8-27 下午02:54:52
	 * @return
	 */
	private boolean getLogin() {
		if ("swiee".equals(swieeSSO)) {
			return true;
		}
		return false;
	}
}
