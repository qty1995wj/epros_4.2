package com.jecn.epros.util;

import com.jecn.epros.configuration.runner.ConfigLoadRunner;
import com.jecn.epros.domain.LinkResource;
import com.jecn.epros.domain.system.Dictionary;
import com.jecn.epros.security.AuthenticatedUserUtil;
import com.jecn.epros.service.ConfigItemBean;
import com.jecn.epros.service.config.ConfigService;
import com.jecn.epros.sqlprovider.server3.ConfigContents;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Created by user on 2017/2/23.
 */
@Component
public class ConfigUtils {

    @Autowired
    private ConfigService configService;
    private static ConfigService configServiceStatic;

    /**
     * 科大讯飞
     *
     * @return
     */
    public static boolean isIflytekLogin() {
        return "40".equals(getValue("otherLoginType").trim());
    }

    @PostConstruct
    private void init() {
        configServiceStatic = configService;
    }

    /**
     * @param type
     * @return
     */
    public static String getSecretByType(Object type) {
        if (type == null) {
            return "";
        }
        ConfigItemBean secret = null;
        switch (type.toString().trim()) {
            case "0":
                secret = getConfig("isSecret");
                break;
            case "1":
                secret = getConfig("isPublic");
                break;
            default:
                break;
        }
        return secret.getName(AuthenticatedUserUtil.isLanguageType());
    }

    public static ConfigItemBean getConfig(String key) {
        return ConfigLoadRunner.ConfigMap.get(key);
    }

    public static String getValue(String mark) {
        if (StringUtils.isBlank(mark)) {
            return "";
        }
        ConfigItemBean itemBean = getConfig(mark);
        return itemBean == null ? "" : itemBean.getValue();
    }

    public static String getName(String mark) {
        if (StringUtils.isBlank(mark)) {
            return "";
        }
        ConfigItemBean itemBean = getConfig(mark);
        return itemBean == null ? "" : itemBean.getName(AuthenticatedUserUtil.isLanguageType());
    }

    public static String getValue(ConfigContents.ConfigItemPartMapMark mark) {
        return getValue(mark.toString());
    }

    /**
     * 配置项值如果是0 返回true 否则为false
     *
     * @param mark
     * @return
     */
    public static boolean valueIsZeroThenTrue(String mark) {
        String value = getValue(mark).trim();
        if ("0".equals(value)) {
            return true;
        }
        return false;
    }

    /**
     * 配置项值如果是1 返回true 否则为false
     *
     * @param mark
     * @return
     */
    public static boolean valueIsOneThenTrue(String mark) {
        String value = getValue(mark).trim();
        if ("1".equals(value)) {
            return true;
        }
        return false;
    }

    /**
     * 是否使用新版本的输入输出
     *
     * @return
     */
    public static boolean useNewInout() {
        return isShowItem(ConfigContents.ConfigItemPartMapMark.useNewInout);
    }


    /**
     * 配置项值如果是0 返回true 否则为false
     *
     * @param mark
     * @return
     */
    public static boolean valueIsZeroThenTrue(ConfigContents.ConfigItemPartMapMark mark) {
        return valueIsZeroThenTrue(mark.toString());
    }

    /**
     * 配置项值如果是1 返回true 否则为false
     *
     * @param mark
     * @return
     */
    public static boolean valueIsOneThenTrue(ConfigContents.ConfigItemPartMapMark mark) {
        return valueIsOneThenTrue(mark.toString());
    }

    public static String getGradeStrByType(int type) {

//        1：高
//        2：中
//        3：低
        String gradeTypeStr = "";
        switch (type) {
            case 1:
                gradeTypeStr = JecnProperties.getValue("high");//高
                break;
            case 2:
                gradeTypeStr = JecnProperties.getValue("in");//中
                break;
            case 3:
                gradeTypeStr = JecnProperties.getValue("low");//低
                break;
            default:
                break;
        }
        return gradeTypeStr;

    }


    /**
     * 获取数据统计时间\频率的内容 0：月 1：季度
     * <p>
     * int
     *
     * @return String
     */
    public static String getKpiHorizontal(int kpiHorizontal) {
        switch (kpiHorizontal) {
            case 0:// 0：月
                return JecnProperties.getValue("month");//月
            case 1:// 1：季度
                return JecnProperties.getValue("quarter");//季度
            default:
                return "";
        }
    }

    /**
     * @param kpiHorizontal
     * @return
     */
    public static String getKpiType(Integer kpiHorizontal) {
        int t;
        if (kpiHorizontal != null) {
            t = kpiHorizontal.intValue();
        } else return "";

        switch (t) {
            case 0:// 0：结果性指标
                return JecnProperties.getValue("ResultSexIndex");//结果性指标
            case 1:// 1：过程性指标
                return JecnProperties.getValue("ProcessIndex");//过程性指标
            default:
                return "";
        }
    }

    /**
     * 获取数据获取方式的内容 0:人工 1：系统
     *
     * @param kpiDataMethod int
     * @return String
     */
    public static String getKpiDataMethod(int kpiDataMethod) {
        switch (kpiDataMethod) {
            case 0:// 0:人工
                return JecnProperties.getValue("artificial");//人工
            case 1:// 1：系统
                return JecnProperties.getValue("system");//系统
            default:
                return "";
        }
    }

    /**
     * 获取:KPI值单位名称(纵向单位)的内容
     *
     * @param kpiVertical int
     * @return String
     */
    public static String getKpiVertical(int kpiVertical) {
        switch (kpiVertical) {
            case 0:// 0:百分比
                return JecnProperties.getValue("percentage");//百分比
            case 1:// 1:季度
                return JecnProperties.getValue("quarter");//季度
            case 2:// 2:月
                return JecnProperties.getValue("month");//月
            case 3:// 3:周
                return JecnProperties.getValue("week");//周
            case 4:// 4:工作日
                return JecnProperties.getValue("workingDay");//工作日
            case 5:// 5:天
                return JecnProperties.getValue("day");//天
            case 6:// 6:小时
                return JecnProperties.getValue("hour");//小时
            case 7:// 7:分钟
                return JecnProperties.getValue("minute");//分钟
            case 8:// 8:个
                return JecnProperties.getValue("individual");
            case 9:// 9:人
                return JecnProperties.getValue("people");
            case 10:// 10:ppm
                return JecnProperties.getValue("ppm");
            case 11:// 11:元
                return JecnProperties.getValue("element");
            case 12:// 12：次
                return JecnProperties.getValue("second");
            default:
                return "";
        }
    }

    /**
     * 获取KPI目标值比较符号的内容 0：= 1：> 2：< 3:>= 4:<=
     *
     * @param kpiTargetOperator int
     * @return String
     */
    public static String getKpiTargetOperator(int kpiTargetOperator) {
        switch (kpiTargetOperator) {
            case 0:
                return "=";
            case 1:
                return ">";
            case 2:
                return "<";
            case 3:
                return ">=";
            case 4:
                return "<=";
            default:
                return "";
        }
    }

    /**
     * 获取相关度的内容 0：强相关 1：弱相关 2：不相关
     *
     * @param kpiRelevance int
     * @return String
     */
    public static String getKpiRelevance(int kpiRelevance) {
        switch (kpiRelevance) {
            case 0:// 强相关
                return JecnProperties.getValue("StrongCorrelation");
            case 1:// 弱相关
                return JecnProperties.getValue("WeakCorrelation");
            case 2:// 不相关
                return JecnProperties.getValue("unrelated");
            default:
                return "";
        }
    }

    /**
     * 获取指标来源的内容 0:二级指标 1:个人PBC指标 2:其他重要工作
     *
     * @param kpiTargetType int
     * @return String
     */
    public static String getKpiTargetType(int kpiTargetType) {
        switch (kpiTargetType) {
            case 0:// 二级指标
                return JecnProperties.getValue("twoLevelIndex");
            case 1:// 个人PBC指标
                return JecnProperties.getValue("IndividualPBCIndex");
            case 2:// 其他重要工作
                return JecnProperties.getValue("OtherImportantWork");
            default:
                return "";
        }
    }

    /* 是否是关键控制点：0：是，1：否*/
    public static String getRiskKeyPoint(int kpiPoint) {
        switch (kpiPoint) {
            case 0:
                return JecnProperties.getValue("yes");
            case 1:
                return JecnProperties.getValue("no");
            default:
                return "";
        }
    }

    /**
     * 控制频率 0:不定期 1:日 2: 周 3:月 4:季度 5:年度
     */
    public static String getRiskFrequency(int frequency) {
        switch (frequency) {
            case 0:
                return JecnProperties.getValue("Irregular");
            case 1:
                return JecnProperties.getValue("ri");
            case 2:
                return JecnProperties.getValue("week");
            case 3:
                return JecnProperties.getValue("month");
            case 4:
                return JecnProperties.getValue("quarter");
            case 5:
                return JecnProperties.getValue("yearD");
            default:
                return "";
        }
    }

    /**
     * 控制方法 0:人工1:IT 2人工/IT
     */
    public static String getRiskMethod(int method) {
        switch (method) {
            case 0:
                return JecnProperties.getValue("artificial");
            case 1:
                return JecnProperties.getValue("iT");
            case 2:
                return JecnProperties.getValue("artificialOrIt");
            default:
                return "";
        }
    }

    /**
     * 控制类型 0:预防性1:发现性
     */
    public static String getRiskType(int type) {
        switch (type) {
            case 0:
                return JecnProperties.getValue("preventive");
            case 1:
                return JecnProperties.getValue("discovery");
            default:
                return "";
        }
    }

    /**
     * 是否东软医疗
     *
     * @return
     */
    public static boolean isDryl() {
        String value = getValue("otherOperaType").trim();
        if ("27".equals(value)) {
            return true;
        }
        return false;
    }

    /**
     * 清单目录显示在后，内容列显示在前
     *
     * @return
     */
    public static boolean inventoryShowDirEnd() {
        return isMengNiuLogin();
    }

    public static boolean isMengNiuLogin() {
        String value = getValue("otherLoginType").trim();
        if ("37".equals(value)) {// 蒙牛s
            return true;
        }
        return false;
    }


    public static boolean isJiuXinLoginType() {//九新药业
        String value = getValue("otherLoginType").trim();
        if ("47".equals(value)) {// j
            return true;
        }
        return false;
    }


    public static String getSyncType() {
        return getValue("otherUserSyncType").trim();
    }

    public static String getConfidentialityLevel(Integer confidentialityLevel) {
        if (confidentialityLevel == null) {
            return "";
        }
        List<Dictionary> dictionaryList = getDictionaryList(DictionaryEnum.JECN_SECURITY_LEVEL);
        for (Dictionary dic : dictionaryList) {
            if (dic.getCode().equals(confidentialityLevel.toString())) {
                return dic.getValue();
            }
        }
        return "";
    }

    public static List<Dictionary> getDictionaryList(DictionaryEnum dictionaryEnum) {
        return ConfigLoadRunner.dictionaryMap.get(dictionaryEnum);
    }

    public static Map<DictionaryEnum, List<Dictionary>> getDictionarys() {
        return Collections.unmodifiableMap(ConfigLoadRunner.dictionaryMap);
    }

    public static Map<DictionaryEnum, List<Dictionary>> getModifiableDictionarys() {
        return new HashMap<>(ConfigLoadRunner.dictionaryMap);
    }

    /**
     * 中车-隐藏文控详情-发布时间
     *
     * @return true隐藏 发布和实施日期
     */
    public static boolean isHiddenHistoryDetailDate() {
        return !isShowItem(ConfigContents.ConfigItemPartMapMark.showHiddenImplDate);
    }

    public static boolean isShowItem(ConfigContents.ConfigItemPartMapMark mark) {
        return "1".equals(getValue(mark));
    }

    public static boolean isProcessFileDownloadAuth() {
        return "1".equals(getValue("flowFileDownMenuShow"));
    }

    /**
     * 是否显示 流程、制度 带附件下载
     *
     * @return
     */
    public static boolean isShowZipFileDownloadAuth() {
        return "1".equals(getValue("isShowZipFileDownloadAuth"));
    }

    public static String getSecurityLevelTitleC() {
        String n = AuthenticatedUserUtil.isLanguageType() == 0 ? "：" : ":";
        return getName(ConfigUtils.getDictionaryList(DictionaryEnum.JECN_SECURITY_LEVEL)) + n;
    }

    /**
     * 是否显示下载的流程文件流水号
     *
     * @return
     */
    public static boolean isShowDownLoadWordIndex() {
        String value = getValue("otherLoginType").trim();
        if ("19".equals(value)) {// 烽火
            return false;
        }
        return true;
    }

    public static boolean isAppRequest() {
        return isShowItem(ConfigContents.ConfigItemPartMapMark.isAppRequest);
    }

    public static boolean isShowEpson() {
        String value = getValue("otherLoginType").trim();
        if ("38".equals(value)) {// 爱普生
            return true;
        }
        return false;
    }

    public static boolean isShowActivityType() {
        String value = getValue("activityType").trim();
        if ("1".equals(value)) {// 爱普生
            return true;
        }
        return false;
    }

    public static int getOtherLongiType() {
        return Integer.valueOf(getValue("otherLoginType"));
    }

    public static boolean showActTimeLine() {
        return isShowItem(ConfigContents.ConfigItemPartMapMark.isShowActiveTimeValue);
    }

    public static boolean newBackCollateEnable() {
        return backCollateEnable() && useNewBackCollate();
    }

    public static boolean oldBackCollateEnable() {
        return backCollateEnable() && useOldBackCollate();
    }

    /**
     * 打回整理是否可用
     *
     * @return
     */
    private static boolean backCollateEnable() {
        return valueIsOneThenTrue(ConfigContents.ConfigItemPartMapMark.taskOperationCollate);
    }

    /**
     * 使用新版本的打回整理
     *
     * @return
     */
    private static boolean useNewBackCollate() {
        return valueIsOneThenTrue(ConfigContents.ConfigItemPartMapMark.enableNewCollate);
    }

    /**
     * 适用旧版本的打回整理
     *
     * @return
     */
    private static boolean useOldBackCollate() {
        return !useNewBackCollate();
    }

    /**
     * 流程文件活动中是否显示IT系统
     **/
    public static boolean processFileActivityShowIT() {
        return valueIsOneThenTrue(ConfigContents.ConfigItemPartMapMark.processFileActivityShowIT);
    }

    /**
     * 流程合规点是否显示
     *
     * @return
     */
    public static boolean isBussTypeShow() {
        return valueIsOneThenTrue(ConfigContents.ConfigItemPartMapMark.processShowBussType);
    }

    /**
     * 所依法规名称
     *
     * @return
     */
    public static boolean isAllowRuleShow() {
        return isZSZQ();
    }

    /**
     * 招商证券
     *
     * @return
     */
    public static boolean isZSZQ() {
        String value = getValue("otherLoginType").trim();
        if ("42".equals(value)) {
            return true;
        }
        return false;
    }

    public static String getDictionaryValue(Object code, DictionaryEnum dictionaryEnum) {
        if (code == null) {
            return "";
        }
        Dictionary dic = getDic(dictionaryEnum, code.toString());
        if (dic != null) {
            return Utils.nullToEmpty(dic.getValue());
        }
        return "";
    }

    public static Dictionary getDic(DictionaryEnum dictionaryEnum, String code) {
        Map<String, Dictionary> codeMap = codeToDicMap(dictionaryEnum);
        Dictionary dictionary = codeMap.get(code);
        return dictionary;
    }

    public static Map<String, Dictionary> codeToDicMap(DictionaryEnum dictionaryEnum) {
        return ConfigLoadRunner.dictionaryCodeMap.get(dictionaryEnum);
    }

    public static Map<String, Dictionary> codeToDicMap(List<Dictionary> dics) {
        Map<String, Dictionary> m = new HashMap<>();
        if (dics == null || dics.isEmpty()) {
            return m;
        }
        for (Dictionary dic : dics) {
            m.put(dic.getCode(), dic);
        }
        return m;
    }

    public static boolean elementEnable(String mark) {
        Map<String, Object> m = configServiceStatic.getElement(mark);
        Integer value = Integer.valueOf(m.getOrDefault("VALUE", 0).toString());
        return value > 0 ? true : false;
    }

    public static void flushDictinary(Map<DictionaryEnum, List<Dictionary>> dictionaryEnumListMap) {
        if (dictionaryEnumListMap == null || dictionaryEnumListMap.size() == 0) {
            return;
        }
        ConfigLoadRunner.dictionaryMap.clear();
        ConfigLoadRunner.dictionaryMap.putAll(dictionaryEnumListMap);
        ConfigLoadRunner.dictionaryCodeMap.clear();
        dictionaryEnumListMap.forEach((dictionaryEnum, dictionaries) -> {
            Map<String, Dictionary> codeMap = ConfigUtils.codeToDicMap(dictionaries);
            ConfigLoadRunner.dictionaryCodeMap.put(dictionaryEnum, codeMap);
        });
    }

    public static boolean isUseNewDriver() {
        return true;
    }

    public static boolean isUseNewInout() {
        return valueIsOneThenTrue(ConfigContents.ConfigItemPartMapMark.useNewInout);
    }

    public static String getName(List<Dictionary> dics) {
        if (Utils.isEmpty(dics)) {
            return "";
        }
        return getName(dics.get(0));
    }

    public static String getName(Dictionary dic) {
        if (AuthenticatedUserUtil.isLanguageType() == 0) {
            return dic.getTransName();
        }
        return dic.getEnTransName();
    }

    /**
     * 日志记录的时间频率 默认三十分钟
     * @param paramMap
     * @return
     */
    public static int getLogRecordIntervalMinute(Map<String, Object> paramMap) {
        return 30;
    }

    /**
     * 日期转换成字符串时间格式 到时分秒
     *
     * @param date Date日期
     * @return String 日期格式
     */
    public static String getStringbyDate(Date date) {
        if (date == null) {
            return null;
        }
        return new SimpleDateFormat("yyyy-MM-dd").format(date);
    }

    /**
     * 是否启用适用部门（包含启用，以及流程文件启用）
     *
     * @return
     */
    public static boolean isEnableScopeOrg() {
        return isShowItem(ConfigContents.ConfigItemPartMapMark.showApplicabilitySeleteDept) && isProcessFileItemEnable(ConfigContents.ConfigItemPartMapMark.a2);
    }

    /**
     * 流程文件某一项是否启用，非0的都是启用
     *
     * @param mark
     * @return
     */
    public static boolean isProcessFileItemEnable(ConfigContents.ConfigItemPartMapMark mark) {
        return !valueIsZeroThenTrue(mark);
    }

    /**
     * 是否显示流程清单编号
     *
     * @return
     */
    public static boolean isShowProcessListNum() {
        return valueIsOneThenTrue(ConfigContents.ConfigItemPartMapMark.isShowFlowNumberColumn);
    }


    /**
     * 任务展示页面允许的链接的类型
     *
     * @return
     */
    public static List<LinkResource.ResourceType> getTaskLawfulResourceTypes() {
        List<LinkResource.ResourceType> resources = new ArrayList<LinkResource.ResourceType>();
        //接口 取消任务编辑
        // resources.add(LinkResource.ResourceType.TASK_EDIT);
        resources.add(LinkResource.ResourceType.TASK_VIEW);
        resources.add(LinkResource.ResourceType.TASK_APPROVE);
        resources.add(LinkResource.ResourceType.TASK_RESUBMIT);
        resources.add(LinkResource.ResourceType.TASK_ORGANIZE);
        return resources;
    }


}
