package epros.draw.gui.figure.shape.total;

import java.awt.BasicStroke;
import java.awt.Graphics;
import java.awt.Graphics2D;

import epros.draw.data.figure.JecnFigureData;
import epros.draw.gui.figure.JecnBaseFigurePanel;

/**
 * 
 * 阶段分隔符
 * 
 * @author zhangxh
 * @date： 日期：Mar 28, 2012 时间：5:02:19 PM
 */
public class PartitionFigure extends JecnBaseFigurePanel {
	private int x1;
	private int x2;
	private int x3;
	private int x4;
	private int y1;
	private int y2;
	private int y3;
	private int y4;

	public PartitionFigure(JecnFigureData figureData) {
		super(figureData);
	}

	@Override
	public void paintComponent(Graphics g) {
		userWidth = this.getWidth() - 1;
		userHeight = this.getHeight() - 1;
		// 分隔符直角高度
		int partitionwW = 36;
		double currentAngle = this.getFlowElementData().getCurrentAngle();
		if (currentAngle == 0.0) {
			x1 = 0;
			y1 = 0;
			x2 = userWidth;
			y2 = partitionwW / 2;
			x3 = 0;
			y3 = partitionwW;
			x4 = 0;
			y4 = userHeight;
		} else if (currentAngle == 90.0) {
			x4 = 0;
			y4 = 0;
			x3 = userWidth - partitionwW;
			y3 = 0;
			x2 = userWidth - partitionwW / 2;
			y2 = userHeight;
			x1 = userWidth;
			y1 = 0;
		} else if (currentAngle == 180.0) {
			x4 = userWidth;
			y4 = 0;
			x3 = userWidth;
			y3 = userHeight - partitionwW;
			x2 = 0;
			y2 = userHeight - partitionwW / 2;
			x1 = userWidth;
			y1 = userHeight;
		} else if (currentAngle == 270.0) {
			x1 = 0;
			y1 = userHeight;
			x2 = partitionwW / 2;
			y2 = 0;
			x3 = partitionwW;
			y3 = userHeight;
			x4 = userWidth;
			y4 = userHeight;
		}
		paintFigure(g);
	}

	/**
	 * 无填充
	 * 
	 */
	public void paintNoneFigure(Graphics2D g2d) {
		g2d.setColor(this.getFlowElementData().getBodyColor());
		g2d.setStroke(new BasicStroke(3.0f));// 实线样式
		g2d.drawLine(x1, y1, x2, y2);
		g2d.drawLine(x2, y2, x3, y3);

		g2d.setStroke(new BasicStroke(3.0f, BasicStroke.CAP_BUTT,
				BasicStroke.JOIN_MITER, 1.0f, new float[] { 5.0f }, 0.0f));// 点线样式
		g2d.drawLine(x3, y3, x4, y4);
	}

	/**
	 * 无阴影无3D 普通效果
	 * 
	 */
	public void paintGeneralFigurePart(Graphics2D g2d) {
		g2d.setColor(this.getFlowElementData().getBodyColor());
		g2d.setStroke(new BasicStroke(3.0f));// 实线样式
		g2d.drawLine(x1, y1, x2, y2);
		g2d.drawLine(x2, y2, x3, y3);

		g2d.setStroke(new BasicStroke(3.0f, BasicStroke.CAP_BUTT,
				BasicStroke.JOIN_MITER, 1.0f, new float[] { 5.0f }, 0.0f));// 点线样式
		g2d.drawLine(x3, y3, x4, y4);
	}

}
