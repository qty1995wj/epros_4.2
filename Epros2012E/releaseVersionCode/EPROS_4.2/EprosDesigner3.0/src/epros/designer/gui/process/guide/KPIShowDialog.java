package epros.designer.gui.process.guide;

import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;
import java.util.Vector;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JMenuBar;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableModel;

import jecntool.cmd.JecnCmd;
import jxl.Cell;
import jxl.CellType;
import jxl.DateCell;
import jxl.Sheet;
import jxl.Workbook;
import jxl.format.Alignment;
import jxl.format.Colour;
import jxl.format.UnderlineStyle;
import jxl.format.VerticalAlignment;
import jxl.write.Label;
import jxl.write.WritableCellFormat;
import jxl.write.WritableFont;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
import jxl.write.WriteException;

import org.apache.log4j.Logger;
import org.springframework.beans.BeanUtils;

import com.jecn.epros.server.bean.process.JecnFlowKpi;
import com.jecn.epros.server.bean.process.JecnFlowKpiNameT;

import epros.designer.gui.common.JecnConstants;
import epros.designer.gui.common.JecnDesignerCommon;
import epros.designer.gui.process.guide.guideTable.ProcessKPIPropertyTable;
import epros.designer.gui.process.guide.guideTable.ProcessKPIValueTable;
import epros.designer.gui.process.kpi.KpiNameT;
import epros.designer.gui.process.kpi.KpiTitleAndValues;
import epros.designer.gui.process.kpi.TmpKpiShowValues;
import epros.designer.util.ConnectionPool;
import epros.designer.util.JecnProperties;
import epros.draw.gui.swing.JecnFileChooser;
import epros.draw.gui.swing.JecnPanel;
import epros.draw.gui.top.dialog.JecnDialog;
import epros.draw.gui.top.optionpane.JecnOptionPane;
import epros.draw.util.JecnUIUtil;

/*******************************************************************************
 * 流程KPI
 * 
 * @author 2012-07-12
 * 
 */
public class KPIShowDialog extends JecnDialog {
	private static Logger log = Logger.getLogger(KPIShowDialog.class);
	/** 主面板 */
	protected JecnPanel mainPanel = new JecnPanel();

	/** 信息显示Panel */
	protected JecnPanel infoPanel = new JecnPanel();

	/** KPI Panel */
	protected JecnPanel kpiPanel = new JecnPanel();

	/** KPIValue Panel */
	protected JecnPanel kpiValuePanel = new JecnPanel();

	/** closePanel */
	protected JecnPanel closePanel = new JecnPanel();

	/** 添加KPI按钮 */
	protected JButton addKPIBut = new JButton(JecnProperties.getValue("addBtn"));

	/** 编辑KPI按钮 */
	protected JButton editKPIBut = new JButton(JecnProperties.getValue("editBtn"));

	/** 删除KPI按钮 */
	protected JButton delteKPIBut = new JButton(JecnProperties.getValue("deleteBtn"));

	/** 关闭按钮 */
	protected JButton cancelBut = new JButton(JecnProperties.getValue("closeBtn"));

	/** KPI属性对应的table */
	public ProcessKPIPropertyTable processKPIPropertyTable = null;

	/** KPI属性对应的table所在的滚动面板 */
	protected JScrollPane processKPIJScrollPane = new JScrollPane();
	// ========================KPI值显示组件===============
	/** KPI值信息显示Panel */
	protected JecnPanel infoValuePanel = new JecnPanel();

	/** KPI值对应的table */
	protected ProcessKPIValueTable processKPIValueTable = null;

	/** KPI值对应的table所在的滚动面板 */
	protected JScrollPane processKPIValueJScrollPane = new JScrollPane();

	/** 添加KPI值按钮 */
	protected JButton addKPIValueBut = new JButton(JecnProperties.getValue("addBtn"));

	/** 编辑KPI值按钮 */
	protected JButton editKPIValuBut = new JButton(JecnProperties.getValue("editBtn"));

	/** 删除KPI值按钮 */
	protected JButton delteKPIValueBut = new JButton(JecnProperties.getValue("deleteBtn"));

	/** 导入 */
	protected JButton importBut = new JButton(JecnProperties.getValue("importFile"));

	/** 导出 */
	protected JButton exportBut = new JButton(JecnProperties.getValue("exportFile"));

	/** 设置大小 */
	protected Dimension dimension = null;

	/** KPI 纵向名称 */
	protected String verName = null;

	/** KPI 横向名称 */
	protected String honName = null;

	/** KPI id */
	protected Long flowKpiId = null;

	/** KPI 名称 */
	protected String kpiName = null;

	/** flowID */
	protected Long flowId = null;// 6359L;//null;

	/** 单条KPI的数据 */
	protected List<JecnFlowKpiNameT> flowKpiNameTList;

	/** KPIvalue集合jecnFlowKpiList */
	protected List<JecnFlowKpi> jecnFlowKpiList = null;

	protected Map<JecnFlowKpiNameT, List<JecnFlowKpi>> flowKpiNameMap;
	/**
	 * 所有下拉框中对应名称集合 1：kpi值单名名称集合 2：kpi类型集合（过程性指标、结果性指标） 3：数据统计时间频率集合 4：数据获取方式集合
	 * 5：指标来源集合 6：相关度 集合
	 * */
	/** kpi类型集合 **/
	private List<String> targetTypeList;
	/** 数据统计时间频率集合 **/
	private List<String> kpiHorizontalList;
	/** kpi值单位名称集合 */
	private List<String> kpiVerticalList;

	/** 数据获取方式集合 */
	private List<String> kpiDataMethodList;
	/** 指标来源集合 */
	private List<String> kpiTargetTypeList;
	/** 相关度 集合 */
	private List<String> kpiRelevanceList;

	private TmpKpiShowValues kpiShowValues;

	public List<String> getTargetTypeList() {
		return targetTypeList;
	}

	public List<String> getKpiHorizontalList() {
		return kpiHorizontalList;
	}

	public List<String> getKpiVerticalList() {
		return kpiVerticalList;
	}

	public void getStrMaps() {
		/** kpi类型集合 **/
		targetTypeList = new ArrayList<String>();
		targetTypeList.add(JecnProperties.getValue("effectTarget"));// 结果性指标
		targetTypeList.add(JecnProperties.getValue("efficiencyTarget"));// 过程性指标
		/** 数据统计时间频率集合 **/
		kpiHorizontalList = new ArrayList<String>();
		kpiHorizontalList.add(JecnProperties.getValue("monthLab"));// 月
		kpiHorizontalList.add(JecnProperties.getValue("quarterLab"));// 季度
		/** 数据获取方式集合 */
		kpiDataMethodList = new ArrayList<String>();
		kpiDataMethodList.add(JecnProperties.getValue("conManual"));// 人工
		kpiDataMethodList.add(JecnProperties.getValue("conSys"));// 系统
		/** 指标来源集合 */
		kpiTargetTypeList = new ArrayList<String>();
		kpiTargetTypeList.add(JecnProperties.getValue("twoLevelTarget"));// 二级指标
		kpiTargetTypeList.add(JecnProperties.getValue("personPBCTarget"));// 个人PBC指标
		kpiTargetTypeList.add(JecnProperties.getValue("otherImpWork"));// 其他重要工作
		/** 相关度 集合 */
		kpiRelevanceList = new ArrayList<String>();
		kpiRelevanceList.add(JecnProperties.getValue("strongCorrelation"));// 强相关
		kpiRelevanceList.add(JecnProperties.getValue("weakCorrelation"));// 弱相关
		kpiRelevanceList.add(JecnProperties.getValue("noCorrelation"));// 不相关
		/** kpi值单位名称集合 */
		kpiVerticalList = new ArrayList<String>();
		kpiVerticalList.add(JecnProperties.getValue("KPIPercentage"));// 百分比
		kpiVerticalList.add(JecnProperties.getValue("quarterLab"));// 季度1
		kpiVerticalList.add(JecnProperties.getValue("monthLab"));// 月2
		kpiVerticalList.add(JecnProperties.getValue("weekLab"));// 周3
		kpiVerticalList.add(JecnProperties.getValue("workdDay"));// 工作日4
		kpiVerticalList.add(JecnProperties.getValue("dayLab"));// 天5
		kpiVerticalList.add(JecnProperties.getValue("KPIHour"));// 小时6
		kpiVerticalList.add(JecnProperties.getValue("task_minutes")); // 分钟7
		kpiVerticalList.add(JecnProperties.getValue("oneUnit"));// 个8
		kpiVerticalList.add(JecnProperties.getValue("personUnit"));// 人9
		kpiVerticalList.add("ppm");// ppm10
		kpiVerticalList.add(JecnProperties.getValue("kpiYuan"));// 元11
		kpiVerticalList.add(JecnProperties.getValue("kpiSecondary"));// 次12
	}

	public TmpKpiShowValues getKpiShowValues() {
		return kpiShowValues;
	}

	public KPIShowDialog(Long flowId) {
		this.flowId = flowId;
		initData();
		getStrMaps();
		processKPIPropertyTable = new ProcessKPIPropertyTable(flowId, this, kpiShowValues);
		processKPIPropertyTable.addMouseListener(new java.awt.event.MouseAdapter() {
			public void mousePressed(java.awt.event.MouseEvent evt) {
				// processKPIPropertyTableMousePressed();
			}
		});
		processKPIValueTable = new ProcessKPIValueTable("", "", null);
		initLayout();
		// 初始化界面
		initializationInterface();
	}

	private void initData() {
		try {
			kpiShowValues = JecnDesignerCommon.getShowValues(flowId);
		} catch (Exception e) {
			e.printStackTrace();
			log.error("KPIShowDialog initData is error！", e);
		}
	}

	/**
	 * 初始化界面
	 * 
	 * @author fuzhh Nov 1, 2012
	 */
	protected void initializationInterface() {
		this.setSize(600, 540);
		this.setTitle(JecnProperties.getValue("flowKPI"));
		this.setLocationRelativeTo(null);
		this.setModal(true);
		this.setResizable(true);
		mainPanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		infoPanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		kpiPanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		kpiValuePanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		closePanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		processKPIJScrollPane.getViewport().setBackground(JecnUIUtil.getDefaultBackgroundColor());
		processKPIValueJScrollPane.getViewport().setBackground(JecnUIUtil.getDefaultBackgroundColor());

		dimension = new Dimension(550, 165);
		processKPIJScrollPane.setPreferredSize(dimension);
		processKPIJScrollPane.setMaximumSize(dimension);
		processKPIJScrollPane.setMinimumSize(dimension);

		dimension = new Dimension(550, 165);
		processKPIValueJScrollPane.setPreferredSize(dimension);
		processKPIValueJScrollPane.setMaximumSize(dimension);
		processKPIValueJScrollPane.setMinimumSize(dimension);

		addKPIBut.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				addKPIButPerformed();
			}
		});

		cancelBut.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				cancelButPerformed();
			}
		});
		editKPIBut.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				editKPIButPerformed();
			}
		});
		delteKPIBut.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				delteKPIButPerformed();
			}
		});
		// KPI值按钮事件
		addKPIValueBut.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				addKPIValueButPerformed();
			}
		});
		editKPIValuBut.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				editKPIValuButPerformed();
			}
		});

		delteKPIValueBut.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				delteKPIValueButPerformed();
			}
		});

		importBut.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				importButPerformed();
			}
		});

		exportBut.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				exportButPerformed();
			}
		});
	}

	protected void initLayout() {
		mainPanel.setLayout(new GridBagLayout());
		GridBagConstraints c = null;
		Insets insets = new Insets(2, 2, 2, 2);

		c = new GridBagConstraints(0, 0, 1, 1, 1.0, 1.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, insets, 0,
				0);
		mainPanel.add(infoPanel, c);
		infoPanel.setLayout(new GridBagLayout());
		infoPanel.setBorder(BorderFactory.createTitledBorder(JecnProperties.getValue("flowKPI")));
		c = new GridBagConstraints(0, 0, 1, 1, 1.0, 1.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, insets, 0,
				0);
		infoPanel.add(processKPIJScrollPane, c);
		processKPIJScrollPane.setViewportView(processKPIPropertyTable);
		// 按钮面板
		c = new GridBagConstraints(0, 1, 1, 1, 1.0, 0.0, GridBagConstraints.EAST, GridBagConstraints.HORIZONTAL,
				insets, 0, 0);
		infoPanel.add(kpiPanel, c);
		kpiPanel.setLayout(new FlowLayout(FlowLayout.RIGHT));
		kpiPanel.add(addKPIBut);
		kpiPanel.add(editKPIBut);
		kpiPanel.add(delteKPIBut);

		// c = new GridBagConstraints(0, 1, 1, 1, 1.0, 1.0,
		// GridBagConstraints.CENTER, GridBagConstraints.BOTH, insets, 0,
		// 0);
		// mainPanel.add(infoValuePanel, c);
		infoValuePanel.setLayout(new GridBagLayout());
		infoValuePanel.setBorder(BorderFactory.createTitledBorder(JecnProperties.getValue("KPIVal")));
		c = new GridBagConstraints(0, 0, 3, 1, 1.0, 1.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, insets, 0,
				0);
		infoValuePanel.add(processKPIValueJScrollPane, c);
		processKPIValueJScrollPane.setViewportView(processKPIValueTable);

		// 按钮面板
		c = new GridBagConstraints(0, 1, 1, 1, 1.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.HORIZONTAL,
				insets, 0, 0);
		infoValuePanel.add(kpiValuePanel, c);

		kpiValuePanel.setLayout(new FlowLayout(FlowLayout.RIGHT));
		kpiValuePanel.add(importBut);
		kpiValuePanel.add(exportBut);
		kpiValuePanel.add(addKPIValueBut);
		kpiValuePanel.add(editKPIValuBut);
		kpiValuePanel.add(delteKPIValueBut);

		// closePanel
		c = new GridBagConstraints(0, 2, 1, 1, 1.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.HORIZONTAL,
				insets, 0, 0);
		mainPanel.add(closePanel, c);
		closePanel.setLayout(new FlowLayout(FlowLayout.RIGHT));

		c = new GridBagConstraints(0, 3, 1, 1, 1.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.HORIZONTAL,
				insets, 0, 0);
		mainPanel.add(closePanel, c);
		closePanel.setLayout(new FlowLayout(FlowLayout.RIGHT));
		closePanel.add(cancelBut);

		this.getContentPane().add(mainPanel);
	}

	/**
	 * 添加流程KPI
	 */
	protected void addKPIButPerformed() {
		AddProcessKPIDialog addProcessKPIDialog = new AddProcessKPIDialog(flowId, this);
		addProcessKPIDialog.setVisible(true);
		if (addProcessKPIDialog.isOperation) {
			Vector<String> row = new Vector<String>();
			row.add(String.valueOf(addProcessKPIDialog.getFlowKpiNameT().getKpiAndId()));
			List<String> rows = getRowsByAdd(addProcessKPIDialog.getFlowKpiNameT());
			if (rows == null) {
				return;
			}
			for (String string : rows) {
				row.add(string);
			}
			row.add(JecnDesignerCommon.getStringbyDate(addProcessKPIDialog.getFlowKpiNameT().getCreatTime()));
			// 将数据显示到Table上
			((DefaultTableModel) processKPIPropertyTable.getModel()).addRow(row);
			this.getKpiShowValues().getKpiNameList().add(addProcessKPIDialog.getFlowKpiNameT());
		}
	}

	private List<String> getRowsByAdd(JecnFlowKpiNameT flowKpiNameT) {
		KpiNameT flowKpiName = new KpiNameT();
		BeanUtils.copyProperties(flowKpiNameT, flowKpiName);
		return KpiTitleAndValues.INSTANCE.getConfigKpiRowValues(flowKpiName,
				ConnectionPool.getConfigAciton().selectTableConfigItemBeanByType(1, 16));
	}

	/***************************************************************************
	 * 编辑流程KPI
	 */
	protected void editKPIButPerformed() {
		int selectRow = processKPIPropertyTable.getSelectedRow();
		if (selectRow == -1) {
			JecnOptionPane.showMessageDialog(this, JecnProperties.getValue("chooseOneRow"));
			return;
		}

		JecnFlowKpiNameT flowKpiNameT = kpiShowValues.getKpiNameList().get(selectRow);

		UpdateProcessKPIDialog updateProcessKPIDialog = new UpdateProcessKPIDialog(flowKpiNameT, flowId, this);
		updateProcessKPIDialog.setVisible(true);
		// 更新table
		if (updateProcessKPIDialog.isOperation) {
			// 选中行
			int selectedRow = processKPIPropertyTable.getSelectedRow();

		}

	}

	/**
	 * 删除流程KPI
	 */
	protected void delteKPIButPerformed() {

		int[] selectRows = processKPIPropertyTable.getSelectedRows();
		if (selectRows.length == 0) {
			JecnOptionPane.showMessageDialog(this, JecnProperties.getValue("deleteKPIDial"));
			return;
		}
		// 提示是否删除
		int option = JecnOptionPane.showConfirmDialog(this, JecnProperties.getValue("isDelete"), null,
				JecnOptionPane.YES_NO_OPTION, JecnOptionPane.INFORMATION_MESSAGE);
		if (option == JecnOptionPane.NO_OPTION) {
			return;
		}
		Vector<Vector<String>> vectors = ((DefaultTableModel) processKPIPropertyTable.getModel()).getDataVector();
		List<Long> kpiIds = new ArrayList<Long>();
		for (int i = selectRows.length - 1; i >= 0; i--) {
			// for (int j = 0; j < vectors.size(); j++) {
			Vector<String> vector = vectors.get(selectRows[i]);
			kpiIds.add(Long.valueOf(vector.get(0)));
			// }
		}
		// 删除数据库中的KPI数据
		try {
			ConnectionPool.getProcessAction().deleteKPIs(kpiIds, JecnConstants.loginBean.getJecnUser().getPeopleId(),
					flowId);
		} catch (Exception e) {

			log.error("KPIShowDialog delteKPIButPerformed is error", e);
		}
		for (int i = selectRows.length - 1; i >= 0; i--) {
			((DefaultTableModel) processKPIPropertyTable.getModel()).removeRow(selectRows[i]);
		}
		processKPIValueTable = new ProcessKPIValueTable(verName, honName, flowKpiId);
		processKPIValueJScrollPane.setViewportView(processKPIValueTable);
		verName = null;
		honName = null;
	}

	/**
	 * 添加流程KPI值
	 */
	protected void addKPIValueButPerformed() {
		if (verName == null || honName == null) {
			JecnOptionPane.showMessageDialog(this, JecnProperties.getValue("selFlowKPI"));
			return;
		}
		AddKPIValueDialog addKPIValueDialog = new AddKPIValueDialog(verName, honName, kpiName, flowKpiId, flowId);
		addKPIValueDialog.setVisible(true);
		if (addKPIValueDialog.isOperation) {
			processKPIValueTable = new ProcessKPIValueTable(verName, honName, flowKpiId);
			processKPIValueJScrollPane.setViewportView(processKPIValueTable);
		}
	}

	/***************************************************************************
	 * 关闭
	 */
	protected void cancelButPerformed() {
		this.dispose();
	}

	/**
	 * 编辑流程KPI值
	 */
	protected void editKPIValuButPerformed() {
		int selectNum = processKPIValueTable.getSelectedRow();
		if (verName == null || honName == null) {
			JecnOptionPane.showMessageDialog(this, JecnProperties.getValue("selFlowKPI"));
			return;
		}
		if (selectNum == -1) {
			JecnOptionPane.showMessageDialog(this, JecnProperties.getValue("chooseOneRow"));
			return;
		}
		Vector<Vector<String>> vectors = ((DefaultTableModel) processKPIValueTable.getModel()).getDataVector();
		JecnFlowKpi flowKpi = new JecnFlowKpi();
		for (int i = 0; i < vectors.size(); i++) {
			Vector<String> vector = vectors.get(selectNum);
			// 主键ID
			flowKpi.setKpiId(Long.valueOf(vector.get(0)));
			// 关联ID
			flowKpi.setKpiAndId(Long.valueOf(vector.get(1)));
			// 纵向值
			flowKpi.setKpiValue(vector.get(2));
			// 横向值
			SimpleDateFormat sdf = null; // new
			// SimpleDateFormat("yyyy-MM-dd")
			Date creatime = null;
			String horTime = "";

			try {
				String horVectors = vector.get(3);
				if (honName.equals(JecnProperties.getValue("yearLab"))) {
					sdf = new SimpleDateFormat("yyyy");
					horTime = horVectors.substring(0, 4);
				} else if (honName.equals(JecnProperties.getValue("quarterLab"))) {
					sdf = new SimpleDateFormat("yyyy-MM");
					if (horVectors.substring(4, 5).equals(JecnProperties.getValue("oneLab"))) {
						horTime = horVectors.substring(0, 4) + "-1";
					} else if (horVectors.substring(4, 5).equals(JecnProperties.getValue("twoLab"))) {
						horTime = horVectors.substring(0, 4) + "-2";
					} else if (horVectors.substring(4, 5).equals(JecnProperties.getValue("treeLab"))) {
						horTime = horVectors.substring(0, 4) + "-3";
					} else if (horVectors.substring(4, 5).equals(JecnProperties.getValue("fourLab"))) {
						horTime = horVectors.substring(0, 4) + "-4";
					}
				} else if (honName.equals(JecnProperties.getValue("monthLab"))) {
					sdf = new SimpleDateFormat("yyyy-MM");
					horTime = horVectors.substring(0, 4) + "-" + horVectors.substring(5, 7);
				} else if (honName.equals(JecnProperties.getValue("dayLab"))) {
					sdf = new SimpleDateFormat("yyyy-MM-dd");
					horTime = horVectors.substring(0, 10);
				} else if (honName.equals(JecnProperties.getValue("weekLab"))) {
					sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
					horTime = horVectors.substring(0, 4) + "-01" + "-01 " + "01:" + horVectors.substring(5, 7) + ":01";
				}
				creatime = sdf.parse(horTime);
			} catch (ParseException e) {
				log.error("KPIShowDialog editKPIValuButPerformed is error", e);

			}

			flowKpi.setKpiHorVlaue(creatime);

		}
		if (flowKpi == null) {
			return;
		}
		UpdateKPIValueDialog updateKPIValueDialog = new UpdateKPIValueDialog(flowKpi, verName, honName, kpiName,
				flowKpiId, flowId);
		updateKPIValueDialog.setVisible(true);
		if (updateKPIValueDialog.isOperation) {
			processKPIValueTable = new ProcessKPIValueTable(verName, honName, flowKpiId);
			processKPIValueJScrollPane.setViewportView(processKPIValueTable);
		}

	}

	/**
	 * 删除KPI值
	 */
	protected void delteKPIValueButPerformed() {
		if (verName == null || honName == null) {
			JecnOptionPane.showMessageDialog(this, JecnProperties.getValue("selFlowKPI"));
			return;
		}
		int[] selectRows = processKPIValueTable.getSelectedRows();
		if (selectRows.length == 0) {
			JecnOptionPane.showMessageDialog(this, JecnProperties.getValue("selDelKPIVal"));
			return;
		}
		// 提示是否删除
		int option = JecnOptionPane.showConfirmDialog(this, JecnProperties.getValue("isDelete"), null,
				JecnOptionPane.YES_NO_OPTION, JecnOptionPane.INFORMATION_MESSAGE);
		if (option == JecnOptionPane.NO_OPTION) {
			return;
		}
		Vector<Vector<String>> vectors = ((DefaultTableModel) processKPIValueTable.getModel()).getDataVector();
		List<Long> kpiValueIds = new ArrayList<Long>();
		for (int i = selectRows.length - 1; i >= 0; i--) {
			Vector<String> vector = vectors.get(selectRows[i]);
			kpiValueIds.add(Long.valueOf(vector.get(0)));
		}
		// 删除数据库中的KPI数据
		try {
			ConnectionPool.getProcessAction().deleteKPIValues(kpiValueIds,
					JecnConstants.loginBean.getJecnUser().getPeopleId(), flowId);
		} catch (Exception e) {
			log.error("KPIShowDialog delteKPIValueButPerformed is error", e);

		}
		// 删除表格中的数据
		for (int i = selectRows.length - 1; i >= 0; i--) {
			((DefaultTableModel) processKPIValueTable.getModel()).removeRow(selectRows[i]);
		}
	}

	/***************************************************************************
	 * 导入
	 * 
	 * @param args
	 */
	protected void importButPerformed() {
		if (verName == null || honName == null) {
			JecnOptionPane.showMessageDialog(this, JecnProperties.getValue("selFlowKPI"));
			return;
		}
		jecnFlowKpiList = new ArrayList<JecnFlowKpi>();
		/**
		 * 获得上传文件的控件的初始路径
		 */
		String fileDirectory = null;
		/** 上传文件的控件* */
		JecnFileChooser fileChooser = new JecnFileChooser(fileDirectory);
		// 不可以同时上传多个文件
		fileChooser.setMultiSelectionEnabled(false);
		/** 限制上传文件的格式* */

		/** 设置文件上传控制的标题* */
		fileChooser.setDialogTitle(JecnProperties.getValue("fileChoose"));
		int i = fileChooser.showSaveDialog(KPIShowDialog.this);
		if (i == javax.swing.JFileChooser.APPROVE_OPTION) {
			File fileArr = fileChooser.getSelectedFile();
			String lastStr = fileArr.toString().substring(fileArr.toString().lastIndexOf("."),
					fileArr.toString().length());
			if (!lastStr.toString().equals(".xls")) {
				JecnOptionPane.showMessageDialog(this, JecnProperties.getValue("selRrightKPIModel"));
				return;
			}
			fileDirectory = fileChooser.getCurrentDirectory().getPath();
			InputStream in = null;
			// 读取本地excel数据
			FileInputStream inputStream = null;
			// 创建excel工作表
			Workbook rwb = null;

			// 读取本地excel数据
			try {
				int dialog = JecnOptionPane.showConfirmDialog(null, JecnProperties.getValue("isCover"), null,
						JecnOptionPane.YES_NO_OPTION);
				if (dialog == JecnOptionPane.NO_OPTION) {
					return;
				}
				inputStream = new FileInputStream(fileArr);
				in = new BufferedInputStream(inputStream);
				// 创建excel工作表

				rwb = Workbook.getWorkbook(in);
				Sheet[] sheets = rwb.getSheets();
				// 验证导入的Excel是否正确
				if (!checkExcel(sheets)) {
					JecnOptionPane.showMessageDialog(this, JecnProperties.getValue("selRrightKPIModel"));
					return;
				}
				Vector<Vector<Object>> vectors = ((DefaultTableModel) processKPIValueTable.getModel()).getDataVector();
				// 判断上传的processKPIValueTable中是否存在该文件，若存在则继续
				for (Vector vector : vectors) {
					if (fileArr.getPath().equals(vector.get(1))) {
						return;
					}
				}
				// 清空表格中数据
				((DefaultTableModel) processKPIValueTable.getModel()).getDataVector().clear();

				// DefaultTableModel modeModel = (DefaultTableModel)
				// processKPIValueTable
				// .getModel();
				// 第二行开始读数据
				String reg = "^[0-9]*$";
				for (int k = 1; k < sheets[0].getRows(); k++) {
					Vector newRow = new Vector();

					// KPI纵向值
					String kpiLongitudinalValue = getCellText(sheets[0].getCell(0, k));
					if (kpiLongitudinalValue == null || "".equals(kpiLongitudinalValue)) {
						// 提示不能为空
						JecnOptionPane.showMessageDialog(this, JecnProperties.getValue("kpiHonValNotNull"));
						return;
					} else if (!kpiLongitudinalValue.matches(reg)) {
						JecnOptionPane.showMessageDialog(this, JecnProperties
								.getValue("KPIOrdinateValueCanOnlyInputDigital"));
						return;
					}
					// KPI横向值
					String kpiTransverseValue = null;
					//
					Cell c = sheets[0].getCell(1, k);
					if (c.getType() == CellType.DATE) {// 手动填写模板文件时为 date
						// 类型，其他情况有可能不是date类型
						DateCell dc = (DateCell) c;
						Date date = dc.getDate();
						TimeZone zone = TimeZone.getTimeZone("GMT");
						SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
						sdf.setTimeZone(zone);
						kpiTransverseValue = sdf.format(date);
					} else {
						kpiTransverseValue = getCellText(sheets[0].getCell(1, k));
					}

					if (kpiTransverseValue == null || "".equals(kpiTransverseValue)) {
						// 提示不能为空
						JecnOptionPane.showMessageDialog(this, JecnProperties.getValue("kpiVerValNotNull"));
						return;
					}

					// newRow.add("");
					// newRow.add(fileArr.getPath());
					// newRow.add(kpiLongitudinalValue);
					// newRow.add(kpiTransverseValue);
					// modeModel.getDataVector().add(newRow);
					// 将Excel中的数据存入数据库
					JecnFlowKpi flowKpi = new JecnFlowKpi();

					// 关联ID
					flowKpi.setKpiAndId(flowKpiId);
					// 纵向值
					flowKpi.setKpiValue(kpiLongitudinalValue);
					// 横向值
					SimpleDateFormat sdf = null; // new
					// SimpleDateFormat("yyyy-MM-dd")
					Date dateHorTime = null;
					String horTime = "";

					try {
						String horVectors = kpiTransverseValue;
						if (honName.equals(JecnProperties.getValue("yearLab"))) {
							sdf = new SimpleDateFormat("yyyy");
							horTime = horVectors.substring(0, 4);
						} else if (honName.equals(JecnProperties.getValue("quarterLab"))) {
							sdf = new SimpleDateFormat("yyyy-MM");
							if (horVectors.substring(4, 5).equals(JecnProperties.getValue("oneLab").toString())) {
								horTime = horVectors.substring(0, 4) + "-1";
							} else if (horVectors.substring(4, 5).equals(JecnProperties.getValue("twoLab").toString())) {
								horTime = horVectors.substring(0, 4) + "-4";
							} else if (horVectors.substring(4, 5).equals(JecnProperties.getValue("treeLab").toString())) {
								horTime = horVectors.substring(0, 4) + "-7";
							} else if (horVectors.substring(4, 5).equals(JecnProperties.getValue("fourLab").toString())) {
								horTime = horVectors.substring(0, 4) + "-10";
							} else {
								JecnOptionPane.showMessageDialog(this, JecnProperties.getValue("kpiVerValformatError"));
								return;
							}
						} else if (honName.equals(JecnProperties.getValue("monthLab"))) {
							sdf = new SimpleDateFormat("yyyy-MM");
							if (horVectors.length() == 7) {
								horTime = horVectors.substring(0, 4) + "-" + horVectors.substring(5, 7);
							} else if (horVectors.length() == 6) {
								horTime = horVectors.substring(0, 4) + "-" + horVectors.substring(5, 6);
							} else {
								JecnOptionPane.showMessageDialog(this, JecnProperties.getValue("kpiVerValformatError"));
								return;
							}
						} else if (honName.equals(JecnProperties.getValue("dayLab"))) {
							sdf = new SimpleDateFormat("yyyy-MM-dd");
							horTime = horVectors.substring(0, 10);
						} else if (honName.equals(JecnProperties.getValue("weekLab"))) {// 周
							sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
							if (horVectors.length() < 8) {// 2012年05周 时间格式
								return;
							}
							// 获取年
							String strYear = horVectors.substring(0, 4);
							String strWeek = horVectors.substring(5, horVectors.length() - 1);
							horTime = strYear + "-01" + "-01 " + "01:" + strWeek + ":01";
						} else {
							JecnOptionPane.showMessageDialog(this, JecnProperties.getValue("kpiVerValformatError"));
							return;
						}
						dateHorTime = sdf.parse(horTime);
					} catch (ParseException e) {
						JecnOptionPane.showMessageDialog(this, JecnProperties.getValue("kpiVerValformatError"));
						log.error("KPIShowDialog importButPerformed is error", e);
						return;
					}
					flowKpi.setKpiHorVlaue(dateHorTime);
					jecnFlowKpiList.add(flowKpi);
				}
				// 批量导入KPI值
				ConnectionPool.getProcessAction().addKPIVaules(flowKpiId, jecnFlowKpiList,
						JecnConstants.loginBean.getJecnUser().getPeopleId(), flowId);
				processKPIValueTable = new ProcessKPIValueTable(verName, honName, flowKpiId);
				processKPIValueJScrollPane.setViewportView(processKPIValueTable);
				// ((DefaultTableModel)
				// processKPIValueTable.getModel()).fireTableDataChanged();
				// //更新table

			} catch (Exception e) {
				log.error("KPIShowDialog importButPerformed is error", e);
				JecnOptionPane.showMessageDialog(this, JecnProperties.getValue("selRrightKPIModel"));
				return;
			} finally {
				if (in != null) {
					try {
						in.close();
					} catch (IOException e) {

						log.error("", e);
					}
				}
				if (inputStream != null) {
					try {
						inputStream.close();
					} catch (IOException e) {

						log.error("", e);
					}
				}

				if (rwb != null) {
					rwb.close();
				}
			}
		}

	}

	/**
	 * 
	 * 验证excel是否正确
	 * 
	 * @param sheets
	 *            导入的excel的sheet数组
	 * @return String
	 */
	protected boolean checkExcel(Sheet[] sheets) {
		if (sheets.length != 1) {
			return false;
		}
		// KPI值校验
		if (!checkProcessKPIExcel(sheets[0])) {
			return false;
		}
		return true;
	}

	/**
	 * 
	 * KPI值校验
	 * 
	 * @param sheet
	 * @return
	 */
	protected boolean checkProcessKPIExcel(Sheet sheet) {

		if (isNullObj(sheet) || sheet.getRows() < 1 || sheet.getRow(1).length != 2) {
			return false;
		}

		// 获取第二行数据
		Cell[] cellArr = sheet.getRow(0);
		// KPI纵向值
		String kpiLongitudinal = getCellText(cellArr[0]);
		// KPI横向值
		String kpiTransverse = getCellText(cellArr[1]);
		String eqLong = JecnProperties.getValue("kpiHonVal") + verName;
		String eqTransever = JecnProperties.getValue("kpiVerVal") + honName;
		if (!eqLong.equals(kpiLongitudinal) || !eqTransever.equals(kpiTransverse)) {
			return false;
		}
		return true;
	}

	/**
	 * 
	 * 获取单元格内容
	 * 
	 * @param cell
	 * @return
	 */
	protected String getCellText(Cell cell) {
		if (isNullObj(cell)) {
			return null;
		} else {
			String cellContent = cell.getContents();
			if (cellContent != null) {
				return cellContent.trim();
			}
			return null;
		}
	}

	/**
	 * 
	 * 给定参数是否为null
	 * 
	 * @param obj
	 *            Object
	 * @return
	 */
	public static boolean isNullObj(Object obj) {
		return (obj == null) ? true : false;
	}

	/**
	 * 导出
	 * 
	 * @param args
	 */
	protected void exportButPerformed() {
		if (verName == null || honName == null) {
			JecnOptionPane.showMessageDialog(this, JecnProperties.getValue("selFlowKPI"));
			return;
		}
		String fileDirectory = null;
		String dirStr = null;
		String heading = JecnProperties.getValue("KPIVal");
		// 获取流程模板文件名称
		// String flowName = selectNode.getJecnTreeBean().getName();
		// 获取流程模板文件名称
		String saveFileName = null;
		// String sourseDirectory = UploadDirectory.getSelectDirectory();
		JecnFileChooser fileChooser = new JecnFileChooser(dirStr);
		fileChooser.setDialogTitle(JecnProperties.getValue("fileSave"));
		fileChooser.setSelectedFile(new File("outputKPIValueExcel.xls"));
		int i = fileChooser.showSaveDialog(new JMenuBar());
		if (i == javax.swing.JFileChooser.APPROVE_OPTION) {
			File fileSelect = fileChooser.getSelectedFile();// 获取文件保存路径
			fileDirectory = fileSelect.getPath();

			Workbook wbookMould = null;
			WritableWorkbook wbookData = null;

			// dirStr = fileSelect.getParent();
			// saveFileName = fileDirectory.substring(dirStr.length() + 1,
			// fileDirectory.length());
			if (fileDirectory != null) {
				File isExists = new File(fileDirectory);
				try {
					if (isExists.exists()) {
						// 0 为是 1为否
						int selectYorN = JecnOptionPane.showConfirmDialog(null, JecnProperties.getValue("isReSave"),
								JecnProperties.getValue("pointLab"), JecnOptionPane.YES_NO_OPTION);
						if (selectYorN == 1) {
							return;
						} else {
							// wbookData = Workbook.createWorkbook(isExists,
							// Workbook.getWorkbook(isExists));
							// 删除原有文件，重新创建新文件
							isExists.delete();
							wbookData = Workbook.createWorkbook(isExists);
						}
					} else {
						wbookData = Workbook.createWorkbook(isExists);
					}
				} catch (Exception e) {
					if (e.getLocalizedMessage().contains("ShellFolder")) {
						JecnOptionPane.showMessageDialog(null, JecnProperties.getValue("saveFilePathError"));
					} else {
						JecnOptionPane.showMessageDialog(null, e.getLocalizedMessage());
					}
					log.error("KPIShowDialog exportButPerformed is error", e);
					return;
				}
				// 创建工作表
				WritableSheet sheet = wbookData.createSheet(heading, wbookData.getNumberOfSheets());
				// 取得Table的行数(rowNum), 列数(colNum)
				int rowNum = processKPIValueTable.getRowCount();
				int colNum = processKPIValueTable.getColumnCount();

				// 填写主标题
				// try {
				// fillHeading(sheet, heading, colNum);
				// } catch (WriteException e) {
				// 
				// }

				// 填写列名
				try {
					fillColumnName(sheet, processKPIValueTable, colNum);
				} catch (WriteException e) {
					log.error("KPIShowDialog exportButPerformed is error", e);

				}

				// 填写数据
				try {
					fillCell(sheet, processKPIValueTable, rowNum, colNum);
				} catch (WriteException e) {
					log.error("KPIShowDialog exportButPerformed is error", e);

				}

				// 写入工作表
				try {
					wbookData.write();
				} catch (IOException e) {
					log.error("KPIShowDialog exportButPerformed is error", e);

				}
				try {
					wbookData.close();
				} catch (WriteException e) {
				} catch (IOException e) {
					log.error("", e);
				}

				// 导出成功提示框
				int dialog = JecnOptionPane.showConfirmDialog(null, JecnProperties.getValue("isOpenNow"),
						JecnProperties.getValue("pointLab"), JecnOptionPane.YES_NO_OPTION);
				if (dialog == JecnOptionPane.YES_OPTION) {
					JecnCmd.openFile(isExists.getPath());
				}
			}
		}
	}

	/**
	 * 填写数据
	 * 
	 * @param sheet
	 * @param talbe
	 * @param rowNum
	 * @param colNum
	 * @throws WriteException
	 */
	protected static void fillCell(WritableSheet sheet, ProcessKPIValueTable table, int rowNum, int colNum)
			throws WriteException {
		// WritableFont font = new WritableFont(WritableFont.ARIAL, 10,
		// WritableFont.NO_BOLD, false, UnderlineStyle.NO_UNDERLINE,
		// Colour.BLACK);// 定义字体
		//		
		// WritableCellFormat format = new WritableCellFormat(font);// 定义格式化对象
		//
		// format.setAlignment(Alignment.LEFT); // 水平居中显示

		for (int i = 2; i < colNum; i++) { // 列

			for (int j = 1; j <= rowNum; j++) {// 行

				String str = table.getValueAt(j - 1, i).toString();

				Label labelN = new Label(i - 2, j, str);

				try {

					sheet.addCell(labelN);

				} catch (Exception e) {
					log.error("KPIShowDialog fillCell is error", e);

				}
			}
		}
	}

	/**
	 * 填写主标题
	 * 
	 * @param sheet
	 * @param heading
	 * @param colNum
	 * @throws WriteException
	 */
	protected static void fillHeading(WritableSheet sheet, String heading, int colNum) throws WriteException {
		WritableFont font = new WritableFont(WritableFont.ARIAL, 14, WritableFont.BOLD, false,
				UnderlineStyle.NO_UNDERLINE, Colour.RED);// 定义字体

		WritableCellFormat format = new WritableCellFormat(font);// 创建格式化对象

		format.setAlignment(Alignment.CENTRE);// 水平居中显示

		format.setVerticalAlignment(VerticalAlignment.CENTRE);// 垂直居中显示

		sheet.mergeCells(0, 0, colNum - 1, 0); // 合并单元格

		sheet.setRowView(0, 600); // 设置行高

		sheet.addCell(new Label(0, 0, heading, format));// 填写工作表

	}

	/**
	 * 填写列名
	 * 
	 * @param sheet
	 * @param table
	 * @param colNum
	 * @throws WriteException
	 */
	protected static void fillColumnName(WritableSheet sheet, JTable table, int colNum) throws WriteException {
		WritableFont font = new WritableFont(WritableFont.ARIAL, 10, WritableFont.BOLD, false,
				UnderlineStyle.NO_UNDERLINE, Colour.BLACK);// 定义字体

		WritableCellFormat format = new WritableCellFormat(font);// 定义格式化对象

		format.setAlignment(Alignment.CENTRE);// 水平居中显示

		sheet.setColumnView(0, 15);// 设置列宽

		for (int col = 2; col < colNum; col++) {

			Label colName = new Label(col - 2, 0, table.getModel().getColumnName(col), format);

			sheet.addCell(colName);
		}
	}

	// processKPIPropertyTableMousePressed
	/***************************************************************************
	 * 点击KPI表格 行数据监听
	 */
	protected void processKPIPropertyTableMousePressed() {
		int row = processKPIPropertyTable.getSelectedRow();
		if (row != -1) {
			verName = (String) processKPIPropertyTable.getValueAt(row, 7);
			honName = (String) processKPIPropertyTable.getValueAt(row, 8);
			flowKpiId = Long.valueOf((String) processKPIPropertyTable.getValueAt(row, 0));
			kpiName = (String) processKPIPropertyTable.getValueAt(row, 4);
			processKPIValueTable = new ProcessKPIValueTable(verName, honName, flowKpiId);
			processKPIValueJScrollPane.setViewportView(processKPIValueTable);
		}
	}

	/***************************************************************************
	 * 点击KPI表格 行数据监听
	 */
	protected void processKPITableMousePressed() {
		int row = processKPIPropertyTable.getSelectedRow();
		if (row != -1) {
			verName = (String) processKPIPropertyTable.getValueAt(row, 7);
			honName = (String) processKPIPropertyTable.getValueAt(row, 8);
			if (!"null".equals(processKPIPropertyTable.getValueAt(row, 0))) {
				flowKpiId = Long.valueOf((String) processKPIPropertyTable.getValueAt(row, 0));
			}
			kpiName = (String) processKPIPropertyTable.getValueAt(row, 4);
			int kpiRow = processKPIPropertyTable.getSelectedRow();
			if (kpiRow != -1) {
				JecnFlowKpiNameT flowKpiNameT = flowKpiNameTList.get(kpiRow);
				processKPIValueTable = new ProcessKPIValueTable(flowKpiNameMap.get(flowKpiNameT), flowKpiNameT
						.getKpiHorizontal(), flowKpiNameT.getKpiVertical());
				processKPIValueJScrollPane.setViewportView(processKPIValueTable);
			}
		}
	}

	/**
	 * 无用方法 删
	 * 
	 * @param selectRowNum
	 */
	public void getVerHonName(int selectRowNum) {
		if (selectRowNum != -1) {
			processKPIPropertyTable.getSelectionModel().setSelectionInterval(selectRowNum, selectRowNum);
			// KPI表格选中行的监听
			processKPIPropertyTable.getSelectionModel().addListSelectionListener(new ListSelectionListener() {
				public void valueChanged(ListSelectionEvent e) {
					int row = processKPIPropertyTable.getSelectedRow();
					if (row != -1) {
						verName = (String) processKPIPropertyTable.getValueAt(row, 7);
						honName = (String) processKPIPropertyTable.getValueAt(row, 8);
					}
				}
			});
		}
	}

	public ProcessKPIPropertyTable getProcessKPIPropertyTable() {
		return processKPIPropertyTable;
	}

}
