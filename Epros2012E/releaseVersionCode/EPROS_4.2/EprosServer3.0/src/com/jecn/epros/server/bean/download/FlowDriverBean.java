package com.jecn.epros.server.bean.download;

import java.io.Serializable;
import java.util.List;

import com.jecn.epros.server.bean.process.driver.JecnFlowDriverTimeBase;

public class FlowDriverBean implements Serializable {
	/** 0事件驱动,1是时间驱动 */
	private Long driveType;
	/** 驱动规则 */
	private String driveRules;
	/** 频率 */
	private String frequency;
	/** 开始时间 */
	private String startTime;
	/** 结束时间 */
	private String endTime;
	/** 时间类型：1:日，2:周，3:月，4:季，5:年 */
	private String quantity;

	private List<? extends JecnFlowDriverTimeBase> times;

	public Long getDriveType() {
		return driveType;
	}

	public void setDriveType(Long driveType) {
		this.driveType = driveType;
	}

	public String getDriveRules() {
		return driveRules;
	}

	public void setDriveRules(String driveRules) {
		this.driveRules = driveRules;
	}

	public String getFrequency() {
		return frequency;
	}

	public void setFrequency(String frequency) {
		this.frequency = frequency;
	}

	public String getStartTime() {
		return startTime;
	}

	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}

	public String getEndTime() {
		return endTime;
	}

	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}

	public String getQuantity() {
		return quantity;
	}

	public void setQuantity(String quantity) {
		this.quantity = quantity;
	}

	public List<? extends JecnFlowDriverTimeBase> getTimes() {
		return times;
	}

	public void setTimes(List<? extends JecnFlowDriverTimeBase> times) {
		this.times = times;
	}
	
}
