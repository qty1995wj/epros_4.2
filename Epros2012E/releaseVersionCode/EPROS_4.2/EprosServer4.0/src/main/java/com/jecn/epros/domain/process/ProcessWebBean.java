package com.jecn.epros.domain.process;

import com.jecn.epros.domain.process.kpi.FlowKpiName;
import com.jecn.epros.service.process.ProcessActiveWebBean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class ProcessWebBean implements Serializable {

    private List<Path> paths = new ArrayList<Path>();

    /**
     * 流程ID
     */
    private Long flowId;
    /**
     * 流程父节点ID
     */
    private Long parentFlowId;
    /**
     * 流程名称
     */
    private String flowName;
    /**
     * 流程编号
     */
    private String flowIdInput;// 流程编号
    /**
     * 责任部门Id
     */
    private Long orgId;
    /**
     * 责任部门
     */
    private String orgName;
    /**
     * 0是人员,1是岗位(流程责任人)和0是人员,1是岗位(流程责任人)
     */
    private int typeResPeople;// 0是人员,1是岗位(流程责任人)和0是人员,1是岗位(流程责任人)
    /**
     * 流程责任人Id
     */
    private Long resPeopleId;// 流程责任人Id
    /**
     * 流程责任人
     */
    private String resPeopleName;// 流程责任人
    /**
     * 发布时间
     */
    private String pubDate;
    /**
     * 更新时间
     */
    private String updateDate;
    /**
     * 未更新时间 单位月
     */
    private Integer noUpdateDate;
    /**
     * 类型 1是上游流程,2是下游流程,3是过程流程,4是子流程
     */
    private Integer flowType;
    /**
     * 密级 0是公开，1是秘密
     */
    private Integer isPublic;
    /**
     * 级别
     */
    private Integer level;
    /**
     * path
     **/
    private String path;
    /**
     * 版本号
     */
    private String versionId;
    /**
     * 流程监护人ID
     */
    private Long guardianId;
    /**
     * 流程监护人名称
     */
    private String guardianName;
    /**
     * 流程拟稿人
     */
    private String draftPerson;
    /**
     * 流程有效期
     */
    private Integer expiry;
    /**
     * 下次审视需完成的时间
     */
    private String nextScanDate;
    /**
     * 是否及时优化 0空 ,1是 ,2否
     */
    private Integer optimization;
    /**
     * 处于什么阶段 0是待建，1是审批，2是发布
     */
    private Integer typeByData;
    /**
     * 0是流程地图，1是流程
     */
    private Integer isflow;
    /**
     * 0未更新，1已更新状态
     */
    private Integer updateType;
    /**
     * 0未发布 ，1发布
     **/
    private Integer isPub;
    /**
     * 审批阶段
     **/
    private Integer state;

    private List<FlowKpiName> relatedKPI;
    /**
     * 关联活动
     **/
    private List<ProcessActiveWebBean> relatedActive;

    public List<FlowKpiName> getRelatedKPI() {
        return relatedKPI;
    }

    public void setRelatedKPI(List<FlowKpiName> relatedKPI) {
        this.relatedKPI = relatedKPI;
    }

    public List<ProcessActiveWebBean> getRelatedActive() {
        return relatedActive;
    }

    public void setRelatedActive(List<ProcessActiveWebBean> relatedActive) {
        this.relatedActive = relatedActive;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public List<Path> getPaths() {
        return paths;
    }

    public void setPaths(List<Path> paths) {
        this.paths = paths;
    }

    public Long getFlowId() {
        return flowId;
    }

    public void setFlowId(Long flowId) {
        this.flowId = flowId;
    }

    public Long getParentFlowId() {
        return parentFlowId;
    }

    public void setParentFlowId(Long parentFlowId) {
        this.parentFlowId = parentFlowId;
    }

    public String getFlowName() {
        return flowName;
    }

    public void setFlowName(String flowName) {
        this.flowName = flowName;
    }

    public String getFlowIdInput() {
        return flowIdInput;
    }

    public void setFlowIdInput(String flowIdInput) {
        this.flowIdInput = flowIdInput;
    }

    public Long getOrgId() {
        return orgId;
    }

    public void setOrgId(Long orgId) {
        this.orgId = orgId;
    }

    public String getOrgName() {
        return orgName;
    }

    public void setOrgName(String orgName) {
        this.orgName = orgName;
    }

    public int getTypeResPeople() {
        return typeResPeople;
    }

    public void setTypeResPeople(int typeResPeople) {
        this.typeResPeople = typeResPeople;
    }

    public Long getResPeopleId() {
        return resPeopleId;
    }

    public void setResPeopleId(Long resPeopleId) {
        this.resPeopleId = resPeopleId;
    }

    public String getResPeopleName() {
        return resPeopleName;
    }

    public void setResPeopleName(String resPeopleName) {
        this.resPeopleName = resPeopleName;
    }

    public String getPubDate() {
        return pubDate;
    }

    public void setPubDate(String pubDate) {
        this.pubDate = pubDate;
    }

    public String getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(String updateDate) {
        this.updateDate = updateDate;
    }

    public Integer getNoUpdateDate() {
        return noUpdateDate;
    }

    public void setNoUpdateDate(Integer noUpdateDate) {
        this.noUpdateDate = noUpdateDate;
    }

    public Integer getFlowType() {
        return flowType;
    }

    public void setFlowType(Integer flowType) {
        this.flowType = flowType;
    }

    public Integer getIsPublic() {
        return isPublic;
    }

    public void setIsPublic(Integer isPublic) {
        this.isPublic = isPublic;
    }

    public Integer getLevel() {
        return level;
    }

    public void setLevel(Integer level) {
        this.level = level;
    }

    public String getVersionId() {
        return versionId;
    }

    public void setVersionId(String versionId) {
        this.versionId = versionId;
    }

    public Long getGuardianId() {
        return guardianId;
    }

    public void setGuardianId(Long guardianId) {
        this.guardianId = guardianId;
    }

    public String getGuardianName() {
        return guardianName;
    }

    public void setGuardianName(String guardianName) {
        this.guardianName = guardianName;
    }

    public String getDraftPerson() {
        return draftPerson;
    }

    public void setDraftPerson(String draftPerson) {
        this.draftPerson = draftPerson;
    }

    public Integer getExpiry() {
        return expiry;
    }

    public void setExpiry(Integer expiry) {
        this.expiry = expiry;
    }

    public String getNextScanDate() {
        return nextScanDate;
    }

    public void setNextScanDate(String nextScanDate) {
        this.nextScanDate = nextScanDate;
    }

    public Integer getOptimization() {
        return optimization;
    }

    public void setOptimization(Integer optimization) {
        this.optimization = optimization;
    }

    public Integer getTypeByData() {
        return typeByData;
    }

    public void setTypeByData(Integer typeByData) {
        this.typeByData = typeByData;
    }

    public Integer getIsflow() {
        return isflow;
    }

    public void setIsflow(Integer isflow) {
        this.isflow = isflow;
    }

    public Integer getUpdateType() {
        return updateType;
    }

    public void setUpdateType(Integer updateType) {
        this.updateType = updateType;
    }

    public Integer getIsPub() {
        return isPub;
    }

    public void setIsPub(Integer isPub) {
        this.isPub = isPub;
    }

    public Integer getState() {
        return state;
    }

    public void setState(Integer state) {
        this.state = state;
    }

}
