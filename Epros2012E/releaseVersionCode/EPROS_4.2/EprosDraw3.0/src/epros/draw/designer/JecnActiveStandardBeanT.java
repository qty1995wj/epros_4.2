package epros.draw.designer;

import java.io.Serializable;

/**
 * 活动与标准关系表
 * 
 * @author ZHANGXIAOHU
 * @date： 日期：2013-11-11 时间：下午03:41:40
 */
public class JecnActiveStandardBeanT implements Serializable {
	private String id;
	/** 活动ID */
	private Long activeId;
	/** 关联类型 0是目录，1文件标准，2流程标准 3.流程地图标准,4标准条款5条款要求 */
	private int relaType;
	/** 所属条款ID */
	private Long clauseId;

	/** 标准ID */
	private Long standardId;
	/** 标准或条款名称 */
	private String relaName;
	private String figureUUID;

	public String getRelaName() {
		return relaName;
	}

	public void setRelaName(String relaName) {
		this.relaName = relaName;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Long getActiveId() {
		return activeId;
	}

	public void setActiveId(Long activeId) {
		this.activeId = activeId;
	}

	public Long getStandardId() {
		return standardId;
	}

	public void setStandardId(Long standardId) {
		this.standardId = standardId;
	}

	public int getRelaType() {
		return relaType;
	}

	public void setRelaType(int relaType) {
		this.relaType = relaType;
	}

	public Long getClauseId() {
		return clauseId;
	}

	public void setClauseId(Long clauseId) {
		this.clauseId = clauseId;
	}

	public String getFigureUUID() {
		return figureUUID;
	}

	public void setFigureUUID(String figureUUID) {
		this.figureUUID = figureUUID;
	}
	public JecnActiveStandardBeanT clone() {
		JecnActiveStandardBeanT standardBeanT = new JecnActiveStandardBeanT();
		standardBeanT.setActiveId(activeId);
		standardBeanT.setClauseId(clauseId);
		standardBeanT.setRelaName(relaName);
		standardBeanT.setRelaType(relaType);
		standardBeanT.setStandardId(standardId);
//		standardBeanT.setId(id);
		standardBeanT.setFigureUUID(figureUUID);
		return standardBeanT;
	}
}
