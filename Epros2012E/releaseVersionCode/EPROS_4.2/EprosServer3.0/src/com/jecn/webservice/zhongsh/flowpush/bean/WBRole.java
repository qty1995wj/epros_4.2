package com.jecn.webservice.zhongsh.flowpush.bean;

/**
 * 角色
 * 
 * @author user
 * 
 */
public class WBRole {

	private String guid;
	private String roleName;
	private String roleIconType = "";
	private String allowedUsers = "";
	private String deniedUsers = "";

	public String getGuid() {
		return guid;
	}

	public void setGuid(String guid) {
		this.guid = guid;
	}

	public String getRoleName() {
		return roleName;
	}

	public void setRoleName(String roleName) {
		this.roleName = roleName == null ? "" : roleName;
	}

	public String getRoleIconType() {
		return roleIconType;
	}

	public void setRoleIconType(String roleIconType) {
		this.roleIconType = roleIconType;
	}

	public String getAllowedUsers() {
		return allowedUsers;
	}

	public void setAllowedUsers(String allowedUsers) {
		this.allowedUsers = allowedUsers;
	}

	public String getDeniedUsers() {
		return deniedUsers;
	}

	public void setDeniedUsers(String deniedUsers) {
		this.deniedUsers = deniedUsers;
	}

}
