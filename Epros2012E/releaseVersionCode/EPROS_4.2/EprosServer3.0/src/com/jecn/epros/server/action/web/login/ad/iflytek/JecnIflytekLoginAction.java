package com.jecn.epros.server.action.web.login.ad.iflytek;

import net.sf.json.JSONObject;

import org.apache.commons.lang3.StringUtils;

import com.ifly.qxb.uap.client.constants.SSOConstants;
import com.ifly.qxb.uap.client.entity.SSOUser;
import com.jecn.epros.server.action.web.login.LoginAction;
import com.jecn.epros.server.action.web.login.ad.JecnAbstractADLoginAction;
import com.jecn.epros.server.common.HttpRequest;

public class JecnIflytekLoginAction extends JecnAbstractADLoginAction {

	public JecnIflytekLoginAction(LoginAction loginAction) {
		super(loginAction);
	}

	@Override
	public String login() {
		// app 访问获取的域账号
		String userAccount = loginAction.getRequest().getParameter("userAccount");
		// app 访问获取的域账号
		String appToken = loginAction.getRequest().getParameter("token");

		log.info("appToken = " + appToken + "app userAccount = " + userAccount);
		if (StringUtils.isNotBlank(userAccount) && StringUtils.isNotBlank(appToken)) {
			// 验证
			String result = getValidateToken(appToken, userAccount);
			log.info("app login result = " + result);
			String status = JSONObject.fromObject(result).getString("success");
			if ("true".equals(status)) {
				return loginByLoginName(userAccount);
			} else {
				loginAction.getResponse().setStatus(401);
				return null;
			}
		}

		SSOUser ssoUser = (SSOUser) loginAction.getSession().getAttribute(SSOConstants.SSO_USER);
		if (ssoUser == null) {
			log.info("ssoUser  is null ");
			return LoginAction.INPUT;
		}

		log.info("AccountName  = " + ssoUser.getAccountName());
		String result = loginByLoginName(ssoUser.getAccountName());
		log.info("SSO result  = " + result);
		return result;
	}

	private String getValidateToken(String token, String userAccount) {
		return HttpRequest.sendPost(JecnIflytekLoginItem.getValue("appSSOURL"), "userAccount=" + userAccount
				+ "&token=" + token);
	}
}
