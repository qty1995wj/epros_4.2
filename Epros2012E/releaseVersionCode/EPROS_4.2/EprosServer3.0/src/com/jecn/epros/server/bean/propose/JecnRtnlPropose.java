package com.jecn.epros.server.bean.propose;

import java.util.Date;

/***
 * 建议表
 * 2013-09-02
 *
 */
public class JecnRtnlPropose implements java.io.Serializable {

	/***主键ID */
	private String id;
	/***关联ID*/
	private Long relationId;
	/***建议内容*/
	private String proposeContent;
	/***回复内容*/
	private String replyContent;
	/***状态0:未读，1：已读 2：采纳*/
	private int state;
	/***回复人*/
	private Long replyPersonId;
	/***回复日期*/
	private Date replyTime;
	/***创建人*/
	private Long createPersonId;
	/***创建日期*/
	private Date createTime;
	/***更新人*/
	private Long updatePersonId;
	/***更新日期*/
	private Date updateTime;
	/**合理化建议类型：0：流程；1：制度*/
	private int rtnlType;
	/***附件建议表主键ID*/
	private String listFileIds;
	/***附件建议表文件名称*/
	private String listFileNames;
	/**创建/更新人员名称*/
	private String createUpPeopoleName;
	/**回复人名称*/
	private String replyName;
	/**流程责任人ID*/
	private Long reFlowPeopleId;
	/** 是否删除合理化建议附件： 0： 否，1： 是 */
	private int isDeleteFileId = 0;
	/** 合理化建议附件编辑界面主键ID */
	private String editProposeFileId;
	/** 页面按钮显示配置*/
	private JecnRtnlProposeButtonShowConfig buttonShowConfig;
	/**创建人员删除状态：0：未删除；1：已删除*/
	private int createPeopleState;
	/**回复人员删除状态：0：未删除；1：已删除*/
	private int replyPeopleState;
	/***关联name*/
	private String relationName;
	
	public String getRelationName() {
		return relationName;
	}
	public void setRelationName(String relationName) {
		this.relationName = relationName;
	}
	public String getId() {
		return id;
	}
	public int getCreatePeopleState() {
		return createPeopleState;
	}
	public void setCreatePeopleState(int createPeopleState) {
		this.createPeopleState = createPeopleState;
	}
	public int getReplyPeopleState() {
		return replyPeopleState;
	}
	public void setReplyPeopleState(int replyPeopleState) {
		this.replyPeopleState = replyPeopleState;
	}
	public void setId(String id) {
		this.id = id;
	}
	public Long getRelationId() {
		return relationId;
	}
	public void setRelationId(Long relationId) {
		this.relationId = relationId;
	}
	public String getProposeContent() {
		return proposeContent;
	}
	public void setProposeContent(String proposeContent) {
		this.proposeContent = proposeContent;
	}
	public String getReplyContent() {
		return replyContent;
	}
	public void setReplyContent(String replyContent) {
		this.replyContent = replyContent;
	}
	public int getState() {
		return state;
	}
	public void setState(int state) {
		this.state = state;
	}
	public Long getReplyPersonId() {
		return replyPersonId;
	}
	public void setReplyPersonId(Long replyPersonId) {
		this.replyPersonId = replyPersonId;
	}
	public Date getReplyTime() {
		return replyTime;
	}
	public void setReplyTime(Date replyTime) {
		this.replyTime = replyTime;
	}
	public Long getCreatePersonId() {
		return createPersonId;
	}
	public void setCreatePersonId(Long createPersonId) {
		this.createPersonId = createPersonId;
	}
	public Date getCreateTime() {
		return createTime;
	}
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	public Long getUpdatePersonId() {
		return updatePersonId;
	}
	public void setUpdatePersonId(Long updatePersonId) {
		this.updatePersonId = updatePersonId;
	}
	public Date getUpdateTime() {
		return updateTime;
	}
	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}
	public String getListFileIds() {
		return listFileIds;
	}
	public void setListFileIds(String listFileIds) {
		this.listFileIds = listFileIds;
	}
	public String getListFileNames() {
		return listFileNames;
	}
	public void setListFileNames(String listFileNames) {
		this.listFileNames = listFileNames;
	}
	public String getCreateUpPeopoleName() {
		
		return createUpPeopoleName;
	}
	public void setCreateUpPeopoleName(String createUpPeopoleName) {
		this.createUpPeopoleName = createUpPeopoleName;
	}
	public String getReplyName() {
		return replyName;
	}
	public void setReplyName(String replyName) {
		this.replyName = replyName;
	}
	public Long getReFlowPeopleId() {
		return reFlowPeopleId;
	}
	public void setReFlowPeopleId(Long reFlowPeopleId) {
		this.reFlowPeopleId = reFlowPeopleId;
	}
	public int getRtnlType() {
		return rtnlType;
	}
	public void setRtnlType(int rtnlType) {
		this.rtnlType = rtnlType;
	}
	public int getIsDeleteFileId() {
		return isDeleteFileId;
	}
	public void setIsDeleteFileId(int isDeleteFileId) {
		this.isDeleteFileId = isDeleteFileId;
	}
	public String getEditProposeFileId() {
		return editProposeFileId;
	}
	public void setEditProposeFileId(String editProposeFileId) {
		this.editProposeFileId = editProposeFileId;
	}
	public JecnRtnlProposeButtonShowConfig getButtonShowConfig() {
		return buttonShowConfig;
	}
	public void setButtonShowConfig(JecnRtnlProposeButtonShowConfig buttonShowConfig) {
		this.buttonShowConfig = buttonShowConfig;
	}
	
	
}
