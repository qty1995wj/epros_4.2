package epros.draw.data.figure;

import java.awt.Dimension;
import java.awt.Point;

import epros.draw.constant.JecnLogConstants;
import epros.draw.constant.JecnWorkflowConstant.EditPointType;
import epros.draw.data.JecnXYData;
import epros.draw.util.DrawCommon;

/**
 * 
 * 图形位置点和大小处理类
 * 
 * 原始状态（100%）下图片的开始位置坐标、图片宽度以及高度
 * 
 * 注意：创建此对象只有调用reSetAttributes方法后才有实际意义
 * 
 * @author ZHOUXY
 * 
 */
public class JecnFigureDataCloneable implements Cloneable {
	/** 矩形左上角坐标对象(位置点) */
	private JecnXYData leftUpXY = new JecnXYData();
	/** 图片宽度 */
	private double width = 0.0D;
	/** 图片高度 */
	private double height = 0.0D;
	// -------------上下左右编辑点
	/** 图形上边线中心点 */
	private JecnXYData editTopPoint = new JecnXYData();
	/** 图形左边线中心点 */
	private JecnXYData editLeftPoint = new JecnXYData();
	/** 图形下边线中心点 */
	private JecnXYData editBottomPoint = new JecnXYData();
	/** 图形右边线中心点 */
	private JecnXYData editRightPoint = new JecnXYData();
	// -------------上下左右编辑点

	// -------------图形八个点坐标
	/** 图形上边线上左 */
	private JecnXYData resetTopLeftPoint = new JecnXYData();
	/** 图形上边线上中 */
	private JecnXYData resetTopCenterPoint = new JecnXYData();
	/** 图形上边线上右 */
	private JecnXYData resetTopRightPoint = new JecnXYData();
	/** 图形左边线左中 */
	private JecnXYData resetLeftCenterPoint = new JecnXYData();
	/** 图形右边线右中 */
	private JecnXYData resetRightCenterPoint = new JecnXYData();
	/** 图形下边线下左 */
	private JecnXYData resetBottomLeftPoint = new JecnXYData();
	/** 图形下边线下中 */
	private JecnXYData resetBottomCenterPoint = new JecnXYData();
	/** 图形下边线下右 */
	private JecnXYData resetBottomRightPoint = new JecnXYData();

	// -------------图形八个点坐标

	// /**
	// *
	// * 通过图片位置点坐标以及其大小，创建FigureCloneable对象
	// *
	// * @param localPoint
	// * 位置点
	// * @param size
	// * 大小
	// * @param multiple
	// * 放大倍数
	// */
	// public JecnFigureDataCloneable(Point localPoint, Dimension size,
	// double multiple) {
	// reSetAttributes(localPoint, size, multiple);
	// }

	public JecnFigureDataCloneable() {
	}

	/**
	 * 
	 * 重置FigureCloneable对象的属性,位置点，大小都改变
	 * 
	 * @param localPoint
	 *            图片的位置点
	 * @param size
	 *            图片的大小
	 */
	public void reSetAttributes(Point localPoint, Dimension size,
			double multiple) {
		if (localPoint == null || size == null) {// 正常逻辑下不走此分支
			JecnLogConstants.LOG_JecnFigureDataCloneable
					.error("JecnFigureDataProcess类reSetAttributes方法：参数不能为null. localPoint="
							+ localPoint + ";size=" + size);
			return;
		}
		initCreateOriginalArributs(localPoint, size, multiple);
	}

	/**
	 * 
	 * 移动图片，只改变位置点
	 * 
	 * @param localPoint
	 *            当前移动的坐标值
	 * @param multiple
	 */
	public void moveFigure(Point localPoint, double multiple) {
		if (localPoint == null) {
			JecnLogConstants.LOG_JecnFigureDataCloneable
					.error("JecnFigureDataProcess类moveFigure方法：参数不能为null. localPoint="
							+ localPoint);
			return;
		}
		initMoveOriginalArributs(localPoint, multiple);
	}

	/**
	 * 
	 * 改变图片大小，不改变位置点
	 * 
	 * @param size
	 * @param multiple
	 */
	public void changFigure(Dimension size, double multiple) {
		if (size == null) {
			JecnLogConstants.LOG_JecnFigureDataCloneable
					.error("JecnFigureDataProcess类changFigure方法：参数不能为null. size="
							+ size);
			return;
		}
		initChangOriginalArributs(size, multiple);
	}

	/**
	 * 
	 * 获取图片对应数据类克隆放大缩小对象
	 * 
	 * @param zooMmultiple
	 *            double 放大缩小倍数
	 * @return JecnFigureDataProcess 放大缩小后的对象，此对象是克隆出来的对象
	 */
	public JecnFigureDataCloneable getZoomFigureDataProcess(double zoomMultiple) {
		// 克隆此对象
		JecnFigureDataCloneable figureCloneble = this.clone();

		// 位置点
		figureCloneble.leftUpXY.zoomXYCloneable(zoomMultiple);

		// 大小
		figureCloneble.width = figureCloneble.width * zoomMultiple;
		figureCloneble.height = figureCloneble.height * zoomMultiple;

		// --------- 上下左右编辑点
		figureCloneble.editTopPoint.zoomXYCloneable(zoomMultiple);
		figureCloneble.editLeftPoint.zoomXYCloneable(zoomMultiple);
		figureCloneble.editBottomPoint.zoomXYCloneable(zoomMultiple);
		figureCloneble.editRightPoint.zoomXYCloneable(zoomMultiple);
		// --------- 上下左右编辑点

		// -------------图形八个点坐标
		figureCloneble.resetTopLeftPoint.zoomXYCloneable(zoomMultiple);
		figureCloneble.resetTopCenterPoint.zoomXYCloneable(zoomMultiple);
		figureCloneble.resetTopRightPoint.zoomXYCloneable(zoomMultiple);
		figureCloneble.resetLeftCenterPoint.zoomXYCloneable(zoomMultiple);
		figureCloneble.resetRightCenterPoint.zoomXYCloneable(zoomMultiple);
		figureCloneble.resetBottomLeftPoint.zoomXYCloneable(zoomMultiple);
		figureCloneble.resetBottomCenterPoint.zoomXYCloneable(zoomMultiple);
		figureCloneble.resetBottomRightPoint.zoomXYCloneable(zoomMultiple);
		// -------------图形八个点坐标

		return figureCloneble;
	}

	/**
	 * 
	 * 根据编辑点类型获取上下左右编辑点坐标
	 * 
	 * @param editPointType
	 *            EditPointType 编辑点类型 老版本：2:上、3：右、4：下、1：左
	 * @return Point 编辑点坐标
	 */
	public Point getSelectedEditPoint(EditPointType editPointType,
			double multiple) {
		Point point = getSelectedOriginalEditPoint(editPointType);
		point.setLocation(point.getX() * multiple, point.getY() * multiple);
		return point;
	}

	/**
	 * 
	 * 根据编辑点类型获取编辑点
	 * 
	 * @param editPointType
	 *            EditPointType 编辑点类型 老版本：2:上、3：右、4：下、1：左
	 * @return
	 */
	public Point getSelectedOriginalEditPoint(EditPointType editPointType) {
		Point point = null;
		switch (editPointType) {
		case top:// 上
			point = this.getEditTopPoint().getPoint();
			break;
		case right:// 右
			point = this.getEditRightPoint().getPoint();
			break;
		case bottom:// 下
			point = this.getEditBottomPoint().getPoint();
			break;
		case left:// 左
			point = this.getEditLeftPoint().getPoint();
			break;
		}
		return point;
	}

	/**
	 * 
	 * 获取原始大小下位置点
	 * 
	 * @return
	 */
	public JecnXYData getLeftUpXY() {
		return leftUpXY;
	}

	/**
	 * 
	 * 获取位置点X坐标
	 * 
	 * @return int 位置点X坐标
	 */
	public int getX() {

		return DrawCommon.convertDoubleToInt(this.leftUpXY.getX());
	}

	/**
	 * 
	 * 获取位置点Y坐标
	 * 
	 * @return int 位置点Y坐标
	 */
	public int getY() {
		return DrawCommon.convertDoubleToInt(this.leftUpXY.getY());
	}

	/**
	 * 
	 * 获取位置点X坐标
	 * 
	 * @return double 位置点X坐标
	 */
	public double getDoubleX() {

		return this.leftUpXY.getX();
	}

	/**
	 * 
	 * 获取位置点Y坐标
	 * 
	 * @return double 位置点Y坐标
	 */
	public double getDoubleY() {
		return this.leftUpXY.getY();
	}

	/**
	 * 
	 * 获取宽度
	 * 
	 * @return
	 */
	public int getWidth() {
		return DrawCommon.convertDoubleToInt(width);
	}

	/**
	 * 
	 * 获取高度
	 * 
	 * @return
	 */
	public int getHeight() {
		return DrawCommon.convertDoubleToInt(height);
	}

	/**
	 * 
	 * 获取宽度
	 * 
	 * @return
	 */
	public double getWidthDouble() {
		return width;
	}

	/**
	 * 
	 * 获取高度
	 * 
	 * @return
	 */
	public double getHeightDouble() {
		return height;
	}

	/**
	 * 
	 * 获取大小
	 * 
	 * @return
	 */
	public Dimension getSize() {
		return new Dimension(this.getWidth(), this.getHeight());
	}

	/**
	 * 克隆对象
	 */
	public JecnFigureDataCloneable clone() {
		JecnFigureDataCloneable figureDataProcess = new JecnFigureDataCloneable();
		figureDataProcess.copyArritures(this);
		return figureDataProcess;
	}

	/**
	 * 
	 * 根据编辑点类型获取编辑点中心点
	 * 
	 * @param editPointType
	 *            EditPointType 编辑点类型
	 * @return JecnXYData 编辑点中心点
	 */
	public JecnXYData getEditPointByEditPointType(EditPointType editPointType) {
		JecnXYData editXyData = null;
		switch (editPointType) {
		case top:// 上
			editXyData = this.editTopPoint;
			break;
		case right:// 右
			editXyData = this.editRightPoint;
			break;
		case bottom:// 下
			editXyData = this.editBottomPoint;
			break;
		default:// 左
			editXyData = this.editLeftPoint;
			break;
		}
		return editXyData;
	}

	/**
	 * 
	 * 创建初始化原始数据,根据放大缩小倍数换算原始状态数据
	 * 
	 * @param localPoint
	 *            Point 位置点
	 * @param size
	 *            Dimension 大小
	 * @param multiple
	 *            double 放大缩小倍数
	 */
	private void initCreateOriginalArributs(Point localPoint, Dimension size,
			double multiple) {
		// 获取原始位置点
		Point localOriginalPoint = getOriginalXY(localPoint, multiple);
		initOriginalArributs(localOriginalPoint.getX(), localOriginalPoint
				.getY(), size.getWidth(), size.getHeight());
	}

	/**
	 * 
	 * 根据图形位置点和大小初始化所有相应的点的原始数据，根据放大缩小倍数换算原始状态数据
	 * 
	 * @param x
	 *            double X坐标
	 * @param y
	 *            double Y坐标
	 * @param width
	 *            double 宽
	 * @param height
	 *            double 高
	 */
	private void initOriginalArributs(double x, double y, double width,
			double height) {

		// 位置点
		this.leftUpXY.setX(x);
		this.leftUpXY.setY(y);

		// 宽度
		this.width = width;
		// 高度
		this.height = height;

		// 上中心点
		this.editTopPoint.setX(x + this.width / 2);
		this.editTopPoint.setY(y);

		// 左中心点
		this.editLeftPoint.setX(x);
		this.editLeftPoint.setY(y + this.height / 2);

		// 下中心点
		this.editBottomPoint.setX(x + this.width / 2);
		this.editBottomPoint.setY(y + this.height);

		// 右中心点
		this.editRightPoint.setX(x + this.width);
		this.editRightPoint.setY(y + this.height / 2);

		// 点击元素的8个位置点
		/** 上左 */
		this.resetTopLeftPoint.setX(x);
		this.resetTopLeftPoint.setY(y);
		/** 上中 */
		this.resetTopCenterPoint.setX(x + this.width / 2);
		this.resetTopCenterPoint.setY(y);
		/** 上右 */
		this.resetTopRightPoint.setX(x + this.width);
		this.resetTopRightPoint.setY(y);
		/** 下左 */
		this.resetBottomLeftPoint.setX(x);
		this.resetBottomLeftPoint.setY(y + this.height);
		/** 下中 */
		this.resetBottomCenterPoint.setX(x + this.width / 2);
		this.resetBottomCenterPoint.setY(y + this.height);
		/** 下右 */
		this.resetBottomRightPoint.setX(x + this.width);
		this.resetBottomRightPoint.setY(y + this.height);
		/** 左中 */
		this.resetLeftCenterPoint.setX(x);
		this.resetLeftCenterPoint.setY(y + this.height / 2);
		/** 右中 */
		this.resetRightCenterPoint.setX(x + this.width);
		this.resetRightCenterPoint.setY(y + this.height / 2);
		// 点击元素的8个位置点

	}

	/**
	 * 
	 * 初始化原始数据，根据放大缩小倍数换算原始状态下数据
	 * 
	 * @param localPoint
	 *            Point 位置点
	 * @param multiple
	 *            double 放大缩小倍数
	 */
	private void initMoveOriginalArributs(Point localPoint, double multiple) {

		// 获取原始位置点
		Point point = getOriginalXY(localPoint, multiple);
		initOriginalArributs(point.getX(), point.getY(), this.width,
				this.height);

	}

	/**
	 * 
	 * 初始化原始数据，根据放大缩小倍数换算原始状态下数据
	 * 
	 * @param size
	 *            Dimension 图形大小
	 * @param multiple
	 *            double 放大缩小倍数
	 */
	private void initChangOriginalArributs(Dimension size, double multiple) {
		// 获取原始大小
		Dimension dimension = getOriginalSize(size, multiple);

		initOriginalArributs(this.leftUpXY.getX(), this.leftUpXY.getY(),
				dimension.getWidth(), dimension.getHeight());
	}

	/**
	 * 
	 * 获取原始位置点坐标
	 * 
	 * @param localPoint
	 *            Point 位置点
	 * @param multiple
	 *            double 放大缩小倍数
	 * @return Point 给定位置点对应的原始位置点
	 */
	private Point getOriginalXY(Point localPoint, double multiple) {
		Point point = new Point();
		point.setLocation(localPoint.getX() / multiple, localPoint.getY()
				/ multiple);
		return point;
	}

	/**
	 * 
	 * 获取原始大小
	 * 
	 * @param size
	 *            Dimension 图形大小
	 * @param multiple
	 *            double 放大缩小倍数
	 * @return Dimension 给定大小对应的原始大小
	 */
	private Dimension getOriginalSize(Dimension size, double multiple) {
		Dimension dimension = new Dimension();
		dimension.setSize(size.getWidth() / multiple, size.getHeight()
				/ multiple);
		return dimension;
	}

	/**
	 * 
	 * 复制属性
	 * 
	 * @param paramF
	 *            JecnFigureDataProcess
	 */
	private void copyArritures(JecnFigureDataCloneable paramF) {
		if (paramF != null) {
			// 位置点
			this.leftUpXY = paramF.leftUpXY.clone();
			// 大小
			this.width = paramF.width;
			this.height = paramF.height;
			// --------- 上下左右编辑点
			this.editTopPoint = paramF.editTopPoint.clone();
			this.editLeftPoint = paramF.editLeftPoint.clone();
			this.editBottomPoint = paramF.editBottomPoint.clone();
			this.editRightPoint = paramF.editRightPoint.clone();
			// --------- 上下左右编辑点

			// -------------图形八个点坐标
			this.resetTopLeftPoint = paramF.resetTopLeftPoint.clone();
			this.resetTopCenterPoint = paramF.resetTopCenterPoint.clone();
			this.resetTopRightPoint = paramF.resetTopRightPoint.clone();
			this.resetLeftCenterPoint = paramF.resetLeftCenterPoint.clone();
			this.resetRightCenterPoint = paramF.resetRightCenterPoint.clone();
			this.resetBottomLeftPoint = paramF.resetBottomLeftPoint.clone();
			this.resetBottomCenterPoint = paramF.resetBottomCenterPoint.clone();
			this.resetBottomRightPoint = paramF.resetBottomRightPoint.clone();
			// -------------图形八个点坐标
		}
	}

	public JecnXYData getEditTopPoint() {
		return editTopPoint;
	}

	public JecnXYData getEditLeftPoint() {
		return editLeftPoint;
	}

	public JecnXYData getEditBottomPoint() {
		return editBottomPoint;
	}

	public JecnXYData getEditRightPoint() {
		return editRightPoint;
	}

	public JecnXYData getResetTopLeftPoint() {
		return resetTopLeftPoint;
	}

	public JecnXYData getResetTopCenterPoint() {
		return resetTopCenterPoint;
	}

	public JecnXYData getResetTopRightPoint() {
		return resetTopRightPoint;
	}

	public JecnXYData getResetLeftCenterPoint() {
		return resetLeftCenterPoint;
	}

	public JecnXYData getResetRightCenterPoint() {
		return resetRightCenterPoint;
	}

	public JecnXYData getResetBottomLeftPoint() {
		return resetBottomLeftPoint;
	}

	public JecnXYData getResetBottomCenterPoint() {
		return resetBottomCenterPoint;
	}

	public JecnXYData getResetBottomRightPoint() {
		return resetBottomRightPoint;
	}
}
