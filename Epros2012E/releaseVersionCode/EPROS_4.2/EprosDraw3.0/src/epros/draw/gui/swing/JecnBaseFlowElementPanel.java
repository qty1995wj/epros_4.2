package epros.draw.gui.swing;

import java.awt.Font;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;

import javax.swing.JPanel;

import epros.draw.constant.JecnLogConstants;
import epros.draw.constant.JecnToolBoxConstant.MapElemType;
import epros.draw.data.JecnAbstractFlowElementData;
import epros.draw.error.ErrorInfoConstant;
import epros.draw.gui.figure.unit.JecnTempFigureBean;
import epros.draw.gui.workflow.JecnDrawMainPanel;
import epros.draw.gui.workflow.tabbedPane.JecnDrawDesktopPane;
import epros.draw.gui.workflow.util.JecnCreateFlowElement;
import epros.draw.util.DrawCommon;

/**
 * 
 * 流程元素图形的基类 只能图形、线使用
 * 
 * @author ZHOUXY
 * 
 */
public abstract class JecnBaseFlowElementPanel extends JPanel {

	/** 显示双线时：双线间距间距 */
	protected int doubleLineSpace = 2;

	/** 数据对象 */
	protected JecnAbstractFlowElementData flowElementData = null;
	/** 添加或拖动图形记录图形位置、大小 */
	protected JecnTempFigureBean tempFigureBean = null;

	/** 记录流程元素上次层级 */
	protected int figureZorder = -10;
	/** 流程元素当前放大缩小倍数下字体 */
	protected Font curFont = null;
	/** 记录执行放大缩小后面板放大缩小倍数 */
	protected double oldWorkScale = 1.0;
	/** 记录操作前字体大小 */
	protected int oldFontSize = 0;
	/** 变化前字体样式 */
	protected int oldFontStyle = 0;
	/** 变化前字体名称 */
	protected String lodFontName = "";

	public JecnBaseFlowElementPanel(JecnAbstractFlowElementData flowElementData) {
		if (flowElementData == null) {
			JecnLogConstants.LOG_BASE_FLOW_ELEMENT_PANEL.error("JecnBaseFlowElementPanel类构造函数：参数为null");
			throw new IllegalArgumentException(ErrorInfoConstant.PARA_OBJ_NULL);
		}
		this.flowElementData = flowElementData;
		initBaseFlowElementPanel();

		// 透明
		this.setOpaque(false);
	}

	/**
	 * 初始化JecnBaseFlowElementPanel
	 */
	private void initBaseFlowElementPanel() {
		// 设置层级
		JecnDrawMainPanel.getMainPanel().setFlowElementLayer(this);
		// 添加流程元素鼠标事件
		this.addMouseListener(JecnDrawMainPanel.getMainPanel());
		this.addMouseMotionListener(JecnDrawMainPanel.getMainPanel());
	}

	public JecnAbstractFlowElementData getFlowElementData() {
		return flowElementData;
	}

	public int getDoubleLineSpace() {
		return doubleLineSpace;
	}

	/**
	 * 设置当前流程元素层级
	 */
	public void reSetFlowElementZorder() {
		if (figureZorder != this.getFlowElementData().getZOrderIndex()) {
			JecnDrawDesktopPane desktopPane = JecnDrawMainPanel.getMainPanel().getWorkflow();
			desktopPane.setLayer(this, this.getFlowElementData().getZOrderIndex());
			figureZorder = this.getFlowElementData().getZOrderIndex();
		}
	}

	/**
	 * 获取克隆对象
	 * 
	 */
	protected JecnBaseFlowElementPanel currClone() {
		MapElemType elemType = this.getFlowElementData().getMapElemType();
		if(MapElemType.FreeText == this.getFlowElementData().getEleShape()){
			elemType = MapElemType.FreeText;
		}
		// 获取新对象
		JecnBaseFlowElementPanel clone = JecnCreateFlowElement.createFlowElementObj(elemType);
		// 数据层数据赋值
		clone.getFlowElementData().setFlowElementAttributes(this.getFlowElementData());
		return clone;
	}

	/**
	 * 
	 * 移除当前流程元素的事件
	 * 
	 */
	public void removeFlowElemPanelEvent() {
		// 清除鼠标事件和鼠标拖动事件
		MouseListener[] mlArray = this.getMouseListeners();
		for (int i = mlArray.length - 1; i >= 0; i--) {
			this.removeMouseListener(mlArray[i]);
		}

		MouseMotionListener[] mmlArray = this.getMouseMotionListeners();
		for (int i = mmlArray.length - 1; i >= 0; i--) {
			this.removeMouseMotionListener(mmlArray[i]);
		}
	}

	/** 获取克隆数据对象 */
	public abstract JecnBaseFlowElementPanel clone();

	public JecnTempFigureBean getTempFigureBean() {
		if (tempFigureBean == null) {
			tempFigureBean = new JecnTempFigureBean();
			setTempFigureBean();
		}
		return tempFigureBean;
	}

	public void setTempFigureBean() {
		if (tempFigureBean == null) {
			return;
		}
		tempFigureBean.setCurrentAngle(this.getFlowElementData().getCurrentAngle());
		tempFigureBean.setMapElemType(this.getFlowElementData().getMapElemType());
		tempFigureBean.setUserWidth(this.getFlowElementData().getFigureSizeX());
		tempFigureBean.setUserHeight(this.getFlowElementData().getFigureSizeY());
	}

	/**
	 * 获取当前放大缩小倍数下字体
	 * 
	 * @return
	 */
	public Font getCurScaleFont() {
		double scale = this.getFlowElementData().getWorkflowScale();
		if (curFont == null
				|| oldWorkScale != scale
				|| oldFontSize != this.getFlowElementData().getFontSize()
				|| oldFontStyle != this.getFlowElementData().getFontStyle()
				|| (lodFontName != null && !lodFontName.equals(this.getFlowElementData().getFontName()))
				|| (this.getFlowElementData().getFontName() != null && !this.getFlowElementData().getFontName().equals(
						lodFontName))) {
			curFont = new Font(this.getFlowElementData().getFontName(), this.getFlowElementData().getFontStyle(),
					DrawCommon.convertDoubleToInt(scale * this.getFlowElementData().getFontSize()));
			// 记录放大缩小倍数
			oldWorkScale = scale;
			oldFontSize = this.getFlowElementData().getFontSize();
			// 字体样式 1：粗体。0普通
			oldFontStyle = this.getFlowElementData().getFontStyle();
			// 字体样式 默认宋体
			lodFontName = this.getFlowElementData().getFontName();
		}
		return curFont;
	}
}
