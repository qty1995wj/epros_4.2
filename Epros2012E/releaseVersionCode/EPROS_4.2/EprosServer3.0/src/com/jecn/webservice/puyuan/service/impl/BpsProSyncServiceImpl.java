package com.jecn.webservice.puyuan.service.impl;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.lang3.math.NumberUtils;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;

import com.jecn.webservice.common.SpecialTreatment;
import com.jecn.webservice.common.CompanyMenum.CompanyType;
import com.jecn.webservice.puyuan.PuyuanNodeUtil;
import com.jecn.webservice.puyuan.bean.FlowInfo;
import com.jecn.webservice.puyuan.bean.FlowLink;
import com.jecn.webservice.puyuan.bean.FlowNode;
import com.jecn.webservice.puyuan.dao.IBpsProcessSynchronous;
import com.jecn.webservice.puyuan.service.IBpsProSyncService;

/**
 * 
 * @ClassName: BpsProSyncServiceImpl 
 * @Description: 普元流程推送处理类
 * @author cheshaowei
 * @date 2015-4-13 下午03:33:46
 * 
 */
@Component("BpsProSyncServiceImpl")
public class BpsProSyncServiceImpl extends SpecialTreatment implements
		IBpsProSyncService {
	
	private Logger log = Logger.getLogger(BpsProSyncServiceImpl.class);
	private IBpsProcessSynchronous bpsProcessSynchronous;

	/**
	 * 所有的流程元素集合
	 * 
	 */
	private List<Object[]> flowImageAllList = new ArrayList<Object[]>();

	/**
	 * 流程信息
	 */
	private List<FlowInfo> flowInfos = new ArrayList<FlowInfo>();
	
	/**
	 * 流程元素
	 */
	private List<FlowNode> flowNodes = new ArrayList<FlowNode>();

	/**
	 * 连接线
	 */
	private List<FlowLink> flowLinks = new ArrayList<FlowLink>();

	/**
	 * 普元同步的所有流程属性对象
	 * */
	private FlowInfo flowInfo = null;


	/**
	 * 流程推送总方法
	 * 
	 * 描述：由于普元的特殊要求，必须存在启动活动否则推送失败。
	 * Epros处理方式：当流程图中不存在启动活动，将以活动编号最小的作为启动活动（活动编号必须为数字）
	 * 
	 * @param flowId
	 *            流程编号
	 * @return 0:流程推送失败 1:流程推送成功  2：不存在启动活动,并且活动编号不是数字类型 
	 * 
	 * @author cheshaowei
	 */
	@Override
	public int SysncFlowToBps(Long flowId,String path) {
        
		flowInfo = new FlowInfo();
		flowLinks.clear();
		flowNodes.clear();
		//设置公司类型为普元（BPS）
        companyType= CompanyType.bps;
		if (flowId == null) {
			log.error("待推送的flowId为空，同步失败！");
			return 0;
		}
		log.info("流程推送开始！");
		// 获取流程元素
		flowImageAllList = bpsProcessSynchronous.findAllByFlowImageId(flowId);
		super.setFlowImageAllList(flowImageAllList);
		// 判断是否存在流程开始元素，如果不存在作特殊处理 具体详看方法
		int isExit = isExitStart();
		if (isExit == 2) { // 生成开始元素失败
			return 2;
		}else if(isExit == 3){
			return 0; 
		}
		findFlowMapOrLFowInfo(flowId); // 获取当前流程信息以及流程所属的流程架构信息
		findDottedRectAll();// 调用处理特殊情况方法
		pdFlowFigureType(); // 判断流程元素类型,转换成BPS所识别的类型
		flowInfo.setFlowLink(flowLinks);
		flowInfo.setFlowNode(flowNodes);
		// 生成xml
		log.info("生成XML开始！");
		String xml = PuyuanNodeUtil.createXml(flowInfo);
		File file =null;
		BufferedWriter fileWriter = null;
		OutputStreamWriter write =null;
		try {
			  file =new File(path+".workflowx");
			  write = new OutputStreamWriter(new FileOutputStream(file), "UTF-8");
			  fileWriter = new BufferedWriter(write);
			  fileWriter.write(xml);
			  log.info("生成XML结束！");
			  log.info("推送成功！");
			  return 1;
		} catch (IOException e) {
			e.printStackTrace();
		}finally{
			try {
				fileWriter.flush();
				write.close();
				fileWriter.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return 0;
	}
  
	
	
	
	
	/**
	 * 判断是否存在流程开始元素,如果不存在查找元素编号为01的作为开始元素（此种情况需要保证元素编号必须为number类型）
	 * 
	 * @author cheshaowei
	 * @Date 2014-01-22
	 * @return int 0:存在起始活动  1：不存在起始活动，以活动编号最小的作为起始活动  2:不存在起始活动 并且活动编号不是number类型 3：出现异常
	 * 
	 * */
	private int isExitStart() {
		try {
			//循环查找是否存在流程开始元素
			for (Object[] startObj : flowImageAllList) {
				if (startObj != null) {
					// 存在流程开始元素
					if (startObj[2] != null
							&& "FlowFigureStart".equals(startObj[2].toString())) {
						return 0;
					}
				}
			}
			// 查找最小活动编号
			int minValue = Integer.MAX_VALUE;
			Object[] obj = null;
			for (int i = 0; i < flowImageAllList.size(); i++) {
				Object[] objActive = flowImageAllList.get(i);
				String activeNum = objActive[10] != null ? objActive[10]
						.toString() : null;
				if (activeNum == null) {
					continue;
				}
				// 如果活动编号不是number类型
				if (!NumberUtils.isNumber(activeNum)) {
					return 2;
				} else {
					if (Integer.parseInt(activeNum) < minValue) {
						minValue = Integer.parseInt(activeNum);
						obj = objActive;
					}
				}
			}
			//将编号为01的元素类型设为开始元素
			if (obj != null && obj[2] != null) {
				obj[2] = "FlowFigureStart";
				return 1;
			}
			return 2; 
		} catch (NumberFormatException e) {
			log.error("判断是否存在流程开始元素出现异常  BpsProSyncServiceImpl isExitStart()",e);
			return 3;
		}
	}

	/**
	 * 查询流程信息
	 * 
	 * @author cheshaowei
	 */
	private void findFlowMapOrLFowInfo(Long flowId) {
		try {
			// ===========获取当前流程信息以及流程所属的流程架构====
			List<Object[]> objects = bpsProcessSynchronous
					.findAllByFlowMapId(flowId);
			if (objects != null && objects.size() > 0) {
				Object[] flowImageObj = objects.get(0);
				if (flowImageObj[0] != null) {
					flowInfo.setFlowId(Long.parseLong(flowImageObj[0]
							.toString()));
				}
				if (flowImageObj[1] != null) {
					flowInfo.setFlowName(flowImageObj[1].toString());
				}
				if (flowImageObj[2] != null) {
					flowInfo.setFlowCreateName(flowImageObj[2].toString());
				}
				if (flowImageObj[3] != null) {
					flowInfo.setFlowDepart(flowImageObj[3].toString());
				}
			}
		} catch (NumberFormatException e) {
			log.error("生成流程架构信息和流程信息出现异常,BpsProSyncServiceImpl findFlowMapOrLFowInfo()",
							e);
		}
	}

	/**
	 * 判断流程元素类型,转换成BPS所识别的类型
	 * 
	 * @param objMap
	 *            当前流程下的所有的流程元素
	 * 
	 */
	public void pdFlowFigureType() {
		try {
			for (Object[] objects : flowImageAllList) {
				String figure = objects[2].toString();
				if (figure == null) {
					continue;
				}
				if ("RoundRectWithLine".equals(figure)) { // 活动
					flowNodeFz(objects, "manual");
				} else if ("CommonLine".equals(figure)) { // 连接线
					flowLink(objects, 0);
				} else if ("ManhattanLine".equals(figure)) {// 连接线带箭头
					flowLink(objects, 0);
				} else if ("RightArrowhead".equals(figure)) { // 返出
					flowLink(objects, 1);
				} else if ("Rhombus".equals(figure)) {// 决策
					flowNodeFz(objects, "route");
				} else if ("ImplFigure".equals(figure)) { // 接口
					flowNodeFz(objects, "manual");
				} else if ("OvalSubFlowFigure".equals(figure)) { // 子流程
					flowNodeFz(objects, "manual");
				} else if ("EndFigure".equals(figure)) { // end结束
					flowNodeFz(objects, "finish");
				} else if ("XORFigure".equals(figure)) { // XOR
					flowNodeFz(objects, "route");
				} else if ("ORFigure".equals(figure)) { // OR
					flowNodeFz(objects, "route");
				} else if ("FlowFigureStart".equals(figure)) { // 流程开始
					flowNodeFz(objects, "start");
				} else if ("FlowFigureStop".equals(figure)) { // 流程结束
					flowNodeFz(objects, "finish");
				} else if ("ITRhombus".equals(figure)) { // IT决策
					flowNodeFz(objects, "route");
				}
			}
		} catch (Exception e) {
			log.error("流程元素类型,转换成BPS所识别的类型 出现异常！BpsProSyncServiceImpl pdFlowFigureType()",
							e);
		}
	}

	/**
	 * 生成流程元素
	 * 
	 * @param objects
	 *            流程元素 nodeType 节点类型
	 */
	public void flowNodeFz(Object[] objects, String nodeType) {

		FlowNode flowNode;

		try {
			flowNode = new FlowNode();
			if (objects[1] != null) {// 节点ID
				flowNode.setNodeId(Long.parseLong(objects[1].toString()));
			}
			if (objects[3] != null) {// 节点名称
				flowNode.setNodeName(objects[3].toString());
			}
			if (objects[6] != null) { // x坐标
				flowNode.setxNodePoint(Integer.parseInt(objects[6].toString()));
			}
			if (objects[7] != null) { // y坐标
				flowNode.setyNodePoint(Integer.parseInt(objects[7].toString()));
			}
			if (objects[8] != null) { // 元素高度
				flowNode.setNodeWidth(Integer.parseInt(objects[8].toString()));
			}
			if (objects[9] != null) { // 元素宽度
				flowNode.setNodeHeight(Integer.parseInt(objects[9].toString()));
			}
			// 活动对应角色
			if (nodeType.equals("manual")) {
				if (objects[12] != null && objects[13] != null) {
					flowNode.setActiveRole(objects[12].toString());
					flowNode.setActiveRoleId(Integer.parseInt(objects[13]
							.toString()));
				}
				if (objects[14] != null) {
					flowNode.setActiveDesc(objects[14].toString());
				}
			}
			// 元素类型
			flowNode.setNodeType(nodeType);
			flowNodes.add(flowNode);
		} catch (Exception e) {
			log.error("生成流程元素出现异常！BpsProSyncServiceImpl flowNodeFz()", e);
		}

	}

	/**
	 * 
	 * @Title: flowLink 
	 * @Description: 生成流程元素连接线
	 * @param  objects 单个流程元素(连接线/返出符） linkType 连接线类型
	 * @param linkType 连接线类型 0：不是返工符 1：是返工符
	 * @return void 
	 * @author cheshaowei
	 * @throws
	 * 
	 */
	public void flowLink(Object[] objects, int linkType) {

		try {
			// ============生成连接线开始=========================
			FlowLink flowLink = new FlowLink();
			if (objects[1] != null) {
				// 连接线编号
				flowLink.setLinkId(Long.parseLong(objects[1].toString()));
			}

			if (objects[3] != null) {
				// 连接线名称
				flowLink.setLinkName(objects[3].toString()); // 连接线名称
			}

			if (linkType == 0) { // 不是返工符
				// 起始元素
				if (objects[4] == null && objects[5] == null) {
					return;
				}
				flowLink.setFromNodeId(Long.parseLong(objects[4].toString()));
				// 结束元素
				flowLink.setToNodeId(Long.parseLong(objects[5].toString()));
			} else if (linkType == 1) { // 返工符
				// 首先根据返出符号查找有没有对应的返入符号
				for (Object[] objRev : flowImageAllList) {
					// 找到对应的返入符
					if ((objRev[3] != null && objects[3] != null)
							&& (objRev[3].toString().equals(objects[3]
									.toString()))
							&& (objRev[2].toString())
									.equals("ReverseArrowhead")) {
						// 找到离返出符最近的元素 
						//objects 返出元素
						Object[] RightArrowhead = rArrowhead(objects, Double
								.parseDouble(objects[11].toString()),
								flowImageAllList);
						
						//找到离返入元素最近的元素
						//objRev 返入元素  
						Object[] ReverseArrowhead = rArrowhead(objRev, Double
								.parseDouble(objRev[11].toString()),
								flowImageAllList);
						
						// 找到返入最近的元素
						if (RightArrowhead != null && ReverseArrowhead != null) {
							
							if (RightArrowhead[1] != null&& ReverseArrowhead[1] != null) {
								// 起始元素
								flowLink.setFromNodeId(Long
												.parseLong(RightArrowhead[1]
							  				.toString()));
								//普元(BPS) 由于bps系统中连接线不能够指向流程开始元素
								//但是epros中的返入符可以指向流程开始元素，所以在生成连接线的时候会指向流程开始元素。
								//出现此种情况将指向流程开始元素的连接线改为指向流程编号为01的元素
								if(companyType.equals(CompanyType.bps)){
									//获取所有流程元素中最小的元素
								   Object[] maxElement = findMinActive();
								   if(maxElement!=null){
									    // 结束元素
									flowLink.setToNodeId(Long.parseLong(maxElement[1]
																			.toString()));
								   }
								}else{
									// 结束元素
									flowLink.setToNodeId(Long.parseLong(ReverseArrowhead[1]
													.toString()));
								}
							}
							
						}
						break;
					}
				}
			}
			// =======================end======================================
			flowLinks.add(flowLink);
			
		} catch (NumberFormatException e) {
			log.error("生成流程元素连接线出现异常，BpsProSyncServiceImpl flowLink()", e);
		}
	}

	public IBpsProcessSynchronous getBpsProcessSynchronous() {
		return bpsProcessSynchronous;
	}

	@Resource
	public void setBpsProcessSynchronous(
			IBpsProcessSynchronous bpsProcessSynchronous) {
		this.bpsProcessSynchronous = bpsProcessSynchronous;
	}

	public List<FlowInfo> getFlowInfos() {
		return flowInfos;
	}

	public void setFlowInfos(List<FlowInfo> flowInfos) {
		this.flowInfos = flowInfos;
	}

	public List<FlowNode> getFlowNodes() {
		return flowNodes;
	}

	public void setFlowNodes(List<FlowNode> flowNodes) {
		this.flowNodes = flowNodes;
	}

	public List<FlowLink> getFlowLinks() {
		return flowLinks;
	}

	public void setFlowLinks(List<FlowLink> flowLinks) {
		this.flowLinks = flowLinks;
	}
}
