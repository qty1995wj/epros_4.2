package com.jecn.epros.domain.common.relatedfiles;

/**
 * Created by Angus on 2017/4/20.
 */
public enum RelatedFilesType {
    processmap(0),
    process(1),
    rule(2),
    standard(3),
    risk(4),
    file(5);


    private Integer value;

    RelatedFilesType(Integer value) {
        this.value = value;
    }

    public Integer getValue() {
        return value;
    }
}
