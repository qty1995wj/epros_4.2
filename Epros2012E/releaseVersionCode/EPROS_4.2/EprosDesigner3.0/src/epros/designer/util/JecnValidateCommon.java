package epros.designer.util;

import java.util.ArrayList;
import java.util.List;

import javax.swing.JLabel;

import com.jecn.epros.server.bean.system.JecnConfigItemBean;
import com.jecn.epros.server.common.JecnConfigContents.ConfigItemPartMapMark;

import epros.designer.gui.publish.PublicFileRequiredDialog;
import epros.designer.tree.JecnTreeNode;
import epros.draw.event.JecnOftenActionProcess;
import epros.draw.gui.top.optionpane.JecnOptionPane;
import epros.draw.gui.workflow.JecnDrawMainPanel;
import epros.draw.gui.workflow.tabbedPane.JecnDrawDesktopPane;
import epros.draw.system.data.JecnUserCheckInfoData;
import epros.draw.util.DrawCommon;
import epros.draw.util.JecnUserCheckUtil;

public class JecnValidateCommon {

	/**
	 * @author yxw 2012-5-8
	 * @description:验证输入的名称是否正确 名称不能为空 名称不能包括任意字符之一: \ / : * ? < > | # %
	 *                          名称不能超过122个字符或61个汉字
	 * @param name
	 * @param jLable
	 * @return 正确返回true 不正确定返回false
	 */
	public static boolean validateName(String name, JLabel jLable) {
		String s = JecnUserCheckUtil.checkName(name);
		if (!DrawCommon.isNullOrEmtryTrim(s)) {
			jLable.setText(s);
			return false;
		}
		// s=JecnUserCheckUtil.checkNameLength(name);
		// if(!Tool.isNullOrEmtryTrim(s)){
		// jLable.setText(s);
		// return false;
		// }
		jLable.setText("");
		return true;
	}

	/**
	 * @author yxw 2013-1-10
	 * @description:验证输入的名称是否正确 名称不能为空 名称不能超过122个字符或61个汉字
	 * @param name
	 * @param jLable
	 * @return 正确返回true 不正确定返回false
	 */
	public static boolean validateNameNoRestrict(String name, JLabel jLable) {
		String s = "";
		if (DrawCommon.isNullOrEmtryTrim(name)) {
			// 名称不能为空
			s = JecnUserCheckInfoData.getNameNotNull();
		} else if (JecnUserCheckUtil.getTextLength(name) > 122) {
			// 名称不能超过122个字符或61个汉字
			s = JecnUserCheckInfoData.getNameLengthInfo();
		}
		if (!DrawCommon.isNullOrEmtryTrim(s)) {
			jLable.setText(s);
			return false;
		}
		// s=JecnUserCheckUtil.checkNameLength(name);
		// if(!Tool.isNullOrEmtryTrim(s)){
		// jLable.setText(s);
		// return false;
		// }
		jLable.setText("");
		return true;
	}

	/**
	 * @author yxw 2012-5-8
	 * @description:判断给定参数是否超过122个字符或60个汉字
	 * @param content
	 * @param jLable
	 * @return true 校验不通过 false校验通过
	 */
	public static boolean validateFieldNotPass(String text, JLabel verfyLab, String tipName) {
		if (DrawCommon.checkNameMaxLength(text)) {
			verfyLab.setText(tipName + JecnProperties.getValue("lengthNotBaiOut"));
			return true;
		}
		return false;

	}

	/**
	 * @author yxw 2012-5-8
	 * @description:判断给定参数是否超过1200个字符或600个汉字
	 * @param content
	 * @param jLable
	 * @return true 校验不通过 false校验通过
	 */
	public static boolean validateAreaNotPass(String content, JLabel verfyLab, String tipName) {
		if (DrawCommon.checkNoteLength(content)) {
			verfyLab.setText(tipName + JecnProperties.getValue("lengthNotOut"));
			return true;
		}
		return false;

	}
	
	
	/**
	 * @description:判断给定参数是否超过1200个字符或600个汉字
	 * @param content
	 * @param jLable
	 * @return true 校验不通过 false校验通过
	 */
	public static boolean validateAreaNotPass(String content, String tipName) {
		if (DrawCommon.checkNoteLength(content)) {
			JecnOptionPane.showMessageDialog(null, tipName + JecnProperties.getValue("lengthNotOut"));
			return true;
		}
		return false;

	}
	
	
	/**
	 * @description: 验证输入内容不能大于5000个汉字或10000字符
	 * @param content
	 * @param jLable
	 * @return true 校验不通过 false校验通过
	 */
	public static boolean validateAreaNotMaxPass(String content,String tipName) {
		if (DrawCommon.checkNoteMaxLength(content)) {
			JecnOptionPane.showMessageDialog(null, tipName + JecnProperties.getValue("lengthNotMaxOut"));
			return true;
		}
		return false;

	}

	/**
	 * @author yxw 2012-5-8
	 * @description:判断给定参数是否超过1200个字符或600个汉字
	 * @param content
	 * @param jLable
	 * @return true 正确 false超出
	 */
	public static boolean validateContent(String content, JLabel jLable) {

		String s = JecnUserCheckUtil.checkNoteLength(content);
		if (!DrawCommon.isNullOrEmtryTrim(s)) {
			jLable.setText(s);
			return false;
		}
		jLable.setText("");
		return true;

	}

	/**
	 * 检查角色和活动是否超出范围
	 * 
	 * @param roles
	 *            角色最大数
	 * @param actives
	 *            活动最大数
	 * @return true 有限制条件
	 */
	public static boolean checkRoleAndActiveLimit(int roles, int actives) {
		// 查询流程基本信息
		List<JecnConfigItemBean> listConfigItem = JecnConfigTool.getProcessOtherItems();
		List<String> strList = new ArrayList<String>();
		for (JecnConfigItemBean confiBean : listConfigItem) {
			if (DrawCommon.isNullOrEmtryTrim(confiBean.getValue())) {
				continue;
			}
			int value = Integer.valueOf(confiBean.getValue()).intValue();
			String error = null;
			if (confiBean.getMark().equals(ConfigItemPartMapMark.basicRoleMax.toString())) { // 角色上限
				if (roles > value) {// 角色个数超过上限！
					error = JecnProperties.getValue("basicRoleMax");
				}
			} else if (confiBean.getMark().equals(ConfigItemPartMapMark.basicRoleMin.toString())) { // 角色下限
				if (roles < value) {// "角色个数低于下限！"
					error = JecnProperties.getValue("basicRoleMin");
				}
			} else if (confiBean.getMark().equals(ConfigItemPartMapMark.basicRoundMax.toString())) { // 活动上限
				if (actives > value) {// "活动个数超过上限！"
					error = JecnProperties.getValue("basicRoundMax");
				}
			} else if (confiBean.getMark().equals(ConfigItemPartMapMark.basicRoundMin.toString())) { // 活动下限
				if (actives < value) {// "活动个数低于下限！"
					error = JecnProperties.getValue("basicRoundMin");
				}
			}
			if (!DrawCommon.isNullOrEmtryTrim(error)) {
				strList.add(error);
			}
		}
		if (strList.size() > 0) {
			PublicFileRequiredDialog requiredDialog = new PublicFileRequiredDialog(strList);
			requiredDialog.setVisible(true);
			return true;
		}
		return false;
	}

	/**
	 * true保存成功返回boolean true
	 * 
	 * @param selectNode
	 * @return true保存成功返回boolean true;
	 */
	public static boolean isSaveMap(JecnTreeNode selectNode) {
		JecnDrawDesktopPane desktopPane = JecnDrawMainPanel.getMainPanel().getWorkflow();
		if (selectNode == null || desktopPane == null) {
			return false;
		}
		if (desktopPane.getFlowMapData().isSave()) {
			int save = JecnOptionPane.showConfirmDialog(JecnDrawMainPanel.getMainPanel(), JecnDrawMainPanel
					.getMainPanel().getResourceManager().getValue("optionInfo"), null, JecnOptionPane.YES_NO_OPTION,
					JecnOptionPane.INFORMATION_MESSAGE);
			if (save == JecnOptionPane.OK_OPTION) {// 提示是否保存，点击是
				boolean isSave = JecnOftenActionProcess.saveWokfFlow(desktopPane);
				if (!isSave) {// 保存弹出框点击取消
					return false;
				}
			} else {
				return false;
			}
		}
		return true;
	}
}
