package epros.draw.event;

import java.awt.event.ActionEvent;

import epros.draw.constant.JecnToolBoxConstant.MapType;
import epros.draw.constant.JecnToolbarConstant.ToolBarElemType;
import epros.draw.gui.operationConfig.ConfigFileInstructionsDialog;
import epros.draw.gui.operationConfig.ProcessOperationIntrusDialog;
import epros.draw.gui.operationConfig.unit.RtfCommon;
import epros.draw.gui.workflow.JecnDrawMainPanel;
import epros.draw.gui.workflow.tabbedPane.JecnDrawDesktopPane;

/**
 * 
 * 工具栏流程操作说明大类中动作处理类
 * 
 * @author ZHOUXY
 * 
 */
public class JecnOperActionProcess extends JecnAbstractActionProcess {
	public JecnOperActionProcess() {

	}

	public void actionPerformed(ActionEvent e, ToolBarElemType toolBarElemType) {
		if (toolBarElemType == null) {
			return;
		}
		JecnDrawDesktopPane drawDesktopPane = JecnDrawMainPanel.getMainPanel()
				.getWorkflow();

		switch (toolBarElemType) {
		case operSetItem:// 配置流程操作说明
			ConfigFileInstructionsDialog configFileInstructionsDialog = new ConfigFileInstructionsDialog();
			configFileInstructionsDialog.setVisible(true);
			break;
		case operDownload:// 下载流程操作说明
			if (drawDesktopPane == null
					|| drawDesktopPane.getFlowMapData().getMapType() != MapType.partMap) {
				return;
			}
			RtfCommon.downLoadMenuMouseClicked();
			break;
		case operEdit: // 编辑流程操作说明
			if (drawDesktopPane == null
					|| drawDesktopPane.getFlowMapData().getMapType() != MapType.partMap) {
				return;
			}
			ProcessOperationIntrusDialog processOperationIntrusDialog = new ProcessOperationIntrusDialog();
			processOperationIntrusDialog.setVisible(true);
			break;
		}
	}
}
