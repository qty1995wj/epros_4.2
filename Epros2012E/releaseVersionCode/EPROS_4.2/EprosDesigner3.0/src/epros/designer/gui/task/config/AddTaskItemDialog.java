package epros.designer.gui.task.config;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JPanel;

import com.jecn.epros.server.bean.task.TaskConfigItem;

import epros.designer.util.JecnProperties;
import epros.draw.gui.top.dialog.JecnDialog;
import epros.draw.util.JecnUIUtil;

/**
 * 
 * 点击加入按钮弹出的确认对话框
 * 
 * @author ZHOUXY
 * 
 */
public class AddTaskItemDialog extends JecnDialog {
	/** 配置界面 */
	private TaskConfigDialog dialog = null;
	/** 主面板 */
	private JPanel mainPanel = null;
	/** 按钮面板**/
	private JPanel buttonPanel=null;
	
	private TaskConfigTableScrollPane tableScrollPane;
	private JButton okBut;
	private JButton cancelBut;

	public AddTaskItemDialog(TaskConfigDialog dialog) {
		super(dialog);
		this.dialog = dialog;

		initComponents();
		initLayout();
		initAcion();
		initData();
		
	}

	private void initAcion() {
		
		okBut.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				
				List<TaskConfigItem> needShowItem = tableScrollPane.getSelectedItemBeanList();
				for(TaskConfigItem item:needShowItem){
					item.setValue("1");
				}
				List<TaskConfigItem> showConfigItems = dialog.getTaskEditPanel().getTaskTablePanel().getShowConfigItems(false);
				dialog.getTaskEditPanel().getTaskTablePanel().initData(showConfigItems);
				AddTaskItemDialog.this.dispose();
			}
		});
		
		cancelBut.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				AddTaskItemDialog.this.dispose();
			}
		});
		
	}

	private void initData() {


		List<TaskConfigItem> configs=dialog.getTaskEditPanel().getTaskTablePanel().getHiddenConfigItems();
		tableScrollPane.initData(configs);
	}

	private void initLayout() {
		
		buttonPanel.setLayout(new FlowLayout(FlowLayout.RIGHT));
		
		buttonPanel.add(okBut);
		buttonPanel.add(cancelBut);
		
		mainPanel.setLayout(new BorderLayout());
		mainPanel.add(tableScrollPane, BorderLayout.CENTER);
		mainPanel.add(buttonPanel, BorderLayout.SOUTH);
		
		// 大小
		Dimension size = new Dimension(390, 415);
		this.setPreferredSize(size);
		this.setMinimumSize(size);
		// 模态
		this.setModal(true);
		// 背景颜色
		this.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		// 关闭处理模式
		this.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		//
		this.setLocationRelativeTo(dialog);

		this.getContentPane().add(mainPanel, BorderLayout.CENTER);
		
	}

	/**
	 * 
	 * 初始化
	 * 
	 */
	protected void initComponents() {
		// 主面板
		mainPanel = new JPanel();
		// 确认取消按钮面板
		buttonPanel = new JPanel();
		// 表面板
		tableScrollPane = new TaskConfigTableScrollPane(dialog);
		// 确定按钮
		okBut=new JButton(JecnProperties.getValue("okBtn"));
		cancelBut=new JButton(JecnProperties.getValue("cancelBtn"));
	
	}

}
