<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%
	response.setHeader("Pragma", "No-cache");
	response.setHeader("Cache-Control", "no-cache");
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	</head>

	<body>
		<div id="div" class="div" >
			<div align="right" style="margin-top: 5px; margin-right: 25px;">
				<table>
					<tr align="right">
						<td align="right" style="padding-right: 15px;">
							<s:text name="total"></s:text>
							<span id="allOrgTotal"></span>
						</td>
						<td align="right" style="color: #FB1A1C; padding-right: 5px;">
							<s:text name="generationToBuildRule"></s:text>
							<span id="noPubRuleTotal"></span>
						</td>
						<td align="right" style="color: #e7a100; padding-right: 5px;">
							<s:text name="forApprovalRule"></s:text>
							<span id="approveRuleTotal"></span>
						</td>
						<td align="right" style="color: #00c317; padding-right: 5px;">
							<s:text name="releaseRule"></s:text>
							<span id="pubRuleTotal"></span>
						</td>
						<td>
							<!--
							<img src="${basePath}images/allShow.gif" style="cursor: pointer;"
								target='_blank' class="table" onclick="showAllRuleSysList()" />
							 -->
							 <s:if test="#session.webLoginBean.isAdmin||#session.webLoginBean.isViewAdmin">
								<img src="${basePath}images/download.gif" style="cursor: pointer;"
									target='_blank' class="table" onclick="downloadRuleSysList()" />
							 </s:if>
						</td>
					</tr>
				</table>
			</div>

			<div id="ruleSysListShow_id">
				
				
			</div>
			<input type="hidden" id="ruleId" />
		</div>
	</body>
</html>