/**
 * CallbackResponse.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.jecn.webservice.jinke.taskPub;

public class CallbackResponse  implements java.io.Serializable {
    private com.jecn.webservice.jinke.taskPub.ServiceResult callbackResult;

    public CallbackResponse() {
    }

    public CallbackResponse(
           com.jecn.webservice.jinke.taskPub.ServiceResult callbackResult) {
           this.callbackResult = callbackResult;
    }


    /**
     * Gets the callbackResult value for this CallbackResponse.
     * 
     * @return callbackResult
     */
    public com.jecn.webservice.jinke.taskPub.ServiceResult getCallbackResult() {
        return callbackResult;
    }


    /**
     * Sets the callbackResult value for this CallbackResponse.
     * 
     * @param callbackResult
     */
    public void setCallbackResult(com.jecn.webservice.jinke.taskPub.ServiceResult callbackResult) {
        this.callbackResult = callbackResult;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof CallbackResponse)) return false;
        CallbackResponse other = (CallbackResponse) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.callbackResult==null && other.getCallbackResult()==null) || 
             (this.callbackResult!=null &&
              this.callbackResult.equals(other.getCallbackResult())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getCallbackResult() != null) {
            _hashCode += getCallbackResult().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(CallbackResponse.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://tempuri.org/", ">CallbackResponse"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("callbackResult");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/", "CallbackResult"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://tempuri.org/", "ServiceResult"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
