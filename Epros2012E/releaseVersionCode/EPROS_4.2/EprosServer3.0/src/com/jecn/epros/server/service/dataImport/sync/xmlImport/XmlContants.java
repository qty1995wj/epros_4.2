package com.jecn.epros.server.service.dataImport.sync.xmlImport;

import java.util.ArrayList;
import java.util.List;

import com.jecn.epros.server.bean.popedom.JecnFlowOrg;
import com.jecn.epros.server.webBean.dataImport.ExcelConfigBean;
import com.jecn.epros.server.webBean.dataImport.OrgExcelBean;

public class XmlContants {

	private static List<ExcelConfigBean> excelFileList;// 人员岗位
	private static List<OrgExcelBean> orgExcelList;
	private static List<JecnFlowOrg> treeList;
	private static List<String> loginNames;
	public static List<ExcelConfigBean> sameNames = new ArrayList<ExcelConfigBean>();

	public final static String ADMIN_ROLE = "admin";// 系统管理员
}
