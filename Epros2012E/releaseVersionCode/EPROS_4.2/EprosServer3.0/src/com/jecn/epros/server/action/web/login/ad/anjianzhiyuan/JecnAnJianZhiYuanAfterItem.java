package com.jecn.epros.server.action.web.login.ad.anjianzhiyuan;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Hashtable;
import java.util.Properties;

import javax.naming.AuthenticationException;
import javax.naming.Context;
import javax.naming.NamingException;
import javax.naming.directory.DirContext;
import javax.naming.directory.InitialDirContext;

import org.apache.log4j.Logger;

import com.jecn.epros.server.common.JecnCommon;
import com.jecn.epros.server.util.JecnPath;

/**
 * 安健致远配置读取类 (cfgFile/anjianzhiyuan/anJianZhiYuanLdapServerConfig.properties)
 * 
 */
public class JecnAnJianZhiYuanAfterItem {
	private static Logger log = Logger.getLogger(JecnAnJianZhiYuanAfterItem.class);

	// Ldap 服务器地址
	public static String LDAP_URL = null;
	// Ldap 权限验证类型
	public static String AUTH_TYPE = null;

	/**
	 * 读取安健致远配置文件
	 * 
	 * @author weidp
	 * @date 2014-9-3 下午05:27:17
	 */
	public static void start() {

		log.info("开始读取配置文件内容");

		Properties props = new Properties();
		String propsURL = JecnPath.CLASS_PATH
				+ "cfgFile/anjianzhiyuan/anJianZhiYuanLdapServerConfig.properties";
		log.info("配置文件地址：" + propsURL);
		FileInputStream in = null;
		try {
			in = new FileInputStream(propsURL);
			props.load(in);

			// Ldap 服务器地址
			String ldapURL = props.getProperty("ldapURL");
			if (!JecnCommon.isNullOrEmtryTrim(ldapURL)) {
				JecnAnJianZhiYuanAfterItem.LDAP_URL = ldapURL;
			}

			// Ldap 权限验证类型
			String authType = props.getProperty("authType");
			if (!JecnCommon.isNullOrEmtryTrim(authType)) {
				JecnAnJianZhiYuanAfterItem.AUTH_TYPE = authType;
			}

		} catch (FileNotFoundException e) {
			log.error("没有找到配置文件：", e);
		} catch (IOException e) {
			log.error("读取配置文件异常：", e);
		} finally {
			if (in != null) {
				try {
					in.close();
				} catch (IOException e1) {
					log.error("释放流异常：", e1);
				}
			}
		}
	}
	
	/**
	 * 获取Ldap验证环境
	 * 
	 * @author weidp
	 * @date 2014-8-27 下午03:00:46
	 * @param account
	 *            用户在Epros登录页面输入的账号
	 * @param password
	 *            密码
	 * @return
	 */
	public static Hashtable<String, String> getLdapEnv(String account, String password) {
		Hashtable<String, String> env = new Hashtable<String, String>();
		env.put(Context.INITIAL_CONTEXT_FACTORY, "com.sun.jndi.ldap.LdapCtxFactory");
		env.put(Context.PROVIDER_URL, JecnAnJianZhiYuanAfterItem.LDAP_URL);
		env.put(Context.SECURITY_AUTHENTICATION, JecnAnJianZhiYuanAfterItem.AUTH_TYPE);
		env.put(Context.SECURITY_PRINCIPAL, account);
		env.put(Context.SECURITY_CREDENTIALS, password);
		return env;
	}
	
	/**
	 * 进行Ldap权限验证
	 * 
	 * @author weidp
	 * @date 2014-9-3 下午04:37:17
	 * @return 0 成功验证 1 验证失败（ 提示用户名或密码不正确） 2 连接Ldap服务器异常
	 */
	public static int doLdapAuthority(String ldapAccount, String ldapPassword) {
		DirContext ctx = null;
		try {
			// 账号或密码为空
			if (JecnCommon.isNullOrEmtryTrim(ldapAccount) || JecnCommon.isNullOrEmtryTrim(ldapPassword)) {
				return 1;
			}

			// 获取Ldap验证环境
			Hashtable<String, String> ldapEnv = JecnAnJianZhiYuanAfterItem.getLdapEnv(ldapAccount, ldapPassword);
			ctx = new InitialDirContext(ldapEnv);
		} catch (AuthenticationException ex) {
			log.error("Ldap权限验证失败：", ex);
			return 1;
		} catch (NamingException e) {
			log.error("连接Ldap服务器出现异常：", e);
			return 2;
		} catch (Exception e) {
			log.error("其他异常：", e);
			return 2;
		} finally {
			if (ctx != null) {
				try {
					ctx.close();
				} catch (NamingException e) {
					log.error("释放资源出现异常：", e);
				}
			}
		}
		return 0;
	}
}
