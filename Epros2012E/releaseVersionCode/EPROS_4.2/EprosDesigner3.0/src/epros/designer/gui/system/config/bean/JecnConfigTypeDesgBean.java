package epros.designer.gui.system.config.bean;

import java.util.ArrayList;
import java.util.List;

import com.jecn.epros.server.bean.system.JecnConfigItemBean;

import epros.draw.util.DrawCommon;

/**
 * 
 * 后台数据分类管理类（0：审批环节配置 1：任务界面显示项配置 2：基本配置 3：流程图建设规范 4：流程文件 5：邮箱配置 6：权限配置）
 * 
 * @author ZHOUXY
 * 
 */
public class JecnConfigTypeDesgBean {
	/** 0：审批环节配置 1：任务界面显示项配置 2：基本配置 3：流程图建设规范 4：流程文件 5：邮箱配置 6：权限配置 */
	private int type = -1;

	/** 后台数据 */
	private List<JecnConfigItemBean> itemList = new ArrayList<JecnConfigItemBean>();

	/** 显示在table表中数据集合 */
	private List<JecnConfigItemBean> tableItemList = new ArrayList<JecnConfigItemBean>();
	/** 显示在非table表中数据集合 */
	private List<JecnConfigItemBean> otherItemList = new ArrayList<JecnConfigItemBean>();

	public JecnConfigTypeDesgBean(int type) {
		this.type = type;
	}

	public int getType() {
		return type;
	}

	public List<JecnConfigItemBean> getTableItemList() {
		return tableItemList;
	}

	public List<JecnConfigItemBean> getOtherItemList() {
		return otherItemList;
	}

	/**
	 * 
	 * 添加数据到itemList中
	 * 
	 * @param itemBean
	 *            JecnConfigItemBean
	 */
	public void addItemList(JecnConfigItemBean itemBean) {
		if (itemBean != null) {
			itemList.add(itemBean);
			if (1 == itemBean.getIsTableData()) {// 是否是表中数据：是：1；否0 （默认：否）
				tableItemList.add(itemBean);
			} else {
				otherItemList.add(itemBean);
			}
		}
	}

	/**
	 * type值为(0：审批环节配置 1：任务界面显示项配置 2：基本配置 3：流程图建设规范 4：流程文件 5：邮箱配置
	 * 6：权限配置)合法，其他都不合法
	 * 
	 * @return boolean true:合法；false：不合法
	 */
	public boolean isTypeRightFul() {
		// 0：审批环节配置 1：任务界面显示项配置 2：基本配置 3：流程图建设规范 4：流程文件 5：邮箱配置
		// 6：权限配置7:任务操作8：消息邮件配置
		switch (type) {
		case 0:
		case 1:
		case 2:
		case 3:
		case 4:
		case 5:
		case 6:
		case 7:
		case 8:
		case 9:
		case 10:// 流程其它配置
		case 11:// 发布配置
		case 16:// 流程KPI配置
			return true;
		default:
			return false;
		}
	}

	/**
	 * 
	 * 是否有数据
	 * 
	 * @return boolean true:存在；false：不存在
	 */
	public boolean existsItemListData() {
		return (itemList == null || itemList.size() == 0) ? false : true;
	}

	/**
	 * 
	 * 默认值替换当前值
	 * 
	 */
	public void defaultDataToData() {
		for (JecnConfigItemBean itemBean : itemList) {
			// 名称
			itemBean.setName(itemBean.getDefaultName());
			// 值
			itemBean.setValue(itemBean.getDefaultValue());
			// 排序
			itemBean.setSort(itemBean.getDefaultSort());
			// 必填
			itemBean.setIsEmpty(itemBean.getDefaultIsEmpty());
			// 英文名
			itemBean.setEnName(itemBean.getEnDefaultName());
		}
	}

	/**
	 * 
	 * 判断给定标识是否在集合中是否存在，存在时返回JecnConfigItemBean对象；否则返回NULL
	 * 
	 * @param showITemBeanList
	 *            List<JecnConfigItemBean> 被查找的集合
	 * @param mark
	 *            String 唯一标识
	 * @return JecnConfigItemBean NULL或对象
	 */
	public JecnConfigItemBean getConfigItemBeanByMark(List<JecnConfigItemBean> showITemBeanList, String mark) {
		if (showITemBeanList == null || showITemBeanList.size() == 0 || DrawCommon.isNullOrEmtryTrim(mark)) {
			return null;
		}

		for (JecnConfigItemBean itemBean : showITemBeanList) {
			if (mark.equals(itemBean.getMark())) {// 是否给定的唯一标识
				return itemBean;
			}
		}
		return null;
	}

	/**
	 * 
	 * 判断给定标识是否在集合中是否存在且必填，存在且必填时返回JecnConfigItemBean对象；否则返回NULL
	 * 
	 * 
	 * @param showITemBeanList
	 *            List<JecnConfigItemBean>
	 * @param mark
	 *            mark
	 * @return JecnConfigItemBean JecnConfigItemBean对象或NULL
	 */
	public JecnConfigItemBean getConfigItemBeanNotEmptyByMark(List<JecnConfigItemBean> showITemBeanList, String mark) {
		if (showITemBeanList == null || showITemBeanList.size() == 0 || DrawCommon.isNullOrEmtryTrim(mark)) {
			return null;
		}

		for (JecnConfigItemBean itemBean : showITemBeanList) {
			if (mark.equals(itemBean.getMark())) {// 是否给定的唯一标识
				if (itemBean.isNotEmpty()) {// true：必填；false：非必填
					return itemBean;
				}
			}
		}
		return null;
	}

	/**
	 * 
	 * 判断给定标识是否在集合中是否存在，存在时判断其后是否至少存在一个记录B，存在返回true：不存在返回false
	 * 
	 * @param showITemBeanList
	 *            List<JecnConfigItemBean> 被查找的集合
	 * @param mark
	 *            String 唯一标识
	 * @return boolean 对象存在且其对象之后还存在对象返回true；其他情况返回false
	 * 
	 */
	public boolean getNextItemBeanByMark(List<JecnConfigItemBean> showITemBeanList, String mark) {
		if (showITemBeanList == null || showITemBeanList.size() == 0 || DrawCommon.isNullOrEmtryTrim(mark)) {
			return false;
		}

		// 给定的审批环节存在标识：true：存在；false：不存在
		boolean paraMarkExistsFlag = false;
		for (JecnConfigItemBean itemBean : showITemBeanList) {
			if (paraMarkExistsFlag) {
				// 给定审批环节后是否存在审批环节：true：存在；false：不存在
				return true;
			}
			if (mark.equals(itemBean.getMark())) {
				paraMarkExistsFlag = true;
			}
		}
		if (!paraMarkExistsFlag) {// 没有评审人会审，返回true
			return true;
		}

		return false;
	}

	/**
	 * 
	 * 判断给定标识是否在集合中是否存在且必填，存在且必填时判断其后是否至少存在一个记录B且必填，满足条件返回true：不满足条件返回false
	 * 
	 * @param showITemBeanList
	 *            List<JecnConfigItemBean> 被查找的集合
	 * @param mark
	 *            String 唯一标识
	 * @return boolean 对象存在且其对象之后还存在对象返回true；其他情况返回false
	 * 
	 */
	public boolean getNextItemBeanNotEmptyByMark(List<JecnConfigItemBean> showITemBeanList, String mark) {
		if (showITemBeanList == null || showITemBeanList.size() == 0 || DrawCommon.isNullOrEmtryTrim(mark)) {
			return false;
		}

		// 评审人会审必填标识：true：必填；false：非必填
		boolean paraMarkEmptyFlag = false;
		// 给定的审批环节存在标识：true：存在；false：不存在
		boolean paraMarkExistsFlag = false;

		for (JecnConfigItemBean itemBean : showITemBeanList) {
			if (paraMarkExistsFlag) {// 存在
				if (paraMarkEmptyFlag) {// 必填
					if (itemBean.isNotEmpty()) {// 评审人会审存在且必填，其后至少要存在一个审核人且必填
						return true;
					}
				} else {
					// 给定审批环节后是否存在审批环节：true：存在；false：不存在
					return true;
				}
			}
			if (mark.equals(itemBean.getMark())) {
				if (itemBean.isNotEmpty()) {// true：必填；false：非必填
					paraMarkEmptyFlag = true;
				}
				paraMarkExistsFlag = true;
			}
		}

		if (!paraMarkExistsFlag) {// 没有评审人会审，返回true
			return true;
		}

		return false;
	}

	public List<JecnConfigItemBean> getItemList() {
		return itemList;
	}
}
