package com.jecn.viewdoc.convert.view;

import java.util.Set;

/**
 * 预览的接口
 * 
 * @author hyl
 * 
 */
public interface View {

	/**
	 * 是否允许预览
	 * 
	 * @return
	 */
	public boolean allowView();

	/**
	 * 预览的地址
	 * 
	 * @return
	 */
	public String getPath();

	/**
	 * 预览的文件名称
	 * 
	 * @return
	 */
	public String getFileName();

	/**
	 * 支持的预览格式，后缀小写
	 * 
	 * @return
	 */
	public Set<String> allowViewExtensions();

	/**
	 * 后缀是否支持预览
	 * 
	 * @param extension
	 * @return
	 */
	public boolean allowViewExtension(String extension);

	/**
	 * 当前预览的文件是否支持预览
	 * 
	 * @return
	 */
	public boolean allowViewExtension();

}
