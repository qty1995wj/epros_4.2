package com.jecn.epros.server.webBean.reports;

import com.jecn.epros.server.webBean.process.ProcessWebBean;

/**
 * 流程访问数和访问人Bean
 * 
 * @author fuzhh Feb 28, 2013
 * 
 */
public class ProcessAccessBean {

	/**流程基本信息*/
	private ProcessWebBean processWebBean;
	/** 访问次数 */
	private int accessCount;
	/** 访问人 */
	private String accessName;
	/** 访问人ID */
	private long accessId;
	/** 访问时间 */
	private String accessTime;
	
	public ProcessWebBean getProcessWebBean() {
		return processWebBean;
	}
	public void setProcessWebBean(ProcessWebBean processWebBean) {
		this.processWebBean = processWebBean;
	}
	public int getAccessCount() {
		return accessCount;
	}
	public void setAccessCount(int accessCount) {
		this.accessCount = accessCount;
	}
	public String getAccessName() {
		return accessName;
	}
	public void setAccessName(String accessName) {
		this.accessName = accessName;
	}
	public long getAccessId() {
		return accessId;
	}
	public void setAccessId(long accessId) {
		this.accessId = accessId;
	}
	public String getAccessTime() {
		return accessTime;
	}
	public void setAccessTime(String accessTime) {
		this.accessTime = accessTime;
	}
}
