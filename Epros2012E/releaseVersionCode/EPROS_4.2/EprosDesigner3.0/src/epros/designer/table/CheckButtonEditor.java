/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package epros.designer.table;

import java.awt.Component;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

import javax.swing.DefaultCellEditor;
import javax.swing.JCheckBox;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

import epros.designer.gui.common.JecnRecycleManageDialog;
import epros.designer.gui.common.JecnRecycleManageDialog.RecyleTable;

public class CheckButtonEditor extends DefaultCellEditor implements ItemListener {
    private static final long serialVersionUID = 198567563L;
    private JCheckBox button;
    private JecnRecycleManageDialog recycleDialog;
    /**全选框是否选中标识*/
    private boolean isFlag = false;
    /**回收站数据表格*/
    private RecyleTable recyTable;
    private DefaultTableModel model;
    private int countModel;

    public CheckButtonEditor(JCheckBox checkBox,JecnRecycleManageDialog recycleDialog,RecyleTable recyTable) {
        super(checkBox);
    	this.recycleDialog = recycleDialog;
    	this.recyTable = recyTable;
    }

    public Component getTableCellEditorComponent(JTable table, Object value,
            boolean isSelected, int row, int column) {
        if (value == null)
            return null;
        button = (JCheckBox) value;
        button.addItemListener(this);
        return (Component) value;
    }

    public Object getCellEditorValue() {
        button.removeItemListener(this);
        return button;
    }

    public void itemStateChanged(ItemEvent e) {
    	super.fireEditingStopped();
    	model = (DefaultTableModel) this.recyTable.getModel();
		// 判断复选框是否有选中的，没有选中提示，请勾选复选框
		countModel = model.getRowCount();
    	//取消选中状态
        if(e.getStateChange() == e.DESELECTED){
        	//取消全选框选中状态
        	recycleDialog.getSelectAllBox().setSelected(false);
        }
        else{
        	//未选中的行数
        	int selectNum = 0;
        	for (int k = 0; k < countModel; k++) {
    			if (!((JCheckBox) model.getValueAt(k, 0)).isSelected()) {
    				selectNum++;
    			}
    		}
        	if(selectNum  >= 1){
        		isFlag = true;
        	}
        	if(!isFlag){
        		//设置全选框选中
        		recycleDialog.getSelectAllBox().setSelected(true);
        	}
        }
        isFlag = false;
    }
}