package epros.draw.gui.line;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Stroke;

import javax.swing.JPanel;

import epros.draw.gui.workflow.JecnDrawMainPanel;
import epros.draw.gui.workflow.tabbedPane.JecnDrawDesktopPane;
import epros.draw.util.JecnUIUtil;

/**
 * 页数设置
 * 
 * @author ZHANGXIAOHU
 * @date： 日期：2014-2-17 时间：下午02:53:32
 */
public class JecnPageSetLine extends JPanel {

	private int intX;
	/**
	 * 线条宽度
	 * 
	 */
	private int lineWidth = 2;

	public void paintComponent(Graphics g) {

		// 定义画笔
		Graphics2D g2d = (Graphics2D) g;
		// 备份
		Stroke oldStroke = g2d.getStroke();

		// 呈现和图像处理服务管理对象
		g2d.setRenderingHints(JecnUIUtil.getRenderingHints());
		// 线条颜色
		g2d.setColor(Color.RED);

		// 线条样式
		g2d.setStroke(new BasicStroke(2));

		int height = getWorkHeight();
		// 画单线 : 竖线
		g2d.drawLine(0, 0, 0, height);
		// 还原备份
		g2d.setStroke(oldStroke);
	}

	/**
	 * 画图面板高度
	 * 
	 * @return
	 */
	private int getWorkHeight() {
		JecnDrawDesktopPane desktopPane = JecnDrawMainPanel.getMainPanel()
				.getWorkflow();
		if (desktopPane == null) {
			return 0;
		}
		return desktopPane.getFlowMapData().getWorkflowHeight();
	}

	/**
	 * 
	 * 设置位置点和大小
	 * 
	 */
	public void setVHLineBounds() {
		JecnDrawDesktopPane desktopPane = JecnDrawMainPanel.getMainPanel()
				.getWorkflow();
		if (desktopPane == null) {
			return;
		}
		// 位置、大小
		this.setBounds(this.intX, 0, (int) (lineWidth * desktopPane
				.getWorkflowScale()), desktopPane.getFlowMapData()
				.getWorkflowHeight());
	}

	public int getIntX() {
		return intX;
	}

	public void setIntX(int intX) {
		this.intX = intX;
	}
}
