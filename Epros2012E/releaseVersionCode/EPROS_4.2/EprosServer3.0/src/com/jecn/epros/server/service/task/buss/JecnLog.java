package com.jecn.epros.server.service.task.buss;

import org.apache.log4j.Logger;

/**
 * 日志
 * 
 * @author ZHANGXIAOHU
 * @date： 日期：2013-12-24 时间：下午04:44:23
 */
public class JecnLog {
	private static Logger log = Logger.getLogger(JecnLog.class);

	public static Logger getLog() {
		return log;
	}
}
