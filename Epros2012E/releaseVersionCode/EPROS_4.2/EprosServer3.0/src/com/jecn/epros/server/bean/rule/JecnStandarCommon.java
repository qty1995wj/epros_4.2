package com.jecn.epros.server.bean.rule;
/***
 * 标准表单
 * 2013-11-22
 *
 */
public class JecnStandarCommon  implements java.io.Serializable{
	private Long standarId;//标准ID
	private String standarName;//标准名称
	private String standaType;//标准类型
	private Long ruleTitleId;//制度标题ID
	private Long relatedId;//关联ID
	private String pName;
	private String tPath;
	
	public String gettPath() {
		return tPath;
	}
	public void settPath(String tPath) {
		this.tPath = tPath;
	}
	public String getpName() {
		return pName;
	}
	public void setpName(String pName) {
		this.pName = pName;
	}
	public Long getStandarId() {
		return standarId;
	}
	public void setStandarId(Long standarId) {
		this.standarId = standarId;
	}
	public String getStandarName() {
		return standarName;
	}
	public void setStandarName(String standarName) {
		this.standarName = standarName;
	}
	public Long getRuleTitleId() {
		return ruleTitleId;
	}
	public void setRuleTitleId(Long ruleTitleId) {
		this.ruleTitleId = ruleTitleId;
	}
	public String getStandaType() {
		return standaType;
	}
	public void setStandaType(String standaType) {
		this.standaType = standaType;
	}
	public Long getRelatedId() {
		return relatedId;
	}
	public void setRelatedId(Long relatedId) {
		this.relatedId = relatedId;
	}
	
}
