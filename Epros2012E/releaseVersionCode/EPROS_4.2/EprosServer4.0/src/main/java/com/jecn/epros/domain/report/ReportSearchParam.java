package com.jecn.epros.domain.report;

import io.swagger.annotations.ApiModelProperty;

import java.util.List;

public class ReportSearchParam {

    @ApiModelProperty("流程架构的id的集合，是一个list")
    private List<Long> mapIds;
    @ApiModelProperty("责任部门的id的集合，是一个list")
    private List<Long> orgIds;
    @ApiModelProperty("责任人的id的集合，是一个list")
    private List<Long> dutyIds;

    public List<Long> getMapIds() {
        return mapIds;
    }

    public void setMapIds(List<Long> mapIds) {
        this.mapIds = mapIds;
    }

    public List<Long> getOrgIds() {
        return orgIds;
    }

    public void setOrgIds(List<Long> orgIds) {
        this.orgIds = orgIds;
    }

    public List<Long> getDutyIds() {
        return dutyIds;
    }

    public void setDutyIds(List<Long> dutyIds) {
        this.dutyIds = dutyIds;
    }
}
