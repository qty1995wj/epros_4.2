package epros.draw.gui.swing;

import java.awt.Point;
import java.util.ArrayList;
import java.util.List;

import epros.draw.constant.JecnWorkflowConstant.EditPointType;
import epros.draw.data.line.JecnManhattanRouterResultData;
import epros.draw.gui.figure.shape.JecnEditPortPanel;

/**
 * 
 * 曼哈顿算法
 * 
 * 从连接线的起始点开始，一个点一个点计算
 * 
 * @author ZHOUXY
 */
public class JecnManhattanRouter {

	private int MINDIST = 20;
	private int count = 0;
	private Point oldPoint = null;
	/** 曼哈顿算法记录临时点 */
	private List<Point> tempList = new ArrayList<Point>();

	/**
	 * 
	 * 画线执行路由算法
	 * 
	 * @param fromPt
	 *            Point 开始点
	 * @param fromDir
	 *            EditPointType 开始点所在图形位置点方向
	 * @param toPt
	 *            结束点
	 * @param toDir
	 *            结束点所在图形位置点方向
	 * 
	 * @return List<JecnLineSegmentData> 小线段坐标集合
	 */
	public List<JecnManhattanRouterResultData> route(Point fromPt,
			EditPointType fromDir, Point toPt, EditPointType toDir) {
		if (fromPt == null || toPt == null || fromDir == null || toDir == null) {
			return null;
		}
		// 根据路由算法生成小线段临时数据对象集合
		List<JecnManhattanRouterResultData> lineSegmentDataList = new ArrayList<JecnManhattanRouterResultData>();
		// 执行曼哈顿路由算法
		route2(lineSegmentDataList, fromPt, fromDir, toPt, toDir);
		// 根据路由算法生成点的集合获取临时数据
		addSegmentData(lineSegmentDataList);
		// 原始点默认状态为空
		oldPoint = null;
		return lineSegmentDataList;
	}

	/**
	 * 
	 * 路由算法
	 * 
	 * 方向参照JecnWorkflowConstant类中EditPointType枚举
	 * 
	 * @param lineSegmentDataList
	 *            List<JecnLineSegmentData> 存储当前生成的线段坐标
	 * @param fromPt
	 *            Point 开始点
	 * @param fromDir
	 *            开始点方向
	 * @param toPt
	 *            结束点
	 * @param toDir
	 *            结束点方向
	 */
	private void route2(
			List<JecnManhattanRouterResultData> lineSegmentDataList,
			Point fromPt, EditPointType fromDir, Point toPt, EditPointType toDir) {
		count = count + 1;

		double tol = 0.1;
		double tolxtol = 0.01;

		int xDiff = fromPt.x - toPt.x;
		int yDiff = fromPt.y - toPt.y;
		Point point = new Point();
		EditPointType dir = null;

		if (((xDiff * xDiff) < tolxtol) && ((yDiff * yDiff) < tolxtol)) {
			addPoint(new Point(toPt.x, toPt.y));
			return;
		}
		if (count == 20) {
			return;
		}

		switch (fromDir) {
		case left:// 图形左边编辑点

			if ((xDiff > 0) && ((yDiff * yDiff) < tol)
					&& (toDir == EditPointType.right)) {
				point = toPt;
				dir = toDir;
			} else {
				if (xDiff < 0) {
					point = new Point(fromPt.x - MINDIST, fromPt.y);
				} else if (((yDiff > 0) && (toDir == EditPointType.bottom))
						|| ((yDiff < 0) && (toDir == EditPointType.top))) {
					point = new Point(toPt.x, fromPt.y);
				} else if (fromDir == toDir) {
					int pos = Math.min(fromPt.x, toPt.x) - MINDIST;
					point = new Point(pos, fromPt.y);
				} else {
					point = new Point(fromPt.x - (xDiff / 2), fromPt.y);
				}

				if (yDiff > 0) {
					dir = EditPointType.top;
				} else {
					dir = EditPointType.bottom;
				}
			}

			break;
		case right:// 图形右边编辑点

			if ((xDiff < 0) && ((yDiff * yDiff) < tol)
					&& (toDir == EditPointType.left)) {
				point = toPt;
				dir = toDir;
			} else {
				if (xDiff > 0) {
					point = new Point(fromPt.x + MINDIST, fromPt.y);
				} else if (((yDiff > 0) && (toDir == EditPointType.bottom))
						|| ((yDiff < 0) && (toDir == EditPointType.top))) {
					point = new Point(toPt.x, fromPt.y);
				} else if (fromDir == toDir) {
					int pos = Math.max(fromPt.x, toPt.x) + MINDIST;
					point = new Point(pos, fromPt.y);
				} else {
					point = new Point(fromPt.x - (xDiff / 2), fromPt.y);
				}

				if (yDiff > 0) {
					dir = EditPointType.top;
				} else {
					dir = EditPointType.bottom;
				}
			}

			break;
		case top:// 图形上边编辑点

			if (((xDiff * xDiff) < tol) && (yDiff > 0)
					&& (toDir == EditPointType.bottom)) {
				point = toPt;
				dir = toDir;
			} else {
				if (yDiff < 0) {
					point = new Point(fromPt.x, fromPt.y - MINDIST);
				} else if (((xDiff > 0) && (toDir == EditPointType.right))
						|| ((xDiff < 0) && (toDir == EditPointType.left))) {
					point = new Point(fromPt.x, toPt.y);
				} else if (fromDir == toDir) {
					int pos = Math.min(fromPt.y, toPt.y) - MINDIST;
					point = new Point(fromPt.x, pos);
				} else {
					point = new Point(fromPt.x, fromPt.y - (yDiff / 2));
				}

				if (xDiff > 0) {
					dir = EditPointType.left;
				} else {
					dir = EditPointType.right;
				}
			}

			break;
		case bottom:// 图形下边编辑点

			if (((xDiff * xDiff) < tol) && (yDiff < 0)
					&& (toDir == EditPointType.top)) {
				point = toPt;
				dir = toDir;
			} else {
				if (yDiff > 0) {
					point = new Point(fromPt.x, fromPt.y + MINDIST);
				} else if (((xDiff > 0) && (toDir == EditPointType.right))
						|| ((xDiff < 0) && (toDir == EditPointType.left))) {
					point = new Point(fromPt.x, toPt.y);
				} else if (fromDir == toDir) {
					int pos = Math.max(fromPt.y, toPt.y) + MINDIST;
					point = new Point(fromPt.x, pos);
				} else {
					point = new Point(fromPt.x, fromPt.y - (yDiff / 2));
				}

				if (xDiff > 0) {
					dir = EditPointType.left;
				} else {
					dir = EditPointType.right;
				}
			}

			break;
		default:
			break;
		}

		route2(lineSegmentDataList, point, dir, toPt, toDir);
		addPoint(fromPt);
		point = null;
	}

	/**
	 * 获取曼哈顿生成的连接点
	 */
	private void addPoint(Point point) {
		tempList.add(point);
	}

	/**
	 * 
	 * 添加结束点
	 * 
	 * 当添加第一个点
	 * 
	 * 添加结束点
	 * 
	 * @param pPoint
	 *            小线段结束点
	 */
	private void addSegmentData(
			List<JecnManhattanRouterResultData> lineSegmentDataList) {
		if (tempList == null || tempList.size() == 0) {
			return;
		}
		int tempSize = tempList.size() - 1;
		for (int i = tempSize; i >= 0; i--) {
			if (oldPoint == null) {
				oldPoint = tempList.get(i);
			} else {
				JecnManhattanRouterResultData resultData = new JecnManhattanRouterResultData();
				resultData.setStartPoint(oldPoint);
				resultData.setEndPint(tempList.get(i));
				oldPoint = tempList.get(i);
				if (i == 0) {
					// 结束线段
					resultData.setEndLine(true);
				}
				lineSegmentDataList.add(resultData);
			}
		}
	}

	/**
	 * 
	 * 添加结束点
	 * 
	 * 当添加第一个点
	 * 
	 * 添加结束点
	 * 
	 * @param pPoint
	 *            小线段结束点
	 * 
	 */
	private void addPointToJecnLineSegmentData(
			List<JecnManhattanRouterResultData> lineSegmentDataList,
			Point pPoint) {

		if (lineSegmentDataList == null || pPoint == null) {
			return;
		}
		if (lineSegmentDataList.size() == 0) {// 第一条线开始点
			JecnManhattanRouterResultData lineSegmentData = new JecnManhattanRouterResultData();
			lineSegmentData.setStartPoint(pPoint);
			lineSegmentDataList.add(lineSegmentData);
		} else if (lineSegmentDataList.size() == 1) {// 第一条线结束点
			JecnManhattanRouterResultData lineSegmentData = lineSegmentDataList
					.get(0);
			lineSegmentData.setEndPint(pPoint);
		} else {
			JecnManhattanRouterResultData lineSegmentData = new JecnManhattanRouterResultData();
			lineSegmentData.setStartPoint(lineSegmentDataList.get(
					lineSegmentDataList.size() - 1).getEndPint());
			lineSegmentData.setEndPint(pPoint);
			lineSegmentDataList.add(lineSegmentData);
		}
	}
}
