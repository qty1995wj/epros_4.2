package com.jecn.epros.server.bean.download;

import java.io.Serializable;

public class ActiveItSystem implements Serializable {
	private Long activeId;
	private String activeName;
	private String activeNum;
	private String systemName;

	public Long getActiveId() {
		return activeId;
	}

	public void setActiveId(Long activeId) {
		this.activeId = activeId;
	}

	public String getActiveName() {
		return activeName;
	}

	public void setActiveName(String activeName) {
		this.activeName = activeName;
	}

	public String getActiveNum() {
		return activeNum;
	}

	public void setActiveNum(String activeNum) {
		this.activeNum = activeNum;
	}

	public String getSystemName() {
		return systemName;
	}

	public void setSystemName(String systemName) {
		this.systemName = systemName;
	}

}
