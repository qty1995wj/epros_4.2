package epros.designer.gui.task.jComponent;

import com.jecn.epros.server.action.designer.process.IFlowStructureAcion;
import com.jecn.epros.server.action.designer.task.IJecnTaskRecordAction;
import com.jecn.epros.server.action.designer.task.approve.IJecnCreateTaskAction;
import com.jecn.epros.server.bean.process.JecnFlowBasicInfoT;
import com.jecn.epros.server.bean.process.JecnFlowStructureT;
import com.jecn.epros.server.bean.process.JecnMainFlowT;
import com.jecn.epros.server.bean.system.ProceeRuleTypeBean;
import com.jecn.epros.server.bean.task.JecnTempCreateTaskBean;

import epros.designer.gui.task.buss.TaskApproveConstant;
import epros.designer.tree.JecnTree;
import epros.designer.tree.JecnTreeNode;
import epros.designer.util.ConnectionPool;
import epros.draw.gui.top.optionpane.JecnOptionPane;

/**
 * 流程任务窗体
 * 
 * @author ZHANGXIAOHU
 * @date： 日期：Nov 14, 2012 时间：7:07:02 PM
 */
public class JecnFlowTaskJDialog extends JecnTaskJDialog {

	private Long flowId = null;
	/** 流程主表信息 */
	private JecnFlowStructureT jecnFlowStructureT = null;
	/** 流程基本信息 */
	private JecnFlowBasicInfoT jecnFlowBasicInfoT = null;
	/** 流程地图信息 */
	private JecnMainFlowT mainFlowT = null;


	public JecnFlowTaskJDialog(JecnTree jTree, JecnTreeNode treeNode, int approveType) {
		super(jTree, treeNode, approveType);
		if (treeNode == null) {
			return;
		}
		this.flowId = treeNode.getJecnTreeBean().getId();
		init();
	}


	/**
	 * 初始化其他显示
	 */
	@Override
	public void initElseShow() {
		taskName = treeNode.getJecnTreeBean().getName();
		jTextFieldTaskName.setText(treeNode.getJecnTreeBean().getName());
		version = this.jTextFieldVersion.getText();
		changeDescription = this.jTextAreaChangeDescription.getText();

		IFlowStructureAcion flowStructureAcion = ConnectionPool.getProcessAction();

		try {
			jecnFlowStructureT = flowStructureAcion.getJecnFlowStructureT(flowId);
			if (taskType == 4) {// 流程地图
				mainFlowT = flowStructureAcion.getFlowMapInfo(flowId);
			} else {// 流程图
				// 获得流程基本信息临时表
				jecnFlowBasicInfoT = flowStructureAcion.getJecnFlowBasicInfoT(flowId);
			}
			if (jecnTaskApplication.getFileType() != 0) {
				if (jecnFlowBasicInfoT.getTypeId() != null) {
					typeIds = jecnFlowBasicInfoT.getTypeId().toString();
				}
				// 流程类别 jComboBoxClassify
				addComboxItem();
			}
			if (this.jecnTaskApplication.getIsDefinition() != 0 && approveType!=2) {
				if (jecnFlowBasicInfoT != null) {
					if (jecnFlowBasicInfoT.getNoutGlossary() != null) {
						definition = jecnFlowBasicInfoT.getNoutGlossary();
						this.jTextAreaDefinition.setText(definition);
					}
				} else if (mainFlowT != null) {
					// 地图属于定于
					definition = mainFlowT.getFlowNounDefine();
					this.jTextAreaDefinition.setText(definition);
				}
			}
		} catch (Exception ex) {
			JecnOptionPane.showMessageDialog(null, TaskApproveConstant.access_server_error);
			log.error("JecnFlowTaskJDialog initElseShow is error", ex);
		}
		if (jecnFlowStructureT != null) {
			if (this.jecnTaskApplication.getIsPublic() != 0) {
				if (jecnFlowStructureT.getIsPublic() != null && jecnFlowStructureT.getIsPublic() == 0) {// 0是秘密，1是公开
					this.jComboBoxSecurityClassification.setSelectedIndex(0);
				} else if (jecnFlowStructureT.getIsPublic() != null && jecnFlowStructureT.getIsPublic() != 0) {
					jComboBoxSecurityClassification.setSelectedIndex(1);
				}
			}
			if (this.jecnTaskApplication.getIsFlowNumber() != 0) {// 编号
				if (jecnFlowStructureT.getFlowIdInput() != null) {
					this.jTextFieldNumber.setText(jecnFlowStructureT.getFlowIdInput());
				}
			}
		}

	}

	/** 确定 */
	protected boolean submitTask() {
		if ((jecnFlowBasicInfoT == null && mainFlowT == null) || jecnFlowStructureT == null) {
			throw new NullPointerException(
					"JecnFlowTaskJDialog submitTask (jecnFlowBasicInfoT == null && mainFlowT == null) || jecnFlowStructureT == null");
		}
		// JecnTaskFlowBeanTemporary jecnTaskFlowBeanTemporary = new
		// JecnTaskFlowBeanTemporary();
		/** 类别和术语定义 */
		if ((this.jecnTaskApplication.getFileType() != 0 || this.jecnTaskApplication.getIsDefinition() != 0
				|| this.isRunVersion != 0 )&& approveType != 2) {
			if (this.jecnTaskApplication.getFileType() != 0) {// 流程类别
				String typeName = (String) jComboBoxClassify.getSelectedItem();
				for (ProceeRuleTypeBean proceeRuleTypeBean : listTypeBean) {
					if (proceeRuleTypeBean.getTypeName().equals(typeName)) {
						// jecnTaskFlowBeanTemporary.setTypeId(proceeRuleTypeBean
						// .getTypeId());
						typeIds = proceeRuleTypeBean.getTypeId().toString();
						jecnFlowBasicInfoT.setTypeId(proceeRuleTypeBean.getTypeId());
					}
				}
				if (listTypeBean.size() == 0) {
					// jecnTaskFlowBeanTemporary.setTypeId(0L);
					jecnFlowBasicInfoT.setTypeId(0L);
				}
			}
			if (this.jecnTaskApplication.getIsDefinition() != 0) {
				if (taskType == 0) {// 流程图
					jecnFlowBasicInfoT.setNoutGlossary(jTextAreaDefinition.getText());
				} else {
					mainFlowT.setFlowNounDefine(jTextAreaDefinition.getText());
				}
			}
		}

		if (this.jecnTaskApplication.getIsPublic() != 0 && approveType != 2) {
			jecnFlowStructureT.setIsPublic(getIsPublic());
		}
		if (this.jecnTaskApplication.getIsFlowNumber() != 0) {
			// jecnTaskFlowBeanTemporary.setFlowInputId(this.jTextFieldNumber
			// .getText());
			jecnFlowStructureT.setFlowIdInput(jTextFieldNumber.getText());
		}
		// 创建提交审批 提交审批 组装待处理信息对象
		JecnTempCreateTaskBean createTaskBean = new JecnTempCreateTaskBean();
		try {
			IJecnCreateTaskAction designerTaskAction = ConnectionPool.getDesignerTaskAction();

			// 组装待处理信息对象
			setJecnTempCreateTaskBean(createTaskBean);
			// 流程基本信息
			createTaskBean.setFlowStructureT(jecnFlowStructureT);
			// 设置流程图信息
			if (taskType == 0) {
				createTaskBean.setFlowBasicInfoT(jecnFlowBasicInfoT);
			} else {// 设置流程地图信息
				createTaskBean.setMainFlowT(mainFlowT);
			}
			// 创建流程、制度、文件 任务
			designerTaskAction.createPRFTask(createTaskBean);
			JecnOptionPane.showMessageDialog(this, TaskApproveConstant.create_app_sucess);
		} catch (Exception ex) {
			log.error("JecnFlowTaskJDialog submitTask is error", ex);
			JecnOptionPane.showMessageDialog(this, TaskApproveConstant.create_app_error);
			return false;
		}
		return true;
	}

	/** 检查版本号存不存在 */
	@Override
	protected boolean checkRevsion() {
		if (flowId != null) {
			IJecnTaskRecordAction taskRecordAction = ConnectionPool.getTaskRecordAction();

			int isFlow = 0;
			if (mainFlowT != null) {// 存在流程地图
				isFlow = 4;
			}
			try {
				if (taskRecordAction.isExistVersionbyFlowId(flowId, this.jTextFieldVersion.getText(), isFlow)) {
					JecnOptionPane.showMessageDialog(this, TaskApproveConstant.num_is_exit);
					return false;
				}
			} catch (Exception ex) {
				JecnOptionPane.showMessageDialog(null, TaskApproveConstant.access_server_error);
				log.error("JecnFlowTaskJDialog checkRevsion is error", ex);
				return false;
			}
		}
		return true;
	}
}
