package com.jecn.epros.domain.user;

import java.util.List;
import java.util.Map;

/**
 * Created by ZXH on 2017/3/1.
 */
public class PermissionsMenuInfo {
    /**
     * 用户权限菜单集合
     */
    private List<PermissionsMenu> sidebarMenus;
    /**
     * 用户权限菜单对应的操作menu集合
     */
    private Map<PermissionsMenuEnum, List<PermissionsMenu>> sidebarContentMenuMap;

    public List<PermissionsMenu> getSidebarMenus() {
        return sidebarMenus;
    }

    public void setSidebarMenus(List<PermissionsMenu> sidebarMenus) {
        this.sidebarMenus = sidebarMenus;
    }

    public Map<PermissionsMenuEnum, List<PermissionsMenu>> getSidebarContentMenuMap() {
        return sidebarContentMenuMap;
    }

    public void setSidebarContentMenuMap(Map<PermissionsMenuEnum, List<PermissionsMenu>> sidebarContentMenuMap) {
        this.sidebarContentMenuMap = sidebarContentMenuMap;
    }
}
