package com.jecn.epros.domain.propose;

import java.util.List;

public class ProposeTotalView {
    private int total;
    private List<ProposeTotalData> listProposeTotalData;

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public List<ProposeTotalData> getListProposeTotalData() {
        return listProposeTotalData;
    }

    public void setListProposeTotalData(List<ProposeTotalData> listProposeTotalData) {
        this.listProposeTotalData = listProposeTotalData;
    }
}
