package com.jecn.epros.server.bean.system;

import java.io.Serializable;

public class JecnMaintainNode implements Serializable {
	private String guid;
	private String mark;
	private String name;
	private String enName;
	private String defaultName;
	private String endefaultName;

	public String getGuid() {
		return guid;
	}

	public void setGuid(String guid) {
		this.guid = guid;
	}

	public String getMark() {
		return mark;
	}

	public void setMark(String mark) {
		this.mark = mark;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEnName() {
		return enName;
	}

	public void setEnName(String enName) {
		this.enName = enName;
	}

	public String getDefaultName() {
		return defaultName;
	}

	public void setDefaultName(String defaultName) {
		this.defaultName = defaultName;
	}

	public String getEndefaultName() {
		return endefaultName;
	}

	public void setEndefaultName(String endefaultName) {
		this.endefaultName = endefaultName;
	}
}
