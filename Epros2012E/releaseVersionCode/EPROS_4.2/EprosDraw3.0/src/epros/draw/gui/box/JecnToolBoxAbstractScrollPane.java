package epros.draw.gui.box;

import java.awt.FlowLayout;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.List;

import javax.swing.ButtonGroup;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;

import epros.draw.constant.JecnLogConstants;
import epros.draw.constant.JecnToolBoxConstant.MapElemType;
import epros.draw.error.ErrorInfoConstant;
import epros.draw.gui.designer.JecnDesignerProcess;
import epros.draw.util.JecnResourceUtil;
import epros.draw.util.JecnUIUtil;

/**
 * 
 * 流程地图/流程图/组织图：流程元素库面板的父类
 * 
 * @author ZHOUXY
 * 
 */
public abstract class JecnToolBoxAbstractScrollPane extends JScrollPane
		implements MouseListener {
	/** 流程元素库 */
	protected JecnToolBoxMainPanel toolBoxMainPanel = null;

	/** 存放流程元素按钮的面板 */
	protected JPanel flowPanel = null;

	/** 流程元素库(流程元素、模型)中所有图标集合 */
	protected JecnToolBoxBean toolBoxBean = null;

	JecnToolBoxAbstractScrollPane(JecnToolBoxMainPanel toolBoxMainPanel) {
		if (toolBoxMainPanel == null) {
			JecnLogConstants.LOG_JecnToolBoxAbstractScrollPane
					.error("JecnToolBoxAbstractScrollPane类构造函数：参数为null或“”.toolBoxMainPanel="
							+ toolBoxMainPanel);
			throw new IllegalArgumentException(ErrorInfoConstant.PARA_OBJ_NULL);
		}
		this.toolBoxMainPanel = toolBoxMainPanel;

		// 初始化组件
		initComponent();
		initShowData();
		// 添加图标按钮到按钮区域中
		initAllFlowElemToPanel();
	}
	
	private void initShowData() {
		if (JecnDesignerProcess.getDesignerProcess().isDraw()) {
			// 初始化常用流程元素数据
			initToolBoxfavrData();
			// 初始化不常用流程元素数据
			initToolBoxGenData();
		} else {
			initShowToolBoxData();
		}
	}

	private void initComponent() {
		// 流程元素库(流程元素、模型)中所有图标集合
		toolBoxBean = new JecnToolBoxBean();
		// 存放流程元素按钮的面板
		flowPanel = new JPanel();

		this.setViewportView(flowPanel);
		this.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		this
				.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);

		flowPanel.setLayout(new FlowLayout(FlowLayout.LEFT));
		flowPanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		// 边框
		flowPanel.setBorder(null);
		this.setBorder(null);

		// 事件：和内容保持一致
		this.addComponentListener(new JecnScrollPaneComponentAdapter());
		// 主要去掉选中状态
		flowPanel.addMouseListener(this);

		//鼠标滚动大小
		this.getHorizontalScrollBar().setUnitIncrement(30);
		this.getVerticalScrollBar().setUnitIncrement(30);
	}

	/**
	 * 
	 * 设置按钮显示样式: 1、图片、内容左右布局 2、图片、内容上下布局 3、图片、详细内容布局
	 * 
	 */
	protected void setButtonShowType() {
		toolBoxBean.setButtonShowType();
	}

	/**
	 * 
	 * 添加图标按钮到按钮区域中
	 * 
	 */
	protected void initAllFlowElemToPanel() {
		// 常用线
		for (JecnToolBoxButton btn : toolBoxBean.getAllShowList()) {
			addNewGenBtnToBtsPanel(btn);
		}
		// 常用图形
//		for (JecnToolBoxButton btn : toolBoxBean.getFavrFigureList()) {
//			addNewGenBtnToBtsPanel(btn);
//		}
//		// 不常用流程元素
//		for (JecnToolBoxButton btn : toolBoxBean.getGenflowElemList()) {
//			addNewGenBtnToBtsPanel(btn);
//		}
	}

	/**
	 * 
	 * 添加常用流程元素到toolboxBean的常用集合中
	 * 
	 * @param mapElemTyperray
	 *            MapElemType[] 流程元素图标标识
	 */
	protected void addOftenBtnToLineList(MapElemType[] mapElemTyperray) {
		if (mapElemTyperray == null) {
			return;
		}
		for (MapElemType mapElemType : mapElemTyperray) {
			// 创建对象
			JecnToolBoxButton btn = new JecnToolBoxButton(toolBoxMainPanel,
					mapElemType);
			toolBoxBean.addBtnToShowList(btn);
		}
	}
	
	/**
	 * 
	 * 添加常用流程元素到toolboxBean的常用集合中
	 * 
	 * @param mapElemTyperray
	 *            MapElemType[] 流程元素图标标识
	 */
	protected void addAllBtnToShowList(List<MapElemType> mapElemTyperray) {
		if (mapElemTyperray == null) {
			return;
		}
		for (MapElemType mapElemType : mapElemTyperray) {
			// 创建对象
			JecnToolBoxButton btn = new JecnToolBoxButton(toolBoxMainPanel,
					mapElemType);
			toolBoxBean.addBtnToShowList(btn);
		}
	}

	/**
	 * 
	 * 添加常用流程元素到toolboxBean的常用集合中
	 * 
	 * @param mapElemTyperray
	 *            MapElemType[] 流程元素图标标识
	 * 
	 */
	protected void addOftenBtnToFigureList(MapElemType[] mapElemTyperray) {
		if (mapElemTyperray == null) {
			return;
		}
		for (MapElemType mapElemType : mapElemTyperray) {
			// 创建对象
			JecnToolBoxButton btn = new JecnToolBoxButton(toolBoxMainPanel,
					mapElemType);
			toolBoxBean.addBtnToShowList(btn);
		}
	}

	/**
	 * 
	 * 从properties文件中读取数据，需要显示的生成按钮图标存放在JecnToolBoxBean数据对象中
	 * 
	 * @param toolBoxDataList
	 *            List<MapElemType> properties文件中读取数据
	 */
	protected void addPropertiesGenBtnToList(List<MapElemType> toolBoxDataList) {
		for (MapElemType mapElemType : toolBoxDataList) {
			addNewGenBtnToBean(mapElemType);
		}
	}

	/**
	 * 
	 * 创建按钮图标且添加图标：面板、互斥组以及数据集合
	 * 
	 * @param genBtn
	 *            JecnToolBoxButton
	 * @return JecnToolBoxButton 添加成功返回JecnToolBoxButton对象，添加失败返回NULL
	 */
	protected JecnToolBoxButton addGenBtnToAll(MapElemType mapElemType) {
		if (mapElemType == null) {
			return null;
		}
		// 添加图标到数据层
		JecnToolBoxButton toolBoxButton = addNewGenBtnToBean(mapElemType);
		if (toolBoxButton == null) {// 没有添加成功
			return toolBoxButton;
		}
		// 添加到按钮面板和互斥组
		addNewGenBtnToBtsPanel(toolBoxButton);

		return toolBoxButton;
	}

	/**
	 * 
	 * 移除不常用图标所在面板、集合、互斥组
	 * 
	 * @param mapElemType
	 *            MapElemType 流程元素类型
	 */
	protected void removeGenBtnToBtsPanel(MapElemType mapElemType) {
		if (mapElemType == null) {
			return;
		}
		JecnToolBoxButton toolBoxButton = this.toolBoxBean
				.isExistGenflowElem(mapElemType);
		if (toolBoxButton != null) {
			removeGenBtnToBtsPanel(toolBoxButton);
		}

	}

	/**
	 * 
	 * 根据给定的流程元素，生成JecnToolBoxButton添加到toolBoxBean的不常用流程元素集合中
	 * 
	 * 当添加的流程元素已经存在则不作添加
	 * 
	 * @param mapElemType
	 *            MapElemType 待添加的流程元素类型
	 * @return JecnToolBoxButton 添加成功返回JecnToolBoxButton对象，添加失败或者待添加已经存在返回NULL
	 */
	protected JecnToolBoxButton addNewGenBtnToBean(MapElemType mapElemType) {
		if (mapElemType == null) {
			return null;
		}

		// 创建对象
		JecnToolBoxButton genBtn = new JecnToolBoxButton(toolBoxMainPanel,
				mapElemType);
		boolean addFlag = toolBoxBean.addBtnToGenflowElemList(genBtn);
		if (!addFlag) {// 没有添加成功
			return null;
		}
		return genBtn;
	}

	/**
	 * 
	 * 添加图标:只添加到面板和互斥组
	 * 
	 * @param genBtn
	 *            JecnToolBoxButton
	 */
	protected void addNewGenBtnToBtsPanel(JecnToolBoxButton genBtn) {
		if (genBtn == null) {
			return;
		}
		// 添加到按钮面板
		this.flowPanel.add(genBtn.getJToolBar());
		// 互斥组
		getButtonGroup().add(genBtn);
	}

	/**
	 * 
	 * 移除不常用图标所在面板、集合、互斥组
	 * 
	 * @param genBtn
	 *            JecnToolBoxButton 按钮图标
	 */
	protected void removeGenBtnToBtsPanel(JecnToolBoxButton genBtn) {
		if (genBtn == null) {
			return;
		}
		// 集合中
		this.toolBoxBean.getAllShowList().remove(genBtn);
		// 添加到按钮面板
		this.flowPanel.remove(genBtn.getJToolBar());
		// 互斥组
		getButtonGroup().remove(genBtn);
	}

	protected ButtonGroup getButtonGroup() {
		return this.toolBoxMainPanel.getBtnGroup();
	}

	protected JecnResourceUtil getResourceManager() {
		return this.toolBoxMainPanel.getResourceManager();
	}

	JecnToolBoxMainPanel getToolBoxMainPanel() {
		return toolBoxMainPanel;
	}

	JecnToolBoxBean getToolBoxBean() {
		return toolBoxBean;
	}

	public JPanel getFlowPanel() {
		return flowPanel;
	}

	public void mouseClicked(MouseEvent e) {
	}

	public void mousePressed(MouseEvent e) {
	}

	public void mouseReleased(MouseEvent e) {
		if (e.getSource() == flowPanel) {
			this.toolBoxMainPanel.initGuide();
		}
	}

	public void mouseEntered(MouseEvent e) {
	}

	public void mouseExited(MouseEvent e) {
	}

	/** 初始化常用流程元素数据 */
	protected abstract void initToolBoxfavrData();

	/** 初始化不常用流程元素图标数据 */
	protected abstract void initToolBoxGenData();

	/** 不常用线数组 */
	abstract MapElemType[] getGenLineTypeArray();

	/** 不常用图形数组 */
	abstract MapElemType[] getGenFigureTypeArray();
	
	/**
	 * 获取数据库中显示的元素集合
	 */
	abstract void initShowToolBoxData();

}
