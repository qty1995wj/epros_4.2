package epros.designer.gui.popedom.organization;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.JTree;

import org.apache.log4j.Logger;

import com.jecn.epros.server.bean.popedom.JecnFlowOrg;
import com.jecn.epros.server.bean.tree.JecnTreeBean;
import com.jecn.epros.server.util.JecnTreeEnumConstant.TreeNodeType;

import epros.designer.gui.common.JecnConstants;
import epros.designer.tree.JecnTreeCommon;
import epros.designer.tree.JecnTreeNode;
import epros.designer.util.ConnectionPool;
import epros.designer.util.JecnProperties;
import epros.draw.gui.top.dialog.JecnDialog;
import epros.draw.gui.top.optionpane.JecnOptionPane;
import epros.draw.system.data.JecnUserCheckInfoData;
import epros.draw.util.DrawCommon;
import epros.draw.util.JecnUIUtil;
import epros.draw.util.JecnUserCheckUtil;

/***
 * 新建组织
 * @author 2012-06-07
 *
 */
public class AddOrgDialog extends JecnDialog {

	private static Logger log = Logger.getLogger(AddOrgDialog.class);
	/** 主面板 */
	private JPanel mainPanel = null;

	/** 控件显示面板 */
	private JPanel topPanel = null;

	/** 按钮面板 */
	private JPanel buttonPanel = null;

	/** 名称Lab */
	private JLabel nameLab = null;

	/** 名称填写框 */
	private JTextField nameTextField = null;
	
	/**编号Lab*/
	private JLabel numLab = null;
	
	/***编号填写框*/
	private JTextField numTextField = null;

	/** 确定按钮 */
	private JButton okBut = null;

	/** 取消按钮 */
	private JButton cancelBut = null;

	/** 名称验证提示 */
	protected JLabel promptLab = null;
	
	/**判断是否点击的确定按钮*/
	private boolean isOk=false;

	/** 设置面板控件大小 */
	Dimension dimension = null;
	
	/**名称必填项 *号提示*/
	private JLabel requiredMarkLab = new JLabel("*");
	/**岗位编号必填项 *号提示*/
	private JLabel requiredNumLab = new JLabel("*");
	private JecnTreeNode pNode = null;
	private JTree jTree = null;
	public AddOrgDialog(JecnTreeNode pNode,JTree jTree){
		this.pNode = pNode;
		this.jTree = jTree;
		this.setLocationRelativeTo(null);
		initCompotents();
		initLayout();
		this.setModal(true);
	}
	public String getDialogTitle() {
		return JecnProperties.getValue("newOrg");
	}

	public String getNameLab() {
		return JecnProperties.getValue("nameC");
	}

	
	/***
	 * 初始化组件
	 */
	private void initCompotents() {
		// 主面板
		mainPanel = new JPanel();

		// 控件显示面板
		topPanel = new JPanel();

		// 按钮面板
		buttonPanel = new JPanel();

		// 名称Lab
		nameLab = new JLabel();

		// 名称填写框
		nameTextField = new JTextField();
		
		// 编号Lab
		numLab = new JLabel();

		// 编号填写框
		numTextField = new JTextField();

		// 名称验证提示
		promptLab = new JLabel();

		// 确定按钮
		okBut = new JButton(JecnProperties.getValue("okBtn"));

		// 取消按钮
		cancelBut = new JButton(JecnProperties.getValue("cancelBtn"));

		// 设置Dialog大小
		this.setSize(330, 150);

		// 设置主面板的默认背景色
		mainPanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());

		// 设置控件显示面板的默认背景色
		topPanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());

		// 设置按钮面板的默认背景色
		buttonPanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());

		dimension = new Dimension(330, 90);
		mainPanel.setPreferredSize(dimension);
		mainPanel.setMaximumSize(dimension);
		mainPanel.setMinimumSize(dimension);
		
		// 设置控件显示面板的大小
		dimension = new Dimension(330, 80);
		topPanel.setPreferredSize(dimension);
		topPanel.setMaximumSize(dimension);
		topPanel.setMinimumSize(dimension);

		// 设置按钮面板的大小
		dimension = new Dimension(323, 30);
		buttonPanel.setPreferredSize(dimension);
		buttonPanel.setMaximumSize(dimension);
		buttonPanel.setMinimumSize(dimension);

		// 设置验证提示文字颜色
		promptLab.setForeground(Color.red);
		
		// 为确认按钮增加回车事件
		this.getRootPane().setDefaultButton(okBut);
		
		requiredMarkLab.setForeground(Color.red);
		requiredNumLab.setForeground(Color.red);
		//岗位编号添加默认值
		this.numTextField.setText(DrawCommon.getUUID());
	}

	/**
	 * 布局
	 */
	protected void initLayout() {
		// 主面板布局
		mainPanel.setLayout(new GridBagLayout());
		GridBagConstraints c = null;

		Insets insets = new Insets(1, 5, 1, 5);
		Insets insetb =	new Insets(1, 5, 5, 5);
		Insets insett =	new Insets(5, 5, 1, 5);
		c = new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
				GridBagConstraints.SOUTHWEST, GridBagConstraints.NONE, insets,
				0, 0);
		mainPanel.add(topPanel, c);
		// 控件显示面板 布局
		topPanel.setLayout(new GridBagLayout());
//		topPanel.setBorder(BorderFactory.createTitledBorder(""));
		// 名称Lab
		c = new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.NONE, insetb, 0, 0);
		topPanel.add(nameLab, c);
		// 名称填写框
		c = new GridBagConstraints(1, 0, 1, 1, 1.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, insetb, 0, 0);
		topPanel.add(nameTextField, c);
		// *号提示
		c = new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 0, 0, 1), 0, 0);
		topPanel.add(requiredMarkLab, c);
		// 编号Lab
		c = new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.NONE, insett, 0, 0);
		topPanel.add(numLab, c);
		// 编号填写框
		c = new GridBagConstraints(1, 1, 1, 1, 1.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, insett, 0, 0);
		topPanel.add(numTextField, c);
		
		// *号提示
		c = new GridBagConstraints(2, 1, 1, 1, 0.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 0, 0, 1), 0, 0);
		topPanel.add(requiredNumLab, c);
		// 按钮面板 布局
		c = new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0,
				GridBagConstraints.SOUTHWEST, GridBagConstraints.NONE, insets,
				0, 0);
		mainPanel.add(buttonPanel, c);

		buttonPanel.setLayout(new FlowLayout(FlowLayout.RIGHT));
//		buttonPanel.setBorder(BorderFactory.createTitledBorder(""));
		buttonPanel.add(promptLab);
		// 确定
		buttonPanel.add(okBut);
		// 取消
		buttonPanel.add(cancelBut);

		this.getContentPane().add(mainPanel);

		// 确定按钮事件监听
		okBut.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				okButtonAction();
			}
		});
		// 取消按钮事件监听
		cancelBut.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				cancelButtonAction();
			}
		});
		// Dialog标题
		this.setTitle(getDialogTitle());
		
		// 设置名称Lab的值
		this.nameLab.setText(getNameLab());
		//设置编号Lab的值
		this.numLab.setText(JecnProperties.getValue("actBaseNumC"));

		// 设置Dialog的大小不被改变
		this.setResizable(false);
	}

	// 确定事件
	public void okButtonAction() {
		String name = nameTextField.getText();
//		nameTextField.setText(name);
		String num = numTextField.getText();
		
		
		// 验证名称是否正确
		if (!validateName(JecnProperties.getValue("name"),name, promptLab)) {
			return;
		}
		// 验证编号是否正确
		if (!validateName(JecnProperties.getValue("actBaseNum"),num, promptLab)) {
			return;
		}
		if(num!=null&&"0".equals(num.trim())){
       		promptLab.setText(JecnProperties.getValue("orgNumber"));
       		return;
		}
		
		try {
			if (validateNodeRepeat(name)) {
				promptLab.setText(JecnProperties.getValue("nameHaved"));
				return;
			} else {
				promptLab.setText("");
			}
			//验证编号全表唯一
			if(ConnectionPool.getOrganizationAction().validateAddUpdateOrgNum(num.trim(), null)){
				promptLab.setText(JecnProperties.getValue("posNumHaved"));
				return;
			}else{
				promptLab.setText("");
			}
		} catch (Exception e) {
			JecnOptionPane.showMessageDialog(this, JecnProperties.getValue("serverConnException"));
			log.error("AddOrgDialog okButtonAction is error！",e);
			return;
			
		}
		saveData();
		isOk=true;
	}
	public boolean validateName(String nameLab,String name, JLabel jLable){
//		return JecnValidateCommon.validateNameNoRestrict(name, promptLab);
		String s="";
		if (DrawCommon.isNullOrEmtryTrim(name)) {
			// 名称不能为空
			s= JecnProperties.getValue("chinaColon") + JecnProperties.getValue("isNotEmpty");
		} else if (JecnUserCheckUtil.getTextLength(name) > 122) {
			// 名称不能超过122个字符或61个汉字
			s= JecnUserCheckInfoData.getNameLengthInfo();
		}
		if (!DrawCommon.isNullOrEmtryTrim(s)) {
			jLable.setText(nameLab+s);
			return false;
		}
		jLable.setText("");
		return true;
	}
	public void saveData() {  
		try {
			//判断相同父节点下的组织名名称是否重复   getOrgInfo
			List<JecnFlowOrg> listOrg = ConnectionPool.getOrganizationAction().getOrgInfos(pNode.getJecnTreeBean().getId());
			for(JecnFlowOrg flowOrg : listOrg){
				if(flowOrg.getOrgName().equals(this.nameTextField.getText().trim())){
					promptLab.setText(JecnProperties.getValue("nameHaved"));
					return;
				}
			}
			//数据库执行添加
			Long id = ConnectionPool.getOrganizationAction().addOrg(
					this.nameTextField.getText().trim(),
					pNode.getJecnTreeBean().getId(),pNode.getJecnTreeBean().getNumberId(), JecnConstants.projectId,
					JecnTreeCommon.getMaxSort(pNode),
					this.numTextField.getText().trim());
			//向树节点添加新建的组织节点
			JecnTreeBean jecnTreeBean = new JecnTreeBean();
			jecnTreeBean.setId(id);
			jecnTreeBean.setName(this.nameTextField.getText().trim());
			jecnTreeBean.setPid(pNode.getJecnTreeBean().getId());
			jecnTreeBean.setPname(pNode.getJecnTreeBean().getName());
			//组织编号
			jecnTreeBean.setNumberId(this.numTextField.getText().trim());
			jecnTreeBean.setTreeNodeType(TreeNodeType.organization);
			JecnTreeCommon.addNodeAndSelectNode(jTree, jecnTreeBean, pNode);
			//关闭窗体
			this.dispose();
		} catch (Exception e) {
			log.error("AddOrgDialog saveData is error",e);
		}
		
	}

	public boolean validateNodeRepeat(String name) {
		return JecnTreeCommon.validateRepeatNameAdd(pNode, name,
				TreeNodeType.organization);
	}
	// 取消事件
	public void cancelButtonAction() {
		isOk=false;
		this.dispose();
	}
	public JTextField getNameTextField() {
		return nameTextField;
	}
	public void setNameTextField(JTextField nameTextField) {
		this.nameTextField = nameTextField;
	}
	public boolean isOk() {
		return isOk;
	}
	public void setOk(boolean isOk) {
		this.isOk = isOk;
	}
	
}
