package epros.designer.gui.common;

import java.awt.Dimension;

import javax.swing.JTextField;

/**
 * @author yxw 2012-6-6
 * @description：TextFild 公用类 
 */
public class JecnTextField extends JTextField {
	
	
	public JecnTextField(){
		// 项的内容大小
		Dimension dimension = new Dimension(450, 20);
		this.setPreferredSize(dimension);
		this.setMinimumSize(dimension);
		this.setMaximumSize(dimension);
	}
	/**
	 * @param widthInt
	 */
	public JecnTextField(int widthInt){
		// 项的内容大小
		Dimension dimension = new Dimension(widthInt, 20);
		this.setPreferredSize(dimension);
		this.setMinimumSize(dimension);
		this.setMaximumSize(dimension);
	}
	/**
	 * @param widthInt
	 * @param heigthInt
	 */
	public JecnTextField(int widthInt,int heigthInt){
		// 项的内容大小
		Dimension dimension = new Dimension(widthInt, heigthInt);
		this.setPreferredSize(dimension);
		this.setMinimumSize(dimension);
		this.setMaximumSize(dimension);
	}
	
}
