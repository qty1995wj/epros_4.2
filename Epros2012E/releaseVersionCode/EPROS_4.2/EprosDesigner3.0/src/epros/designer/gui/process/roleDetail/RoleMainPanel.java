package epros.designer.gui.process.roleDetail;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.event.PopupMenuEvent;
import javax.swing.event.PopupMenuListener;
import javax.swing.table.DefaultTableModel;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import com.jecn.epros.server.bean.tree.JecnTreeBean;
import com.jecn.epros.server.common.JecnConfigContents.ConfigItemPartMapMark;

import epros.designer.gui.common.JecnConstants;
import epros.designer.gui.common.JecnDesignerCommon;
import epros.designer.gui.common.JecnSearchPopup;
import epros.designer.gui.common.JecnTextArea;
import epros.designer.gui.common.JecnTextField;
import epros.designer.gui.system.CommonJlabelMustWrite;
import epros.designer.service.system.JecnElementAttributePanel;
import epros.designer.util.ConnectionPool;
import epros.designer.util.JecnConfigTool;
import epros.designer.util.JecnProperties;
import epros.designer.util.JecnValidateCommon;
import epros.draw.constant.JecnToolBoxConstant.MapElemType;
import epros.draw.constant.JecnToolBoxConstant.MapType;
import epros.draw.constant.JecnToolBoxConstant.ModeType;
import epros.draw.data.figure.JecnRoleData;
import epros.draw.designer.JecnDesignerData;
import epros.draw.designer.JecnFlowStationT;
import epros.draw.gui.figure.JecnBaseFigurePanel;
import epros.draw.gui.figure.JecnBaseRoleFigure;
import epros.draw.gui.swing.JecnPanel;
import epros.draw.gui.swing.textfield.search.JSearchTextField;
import epros.draw.gui.swing.textfield.search.JecnSearchDocumentListener;
import epros.draw.gui.top.optionpane.JecnOptionPane;
import epros.draw.gui.workflow.JecnDrawMainPanel;
import epros.draw.util.DrawCommon;
import epros.draw.util.JecnUIUtil;

public class RoleMainPanel extends JecnElementAttributePanel {
	private static Logger log = Logger.getLogger(RoleDetailDialog.class);
	/** * 信息面板 */
	private JecnPanel infoPanel = new JecnPanel();

	/** * 按钮面板 */
	private JecnPanel butPanel = new JecnPanel();

	/** * 角色名称Lab */
	private JLabel roleNameLab = new JLabel(JecnProperties.getValue("roleNameC"));

	/** 角色名称Field */
	private JecnTextField roleNameField = new JecnTextField();

	/** * 主责岗位Lab */
	private JLabel mainPosLab = new JLabel(JecnProperties.getValue("mainPosC"));
	/** 主责岗位是否的容器 */
	private JPanel mainPosPanel = new JPanel();
	/** * 主责岗位:是 */
	private JRadioButton mainPosYesRadioBtn = new JRadioButton(JecnProperties.getValue("isLab"));
	/** * 主责岗位:否 */
	private JRadioButton mainPosNoRadioBtn = new JRadioButton(JecnProperties.getValue("notLab"));

	/** * 岗位名称Lab */
	private JLabel posNameLab = new JLabel(JecnProperties.getValue("positionNameC"));

	/** * 岗位名称Area */
	private JSearchTextField posNameField = new JSearchTextField();
	private String posIds = null;

	/** * 岗位组Lab */
	private JLabel posGropLab = new JLabel(JecnProperties.getValue("positionGroupC"));

	/** * 岗位组Area */
	private JSearchTextField posGropField = new JSearchTextField();
	private String posGropIds = null;

	/** * 角色职责Lab */
	private JLabel roleResponseLab = new JLabel(JecnProperties.getValue("roleResponse")); // roleResponse

	/** * 角色职责Area */
	private JecnTextArea roleResponseArea = new JecnTextArea();

	/** 角色职责Panel */
	private JScrollPane roleResScrollPane = new JScrollPane();

	/** * 岗位选择But */
	private JButton posBut = new JButton(JecnProperties.getValue("selectBtn")); //

	/** * 岗位组选择But */
	private JButton posGropBut = new JButton(JecnProperties.getValue("selectBtn"));

	private List<JecnTreeBean> listPos = new ArrayList<JecnTreeBean>();
	private List<JecnTreeBean> listPosGrops = new ArrayList<JecnTreeBean>();
	/** 角色元素数据层 */
	private JecnBaseRoleFigure baseRoleFigure;

	/** 岗位搜索弹出popup */
	private JecnSearchPopup positionSearchPopup = null;
	/** 岗位组搜索弹出popup */
	private JecnSearchPopup posGroupSearchPopup = null;
	/** tableMode */
	/** 搜索出的数据集合 */
	private List<JecnTreeBean> tableList = null;

	protected JLabel errorLab = new JLabel();

	protected JLabel promptLab = new JLabel();

	private JecnDesignerData jecnDesignerData = JecnDrawMainPanel.getMainPanel().getWorkflow().getFlowMapData()
			.getDesignerData();

	public RoleMainPanel(JecnBaseRoleFigure baseRoleFigure) {
		super(baseRoleFigure.getFlowElementData(), baseRoleFigure);
		this.baseRoleFigure = baseRoleFigure;
		initComponents();
		initLayout();
		initData();
		initEvent();
	}

	private void initData() {
		initRoleDataToJecnTreeBean();
		// 岗位名称
		roleNameField.setText(baseRoleFigure.getFlowElementData().getFigureText());

		// 主责岗位
		if ("4".equals(getRoleData().getActivityId())) {
			mainPosYesRadioBtn.setSelected(true);
			mainPosNoRadioBtn.setSelected(false);
		} else {
			mainPosNoRadioBtn.setSelected(true);
			mainPosYesRadioBtn.setSelected(false);
		}

		// 角色职责
		roleResponseArea.setText(getRoleData().getRoleRes());
		// posNameField 赋值已选择的岗位
		String posName = "";
		for (JecnTreeBean treeBean : listPos) {
			posName += treeBean.getName() + "/";
		}
		if (!"".equals(posName)) {
			posName = posName.substring(0, posName.length() - 1);
		}
		posNameField.setText(posName);
		// JecnCommon.getFieldLook(listPos, posNameField);
		// posNameField 赋值已选择的岗位组
		String posGropName = "";
		for (JecnTreeBean treeBean : listPosGrops) {
			posGropName += treeBean.getName() + "/";
		}
		if (!"".equals(posGropName)) {
			posGropName = posGropName.substring(0, posGropName.length() - 1);
		}
		posGropField.setText(posGropName);
	}

	public void initComponents() {
		// 角色职责带滚动条
		roleResScrollPane.setBorder(null);
		roleResScrollPane.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		roleResScrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);

		roleNameField.setEditable(false);
		roleNameField.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		posNameField.setBorder(BorderFactory.createLineBorder(Color.gray));
		posGropField.setBorder(BorderFactory.createLineBorder(Color.gray));
		roleResponseArea.setBorder(BorderFactory.createLineBorder(Color.gray));

		// 项的内容大小
		Dimension dimension = new Dimension(20, 20);
		posNameField.setPreferredSize(dimension);
		posNameField.setMinimumSize(dimension);
		posGropField.setPreferredSize(dimension);
		posGropField.setMinimumSize(dimension);

		// 主责岗位
		mainPosLab.setOpaque(false);
		mainPosPanel.setOpaque(false);
		mainPosYesRadioBtn.setOpaque(false);
		mainPosNoRadioBtn.setOpaque(false);

		ButtonGroup gBtn = new ButtonGroup();
		gBtn.add(mainPosYesRadioBtn);
		gBtn.add(mainPosNoRadioBtn);
		// 客户，单选按钮不可编辑
		hiddenRadio();

		mainPosNoRadioBtn.setSelected(true);
		mainPosPanel.setLayout(new FlowLayout(FlowLayout.LEFT, 5, 0));
		mainPosPanel.add(mainPosYesRadioBtn);
		mainPosPanel.add(mainPosNoRadioBtn);

		this.setLayout(new GridBagLayout());
		infoPanel.setLayout(new GridBagLayout());
		infoPanel.setBorder(BorderFactory.createTitledBorder(""));
		butPanel.setLayout(new FlowLayout(FlowLayout.RIGHT));

		positionSearchPopup = new JecnSearchPopup(posNameField);
		// 隐藏第1列和第3列
		positionSearchPopup.hiddenColumn(0, 2);
		posGroupSearchPopup = new JecnSearchPopup(posGropField);
		// 隐藏第1列和第3列
		posGroupSearchPopup.hiddenColumn(0, 2);
		errorLab.setForeground(Color.red);
	}

	/**
	 * 
	 * 判断是否有更新，有返回true，没有返回false
	 * 
	 * @return boolean 有返回true，没有返回false
	 */
	public boolean isUpdate() {
		// 角色职责
		String roleResponse = roleResponseArea.getText();

		JecnRoleData jecnRoleData = baseRoleFigure.getFlowElementData();
		if (mainPosYesRadioBtn.isSelected()// 选中主责岗位
				&& !"4".equals(jecnRoleData.getActivityId())) {// 主责岗位是否发生变化
			return true;
		}
		if (!mainPosYesRadioBtn.isSelected()// 选中主责岗位
				&& "4".equals(jecnRoleData.getActivityId())) {// 主责岗位是否发生变化
			return true;
		}

		if (jecnRoleData == null) {
			throw new NullPointerException("RoleDetailDialog isUpdate is error！");
		}

		if (posOrGroupPosUpdate(jecnRoleData.getFlowStationList())) {// 验证添加角色岗位或岗位组是否发生变化
			return true;
		}
		if (!DrawCommon.checkStringSame(roleResponse, jecnRoleData.getRoleRes())) {// 角色职责是否为发生变化
			return true;
		}
		return false;
	}

	/**
	 * 验证添加角色岗位或岗位组是否发生编号
	 * 
	 * @param flowStationList
	 *            记录角色对应岗位或岗位组集合
	 * @return true 角色对应的岗位或岗位组发生编号
	 */
	protected boolean posOrGroupPosUpdate(List<JecnFlowStationT> flowStationList) {
		if (flowStationList.size() != (listPos.size() + listPosGrops.size())) {
			return true;
		}
		int count = 0;
		for (JecnFlowStationT flowStationT : flowStationList) {
			for (JecnTreeBean posTreeBean : listPos) {// 岗位
				if ("0".equals(flowStationT.getType())
						&& flowStationT.getFigurePositionId().equals(posTreeBean.getId())) {
					count++;
				}
			}
			for (JecnTreeBean posTreeBean : listPosGrops) {// 岗位组
				if ("1".equals(flowStationT.getType())
						&& flowStationT.getFigurePositionId().equals(posTreeBean.getId())) {
					count++;
				}
			}
		}
		if (count != flowStationList.size()) {
			return true;
		}
		return false;
	}

	/**
	 * 客户，单选按钮不可编辑
	 * 
	 */
	private void hiddenRadio() {
		if (baseRoleFigure.getFlowElementData().getMapElemType() == MapElemType.CustomFigure) {// 客户
			mainPosYesRadioBtn.setEnabled(false);
			mainPosNoRadioBtn.setEnabled(false);
		}
	}

	/***************************************************************************
	 * 布局
	 */
	private void initLayout() {
		GridBagConstraints c = null;
		Insets insets = new Insets(5, 5, 5, 5);
		c = new GridBagConstraints(0, 0, 1, 1, 1.0, 1.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, insets, 0,
				0);
		this.add(infoPanel, c);
		// 角色名称
		c = new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.NONE, insets,
				0, 0);
		infoPanel.add(roleNameLab, c);
		c = new GridBagConstraints(1, 0, 2, 1, 1.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.HORIZONTAL,
				insets, 0, 0);
		infoPanel.add(roleNameField, c);
		if (jecnDesignerData != null && jecnDesignerData.getModeType() == ModeType.none) { // 流程

			if (baseRoleFigure.getFlowElementData().getMapElemType() != MapElemType.VirtualRoleFigure) {
				if (!JecnConfigTool.isIflytekLogin()) {
					// 主责岗位Lab
					c = new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.NORTHWEST,
							GridBagConstraints.NONE, insets, 0, 0);
					infoPanel.add(mainPosLab, c);
					// 主责岗位panel
					c = new GridBagConstraints(1, 1, 2, 1, 1.0, 0.0, GridBagConstraints.NORTHWEST,
							GridBagConstraints.HORIZONTAL, insets, 0, 0);
					infoPanel.add(mainPosPanel, c);
				}
				// 岗位名
				c = new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.NONE,
						insets, 0, 0);
				infoPanel.add(posNameLab, c);
				c = new GridBagConstraints(1, 2, 1, 1, 1.0, 0.0, GridBagConstraints.NORTHWEST,
						GridBagConstraints.HORIZONTAL, insets, 0, 0);
				infoPanel.add(posNameField, c);
				c = new GridBagConstraints(2, 2, 1, 1, 0.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.NONE,
						insets, 0, 0);
				infoPanel.add(posBut, c);
				// 岗位组
				c = new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.NONE,
						insets, 0, 0);
				infoPanel.add(posGropLab, c);
				c = new GridBagConstraints(1, 3, 1, 1, 1.0, 0.0, GridBagConstraints.NORTHWEST,
						GridBagConstraints.HORIZONTAL, insets, 0, 0);
				infoPanel.add(posGropField, c);
				c = new GridBagConstraints(2, 3, 1, 1, 0.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.NONE,
						insets, 0, 0);
				infoPanel.add(posGropBut, c);
			}

		}
		// 角色职责
		c = new GridBagConstraints(0, 4, 1, 1, 0.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.NONE, insets,
				0, 0);
		infoPanel.add(roleResponseLab, c);
		c = new GridBagConstraints(1, 4, 2, 1, 1.0, 1.0, GridBagConstraints.NORTHWEST, GridBagConstraints.BOTH, insets,
				0, 0);
		roleResScrollPane.setViewportView(roleResponseArea);
		infoPanel.add(roleResScrollPane, c);
		if (JecnConfigTool.isShowItem(ConfigItemPartMapMark.standRoleInfoNotEmpty)) {// 建设规范中
			// 加入角色职责
			// 必填
			c = new GridBagConstraints(3, 4, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE,
					new Insets(3, 1, 3, 1), 0, 0);
			infoPanel.add(new CommonJlabelMustWrite(), c);
		}

		if (JecnConfigTool.isShowItem(ConfigItemPartMapMark.standRoleExistPosition)
				&& baseRoleFigure.getFlowElementData().getMapElemType() != MapElemType.VirtualRoleFigure) {
			promptLab.setText(JecnProperties.getValue("noteRolePostValidate"));
			promptLab.setForeground(Color.red);
			c = new GridBagConstraints(0, 1, 1, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, insets,
					0, 0);
			this.add(promptLab, c);
		}

		// 按钮面板
		c = new GridBagConstraints(0, 2, 1, 1, 1.0, 0.0, GridBagConstraints.EAST, GridBagConstraints.NONE, insets, 0, 0);
		this.add(butPanel, c);

		butPanel.add(errorLab);
	}

	private void initEvent() {
		roleResponseArea.getDocument().addDocumentListener(new DocumentListener() {
			public void removeUpdate(DocumentEvent e) {
				JecnValidateCommon.validateContent(roleResponseArea.getText(), errorLab);
			}

			public void insertUpdate(DocumentEvent e) {
				JecnValidateCommon.validateContent(roleResponseArea.getText(), errorLab);
			}

			public void changedUpdate(DocumentEvent e) {
			}
		});
		// 岗位选择
		posBut.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				posIds = JecnDesignerCommon.setPositionLook(listPos, posNameField, null);
			}
		});
		// 岗位组选择
		posGropBut.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				posGropIds = JecnDesignerCommon.setPositionGroupLook(listPosGrops, posGropField, null);
			}
		});

		// 岗位名称Area
		posNameField.addDocumentListener(new JecnSearchDocumentListener(posNameField) {
			@Override
			public void removeUpdateJecn(DocumentEvent e) {
				positionSearch();
			}

			@Override
			public void insertUpdateJecn(DocumentEvent e) {
				positionSearch();
			}

			@Override
			public void keyEnterReleased(KeyEvent e) {
				positionSearchPopup.searchTableMousePressed();
			}
		});
		posGropField.addDocumentListener(new JecnSearchDocumentListener(posGropField) {
			@Override
			public void removeUpdateJecn(DocumentEvent e) {
				posGroupSearch();
			}

			@Override
			public void insertUpdateJecn(DocumentEvent e) {
				posGroupSearch();
			}

			@Override
			public void keyEnterReleased(KeyEvent e) {
				posGroupSearchPopup.searchTableMousePressed();
			}
		});

		positionSearchPopup.addPopupMenuListener(new PopupMenuListener() {

			@Override
			public void popupMenuWillBecomeVisible(PopupMenuEvent e) {
				// TODO Auto-generated method stub

			}

			@Override
			public void popupMenuWillBecomeInvisible(PopupMenuEvent e) {

				if (positionSearchPopup.getSelectTreeBean() != null) {
					posIds = positionSearchPopup.getSelectTreeBean().getId().toString();
					// 双击选择岗位后，把listPos清空，加入刚选择的数据
					listPos.clear();
					listPos.add(positionSearchPopup.getSelectTreeBean());
					positionSearchPopup.setSelectTreeBean(null);
				}

			}

			@Override
			public void popupMenuCanceled(PopupMenuEvent e) {
				// TODO Auto-generated method stub

			}
		});
		posGroupSearchPopup.addPopupMenuListener(new PopupMenuListener() {

			@Override
			public void popupMenuWillBecomeVisible(PopupMenuEvent e) {

			}

			@Override
			public void popupMenuWillBecomeInvisible(PopupMenuEvent e) {
				if (posGroupSearchPopup.getSelectTreeBean() != null) {
					posGropIds = posGroupSearchPopup.getSelectTreeBean().getId().toString();
					// 双击选择岗位组后，把listPos清空，加入刚选择的数据
					listPosGrops.clear();
					listPosGrops.add(posGroupSearchPopup.getSelectTreeBean());
					posGroupSearchPopup.setSelectTreeBean(null);
				}

			}

			@Override
			public void popupMenuCanceled(PopupMenuEvent e) {

			}
		});

	}

	/***************************************************************************
	 * 确定
	 */
	protected boolean saveData() {
		String roleRes = roleResponseArea.getText().trim();
		if (!JecnValidateCommon.validateContent(roleRes, errorLab)) {
			return false;
		}
		getRoleData().setRoleRes(roleRes);
		// 面板保存标识
		JecnDrawMainPanel.getMainPanel().getWorkflow().getFlowMapData().setSave(true);
		for (JecnTreeBean b : listPosGrops) {
			try {
				List<JecnTreeBean> posList = ConnectionPool.getPosGroup().getPosByPositionGroupId(b.getId());
				// 岗位组包括选择的岗位
				for (JecnTreeBean ob : posList) {
					for (JecnTreeBean ib : listPos) {
						if (ob.getId().equals(ib.getId())) {
							// 岗位组已经存在岗位!
							JecnOptionPane.showMessageDialog(this, JecnProperties.getValue("positionGroup") + "\""
									+ b.getName() + "\"" + JecnProperties.getValue("isExist") + "\"" + ob.getName()
									+ "\"!");
							return false;
						}
					}
				}
			} catch (Exception e) {
				log.error("RoleDetailDialog okButPerformed is error", e);
				return false;
			}

		}
		List<JecnFlowStationT> flowStationList = getRoleData().getFlowStationList();
		if ((flowStationList == null || flowStationList.size() == 0) && (listPos.size() > 0 || listPosGrops.size() > 0)) {
			if (flowStationList == null) {
				flowStationList = new ArrayList<JecnFlowStationT>();
				getRoleData().setFlowStationList(flowStationList);
			}

			for (JecnTreeBean b : listPos) {
				JecnFlowStationT jecnFlowStationT = new JecnFlowStationT();
				jecnFlowStationT.setFigureFlowId(getRoleData().getFlowElementId());
				jecnFlowStationT.setFigurePositionId(b.getId());
				jecnFlowStationT.setType("0");
				jecnFlowStationT.setStationName(b.getName());
				flowStationList.add(jecnFlowStationT);
			}
			for (JecnTreeBean c : listPosGrops) {
				JecnFlowStationT jecnFlowStationT = new JecnFlowStationT();
				jecnFlowStationT.setFigureFlowId(getRoleData().getFlowElementId());
				jecnFlowStationT.setFigurePositionId(c.getId());
				jecnFlowStationT.setType("1");
				jecnFlowStationT.setStationName(c.getName());
				flowStationList.add(jecnFlowStationT);
			}
			// 面板保存
			JecnDrawMainPanel.getMainPanel().getWorkflow().getFlowMapData().setSave(true);
		} else {
			List<JecnFlowStationT> addPos = new ArrayList<JecnFlowStationT>();
			List<JecnFlowStationT> removePos = new ArrayList<JecnFlowStationT>();
			List<JecnFlowStationT> addPosGrops = new ArrayList<JecnFlowStationT>();
			List<JecnFlowStationT> removeGrops = new ArrayList<JecnFlowStationT>();
			// 循环出要删除的岗位
			out: for (JecnFlowStationT s : flowStationList) {
				// 岗位
				if (s.getType().equals("0")) {
					boolean f = true;
					for (JecnTreeBean b : listPos) {
						if (b.getId().longValue() == s.getFigurePositionId().longValue()) {
							f = false;
							continue out;
						}

					}
					if (f) {

						removePos.add(s);
						// 面板保存
						JecnDrawMainPanel.getMainPanel().getWorkflow().getFlowMapData().setSave(true);
					}
				}

			}
			// 循环出要增加的岗位
			out: for (JecnTreeBean b : listPos) {
				boolean f = true;
				for (JecnFlowStationT s : flowStationList) {
					// 岗位
					if (s.getType().equals("0")) {
						if (b.getId().longValue() == s.getFigurePositionId().longValue()) {
							f = false;
							continue out;
						}
					}
				}
				if (f) {
					JecnFlowStationT jecnFlowStationT = new JecnFlowStationT();
					jecnFlowStationT.setFigureFlowId(getRoleData().getFlowElementId());
					jecnFlowStationT.setFigurePositionId(b.getId());
					jecnFlowStationT.setType("0");
					jecnFlowStationT.setStationName(b.getName());
					addPos.add(jecnFlowStationT);
					// 面板保存
					JecnDrawMainPanel.getMainPanel().getWorkflow().getFlowMapData().setSave(true);
				}
			}

			// 循环出要删除的 岗位组
			out: for (JecnFlowStationT s : flowStationList) {
				// 岗位组
				if (s.getType().equals("1")) {
					boolean f = true;
					for (JecnTreeBean b : listPosGrops) {
						if (b.getId().longValue() == s.getFigurePositionId().longValue()) {
							f = false;
							continue out;
						}
					}
					if (f) {
						removeGrops.add(s);
						// 面板保存
						JecnDrawMainPanel.getMainPanel().getWorkflow().getFlowMapData().setSave(true);
					}
				}

			}
			// 循环出要增加的 岗位组
			out: for (JecnTreeBean b : listPosGrops) {
				boolean f = true;
				for (JecnFlowStationT s : flowStationList) {
					// 岗位组
					if (s.getType().equals("1")) {
						if (b.getId().longValue() == s.getFigurePositionId().longValue()) {
							f = false;
							continue out;
						}
					}

				}
				if (f) {
					JecnFlowStationT jecnFlowStationT = new JecnFlowStationT();
					jecnFlowStationT.setFigureFlowId(getRoleData().getFlowElementId());
					jecnFlowStationT.setFigurePositionId(b.getId());
					jecnFlowStationT.setType("1");
					jecnFlowStationT.setStationName(b.getName());
					addPosGrops.add(jecnFlowStationT);
					// 面板保存标识
					JecnDrawMainPanel.getMainPanel().getWorkflow().getFlowMapData().setSave(true);
				}
			}

			flowStationList.removeAll(removePos);
			flowStationList.addAll(addPos);
			flowStationList.removeAll(removeGrops);
			flowStationList.addAll(addPosGrops);
		}

		// 是否为主责岗位
		boolean b = mainPosYesRadioBtn.isSelected();
		if (b) {
			getRoleData().setActivityId("4");
		} else {
			getRoleData().setActivityId("5");
		}
		// 默认别名 角色
		String roleText = JecnProperties.getValue("role");
		String customerText = JecnProperties.getValue("customerC");
		if (!DrawCommon.isNullOrEmtryTrim(posGropField.getText()) && listPosGrops.size() > 0) {
			if (!JecnConfigTool.isFirstUpdateRoleAlias()) {
				if ((!JecnConfigTool.isUpdateRoleAlias()) && StringUtils.isNotBlank(posGropField.getText())) {
					getRoleData().setFigureText(posGropField.getText());
				}
			} else {
				if ((roleText.equals(getRoleData().getFigureText().trim()) || customerText.equals(getRoleData()
						.getFigureText().trim()))
						&& StringUtils.isNotBlank(posGropField.getText())) {
					getRoleData().setFigureText(posGropField.getText());
				}
			}
			baseRoleFigure.updateUI();
		} else if (!DrawCommon.isNullOrEmtryTrim(posNameField.getText()) && listPos.size() > 0) {
			// v3.06 角色关联一个岗位时添加部门名称至岗位名称前
			if (needConcatOrgName(listPos.size())) {
				getRoleData().setFigureText(listPos.get(0).getPname() + listPos.get(0).getName());
			} else {
				if (!JecnConfigTool.isFirstUpdateRoleAlias()) {
					if ((!JecnConfigTool.isUpdateRoleAlias()) && StringUtils.isNotBlank(posNameField.getText())) {
						getRoleData().setFigureText(posNameField.getText());
					}
				} else {
					if ((roleText.equals(getRoleData().getFigureText().trim()) || customerText.equals(getRoleData()
							.getFigureText().trim()))
							&& StringUtils.isNotBlank(posNameField.getText())) {
						getRoleData().setFigureText(posNameField.getText());
					}
				}
			}
			baseRoleFigure.updateUI();
		}

		if (baseRoleFigure != null) {
			JecnDesignerCommon.addElemLink(baseRoleFigure, MapType.partMap);
		}
		getRoleData().setPosName(posNameField.getText());
		getRoleData().setPosGroupName(posGropField.getText());

		return true;
	}

	/**
	 * 是否需要追加部门名称于岗位名称前
	 * 
	 * @author weidp
	 * @date 2014-10-29 下午04:04:46
	 * @param size
	 * @return
	 */
	private boolean needConcatOrgName(int size) {
		// 关联多个岗位时返回false
		if (size == 1) {
			// 关联一个岗位且开启了此配置
			int configValue = Integer.valueOf(ConnectionPool.getConfigAciton().selectValue(1,
					ConfigItemPartMapMark.shouOrgPosName.toString()));
			return configValue == 1 ? true : false;
		}
		return false;
	}

	protected JecnRoleData getRoleData() {
		return this.baseRoleFigure.getFlowElementData();
	}

	private void positionSearch() {
		try {
			String posName = posNameField.getText().trim();
			if (!"".equals(posName)) {
				if (positionSearchPopup.isCanSearch()) {
					tableList = ConnectionPool.getOrganizationAction().searchPositionByName(posName,
							JecnConstants.projectId);
					positionSearchPopup.setTableData(tableList);
				}
				positionSearchPopup.setCanSearch(true);
			}

		} catch (Exception e) {
			JecnOptionPane.showMessageDialog(this, JecnProperties.getValue("serverConnException"));
			log.error("RoleDetailDialog positionSearch is error", e);
		}
	}

	private void posGroupSearch() {
		try {
			String posGroupName = posGropField.getText().trim();
			if (!"".equals(posGroupName)) {
				if (posGroupSearchPopup.isCanSearch()) {
					tableList = ConnectionPool.getPosGroup().searchPositionGroupsByName(JecnConstants.projectId,
							posGroupName, null);
					posGroupSearchPopup.setTableData(tableList);
				}
				posGroupSearchPopup.setCanSearch(true);
			}

		} catch (Exception e) {
			JecnOptionPane.showMessageDialog(this, JecnProperties.getValue("serverConnException"));
			log.error("RoleDetailDialog posGroupSearch is error", e);
		}
	}

	private void initRoleDataToJecnTreeBean() {
		List<JecnFlowStationT> flowStationList = getRoleData().getFlowStationList();

		if (flowStationList != null) {
			StringBuffer ids = new StringBuffer();
			for (JecnFlowStationT s : flowStationList) {

				JecnTreeBean b = new JecnTreeBean();
				b.setId(s.getFigurePositionId());
				b.setName(s.getStationName());
				// 岗位
				if (("0").equals(s.getType())) {
					ids.append(s.getFigurePositionId() + ",");
					// listPos.add(b);
				} else {// 岗位组
					listPosGrops.add(b);
				}
			}
			if (ids.length() > 0) {
				try {
					listPos = ConnectionPool.getOrganizationAction().getPositionsByIds(
							ids.toString().substring(0, ids.length() - 1));
				} catch (Exception e) {
					log.error("RoleDetailDialog:828L", e);
				}
			}

		}
	}

	private Vector<Vector<Object>> toTableData(List<JecnTreeBean> list) {
		Vector<Vector<Object>> data = new Vector<Vector<Object>>();
		if (list != null) {

			for (JecnTreeBean b : list) {
				Vector<Object> v = new Vector<Object>();
				v.add(b.getId());
				v.add(b.getName());
				data.add(v);
			}
		}
		return data;
	}

	class SearchTableMode extends DefaultTableModel {
		public SearchTableMode(Vector<String> title) {
			super(title, 0);
		}

		@Override
		public Class getColumnClass(int columnIndex) {

			return super.getColumnClass(columnIndex);

		}

		public boolean isCellEditable(int rowindex, int colindex) {
			return false;
		}

	}

	public boolean saveButAction() {
		// 是否更新
		boolean isUpdate = isUpdate();

		boolean saveResult = saveData();
		if (!saveResult) {
			return false;
		}
		// 面板保存
		if (isUpdate) {
			JecnDrawMainPanel.getMainPanel().getWorkflow().getFlowMapData().setSave(true);
			JecnDrawMainPanel.getMainPanel().getWorkflow().repaint();
		}
		return true;
	}

	public void initElementPanelData(JecnBaseFigurePanel figurePanel) {
		this.baseRoleFigure = (JecnBaseRoleFigure) figurePanel;
		listPos.clear();
		listPosGrops.clear();
		posGroupSearchPopup.setCanSearch(false);
		positionSearchPopup.setCanSearch(false);
		initData();
	}

	public JecnPanel getButPanel() {
		return butPanel;
	}

	public JecnDesignerData getJecnDesignerData() {
		return jecnDesignerData;
	}

	protected JComponent getElementAttrubutePanel() {
		return this;
	}
}
