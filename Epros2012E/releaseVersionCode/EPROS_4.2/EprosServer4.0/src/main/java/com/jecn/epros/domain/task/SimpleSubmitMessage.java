package com.jecn.epros.domain.task;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiParam;

import java.util.Date;
import java.util.List;

/**
 * 任务提交信息
 */
public class SimpleSubmitMessage {
    @ApiParam(value = "提交审核信息也就是审批意见", required = true)
    private String opinion;
    @ApiParam(value = "任务主键ID", required = true)
    private String taskId;
    @ApiParam(value = "当前需要处理的阶段", required = true)
    private String taskState;
    @ApiParam(value = "任务审批人的操作状态 0：拟稿人提交审批 1：通过 2：交办 3：转批 4：打回 5：提交意见（提交审批意见(评审-------->拟稿人) 6：二次评审 拟稿人------>评审 7：拟稿人重新提交审批 8：完成 9:打回整理意见(批准------>拟稿人)）10：交办人提交", required = true)
    private String taskOperation;
    @ApiParam(value = "操作人PeopleId，不需要页面传递")
    private String curPeopleId;
    @ApiParam(value = "交办和转批人ID，在交办和转批用到")
    private String userAssignedId;
    @ApiParam(value = "二次评审人IDS，在二次评审用到")
    private String newViewerIds;
    @ApiParam(value = "任务变更说明，在重新提交或者文控审核人主导审批时用到")
    private String taskDesc;
    @ApiParam(value = "密级 0秘密，1 公开，在重新提交或者文控审核人主导审批时用到")
    private String strPublic;
    @ApiParam(value = "查阅权限组织ID集合 ','分隔，在重新提交或者文控审核人主导审批时用到")
    private String orgIds;
    @ApiParam(value = "查阅权限岗位ID集合 ','分隔，在重新提交或者文控审核人主导审批时用到")
    private String posIds;
    @ApiParam(value = "查阅岗位组ID集合 ','分隔，在重新提交或者文控审核人主导审批时用到")
    private String groupIds;
    @ApiParam(value = "页面提交新增审批人,拟稿人重新提交，或者文控审核人主导审批时或者管理员编辑时用到")
    private List<TempAuditPeopleBean> listAuditPeople;
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+08")
    @ApiParam(value = "预估结束时间,在重新提交或者文控审核人主导审批时用到")
    private Date endTime;
    @ApiParam(value = "流程类别id，拟稿人主导审批，在拟稿人提交时使用")
    private String type;
    @ApiParam(value = "流程类别名称，拟稿人主导审批，在拟稿人提交时使用")
    private String typeName;
    @ApiParam(value = " 查阅权限相关部门名称‘/’分离，在重新提交或者文控审核人主导审批时用到")
    private String orgNames;
    @ApiParam(value = "查阅权限相关岗位名称‘/’分离，在重新提交或者文控审核人主导审批时用到")
    private String posNames;
    @ApiParam(value = "查阅岗位组名称集合 ','分隔，在重新提交或者文控审核人主导审批时用到")
    private String groupNames;

    public String getTypeName() {
        return typeName;
    }

    public void setTypeName(String typeName) {
        this.typeName = typeName;
    }

    public String getOpinion() {
        return opinion;
    }

    public void setOpinion(String opinion) {
        this.opinion = opinion;
    }

    public String getTaskId() {
        return taskId;
    }

    public void setTaskId(String taskId) {
        this.taskId = taskId;
    }

    public String getTaskState() {
        return taskState;
    }

    public void setTaskState(String taskState) {
        this.taskState = taskState;
    }

    public String getTaskOperation() {
        return taskOperation;
    }

    public void setTaskOperation(String taskOperation) {
        this.taskOperation = taskOperation;
    }

    public String getCurPeopleId() {
        return curPeopleId;
    }

    public void setCurPeopleId(String curPeopleId) {
        this.curPeopleId = curPeopleId;
    }

    public String getUserAssignedId() {
        return userAssignedId;
    }

    public void setUserAssignedId(String userAssignedId) {
        this.userAssignedId = userAssignedId;
    }

    public String getNewViewerIds() {
        return newViewerIds;
    }

    public void setNewViewerIds(String newViewerIds) {
        this.newViewerIds = newViewerIds;
    }

    public String getTaskDesc() {
        return taskDesc;
    }

    public void setTaskDesc(String taskDesc) {
        this.taskDesc = taskDesc;
    }

    public String getStrPublic() {
        return strPublic;
    }

    public void setStrPublic(String strPublic) {
        this.strPublic = strPublic;
    }

    public String getOrgIds() {
        return orgIds;
    }

    public void setOrgIds(String orgIds) {
        this.orgIds = orgIds;
    }

    public String getPosIds() {
        return posIds;
    }

    public void setPosIds(String posIds) {
        this.posIds = posIds;
    }

    public String getGroupIds() {
        return groupIds;
    }

    public void setGroupIds(String groupIds) {
        this.groupIds = groupIds;
    }

    public List<TempAuditPeopleBean> getListAuditPeople() {
        return listAuditPeople;
    }

    public void setListAuditPeople(List<TempAuditPeopleBean> listAuditPeople) {
        this.listAuditPeople = listAuditPeople;
    }

    public Date getEndTime() {
        return endTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getOrgNames() {
        return orgNames;
    }

    public void setOrgNames(String orgNames) {
        this.orgNames = orgNames;
    }

    public String getPosNames() {
        return posNames;
    }

    public void setPosNames(String posNames) {
        this.posNames = posNames;
    }

    public String getGroupNames() {
        return groupNames;
    }

    public void setGroupNames(String groupNames) {
        this.groupNames = groupNames;
    }


}
