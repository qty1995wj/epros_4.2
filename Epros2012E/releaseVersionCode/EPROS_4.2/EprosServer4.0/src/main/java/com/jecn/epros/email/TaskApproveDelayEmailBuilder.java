package com.jecn.epros.email;

import com.jecn.epros.domain.JecnUser;
import com.jecn.epros.domain.email.EmailBasicInfo;

public class TaskApproveDelayEmailBuilder extends DynamicContentEmailBuilder {

    @Override
    protected EmailBasicInfo getEmailBasicInfo(JecnUser user) {

        Object[] data = getData();
        Long taskId = Long.valueOf(data[0].toString());

        return createEmail(user, taskId);
    }

    private EmailBasicInfo createEmail(JecnUser user, Long taskId) {

        String httpUrl = ToolEmail.getTaskHttpUrl(taskId, user.getId());
        String subject = "任务审批超时提醒";
        String content = "您有任务未审批，现已超时！请及时审批。";

        EmailContentBuilder builder = new EmailContentBuilder(getTip());
        builder.setContent(content);
        builder.setResourceUrl(httpUrl);

        EmailBasicInfo basicInfo = new EmailBasicInfo();
        basicInfo.setContent(builder.buildContent());
        basicInfo.setSubject(subject);
        basicInfo.addRecipients(user);
        return basicInfo;
    }


}
