package com.jecn.epros.domain.journal;

import java.util.List;

public class LogUserOperatesView {
    private int total;
    private List<UserOperateResult> data;

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public List<UserOperateResult> getData() {
        return data;
    }

    public void setData(List<UserOperateResult> data) {
        this.data = data;
    }
}
