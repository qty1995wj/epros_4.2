package com.jecn.epros.server.dao.system.impl;

import com.jecn.epros.server.bean.system.JecnFixedEmail;
import com.jecn.epros.server.common.AbsBaseDao;
import com.jecn.epros.server.dao.system.IJecnFixedEmailDao;
/***
 * 发布 固定人员处理
 * 2014-04-09
 *
 */
public class JecnFixedEmailDaoImpl extends AbsBaseDao<JecnFixedEmail, String>
		implements IJecnFixedEmailDao {

}
