package com.jecn.epros.domain;

import com.jecn.epros.domain.process.Inventory;
import com.jecn.epros.util.Utils;
import com.jecn.epros.util.excel.InventoryStyleManager;

import java.util.*;

/**
 * 清单辅助类
 *
 * @author hyl
 */
public class EasyCell {
    private final List<InventoryCell> cells = new ArrayList<>();
    private final List<InventoryCellStyle> titleStyles = new ArrayList<>();
    private final List<InventoryCellStyle> contentStyles = new ArrayList<>();

    public static EasyCell newInstance() {
        return new EasyCell();
    }

    public InventoryCell create(String text) {
        InventoryCell cell = new InventoryCell(text);
        cells.add(cell);
        return cell;
    }

    public InventoryCell create(InventoryCellStyle style, String text) {
        InventoryCell cell = create(text);
        cell.setStyle(style);
        return cell;
    }

    public EasyCell add(String text) {
        create(text);
        return this;
    }

    public EasyCell add(String... texts) {
        for (String text : texts) {
            add(text);
        }
        return this;
    }

    public EasyCell add(InventoryCellStyle style, String... texts) {
        for (String text : texts) {
            add(style, text);
        }
        return this;
    }

    public EasyCell add(Resource t) {
        String text = "";
        if (t != null) {
            text = t.getName();
        }
        create(text);
        return this;
    }

    public EasyCell add(InventoryCellStyle style, Resource t) {
        String text = "";
        if (t != null) {
            text = t.getName();
        }
        create(style, text);
        return this;
    }

    public EasyCell add(Number t) {
        String text = "";
        if (t != null) {
            text = t.toString();
        }
        create(text);
        return this;
    }

    public EasyCell add(InventoryCellStyle style, Number t) {
        String text = "";
        if (t != null) {
            text = t.toString();
        }
        create(style, text);
        return this;
    }

    public EasyCell add(Date date) {
        if (date != null) {
            create(Utils.dateFormat(date, "yyyy-MM-dd"));
        } else {
            create("");
        }
        return this;
    }

    public EasyCell add(InventoryCellStyle style, Date date) {
        if (date != null) {
            create(style, Utils.dateFormat(date, "yyyy-MM-dd"));
        } else {
            create(style, "");
        }
        return this;
    }

    public EasyCell add(Date date, String pattern) {
        if (date != null) {
            create(Utils.dateFormat(date, pattern));
        } else {
            create("");
        }
        return this;
    }

    public EasyCell add(InventoryCellStyle style, Date date, String pattern) {
        if (date != null) {
            create(style, Utils.dateFormat(date, pattern));
        } else {
            create(style, "");
        }
        return this;
    }

    public EasyCell add(InventoryCell cell) {
        cells.add(cell);
        return this;
    }

    public EasyCell add(InventoryCellStyle style, InventoryCell cell) {
        cells.add(cell);
        cell.setStyle(style);
        return this;
    }

    public EasyCell add(boolean add, String text) {
        if (add) {
            add(text);
        }
        return this;
    }

    public EasyCell add(boolean add, String... texts) {
        if (add) {
            add(texts);
        }
        return this;
    }

    public EasyCell add(boolean add, Resource t) {
        if (add) {
            add(t);
        }
        return this;
    }

    public EasyCell add(boolean add, Number t) {
        if (add) {
            add(t);
        }
        return this;
    }

    public EasyCell add(boolean add, Date date) {
        if (add) {
            add(date);
        }
        return this;
    }

    public EasyCell add(boolean add, Date date, String pattern) {
        if (add) {
            add(date, pattern);
        }
        return this;
    }

    public EasyCell add(boolean add, InventoryCell cell) {
        if (add) {
            add(cell);
        }
        return this;
    }

    public EasyCell add(InventoryCellStyle style, String text) {
        create(style, text);
        return this;
    }

    public EasyCell add(InventoryCellStyle style, boolean add, String text) {
        if (add) {
            add(style, text);
        }
        return this;
    }

    public EasyCell add(InventoryCellStyle style, boolean add, String... texts) {
        if (add) {
            add(style, texts);
        }
        return this;
    }

    public EasyCell add(InventoryCellStyle style, boolean add, Resource t) {
        if (add) {
            add(style, t);
        }
        return this;
    }

    public EasyCell add(InventoryCellStyle style, boolean add, Number t) {
        if (add) {
            add(style, t);
        }
        return this;
    }

    public EasyCell add(InventoryCellStyle style, boolean add, Date date) {
        if (add) {
            add(style, date);
        }
        return this;
    }

    public EasyCell add(InventoryCellStyle style, boolean add, Date date, String pattern) {
        if (add) {
            add(style, date, pattern);
        }
        return this;
    }

    public EasyCell add(InventoryCellStyle style, boolean add, InventoryCell cell) {
        if (add) {
            add(style, cell);
        }
        return this;
    }

    public List<InventoryCell> build() {// 生成数据
        return cells;
    }

    public List<InventoryCell> buildWithTitle() {// 生成数据，样式
        cells.stream().forEach(c -> {
            InventoryCellStyle titleStyle = c.getStyle();
            if (titleStyle != null) {
                titleStyles.add(c.getStyle());
                InventoryCellStyle content = new InventoryCellStyle();
                contentStyles.add(content);
                if (titleStyle.getType() == 2 || titleStyle.getType() == 4) {
                    content.setAlign(InventoryTable.Align.left);
                }
            }
        });
        return cells;
    }

    public Map<String, List<InventoryCellStyle>> getStyleMap() {
        if (titleStyles == null || titleStyles.size() == 0) {
            throw new IllegalArgumentException("titleStyles 大小为空");
        }
        Map<String, List<InventoryCellStyle>> m = new HashMap<>();
        m.put(InventoryStyleManager.TITLE, titleStyles);
        m.put(InventoryStyleManager.CONTENT, contentStyles);
        return m;
    }

    public List<InventoryCellStyle> getTitleStyles() {
        return titleStyles;
    }

    public List<InventoryCellStyle> getContentStyles() {
        return contentStyles;
    }
}
