package com.jecn.epros.server.service.dataImport.match.constant;

public class TmpMatchTreePostBean {
	/** 节点id */
	private String id;
	/** 节点名称 */
	private String name;
	/** 部门名称 */
	private String pName;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPName() {
		return pName;
	}

	public void setPName(String name) {
		pName = name;
	}
}
