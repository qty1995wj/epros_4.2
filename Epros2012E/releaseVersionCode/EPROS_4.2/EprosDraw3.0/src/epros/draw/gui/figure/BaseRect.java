/*
 * Rect.java
 *方形
 * Created on 2008年11月3日, 上午11:26
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */
package epros.draw.gui.figure;

import java.awt.Graphics;
import java.awt.Graphics2D;

import epros.draw.data.figure.JecnFigureData;

/**
 * 
 * 矩形，所有方框图形都继承此类 默认为组织框
 * 
 * @date： 日期：Mar 21, 2012 时间：10:43:10 AM
 * @author Administrator
 */
public class BaseRect extends JecnBaseFigurePanel {
	protected int x = 0;
	protected int y = 0;

	protected int x1;
	protected int x2;
	protected int x3;
	protected int x4;
	protected int y1;
	protected int y2;
	protected int y3;
	protected int y4;
	protected int x5;
	protected int y5;
	protected int x6;
	protected int y6;

	public BaseRect(JecnFigureData flowElementData) {
		super(flowElementData);
	}

	@Override
	public void paintComponent(Graphics g) {
		userWidth = this.getWidth() - 1;
		userHeight = this.getHeight() - 1;
		// 画图
		paintFigure(g);
	}

	/**
	 * 无填充
	 * 
	 */
	protected void paintNoneFigure(Graphics2D g2d) {
		JecnPaintEnum.INTANCE.paintNoneFigure(flowElementData.getMapElemType(), flowElementData, g2d,  x1, x2,
				x3, x4, x5, x6, y1, y2, y3, y4, y5, y6, userWidth, userHeight);
	}

	/**
	 * 
	 * 阴影效果
	 */
	public void paintShadowsOnly(Graphics2D g2d) {
		// 阴影
		paintShadows(g2d);
	}

	/**
	 * 无阴影无3D 普通效果
	 * 
	 */
	public void paintGeneralFigurePart(Graphics2D g2d) {
		// 顶层 原图，榜值均为零
		paintRectTop(g2d, 0);
	}

	/**
	 * 阴影 顶层填充
	 * 
	 */
	public void paintTop(Graphics2D g2d) {
		// 顶层 原图，榜值均为零
		paintRectTop(g2d, shadowCount);
	}

	/**
	 * 无阴影无3D 普通效果
	 * 
	 */
	protected void paintRectTop(Graphics2D g2d, int shadowCount) {
		JecnPaintEnum.INTANCE.paintHandTop(flowElementData.getMapElemType(), flowElementData, g2d, x1, x2, x3, x4, x5,
				x6, y1, y2, y3, y4, y5, y6, shadowCount, shadowCount, userWidth, userHeight);
	}

	/**
	 * 
	 * 填充背景区域
	 */
	public void paint3DLow(Graphics2D g2d) {
		paint3DRectLow(g2d, 0);
	}

	/**
	 * 3D和阴影效果底层填充
	 * 
	 * @param g2d
	 * @param indent3Dvalue
	 */
	public void paint3DAndShadowsLow(Graphics2D g2d) {
		paint3DRectLow(g2d, linevalue);
	}

	private void paint3DRectLow(Graphics2D g2d, int indent3Dvalue) {
		JecnPaintEnum.INTANCE.paint3DHandLow(flowElementData.getMapElemType(), flowElementData, g2d, x1, x2, x3, x4,
				x5, x6, y1, y2, y3, y4, y5, y6, indent3Dvalue, indent3Dvalue, userWidth, userHeight);
	}

	/**
	 * 3D效果填充线条
	 */
	public void paint3DPartLine(Graphics2D g2d) {
		paint3DRectLine(g2d, 0);
	}

	private void paint3DRectLine(Graphics2D g2d, int indent3Dvalue) {
		JecnPaintEnum.INTANCE.paint3DInputHandLine(flowElementData.getMapElemType(), flowElementData, g2d, x1, x2, x3,
				x4, x5, x6, y1, y2, y3, y4, y5, y6, indent3Dvalue, indent3Dvalue, userWidth, userHeight);
	}

	/**
	 * 3D和阴影效果填充线条
	 */
	public void paint3DAndShadowsPartLine(Graphics2D g2d) {
		paint3DRectLine(g2d, indent3Dvalue);
	}

	/**
	 * 顶层 颜色填充,渐变色区域
	 */
	protected void paint3DPartTop(Graphics2D g2d) {
		JecnPaintEnum.INTANCE.paint3DPartTop(flowElementData.getMapElemType(), flowElementData, g2d, x1, x2, x3, x4,
				x5, x6, y1, y2, y3, y4, y5, y6, indent3Dvalue, userWidth, userHeight);
	}

	/**
	 * 既有3D又有阴影填充顶层
	 * 
	 * @param g2d
	 */
	protected void paint3DAndShadowsPartTop(Graphics2D g2d) {
		JecnPaintEnum.INTANCE.paint3DAndShadowsPartTop(flowElementData.getMapElemType(), flowElementData, g2d, x1, x2,
				x3, x4, x5, x6, y1, y2, y3, y4, y5, y6, indent3Dvalue, userWidth, userHeight);
	}

	/**
	 * 阴影效果
	 * 
	 * @param g2d
	 * @param x
	 * @param y
	 * @param localCount
	 *            有阴影时原图像缩进榜值
	 * @param content
	 *            有阴影时顶层图像缩进榜值
	 */
	private void paintShadows(Graphics2D g2d) {
		// g2d.setPaint(flowElementData.getShadowColor());
		// g2d.fillRect(x + shadowCount, y + shadowCount, userWidth -
		// shadowCount, userHeight);
	}
}
