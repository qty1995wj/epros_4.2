package com.jecn.search.bean;

import java.util.List;

/**
 * 
 * 关键字搜索返回结果集
 * 
 * @author ZXH
 * 
 */
public class JecnSearchResultBean {
	private int flowMapCount;
	private int flowCount;
	private int fileCount;
	private int ruleCount;
	private int standardCount;
	private int riskCount;
	private int resultNum;
	private int totalPage;

	private List<JecnKeyWordSearchBean> searchResultLists;

	public int getFlowMapCount() {
		return flowMapCount;
	}

	public void setFlowMapCount(int flowMapCount) {
		this.flowMapCount = flowMapCount;
	}

	public int getFlowCount() {
		return flowCount;
	}

	public void setFlowCount(int flowCount) {
		this.flowCount = flowCount;
	}

	public int getFileCount() {
		return fileCount;
	}

	public void setFileCount(int fileCount) {
		this.fileCount = fileCount;
	}

	public int getRuleCount() {
		return ruleCount;
	}

	public void setRuleCount(int ruleCount) {
		this.ruleCount = ruleCount;
	}

	public int getStandardCount() {
		return standardCount;
	}

	public void setStandardCount(int standardCount) {
		this.standardCount = standardCount;
	}

	public int getRiskCount() {
		return riskCount;
	}

	public void setRiskCount(int riskCount) {
		this.riskCount = riskCount;
	}

	public List<JecnKeyWordSearchBean> getSearchResultLists() {
		return searchResultLists;
	}

	public void setSearchResultLists(List<JecnKeyWordSearchBean> searchResultLists) {
		this.searchResultLists = searchResultLists;
	}

	public int getTotalPage() {
		return totalPage;
	}

	public void setTotalPage(int totalPage) {
		this.totalPage = totalPage;
	}

	public void setResultNum(int resultNum) {
		this.resultNum = resultNum;
	}

	public int getResultNum() {
		resultNum = this.flowMapCount + this.flowCount + this.fileCount + this.ruleCount + this.standardCount
				+ this.riskCount;
		return resultNum;
	}
}
