package com.jecn.epros.server.bean.popedom;

import java.util.List;

import com.jecn.epros.server.bean.tree.JecnTreeBean;
import com.jecn.epros.server.webBean.popedom.OrgBean;
import com.jecn.epros.server.webBean.popedom.PosBean;

public class UserInfo {
        
	private JecnUser jecnUser;
	/**岗位、组织信息*/
	private List<PosBean> listPosBean;
	/**组织*/
	private List<OrgBean> listOrgBean;
	/**角色信息*/
	private List<JecnRoleInfo> roleList;
	public JecnUser getJecnUser() {
		return jecnUser;
	}
	public void setJecnUser(JecnUser jecnUser) {
		this.jecnUser = jecnUser;
	}
	public List<PosBean> getListPosBean() {
		return listPosBean;
	}
	public void setListPosBean(List<PosBean> listPosBean) {
		this.listPosBean = listPosBean;
	}
	public List<OrgBean> getListOrgBean() {
		return listOrgBean;
	}
	public void setListOrgBean(List<OrgBean> listOrgBean) {
		this.listOrgBean = listOrgBean;
	}
	public List<JecnRoleInfo> getRoleList() {
		return roleList;
	}
	public void setRoleList(List<JecnRoleInfo> roleList) {
		this.roleList = roleList;
	}
	
	
	
	
}
