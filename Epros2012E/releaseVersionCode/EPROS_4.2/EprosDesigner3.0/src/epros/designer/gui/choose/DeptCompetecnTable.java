package epros.designer.gui.choose;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.List;
import java.util.Vector;

import javax.swing.DefaultCellEditor;
import javax.swing.JCheckBox;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;

import com.jecn.epros.server.bean.tree.JecnTreeBean;

import epros.designer.util.JecnProperties;
import epros.draw.gui.ui.JecnBasicTableHeaderUI;
import epros.draw.util.JecnUIUtil;

/**
 * 
 * 部门树
 * 
 * @author Administrator
 * 
 */
public class DeptCompetecnTable extends DeleteableTable {
	boolean isDowLoad = false; // 是否有设置下载权限
	private JCheckBox box = new JCheckBox();

	public DeptCompetecnTable(List<JecnTreeBean> orglist, boolean isDownLoad) {
		super(orglist);
		this.isDowLoad = isDownLoad;
		this.setModel(getTableModel());
		// 渲染器
		if (isDowLoad) {
			this.getColumnModel().getColumn(2).setCellEditor(new CheckButtonEditor(new JCheckBox()));
			this.getColumnModel().getColumn(2).setCellRenderer(new CheckBoxRenderer());
			this.getColumnModel().getColumn(2).setMaxWidth(100);
		}
		if (isSelectMutil()) {
			this.getSelectionModel().setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
		} else {
			this.getSelectionModel().setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		}
		int[] cols = gethiddenCols();
		if (cols != null && cols.length > 0) {
			TableColumnModel columnModel = this.getColumnModel();
			for (int col : cols) {
				TableColumn tableColumn = columnModel.getColumn(col);
				tableColumn.setMinWidth(0);
				tableColumn.setMaxWidth(0);
			}
		}
		// 设置默认背景色
		this.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		this.getTableHeader().setReorderingAllowed(false);
		// 自定义表头UI
		this.getTableHeader().setUI(new JecnBasicTableHeaderUI());
		// 隐藏表头
		hiddenTableHeader();
	}

	public DeptCompetecnMode getTableModel() {
		Vector<String> title = new Vector<String>();
		title.add("id");
		title.add(JecnProperties.getValue("name"));
		if (isDowLoad) {
			title.add(JecnProperties.getValue("downLoadAccess"));
		}
		return new DeptCompetecnMode(getContent(data), title);
	}

	class DeptCompetecnMode extends DefaultTableModel {
		public DeptCompetecnMode(Vector<Vector<Object>> vector, Vector<String> title) {
			super(vector, title);

		}

		public boolean isCellEditable(int rowindex, int colindex) {
			if (colindex == 2) {
				return true;
			}
			return false;
		}
	}

	public Vector<Vector<Object>> getContent(List<JecnTreeBean> list) {
		Vector<Vector<Object>> content = new Vector<Vector<Object>>();
		if (list != null) {
			for (JecnTreeBean jecnTreeBean : list) {
				Vector<Object> data = new Vector<Object>();
				data.add(0, jecnTreeBean.getId().toString());
				data.add(1, jecnTreeBean.getName());
				if (isDowLoad) {
					data.add(2, new JCheckBox(JecnProperties.getValue("downLoadAccess"), jecnTreeBean.isDownLoad()));
				}
				content.add(data);
			}
		}
		return content;
	}

	public Vector<String> getTableTitle() {
		Vector<String> title = new Vector<String>();
		title.add("id");
		title.add(JecnProperties.getValue("name"));
		return title;
	}

	public void remoeAll() {
		// 清空
		for (int index = this.getModel().getRowCount() - 1; index >= 0; index--) {
			((DefaultTableModel) this.getModel()).removeRow(index);
		}
	}

	public int[] gethiddenCols() {
		return new int[] { 0 };
	}

	public boolean isSelectMutil() {
		return true;
	}

	/** 隐藏表头 */
	public void hiddenTableHeader() {
		this.getTableHeader().setVisible(false);
		DefaultTableCellRenderer renderer = new DefaultTableCellRenderer();
		renderer.setPreferredSize(new Dimension(0, 0));
		this.getTableHeader().setDefaultRenderer(renderer);
	}

	/**
	 * 根据表格ID 获取 查阅权限的下载标识
	 * 
	 * @param id
	 *            获取的ID
	 * @return
	 */
	public boolean getIsDownLoadByTableId(Long id) {
		for (int i = 0; i < this.getRowCount(); i++) {
			Long table_id = Long.valueOf(this.getValueAt(i, 0).toString());
			if (table_id.longValue() == id.longValue()) {
				if (isDowLoad) {
					JCheckBox box = (JCheckBox) this.getValueAt(i, 2);
					return box.isSelected();
				} else {
					return true;
				}
			}
		}
		return false;
	}

	class CheckButtonEditor extends DefaultCellEditor implements ItemListener {
		private JCheckBox button;

		public CheckButtonEditor(JCheckBox checkBox) {
			super(checkBox);
		}

		public Component getTableCellEditorComponent(JTable table, Object value, boolean isSelected, int row, int column) {
			if (value == null)
				return null;
			button = (JCheckBox) value;
			button.addItemListener(this);
			Long table_id = Long.valueOf(DeptCompetecnTable.this.getValueAt(row, 0).toString());
			for (JecnTreeBean elemt : data) {
				if (elemt.getId().longValue() == table_id.longValue()) {
					elemt.setDownLoad(!button.isSelected());
				}
			}
			return (Component) value;
		}

		public Object getCellEditorValue() {
			button.removeItemListener(this);
			return button;
		}

		public void itemStateChanged(ItemEvent e) {
			super.fireEditingStopped();
		}
	}

	class CheckBoxRenderer implements TableCellRenderer {

		@Override
		public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected,
				boolean hasFocus, int row, int column) {
			if (value == null) {
				return null;
			}
			return (Component) value;
		}
	}

}
