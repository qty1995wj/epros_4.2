package epros.designer.gui.process.guide.guideTable;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.log4j.Logger;

import com.jecn.epros.server.bean.process.JecnFlowKpi;

import epros.designer.gui.process.guide.UpdateKPIValueDialog;
import epros.designer.util.JecnProperties;

/*******************************************************************************
 * 编辑KPI值
 * 
 * @author 2012-07-12
 * 
 */
public class DesignerUpdateKPIValueDialog extends UpdateKPIValueDialog {
	private static Logger log = Logger.getLogger(DesignerUpdateKPIValueDialog.class);
	private JecnFlowKpi flowKpi = null;

	public DesignerUpdateKPIValueDialog(JecnFlowKpi flowKpi, String verName,
			String honName, String kpiName, Long flowKpiId, Long flowId) {
		super(flowKpi, verName, honName, kpiName, flowKpiId, flowId);
		this.flowKpi = flowKpi;
	}

	@Override
	public void saveData() {
		String reg = "^[0-9]*$";
		String nreg = "^\\d{4}$";
		String strLongitudinal = this.longitudinalField.getText();

		String weekTex = this.longtwoField.getText();// 周 月 季度 天数
		String yearTex = this.longOneField.getText();// 年
		// SimpleDateFormat sdf = null;
		// sdf = new SimpleDateFormat("yyyy");
		// Date dateHonName = null;
		if (strLongitudinal == null || "".equals(strLongitudinal)) {
			this.okShowInfo.setText(JecnProperties.getValue("pleaseInputKPIYCoordinateValue"));
			return;
		} else if (!strLongitudinal.matches(reg)) {
			this.okShowInfo.setText(JecnProperties.getValue("KPIOrdinateValueCanOnlyInputDigital"));
			return;
		}
		if (honName.equals(JecnProperties.getValue("dayLab"))) {
			if (this.transverseDateField.getText() == null
					|| "".equals(this.transverseDateField.getText().toString())) {
				this.okShowInfo.setText(JecnProperties.getValue("pleaseInputKPIAbscissaValue"));
				return;
			}
		} else {
			if (!honName.equals(JecnProperties.getValue("yearLab"))) {

				if (weekTex == null || "".equals(weekTex)) {
					// 周
					if (honName.equals(JecnProperties.getValue("weekLab"))) {
						this.okShowInfo.setText(JecnProperties.getValue("KPIAbscissaValueWeeksCantForEmpty"));
					}
					// 月
					else if (honName
							.equals(JecnProperties.getValue("monthLab"))) {
						this.okShowInfo.setText(JecnProperties.getValue("KPIAbscissaValueMonthCantForEmpty"));
					}
					// 季度
					else if (honName.equals(JecnProperties
							.getValue("quarterLab"))) {
						this.okShowInfo.setText(JecnProperties.getValue("KPIAbscissaValueQuarterCantForEmpty"));
					}
					return;
				} else {
					// 周
					if (honName.equals(JecnProperties.getValue("weekLab"))) {
						if (Integer.parseInt(weekTex) > 52) {
							this.okShowInfo.setText(JecnProperties.getValue("KPIAbscissaValueCycleIsNotGreaterThan52"));
							return;
						}

					}
					// 月
					if (honName.equals(JecnProperties.getValue("monthLab"))) {
						if (Integer.parseInt(weekTex) > 12) {
							this.okShowInfo.setText(JecnProperties.getValue("KPIAbscissaValueMonthValueIsNotGreaterThan12"));
							return;
						}

					}
					// 季度
					if (honName.equals(JecnProperties.getValue("quarterLab"))) {
						if (Integer.parseInt(weekTex) > 4) {
							this.okShowInfo.setText(JecnProperties.getValue("KPIAbscissaValueQuarterValueIsNotGreaterThan4"));
							return;
						}

					}
				}
			}
			if (yearTex == null || "".equals(yearTex)) {
				this.okShowInfo.setText(JecnProperties.getValue("KPIAbscissaValueYearsCantForEmpty"));
				return;
			} else if (!yearTex.matches(nreg)) {
				// dateHonName = sdf.parse(honValue);
				this.okShowInfo.setText(JecnProperties.getValue("KPIAbscissaValueYearValueIsNotGreaterThan4"));
				return;
			}

		}
		SimpleDateFormat sdf = null;
		if (honName != null || !"".equals(honName)) {
			if (honName.equals(JecnProperties.getValue("dayLab"))) {
				sdf = new SimpleDateFormat("yyyy-MM-dd");
				honValue = this.transverseDateField.getText();
			} else if (honName.equals(JecnProperties.getValue("yearLab"))) {
				sdf = new SimpleDateFormat("yyyy");
				honValue = this.yearField.getText();
			} else if (honName.equals(JecnProperties.getValue("weekLab"))) {
				sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
				honValue = this.longOneField.getText() + "-01" + "-01 " + "01:"
						+ this.longtwoField.getText() + ":01";
			} else if (honName.equals(JecnProperties.getValue("monthLab"))) {
				sdf = new SimpleDateFormat("yyyy-MM");
				honValue = this.longOneField.getText() + "-"
						+ this.longtwoField.getText();
			} else if (honName.equals(JecnProperties.getValue("quarterLab"))) {
				sdf = new SimpleDateFormat("yyyy-MM");
				honValue = this.longOneField.getText() + "-"
						+ this.longtwoField.getText();
			}
		}
		// 将数据放入JecnFlowKpi 中
		// flowKpi.setKpiAndId(flowKpiId);
		Date dateHonName = null;
		try {
			dateHonName = sdf.parse(honValue);
		} catch (ParseException e) {
			log.error("DesignerUpdateKPIValueDialog saveData is error",e);
		}
		flowKpi.setKpiHorVlaue(dateHonName);
		// 纵坐标
		flowKpi.setKpiValue(this.longitudinalField.getText());
		isOperation = true;
		this.dispose();
	}

	@Override
	public String getKPIValueTitle() {
		return JecnProperties.getValue("editFlowKPIValue");
	}

	@Override
	public String getLongField() {
		return verName;
	}

	@Override
	public String getTransverseField() {
		return honName;
	}

	@Override
	public String getKPIName() {
		return kpiName;
	}

}
