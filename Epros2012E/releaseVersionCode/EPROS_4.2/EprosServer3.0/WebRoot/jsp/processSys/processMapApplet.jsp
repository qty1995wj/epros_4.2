<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	</head>
	<body>
		<applet id="jecnProcessId" codebase="${basePath}"
			code="view.JecnMapApplet" width="100%" height="100%"
			archive="${basePath}view/view.jar">
			<param name="mapID" value="${param.mapID}" />
			<param name="mapName" value="${param.mapName}" />
			<param name="mapType" value="${param.mapType}" />
			<param name="isPub" value="${param.isPub}" />
			<param name="target" value="${param.target}" />
			<param name="sessionID" value="JSESSIONID=<%=session.getId()%>" />
		</applet>
	</body>
</html>