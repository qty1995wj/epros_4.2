package epros.draw.gui.workflow.util;

import epros.draw.gui.top.button.JecnToolbarContentButton;
import epros.draw.gui.workflow.JecnDrawMainPanel;
import epros.draw.gui.workflow.tabbedPane.JecnDrawDesktopPane;

/**
 * 工具栏同步处理
 * 
 * @author ZHANGXH
 * @date： 日期：Jul 31, 2012 时间：2:47:46 PM
 */
public class JecnSyncTollBar {

	/**
	 * 清空面板同步处理
	 * 
	 */
	public static void syncClearWorkPanel() {
		// 设置放大缩小下拉框系统默认倍数
		JecnDrawMainPanel.getMainPanel().getToolbar().getOftenPartPanel()
				.setZoomComboBoxDefaultItem();
		// 撤销、恢复按钮至于灰色
		// 撤销按钮
		JecnToolbarContentButton undoButton = JecnDrawMainPanel.getMainPanel()
				.getToolbar().getOftenPartPanel().getUndoBtn();
		undoButton.setEnabled(false);
		// 恢复按钮
		JecnToolbarContentButton redoButton = JecnDrawMainPanel.getMainPanel()
				.getToolbar().getOftenPartPanel().getRedoBtn();
		redoButton.setEnabled(false);
	}

	/**
	 * 面板切换，同步处理
	 * 
	 * @param desktopPane
	 */
	public static void syncChangedProcess(JecnDrawDesktopPane desktopPane) {
		if (desktopPane == null) {
			return;
		}
		// 同步功能：刷新滚动面板标题栏
		desktopPane.getScrollPanle().showScrollPanelHeader();
		// 同步撤销恢复状态
		desktopPane.getUndoRedoManager().syncToolBar();
		// 同步工具栏放大缩小倍数
		JecnDrawMainPanel.getMainPanel().getToolbar().getOftenPartPanel()
				.setZoomComboBoxVaule();
		// 切换面板清空选中集合
		JecnWorkflowUtil.hideAllResizeHandle();

		// 默认流程元素库默认指针
		JecnDrawMainPanel.getMainPanel().getBoxPanel().initGuide();
	}
}
