package com.jecn.epros.server.action.web.login.ad;

import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import com.jecn.epros.server.action.web.login.LoginAction;
import com.jecn.epros.server.bean.popedom.JecnUser;
import com.jecn.epros.server.util.JecnUtil;
import com.jecn.webservice.common.TaskApiUrlHandler;

/**
 * 
 * 单点登录父类
 * 
 * @author ZHOUXY
 * 
 */
public abstract class JecnAbstractADLoginAction {
	protected static final Logger log = Logger.getLogger(JecnAbstractADLoginAction.class);

	/** 登录主action */
	protected LoginAction loginAction = null;
	protected String redirectUrl;

	public JecnAbstractADLoginAction(LoginAction loginAction) {
		if (loginAction == null) {
			throw new IllegalArgumentException("参数为空。loginAction" + loginAction);
		}
		this.loginAction = loginAction;
	}

	/**
	 * 
	 * 只通过登录名称验证是否存在，存在登录
	 * 
	 * @param loginName
	 *            String 解密后的登录名称
	 * @return String 返回类型
	 */
	protected String loginByLoginName(String loginName) {
		try {
			if (StringUtils.isBlank(loginName)) {
				log.error("登录名称为空！");
				return LoginAction.INPUT;
			}

			List<JecnUser> userList = loginAction.getPersonService().getUserByLoginName(loginName);

			if (userList.size() != 1) {
				log.error("通过登录名称获取人员信息不是只有一条数据：loginName=" + loginName);
				return LoginAction.INPUT;
			}
			JecnUser jecnUser = userList.get(0);
			// 给登录名称/密码赋值
			loginAction.setLoginName(jecnUser.getLoginName());
			loginAction.setPassword(jecnUser.getPassword());

			// 验证登录
			return loginAction.userLoginGen(false);

		} catch (Exception e) {
			log.error("获取用户信息异常", e);
			return LoginAction.INPUT;
		}
	}

	/**
	 * 根据域名匹配邮箱登录
	 * 
	 * @param loginName
	 * @return
	 */
	protected String loginByLoginDomainName(String loginName) {
		try {
			if (StringUtils.isBlank(loginName)) {
				log.error("登录名称为空！");
				return LoginAction.INPUT;
			}
			JecnUser jecnUser = loginAction.getPersonService().isLoginByDomainName(loginName);

			if (jecnUser == null) {
				log.error("通过登录名称获取人员信息不是只有一条数据：loginName=" + loginName);
				return LoginAction.INPUT;
			}
			// 给登录名称/密码赋值
			loginAction.setLoginName(jecnUser.getLoginName());
			loginAction.setPassword(jecnUser.getPassword());

			// 验证登录
			return loginAction.userLoginGen(false);

		} catch (Exception e) {
			log.error("获取用户信息异常", e);
			return LoginAction.INPUT;
		}
	}

	/**
	 * 
	 * 单点登录入口
	 * 
	 * @return String
	 */
	public abstract String login();

	protected String redirect() throws Exception {
		redirectUrl = getRedirectUrlParam();
		loginAction.setCustomRedirectURL(redirectUrl);
		return "redirect";
	}

	protected String getRedirectUrlParam() throws Exception {
		String result = loginAction.getRequest().getParameter(getUrlParamName());
		if (StringUtils.isBlank(result)) {
			throw new RuntimeException("getRedirectUrlParam 参数值为空");
		}
		if (urlNeedDecode()) {
			result = JecnUtil.decodeUrl(result);
		}
		log.info("最后跳转的地址为：" + result);
		return result;
	}

	/**
	 * 根据 参数 id 和 类型 获取 单点跳转的 流程,制度 ,文件页面
	 * 
	 * @return
	 * @throws Exception
	 */
	protected String getRequestUrl() throws Exception {
		// 获取传过来的流程ID
		String url = "";
		String id = loginAction.getRequest().getParameter(getParamId());
		if (StringUtils.isNotBlank(id)) {
			String type = loginAction.getRequest().getParameter(getParamType()).toUpperCase();
			if ("PROCESS".equals(type) || "PROCESS_MAP".equals(type)) {
				url = JecnUtil.decodeUrl(TaskApiUrlHandler.getProcess_Map_Chart(id, "", loginAction.getJecnUser()
						.getPeopleId().toString()));
			} else if ("RULE".equals(type)) {
				url = JecnUtil.decodeUrl(TaskApiUrlHandler.getRule_Info(id, "", loginAction.getJecnUser().getPeopleId()
						.toString()));
			} else if ("FILE".equals(type)) {
				url = JecnUtil.decodeUrl(TaskApiUrlHandler.getFile_Info(id, "", loginAction.getJecnUser().getPeopleId()
						.toString()));
			}
		}
		return url;
	}

	protected String getParamId() {
		return "id";
	}

	protected String getParamType() {
		return "type";
	}

	protected String getUrlParamName() {
		return "url";
	}

	protected boolean urlNeedDecode() {
		return true;
	}

	public String getRedirectUrl() {
		return redirectUrl;
	}

	public void setRedirectUrl(String redirectUrl) {
		this.redirectUrl = redirectUrl;
	}

}
