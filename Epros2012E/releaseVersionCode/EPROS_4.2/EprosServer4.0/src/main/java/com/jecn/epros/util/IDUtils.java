package com.jecn.epros.util;

import java.util.UUID;

/**
 * Created by thinkpad on 2017/1/3.
 */
public class IDUtils {

    public static String makeUUID() {
        return UUID.randomUUID().toString();
    }
}
