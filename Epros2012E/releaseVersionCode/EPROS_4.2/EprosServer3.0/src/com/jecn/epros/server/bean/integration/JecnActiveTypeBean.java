package com.jecn.epros.server.bean.integration;

/**
 * 活动类别
 * 
 * @author ZHANGXIAOHU
 * @date： 日期：2013-10-30 时间：下午02:12:31
 */
public class JecnActiveTypeBean implements java.io.Serializable {
	/** 主键ID */
	private Long typeId;
	/** 类别名称 */
	private String typeName;

	public Long getTypeId() {
		return typeId;
	}

	public void setTypeId(Long typeId) {
		this.typeId = typeId;
	}

	public String getTypeName() {
		return typeName;
	}

	public void setTypeName(String typeName) {
		this.typeName = typeName;
	}

}
