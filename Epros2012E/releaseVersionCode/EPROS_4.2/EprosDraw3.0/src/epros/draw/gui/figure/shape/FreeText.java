/*
 * FreeText.java
 *自由文本框
 * Created on 2008年12月2日, 上午10:00
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */
package epros.draw.gui.figure.shape;

import java.awt.Graphics;
import java.awt.Graphics2D;

import epros.draw.constant.JecnToolBoxConstant.MapElemType;
import epros.draw.data.figure.JecnFigureData;
import epros.draw.gui.figure.BaseRect;
import epros.draw.gui.figure.JecnPaintEnum;
import epros.draw.gui.workflow.JecnDrawMainPanel;
import epros.draw.util.DrawCommon;

/**
 * 
 * 自由文本框
 * 
 * @author zhangxh
 * @date： 日期：Mar 22, 2012 时间：10:40:13 AM
 */
public class FreeText extends BaseRect {

	/** Creates a new instance of FreeText */
	public FreeText(JecnFigureData figureData) {
		super(figureData);
	}

	@Override
	public void paintComponent(Graphics g) {
		userWidth = this.getWidth() - 1;
		userHeight = this.getHeight() - 1;
		initPaint(flowElementData.getMapElemType());
		super.paintComponent(g);
	}
	
	

	public void initPaint(MapElemType mapElemType) {
		switch (mapElemType) {
		case Oval:// 椭圆
			break;
		case Triangle:// 三角形
			initTriangle();
			break;
		case FunctionField:// 四边形
			break;
		case Pentagon:// 五边形
			paintPentagon();
			break;
		case RightHexagon:// 箭头六边形
		case InterfaceRightHexagon://接口六边形
			initRightHexagon();
			break;
		case InputHandFigure:// 手动输入
			initInputHandFigure();
			break;
		case IsoscelesTrapezoidFigure:// 等腰梯形
			initIsoscelesTrapezoidFigure();
			break;
		default:
			break;
		}
	}
	
	private void initIsoscelesTrapezoidFigure(){
		if (this.getFlowElementData().getCurrentAngle() == 0) {// 默认零度
			x1 = 0;
			y1 = 0;
			x2 = userWidth;
			y2 = 0;
			x3 = this.getWidth() - 25;
			y3 = userHeight;
			x4 = 25;
			y4 = userHeight;
		} else if (this.getFlowElementData().getCurrentAngle() == 90.0) {
			x1 = 0;
			y1 = 25;
			x2 = userWidth;
			y2 = 0;
			x3 = userWidth;
			y3 = userHeight;
			x4 = 0;
			y4 = this.getHeight() - 25;
		} else if (this.getFlowElementData().getCurrentAngle() == 180.0) {
			x1 = 25;
			y1 = 0;
			x2 = this.getWidth() - 25;
			y2 = 0;
			x3 = userWidth;
			y3 = userHeight;
			x4 = 0;
			y4 = userHeight;
		} else if (this.getFlowElementData().getCurrentAngle() == 270.0) {
			x1 = 0;
			y1 = 0;
			x2 = userWidth;
			y2 = 25;
			x3 = userWidth;
			y3 = this.getHeight() - 25;
			x4 = 0;
			y4 = userHeight;
		}
	}
	
	private void initInputHandFigure(){
		double scale = this.getFlowElementData().getCurrentAngle();
		if (scale == 0.0) {
			x1 = 0;
			y1 = userHeight / 3;
			x2 = userWidth;
			y2 = 0;
			x3 = userWidth;
			y3 = userHeight;
			x4 = 0;
			y4 = userHeight;
		} else if (scale == 90.0) {
			x1 = 0;
			y1 = 0;
			x2 = userWidth * 2 / 3;
			y2 = 0;
			x3 = userWidth;
			y3 = userHeight;
			x4 = 0;
			y4 = userHeight;
		} else if (scale == 180.0) {
			x1 = 0;
			y1 = 0;
			x2 = userWidth;
			y2 = 0;
			x3 = userWidth;
			y3 = userHeight * 2 / 3;
			x4 = 0;
			y4 = userHeight;
		} else if (scale == 270.0) {
			x1 = 0;
			y1 = 0;
			x2 = userWidth;
			y2 = 0;
			x3 = userWidth;
			y3 = userHeight;
			x4 = userWidth / 3;
			y4 = userHeight;
		}
	}
	
	private void initTriangle(){
		if (flowElementData.getCurrentAngle() == 0.0) {
			x1 = userWidth / 2;
			y1 = 0;
			x2 = userWidth;
			y2 = userHeight;
			x3 = 0;
			y3 = userHeight;
		} else if (flowElementData.getCurrentAngle() == 90.0) {
			x1 = userWidth;
			y1 = userHeight / 2;
			x2 = 0;
			y2 = userHeight;
			x3 = 0;
			y3 = 0;
		} else if (flowElementData.getCurrentAngle() == 180.0) {
			x1 = userWidth / 2;
			y1 = userHeight;
			x2 = 0;
			y2 = 0;
			x3 = userWidth;
			y3 = 0;
		} else if (flowElementData.getCurrentAngle() == 270.0) {
			x1 = 0;
			y1 = userHeight / 2;
			x2 = userWidth;
			y2 = 0;
			x3 = userWidth;
			y3 = userHeight;
		}
	}
	private void initRightHexagon(){
		double scale = JecnDrawMainPanel.getMainPanel().getCurrWorkflowScale();
		int count = DrawCommon.convertDoubleToInt(19 * scale);
		if (flowElementData.getCurrentAngle() == 0.0) {
			x1 = 0;
			y1 = 0;
			x2 = userWidth - count;
			y2 = 0;
			x3 = userWidth;
			y3 = userHeight / 2;
			x4 = userWidth - count;
			y4 = userHeight;
			x5 = 0;
			y5 = userHeight;
			x6 = count;
			y6 = userHeight / 2;

		} else if (flowElementData.getCurrentAngle() == 90.0) {
			x1 = 0;
			y1 = 0;
			x2 = userWidth / 2;
			y2 = count;
			x3 = userWidth;
			y3 = 0;
			x4 = userWidth;
			y4 = userHeight - count;
			x5 = userWidth / 2;
			y5 = userHeight;
			x6 = 0;
			y6 = userHeight - count;
		} else if (flowElementData.getCurrentAngle() == 180.0) {
			x1 = count;
			y1 = 0;
			x2 = userWidth;
			y2 = 0;
			x3 = userWidth - count;
			y3 = userHeight / 2;
			x4 = userWidth;
			y4 = userHeight;
			x5 = count;
			y5 = userHeight;
			x6 = 0;
			y6 = userHeight / 2;
		} else if (flowElementData.getCurrentAngle() == 270.0) {
			x1 = 0;
			y1 = count;
			x2 = userWidth / 2;
			y2 = 0;
			x3 = userWidth;
			y3 = count;
			x4 = userWidth;
			y4 = userHeight;
			x5 = userWidth / 2;
			y5 = userHeight - count;
			x6 = 0;
			y6 = userHeight;
		}
	}
	/**
	 * 五边形
	 */
	private void paintPentagon() {
		userWidth = this.getWidth() - 1;
		userHeight = this.getHeight() - 1;
		double scale = JecnDrawMainPanel.getMainPanel().getCurrWorkflowScale();
		int count = DrawCommon.convertDoubleToInt(19 * scale);
		if (flowElementData.getCurrentAngle() == 0.0) {
			x1 = 0;
			y1 = 0;
			x2 = userWidth - count;
			y2 = 0;
			x3 = userWidth;
			y3 = userHeight / 2;
			x4 = userWidth - count;
			y4 = userHeight;
			x5 = 0;
			y5 = userHeight;
		} else if (flowElementData.getCurrentAngle() == 90.0) {
			x1 = 0;
			y1 = 0;
			x2 = userWidth;
			y2 = 0;
			x3 = userWidth;
			y3 = userHeight - count;
			x4 = userWidth / 2;
			y4 = userHeight;
			x5 = 0;
			y5 = userHeight - count;
		} else if (flowElementData.getCurrentAngle() == 180.0) {
			x1 = 0;
			y1 = userHeight / 2;
			x2 = count;
			y2 = 0;
			x3 = userWidth;
			y3 = 0;
			x4 = userWidth;
			y4 = userHeight;
			x5 = count;
			y5 = userHeight;
		} else if (flowElementData.getCurrentAngle() == 270.0) {
			x1 = userWidth / 2;
			y1 = 0;
			x2 = userWidth;
			y2 = count;
			x3 = userWidth;
			y3 = userHeight;
			x4 = 0;
			y4 = userHeight;
			x5 = 0;
			y5 = count;
		}
	}

	/**
	 * 无填充
	 * 
	 */
	public void paintNoneFigure(Graphics2D g2d) {
		JecnPaintEnum.INTANCE.paintNoneFigure(flowElementData.getMapElemType(), flowElementData, g2d, x1, x2, x3, x4, x5, x6, y1, y2, y3,
				y4, y5, y6, userWidth, userHeight);
	}

}
