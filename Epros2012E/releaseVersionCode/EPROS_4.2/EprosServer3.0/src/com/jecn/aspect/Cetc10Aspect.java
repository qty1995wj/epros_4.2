//package com.jecn.aspect;
//
//import org.aspectj.lang.JoinPoint;
//import org.aspectj.lang.annotation.AfterReturning;
//import org.aspectj.lang.annotation.Aspect;
//import org.aspectj.lang.annotation.Before;
//import org.aspectj.lang.annotation.Pointcut;
//import org.springframework.stereotype.Component;
//
//@Aspect
//@Component
//public class Cetc10Aspect {
//
//	/**
//	 * 多个表达式之间使用 || 拼接
//	 * 如果需要植入切点的方法有多个重载，可以指定方法体内的参数类型来明确调用某一个重载方法
//	 **/
//	private static final String POINT_CUT = "execution(public * com.jecn.epros.server.action.web.process.ProcessAction.process(..)) "
//			+ "|| execution(public * com.jecn.epros.server.action.web.rule.RuleAction.rule(..))";
//
//	// 此方法不需要实现，只是为了声明一个切点，方便调用
//	@Pointcut(POINT_CUT)
//	public void taskFlow() {};
//
//	@Before(value = "taskFlow()")
//	public void doBefore(JoinPoint joinPoint) throws Throwable {
//		// 可以通过 joinPoint 拿到触发切点的方法及参数等等信息
//	}
//
//	@AfterReturning(returning = "ret", pointcut = "taskFlow()")
//	public void doAfterReturning(Object ret) throws Throwable {
//		// Object 类型可以声明成切点方法返回的类型
//	}
//
//}
