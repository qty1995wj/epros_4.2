package epros.draw.gui.figure.shape.part;

import epros.draw.data.figure.JecnFigureData;
import epros.draw.gui.figure.JecnBaseRhombus;

/**
 * 石勘院添加专家决策
 * 
 * @author ZXH
 * @date 2016-10-18上午10:53:08
 */
public class ExpertRhombusAR extends JecnBaseRhombus {

	public ExpertRhombusAR(JecnFigureData jecnActiveData) {
		super(jecnActiveData);
	}
}
