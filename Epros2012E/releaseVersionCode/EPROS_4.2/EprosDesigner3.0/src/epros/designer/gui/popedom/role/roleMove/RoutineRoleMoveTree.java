package epros.designer.gui.popedom.role.roleMove;

import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.jecn.epros.server.bean.tree.JecnTreeBean;

import epros.designer.gui.common.JecnConstants;
import epros.designer.gui.popedom.role.JecnRoleCommon;
import epros.designer.tree.JecnRoutineTree;
import epros.designer.tree.JecnTreeModel;
import epros.designer.util.ConnectionPool;

public class RoutineRoleMoveTree extends JecnRoutineTree {
	private Logger log = Logger.getLogger(RoutineRoleMoveTree.class);
	/***/
	private List<JecnTreeBean> list = new ArrayList<JecnTreeBean>();

	/** 待移动ID集合 */
	private List<Long> listIds = new ArrayList<Long>();

	public RoutineRoleMoveTree(List<Long> listIds) {
		super(listIds);
		this.listIds = listIds;
	}

	@Override
	public JecnTreeModel getTreeModel() {
		try {
			list = ConnectionPool.getJecnRole().getAllRoleDirs(
					JecnConstants.projectId);
		} catch (Exception e) {
			log.error("RoutineRoleMoveTree getTreeModel is error！", e);
		}
		return JecnRoleCommon.getTreeMoveModel(list, listIds);
	}

	@Override
	public boolean isSelectMutil() {
		return false;
	}

	@Override
	public void jTreeMousePressed(MouseEvent evt) {
	}

}
