package com.jecn.epros.server.action.web.task;

import java.util.Date;

import com.jecn.epros.server.webBean.WebLoginBean;

import junit.JUnitBaseAction;

public class TestTaskAction extends JUnitBaseAction {

	@Override
	protected void setupBeforeInitDispatcher() throws Exception {
		super.setupBeforeInitDispatcher();
		WebLoginBean webLoginBean = createWebLoginBean("admin", "a");
		this.webLoginBean = webLoginBean;
	}

	/**
	 * 点击"我的主页"时执行的方法 测试我的任务列表
	 */
	public void testGetMyTaskTotal() {
		try {
			this.setHttpGet();
			String result = this.executeActionProxy("/getMyTaskTotal.action");
			System.out.println("result:" + result);
			System.out
					.println("taskTotal:" + request.getAttribute("taskTotal"));
			assertEquals("success", result);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	/**
	 * 点击"我的体系任务"和"搜索"时执行的方法 测试 查询我的任务
	 */

	public void testGetMyTask() {
		this.setHttpGet();
		try {
			String result = this.executeActionProxy("/getMyTask.action");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
