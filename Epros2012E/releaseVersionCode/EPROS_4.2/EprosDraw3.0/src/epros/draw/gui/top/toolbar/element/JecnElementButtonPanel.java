package epros.draw.gui.top.toolbar.element;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JPanel;

import epros.draw.constant.JecnLogConstants;
import epros.draw.error.ErrorInfoConstant;
import epros.draw.util.JecnUIUtil;

/**
 * 
 * 主要用于颜色显示面板
 * 
 * 主要用于显示是面板形式，动作使用按钮动作
 * 
 * @author ZHOUXY
 * 
 */
public class JecnElementButtonPanel extends JPanel {
	/** 面板上的按钮 主要为了添加事件操作 */
	private JButton button = null;
	/** 动作命令 */
	private String actionCommand = null;
	/** 流程元素面板 */
	private JecnFlowElementPanel flowElementPanel = null;

	/**
	 * 
	 * 主要用于颜色显示面板：主要用于显示是面板形式，动作使用按钮动作
	 * 
	 * @param flowElementPanel
	 *            JecnFlowElementPanel 流程元素面板
	 * @param size
	 *            Dimension 颜色显示面板大小
	 * @param actionCommand
	 *            String 按钮动作名称
	 */
	public JecnElementButtonPanel(JecnFlowElementPanel flowElementPanel,
			Dimension size, String actionCommand) {
		if (flowElementPanel == null || actionCommand == null) {
			JecnLogConstants.LOG_ELEMENT_BUTTON_PANEL
					.error("JecnElementButtonPanel类构造函数：参数为null。flowElementPanel="
							+ flowElementPanel
							+ ";actionCommand="
							+ actionCommand);
			throw new IllegalArgumentException(ErrorInfoConstant.PARA_OBJ_NULL);
		}

		this.flowElementPanel = flowElementPanel;
		this.actionCommand = actionCommand;

		initComponents(size);
	}

	private void initComponents(Dimension size) {
		//
		this.setLayout(new BorderLayout());
		// 初始化背景颜色
		this.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		this.setBorder(BorderFactory.createLineBorder(Color.GRAY));
		// 大小
		if (size != null) {
			this.setPreferredSize(size);
			this.setMinimumSize(size);
			this.setMaximumSize(size);
		}

		// 按钮 主要为了添加事件操作
		button = new JButton();
		// 不绘制内容区域
		button.setContentAreaFilled(false);
		// 不显示选中框
		button.setFocusable(false);
		button.addActionListener(flowElementPanel);
		button.setActionCommand(actionCommand);
		this.add(button);
	}
	
    public void setEnabled(boolean enabled) {
    	super.setEnabled(enabled);
    	this.button.setEnabled(enabled);
    }
}