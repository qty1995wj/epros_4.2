package com.jecn.epros.server.service.dataImport.sync.excelOrDb.datafilter.impl;

import com.jecn.epros.server.service.dataImport.sync.excelOrDb.datafilter.AbstractSyncSpecialDataFilter;
import com.jecn.epros.server.util.JecnPath;
import com.jecn.epros.server.webBean.dataImport.sync.excelOrDB.BaseBean;

public class Cetc10SyncDataFilter extends AbstractSyncSpecialDataFilter {

	public Cetc10SyncDataFilter(BaseBean baseBean) {
		super(baseBean);
	}

	@Override
	protected void initSpecialDataPath() {
		this.specialDataPath = JecnPath.CLASS_PATH + "cfgFile/cetc10/cetc10SyncSpecialData.xls";
	}

	@Override
	protected void filterDeptDataBeforeCheck() {
		// TODO Auto-generated method stub

	}

	@Override
	protected void filterUserPosDataBeforeCheck() {
		// TODO Auto-generated method stub

	}

	@Override
	public void removeUnusefulDataAfterCheck() {
		// TODO Auto-generated method stub

	}

}
