package epros.designer.gui.publish;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;

import org.apache.log4j.Logger;

import com.jecn.epros.server.bean.popedom.JecnUser;
import com.jecn.epros.server.bean.task.JecnTaskHistoryFollow;
import com.jecn.epros.server.bean.task.JecnTaskHistoryNew;
import com.jecn.epros.server.bean.task.TaskConfigItem;
import com.jecn.epros.server.bean.tree.JecnTreeBean;
import com.jecn.epros.server.common.JecnConfigContents.ConfigItemPartMapMark;
import com.jecn.epros.server.util.JecnTreeEnumConstant.TreeNodeType;

import epros.designer.gui.common.JecnTextArea;
import epros.designer.gui.system.config.ui.comp.JecnConfigCheckBox;
import epros.designer.gui.task.buss.TaskCommon;
import epros.designer.gui.task.jComponent.JecnDesignerTaskCommon;
import epros.designer.gui.task.jComponent.JecnTaskJDialog.TaskStateEnum;
import epros.designer.tree.JecnTreeCommon;
import epros.designer.tree.JecnTreeNode;
import epros.designer.util.ConnectionPool;
import epros.designer.util.JecnConfigTool;
import epros.designer.util.JecnProperties;
import epros.designer.util.JecnTool;
import epros.designer.util.JecnVersionUtil;
import epros.draw.gui.swing.JecnPanel;
import epros.draw.gui.swing.textfield.JecnTextFieldAndCalendarPanel;
import epros.draw.gui.top.dialog.JecnDialog;
import epros.draw.system.data.JecnUserCheckInfoData;
import epros.draw.util.DrawCommon;
import epros.draw.util.JecnResourceUtil;
import epros.draw.util.JecnUIUtil;
import epros.draw.util.JecnUserCheckUtil;

/*******************************************************************************
 * 发布
 * 
 * @author Administrator 2012-10-23
 */
public class BasePublishDialog extends JecnDialog {
	/** 主面板 */
	protected JecnPanel mainPanel = new JecnPanel();

	/** 头部面板 */
	protected JecnPanel topPanel = new JecnPanel();

	/** 信息面板 */
	protected JecnPanel infoPanel = new JecnPanel();

	/** 按钮面板 */
	protected JecnPanel butPanel = new JecnPanel();

	/** 流程名称显示Lab **/
	protected JLabel flowNameLab = new JLabel();

	/** 历史记录下拉菜单 */
	JMenu historyMenu = new JMenu(JecnProperties.getValue("historyVersion"));

	protected JecnPanel menuPanel = new JecnPanel();

	/** 流程名称下的分割线 */
	protected JTextField namebutomField = new JTextField();

	/** 版本号Lab */
	protected JLabel versonNumLab = new JLabel(JecnProperties.getValue("versionNumC"));

	/** 版本号Field */
	protected JTextField versonNumField = new JTextField();

	/** 拟稿人Lab */
	protected JLabel draftPeopleLab = new JLabel(JecnProperties.getValue("draftPeopleC"));

	/** 拟稿人Field */
	protected JTextField draftPeopleField = new JTextField();

	/** 文控审核人Lab */
	protected JLabel documentControlAuditLab = new JLabel();// "文控审核人："

	/** 文控审核人Field */
	protected JTextField documentControlAuditField = new JTextField();

	/** 部门审核人Lab */
	protected JLabel deptAuditLab = new JLabel();// "部门审核人："

	/** 部门审核人Field */
	protected JTextField deptAuditField = new JTextField();

	/** 评审人Lab */
	protected JLabel reviewersAuditLab = new JLabel();// "评审人："

	/** 评审人Field */
	protected JecnTextArea reviewersAuditArea = new JecnTextArea();

	/** 评审人面板 */
	protected JScrollPane reviewersAuditScrollPane = new JScrollPane(reviewersAuditArea);

	/** 批准人Lab */
	protected JLabel approvalAuditLab = new JLabel();// "批准人："

	/** 批准人Field */
	protected JTextField approvalAuditField = new JTextField();

	/** 各业务体系负责人Lab */
	protected JLabel operationAuditLab = new JLabel();// /"各业务体系负责人："

	/** 各业务体系负责人Field */
	protected JTextField operationAuditField = new JTextField();

	/** IT总监Lab */
	protected JLabel ITPeopleAuditLab = new JLabel();// "IT总监："

	/** IT总监Field */
	protected JTextField iTPeopleAuditField = new JTextField();

	/** 事业部经理Lab */
	protected JLabel divisionManagerLab = new JLabel();// "事业部经理："

	/** 事业部经理Field */
	protected JTextField divisionManagerField = new JTextField();

	/** 自定义1 **/
	private JLabel taskCustomApproval1Lab = new JLabel();
	/** 自定义2 **/
	private JLabel taskCustomApproval2Lab = new JLabel();
	/** 自定义3 **/
	private JLabel taskCustomApproval3Lab = new JLabel();
	/** 自定义4 **/
	private JLabel taskCustomApproval4Lab = new JLabel();

	/** 自定义1Field */
	protected JecnTextArea taskCustomApproval1Area = new JecnTextArea();
	/** 自定义2Field */
	protected JecnTextArea taskCustomApproval2Area = new JecnTextArea();
	/** 自定义3Field */
	protected JecnTextArea taskCustomApproval3Area = new JecnTextArea();
	/** 自定义4Field */
	protected JecnTextArea taskCustomApproval4Area = new JecnTextArea();

	protected JScrollPane taskCustomApproval1ScrollPane = new JScrollPane(taskCustomApproval1Area);
	protected JScrollPane taskCustomApproval2ScrollPane = new JScrollPane(taskCustomApproval2Area);
	protected JScrollPane taskCustomApproval3ScrollPane = new JScrollPane(taskCustomApproval3Area);
	protected JScrollPane taskCustomApproval4ScrollPane = new JScrollPane(taskCustomApproval4Area);

	/** 总经理Lab */
	protected JLabel generalManagerLab = new JLabel();// "总经理："

	/** 总经理Field */
	protected JTextField generalManagerField = new JTextField();

	/** 变更说明Lab */
	protected JLabel descofChangeLab = new JLabel(JecnProperties.getValue("descofChangeC"));

	/** 变更说明Field */
	protected JecnTextArea descofChangeField = new JecnTextArea();

	/** 变更说明面板 */
	protected JScrollPane descofChangeScrollPane = new JScrollPane(descofChangeField);

	/** 验证提示 */
	private JLabel verfyLab = new JLabel();
	/** 确定按钮 */
	protected JButton okBut = new JButton(JecnProperties.getValue("okBtn"));

	/** 取消按钮 */
	protected JButton cancelBut = new JButton(JecnProperties.getValue("cancelBtn"));
	// 文控历史版本版本号 前五条
	protected List<String> versList = new ArrayList<String>();

	// 文控历史版本版本号 全部
	protected List<String> versListAll = new ArrayList<String>();
	/** 选中树节点 */
	protected JecnTreeNode treeNode = null;
	/** 树节点对应详细信息 */
	protected JecnTreeBean treeBean = null;
	/** 按照审批顺序存各审批阶段的标示 */
	protected List<Integer> listStateMask = new ArrayList<Integer>();
	/** 安装顺序存储显示的个审批环节审批人名称 */
	protected List<JComponent> textFiledList = new ArrayList<JComponent>();
	/** 安装顺序存储显示的个审批环节审批人名称 */
	protected List<JLabel> textJLabelList = new ArrayList<JLabel>();
	/** 安装顺序存储显示的个审批环节审批人名称 */
	protected List<JLabel> textRequiredLabList = new ArrayList<JLabel>();
	/** 根据顺序记录审批阶段名称和审批顺序 */
	protected List<JecnTaskHistoryFollow> taskHistoryFollowList = new ArrayList<JecnTaskHistoryFollow>();
	/** 发布任务的类型 */
	protected int type;
	/** 版本号 */
	private JLabel versionRequiredLab = new JLabel(JecnProperties.getValue("required"));
	/** *拟稿人 */
	private JLabel draftPeopleRequiredLab = new JLabel(JecnProperties.getValue("required"));
	/** 文控审核人 */
	private JLabel applicationRequiredLab = new JLabel(JecnProperties.getValue("required"));
	/** 部门审核人 */
	private JLabel deptPeopleRequiredLab = new JLabel(JecnProperties.getValue("required"));
	/** 评审人 */
	private JLabel reviewRequiredLab = new JLabel(JecnProperties.getValue("required"));
	/** 批准人 */
	private JLabel approveRequiredLab = new JLabel(JecnProperties.getValue("required"));
	/** 各业务体系负责人 */
	private JLabel bussReuiredLab = new JLabel(JecnProperties.getValue("required"));
	/** IT总监 */
	private JLabel fieldITRequiredLab = new JLabel(JecnProperties.getValue("required"));
	/** 事业部经理 */
	private JLabel fieldDivRequiredLab = new JLabel(JecnProperties.getValue("required"));
	/** 总经理 */
	private JLabel fieldGenRequiredLab = new JLabel(JecnProperties.getValue("required"));
	/** 自定义1 **/
	private JLabel fieldTaskCustomApproval1Lab = new JLabel(JecnProperties.getValue("required"));
	/** 自定义2 **/
	private JLabel fieldTaskCustomApproval2Lab = new JLabel(JecnProperties.getValue("required"));
	/** 自定义3 **/
	private JLabel fieldTaskCustomApproval3Lab = new JLabel(JecnProperties.getValue("required"));
	/** 自定义4 **/
	private JLabel fieldTaskCustomApproval4Lab = new JLabel(JecnProperties.getValue("required"));

	/** 变更说明 */
	private JLabel areaChangeDescriptionLab = new JLabel(JecnProperties.getValue("required"));
	/** logger日志 */
	protected Logger log = Logger.getLogger(BasePublishDialog.class);
	/** 点击确定 true 点击确定 */
	protected boolean isOkBut = false;
	/** 节点标签名称 */
	protected String prfName;
	/** 窗体标题名称 */
	protected String strTitle;

	private List<TaskConfigItem> itemBeanList;

	private JLabel implementDateLab = new JLabel(JecnProperties.getValue("implementationDateC"));
	private JecnTextFieldAndCalendarPanel implementDatePanel = new JecnTextFieldAndCalendarPanel(new Date());

	public BasePublishDialog(JecnTreeNode treeNode) {
		this.treeNode = treeNode;
		treeBean = treeNode.getJecnTreeBean();
		// 初始化变量值
		initialize();
		// 初始化带显示数据
		initMethod();
		init();
	}

	protected void initialize() {
	}

	protected void initMethod() {
		if (treeBean == null) {
			return;
		}
		// 设置流程名称 flowNameLab
		flowNameLab.setText(treeBean.getName());
		// 根据流程ID获取文控信息版本号集合
		try {
			versList = ConnectionPool.getDocControlAction().findVersionListByFlowId(treeBean.getId(), type);
			versListAll = ConnectionPool.getDocControlAction().findVersionListByFlowIdNoPage(treeBean.getId(), type);
		} catch (Exception e) {
			log.error("BasePublishDialog initMethod is error！", e);
		}
		// 初始化自动版本号
		JecnVersionUtil.initVersion(versonNumField, treeBean, type);
	}

	/**
	 * 九新药业根据模板类型获取默认审批人
	 * 
	 * @param taskStateEnum
	 * @return
	 */
	private String getDefaultsPeopleAppId(TaskStateEnum taskStateEnum, String defaultName) {
		if (JecnConfigTool.isJiuXinLoginType()
				&& this.treeNode.getJecnTreeBean().getTreeNodeType() == TreeNodeType.process) {// 任务默认审批人显示顺序
			String divisionManDutyPeople = JecnDesignerTaskCommon.getDefaultsPeopleAppId(taskStateEnum, type, treeNode);
			if (divisionManDutyPeople != null && !"".equals(divisionManDutyPeople)) {
				JecnUser userBean = ConnectionPool.getPersonAction().getJecnUserByPeopleId(
						Long.valueOf(divisionManDutyPeople));
				return userBean.getTrueName() == null ? " " : userBean.getTrueName();
			} else {
				return "";
			}
		}
		return defaultName;
	}

	protected void init() {
		this.setSize(this.getWidthMax(), 600);
		// 设置标题
		this.setTitle(strTitle);
		this.setLocationRelativeTo(null);
		this.setResizable(true);
		this.setModal(true);
		verfyLab.setForeground(Color.red);
		Dimension sizemenu = new Dimension(100, 20);
		historyMenu.setPreferredSize(sizemenu);
		historyMenu.setMinimumSize(sizemenu);
		historyMenu.setMaximumSize(sizemenu);
		// 历史版本
		// 获取数据库中的版本信息
		// versList = ;
		for (int i = 0; i < versList.size(); i++) {
			JMenuItem menu = new JMenuItem(versList.get(i));
			historyMenu.add(menu);
		}
		JMenuBar menuBar = new JMenuBar();
		menuPanel.add(menuBar);
		menuBar.add(historyMenu);

		// 评审人
		reviewersAuditArea.setBorder(null);
		reviewersAuditArea.setRows(4);
		reviewersAuditScrollPane.setBorder(BorderFactory.createLineBorder(Color.gray));
		reviewersAuditScrollPane.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		reviewersAuditScrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);

		taskCustomApproval1Area.setBorder(null);
		taskCustomApproval1Area.setRows(4);
		taskCustomApproval1ScrollPane.setBorder(BorderFactory.createLineBorder(Color.gray));
		taskCustomApproval1ScrollPane.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		taskCustomApproval1ScrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);

		taskCustomApproval2Area.setBorder(null);
		taskCustomApproval2Area.setRows(4);
		taskCustomApproval2ScrollPane.setBorder(BorderFactory.createLineBorder(Color.gray));
		taskCustomApproval2ScrollPane.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		taskCustomApproval2ScrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);

		taskCustomApproval3Area.setBorder(null);
		taskCustomApproval3Area.setRows(4);
		taskCustomApproval3ScrollPane.setBorder(BorderFactory.createLineBorder(Color.gray));
		taskCustomApproval3ScrollPane.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		taskCustomApproval3ScrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);

		taskCustomApproval4Area.setBorder(null);
		taskCustomApproval4Area.setRows(4);
		taskCustomApproval4ScrollPane.setBorder(BorderFactory.createLineBorder(Color.gray));
		taskCustomApproval4ScrollPane.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		taskCustomApproval4ScrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);

		// 变更说明
		descofChangeField.setBorder(null);
		descofChangeField.setRows(4);
		descofChangeScrollPane.setBorder(BorderFactory.createLineBorder(Color.gray));
		descofChangeScrollPane.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		descofChangeScrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);

		historyMenu.setBackground(JecnUIUtil.getDefaultBackgroundColor());

		// 分割线设置
		Dimension namebuttondim = new Dimension(100, 1);
		namebutomField.setPreferredSize(namebuttondim);
		namebutomField.setMaximumSize(namebuttondim);
		namebutomField.setMinimumSize(namebuttondim);

		cancelBut.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				cancelButPerformed();
			}
		});

		okBut.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				okButPerformed();
			}
		});

		// 文控审核人
		documentControlAuditLab.setVisible(false);
		documentControlAuditField.setVisible(false);
		applicationRequiredLab.setVisible(false);
		// 部门审核人
		deptAuditLab.setVisible(false);
		deptAuditField.setVisible(false);
		deptPeopleRequiredLab.setVisible(false);
		// 评审人
		reviewersAuditLab.setVisible(false);
		reviewersAuditArea.setVisible(false);
		reviewersAuditScrollPane.setVisible(false);
		reviewRequiredLab.setVisible(false);
		// 自定义1
		taskCustomApproval1Lab.setVisible(false);
		taskCustomApproval1Lab.setVisible(false);
		taskCustomApproval1ScrollPane.setVisible(false);
		fieldTaskCustomApproval1Lab.setVisible(false);
		// 自定义2
		taskCustomApproval2Lab.setVisible(false);
		taskCustomApproval2Lab.setVisible(false);
		taskCustomApproval2ScrollPane.setVisible(false);
		fieldTaskCustomApproval2Lab.setVisible(false);
		// 自定义3
		taskCustomApproval3Lab.setVisible(false);
		taskCustomApproval3Lab.setVisible(false);
		taskCustomApproval3ScrollPane.setVisible(false);
		fieldTaskCustomApproval3Lab.setVisible(false);
		// 自定义4
		taskCustomApproval4Lab.setVisible(false);
		taskCustomApproval4Lab.setVisible(false);
		taskCustomApproval4ScrollPane.setVisible(false);
		fieldTaskCustomApproval4Lab.setVisible(false);

		// 批准人
		approvalAuditLab.setVisible(false);
		approvalAuditField.setVisible(false);
		approveRequiredLab.setVisible(false);
		// 各业务体系负责人
		operationAuditLab.setVisible(false);
		operationAuditField.setVisible(false);
		bussReuiredLab.setVisible(false);
		// IT总监
		ITPeopleAuditLab.setVisible(false);
		iTPeopleAuditField.setVisible(false);
		fieldITRequiredLab.setVisible(false);
		// 事业部经理
		divisionManagerLab.setVisible(false);
		divisionManagerField.setVisible(false);
		fieldDivRequiredLab.setVisible(false);
		// 总经理
		generalManagerLab.setVisible(false);
		generalManagerField.setVisible(false);
		fieldGenRequiredLab.setVisible(false);
		// 版本号必填提示*
		versionRequiredLab.setForeground(Color.red);
		// 拟稿人必填提示*
		draftPeopleRequiredLab.setForeground(Color.red);
		// 变更说明必填提示*
		areaChangeDescriptionLab.setForeground(Color.red);

		itemBeanList = listApproveStage();
		// 冒号
		String chinaColon = JecnProperties.getValue("chinaColon");

		for (TaskConfigItem itemBean : itemBeanList) {

			if (!itemBean.getMark().equals(ConfigItemPartMapMark.taskAppPubType.toString())) {// 判断不为主导发布类型
				// 记录文控从表
				JecnTaskHistoryFollow taskHistoryFollow = new JecnTaskHistoryFollow();
				// 显示顺序
				taskHistoryFollow.setSort(itemBean.getSort());
				// 审批环节名称
				taskHistoryFollow.setAppellation(itemBean.getName());
				taskHistoryFollow.setEn_Appellation(itemBean.getEnName());
				taskHistoryFollowList.add(taskHistoryFollow);
			}

			// 各阶段审批人名称
			String name = itemBean.getName(JecnResourceUtil.getLocale()) + chinaColon;

			// itemBean.getIsShow() 1：显示
			if ("taskAppDocCtrlUser".equals(itemBean.getMark()) && "1".equals(itemBean.getValue())) {// 文控审核人
				documentControlAuditLab.setText(name);
				documentControlAuditLab.setVisible(true);
				documentControlAuditField.setVisible(true);
				documentControlAuditField.setEditable(whetherEdit());
				textFiledList.add(documentControlAuditField);
				listStateMask.add(1);
				textJLabelList.add(documentControlAuditLab);
				if (itemBean.isNotEmpty()) {// bootean true：必填；false：非必填
					// 文控审核人必填项
					applicationRequiredLab.setVisible(true);
					applicationRequiredLab.setForeground(Color.red);
				}
				textRequiredLabList.add(applicationRequiredLab);
				documentControlAuditField.setText(getDefaultsPeopleAppId(TaskStateEnum.a1,
						getApproveStateName(itemBean)));
			} else if ("taskAppDeptUser".equals(itemBean.getMark()) && "1".equals(itemBean.getValue())) {// 部门审核人
				deptAuditLab.setText(name);
				deptAuditLab.setVisible(true);
				deptAuditField.setVisible(true);
				deptAuditField.setEditable(whetherEdit());
				textFiledList.add(deptAuditField);
				listStateMask.add(2);
				textJLabelList.add(deptAuditLab);
				if (itemBean.isNotEmpty()) {// bootean true：必填；false：非必填
					// 文控审核人必填项
					deptPeopleRequiredLab.setVisible(true);
					deptPeopleRequiredLab.setForeground(Color.red);
				}
				textRequiredLabList.add(deptPeopleRequiredLab);
				deptAuditField.setText(getDefaultsPeopleAppId(TaskStateEnum.a2, getApproveStateName(itemBean)));
			} else if ("taskAppReviewer".equals(itemBean.getMark()) && "1".equals(itemBean.getValue())) {// 评审人会审
				reviewersAuditLab.setText(name);
				reviewersAuditLab.setVisible(true);
				reviewersAuditArea.setVisible(true);
				reviewersAuditScrollPane.setVisible(true);
				reviewersAuditArea.setEditable(whetherEdit());
				// TODO
				textJLabelList.add(reviewersAuditLab);
				textFiledList.add(reviewersAuditScrollPane);
				listStateMask.add(3);
				if (itemBean.isNotEmpty()) {// bootean true：必填；false：非必填
					// 评审核人必填项
					reviewRequiredLab.setVisible(true);
					reviewRequiredLab.setForeground(Color.red);
				}
				textRequiredLabList.add(reviewRequiredLab);
				reviewersAuditArea.setText(getApproveStateName(itemBean));
			} else if ("taskAppPzhUser".equals(itemBean.getMark()) && "1".equals(itemBean.getValue())) {// 批准人审核
				approvalAuditLab.setText(name);
				approvalAuditLab.setVisible(true);
				approvalAuditField.setVisible(true);
				approvalAuditField.setEditable(whetherEdit());
				textFiledList.add(approvalAuditField);
				textJLabelList.add(approvalAuditLab);
				listStateMask.add(4);
				if (itemBean.isNotEmpty()) {// bootean true：必填；false：非必填
					// 文控审核人必填项
					approveRequiredLab.setVisible(true);
					approveRequiredLab.setForeground(Color.red);
				}
				textRequiredLabList.add(approveRequiredLab);
				approvalAuditField.setText(getDefaultsPeopleAppId(TaskStateEnum.a4, getApproveStateName(itemBean)));
			} else if ("taskAppOprtUser".equals(itemBean.getMark()) && "1".equals(itemBean.getValue())) {// 各业务体系负责人
				operationAuditLab.setText(name);
				operationAuditLab.setVisible(true);
				operationAuditField.setVisible(true);
				operationAuditField.setEditable(whetherEdit());
				textFiledList.add(operationAuditField);
				textJLabelList.add(operationAuditLab);
				listStateMask.add(6);
				if (itemBean.isNotEmpty()) {// bootean true：必填；false：非必填
					// 文控审核人必填项
					bussReuiredLab.setVisible(true);
					bussReuiredLab.setForeground(Color.red);
				}
				textRequiredLabList.add(bussReuiredLab);
				operationAuditField.setText(getApproveStateName(itemBean));
			} else if ("taskAppITUser".equals(itemBean.getMark()) && "1".equals(itemBean.getValue())) {// IT总监
				ITPeopleAuditLab.setText(name);
				ITPeopleAuditLab.setVisible(true);
				iTPeopleAuditField.setVisible(true);
				iTPeopleAuditField.setEditable(whetherEdit());
				textFiledList.add(iTPeopleAuditField);
				textJLabelList.add(ITPeopleAuditLab);
				listStateMask.add(7);
				if (itemBean.isNotEmpty()) {// bootean true：必填；false：非必填
					// 文控审核人必填项
					fieldITRequiredLab.setVisible(true);
					fieldITRequiredLab.setForeground(Color.red);
				}
				textRequiredLabList.add(fieldITRequiredLab);
				iTPeopleAuditField.setText(getDefaultsPeopleAppId(TaskStateEnum.a6, getApproveStateName(itemBean)));
			} else if ("taskAppDivisionManager".equals(itemBean.getMark()) && "1".equals(itemBean.getValue())) {// 事业部经理
				divisionManagerLab.setText(name);
				divisionManagerLab.setVisible(true);
				divisionManagerField.setVisible(true);
				divisionManagerField.setEditable(whetherEdit());
				textFiledList.add(divisionManagerField);
				textJLabelList.add(divisionManagerLab);
				listStateMask.add(8);
				if (itemBean.isNotEmpty()) {// bootean true：必填；false：非必填
					// 文控审核人必填项
					fieldDivRequiredLab.setVisible(true);
					fieldDivRequiredLab.setForeground(Color.red);
				}
				textRequiredLabList.add(fieldDivRequiredLab);
				divisionManagerField.setText(getDefaultsPeopleAppId(TaskStateEnum.a7, getApproveStateName(itemBean)));
			} else if ("taskAppGeneralManager".equals(itemBean.getMark()) && "1".equals(itemBean.getValue())) {// 总经理
				generalManagerLab.setText(name);
				generalManagerLab.setVisible(true);
				generalManagerField.setVisible(true);
				generalManagerField.setEditable(whetherEdit());
				textFiledList.add(generalManagerField);
				textJLabelList.add(generalManagerLab);
				listStateMask.add(9);
				if (itemBean.isNotEmpty()) {// bootean true：必填；false：非必填
					// 文控审核人必填项
					fieldGenRequiredLab.setVisible(true);
					fieldGenRequiredLab.setForeground(Color.red);
				}
				textRequiredLabList.add(fieldGenRequiredLab);
				generalManagerField.setText(getApproveStateName(itemBean));
			} else if ("taskCustomApproval1".equals(itemBean.getMark()) && "1".equals(itemBean.getValue())) {// 自定义1
				taskCustomApproval1Lab.setText(name);
				taskCustomApproval1Lab.setVisible(true);
				taskCustomApproval1Area.setVisible(true);
				taskCustomApproval1Area.setEditable(whetherEdit());
				taskCustomApproval1ScrollPane.setVisible(true);
				textJLabelList.add(taskCustomApproval1Lab);
				textFiledList.add(taskCustomApproval1ScrollPane);
				listStateMask.add(11);
				if (itemBean.isNotEmpty()) {// bootean true：必填；false：非必填
					// 评审核人必填项
					fieldTaskCustomApproval1Lab.setVisible(true);
					fieldTaskCustomApproval1Lab.setForeground(Color.red);
				}
				textRequiredLabList.add(fieldTaskCustomApproval1Lab);
				taskCustomApproval1Area.setText(getApproveStateName(itemBean));
			} else if ("taskCustomApproval2".equals(itemBean.getMark()) && "1".equals(itemBean.getValue())) {// 自定义2
				taskCustomApproval2Lab.setText(name);
				taskCustomApproval2Lab.setVisible(true);
				taskCustomApproval2Area.setVisible(true);
				taskCustomApproval2Area.setEditable(whetherEdit());
				taskCustomApproval2ScrollPane.setVisible(true);
				textJLabelList.add(taskCustomApproval2Lab);
				textFiledList.add(taskCustomApproval2ScrollPane);
				listStateMask.add(12);
				if (itemBean.isNotEmpty()) {// bootean true：必填；false：非必填
					// 评审核人必填项
					fieldTaskCustomApproval2Lab.setVisible(true);
					fieldTaskCustomApproval2Lab.setForeground(Color.red);
				}
				textRequiredLabList.add(fieldTaskCustomApproval2Lab);
				taskCustomApproval2Area.setText(getApproveStateName(itemBean));
			} else if ("taskCustomApproval3".equals(itemBean.getMark()) && "1".equals(itemBean.getValue())) {// 自定义3
				taskCustomApproval3Lab.setText(name);
				taskCustomApproval3Lab.setVisible(true);
				taskCustomApproval3Area.setVisible(true);
				taskCustomApproval3Area.setEditable(whetherEdit());
				taskCustomApproval3ScrollPane.setVisible(true);
				textJLabelList.add(taskCustomApproval3Lab);
				textFiledList.add(taskCustomApproval3ScrollPane);
				listStateMask.add(13);
				if (itemBean.isNotEmpty()) {// bootean true：必填；false：非必填
					// 评审核人必填项
					fieldTaskCustomApproval3Lab.setVisible(true);
					fieldTaskCustomApproval3Lab.setForeground(Color.red);
				}
				textRequiredLabList.add(fieldTaskCustomApproval3Lab);
				taskCustomApproval3Area.setText(getApproveStateName(itemBean));
			} else if ("taskCustomApproval4".equals(itemBean.getMark()) && "1".equals(itemBean.getValue())) {// 自定义4
				taskCustomApproval4Lab.setText(name);
				taskCustomApproval4Lab.setVisible(true);
				taskCustomApproval4Area.setVisible(true);
				taskCustomApproval4Area.setEditable(whetherEdit());
				taskCustomApproval4ScrollPane.setVisible(true);
				textJLabelList.add(taskCustomApproval4Lab);
				textFiledList.add(taskCustomApproval4ScrollPane);
				listStateMask.add(14);
				if (itemBean.isNotEmpty()) {// bootean true：必填；false：非必填
					// 评审核人必填项
					fieldTaskCustomApproval4Lab.setVisible(true);
					fieldTaskCustomApproval4Lab.setForeground(Color.red);
				}
				textRequiredLabList.add(fieldTaskCustomApproval4Lab);
				taskCustomApproval4Area.setText(getApproveStateName(itemBean));
			}
		}
		initLayout();
		if (JecnConfigTool.autoVersionNumber() && JecnConfigTool.isShowItem(ConfigItemPartMapMark.enableSmallVersion)) {
			// 大版本 复选框
			JecnVersionUtil.getLargeVersionCheckBox().addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent e) {
					// TODO Auto-generated method stub
					JecnVersionUtil.initVersion(versonNumField, treeBean, type);
				}
			});
			// 小版本复选框
			JecnVersionUtil.getSmallVersionCheckBox().addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent e) {
					// TODO Auto-generated method stub
					JecnVersionUtil.initVersion(versonNumField, treeBean, type);
				}
			});
		}
	}

	private String getApproveStateName(TaskConfigItem itemBean) {
		String split = " ";
		String name = "";
		for (JecnUser user : itemBean.getUsers()) {
			name += split + user.getTrueName();
		}
		name = name.trim();
		return name;
	}

	// 是否隐藏 编辑框
	private boolean whetherEdit() {
		return !JecnConfigTool.isJiuXinLoginType()
				|| this.treeNode.getJecnTreeBean().getTreeNodeType() == TreeNodeType.processMap;
	}

	private List<TaskConfigItem> listApproveStage() {
		List<TaskConfigItem> configs = new ArrayList<TaskConfigItem>();
		try {
			List<TaskConfigItem> fullConfigs = JecnTreeCommon.getTaskConfigItems(treeNode, 0);
			for (TaskConfigItem config : fullConfigs) {
				if (config.getMark().equals(ConfigItemPartMapMark.taskAppPubType.toString())) {
					configs.add(config);
				} else {
					if ("1".equals(config.getValue())) {
						configs.add(config);
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			log.error(e);
		}
		return configs;
	}

	/***************************************************************************
	 * 布局
	 * 
	 * @param args
	 */
	protected void initLayout() {
		mainPanel.setLayout(new GridBagLayout());
		GridBagConstraints c = null;
		Insets insets = new Insets(5, 5, 5, 5);
		// *********** 头部面板显示信息
		c = new GridBagConstraints(0, 0, 1, 1, 1.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL,
				insets, 0, 0);
		mainPanel.add(topPanel, c);
		topPanel.setLayout(new GridBagLayout());
		// 流程名称 flowNameLab
		c = new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, insets, 0, 0);
		topPanel.add(new JLabel(prfName), c);
		c = new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, insets, 0, 0);
		topPanel.add(flowNameLab, c);
		// 分割线
		c = new GridBagConstraints(0, 1, 2, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				insets, 0, 0);
		topPanel.add(namebutomField, c);
		// *********** 中心控件显示面板
		c = new GridBagConstraints(0, 1, 1, 1, 1.0, 1.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, insets, 0,
				0);

		JScrollPane jScrollPane = new JScrollPane();
		jScrollPane.setViewportView(infoPanel);
		jScrollPane.getViewport().setBackground(JecnUIUtil.getDefaultBackgroundColor());
		mainPanel.add(jScrollPane, c);

		infoPanel.setBorder(BorderFactory.createTitledBorder(JecnProperties.getValue("checFileControl")));
		infoPanel.setLayout(new GridBagLayout());
		// 版本号
		c = new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.NONE, insets,
				0, 0);
		infoPanel.add(versonNumLab, c);

		c = new GridBagConstraints(1, 0, 1, 1, 1.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.HORIZONTAL,
				insets, 0, 0);
		infoPanel.add(JecnVersionUtil.getVersionTextPanel(versonNumField), c);

		c = new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.NONE, insets,
				0, 0);
		infoPanel.add(menuPanel, c);
		// 必填*显示
		c = new GridBagConstraints(3, 0, 1, 1, 0.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.NONE, insets,
				0, 0);
		infoPanel.add(versionRequiredLab, c);
		// 拟稿人
		c = new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.NONE, insets,
				0, 0);
		infoPanel.add(draftPeopleLab, c);
		c = new GridBagConstraints(1, 1, 2, 1, 1.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.HORIZONTAL,
				insets, 0, 0);
		infoPanel.add(draftPeopleField, c);
		// 显示必填
		c = new GridBagConstraints(3, 1, 1, 1, 0.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.NONE, insets,
				0, 0);
		infoPanel.add(draftPeopleRequiredLab, c);

		// 审批人布局
		initListLayout();
		// 变更说明
		c = new GridBagConstraints(0, 15, 1, 1, 0.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.NONE,
				insets, 0, 0);
		infoPanel.add(descofChangeLab, c);
		c = new GridBagConstraints(1, 15, 2, 1, 1.0, 1.0, GridBagConstraints.NORTHWEST, GridBagConstraints.BOTH,
				insets, 0, 0);
		infoPanel.add(descofChangeScrollPane, c);
		// 变更说明必填
		c = new GridBagConstraints(3, 15, 1, 1, 0.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.NONE,
				insets, 0, 0);
		infoPanel.add(areaChangeDescriptionLab, c);

		if (!JecnConfigTool.isHiddenImplDate()) {
			c = new GridBagConstraints(0, 16, 1, 1, 0.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.NONE,
					insets, 0, 0);
			infoPanel.add(implementDateLab, c);
			c = new GridBagConstraints(1, 16, 1, 1, 0.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.NONE,
					insets, 0, 0);
			infoPanel.add(implementDatePanel, c);
		}

		// ************ 按钮面板
		c = new GridBagConstraints(0, 2, 1, 1, 1.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL,
				insets, 0, 0);
		mainPanel.add(butPanel, c);
		butPanel.setLayout(new FlowLayout(FlowLayout.RIGHT));
		butPanel.add(verfyLab);
		butPanel.add(okBut);
		butPanel.add(cancelBut);

		this.getContentPane().add(mainPanel);
	}

	/**
	 * 审批人配置
	 * 
	 */
	private void initListLayout() {
		GridBagConstraints c;
		Insets insets = new Insets(5, 5, 5, 5);

		if (textFiledList == null) {
			return;
		}

		int count = 2;
		for (int i = 0; i < textFiledList.size(); i++) {
			JComponent jComponent = textFiledList.get(i);
			JLabel label = textJLabelList.get(i);
			JLabel requestLabel = textRequiredLabList.get(i);

			c = new GridBagConstraints(0, count, 1, 1, 0.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.NONE,
					insets, 0, 0);
			infoPanel.add(label, c);
			if (jComponent instanceof JScrollPane) {
				c = new GridBagConstraints(1, count, 2, 1, 1.0, 1.0, GridBagConstraints.NORTHWEST,
						GridBagConstraints.BOTH, insets, 0, 0);
			} else {
				c = new GridBagConstraints(1, count, 2, 1, 1.0, 0.0, GridBagConstraints.NORTHWEST,
						GridBagConstraints.HORIZONTAL, insets, 0, 0);
			}
			infoPanel.add(jComponent, c);

			// 必填项
			c = new GridBagConstraints(3, count, 1, 1, 0.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.NONE,
					insets, 0, 0);
			infoPanel.add(requestLabel, c);

			count++;
		}
	}

	/**
	 * 取消
	 */
	protected void cancelButPerformed() {
		isOkBut = false;
		this.dispose();
	}

	/***************************************************************************
	 * 确定
	 * 
	 * @param args
	 */
	protected void okButPerformed() {
		boolean isCheck = okButCheck();
		if (!isCheck) {
			return;
		}
		// 获取文控信息对象
		JecnTaskHistoryNew historyNew = getJecnTaskHistoryNew();
		// 0是流程图、1是文件,2是制度目录,3制度文件，4流程地图
		historyNew.setType(type);
		// 关联ID
		historyNew.setRelateId(treeBean.getId());
		// 流程编号
		historyNew.setNumberId(treeBean.getNumberId());
		// 拟稿人
		historyNew.setDraftPerson(draftPeopleField.getText());
		// 文件名称
		historyNew.setRelateName(treeBean.getName());
		// 实施日期
		historyNew.setImplementDate(JecnTool.getDateByString(implementDatePanel.getText()));
		try {
			ConnectionPool.getDocControlAction().saveJecnTaskHistoryNew(historyNew);
		} catch (Exception e) {
			log.error("BasePublishDialog okButPerformed is error", e);
			return;
		}
		isOkBut = true;
		this.dispose();
	}

	/**
	 * 提交信息验证
	 * 
	 * @return true 验证通过；false 验证失败
	 */
	protected boolean okButCheck() {
		verfyLab.setText("");

		if (JecnConfigTool.autoVersionNumber() && JecnConfigTool.isShowItem(ConfigItemPartMapMark.enableSmallVersion)) {
			if ((JecnVersionUtil.getLargeVersionCheckBox() != null && !JecnVersionUtil.getLargeVersionCheckBox()
					.isSelected())
					&& (JecnVersionUtil.getSmallVersionCheckBox() != null && !JecnVersionUtil.getSmallVersionCheckBox()
							.isSelected())) {
				verfyLab.setText(JecnProperties.getValue("pleaseVersionNumber"));
				return false;
			}
		}
		// 验证版本号不能重复
		if (versonNumField.getText() != null && !"".equals(versonNumField.getText())) {
			for (String str : versListAll) {
				if (str.equals(versonNumField.getText().trim())) {
					verfyLab.setText(JecnProperties.getValue("versionNumOnly"));
					return false;
				}
			}
		}
		// 版本号验证 122字符
		String strName = TaskCommon.checTextName(versonNumField, versonNumLab);
		if (!DrawCommon.isNullOrEmtryTrim(strName)) {
			verfyLab.setText(strName);
			return false;
		}

		// 拟稿人验证 122字符
		strName = checkField(draftPeopleField, draftPeopleLab);
		if (!DrawCommon.isNullOrEmtryTrim(strName)) {
			verfyLab.setText(strName);
			return false;
		}

		// -----------------------

		for (TaskConfigItem itemBean : itemBeanList) {

			// itemBean.getIsShow() 1：显示
			if ("taskAppDocCtrlUser".equals(itemBean.getMark()) && "1".equals(itemBean.getValue())) {// 文控审核人
				if (documentControlAuditField.isVisible()) {
					if (applicationRequiredLab.isVisible()) {// 必填项
						strName = checkField(documentControlAuditField, documentControlAuditLab);
					} else {// 非必填项
						strName = checkFieldContainsNull(documentControlAuditField, documentControlAuditLab);
					}
					// 文控审核人验证 122字符
					if (!DrawCommon.isNullOrEmtryTrim(strName)) {
						verfyLab.setText(strName);
						return false;
					}
				}
			} else if ("taskAppDeptUser".equals(itemBean.getMark()) && "1".equals(itemBean.getValue())) {// 部门审核人

				if (deptAuditField.isVisible()) {
					if (deptPeopleRequiredLab.isVisible()) {// 必填项
						// 部门验证 122字符
						strName = checkField(deptAuditField, deptAuditLab);
					} else {// 非必填项
						// 部门验证 122字符
						strName = checkFieldContainsNull(deptAuditField, deptAuditLab);
					}
					// 文控审核人验证 122字符
					if (!DrawCommon.isNullOrEmtryTrim(strName)) {
						verfyLab.setText(strName);
						return false;
					}
				}
			} else if ("taskAppReviewer".equals(itemBean.getMark()) && "1".equals(itemBean.getValue())) {// 评审人会审
				// 会审人员必填项验证
				if (reviewersAuditArea.isVisible()) {
					if (reviewRequiredLab.isVisible()) {// 必填项
						// 会审人员验证 122字符
						strName = checkReviewPeople(reviewersAuditArea, reviewersAuditLab);
					} else {// 非必填项
						// 会审人员验证 122字符
						strName = checkReviewPeopleContainsNull(reviewersAuditArea, reviewersAuditLab);
					}
					// 会审人员人验证 122字符
					if (!DrawCommon.isNullOrEmtryTrim(strName)) {
						verfyLab.setText(strName);
						return false;
					}
				}
			} else if ("taskAppPzhUser".equals(itemBean.getMark()) && "1".equals(itemBean.getValue())) {// 批准人审核
				if (approvalAuditField.isVisible()) {// 非必填项
					if (approveRequiredLab.isVisible()) {// 必填项
						// 批准人验证 122字符
						strName = checkField(approvalAuditField, approvalAuditLab);
					} else {// 非必填项
						// 批准人验证 122字符
						strName = checkFieldContainsNull(approvalAuditField, approvalAuditLab);
					}
					if (!DrawCommon.isNullOrEmtryTrim(strName)) {
						verfyLab.setText(strName);
						return false;
					}
				}
			} else if ("taskAppOprtUser".equals(itemBean.getMark()) && "1".equals(itemBean.getValue())) {// 各业务体系负责人
				if (operationAuditField.isVisible()) {
					if (bussReuiredLab.isVisible()) {// 必填项
						// 各业务体系负责人验证 122字符
						strName = checkField(operationAuditField, operationAuditLab);
					} else {// 非必填项
						// 各业务体系负责人验证 122字符
						strName = checkFieldContainsNull(operationAuditField, operationAuditLab);
					}
					if (!DrawCommon.isNullOrEmtryTrim(strName)) {
						verfyLab.setText(strName);
						return false;
					}
				}
			} else if ("taskAppITUser".equals(itemBean.getMark()) && "1".equals(itemBean.getValue())) {// IT总监
				if (iTPeopleAuditField.isVisible()) {
					if (fieldITRequiredLab.isVisible()) {// 必填项
						// IT总监验证 122字符
						strName = checkField(iTPeopleAuditField, ITPeopleAuditLab);
					} else {// 非必填项
						// 各业务体系负责人验证 122字符
						strName = checkFieldContainsNull(iTPeopleAuditField, ITPeopleAuditLab);
					}
					if (!DrawCommon.isNullOrEmtryTrim(strName)) {
						verfyLab.setText(strName);
						return false;
					}
				}
			} else if ("taskAppDivisionManager".equals(itemBean.getMark()) && "1".equals(itemBean.getValue())) {// 事业部经理
				if (divisionManagerField.isVisible()) {
					if (fieldDivRequiredLab.isVisible()) {// 必填项
						// 事业部经理验证 122字符
						strName = checkField(divisionManagerField, divisionManagerLab);
					} else {// 非必填项
						// 事业部经理验证 122字符
						strName = checkFieldContainsNull(divisionManagerField, divisionManagerLab);
					}
					if (!DrawCommon.isNullOrEmtryTrim(strName)) {
						verfyLab.setText(strName);
						return false;
					}
				}
			} else if ("taskAppGeneralManager".equals(itemBean.getMark()) && "1".equals(itemBean.getValue())) {// 总经理
				if (generalManagerField.isVisible()) {
					if (fieldGenRequiredLab.isVisible()) {// 必填项
						// 总经理122字符
						strName = checkField(generalManagerField, generalManagerLab);
					} else {// 非必填项
						// 总经理122字符
						strName = checkFieldContainsNull(generalManagerField, generalManagerLab);
					}
					if (!DrawCommon.isNullOrEmtryTrim(strName)) {
						verfyLab.setText(strName);
						return false;
					}
				}
			} else if ("taskCustomApproval1".equals(itemBean.getMark()) && "1".equals(itemBean.getValue())) {// 自定义1
				// 人员必填项验证
				if (taskCustomApproval1Area.isVisible()) {
					if (fieldTaskCustomApproval1Lab.isVisible()) {// 必填项
						// 122字符
						strName = checkReviewPeople(taskCustomApproval1Area, taskCustomApproval1Lab);
					} else {// 非必填项
						// 验证 122字符
						strName = checkReviewPeopleContainsNull(taskCustomApproval1Area, taskCustomApproval1Lab);
					}
					// 会审人员人验证 122字符
					if (!DrawCommon.isNullOrEmtryTrim(strName)) {
						verfyLab.setText(strName);
						return false;
					}
				}
			} else if ("taskCustomApproval2".equals(itemBean.getMark()) && "1".equals(itemBean.getValue())) {// 自定义2
				// 人员必填项验证
				if (taskCustomApproval2Area.isVisible()) {
					if (fieldTaskCustomApproval2Lab.isVisible()) {// 必填项
						strName = checkReviewPeople(taskCustomApproval2Area, taskCustomApproval2Lab);
					} else {// 非必填项
						strName = checkReviewPeopleContainsNull(taskCustomApproval2Area, taskCustomApproval2Lab);
					}
					if (!DrawCommon.isNullOrEmtryTrim(strName)) {
						verfyLab.setText(strName);
						return false;
					}
				}
			} else if ("taskCustomApproval3".equals(itemBean.getMark()) && "1".equals(itemBean.getValue())) {// 自定义3
				// 人员必填项验证
				if (taskCustomApproval3Area.isVisible()) {
					if (fieldTaskCustomApproval3Lab.isVisible()) {// 必填项
						strName = checkReviewPeople(taskCustomApproval3Area, taskCustomApproval3Lab);
					} else {// 非必填项
						strName = checkReviewPeopleContainsNull(taskCustomApproval3Area, taskCustomApproval3Lab);
					}
					if (!DrawCommon.isNullOrEmtryTrim(strName)) {
						verfyLab.setText(strName);
						return false;
					}
				}
			} else if ("taskCustomApproval4".equals(itemBean.getMark()) && "1".equals(itemBean.getValue())) {// 自定义4
				// 人员必填项验证
				if (taskCustomApproval4Area.isVisible()) {
					if (fieldTaskCustomApproval4Lab.isVisible()) {// 必填项
						strName = checkReviewPeople(taskCustomApproval4Area, taskCustomApproval4Lab);
					} else {// 非必填项
						strName = checkReviewPeopleContainsNull(taskCustomApproval4Area, taskCustomApproval4Lab);
					}
					if (!DrawCommon.isNullOrEmtryTrim(strName)) {
						verfyLab.setText(strName);
						return false;
					}
				}
			}
		}

		// -----------------------

		if (descofChangeField.isVisible()) {
			// 变更说明 1200字符
			strName = TaskCommon.checkAreaName(descofChangeField, descofChangeLab);
			if (!DrawCommon.isNullOrEmtryTrim(strName)) {
				verfyLab.setText(strName);
				return false;
			}
		}
		return true;
	}

	/**
	 * 检测文本是否符合规定要求，不为空、不包含特殊字符、不超出规定范围
	 * 
	 * @param textField
	 *            文本
	 * @param jLabel
	 *            标签
	 * @return true 验证失败，输入文本不符合要求
	 */
	public static String checkReviewPeople(JTextArea textArea, JLabel jLabel) {
		String str = checkAreaName(textArea.getText());
		if (!DrawCommon.isNullOrEmtryTrim(str)) {
			str = jLabel.getText() + str;
		}
		return str;
	}

	/**
	 * 如果内容不为空的话校验
	 * 
	 * @param textField
	 *            文本
	 * @param jLabel
	 *            标签
	 * @return true 验证失败，输入文本不符合要求
	 */
	public static String checkReviewPeopleContainsNull(JTextArea textArea, JLabel jLabel) {
		if (DrawCommon.isNullOrEmtryTrim(textArea.getText().trim())) {
			return "";
		}

		return checkReviewPeople(textArea, jLabel);
	}

	/**
	 * 
	 * 校验名称是否满足要求,验证（名称不为空，字符正确，长度）成功返回“”，否则返回相应提示信息
	 * 
	 * @param name
	 *            String 名称
	 * @return String 验证（名称不为空，字符正确，长度）成功返回""，否则返回相应提示信息
	 */
	public static String checkAreaName(String name) {
		if (DrawCommon.isNullOrEmtryTrim(name)) {
			// 名称不能为空
			return JecnUserCheckInfoData.getNameNotNull();
		} else if (!JecnUserCheckUtil.checkNameFileSpecialChar(name)) {
			// 名称不能包括任意字符之一: \ / : * ? < > | # %
			return JecnUserCheckInfoData.getNameErrorCharInfo();
		} else if (JecnUserCheckUtil.getTextLength(name) > 122) {

			return JecnUserCheckInfoData.getNameLengthInfo();
		}
		return "";
	}

	/**
	 * 
	 * 获取文控信息对象
	 * 
	 * @return JecnTaskHistoryNew
	 */
	protected JecnTaskHistoryNew getJecnTaskHistoryNew() {
		JecnTaskHistoryNew historyNew = new JecnTaskHistoryNew();
		// 版本号
		historyNew.setVersionId(versonNumField.getText().trim());

		List<JecnTaskHistoryFollow> list = new ArrayList<JecnTaskHistoryFollow>();
		// 默认添加拟稿人
		JecnTaskHistoryFollow follow = new JecnTaskHistoryFollow();
		follow.setName(draftPeopleField.getText());
		// 获得各个阶段标示
		follow.setStageMark(0);
		follow.setAppellation("拟稿人");
		follow.setEn_Appellation("Originator");
		follow.setSort(0);
		list.add(follow);

		for (int i = 0; i < textFiledList.size(); i++) {
			JComponent jComponent = textFiledList.get(i);
			if (jComponent == null) {
				continue;
			}
			JLabel label = textJLabelList.get(i);
			JecnTaskHistoryFollow historyFollow = taskHistoryFollowList.get(i);

			String stageName = "";

			if (jComponent instanceof JTextField) {
				stageName = ((JTextField) jComponent).getText();
			} else {
				// TODO
				stageName = ((JTextArea) ((JScrollPane) jComponent).getViewport().getView()).getText();
			}
			historyFollow.setName(stageName);
			// 获得各个阶段标示
			historyFollow.setStageMark(this.listStateMask.get(i));
			list.add(historyFollow);
		}
		historyNew.setListJecnTaskHistoryFollow(list);
		// 变更说明
		historyNew.setModifyExplain(descofChangeField.getText());
		// 驳回次数
		historyNew.setApproveCount(0L);
		// 设置为升级版本
		historyNew.setTestRunNumber(1);
		return historyNew;
	}

	public boolean isOkBut() {
		return isOkBut;
	}

	/**
	 * 非空验证
	 * 
	 * @param textField
	 * @param jLabel
	 * @return
	 */
	private String checkField(JTextField textField, JLabel jLabel) {

		String result = TaskCommon.checTextName(textField, jLabel);
		// 有错误信息
		if (!DrawCommon.isNullOrEmtryTrim(result)) {
			return result;
		} else if (!JecnUserCheckUtil.checkNameFileSpecialChar(textField.getText().trim())) {
			// 名称不能包括任意字符之一: \ / : * ? < > | # %
			result = jLabel.getText() + JecnUserCheckInfoData.getNameErrorCharInfo();
			return result;
		}
		return "";

	}

	/**
	 * 如果内容非空的话验证
	 * 
	 * @param textField
	 * @param jLabel
	 * @return
	 */
	private String checkFieldContainsNull(JTextField textField, JLabel jLabel) {

		if (DrawCommon.isNullOrEmtryTrim(textField.getText().trim())) {
			return "";
		}

		return checkField(textField, jLabel);

	}

}
