package epros.designer.gui;

import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.JFrame;

import epros.designer.gui.common.JecnConstants;
import epros.designer.gui.header.JecnDesignerModuleShow;
import epros.designer.gui.popedom.person.JecnPersonInfoPanel;
import epros.designer.gui.workflow.JecnDesignerMainPanel;
import epros.designer.util.JecnProperties;
import epros.draw.gui.designer.JecnDesigner;
import epros.draw.gui.designer.JecnDesignerProcess;
import epros.draw.gui.top.frame.JecnFrame;
import epros.draw.gui.top.frame.listener.JecnFrameEvent;
import epros.draw.gui.top.frame.listener.JecnFrameListener;
import epros.draw.gui.workflow.JecnDrawMainPanel;
import epros.draw.system.data.JecnSystemStaticData;
import epros.draw.util.JecnUIUtil;

/**
 * 
 * 窗体 软件启动入口
 * 
 * @author ZHOUXY
 * 
 */
public class JecnDesignerFrame {
	// 初始化树面板和向导面板对应的窗体右上角复选框
	private static JecnDesignerModuleShow designModuleShow;

	public JecnDesignerFrame() {
		initCompotents();
	}

	private void initCompotents() {
		JecnFrame frame = new JecnFrame();
		// 主要解决弹出对话框（Jecndialog）模态不好使问题
		JecnSystemStaticData.setFrame(frame);

		// 期望大小
		frame.setPreferredSize(JecnUIUtil.getEprosDrawSize());
		// 最小大小
		frame.setMinimumSize(JecnUIUtil.getEprosDrawSize());

		if (JecnConstants.versionType == 99 || JecnConstants.versionType == 100) {
			frame.setTitle(JecnProperties.getValue("frameTitle"));
		} else {
			frame.setTitle(JecnProperties.getValue("frameTitleStandard"));
		}

		// 居中
		frame.setLocationRelativeTo(null);
		// 关闭退出
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		// 默认最大化
		frame.setMaxFrameBounds();
		// 隐藏窗体标题头和边框
		frame.setUndecorated(true);

		JecnDesignerMainPanel desMainPanel = JecnDesignerMainPanel.getDesignerMainPanel();

		// 画图主界面
		JecnDrawMainPanel mainPanel = JecnDrawMainPanel.getMainPanel();

		// 添加工具栏标题按钮面板
		frame.addToolTitlePanel(mainPanel.getToolbarTitle());
		// 窗体空闲区域添加显示模板组合框
		frame.addRightBottomPanel(mainPanel.getModuleShowPanel());

		// 初始化树面板和向导面板对应的窗体右上角复选框
		designModuleShow = new JecnDesignerModuleShow(desMainPanel.getMainSplitPane(), desMainPanel.getSplitPane(),
				mainPanel.getModuleShowPanel(), mainPanel.getLeftSplitPane());

		// 添加个人信息面板
		frame.addCompToFreePanel(new JecnPersonInfoPanel());

		// 设计器特殊点：画图工具打开转移到常用面板中做导入；另存为改成导出
		JecnInitDraw.initToolBar(mainPanel.getToolbar());

		// 任务栏右键关闭
		frame.addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent e) {
				JecnDesigner.removeAllPanelAndLoginOut(false);
			}
		});
		frame.addJecnFrameListener(new JecnFrameListener() {
			public boolean FrameCloseBefore(JecnFrameEvent e) {
				// 是否保存验证
				return JecnDesigner.removeAllPanelAndLoginOut(true);
			}
		}, this);
		frame.getContentPane().add(mainPanel);
		frame.setVisible(true);
	}

	/**
	 * 
	 * 设计器显示/隐藏树面板/流程向导处理类
	 * 
	 * @return
	 */
	public static JecnDesignerModuleShow getDesignModuleShow() {
		if (designModuleShow == null) {
			return null;
		}
		return designModuleShow;
	}

	public static void startDesigner() {
		// 启动组件
		new JecnDesignerFrame();
		// 读取模型数据
		JecnDesignerProcess.getDesignerProcess().getJecnProcessTemplateBeanList();
	}
}
