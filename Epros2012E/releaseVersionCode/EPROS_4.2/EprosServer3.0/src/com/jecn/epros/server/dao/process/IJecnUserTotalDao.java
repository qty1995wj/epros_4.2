package com.jecn.epros.server.dao.process;

import com.jecn.epros.server.bean.system.JecnUserTotal;
import com.jecn.epros.server.common.IBaseDao;
/**
 * @author yxw 2013-3-6
 * @description：
 */
public interface IJecnUserTotalDao extends IBaseDao<JecnUserTotal, Long> {
	
}
