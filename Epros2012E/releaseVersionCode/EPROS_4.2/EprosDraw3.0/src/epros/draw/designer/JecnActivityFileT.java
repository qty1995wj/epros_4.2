package epros.draw.designer;


public class JecnActivityFileT implements java.io.Serializable{
	private Long fileId;//主键ID
	private String fileName;//文件名称（不存放在数据库）
	private Long fileType;//文件类型（0是输入，1是操作规范）
	private Long figureId;//活动FigureId
	private Long fileSId;//文件ID
	private String fileNumber; // 文件编号
	private String figureUUID;
	private String UUID;

	public String getFileNumber() {
		return fileNumber;
	}
	public void setFileNumber(String fileNumber) {
		this.fileNumber = fileNumber;
	}
	public Long getFileId() {
		return fileId;
	}
	public void setFileId(Long fileId) {
		this.fileId = fileId;
	}
	public String getFileName() {
		return fileName;
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	public Long getFileType() {
		return fileType;
	}
	public void setFileType(Long fileType) {
		this.fileType = fileType;
	}
	public Long getFigureId() {
		return figureId;
	}
	public void setFigureId(Long figureId) {
		this.figureId = figureId;
	}
	public Long getFileSId() {
		return fileSId;
	}
	public void setFileSId(Long fileSId) {
		this.fileSId = fileSId;
	}
	
	public String getFigureUUID() {
		return figureUUID;
	}

	public void setFigureUUID(String figureUUID) {
		this.figureUUID = figureUUID;
	}

	public String getUUID() {
		return UUID;
	}

	public void setUUID(String uUID) {
		UUID = uUID;
	}

	public JecnActivityFileT clone(){
		JecnActivityFileT activityFileT = new JecnActivityFileT();
		if (this.getFileId() != null) {
			activityFileT.setFileId(this.getFileId().longValue());// 主键ID
		}
		activityFileT.setFileName(this.getFileName());// 文件名称（不存放在数据库）
		activityFileT.setFileType(this.getFileType());// 文件类型（0是输入，1是操作规范）
		activityFileT.setFigureId(this.getFigureId());// 活动FigureId
		activityFileT.setFileSId(this.getFileSId());// 文件名称
		activityFileT.setFigureUUID(figureUUID);
		return activityFileT;
	}
}

