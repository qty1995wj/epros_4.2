package com.jecn.epros.server.dao.dataImport.xml;

import java.util.List;

import com.jecn.epros.server.bean.popedom.JecnFlowOrg;
import com.jecn.epros.server.bean.popedom.JecnFlowOrgImage;
import com.jecn.epros.server.bean.popedom.JecnUser;
import com.jecn.epros.server.bean.popedom.JecnUserInfo;
import com.jecn.epros.server.common.IBaseDao;
import com.jecn.epros.server.webBean.dataImport.sync.xmlImport.DepartmentBean;
import com.jecn.epros.server.webBean.dataImport.sync.xmlImport.PersonBean;
import com.jecn.epros.server.webBean.dataImport.sync.xmlImport.PositionBean;
import com.jecn.epros.server.webBean.dataImport.sync.xmlImport.StationAndPeopleRelate;
import com.jecn.epros.server.webBean.dataImport.sync.xmlImport.XmlStructureBean;

public interface IXmpImportDAO extends IBaseDao<PersonBean, Long> {
	public List<DepartmentBean> getAllDeptment() throws Exception;

	public void clear() throws Exception;

	public List<XmlStructureBean> getAllXmlStructure() throws Exception;

	public List<DepartmentBean> getAllOrg() throws Exception;

	public List<PositionBean> getAllPosition() throws Exception;

	public List<PersonBean> getAllPersonBean() throws Exception;

	public void synchronizationPeoples(List<JecnUser> listUpdate,
			List<Long> listdelete, List<JecnUser> listNew) throws Exception;

	public List<Object[]> getUnMatchPosition();

	void saveXmlStructureBean(XmlStructureBean xsb) throws Exception;

	void updateXmlStructureBean(XmlStructureBean xsb) throws Exception;

	boolean saveDepartmentBean(List<DepartmentBean> newDeptList)
			throws Exception;

	void saveNoDataOrgs(List<JecnFlowOrg> listNewJecnFlowOrg, Long projectId)
			throws Exception;

	void updateOrg(List<JecnFlowOrg> listOrgAlls,
			List<JecnFlowOrg> listNewJecnFlowOrg, Long projectId)
			throws Exception;

	void updateUserFH(List<JecnFlowOrg> listOrgAlls,
			List<JecnFlowOrgImage> getStationsList,
			List<JecnUser> listJecnUser, List<JecnUserInfo> listJecnUserInfo,
			List<JecnFlowOrgImage> listNewJecnFlowOrgImage,
			List<JecnUser> listNewJecnUser,
			List<StationAndPeopleRelate> listStationAndPeopleRelate,
			Long projectId) throws Exception;
}
