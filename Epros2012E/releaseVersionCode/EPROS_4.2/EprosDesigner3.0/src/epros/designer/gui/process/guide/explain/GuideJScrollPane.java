package epros.designer.gui.process.guide.explain;

import java.awt.Dimension;

import javax.swing.JScrollPane;

public class GuideJScrollPane extends JScrollPane{
	public GuideJScrollPane() {
		/** 设置大小 */
		Dimension dmion = new Dimension(540, 100);
		this.setPreferredSize(dmion);
		this.setMaximumSize(dmion);
		this.setMinimumSize(dmion);
	}
}
