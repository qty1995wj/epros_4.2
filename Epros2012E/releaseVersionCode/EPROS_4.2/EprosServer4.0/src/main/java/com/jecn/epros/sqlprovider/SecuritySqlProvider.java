package com.jecn.epros.sqlprovider;

import com.jecn.epros.security.AuthenticatedUserUtil;
import com.jecn.epros.sqlprovider.server3.AuthSqlConstant;
import com.jecn.epros.sqlprovider.server3.AuthSqlConstant.TYPEAUTH;
import com.jecn.epros.sqlprovider.server3.CommonSqlTPath;
import com.jecn.epros.sqlprovider.server3.SqlCommon;
import org.apache.commons.collections.map.HashedMap;

import java.util.Map;
import java.util.Set;

public class SecuritySqlProvider extends BaseSqlProvider {

    /**
     * 部门所有父部门集合（包括自己）
     *
     * @param map
     * @return
     */
    public String findOrgIds(Map<String, Object> map) {
        return "select distinct pub.org_id from jecn_flow_org jfo"
                + "      inner join jecn_flow_org pub on jfo.t_path like " + SqlCommon.getJoinFunc("pub")
                + "      where jfo.org_id in" + "      (SELECT ORG.ORG_ID FROM JECN_FLOW_ORG ORG"
                + "         INNER JOIN JECN_FLOW_ORG_IMAGE POS ON POS.ORG_ID=ORG.ORG_ID"
                + "         INNER JOIN JECN_USER_POSITION_RELATED JUPR ON POS.FIGURE_ID=JUPR.FIGURE_ID AND JUPR.PEOPLE_ID="
                + (Long) map.get("peopleId") + "         WHERE ORG.DEL_STATE=0 AND ORG.PROJECTID="
                + (Long) map.get("projectId") + ")";
    }

    /**
     * 获得用户默认权限的关键字
     *
     * @param map
     * @return
     */
    public String findDefaultRoleFilter(Map<String, Object> map) {
        return "SELECT  JRI.FILTER_ID" + "  FROM JECN_USER_ROLE JUR, JECN_ROLE_INFO JRI" + "  WHERE JUR.PEOPLE_ID = "
                + (Long) map.get("peopleId") + "  AND JUR.ROLE_ID = JRI.ROLE_ID AND JRI.FILTER_ID in " + SqlCommon.getLoginDefaultAuthString();
    }

    /**
     * 判断是不是参与流程
     *
     * @param posIds
     * @return
     */
    public String isParticipationProcess(Set<Long> posIds) {
        return "SELECT COUNT(JFS_PROCESS.FLOW_ID) FROM JECN_FLOW_STRUCTURE JFS_PROCESS"
                + "    INNER JOIN (SELECT J_FLOW.FLOW_ID" + "      FROM PROCESS_STATION_RELATED   PSR,"
                + "           JECN_FLOW_STRUCTURE_IMAGE J_ROLE," + "           JECN_FLOW_STRUCTURE       J_FLOW"
                + "     WHERE PSR.TYPE = '0'" + "       AND PSR.FIGURE_FLOW_ID = J_ROLE.FIGURE_ID"
                + "       AND J_ROLE.FLOW_ID = J_FLOW.FLOW_ID" + "       AND J_FLOW.DEL_STATE = 0"
                + "       AND PSR.FIGURE_POSITION_ID IN " + SqlCommon.getIdsSet(posIds) + "    UNION"
                + "    SELECT J_FLOW.FLOW_ID" + "      FROM PROCESS_STATION_RELATED   PSR,"
                + "           JECN_FLOW_STRUCTURE_IMAGE J_ROLE," + "           JECN_FLOW_STRUCTURE       J_FLOW,"
                + "           JECN_POSITION_GROUP_R     JPGR" + "     WHERE PSR.TYPE = '1'"
                + "       AND PSR.FIGURE_FLOW_ID = J_ROLE.FIGURE_ID" + "       AND J_ROLE.FLOW_ID = J_FLOW.FLOW_ID"
                + "       AND J_FLOW.DEL_STATE = 0" + "       AND PSR.FIGURE_POSITION_ID = JPGR.GROUP_ID"
                + "       AND JPGR.FIGURE_ID IN " + SqlCommon.getIdsSet(posIds)
                + "       ) P_FLOW ON JFS_PROCESS.FLOW_ID = P_FLOW.FLOW_ID";
    }

    /**
     * 是否有岗位查阅权限
     *
     * @param map
     * @return
     */
    public String isAccessPosPermissions(Map<String, Object> map) {
        // 0是流程、1是文件、2是标准、3制度
        int type = Integer.parseInt(map.get("type").toString());
        Long relateId = (Long) map.get("relateId");
        return "SELECT COUNT(*)" + "  FROM (SELECT AP.FIGURE_ID" + "          FROM JECN_ACCESS_PERMISSIONS AP"
                + "         INNER JOIN JECN_FLOW_ORG_IMAGE FOI ON AP.FIGURE_ID ="
                + "                                               FOI.FIGURE_ID"
                + "         INNER JOIN JECN_FLOW_ORG FO ON FOI.ORG_ID = FO.ORG_ID"
                + "                                    AND FO.DEL_STATE = 0"
                + "                                    AND AP.TYPE = " + type
                + "                                    AND AP.RELATE_ID = " + relateId + "        UNION "
                + "        SELECT PGR.FIGURE_ID" + "          FROM JECN_GROUP_PERMISSIONS GP"
                + "         INNER JOIN JECN_POSITION_GROUP_R PGR ON GP.POSTGROUP_ID ="
                + "                                                 PGR.GROUP_ID"
                + "                                             AND GP.TYPE = " + type
                + "                                             AND GP.RELATE_ID = " + relateId + ") TMP_FIGURE"
                + " WHERE TMP_FIGURE.FIGURE_ID IN " + SqlCommon.getIdsSet((Set<Long>) map.get("posIds"));

    }

    /**
     * 是否有查阅权限(部门)
     *
     * @param map
     * @return
     */
    public String isAccessOrgPermissions(Map<String, Object> map) {
        // 0是流程、1是文件、2是标准、3制度
        int type = Integer.parseInt(map.get("type").toString());
        Long relateId = (Long) map.get("relateId");
        return "SELECT COUNT(ORG_P.ID)" + "  FROM JECN_ORG_ACCESS_PERMISSIONS ORG_P" + "  WHERE ORG_P.RELATE_ID="
                + relateId + " AND ORG_P.TYPE=" + type + " AND ORG_P.ORG_ID IN "
                + SqlCommon.getIdsSet((Set<Long>) map.get("orgIds"));

    }

    /**
     * 获得流程基础信息
     *
     * @param id
     * @return
     */
    public String getFlowBese(Long id) {
        return "SELECT JFS.FLOW_ID AS ID,JFS.FLOW_NAME AS NAME,JFS.ISFLOW AS TYPE,JFS.IS_PUBLIC AS PUBLIC_TYPE"
                + "       FROM JECN_FLOW_STRUCTURE JFS WHERE JFS.FLOW_ID=" + id;
    }

    /**
     * 获得制度基础信息
     *
     * @param id
     * @return
     */
    public String getRuleBese(Long id) {
        return "SELECT JFS.ID AS ID,JFS.RULE_NAME AS NAME,JFS.IS_DIR AS TYPE,JFS.IS_PUBLIC AS PUBLIC_TYPE"
                + "       FROM JECN_RULE JFS WHERE JFS.ID=" + id;
    }

    /**
     * 获得标准基础信息
     *
     * @param id
     * @return
     */
    public String getStandardBese(Long id) {
        return "SELECT JFS.CRITERION_CLASS_ID AS ID,JFS.CRITERION_CLASS_NAME AS NAME,JFS.STAN_TYPE AS TYPE,JFS.IS_PUBLIC AS PUBLIC_TYPE"
                + "       FROM JECN_CRITERION_CLASSES JFS WHERE JFS.CRITERION_CLASS_ID=" + id;
    }

    /**
     * 获得文件基础信息
     *
     * @param id
     * @return
     */
    public String getFileBese(Long id) {
        return "SELECT JFS.FILE_ID AS ID,JFS.FILE_NAME AS NAME,JFS.IS_DIR AS TYPE,JFS.IS_PUBLIC AS PUBLIC_TYPE"
                + "       FROM JECN_FILE JFS WHERE JFS.FILE_ID=" + id;

    }

    /**
     * 获取数据权限节点SQL
     *
     * @param map
     * @return
     */
    public String getInitAuthorityNodesSql(Map<String, Object> map) {
        AuthSqlConstant.TYPEAUTH typeAuth = (AuthSqlConstant.TYPEAUTH) map.get("typeAuth");
        String sql = null;
        if (typeAuth == TYPEAUTH.FLOW) {// 流程
            sql = CommonSqlTPath.INSTANCE.getAuthSQLCommon(map) + CommonSqlTPath.INSTANCE.getFlowJoinRole(map);
        } else {
            sql = CommonSqlTPath.INSTANCE.getAuthSQLCommon(map);
        }
        Set<Long> listFigureIds = map.get("listFigureIds") == null ? null : (Set<Long>) map.get("listFigureIds");
        CommonSqlTPath.TmpNodeBean nodeBean = CommonSqlTPath.INSTANCE.getNodeTable(typeAuth);
        sql = " SELECT T.T_PATH,T." + nodeBean.getId() + " ID,null ACCESS_TYPE FROM (" + sql + ") T";
        String downloadSql = "";
        if (listFigureIds != null && listFigureIds.size() != 0) {
            downloadSql = CommonSqlTPath.INSTANCE.getDownloadAuthSQLCommon(map);
            downloadSql = " union  SELECT T.T_PATH,T." + nodeBean.getId() + " ID,ACCESS_TYPE FROM (" + downloadSql + ") T";
        }
        return sql + downloadSql;
    }

    public static String getAuthorityProcessesSql() {
        Map<String, Object> map = new HashedMap();
        map.put("listFigureIds", AuthenticatedUserUtil.getPosIds());
        map.put("listOrgIds", AuthenticatedUserUtil.getOrgIds());
        map.put("typeAuth", AuthSqlConstant.TYPEAUTH.FLOW);
        map.put("projectId", AuthenticatedUserUtil.getProjectId());
        String sql = null;
        if (AuthenticatedUserUtil.isAdmin() || AuthenticatedUserUtil.isViewFlowAdmin()) {
            sql = " select T.T_PATH,T.FLOW_ID from JECN_FLOW_STRUCTURE T where T.DEL_STATE=0 AND T.PROJECTID=" + AuthenticatedUserUtil.getProjectId();
        } else {
            sql = CommonSqlTPath.INSTANCE.getAuthSQLCommon(map) + CommonSqlTPath.INSTANCE.getFlowJoinRole(map);
        }
        return sql;
    }

    public static String getAuthorityRuleSql() {
        Map<String, Object> map = new HashedMap();
        map.put("listFigureIds", AuthenticatedUserUtil.getPosIds());
        map.put("listOrgIds", AuthenticatedUserUtil.getOrgIds());
        map.put("typeAuth", TYPEAUTH.RULE);
        map.put("projectId", AuthenticatedUserUtil.getProjectId());
        String sql = null;
        if (AuthenticatedUserUtil.isAdmin() || AuthenticatedUserUtil.isViewFlowAdmin()) {
            sql = " select T.T_PATH,T.ID from JECN_RULE T where T.DEL_STATE=0 AND T.PROJECT_ID=" + AuthenticatedUserUtil.getProjectId();
        } else {
            sql = CommonSqlTPath.INSTANCE.getAuthSQLCommon(map) + CommonSqlTPath.INSTANCE.getFlowJoinRole(map);
        }
        return sql;
    }

    public String getOrgScope(Map<String, Object> param) {
        String sql = "select JFO.ORG_ID," +
                "       JFS.FLOW_ID," +
                "       JFS.T_PATH" +
                "  from JECN_FLOW_ORG JFO" +
                "  INNER JOIN JECN_FLOW_ORG_IMAGE JFOI ON JFO.ORG_ID=JFOI.ORG_ID" +
                "  INNER JOIN JECN_USER_POSITION_RELATED JUP ON JFOI.FIGURE_ID=JUP.FIGURE_ID AND JUP.PEOPLE_ID=#{peopleId}" +
                "  INNER JOIN JECN_FLOW_APPLY_ORG JF ON JF.ORG_ID=JFO.ORG_ID" +
                "  INNER JOIN JECN_FLOW_STRUCTURE JFS" +
                "    ON JF.RELATED_ID = JFS.FLOW_ID" +
                "   AND JFS.DEL_STATE = 0";
        return sql;
    }
}
