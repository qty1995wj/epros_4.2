package epros.designer.gui.system;

import java.awt.Insets;
import java.util.List;

import org.apache.log4j.Logger;

import com.jecn.epros.server.bean.tree.JecnTreeBean;
import com.jecn.epros.server.util.JecnTreeEnumConstant.TreeNodeType;

import epros.designer.util.ConnectionPool;
import epros.designer.util.JecnConfigTool;
import epros.designer.util.JecnProperties;
import epros.draw.gui.swing.JecnPanel;
import epros.draw.gui.top.dialog.JecnDialog;
import epros.draw.gui.top.optionpane.JecnOptionPane;

public class PersonliableSelectCommon {
	private static Logger log = Logger.getLogger(PersonliableSelectCommon.class);
	/** 0是流程，1是制度 */
	private int nodeType = 0;

	private SingleSelectCommon singleSelectCommon = null;

	private int peopleResType;

	private String labName = JecnProperties.getValue("personLiableC");

	public PersonliableSelectCommon(int rows, JecnPanel infoPanel, Insets insets, Long peopleId, int peopleResType,
			String peopleName, JecnDialog jecnDialog, int nodeType, boolean isRequest, String labName) {
		this(rows, infoPanel, insets, peopleId, peopleResType, peopleName, jecnDialog, nodeType, isRequest);
		singleSelectCommon.getJecnLab().setText(labName);
		singleSelectCommon.setLabName(labName);
	}

	public PersonliableSelectCommon(int rows, JecnPanel infoPanel, Insets insets, Long peopleId, int peopleResType,
			String peopleName, JecnDialog jecnDialog, int nodeType, boolean isRequest) {
		this.nodeType = nodeType;
		JecnTreeBean jecnTreeBean = null;
		if (peopleId != null) {
			// 获取流程责任人listPerson
			if (peopleResType == 1) {
				try {
					List<JecnTreeBean> listPerson = ConnectionPool.getOrganizationAction().getPositionsByIds(
							peopleId.toString());
					if (listPerson.size() == 1) {
						jecnTreeBean = listPerson.get(0);
						jecnTreeBean.setTreeNodeType(TreeNodeType.position);
					}
				} catch (Exception e) {
					log.error(JecnProperties.getValue("responsiblePersonQueryException"), e);
					JecnOptionPane.showMessageDialog(jecnDialog, JecnProperties
							.getValue("responsiblePersonQueryException"));
				}

			} else {
				jecnTreeBean = new JecnTreeBean();
				jecnTreeBean.setId(peopleId);
				jecnTreeBean.setName(peopleName);
				jecnTreeBean.setTreeNodeType(TreeNodeType.person);
			}
		} else {
			peopleResType = getResPeopleTypeConfig();
		}
		if (peopleResType == 1) {
			singleSelectCommon = new posSelectCommon(rows, infoPanel, insets, jecnTreeBean, jecnDialog, labName,
					isRequest);
		} else {
			singleSelectCommon = new PeopleSelectCommon(rows, infoPanel, insets, jecnTreeBean, jecnDialog, labName,
					isRequest);
		}
		this.peopleResType = peopleResType;
	}

	/**
	 * 返回责任人类型
	 * 
	 * @return
	 */
	private int getResPeopleTypeConfig() {
		if (nodeType == 0) {
			return JecnConfigTool.processResponsiblePersonType();
		} else if (nodeType == 1) {
			return JecnConfigTool.ruleResponsiblePersonType();
		} else if (nodeType == 2) {
			return JecnConfigTool.fileResponsiblePersonType();
		} else if (nodeType == 3) {
			return JecnConfigTool.processMapResponsiblePersonType();
		}
		return 0;
	}

	public int getPeopleResType() {
		return peopleResType;
	}

	public SingleSelectCommon getSingleSelectCommon() {
		return singleSelectCommon;
	}

	public String getLabName() {
		return labName;
	}

	public void setLabName(String labName) {
		this.labName = labName;
	}

}
