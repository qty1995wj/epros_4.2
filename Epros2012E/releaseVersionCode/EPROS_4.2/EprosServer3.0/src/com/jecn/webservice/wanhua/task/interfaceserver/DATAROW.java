package com.jecn.webservice.wanhua.task.interfaceserver;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

@XmlAccessorType(XmlAccessType.FIELD)
public class DATAROW {

	@XmlElement(name = "OrderSource")
	protected String orderSource;
	@XmlElement(name = "OrderId")
	protected String orderId;
	@XmlElement(name = "OrderType")
	protected String orderType;
	@XmlElement(name = "OrderTypeName")
	protected String orderTypeName;
	@XmlElement(name = "OrderTaskId")
	protected String orderTaskId;
	@XmlElement(name = "OrderTaskName")
	protected String orderTaskName;
	@XmlElement(name = "OrderUserId")
	protected String orderUserId;
	@XmlElement(name = "OrderUserName")
	protected String orderUserName;
	@XmlElement(name = "OrderTaskCurrentUserId")
	protected String orderTaskCurrentUserId;
	@XmlElement(name = "OrderTaskCurrentUserName")
	protected String orderTaskCurrentUserName;
	@XmlElement(name = "OrderTime")
	protected String orderTime;
	@XmlElement(name = "OrderTaskTime")
	protected String orderTaskTime;
	@XmlElement(name = "OrderTaskStatus")
	protected String orderTaskStatus;
	@XmlElement(name = "OrderTaskDesc")
	protected String orderTaskDesc;
	@XmlElement(name = "OrderTaskUrl")
	protected String orderTaskUrl;
	@XmlElement(name = "OrderDoneTime")
	protected String orderDoneTime = "";
	@XmlElement(name = "OrderCode")
	protected String orderCode = "";

	public String getOrderCode() {
		return orderCode;
	}

	public void setOrderCode(String orderCode) {
		this.orderCode = orderCode;
	}

	public String getOrderSource() {
		return orderSource;
	}

	public void setOrderSource(String orderSource) {
		this.orderSource = orderSource;
	}

	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	public String getOrderType() {
		return orderType;
	}

	public void setOrderType(String orderType) {
		this.orderType = orderType;
	}

	public String getOrderTypeName() {
		return orderTypeName;
	}

	public void setOrderTypeName(String orderTypeName) {
		this.orderTypeName = orderTypeName;
	}

	public String getOrderTaskId() {
		return orderTaskId;
	}

	public void setOrderTaskId(String orderTaskId) {
		this.orderTaskId = orderTaskId;
	}

	public String getOrderTaskName() {
		return orderTaskName;
	}

	public void setOrderTaskName(String orderTaskName) {
		this.orderTaskName = orderTaskName;
	}

	public String getOrderUserId() {
		return orderUserId;
	}

	public void setOrderUserId(String orderUserId) {
		this.orderUserId = orderUserId;
	}

	public String getOrderUserName() {
		return orderUserName;
	}

	public void setOrderUserName(String orderUserName) {
		this.orderUserName = orderUserName;
	}

	public String getOrderTaskCurrentUserId() {
		return orderTaskCurrentUserId;
	}

	public void setOrderTaskCurrentUserId(String orderTaskCurrentUserId) {
		this.orderTaskCurrentUserId = orderTaskCurrentUserId;
	}

	public String getOrderTaskCurrentUserName() {
		return orderTaskCurrentUserName;
	}

	public void setOrderTaskCurrentUserName(String orderTaskCurrentUserName) {
		this.orderTaskCurrentUserName = orderTaskCurrentUserName;
	}

	public String getOrderTime() {
		return orderTime;
	}

	public void setOrderTime(String orderTime) {
		this.orderTime = orderTime;
	}

	public String getOrderTaskTime() {
		return orderTaskTime;
	}

	public void setOrderTaskTime(String orderTaskTime) {
		this.orderTaskTime = orderTaskTime;
	}

	public String getOrderTaskStatus() {
		return orderTaskStatus;
	}

	public void setOrderTaskStatus(String orderTaskStatus) {
		this.orderTaskStatus = orderTaskStatus;
	}

	public String getOrderTaskDesc() {
		return orderTaskDesc;
	}

	public void setOrderTaskDesc(String orderTaskDesc) {
		this.orderTaskDesc = orderTaskDesc;
	}

	public String getOrderTaskUrl() {
		return orderTaskUrl;
	}

	public void setOrderTaskUrl(String orderTaskUrl) {
		this.orderTaskUrl = orderTaskUrl;
	}

	public String getOrderDoneTime() {
		return orderDoneTime;
	}

	public void setOrderDoneTime(String orderDoneTime) {
		this.orderDoneTime = orderDoneTime;
	}

}
