package com.jecn.epros.domain.inventory;

import java.util.List;

public class FileInventoryRelateBean {
    private Long id;
    private List<FileInventoryCommonBean> listFileInventoryCommonBean;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public List<FileInventoryCommonBean> getListFileInventoryCommonBean() {
        return listFileInventoryCommonBean;
    }

    public void setListFileInventoryCommonBean(List<FileInventoryCommonBean> listFileInventoryCommonBean) {
        this.listFileInventoryCommonBean = listFileInventoryCommonBean;
    }
}
