package com.jecn.epros.server.webBean.popedom;

import java.util.List;

import com.jecn.epros.server.bean.tree.JecnTreeBean;
import com.jecn.epros.server.webBean.process.JecnRoleProcessBean;

public class JobDescriptionBean {
	/** 岗位ID */
	private long posId;
	/** 岗位名称 */
	private String posName;
	/** 所属部门ID */
	private long orgId;
	/** 所属部门名称 */
	private String orgName;
	/** 上级岗位 */
	private List<JecnTreeBean> upPositionList;
	/** 下属岗位 */
	private List<JecnTreeBean> lowerPositionList;
	/** 部门内相关制度 */
	private List<JecnTreeBean> inListRule;
	/** 部门外相关制度 */
	private List<JecnTreeBean> outListRule;
	/** 岗位类别 */
	private String positionsNature;
	/** 核心价值 */
	private String schoolExperience;
	/** 学历 */
	private String knowledge;
	/** 知识 */
	private String positionSkill;
	/** 技能 */
	private String positionImportance;
	/** 素质模型 */
	private String positionDiathesisModel;
	/** 岗位职责List */
	private JecnRoleProcessBean jecnRoleProcessBean;
	/** 附件 */
	private Long fileId;
	/** 附件名 */
	private String fileName;

	public String getKnowledge() {
		return knowledge;
	}

	public void setKnowledge(String knowledge) {
		this.knowledge = knowledge;
	}

	public String getPositionsNature() {
		return positionsNature;
	}

	public void setPositionsNature(String positionsNature) {
		this.positionsNature = positionsNature;
	}

	public String getSchoolExperience() {
		return schoolExperience;
	}

	public void setSchoolExperience(String schoolExperience) {
		this.schoolExperience = schoolExperience;
	}

	public String getPositionSkill() {
		return positionSkill;
	}

	public void setPositionSkill(String positionSkill) {
		this.positionSkill = positionSkill;
	}

	public String getPositionImportance() {
		return positionImportance;
	}

	public void setPositionImportance(String positionImportance) {
		this.positionImportance = positionImportance;
	}

	public String getPositionDiathesisModel() {
		return positionDiathesisModel;
	}

	public void setPositionDiathesisModel(String positionDiathesisModel) {
		this.positionDiathesisModel = positionDiathesisModel;
	}

	public JecnRoleProcessBean getJecnRoleProcessBean() {
		return jecnRoleProcessBean;
	}

	public void setJecnRoleProcessBean(JecnRoleProcessBean jecnRoleProcessBean) {
		this.jecnRoleProcessBean = jecnRoleProcessBean;
	}

	public String getOrgName() {
		return orgName;
	}

	public void setOrgName(String orgName) {
		this.orgName = orgName;
	}

	public List<JecnTreeBean> getUpPositionList() {
		return upPositionList;
	}

	public void setUpPositionList(List<JecnTreeBean> upPositionList) {
		this.upPositionList = upPositionList;
	}

	public List<JecnTreeBean> getLowerPositionList() {
		return lowerPositionList;
	}

	public void setLowerPositionList(List<JecnTreeBean> lowerPositionList) {
		this.lowerPositionList = lowerPositionList;
	}

	public List<JecnTreeBean> getInListRule() {
		return inListRule;
	}

	public void setInListRule(List<JecnTreeBean> inListRule) {
		this.inListRule = inListRule;
	}

	public List<JecnTreeBean> getOutListRule() {
		return outListRule;
	}

	public void setOutListRule(List<JecnTreeBean> outListRule) {
		this.outListRule = outListRule;
	}

	public long getPosId() {
		return posId;
	}

	public void setPosId(long posId) {
		this.posId = posId;
	}

	public String getPosName() {
		return posName;
	}

	public void setPosName(String posName) {
		this.posName = posName;
	}

	public long getOrgId() {
		return orgId;
	}

	public void setOrgId(long orgId) {
		this.orgId = orgId;
	}

	public Long getFileId() {
		return fileId;
	}

	public void setFileId(Long fileId) {
		this.fileId = fileId;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	
}
