package com.jecn.epros.service;

import com.jecn.epros.bean.*;
import com.jecn.epros.bean.external.TaskParam;

import java.util.List;
import java.util.Map;

public interface ITaskRPCService {

    /**
     * 通过，返回，打回
     *
     * @param taskParam
     */
    public MessageResult approve(Long curPeopleId, BaseTaskParam taskParam);

    /**
     * 部门，文控 通过，返回，打回
     *
     * @param taskParam
     */
    public MessageResult approveChangeInfo(Long curPeopleId, TaskChangeInfoApproveParam taskParam);

    /**
     * 转批，交办
     *
     * @param taskParam
     */
    public MessageResult assign(Long curPeopleId, TaskAssignParam taskParam);

    /**
     * 部门，文控 转批，交办
     *
     * @param taskParam
     */
    public MessageResult assignChangeInfo(Long curPeopleId, TaskChangeInfoAssignParam taskParam);

    /**
     * 拟稿人重新提交
     *
     * @param taskParam
     */
    public MessageResult resubmit(Long curPeopleId, TaskReSubmitParam taskParam);

    /**
     * 文控主导审批重新提交
     *
     * @param taskParam
     */
    public MessageResult controlSubmit(Long curPeopleId, TaskControlSubmitParam taskParam);

    /**
     * 编辑提交
     *
     * @param taskParam
     */
    public MessageResult editSubmit(Long curPeopleId, TaskEditSubmitParam taskParam);

    /**
     * 编辑打回
     *
     * @param taskParam
     */
    public MessageResult editRollBack(Long curPeopleId, TaskEditSubmitParam taskParam);

    /**
     * 二次评审
     *
     * @param taskParam
     */
    public MessageResult twoApprove(Long curPeopleId, TaskTwoApproveParam taskParam);

    /**
     * 撤回
     *
     * @param taskParam
     */
    public MessageResult callback(Long curPeopleId, BaseTaskParam taskParam);

    /**
     * 下载任务
     *
     * @param taskParam
     */
    public ByteFile downloadTask(Long curPeopleId, TaskSearchBean taskParam);

    public String taskCountsByUser(String userCode);

    public List<Map<String, Object>> tasksByUser(String userCode);

    public void deleteSendInfos(Long taskId) throws Exception;

    MessageResult handleTaskExternal(TaskParam task);
}
