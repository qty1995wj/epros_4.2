package epros.designer.gui.common;

import java.awt.Cursor;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;

import org.apache.log4j.Logger;

import com.jecn.epros.server.bean.popedom.JecnOpenOrgBean;
import com.jecn.epros.server.bean.process.JecnFlowStructureT;
import com.jecn.epros.server.bean.process.ProcessOpenData;
import com.jecn.epros.server.bean.process.ProcessOpenMapData;
import com.jecn.epros.server.util.JecnTreeEnumConstant.TreeNodeType;

import epros.designer.gui.process.activitiesProperty.ActiveDetailPopupMenu;
import epros.designer.gui.process.flow.EditFlowFileDialog;
import epros.designer.gui.process.flow.FlowRoleActiveDialog;
import epros.designer.gui.process.flowmap.DescDetailPopupMenu;
import epros.designer.gui.workflow.JecnDesignerMainPanel;
import epros.designer.service.process.JecnOrganization;
import epros.designer.service.process.JecnProcess;
import epros.designer.tree.JecnTreeCommon;
import epros.designer.tree.JecnTreeNode;
import epros.designer.tree.JecnTreeRenderer;
import epros.designer.util.ConnectionPool;
import epros.designer.util.JecnConfigTool;
import epros.designer.util.JecnProperties;
import epros.draw.constant.JecnToolBoxConstant.MapElemType;
import epros.draw.constant.JecnToolBoxConstant.MapType;
import epros.draw.data.figure.JecnActiveData;
import epros.draw.data.figure.JecnFigureData;
import epros.draw.data.figure.JecnRoleData;
import epros.draw.designer.JecnControlPointT;
import epros.draw.gui.figure.JecnBaseFigurePanel;
import epros.draw.gui.swing.JecnBaseFlowElementPanel;
import epros.draw.gui.top.optionpane.JecnOptionPane;
import epros.draw.gui.workflow.JecnDrawMainPanel;
import epros.draw.gui.workflow.tabbedPane.JecnDrawDesktopPane;
import epros.draw.system.data.JecnSystemStaticData;
import epros.draw.util.DrawCommon;

/**
 * 
 * 图形元素超链接
 * 
 * @author YUXW
 * 
 */
public class JecnLinkButton extends JButton implements MouseListener, ActionListener {
	private static Logger log = Logger.getLogger(JecnLinkButton.class);
	/** 图片被添加的父元素 */
	private JecnBaseFigurePanel elementPanel;
	/** 0是连接，1是角色，2是活动输入，3是活动的输出，4是活动操作规范 */
	private int type;

	/** 画图面板的类型 */
	// private MapType mapType;

	public JecnLinkButton(JecnBaseFigurePanel elementPanel, MapType mapType, int type) {
		this.elementPanel = elementPanel;
		// this.mapType = mapType;
		this.setBorder(null);
		this.setContentAreaFilled(false);
		this.type = type;
		this.addMouseListener(this);
		this.addActionListener(this);
		// 元素数据
		JecnFigureData figureData = elementPanel.getFlowElementData();
		switch (type) {
		case 0:
			if (DrawCommon.isImplFigure(figureData.getMapElemType())) {
				String linkType = figureData.getDesignerFigureData().getLineType();
				if ("1".equals(linkType)) {
					this.setIcon(new ImageIcon("images/menuImage/relaUpProcess.gif"));
					this.setToolTipText(JecnProperties.getValue("topFlow"));
				} else if ("2".equals(linkType)) {
					this.setIcon(new ImageIcon("images/menuImage/relaDownProcess.gif"));
					this.setToolTipText(JecnProperties.getValue("bottomFlow"));
				} else if ("3".equals(linkType)) {
					this.setIcon(new ImageIcon("images/menuImage/relaInterfaceProcess.gif"));
					this.setToolTipText(JecnProperties.getValue("interfaceFlow"));
				} else if ("4".equals(linkType)) {
					this.setIcon(new ImageIcon("images/menuImage/relaSonProcess.gif"));
					this.setToolTipText(JecnProperties.getValue("sonFlow"));
				}
			} else {
				this.setIcon(new ImageIcon("images/icons/linkFlow.gif"));
			}
			break;
		case 1:
			JecnRoleData roleData = (JecnRoleData) figureData;
			if (roleData.getFlowStationList() != null && roleData.getFlowStationList().size() > 0) {
				if ("4".equals(roleData.getActivityId())) {
					this.setIcon(new ImageIcon("images/icons/linkPositionMain.gif"));
				} else {
					this.setIcon(new ImageIcon("images/icons/linkPosition.gif"));
				}
			}

			break;
		case 2:
			this.setIcon(new ImageIcon("images/icons/linkArrowLine.gif"));
			break;
		case 3:
			this.setIcon(new ImageIcon("images/icons/linkArrowLine.gif"));
			break;
		case 4:
			this.setIcon(new ImageIcon("images/icons/linkOperatorCriterion.gif"));
			break;
		case 5:
			this.setIcon(new ImageIcon("images/icons/linkOperatorCriterion.gif"));
			break;
		case 6:// 相关控制点
			setControlPointIcon();
			break;
		case 8:// 相关标准
			this.setIcon(new ImageIcon("images/icons/linkBZ.gif"));
			break;
		case 9:// 活动输入
			this.setIcon(new ImageIcon("images/icons/linkArrowLineText.gif"));
			break;
		case 10:// 活动输出
			this.setIcon(new ImageIcon("images/icons/linkArrowLineText.gif"));
			break;
		default:
			break;

		}

	}

	/**
	 * 设置控制点图标
	 * 
	 * @author weidp
	 * @date 2014-11-5 下午03:02:23
	 */
	private void setControlPointIcon() {
		JecnActiveData activeData = (JecnActiveData) elementPanel.getFlowElementData();
		boolean flag = false;
		// 有一个控制点为关键控制点时 使用关键控制点的图标
		for (JecnControlPointT controlPoint : activeData.getJecnControlPointTList()) {
			if (controlPoint.getKeyPoint() == 0) {
				flag = true;
				break;
			}
		}
		if (flag) {
			this.setIcon(new ImageIcon("images/icons/linkKeyControl.gif"));
		} else {
			this.setIcon(new ImageIcon("images/icons/linkControlP.gif"));
		}
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		MapElemType elemType = elementPanel.getFlowElementData().getMapElemType();
		TreeNodeType nodeType = null;
		switch (type) {
		case 0:
			switch (elemType) {
			case SystemFigure:// 打开制度文件
				JecnDesignerCommon.openRuleInfoDialog(elementPanel.getFlowElementData().getDesignerFigureData()
						.getLinkId());
				break;
			case Rect: // 组织
				Long orgId = elementPanel.getFlowElementData().getDesignerFigureData().getLinkId();
				// 判断是否已经打开
				if (JecnProcess.isOpen(orgId.intValue(), MapType.orgMap)) {
					return;
				}
				try {
					JecnTreeCommon.searchNode(orgId, TreeNodeType.organization, JecnDesignerMainPanel
							.getDesignerMainPanel().getTree());
					JecnOpenOrgBean orgOpenData = ConnectionPool.getOrganizationAction().openOrgMap(orgId);
					JecnOrganization.openOrganizationMap(orgOpenData);
				} catch (Exception e1) {
					log.error("", e1);
					JecnOptionPane.showMessageDialog(JecnSystemStaticData.getFrame(), JecnProperties
							.getValue("serverConnException"));
				}
				break;
			case PositionFigure:// 岗位
				break;
			default:// 打开流程
				try {
					long id = elementPanel.getFlowElementData().getDesignerFigureData().getLinkId() == null ? 0
							: elementPanel.getFlowElementData().getDesignerFigureData().getLinkId();
					if (id == 0) {
						return;
					}
					JecnFlowStructureT flowStructureT = ConnectionPool.getProcessAction().getJecnFlowStructureT(id);
					// 流程已删除
					if (flowStructureT == null) {
						JecnOptionPane.showMessageDialog(null, JecnProperties.getValue("fileDelOrAbolish"));
						return;
					}
					nodeType = flowStructureT.getIsFlow() == 0L ? TreeNodeType.processMap : TreeNodeType.process;
					if (!JecnDesignerCommon.processOpenValidate(id, nodeType)) {
						return;
					}
					nodeType = flowStructureT.getNodeType() == 1 ? TreeNodeType.processFile : nodeType;
					// 搜索节点
					JecnTreeNode node = JecnTreeCommon.searchNode(flowStructureT.getFlowId(), nodeType,
							JecnDesignerMainPanel.getDesignerMainPanel().getTree());
					if (node != null) {
						int status = JecnDesignerCommon.getSaveAuthStatus(node.getJecnTreeBean().getTreeNodeType(),
								node.getJecnTreeBean().getId());
						if (nodeType == TreeNodeType.processMap) {
							ProcessOpenMapData processMapData = ConnectionPool.getProcessAction().getProcessMapData(id,
									JecnConstants.projectId);
							processMapData.setIsAuthSave(status);
							JecnProcess.openJecnProcessTotalMap(processMapData, false);

						} else if (nodeType == TreeNodeType.process) {
							ProcessOpenData processOpenData = ConnectionPool.getProcessAction().getProcessData(id,
									JecnConstants.projectId);
							processOpenData.setIsAuthSave(status);
							JecnProcess.openJecnProcessMap(processOpenData, false);

						} else if (TreeNodeType.processFile == nodeType) {
							EditFlowFileDialog editFlowFileDialog = new EditFlowFileDialog(node);
							editFlowFileDialog.setVisible(true);
						}
						JecnDrawDesktopPane desktopPane = JecnDrawMainPanel.getMainPanel().getWorkflow();
						if (!node.getJecnTreeBean().isPub()) {
							desktopPane.getTabbedTitlePanel().setTabIcon(
									(Icon) JecnTreeRenderer.class.getField(
											node.getJecnTreeBean().getTreeNodeType().toString() + "NoPub").get(null));
						}
						if (nodeType == TreeNodeType.process) {
							// 如果面板中没有任何元素，弹出重置模板
							boolean flag = false;
							for (JecnBaseFlowElementPanel baseFlowElementPanel : desktopPane.getPanelList()) {
								if (baseFlowElementPanel instanceof JecnBaseFigurePanel) {
									flag = true;
									break;
								}
							}
							if (!flag) {
								FlowRoleActiveDialog d = new FlowRoleActiveDialog();
								d.setVisible(true);
							}
						}
					}

				} catch (Exception ex) {
					log.error("JecnLinkButton actionPerformed is error！", ex);
					JecnOptionPane.showMessageDialog(JecnSystemStaticData.getFrame(), JecnProperties
							.getValue("serverConnException"));
				}
				break;
			}
			break;
		case 1:

			break;
		case 2:// 输入
			if (JecnConfigTool.isCetc10Login()) {
				break;
			}
			JecnActiveData activeData = (JecnActiveData) elementPanel.getFlowElementData();
			if (!JecnConfigTool.useNewInout()) {
				if (!activeData.isInFiles()) {
					break;
				}
			} else {
				if (!activeData.isInTermFiles()) {
					break;
				}
			}
			ActiveDetailPopupMenu activeIn = new ActiveDetailPopupMenu((JecnActiveData) elementPanel
					.getFlowElementData(), 2);
			activeIn.show(elementPanel, -JecnConstants.detailPopupMenuWidth, 10);
			break;
		case 3:// 输出
			if (JecnConfigTool.isCetc10Login()) {
				break;
			}
			activeData = (JecnActiveData) elementPanel.getFlowElementData();
			if (!JecnConfigTool.useNewInout()) {
				if (activeData.getListModeFileT() == null || activeData.getListModeFileT().size() == 0) {
					break;
				}
			} else {
				if (activeData.getListFigureOutTs() == null || activeData.getListFigureOutTs().size() == 0) {
					break;
				}
			}
			ActiveDetailPopupMenu activeOut = new ActiveDetailPopupMenu((JecnActiveData) elementPanel
					.getFlowElementData(), 3);
			activeOut.show(elementPanel, elementPanel.getWidth() + 1, 10);
			break;
		case 4:// 操作规范
			ActiveDetailPopupMenu activeOperator = new ActiveDetailPopupMenu((JecnActiveData) elementPanel
					.getFlowElementData(), 4);
			activeOperator.show(elementPanel, 0, elementPanel.getHeight() + 1);
			break;
		case 5:// 说明
			DescDetailPopupMenu detailPopupMenu = new DescDetailPopupMenu((JecnFigureData) elementPanel
					.getFlowElementData(), 5);
			detailPopupMenu.show(elementPanel, 0, elementPanel.getHeight() + 1);
			break;
		case 6:// 相关控制点
			ActiveDetailPopupMenu activeControl = new ActiveDetailPopupMenu((JecnActiveData) elementPanel
					.getFlowElementData(), 5);
			activeControl.show(elementPanel, -JecnConstants.detailPopupMenuWidth, 10);
			break;
		case 8:// 相关标准
			ActiveDetailPopupMenu activeStandards = new ActiveDetailPopupMenu((JecnActiveData) elementPanel
					.getFlowElementData(), 6);
			activeStandards.show(elementPanel, elementPanel.getWidth() + 1, 10);
			break;
		case 9:
			activeData = (JecnActiveData) elementPanel.getFlowElementData();
			if (!JecnConfigTool.useNewInout()) {
				if (!activeData.isInFiles()) {
					break;
				}
			} else {
				if (!activeData.isInTermFiles()) {
					break;
				}
			}
			activeIn = new ActiveDetailPopupMenu((JecnActiveData) elementPanel.getFlowElementData(), 2);
			activeIn.show(elementPanel, -JecnConstants.detailPopupMenuWidth, 10);
			break;
		case 10:// 输出
			activeData = (JecnActiveData) elementPanel.getFlowElementData();
			if (!JecnConfigTool.useNewInout()) {
				if (activeData.getListModeFileT() == null || activeData.getListModeFileT().size() == 0) {
					break;
				}
			} else {
				if (activeData.getListFigureOutTs() == null || activeData.getListFigureOutTs().size() == 0) {
					break;
				}
			}
			activeOut = new ActiveDetailPopupMenu((JecnActiveData) elementPanel.getFlowElementData(), 3);
			activeOut.show(elementPanel, elementPanel.getWidth() + 1, 10);
			break;
		default:
			break;
		}
	}

	@Override
	public void mouseClicked(MouseEvent e) {
	}

	@Override
	public void mousePressed(MouseEvent e) {

	}

	@Override
	public void mouseReleased(MouseEvent e) {

	}

	@Override
	public void mouseEntered(MouseEvent e) {
		this.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
	}

	@Override
	public void mouseExited(MouseEvent e) {
		this.setCursor(Cursor.getDefaultCursor());
	}

}
