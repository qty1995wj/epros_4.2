package com.jecn.epros.server.webBean.standard;

/**
 * 标准清单相关文件信息
 * 
 * @author ZHAGNXIAOHU
 * @date： 日期：2013-12-13 时间：下午05:29:46
 */
public class StandardRelatePRFBean {
	/** refType=1 为流程ID，refType = 2 为制度ID */
	private Long refId;
	private String refName;
	/** 1:流程，2：制度文件,3:活动，4：制度模板文件 */
	private int refType;
	/** 活动主键ID */
	private Long figureId;
	/** 活动名称 */
	private String activeName;

	public Long getRefId() {
		return refId;
	}

	public void setRefId(Long refId) {
		this.refId = refId;
	}

	public String getRefName() {
		return refName;
	}

	public void setRefName(String refName) {
		this.refName = refName;
	}

	public int getRefType() {
		return refType;
	}

	public void setRefType(int refType) {
		this.refType = refType;
	}

	public Long getFigureId() {
		return figureId;
	}

	public void setFigureId(Long figureId) {
		this.figureId = figureId;
	}

	public String getActiveName() {
		return activeName;
	}

	public void setActiveName(String activeName) {
		this.activeName = activeName;
	}

	/**
	 * 活动链接
	 * 
	 * @return
	 */
	public String getHrefActive() {
		return "<a href='processActive.action?activeId=" + this.figureId
				+ "' target='_blank' style='cursor: pointer;' class='table'>"
				+ this.activeName + "</a>";
	}

	/**
	 * 流程链接
	 * 
	 * @return
	 */
	public String getHrefFlow() {
		return "<a href='process.action?type=process&flowId=" + this.refId
				+ "' target='_blank' style='cursor: pointer;' class='table'>"
				+ this.refName + "</a>";
	}

	/**
	 * 制度链接
	 * 
	 * @return
	 */
	public String getHrefRule() {
		String type = "";
		if (refType == 2) {
			type = "ruleFile";
		} else if (refType == 4) {
			type = "ruleModeFile";
		}
		return "<a href='rule.action?type=" + type + "&ruleId=" + this.refId
				+ "' target='_blank' style='cursor: pointer;' class='table'>"
				+ this.refName + "</a>";
	}
}
