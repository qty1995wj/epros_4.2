package epros.draw.gui.swing;

import java.awt.BorderLayout;
import java.awt.Color;

import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JTextPane;

import epros.draw.util.DrawCommon;

/**
 * 
 * 弹出提示信息菜单
 * 
 * @author ZHOUXY
 *
 */
public class JecnTipPopupMenu {
	/** 弹出菜单 */
	private JPopupMenu popupMenu = null;
	/** 内容面板 */
	private JPanel panel = null;
	/** 内容标签 */
	private JTextPane textPane = null;

	public JecnTipPopupMenu() {
		initComponent();
	}

	private void initComponent() {
		// 弹出菜单
		popupMenu = new JPopupMenu();
		// 内容面板
		panel = new JPanel();
		// 内容标签
		textPane = new JTextPane();

		panel.setLayout(new BorderLayout());

		textPane.setEditable(false);
		textPane.setBorder(null);
		textPane.setBackground(Color.YELLOW);
		textPane.setForeground(Color.red);
		textPane.setContentType("text/html");

		popupMenu.add(panel);
		panel.add(textPane, BorderLayout.CENTER);
	}

	/**
	 * 
	 * 提示内容
	 * 
	 * @param text
	 *            String
	 */
	public void setText(String text) {
		String tmp = "";
		if (!DrawCommon.isNullOrEmtryTrim(text)) {
			tmp = text;
		}
		textPane
				.setText("<html>"
						+ "<div style= 'white-space:normal; display:block;width:200px; word-break:break-all;font-size:10px;color:red'>"
						+ tmp + "</div></html>");
	}

	public JPopupMenu getPopupMenu() {
		return popupMenu;
	}
}
