package com.jecn.epros.server.service.reports.excel.impl;

import java.util.List;

import com.jecn.epros.server.bean.process.temp.TempApproveTimeHeadersBean;
import com.jecn.epros.server.bean.process.temp.TempTaskApproveTime;
import com.jecn.epros.server.bean.propose.JecnRtnlProposeCount;
import com.jecn.epros.server.service.reports.IReportDownExcelService;
import com.jecn.epros.server.service.reports.excel.ProcessAccessSumDownExcelService;
import com.jecn.epros.server.service.reports.excel.ProcessAccessUserDownExcelService;
import com.jecn.epros.server.service.reports.excel.ProcessActiveDownExcelService;
import com.jecn.epros.server.service.reports.excel.ProcessAgingDownExcelService;
import com.jecn.epros.server.service.reports.excel.ProcessAnalysisDownExcelService;
import com.jecn.epros.server.service.reports.excel.ProcessAppUserCountDownExcelService;
import com.jecn.epros.server.service.reports.excel.ProcessBuildCoverDownExcelService;
import com.jecn.epros.server.service.reports.excel.ProcessDetailDownExcelService;
import com.jecn.epros.server.service.reports.excel.ProcessDownSumDownExcelService;
import com.jecn.epros.server.service.reports.excel.ProcessDownUserDownExcelService;
import com.jecn.epros.server.service.reports.excel.ProcessExpiryDownErrorExcelService;
import com.jecn.epros.server.service.reports.excel.ProcessExpiryDownExcelService;
import com.jecn.epros.server.service.reports.excel.ProcessInputOutputDownExcelService;
import com.jecn.epros.server.service.reports.excel.ProcessKPIFollowDownExcelService;
import com.jecn.epros.server.service.reports.excel.ProcessRuleDownExcelService;
import com.jecn.epros.server.service.reports.excel.ProcessScanDownExcelService;
import com.jecn.epros.server.service.reports.excel.ProposeDownExcelService;
import com.jecn.epros.server.service.reports.excel.TaskApproveTimeDownExcelService;
import com.jecn.epros.server.webBean.reports.AgingProcessBean;
import com.jecn.epros.server.webBean.reports.ProcessAccessBean;
import com.jecn.epros.server.webBean.reports.ProcessActivitiesBean;
import com.jecn.epros.server.webBean.reports.ProcessAnalysisBean;
import com.jecn.epros.server.webBean.reports.ProcessApplicationDetailsBean;
import com.jecn.epros.server.webBean.reports.ProcessConstructionBean;
import com.jecn.epros.server.webBean.reports.ProcessDetailBean;
import com.jecn.epros.server.webBean.reports.ProcessExpiryMaintenanceBean;
import com.jecn.epros.server.webBean.reports.ProcessInputOutputBean;
import com.jecn.epros.server.webBean.reports.ProcessKPIFollowBean;
import com.jecn.epros.server.webBean.reports.ProcessRoleBean;
import com.jecn.epros.server.webBean.reports.ProcessScanOptimizeBean;

/**
 * 
 * 报表统计下载Excel类实现类
 * 
 * @author ZHOUXY
 * 
 */
public class ReportDownExcelServiceImpl implements IReportDownExcelService {

	/**
	 * 
	 * 获取流程时效性数据
	 * 
	 * @param agingProcessBean
	 *            AgingProcessBean 待下载数据
	 * @return byte[]
	 */
	@Override
	public byte[] getAgingProcessExcel(AgingProcessBean agingProcessBean) {
		ProcessAgingDownExcelService excel = new ProcessAgingDownExcelService();
		excel.setAgingProcessBean(agingProcessBean);

		return excel.getExcelFile();
	}

	/**
	 * 
	 * 获取流程建设覆盖度数据
	 * 
	 * @param buildCoverageBean
	 *            ProcessConstructionBean 待下载数据
	 * @return byte[]
	 */
	@Override
	public byte[] getProcessBuildCoverageExcel(
			ProcessConstructionBean buildCoverageBean) {
		ProcessBuildCoverDownExcelService excel = new ProcessBuildCoverDownExcelService();
		excel.setBuildCoverageBean(buildCoverageBean);
		return excel.getExcelFile();
	}

	/**
	 * 流程角色数统计下载
	 * 
	 * @param processRoleList
	 *            List<ProcessRoleBean> 待下载数据
	 * @return byte[]
	 */
	public byte[] downloadProcessRole(List<ProcessRoleBean> processRoleList) {
		ProcessRuleDownExcelService excel = new ProcessRuleDownExcelService();
		excel.setProcessRoleList(processRoleList);
		return excel.getExcelFile();
	}

	/**
	 * 流程活动数统计下载
	 * 
	 * @param processActivitiesList
	 *            List<ProcessActivitiesBean> 待下载数据
	 * @return byte[]
	 */
	public byte[] downloadProcessActivities(
			List<ProcessActivitiesBean> processActivitiesList) {
		ProcessActiveDownExcelService excel = new ProcessActiveDownExcelService();
		excel.setProcessActivitiesList(processActivitiesList);
		return excel.getExcelFile();
	}

	/**
	 * 流程应用人数统计下载
	 * 
	 * @param appUserCountList
	 *            List<ProcessApplicationDetailsBean> 待下载数据
	 * @return byte[]
	 */
	public byte[] downloadProcessApplication(
			List<ProcessApplicationDetailsBean> appUserCountList) {
		ProcessAppUserCountDownExcelService excel = new ProcessAppUserCountDownExcelService();
		excel.setAppUserCountList(appUserCountList);
		return excel.getExcelFile();
	}

	/**
	 * 流程输入输出统计下载
	 * 
	 * @param processInputOutputList
	 *            List<ProcessInputOutputBean> 待下载数据
	 * @return byte[]
	 */
	public byte[] downloadProcessInputOutput(
			List<ProcessInputOutputBean> processInputOutputList) {
		ProcessInputOutputDownExcelService excel = new ProcessInputOutputDownExcelService();
		excel.setProcessInputOutputList(processInputOutputList);
		return excel.getExcelFile();
	}

	/**
	 * 流程分析统计下载
	 * 
	 * @param processAnalysisList
	 *            List<ProcessAnalysisBean> 待下载数据
	 * @return byte[]
	 */
	public byte[] downloadProcessAnalysis(
			List<ProcessAnalysisBean> processAnalysisList) {
		ProcessAnalysisDownExcelService excel = new ProcessAnalysisDownExcelService();
		excel.setProcessAnalysisList(processAnalysisList);
		return excel.getExcelFile();
	}

	/**
	 * 流程访问数统计下载
	 * 
	 * @param processAccessList
	 *            List<ProcessAccessBean> 待下载数据
	 * @return byte[]
	 */
	public byte[] downloadProcessAccess(
			List<ProcessAccessBean> processAccessList) {
		ProcessAccessSumDownExcelService excel = new ProcessAccessSumDownExcelService();
		excel.setProcessAccessList(processAccessList);
		return excel.getExcelFile();
	}

	/**
	 * 流程访问人统计下载
	 * 
	 * @param processAccessList
	 *            List<ProcessAccessBean> 待下载数据
	 * @return byte[]
	 */
	public byte[] downloadProcessInterviewer(
			List<ProcessAccessBean> processAccessList) {
		ProcessAccessUserDownExcelService excel = new ProcessAccessUserDownExcelService();
		excel.setProcessAccessList(processAccessList);
		return excel.getExcelFile();
	}

	/**
	 * 流程文档下载数统计下载
	 * 
	 * @param processDocumentDownloadList
	 *            List<ProcessDocumentDownloadBean> 待下载数据
	 * @return byte[]
	 */
	public byte[] downloadProcessDocumentNumber(
			List<ProcessAccessBean> processDocumentDownloadList) {
		ProcessDownSumDownExcelService excel = new ProcessDownSumDownExcelService();
		excel.setProcessAccessList(processDocumentDownloadList);
		return excel.getExcelFile();
	}

	/**
	 * 流程文档下载人统计下载
	 * 
	 * @param processDocumentDownloadList
	 *            List<ProcessDocumentDownloadBean> 待下载数据
	 * @return byte[]
	 */
	public byte[] downloadProcessDocumentPeople(
			List<ProcessAccessBean> processDocumentDownloadList) {
		ProcessDownUserDownExcelService excel = new ProcessDownUserDownExcelService();
		excel.setProcessAccessList(processDocumentDownloadList);
		return excel.getExcelFile();
	}

	/**
	 * 任务审批时间统计下载
	 * 
	 * @param taskApproveTimeList
	 *            List<TempTaskApproveTime> 待下载数据
	 * @return byte[]
	 */
	
	public byte[] downloadTaskApproveTime(
			List<TempTaskApproveTime> taskApproveTimeList,TempApproveTimeHeadersBean headersBean) {
		TaskApproveTimeDownExcelService excel=new TaskApproveTimeDownExcelService();
		excel.setTaskApproveTimeList(taskApproveTimeList);
		excel.setHeadersBean(headersBean);
		return excel.getExcelFile();
	}

	/**
	 * 流程清单待下载数据
	 * @param processDetailBean
	 *            ProcessDetailBean 待下载数据 
	 * @return byte[]           
	 * @throws Exception 
	 */
	@Override
	public byte[] downloadProcessDetail(ProcessDetailBean processDetailBean) throws Exception {
		ProcessDetailDownExcelService excel=new ProcessDetailDownExcelService();
		excel.setProcessDetailBean(processDetailBean);
		return excel.getExcelFile();
	}

	/**
	 * 流程优化审视统计表待下载数据
	 * @param processScanOptimize
	 *            ProcessScanOptimizeBean 待下载数据 
	 * @return byte[] 
     * @throws Exception         
	 */
	@Override
	public byte[] downloadProcessScanOptimize(
			ProcessScanOptimizeBean processScanOptimize) throws Exception{
		ProcessScanDownExcelService excel=new ProcessScanDownExcelService();
		excel.setProcessScanOptimizeBean(processScanOptimize);
		return excel.getExcelFile();
	}

	/**
	 * 流程KPI跟踪表待下载数据
	 * @param processKPIFollowBean
	 *            ProcessKPIFollowBean 待下载数据
	 * @return byte[] 
     * @throws Exception         
	 */
	@Override
	public byte[] downloadProcessKPIFollow(
			ProcessKPIFollowBean processKPIFollowBean) throws Exception{
		ProcessKPIFollowDownExcelService excel=new ProcessKPIFollowDownExcelService();
		excel.setProcessKPIFollowBean(processKPIFollowBean);
		return excel.getExcelFile();
	}

	@Override
	public byte[] downloadProcessExpiryMaintenanceBean(
			ProcessExpiryMaintenanceBean expiryMaintenanceBean) throws Exception {
		ProcessExpiryDownExcelService excel=new ProcessExpiryDownExcelService(expiryMaintenanceBean);
		return excel.getExcelFile();
	}

	@Override
	public byte[] downloadProcessExpiryMaintenanceErrorDate(Long peopleId,int type) throws Exception{
		ProcessExpiryDownErrorExcelService excel=new ProcessExpiryDownErrorExcelService();
		excel.setType(type);
		return excel.getExcelFile(peopleId);
	}

	@Override
	public byte[] downloadProposeCount(JecnRtnlProposeCount proposeCountBean) throws Exception {
		ProposeDownExcelService excel=new ProposeDownExcelService();
		excel.setProposeCountBean(proposeCountBean);
		return excel.getExcelFile();
	}
}
