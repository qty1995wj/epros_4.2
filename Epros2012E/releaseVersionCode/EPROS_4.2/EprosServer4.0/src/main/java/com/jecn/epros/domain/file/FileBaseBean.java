package com.jecn.epros.domain.file;

import com.jecn.epros.domain.LinkResource;
import com.jecn.epros.domain.LinkResource.ResourceType;
import com.jecn.epros.domain.Resource;
import com.jecn.epros.service.TreeNodeUtils;
import com.jecn.epros.util.HttpUtil;

import java.util.List;

public class FileBaseBean {
    private String id;
    private Long fileId;
    private String fileName;
    private boolean isHistory;
    private String explain;
    private String name;
    //新版模板 为多个
    private List<FileBaseBean> moduleList;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public List<FileBaseBean> getModuleList() {
        return moduleList;
    }

    public void setModuleList(List<FileBaseBean> moduleList) {
        this.moduleList = moduleList;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getExplain() {
        return explain;
    }

    public void setExplain(String explain) {
        this.explain = explain;
    }

    public void setHistory(boolean history) {
        isHistory = history;
    }

    public void setFileId(Long fileId) {
        this.fileId = fileId;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public LinkResource getLink() {
 /*       String url = HttpUtil.getDownLoadHistoryFileHttp(fileId, false, "", isHistory);*/
        return new LinkResource(fileId, fileName, ResourceType.FILE, TreeNodeUtils.NodeType.process);
    }
}
