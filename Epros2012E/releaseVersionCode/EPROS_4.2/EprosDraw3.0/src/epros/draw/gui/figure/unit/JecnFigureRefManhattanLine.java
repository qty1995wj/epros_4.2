package epros.draw.gui.figure.unit;

import epros.draw.constant.JecnLogConstants;
import epros.draw.constant.JecnWorkflowConstant.EditPointType;
import epros.draw.error.ErrorInfoConstant;
import epros.draw.gui.line.JecnBaseManhattanLinePanel;

/**
 * 记录图形相关联的连接线
 * 
 * @author ZHANGXH
 * @date： 日期：May 8, 2012 时间：5:18:22 PM
 */
public class JecnFigureRefManhattanLine {
	/** 图形相关的连接线 */
	private JecnBaseManhattanLinePanel manhattanLinePanel = null;
	/** 开始线或结束线标识 True开始先，false 结束线 */
	private boolean isStart = false;
	/** 连接线对应图形的编辑点位置 top , right 右, bottom 下, left 左 */
	private EditPointType editPointType = null;

	public JecnFigureRefManhattanLine(
			JecnBaseManhattanLinePanel manhattanLinePanel,
			EditPointType editPointType, boolean isStart) {
		if (manhattanLinePanel == null || editPointType == null) {
			JecnLogConstants.LOG_FIGURE_REF_MANHATTANLINE
					.error("JecnFigureRefManhattanLine类构造函数：参数为null"
							+ "manhattanLinePanel =" + manhattanLinePanel
							+ "editPointType =" + editPointType);
			throw new IllegalArgumentException(ErrorInfoConstant.PARA_OBJ_NULL);
		}
		this.manhattanLinePanel = manhattanLinePanel;
		this.isStart = isStart;
		this.editPointType = editPointType;
	}

	public JecnBaseManhattanLinePanel getManhattanLinePanel() {
		return manhattanLinePanel;
	}

	public void setManhattanLinePanel(
			JecnBaseManhattanLinePanel manhattanLinePanel) {
		if (manhattanLinePanel == null ) {
			JecnLogConstants.LOG_FIGURE_REF_MANHATTANLINE
					.error("JecnFigureRefManhattanLine类方法setManhattanLinePanel：参数为null"
							+ "manhattanLinePanel =" + manhattanLinePanel);
			throw new IllegalArgumentException(ErrorInfoConstant.PARA_OBJ_NULL);
		}
		this.manhattanLinePanel = manhattanLinePanel;
	}

	public boolean isStart() {
		return isStart;
	}

	public void setStart(boolean isStart) {
		this.isStart = isStart;
	}

	public EditPointType getEditPointType() {
		return editPointType;
	}

	public void setEditPointType(EditPointType editPointType) {
		if (editPointType == null) {
			JecnLogConstants.LOG_FIGURE_REF_MANHATTANLINE
					.error("JecnFigureRefManhattanLine类方法setEditPointType：参数为null"
							+ "editPointType =" + editPointType);
			throw new IllegalArgumentException(ErrorInfoConstant.PARA_OBJ_NULL);
		}
		this.editPointType = editPointType;
	}
}
