package com.jecn.epros.domain.report;

import java.util.List;

/**
 * 报表权限报表
 * Created by Administrator on 2018/9/30.
 */
public class PermissionsReport {
    //文件Id
    private Long id;
    //文件名称
    private String name;
    //责任部门Id
    private Long orgId;
    //责任部门
    private String orgName;
    //部门权限数据
    private List<PermissionsData> orgData;
    //岗位权限数据
    private List<PermissionsData> posData;
    //岗位组合权限数据
    private List<PermissionsData> posGroupData;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getOrgId() {
        return orgId;
    }

    public void setOrgId(Long orgId) {
        this.orgId = orgId;
    }

    public String getOrgName() {
        return orgName;
    }

    public void setOrgName(String orgName) {
        this.orgName = orgName;
    }

    public List<PermissionsData> getOrgData() {
        return orgData;
    }

    public void setOrgData(List<PermissionsData> orgData) {
        this.orgData = orgData;
    }

    public List<PermissionsData> getPosData() {
        return posData;
    }

    public void setPosData(List<PermissionsData> posData) {
        this.posData = posData;
    }

    public List<PermissionsData> getPosGroupData() {
        return posGroupData;
    }

    public void setPosGroupData(List<PermissionsData> posGroupData) {
        this.posGroupData = posGroupData;
    }
}
