package com.jecn.epros.domain.report;

/**
 * Created by user on 2017/3/29.
 * 流程清单报表页面bean
 */
public class ProcessDetail extends ReportBaseBean{

    private Integer pubCount;
    private Integer notPubCount;
    private Integer total;
    private Integer percentage;

    public Integer getPubCount() {
        return pubCount;
    }

    public void setPubCount(Integer pubCount) {
        this.pubCount = pubCount;
    }

    public Integer getNotPubCount() {
        return notPubCount;
    }

    public void setNotPubCount(Integer notPubCount) {
        this.notPubCount = notPubCount;
    }

    public Integer getTotal() {
        return total;
    }

    public void setTotal(Integer total) {
        this.total = total;
    }

    public Integer getPercentage() {
        return percentage;
    }

    public void setPercentage(Integer percentage) {
        this.percentage = percentage;
    }
}
