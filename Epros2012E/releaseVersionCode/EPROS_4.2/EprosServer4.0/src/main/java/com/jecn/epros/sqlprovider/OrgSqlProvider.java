package com.jecn.epros.sqlprovider;

import com.jecn.epros.security.AuthenticatedUserUtil;
import com.jecn.epros.sqlprovider.server3.CommonSqlTPath;
import com.jecn.epros.sqlprovider.server3.SqlCommon;
import org.apache.commons.lang3.StringUtils;
import org.apache.ibatis.jdbc.SQL;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class OrgSqlProvider extends BaseSqlProvider {

    public String findOrgNameByIdSql(Long id) {
        return "SELECT ORG_NAME FROM JECN_FLOW_ORG WHERE ORG_ID = " + id;
    }

    public String findPostNameByIdSql(Long id) {
        return "SELECT FIGURE_TEXT FROM JECN_FLOW_ORG_IMAGE WHERE FIGURE_ID = " + id;
    }

    /**
     * 通过Id获得类别名称
     *
     * @param typeId
     * @return
     */
    public String findTypeNameById(Long typeId) {
        String sql = "";
        if (AuthenticatedUserUtil.isLanguageType() == 0) {
            sql = "select type_name from JECN_FLOW_TYPE where type_id=" + typeId;

        } else {
            sql = "select EN_NAME from JECN_FLOW_TYPE where type_id=" + typeId;
        }
        return sql;
    }

    /**
     * 组织树展开
     *
     * @param map
     * @return
     */
    public String findChildOrgs(Map<String, Object> map) {
        Long projectId = Long.valueOf(map.get("projectId").toString());
        Long pId = Long.valueOf(map.get("pId").toString());
        return "SELECT RESULT.*" +
                "  FROM (SELECT DISTINCT TT.ORG_ID AS ID," +
                "                        TT.ORG_NAME AS NAME," +
                "                        CASE" +
                "                          WHEN ORG.ORG_ID IS NULL AND IMAGE.FIGURE_ID IS NULL THEN" +
                "                           0" +
                "                          ELSE" +
                "                           1" +
                "                        END AS COUNT," +
                "                        0 AS TYPE," +
                "                        TT.SORT_ID" +
                "          FROM JECN_FLOW_ORG TT" +
                "          LEFT JOIN JECN_FLOW_ORG ORG ON TT.ORG_ID = ORG.PER_ORG_ID" +
                "                                     AND ORG.DEL_STATE = 0" +
                "          LEFT JOIN JECN_FLOW_ORG_IMAGE IMAGE ON TT.ORG_ID = IMAGE.ORG_ID" +
                "                                             AND IMAGE.FIGURE_TYPE =" +
                "                                                 'PositionFigure'" +
                "         WHERE TT.PER_ORG_ID = " + pId +
                "           AND TT.PROJECTID = " + projectId +
                "           AND TT.DEL_STATE = 0" +
                "        UNION" +
                "        SELECT JFOI.FIGURE_ID AS ID," +
                "               JFOI.FIGURE_TEXT AS NAME," +
                "               0 AS COUNT," +
                "               1 AS TYPE," +
                "               JFOI.SORT_ID" +
                "          FROM JECN_FLOW_ORG_IMAGE JFOI" +
                "         WHERE JFOI.FIGURE_TYPE = 'PositionFigure'" +
                "           AND JFOI.ORG_ID = " + pId + ") RESULT" +
                " ORDER BY RESULT.TYPE, RESULT.SORT_ID";
    }

    /**
     * 岗位基本信息
     *
     * @param posId
     * @return
     */
    public String findPosBaseCommonBean(Long posId) {
        return "select pos.FIGURE_ID, " +
                "pos.POSITION_IMPORTANCE,    " +
                "pos.POSITION_SKILL,    " +
                "pos.POSITION_DIATHESIS_MODEL   ,  " +
                "pos.SCHOOL_EXPERIENCE ," +
                "pos.KNOWLEDGE," +
                "pos.POSITIONS_NATURE  , " +
                "JF.VERSION_ID FILE_ID," +
                " jfoi.figure_text,jf.file_name from jecn_position_fig_info pos"
                + "       left join jecn_flow_org_image jfoi on pos.figure_id = jfoi.figure_id"
                + "       left join jecn_file jf on pos.file_id =jf.file_id and jf.del_state = 0    where pos.figure_id=" + posId;
    }

    /**
     * 获得上级岗位
     *
     * @param posId
     * @return
     */
    public String findPubPosPosRelatedBeans(Long posId) {
        return "select jfoi.figure_id as id, jfoi.figure_text as name,0 as type"
                + "            from jecn_position_upper_lower t"
                + "            inner join jecn_flow_org_image jfoi on t.leve_position_id = jfoi.figure_id"
                + "            where t.leve_type=0 and t.position_id=" + posId;
    }

    /**
     * 获得下级岗位
     *
     * @param posId
     * @return
     */
    public String findSubPosPosRelatedBeans(Long posId) {
        return "select jfoi.figure_id as id, jfoi.figure_text as name,0 as type"
                + "            from jecn_position_upper_lower t"
                + "            inner join jecn_flow_org_image jfoi on t.leve_position_id = jfoi.figure_id"
                + "            where t.leve_type=1 and t.position_id=" + posId;
    }

    /**
     * 获得部门内制度
     *
     * @param posId
     * @return
     */
    public String findInRulePosRelatedBeans(Long posId) {
        return "select t.id,t.rule_name as name,1 as type" + "      from jecn_position_ref_system jr,jecn_rule t"
                + "      where jr.system_id =t.id and t.del_state=0 and jr.position_id=" + posId + "                and jr.type='0'";
    }

    /**
     * 获得部门外制度
     *
     * @param posId
     * @return
     */
    public String findOutRulePosRelatedBeans(Long posId) {
        return "select t.id,t.rule_name as name,1 as type" + "      from jecn_position_ref_system jr,jecn_rule t"
                + "      where jr.system_id =t.id and t.del_state=0 and jr.position_id=" + posId + "                and jr.type='1'";
    }

    /**
     * @param posId
     * @return
     */
    public String findPosRelateRole(Long posId) {
        return "SELECT PROCESS_ROLE.FIGURE_ID      AS ROLE_ID," +
                "               PROCESS_ROLE.FIGURE_TEXT    AS ROLE_NAME," +
                "               PROCESS_ROLE.ROLE_RES," +
                "               JFS.FLOW_ID," +
                "               JFS.FLOW_NAME," +
                "               ACTIVE.FIGURE_ID    AS ACTIVE_ID," +
                "               ACTIVE.FIGURE_TEXT  AS ACTIVE_NAME," +
                "               ACTIVE.ACTIVITY_ID  AS ACTIVE_NUM," +
                "               JRI.INDICATOR_NAME," +
                "               JRI.INDICATOR_VALUE" +
                "          FROM JECN_FLOW_STRUCTURE_IMAGE PROCESS_ROLE" +
                "         INNER JOIN JECN_ROLE_ACTIVE JRA" +
                "            ON JRA.FIGURE_ROLE_ID = PROCESS_ROLE.FIGURE_ID" +
                "         INNER JOIN JECN_FLOW_STRUCTURE_IMAGE ACTIVE" +
                "            ON ACTIVE.FIGURE_ID = JRA.FIGURE_ACTIVE_ID" +
                "         INNER JOIN PROCESS_STATION_RELATED POS" +
                "            ON PROCESS_ROLE.FIGURE_ID = POS.FIGURE_FLOW_ID" +
                "         INNER JOIN JECN_POSITION_GROUP_R JPGR" +
                "            ON POS.FIGURE_POSITION_ID = JPGR.GROUP_ID" +
                "           AND POS.TYPE = '1'" +
                "           AND JPGR.FIGURE_ID =" + posId +
                "          LEFT JOIN JECN_FLOW_STRUCTURE JFS" +
                "            ON PROCESS_ROLE.FLOW_ID = JFS.FLOW_ID" +
                "           AND JFS.DEL_STATE = 0" +
                "          LEFT JOIN JECN_REF_INDICATORS JRI" +
                "            ON JRI.ACTIVITY_ID = ACTIVE.FIGURE_ID" +
                "         UNION" +
                "         SELECT PROCESS_ROLE.FIGURE_ID      AS ROLE_ID," +
                "               PROCESS_ROLE.FIGURE_TEXT    AS ROLE_NAME," +
                "               PROCESS_ROLE.ROLE_RES," +
                "               JFS.FLOW_ID," +
                "               JFS.FLOW_NAME," +
                "               ACTIVE.FIGURE_ID    AS ACTIVE_ID," +
                "               ACTIVE.FIGURE_TEXT  AS ACTIVE_NAME," +
                "               ACTIVE.ACTIVITY_ID  AS ACTIVE_NUM," +
                "               JRI.INDICATOR_NAME," +
                "               JRI.INDICATOR_VALUE" +
                "          FROM JECN_FLOW_STRUCTURE_IMAGE PROCESS_ROLE" +
                "         INNER JOIN JECN_ROLE_ACTIVE JRA" +
                "            ON JRA.FIGURE_ROLE_ID = PROCESS_ROLE.FIGURE_ID" +
                "         INNER JOIN JECN_FLOW_STRUCTURE_IMAGE ACTIVE" +
                "            ON ACTIVE.FIGURE_ID = JRA.FIGURE_ACTIVE_ID" +
                "         INNER JOIN PROCESS_STATION_RELATED POS" +
                "            ON PROCESS_ROLE.FIGURE_ID = POS.FIGURE_FLOW_ID" +
                "           AND POS.TYPE = '0'" +
                "   AND POS.FIGURE_POSITION_ID =" + posId +
                "  LEFT JOIN JECN_FLOW_STRUCTURE JFS" +
                "    ON PROCESS_ROLE.FLOW_ID = JFS.FLOW_ID" +
                "   AND JFS.DEL_STATE = 0" +
                "  LEFT JOIN JECN_REF_INDICATORS JRI" +
                "    ON JRI.ACTIVITY_ID = ACTIVE.FIGURE_ID" +
                " ORDER BY ROLE_ID, FLOW_ID, ACTIVE_NUM";
    }

    /**
     * 搜索岗位 总数
     *
     * @param map
     * @return
     */
    public String searchPosTotal(Map<String, Object> map) {
        Long projectId = Long.valueOf(map.get("projectId").toString());
        String posName = map.get("name").toString();
        String sql = "select count(jfoi.figure_id)" +
                "  from jecn_flow_org_image jfoi,jecn_flow_org jfo" +
                "  where jfoi.figure_type='PositionFigure' and jfoi.org_id = jfo.org_id" +
                "  and jfo.projectid=" + projectId + " and jfo.del_state=0";
        if (StringUtils.isNotBlank(posName)) {
            sql = sql + " and jfoi.figure_text like '%" + posName + "%'";
        }
        sql +=" order by jfoi.sort_id asc";
        return sql;
    }

    /**
     * 搜索岗位组 总数
     *
     * @param map
     * @return
     */
    public String searchPosGroupTotal(Map<String, Object> map) {
        String posName = map.get("name").toString();
        String sql = "select count(jfoi.id)" +
                "  from jecn_position_group jfoi";
        if (StringUtils.isNotBlank(posName)) {
            sql = sql + " where  jfoi.name like '%" + posName + "%'";
        }
        return sql;
    }

    /**
     * 搜索岗位组
     *
     * @param map
     * @return
     */
    public String searchPosGroup(Map<String, Object> map) {
        String posName = map.get("name").toString();
        String sql = "select jfoi.id figureId,jfoi.name figureName" +
                "  from jecn_position_group jfoi";
        if (StringUtils.isNotBlank(posName)) {
            sql = sql + " where jfoi.name like '%" + posName + "%'";
        }
        return sql;
    }


    /**
     * 搜索岗位 统计
     *
     * @param map
     * @return
     */
    public String searchPosStatistics(Map<String, Object> map) {
        Long projectId = Long.valueOf(map.get("projectId").toString());
        List<String> ids = map.get("ids") != null ? (List<String>) map.get("ids") : new ArrayList<>();
        String posName = map.get("name").toString();
        String sql = "SELECT OI.Figure_Id             figure_id," +
                "       OI.FIGURE_TEXT           figure_text," +
                "       OI.ORG_ID                org_id," +
                "       ORG.ORG_NAME             org_name," +
                "       PROCESS_ROLE.FIGURE_ID   AS ROLE_ID," +
                "       PROCESS_ROLE.FIGURE_TEXT AS ROLE_NAME," +
                "       PROCESS_ROLE.ROLE_RES," +
                "       JFS.FLOW_ID," +
                "       JFS.FLOW_NAME," +
                "       ACTIVE.FIGURE_ID         AS ACTIVE_ID," +
                "       ACTIVE.FIGURE_TEXT       AS ACTIVE_NAME," +
                "       ACTIVE.ACTIVITY_ID       AS ACTIVE_NUM," +
                "       ACTIVE.Target_Value      AS TARGET_VALUE," +
                "       ACTIVE.STATUS_VALUE      AS STATUS_VALUE" +
                "  FROM JECN_FLOW_ORG_IMAGE OI" +
                "  left JOIN PROCESS_STATION_RELATED POS ON OI.FIGURE_ID = POS.FIGURE_POSITION_ID AND POS.TYPE = '0'" +
                "  LEFT JOIN JECN_FLOW_STRUCTURE_IMAGE PROCESS_ROLE" +
                "    ON PROCESS_ROLE.FIGURE_ID = POS.FIGURE_FLOW_ID" +
                "  LEFT JOIN JECN_ROLE_ACTIVE JRA" +
                "    ON JRA.FIGURE_ROLE_ID = PROCESS_ROLE.FIGURE_ID" +
                "  LEFT JOIN JECN_FLOW_STRUCTURE_IMAGE ACTIVE" +
                "    ON ACTIVE.FIGURE_ID = JRA.FIGURE_ACTIVE_ID" +
                "  LEFT JOIN JECN_FLOW_STRUCTURE JFS" +
                "    ON PROCESS_ROLE.FLOW_ID = JFS.FLOW_ID" +
                "   AND JFS.DEL_STATE = 0" +
                "  LEFT JOIN JECN_REF_INDICATORS JRI" +
                "    ON JRI.ACTIVITY_ID = ACTIVE.FIGURE_ID" +
                "  LEFT JOIN JECN_FLOW_ORG ORG" +
                "    ON ORG.ORG_ID = OI.ORG_ID ";
        sql += " WHERE " + (!ids.isEmpty() ? " OI.FIGURE_ID in " + SqlCommon.getStrs(ids) : " OI.FIGURE_TEXT like '%" + posName + "%'");
        return sql;
    }

    /**
     * 搜索岗位组 统计
     *
     * @param map
     * @return
     */
    public String searchPosGroupStatistics(Map<String, Object> map) {
        Long projectId = Long.valueOf(map.get("projectId").toString());
        String posGroupName = map.get("name").toString();
        List<String> ids = map.get("ids") != null ? (List<String>) map.get("ids") : new ArrayList<>();
        String sql = "SELECT GR.ID," +
                "       GR.NAME," +
                "       GR.EXPLAIN," +
                "       OI.FIGURE_ID," +
                "       OI.FIGURE_TEXT," +
                "       ROLE_INFO.FIGURE_ID   AS ROLE_ID," +
                "       ROLE_INFO.FIGURE_TEXT AS ROLE_NAME," +
                "       ROLE_INFO.ROLE_RES," +
                "       JFS.FLOW_ID," +
                "       JFS.FLOW_NAME," +
                "       IMG.FIGURE_ID         AS ACTIVE_ID," +
                "       IMG.FIGURE_TEXT       AS ACTIVE_NAME," +
                "       IMG.ACTIVITY_ID       AS ACTIVE_NUM," +
                "       IMG.TARGET_VALUE         AS TARGET_VALUE," +
                "       IMG.STATUS_VALUE         AS STATUS_VALUE" +
                "  FROM JECN_POSITION_GROUP GR" +
                "  LEFT JOIN JECN_POSITION_GROUP_R GRR" +
                "    ON GRR.GROUP_ID = GR.ID" +
                "  LEFT JOIN JECN_FLOW_ORG_IMAGE OI" +
                "    ON OI.FIGURE_ID = GRR.FIGURE_ID" +
                "  LEFT JOIN PROCESS_STATION_RELATED POS" +
                "    ON GR.ID = POS.FIGURE_POSITION_ID" +
                "   AND POS.TYPE = '1'" +
                "  LEFT JOIN JECN_FLOW_STRUCTURE_IMAGE PROCESS_ROLE" +
                "    ON PROCESS_ROLE.FIGURE_ID = POS.FIGURE_FLOW_ID" +
                "  LEFT JOIN JECN_FLOW_STRUCTURE JFS" +
                "    ON PROCESS_ROLE.FLOW_ID = JFS.FLOW_ID" +
                "   AND JFS.DEL_STATE = 0" +
                "     LEFT JOIN JECN_FLOW_STRUCTURE_IMAGE ROLE_INFO " +
                "    ON ROLE_INFO.FIGURE_ID = POS.FIGURE_FLOW_ID " +
                "    AND ROLE_INFO.FLOW_ID = JFS.FLOW_ID" +
                " LEFT JOIN JECN_ROLE_ACTIVE JRA " +
                "   ON JRA.FIGURE_ROLE_ID = PROCESS_ROLE.FIGURE_ID " +
                "   LEFT JOIN JECN_FLOW_STRUCTURE_IMAGE IMG " +
                "ON IMG.FIGURE_ID = JRA.FIGURE_ACTIVE_ID  AND JFS.FLOW_ID = IMG.FLOW_ID" +
                "   AND IMG.FIGURE_TYPE IN " + SqlCommon.getOperationPostGString();
        sql += " WHERE  GR.IS_DIR = 0 AND" + (!ids.isEmpty() ? "  gr.id in " + SqlCommon.getStrs(ids) : " gr.name like '%" + posGroupName + "%'");
        return sql;
    }


    /**
     * 搜索岗位
     *
     * @param map
     * @return
     */
    public String searchPos(Map<String, Object> map) {
        Long projectId = Long.valueOf(map.get("projectId").toString());
        String name = map.get("name").toString();
        String sql = "select jfoi.figure_id,jfoi.figure_text,jfo.org_id,jfo.org_name" +
                "  from jecn_flow_org_image jfoi,jecn_flow_org jfo" +
                "  where jfoi.figure_type='PositionFigure' and jfoi.org_id = jfo.org_id" +
                "  and jfo.projectid=" + projectId + " and jfo.del_state=0";
        if (StringUtils.isNotBlank(name)) {
            sql = sql + " and jfoi.figure_text like '%" + name + "%'";
        }
        return sql;
    }

    /**
     * 搜索组织 总数
     *
     * @param map
     * @return
     */
    public String searchOrgTotal(Map<String, Object> map) {
        Long projectId = Long.valueOf(map.get("projectId").toString());
        String name = map.get("name").toString();
        String sql = "select count(jfo.org_id)" +
                "  from jecn_flow_org jfo" +
                "  left join jecn_flow_org pub on pub.org_id=jfo.per_org_id" +
                "  where jfo.projectid=" + projectId + " and jfo.del_state=0";
        if (StringUtils.isNotBlank(name)) {
            sql = sql + " and jfo.org_name like '%" + name + "%'";
        }
        return sql;
    }

    /**
     * 搜索组织
     *
     * @param map
     * @return
     */
    public String searchOrg(Map<String, Object> map) {
        Long projectId = Long.valueOf(map.get("projectId").toString());
        String name = map.get("name").toString();
        String sql = "select jfo.org_id,jfo.org_name,pub.org_id as p_id,pub.org_name as p_name" +
                "  from jecn_flow_org jfo" +
                "  left join jecn_flow_org pub on pub.org_id=jfo.per_org_id" +
                "  where jfo.projectid=" + projectId + " and jfo.del_state=0";
        if (StringUtils.isNotBlank(name)) {
            sql = sql + " and jfo.org_name like '%" + name + "%'";
        }
        return sql;
    }

    /**
     * 组织清单
     *
     * @param map
     * @return
     */
    public String fetchInventory(Map<String, Object> map) {
        String sql = "select * from (" +
                " SELECT DISTINCT MO.ORG_ID," +
                "       MO.PER_ORG_ID," +
                "       MO.ORG_NAME," +
                "       MO.T_LEVEL,JFST.FLOW_ID," +
                "       CASE WHEN JFS.FLOW_ID IS NULL THEN 0 ELSE 1 END AS PUB_STATE," +
                "       JFST.FLOW_NAME," +
                "       JFST.FLOW_ID_INPUT," +
                "       CASE WHEN JFBI.TYPE_RES_PEOPLE = 1 THEN JFOI.FIGURE_TEXT" +
                "       WHEN JFBI.TYPE_RES_PEOPLE = 0 THEN RES_P.TRUE_NAME" +
                "       END AS RES_PEOPLE," +
                "       GUARDIAN_P.TRUE_NAME AS GUARDIAN_PEOPLE," +
                "       JTHN.DRAFT_PERSON AS DRAFT_PEOPLE," +
                "       JTHN.VERSION_ID AS VERSION_ID," +
                "       JFST.PUB_TIME AS PUB_TIME," +
                "       JFBI.EXPIRY,JTHN.NEXT_SCAN_DATE," +
                "       CASE WHEN JFS.FLOW_ID IS NULL AND JTBN.STATE IS NOT NULL THEN  1" +
                "       WHEN JFS.FLOW_ID IS NOT NULL THEN 2 ELSE 0 END AS TYPE_BY_DATA," +
                "       CASE WHEN JFS.FLOW_ID IS NOT NULL AND JFAOT.ID IS NOT NULL THEN '1'" +
                "       WHEN JFS.FLOW_ID IS NOT NULL THEN '2' END AS ORG_TYPE," +
                "       MO.VIEW_SORT," +
                "       JFST.VIEW_SORT AS FLOW_VIEW_SORT" +
                "  FROM (" + InventorySqlProvider.getOrgs(map) + ") MO" +
                "  LEFT JOIN JECN_FLOW_RELATED_ORG_T JFROT ON MO.ORG_ID = JFROT.ORG_ID" +
                "  LEFT JOIN JECN_FLOW_STRUCTURE_T JFST ON JFROT.FLOW_ID=JFST.FLOW_ID AND JFST.DEL_STATE=0" +
                "  LEFT JOIN JECN_FLOW_BASIC_INFO_T JFBI ON JFST.FLOW_ID = JFBI.FLOW_ID" +
                "  LEFT JOIN JECN_FLOW_ORG_IMAGE JFOI ON JFBI.TYPE_RES_PEOPLE=1 AND JFBI.RES_PEOPLE_ID = JFOI.FIGURE_ID" +
                "  LEFT JOIN JECN_USER RES_P ON JFBI.TYPE_RES_PEOPLE=0 AND JFBI.RES_PEOPLE_ID = RES_P.PEOPLE_ID" +
                "  LEFT JOIN JECN_USER GUARDIAN_P ON JFBI.WARD_PEOPLE_ID = GUARDIAN_P.PEOPLE_ID" +
                "  LEFT JOIN JECN_TASK_HISTORY_NEW JTHN ON JFST.HISTORY_ID = JTHN.ID" +
                "  LEFT JOIN JECN_FLOW_STRUCTURE JFS ON JFST.FLOW_ID = JFS.FLOW_ID" +
                "  LEFT JOIN JECN_TASK_BEAN_NEW JTBN ON JTBN.R_ID = JFST.FLOW_ID AND JTBN.TASK_TYPE = 0" +
                "     AND JTBN.IS_LOCK = 1 AND JTBN.STATE <> 5 " +
                "  LEFT JOIN JECN_FLOW_APPLY_ORG_T JFAOT ON JFST.FLOW_ID=JFAOT.RELATED_ID AND JFAOT.ORG_ID=JFROT.ORG_ID" +
                " )A" +
                " ORDER BY a.view_sort,a.FLOW_VIEW_SORT ASC ";
        return sql;
    }

    public String findOrgPaterStrByOrgId(Long orgId) {
        String sql = "SELECT T.ORG_ID,T.ORG_NAME" +
                "  FROM JECN_FLOW_ORG ORG" +
                " INNER JOIN JECN_FLOW_ORG T" +
                CommonSqlTPath.INSTANCE.getSubStringSqlByDBType("ORG") +
                " WHERE ORG.ORG_ID = #{orgId} AND T.ORG_ID <> ORG.ORG_ID  ORDER BY T.T_PATH DESC";
        return sql;
    }
}
