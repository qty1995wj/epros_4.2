package com.jecn.epros.server.action.web.login.ad.hisense;

import java.util.Hashtable;

import javax.naming.AuthenticationException;
import javax.naming.Context;
import javax.naming.NamingEnumeration;
import javax.naming.NamingException;
import javax.naming.directory.DirContext;
import javax.naming.directory.InitialDirContext;
import javax.naming.directory.SearchControls;
import javax.naming.directory.SearchResult;
import javax.naming.ldap.LdapContext;

import org.apache.commons.lang.StringUtils;

import com.jecn.epros.server.action.web.login.LoginAction;
import com.jecn.epros.server.action.web.login.ad.JecnAbstractADLoginAction;
import com.jecn.epros.server.common.JecnCommon;

/**
 * 海信登录处理类
 * 
 * @author weidp
 * 
 */
public class HisenseLoginAction extends JecnAbstractADLoginAction {
	/** 用户Ldap账号 */
	private String ldapAccount = null;
	/** 单点登录标识 */
	private String hissenSSO = "";

	public HisenseLoginAction(LoginAction loginAction) {
		super(loginAction);
		hissenSSO = this.loginAction.getRequest().getParameter("hissenSSO");
	}

	@Override
	public String login() {
		try {
			// 单点登录名称
			String adLoginName = this.loginAction.getLoginName();
			// 单点登录密码
			String adPassWord = this.loginAction.getPassword();

			log.info(" adLoginName = " + adLoginName + " adPassWord = " + adPassWord);
			if (StringUtils.isNotBlank(adLoginName) && StringUtils.isNotBlank(adPassWord)) {
				return loginAction.userLoginGen(true);
			}
			// admin 登录处理
			if ("admin".equals(loginAction.getLoginName())) {
				return loginAction.userLoginGen(true);
			}
			this.loginAction.setIsSSo(true);
			// 海信单点登录
			if (getLogin()) {
				return doHisenseSSO();
			}
			// Ldap 登录验证
			DirContext dirContext = doLdapAuthority();
			// 未通过验证时提示账号密码错误
			if (dirContext == null) {
				loginAction.addFieldError("loginMessage", loginAction.getText("nameOrPasswordError"));
				return LoginAction.INPUT;
			}
			// 通过验证后获取用户employNumber 进行不验证密码登录
			return doEprosLogin(getLoginNameFromLdap(dirContext));
		} catch (Exception e) {
			log.error("获取用户信息异常", e);
			return LoginAction.INPUT;
		}
	}

	/**
	 * 登录至Epros（不验证密码）
	 * 
	 * @author weidp
	 * @date 2014-7-9 上午11:38:46
	 * @param loginName
	 * @return
	 */
	private String doEprosLogin(String loginName) {
		log.info("用户名：" + loginName);
		if (JecnCommon.isNullOrEmtryTrim(loginName)) {
			loginAction.addFieldError("loginMessage", loginAction.getText("noAuthToLogin"));
			return LoginAction.INPUT;
		}
		loginAction.setLoginName(loginName);
		return loginAction.userLoginGen(false);
	}

	/**
	 * 海信单点登录
	 * 
	 * @author weidp
	 * @date 2014-7-9 上午11:30:15
	 */
	private String doHisenseSSO() {
		log.info("进入单点登录");
		String loginName = this.loginAction.getRequest().getHeader("employeeNumber");
		return doEprosLogin(loginName);
	}

	/**
	 * 从Ldap获取用户的员工编号（"登录名"）
	 * 
	 * @author weidp
	 * @date 2014-7-9 上午10:34:28
	 * @return
	 */
	private String getLoginNameFromLdap(DirContext dirContext) {
		LdapContext ctx = null;
		String eprosLoginName = "";
		if (dirContext == null) {
			log.info("dirContext为空");
			return eprosLoginName;
		}

		try {
			// 组基准DN
			final String groupDN = JecnHisenseAfterItem.EPROS_GROUP_DN;
			SearchControls searchCtrl = new SearchControls();
			searchCtrl.setSearchScope(SearchControls.SUBTREE_SCOPE);
			// 过滤条件
			String filter = "(uniqueMember=uid=" + ldapAccount + ",*)";
			// 搜索epros用户组中是否存在该成员，如果存在返回该组(只会返回一条或零条结果)
			NamingEnumeration<SearchResult> enu = dirContext.search(groupDN, filter, searchCtrl);
			// 是否存在于组中
			boolean isUserInGroup = false;
			while (enu.hasMore()) {
				isUserInGroup = true;
			}
			if (!isUserInGroup) {
				return eprosLoginName;
			}

			// 用户组准DN
			final String peopleDN = JecnHisenseAfterItem.PEOPLE_DN;
			ctx = (LdapContext) dirContext.lookup(peopleDN);
			// 获取用户员工编号（Epros登录名）
			eprosLoginName = ctx.getAttributes("uid=" + ldapAccount + "").get("employeeNumber").get().toString();
		} catch (NamingException e) {
			log.error("获取Ldap用户登录名异常：", e);
		} finally {
			try {
				if (ctx != null) {
					ctx.close();
				}
				if (dirContext != null) {
					dirContext.close();
				}
			} catch (NamingException ex) {
				log.error("释放资源出现异常：", ex);
			}
		}
		return eprosLoginName;
	}

	/**
	 * 进行ldap权限验证
	 * 
	 * @author weidp
	 * @date 2014-7-9 上午10:32:03
	 * @return
	 */
	private DirContext doLdapAuthority() {
		// 获取用户输入的Ldap账号
		ldapAccount = loginAction.getLoginName();
		// 获取用户输入的密码
		String ldapPassword = loginAction.getPassword();
		ldapAccount = "uid=" + ldapAccount + ",ou=applications,o=hisense.com,o=isp";
		DirContext dirContext = null;
		log.info("登录名：" + ldapAccount);
		try {
			dirContext = new InitialDirContext(getLdapEnv(ldapAccount, ldapPassword));
		} catch (AuthenticationException ex) {
			log.error("Ldap权限验证失败：", ex);
		} catch (Exception e) {
			log.error("连接Ldap服务器出现异常：", e);
		}
		return dirContext;
	}

	/**
	 * 获取Ldap环境
	 * 
	 * @author weidp
	 * @date 2014-7-9 上午10:35:53
	 * @param account
	 *            账户
	 * @param password
	 *            密码
	 * @return
	 */
	private Hashtable<String, String> getLdapEnv(String account, String password) {
		if (JecnCommon.isNullOrEmtryTrim(account) || JecnCommon.isNullOrEmtryTrim(password)) {
			throw new NullPointerException("Ldap 账号或密码为空！");
		}
		Hashtable<String, String> env = new Hashtable<String, String>();
		env.put(Context.INITIAL_CONTEXT_FACTORY, JecnHisenseAfterItem.LDAP_CONTEXT_FACTORY);
		env.put(Context.PROVIDER_URL, JecnHisenseAfterItem.LDAP_URL);
		env.put(Context.SECURITY_AUTHENTICATION, JecnHisenseAfterItem.AUTH_TYPE);
		env.put(Context.SECURITY_PRINCIPAL, account);
		env.put(Context.SECURITY_CREDENTIALS, password);
		return env;
	}

	/**
	 * 
	 * 是否进行单点登录
	 * 
	 * @return true为登录，false为不需要
	 */
	private boolean getLogin() {
		if ("hissen".equals(hissenSSO)) {
			return true;
		}
		return false;
	}
}
