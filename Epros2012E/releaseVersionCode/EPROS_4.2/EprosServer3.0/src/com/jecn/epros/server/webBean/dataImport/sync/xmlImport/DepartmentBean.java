package com.jecn.epros.server.webBean.dataImport.sync.xmlImport;

import java.util.ArrayList;
import java.util.List;

public class DepartmentBean {
	private Long id;
	private String deptId;
	private String parentDeptId;
	private String deptName;
	private Long pid;
	private List<PositionBean> positionList = new ArrayList<PositionBean>();

	public String getDeptId() {
		return deptId;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public void setDeptId(String deptId) {
		this.deptId = deptId;
	}

	public String getDeptName() {
		return deptName;
	}

	public void setDeptName(String deptName) {
		this.deptName = deptName;
	}

	public Long getPid() {
		return pid;
	}

	public void setPid(Long pid) {
		this.pid = pid;
	}

	public List<PositionBean> getPositionList() {
		return positionList;
	}

	public void setPositionList(List<PositionBean> positionList) {
		this.positionList = positionList;
	}

	public String getParentDeptId() {
		return parentDeptId;
	}

	public void setParentDeptId(String parentDeptId) {
		this.parentDeptId = parentDeptId;
	}
	
}
