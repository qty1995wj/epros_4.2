package com.jecn.webservice.jinke;

import javax.jws.WebMethod;
import javax.jws.WebService;

/**
 * 金科 - 人员组织同步接口
 * 
 * @author xiaohu
 * 
 */
@WebService(endpointInterface = "jkOASyncService")
public interface IJKOASyncService {

	@WebMethod(operationName = "saveSyncDeptAndUsers")
	public String saveSyncDeptAndUsers(String userJson);
}
