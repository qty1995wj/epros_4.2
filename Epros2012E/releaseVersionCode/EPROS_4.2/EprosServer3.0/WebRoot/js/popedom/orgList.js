function orgListGrid(json,isPop) {
	orgListGridStore = new Ext.data.JsonStore( {
		autoLoad : false,
		data : json.dataList,
		fields : json.stores
	});

	orgListGridColumnModel = new Ext.grid.ColumnModel( {
		defaults : {
			align : 'center',
			sortable : true,
			menuDisabled : false,
			width : 120,
			flex :100
		},
		columns : json.columns
	});
	if (isPop == true || isPop == "true") {
		processListHeight = document.documentElement.clientHeight - 85;
	} else {
		processListHeight = document.documentElement.clientHeight - 150;
	}
	
	//解决IE6滚动条不显示问题
    if (Ext.isIE6 || Ext.isIE7) { // 判断ie6、7浏览器
		if (Ext.get("relationProcess_id") != null) {  //由于两个组织清单tabpanel面板的ID不相同，所以需要作如下分支判断
			extWidth = Ext.get("relationProcess_id").getWidth();
		} else {
			extWidth = Ext.get("i_orgList").getWidth();
		}
	} else {
		extWidth = '100%';
	}
	orgListGridPanel = new Ext.grid.GridPanel( {
		id : "orgListGridPanel_id",
		renderTo : 'orgListShow_id',
		height : processListHeight,
		width:extWidth,
		autoScroll : true,//滚动条
		stripeRows : true,//显示行的分隔符
		columnLines : true,//显示列的分隔线
		enableColumnMove : false,
		//		viewConfig : {
		//			//		 让列的宽度和grid一样	
		//		forceFit : true
		//	},
		frame : false,
		store : orgListGridStore,
		colModel : orgListGridColumnModel
	})
}

function orgListJson(orgId, isShow,isPop) {
	Ext.Ajax.request( {
		url : 'findOrgList.action',
		method : 'POST',
		timeout : 1000000,
		params : {
			orgId : orgId,
			showAll : isShow
		},
		success : function(response, options) {
			// 获取response中的数据
		var json = Ext.util.JSON.decode(response.responseText);
		orgListGrid(json,isPop);
		document.getElementById("allOrgTotal").innerHTML = "(" + json.allTotal
				+ ")";
		document.getElementById("allFlowTotal").innerHTML = "("
				+ json.flowTotal + ")";
		document.getElementById("noPubFlowTotal").innerHTML = "("
				+ json.noPubTotal + ")";
		document.getElementById("approveFlowTotal").innerHTML = "("
				+ json.approveTotal + ")";
		document.getElementById("pubFlowTotal").innerHTML = "(" + json.pubTotal
				+ ")";

		document.getElementById("orgId").value = orgId;
		document.getElementById("isPop").value = isPop;
		
	},
	// 失败调用
		failure : function(response, options) {
			alert(i_readOrgListException)
		}
	});
}
// 列内容渲染器（后台生成列时附加）
function orgProcessListNameRenderer(value, cellmeta, record) {
	return orgReturnConnect(record.data['flowId'], record.data['typeByData'],
			value);
}

function orgReturnColor(typeByData) {
	if (typeByData == 0) {
		return "#FB1A1C;";
	} else if (typeByData == 1) {
		return "#e7a100;";
	} else if (typeByData == 2) {
		return "#00c317;";
	}
}

function orgReturnHref(flowId){
	return "process.action?type=process&flowId="+flowId;
}


function orgReturnConnect(flowId,typeByData,value){
	if(typeByData==2){
		return "<div  ext:qtip='"
			+ value+ "'><a target='_blank' class='a-operation' href='"
			+ orgReturnHref(flowId)
			+ "' style='color:"
			+ orgReturnColor(typeByData)
			+ "'>" + value + "</a></div>";
	}else{
		return "<div  ext:qtip='"
			+ value
			+ "' style='color:"
			+ orgReturnColor(typeByData)
			+ "'>" + value + "</div>";
	}
}

// 显示全部
function showAllOrgList() {
	var orgId = document.getElementById("orgId").value;
	var isPop = document.getElementById("isPop").value;
	//父页面隐藏域
	var _isPop= document.getElementById("_isPop");
	//判断是否是被其他页面嵌入进来的
	if(_isPop!=null && _isPop!=undefined){
		isPop=_isPop.value;
	}
	orgListGridPanel.destroy();
	orgListJson(orgId, true, isPop);
}

// 下载
function downloadOrgList() {
	var orgId = document.getElementById("orgId").value;
	window.open("downLoadOrgList.action?orgId=" + orgId);
	//	window.location.href = "downLoadOrgList.action?orgId=" + orgId;
}
