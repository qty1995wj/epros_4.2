<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	</head>
	<body>
	
	<div id="div" class="div">
			<div id="matchSearch" class="divBorder">
				<!-- 岗位匹配岗位搜索 -->
				<div id="matchSearchText" style="overflow: hidden;" align="center">
				    <table width="70%">
						<tr>
							<td height="25px" width="15%">
								<!-- 岗位名称： -->
							<s:text name="positionNameC"></s:text>
							</td>
							<td>
								<input id="nomatchProposName" name="nomatchProposName" style="width: 72%; vertical-align: middle;" maxLength="32" />
							</td>
						</tr>
					</table>
				</div>

				<div id="searchBut" align="center">
					<img style="cursor: pointer;"
						src="${basePath}images/iParticipateInProcess/search.gif"
						onclick="matchSearch()" />
					<img style="cursor: pointer;"
						src="${basePath}images/iParticipateInProcess/reset.gif"
						onclick="taskSearchNomatchPro()" />
				</div>
			</div>
	<!-- 添加岗位匹配 -->
		<div id="noMatchProcessGrid_id" >
		</div>
	</div>
		
	</body>
</html>
