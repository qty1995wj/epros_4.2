package com.jecn.epros.server.action.web.login.ad.sanfu;

import java.io.IOException;

import org.apache.commons.httpclient.DefaultHttpMethodRetryHandler;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpStatus;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.httpclient.params.HttpMethodParams;
import org.apache.commons.lang.StringUtils;

import com.jecn.epros.server.action.web.login.LoginAction;
import com.jecn.epros.server.action.web.login.ad.JecnAbstractADLoginAction;
import com.jecn.epros.server.common.JecnCommon;

/**
 * 
 * 三福百货：单点登录
 * 
 * 获取单点登录key值 （三福点的登录不同的系统分配不同的key）
 * 
 * @author ZHAGNXH
 * 
 */
public class SanfuReadSSOAction extends JecnAbstractADLoginAction {
	public SanfuReadSSOAction(LoginAction loginAction) {
		super(loginAction);
	}

	/**
	 * 
	 * 单点登录入口
	 * 
	 * @return String
	 */
	public String login() {
		try {
			// request 获取连接传入数据
			String adLoginName = loginAction.getRequest().getParameter("Para");
			if (StringUtils.isBlank(adLoginName)) {// 单点登录名称为空，执行普通登录路径
				return loginAction.loginGen();
			}
			// 通过单点登录验证、获取员工编号
			adLoginName = getUserNumber(adLoginName);
			// 验证登录
			return this.loginByLoginName(adLoginName);

		} catch (Exception e) {
			log.error("获取用户信息异常", e);
			return LoginAction.INPUT;
		}
	}

	/**
	 * 
	 * 单点登录解密
	 * 
	 * @return String 解密后获取人员编号
	 */
	private String getUserNumber(String employeeid) {
		// 获取单点登录key值
		String strKey = JecnSanfuAfterItem.value;

		log.info("获取单点登录key值！" + strKey);

		// 解密
		String strDecrypt = AESUtils.decrypt(employeeid, strKey);
		if (JecnCommon.isNullOrEmtryTrim(strDecrypt)) {
			log.error("三福单点登录信息解密失败！ employeeid=" + employeeid);
			return null;
		}
		//
		String[] strList = strDecrypt.split("&");

		// 随机数GUID
		String key = null;

		for (int i = 0; i < strList.length; i++) {
			String paraitemvalue[] = strList[i].split("=");
			if ("Employeeid".equals(paraitemvalue[0].trim())) {// 字符串截取‘Employeeid=06400007’
				employeeid = paraitemvalue[1].trim();
			} else if ("key".equalsIgnoreCase(paraitemvalue[0].trim())) {
				key = paraitemvalue[1].trim();
			}
		}

		if (employeeid == null || key == null) {
			log.error("获取用户编号或用户登录随机数失败" + "employeeid = " + employeeid
					+ "key = " + key);
			return null;
		}

		// 员工编号
		String userNumber = employeeid;

		// 验证用户随机数 true 用户信息验证成功，false 用户验证失败
		boolean isLoginResult = isLongResult(userNumber, key);

		if (isLoginResult) {
			return userNumber;
		} else {
			return null;
		}
	}

	/**
	 * 验证用户登录时请求BMP系统 用户随机数和员工编号是否成功
	 * 
	 * @param employeeid
	 *            String 用户员工编号
	 * @param key
	 *            String 用户随机数
	 * @return boolean :true 用户信息验证成功，false 用户验证失败
	 */
	private boolean isLongResult(String employeeid, String key) {
		// （1）构造HttpClient的实例
		HttpClient httpClient = new HttpClient();

		log.info("用户随机数和员工编号是否成功 employeeid = " + employeeid + ";key = " + key);
		// （2）创建POST方法的实例http://172.28.9.202:9001/greenOffice/hmrcbRoute/CheckRandom.aspx?
		String url = JecnSanfuAfterItem.adURL + "key=" + key + "&employeeid="
				+ employeeid;
		log.info("url =" + url);

		PostMethod postMethod = new PostMethod(url);

		log.info("创建post方法实例成功 ！！！ ");

		// 使用系统提供的默认的恢复策略
		postMethod.getParams().setParameter(HttpMethodParams.RETRY_HANDLER,
				new DefaultHttpMethodRetryHandler());

		String responseBody = null;
		try {
			// （4）执行postMethod
			int statusCode = httpClient.executeMethod(postMethod);

			if (statusCode == HttpStatus.SC_OK) {
				// （6）读取内容
				log.info("发送成功！　请求结果 ：" + postMethod.getResponseBodyAsString());
				responseBody = postMethod.getResponseBodyAsString();
			} else {
				log.error("http请求失败，状态为:  " + statusCode);
				return false;
			}

			// （7） 处理内容
			if ("SSO_SUCCESS".equals(responseBody.substring(0, 11))) {
				log.info("请求验证成功！！！" + responseBody);
				return true;
			}
			log.error("请求验证失败！！！" + responseBody);

		} catch (IOException e) {
			log.error("用户登录BMP请求随机数验证失败！", e);
		} finally {
			if (postMethod != null) {
				postMethod.releaseConnection();
			}
		}
		return false;
	}

}
