package com.jecn.epros.domain.propose;

import org.springframework.web.multipart.MultipartFile;

/**
 * 编辑合理化建议的bean
 *
 * @author user
 */
public class ProposeEdit {

    /**
     * 合理化建议的id
     **/
    private String proposeId;
    /**
     * 合理化建议内容
     **/
    private String content;
    /**
     * 附件
     **/
    private MultipartFile file;
    /**
     * 是否删除附件
     **/
    private Boolean delFile = false;
    /**
     * 0设置的处理人可见1所有人可见
     **/
    private Integer showType;


    public Integer getShowType() {
        return showType;
    }

    public void setShowType(Integer showType) {
        this.showType = showType;
    }

    public String getProposeId() {
        return proposeId;
    }

    public void setProposeId(String proposeId) {
        this.proposeId = proposeId;
    }

    public Boolean getDelFile() {
        return delFile;
    }

    public void setDelFile(Boolean delFile) {
        this.delFile = delFile;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public MultipartFile getFile() {
        return file;
    }

    public void setFile(MultipartFile file) {
        this.file = file;
    }

}
