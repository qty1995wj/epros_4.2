package epros.designer.gui.publish;

import epros.designer.tree.JecnTreeNode;
import epros.designer.util.JecnProperties;

/*******************************************************************************
 * 制度模板发布
 * 
 * @author Administrator 2012-10-23
 */
public class RuleModeFilePublishDialog extends BasePublishDialog {

	public RuleModeFilePublishDialog(JecnTreeNode treeNode) {
		super(treeNode);
		if (!"ruleModeFile".equals(treeBean.getTreeNodeType().toString())) {
			log.error("RuleModeFilePublishDialog！　treeBean.getTreeNodeType()　＝"
					+ treeBean.getTreeNodeType());
			return;
		}
	}

	public void initialize() {
		type = 2;
		prfName = JecnProperties.getValue("ruleNameC");
		strTitle = JecnProperties.getValue("rulePubI");
	}
}
