package com.jecn.epros.server.dao.process.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import com.jecn.epros.server.bean.process.JecnFlowKpiName;
import com.jecn.epros.server.bean.process.JecnFlowKpiNameT;
import com.jecn.epros.server.common.AbsBaseDao;
import com.jecn.epros.server.common.JecnCommonSql;
import com.jecn.epros.server.dao.process.IProcessKPIDao;

public class ProcessKPIDaoImpl extends AbsBaseDao<JecnFlowKpiNameT, Long>
		implements IProcessKPIDao {

	@Override
	public List<JecnFlowKpiNameT> getJecnFlowKpiNameTList(Long flowId)
			throws Exception {
		String hql = "from JecnFlowKpiNameT where flowId=?";
		return this.listHql(hql, flowId);
	}

	@Override
	public List<JecnFlowKpiName> getJecnFlowKpiNameList(Long flowId)
			throws Exception {
		String hql = "from JecnFlowKpiName where flowId=?";
		return this.listHql(hql, flowId);
	}

	@Override
	/** * 0 KPI的名称
	1 类型
	2 目标值
	3 kpi定义
	4 kpi统计方法
	5 数据统计时间频率
	6KIP值单位名称
	7 数据获取方式
	8 相关度
	9 指标来源
	10 数据提供者名称
	11 支撑的一级指标内容
	12 目的
	13 测量点
	14 统计周期
	15 说明
	16 IT系统
	17 KPI_AND_ID
	 */
	public List<Object[]> getKpiNameList(Long flowId, boolean isPub)
			throws Exception {
		String temp = "";
		if (!isPub) {
			temp = "_T";
		}
		String sql = "select K.KPI_NAME,K.KPI_TYPE,K.KPI_TARGET,K.KPI_DEFINITION,"
				+ "K.KPI_STATISTICAL_METHODS,K.KPI_HORIZONTAL,K.KPI_VERTICAL,K.KPI_DATA_METHOD,K.KPI_RELEVANCE,K.KPI_TARGET_TYPE,"
				+ "U.TRUE_NAME,F.TARGER_CONTENT,K.KPI_PURPOSE,K.KPI_POINT,K.KPI_PERIOD,K.KPI_EXPLAIN ,'' ,K.KPI_AND_ID from JECN_FLOW_KPI_NAME"
				+ temp
				+ " K"
				+ "  LEFT join JECN_USER U on U.PEOPLE_ID = K.KPI_DATA_PEOPEL_ID"
				+ "  LEFT join JECN_FIRST_TARGER F on F.ID = K.FIRST_TARGER_ID"
				+ "  where K.FLOW_ID=?";
		return this.listNativeSql(sql, flowId);
	}

	/**
	 * 流程KPIIT系统
	 */
	public List<Object[]> getKPIITSysTem(Long flowId,Long kpiAndId, boolean isPub) {
		String temp = "";
		if (!isPub) {
			temp = "_T";
		}
		String sql = "SELECT TOOL.FLOW_SUSTAIN_TOOL_DESCRIBE ,''"
				+ " FROM JECN_SUSTAIN_TOOL_CONN"
				+ temp
				+ " CONN"
				+ " INNER JOIN JECN_FLOW_KPI_NAME"
				+ temp
				+ " KPI ON KPI.KPI_AND_ID = CONN.RELATED_ID"
				+ " INNER JOIN JECN_FLOW_SUSTAIN_TOOL TOOL ON TOOL.FLOW_SUSTAIN_TOOL_ID =CONN.SUSTAIN_TOOL_ID"
				+ " WHERE KPI.FLOW_ID = ? and  KPI.KPI_AND_ID=? ";
		return this.listNativeSql(sql, flowId,kpiAndId);
	}

	// 流程关键评测指标数据 0 名称 1统计方法 2 统计频率 3 数据提供者
	public List<Object[]> nongfushanquan_getKpiNameList(Long flowId,
			boolean isPub) throws Exception {
		String sql = null;
		if (isPub) {
			sql = "SELECT JFK.KPI_NAME,JFK.KPI_STATISTICAL_METHODS,JFK.KPI_HORIZONTAL,JU.TRUE_NAME FROM JECN_FLOW_KPI_NAME_T JFK LEFT JOIN JECN_USER JU ON  JU.PEOPLE_ID = JFK.KPI_DATA_PEOPEL_ID WHERE JFK.FLOW_ID=?";
		} else {
			sql = "SELECT JFK.KPI_NAME,JFK.KPI_STATISTICAL_METHODS,JFK.KPI_HORIZONTAL,JU.TRUE_NAME FROM JECN_FLOW_KPI_NAME_T JFK LEFT JOIN JECN_USER JU ON  JU.PEOPLE_ID = JFK.KPI_DATA_PEOPEL_ID WHERE JFK.FLOW_ID=?";
		}
		return this.listNativeSql(sql, flowId);
	}

	/**
	 * 根据流程set集合 查询KPI
	 * 
	 * @author fuzhh 2013-12-4
	 * @param flowIdSet
	 * @return
	 * @throws Exception
	 */
	public List<JecnFlowKpiName> findFlowKpiBySet(Set<Long> flowIdSet)
			throws Exception {
		if (flowIdSet == null || flowIdSet.size() <= 0) {
			return new ArrayList<JecnFlowKpiName>();
		}
		String sql = "select * from JECN_FLOW_KPI_NAME jfkn where jfkn.FLOW_ID in ";

		List<String> lsit = JecnCommonSql.getSetSqls(sql, flowIdSet);
		List<JecnFlowKpiName> flowKpiNameList = new ArrayList<JecnFlowKpiName>();
		for (String strSql : lsit) {
			flowKpiNameList.addAll(this.getSession().createSQLQuery(strSql)
					.addEntity(JecnFlowKpiName.class).list());
		}
		return flowKpiNameList;
	}

	@Override
	public List<Object[]> getFlowKpiNameTList(Long flowId,String pub) throws Exception {
		String sql = "select t.kpi_and_id,"
			+ "       t.flow_id,"
			+ "       t.kpi_name,"
			+ "       t.kpi_type,"
			+ "       t.kpi_definition,"
			+ "       t.kpi_statistical_methods,"
			+ "       t.kpi_target,"
			+ "       t.kpi_horizontal,"
			+ "       t.kpi_vertical,"
			+ "       t.creat_time,"
			+ "       t.kpi_target_operator,"
			+ "       t.kpi_data_method,"
			+ "       t.kpi_data_peopel_id,"
			+ "       t.kpi_relevance,"
			+ "       t.kpi_target_type,"
			+ "       t.first_targer_id,"
			+ "       t.update_time,"
			+ "       t.create_people_id,"
			+ "       t.update_people_id,"
			+ "       ju.true_name,"
			+ "       jft.targer_content,"
			+ "       t.kpi_purpose,"
			+ "       t.kpi_point,"
			+ "       t.kpi_explain,"
			+ "       t.kpi_period"
			+ "  from jecn_flow_kpi_name" + pub +" t"
			+ "  left join jecn_user ju on t.kpi_data_peopel_id = ju.people_id"
			+ "  left join JECN_FIRST_TARGER jft on t.first_targer_id = jft.id"
			+ " where t.flow_Id = ?";
		return this.listNativeSql(sql, flowId);
	}
}
