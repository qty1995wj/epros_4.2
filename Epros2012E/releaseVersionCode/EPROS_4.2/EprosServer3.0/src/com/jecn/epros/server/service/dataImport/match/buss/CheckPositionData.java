package com.jecn.epros.server.service.dataImport.match.buss;

import java.util.List;

import com.jecn.epros.server.common.JecnCommon;
import com.jecn.epros.server.service.dataImport.match.constant.MatchErrorInfo;
import com.jecn.epros.server.service.dataImport.match.util.MatchTool;
import com.jecn.epros.server.webBean.dataImport.match.BaseBean;
import com.jecn.epros.server.webBean.dataImport.match.PosMatchBean;

/**
 * 
 * 岗位行内检验、行间校验
 * 
 * @author Administrator
 * 
 */
public class CheckPositionData {
	/**
	 * 
	 * 岗位行内检验
	 * 
	 */
	public boolean checkPosRow(BaseBean baseBean) {
		if (baseBean == null || baseBean.isNull()) {
			return false;
		}
		boolean ret = true;

		// 获取岗位数据
		List<PosMatchBean> posBeanList = baseBean.getPosBeanList();
		if (posBeanList == null || posBeanList.size() == 0) {
			return ret;
		}
		for (int i = 0; i < posBeanList.size(); i++) {
			// 获取当前岗位记录
			PosMatchBean posBean = posBeanList.get(i);

			// 基准岗位编号不为空
			if (!JecnCommon.isNullOrEmtryTrim(posBean.getBasePosNum())) {
				// posBean.addError(MatchErrorInfo.POS_NUM_NULL);
				// ret = false;
				posBean.setBasePosNum(posBean.getBasePosNum().trim());
			} else {
				// 去空

			}

			// 基准岗位名称
			if (!JecnCommon.isNullOrEmtryTrim(posBean.getBasePosName())) {
				// posBean.addError(MatchErrorInfo.POS_NAME_NULL);
				// ret = false;
				posBean.setBasePosName(posBean.getBasePosName().trim());
			} else {
				// 去空

			}

			// 实际岗位编号
			if (JecnCommon.isNullOrEmtryTrim(posBean.getActPosNum())) {
				posBean.addError(MatchErrorInfo.POS_NAME_NULL);
				ret = false;
			} else {
				// 去空
				posBean.setActPosNum(posBean.getActPosNum().trim());
			}

			// 实际岗位名称
			if (JecnCommon.isNullOrEmtryTrim(posBean.getActPosName())) {
				posBean.addError(MatchErrorInfo.POS_NAME_NULL);
				ret = false;
			} else if (MatchTool.checkNameMaxLength(posBean.getActPosName())) {
				// 任职岗位名称不能超过122个字符
				posBean.addError(MatchErrorInfo.POS_NAME_LENGTH_ERROR);
				ret = false;
			} else {
				// 去空
				posBean.setActPosName(posBean.getActPosName().trim());
			}

			// 岗位所属部门编号去前缀后缀空后不为空
			if (JecnCommon.isNullOrEmtryTrim(posBean.getDeptNum())) {

				posBean.addError(MatchErrorInfo.POS_DEPT_NUM_NULL);
				ret = false;

			} else {

				// 岗位所属部门编号在部门集合中不存在
				// boolean existsPosDept = baseBean.existsPosByDept(posBean);
				// if (!existsPosDept) {
				// // posBean
				// posBean
				// .addError(MatchErrorInfo.POS_DEPT_NUM_NOT_EXISTS_DEPT_LIST);
				// ret = false;
				// } else {
				// 去空
				posBean.setDeptNum(posBean.getDeptNum().trim());
				// }
			}

			// // 标识位不为空
			// if
			// (JecnCommon.isNullOrEmtryTrim(String.valueOf(posBean.getPosFlag())))
			// {
			// // posBean.addError(ErrorInfo.POS_NAME_NULL);
			// ret = false;
			// } else {
			// // 去空
			// posBean.setPosFlag(posBean.getPosFlag());
			// }
		}
		// 行内检验失败，返回
		if (!ret) {
			return ret;
		}
		// ---------------行间校验
		// 岗位编号唯一
		// ret = checkPosRows(posBeanList);
		// if (!ret) {
		// return ret;
		// }
		// ---------------行间校验
		return ret;
	}

	/**
	 * 
	 * 判断岗位编号是否唯一，唯一返回true，不唯一返回false
	 * 
	 * @param posBeanList
	 *            List<PosBean>岗位集合
	 * @return boolean 唯一返回true，不唯一返回false
	 */
	private boolean checkPosRows(List<PosMatchBean> posBeanList) {
		boolean ret = true;
		for (PosMatchBean posBean : posBeanList) {
			for (PosMatchBean posBean2 : posBeanList) {
				// 不是同一个对象，基准岗位编号相同
				// if (!posBean.equals(posBean2)
				// && posBean.getBasePosNum().equals(
				// posBean2.getBasePosNum())) {
				// posBean.addError(ErrorInfo.POS_NUM_NOT_ONLY);
				// ret = false;
				// }

				// 不是同一个对象，实际岗位编号相同
				if (!posBean.equals(posBean2)
						&& posBean.getActPosNum().equals(
								posBean2.getActPosNum())) {
					posBean.addError(MatchErrorInfo.POS_NUM_NOT_ONLY);
					ret = false;
				}
				// 同一部门下基准岗位名称不能重复
				// if (!posBean.equals(posBean2)
				// && posBean.getDeptNum() != null
				// && posBean.getDeptNum().equals(posBean2.getDeptNum())
				// && posBean.getBasePosName().equals(
				// posBean2.getBasePosName())) {
				// // 同一部门下基准岗位名称不能重复
				// posBean.addError(ErrorInfo.POS_NAME_NOT_ONLY);
				// ret = false;
				//
				// }
				// 同一部门下实际岗位名称不能重复
				if (!posBean.equals(posBean2)
						&& posBean.getDeptNum() != null
						&& posBean.getDeptNum().equals(posBean2.getDeptNum())
						&& posBean.getActPosName().equals(
								posBean2.getActPosName())) {
					// 同一部门下实际岗位名称不能重复
					posBean.addError(MatchErrorInfo.POS_NAME_NOT_ONLY);
					ret = false;

				}
			}
		}
		return ret;
	}
}
