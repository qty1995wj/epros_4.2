package epros.designer.gui.author;

public enum AuthorMenuEnum {

	/************** 【内控指引知识库树菜单】 ***********/
	// 创建目录
	Control_addDir(true, true, true),
	// 创建条款
	Control_createClause(true, true, true),
	// 重命名
	Control_rename(true, true, true),
	// 刷新
	Control_refresh(true, true, true),
	// 删除
	Control_delete(true, true, true),
	// 节点移动
	Control_nodeMove(true, true, true),
	// 节点排序
	Control_nodeSort(true, true, true),
	// 条款编辑
	Control_editClause(true, true, true),

	/************** 【回收站树节点右键菜单】 ************/
	// 恢复 (恢复当前节点及子节点)
	Recycle_recyleFileNodes(true, true, true),
	// 恢复(只恢复当前节点)
	Recycle_recyleOnlyNode(true, true, true),
	// 删除
	Recycle_delete(true, true, true),

	/************** 【文件右键菜单】 ************/
	// 创建目录
	Filec_createDir(true, true, true),
	// 重命名
	File_rename(true, true, true),
	// 上传文件
	File_uploadFile(true, true, true),
	// 更新文件
	File_updateFile(true, true, true),
	// 提交审批
	File_submitApp(true, true, true),
	// 发布
	File_release(true, true, true),
	// 撤销发布
	File_cancelRelease(true, true, true),
	// 直接发布-不记录版本
	File_directRelease(true, true, true),
	// 打开文件
	File_openFile(true, true, true),
	// 文件属性
	File_fileProperty(true, true, true),
	// 刷新
	File_refresh(true, true, true),
	// 查阅权限(部门)
	File_lookPermissions(true, true, true),
	// 删除
	File_delete(true, true, true),
	// 节点移动
	File_nodeMove(true, true, true),
	// 节点排序
	File_nodeSort(true, true, true),
	// 文控信息
	File_historyRecord(true, true, true),
	// 显示/隐藏
	FileHideItem(true, true, true),

	/************** 【风险资源树节点右键菜单】 ************/
	// 新建风险目录
	RiskNewRiskDirItem(true, true, true),
	// 新建风险点
	RiskNewRiskItem(true, true, true),
	// 节点排序
	RiskNodeSortItem(true, true, true),
	// 节点移动
	RiskNodeMoveItem(true, true, true),
	// 刷新
	RiskRefreshItem(true, true, true),
	// 重命名
	RiskRenameItem(true, true, true),
	// 删除
	RiskDeleteItem(true, true, true),
	// 风险点属性
	RiskRiskPointPropertyItem(true, true, true),

	/************** 【组织右键菜单】 ************/
	// 新建组织
	OrgNewOrgItem(true, true, true),
	// 新建岗位
	OrgNewPositionItem(true, true, true),
	// 搜索
	OrgSearchItem(true, true, true),
	// 生成组织图
	OrgCreateOrgMapItem(true, true, true),
	// 节点排序
	OrgNodeSortItem(true, true, true),
	// 节点移动
	OrgNodeMoveItem(true, true, true),
	// 刷新
	OrgRefreshItem(true, true, true),
	// 组织属性
	OrgDutyItem(true, true, true),
	// 岗位属性
	OrgPositionPropItem(true, true, true),
	// 重命名
	OrgRenameItem(true, true, true),
	// 删除
	OrgDeleteItem(true, true, true),
	// 解除编辑
	OrgRemoveEditItem(true, true, true),

	/************** 【岗位组】 ************/
	// 新建目录
	PosGroupAddDir(true, true, true),
	// 新建岗位组
	PosGroupAddPosGroup(true, true, true),
	// 重命名
	PosGroupReName(true, true, true),
	// 删除
	PosGroupDelData(true, true, true),
	// 刷新
	PosGroupRefurbish(true, true, true),
	// 节点排序
	PosGroupNodeSort(true, true, true),
	// 节点移动
	PosGroupDragNode(true, true, true),
	// 关联岗位
	PosGroupUpdatePosition(true, true, true),
	// 关联HR基准岗位
	PosGroupBaseMenuItem(true, true, true),

	/************** 【角色】 ************/
	// 新建目录
	RoleDirMenu(true, true, true),
	// 新建角色
	RoleAddMenu(true, true, true),
	// 更新角色
	RoleUpdateMenu(true, true, true),
	// 重命名
	RoleReNameMenu(true, true, true),
	// 删除
	RoleDelMenu(true, true, true),
	// 刷新
	RoleRefurbishMenu(true, true, true),
	// 节点移动
	RoleDragNode(true, true, true),
	// 节点排序
	RoleNodeSort(true, true, true),

	/************** 【流程右键菜单项】 ************/
	// 新建流程地图
	FlowNewFlowMapItem(true, true, true),
	// 新建流程图
	FlowNewFlowItem(true, true, true),
	// 推送流程图
	FlowSysNcFlowItem(true, true, true),
	// 关联制度
	FlowRelRuleItem(true, true, true),
	// 搜索
	FlowSearchItem(true, true, true),
	// 节点排序
	FlowNodeSortItem(true, true, true),
	// 节点移动
	FlowNodeMoveItem(true, true, true),
	// 刷新
	FlowRefreshItem(true, true, true),
	// 发布
	FlowReleaseItem(true, true, true),
	// 撤销发布
	FlowCancelReleaseItem(true, true, true),
	// 直接发布-不记录版本
	FlowDirectReleaseItem(true, true, true),
	// 提交审批
	FlowSubmitAppItem(true, true, true),
	// 重命名
	FlowRenameItem(true, true, true),
	// 查阅权限(部门) 已与（岗位）合并为（查阅权限）
	FlowLookItem(true, true, true),
	// 流程地图描述
	FlowFlowMapDesItem(true, true, true),
	// 删除
	FlowDeleteItem(true, true, true),
	// 文控信息
	FlowHistoryItem(true, true, true),
	// 操作说明 下载
	FlowDownItem(true, true, true),
	// 操作说明下载(带附件)
	FlowDownFileItem(true, true, true),
	// 解除编辑
	FlowRemoveEditItem(true, true, true),
	// 重置编号
	FlowReNumItem(true, true, true),
	// 设计者权限：只显示设计权限节点/显示所有节点
	FlowDesignAccessMenu(true, true, true),

	/************** 【流程支持工具】 ************/
	// 新建支持工具
	FlowToolAddFlowToolMenu(true, true, true),
	// 更新支持工具
	FlowToolUpdateFlowToolMenu(true, true, true),
	// 删除
	FlowToolDelMenu(true, true, true),
	// 刷新
	FlowToolRefurbishMenu(true, true, true),
	// 节点移动
	FlowToolDragNode(true, true, true),
	// 节点排序
	FlowToolNodeSort(true, true, true),

	/************** 【流程模板树节点】 ************/
	// 新建流程地图模板
	FlowModelNewFlowMapModelItem(true, true, true),
	// 新建流程图模板
	FlowModelNewFlowModelItem(true, true, true),
	// 删除
	FlowModelDeleteModelItem(true, true, true),
	// 节点排序
	FlowModelNodeSortItem(true, true, true),
	// 节点移动
	FlowModelNodeMoveItem(true, true, true),
	// 刷新
	FlowModelRefreshItem(true, true, true),
	// 重命名
	FlowModelRenameItem(true, true, true),

	/************** 【项目】 ************/
	// 重命名
	ProjectRenameItem(true, false, false),

	/************** 【制度树右键菜单】 ************/
	// 新建制度目录
	RuleNewRuleDirItem(true, true, true),
	// 新建制度
	RuleNewRuleItem(true, true, true),
	// 关联标准
	RuleRelStanItem(true, true, true),
	// 关联风险
	RuleRelRiskItem(true, true, true),
	// 搜索
	RuleSearchItem(true, true, true),
	// 节点排序
	RuleNodeSortItem(true, true, true),
	// 节点移动
	RuleNodeMoveItem(true, true, true),
	// 刷新
	RuleRefreshItem(true, true, true),
	// 发布
	RuleReleaseItem(true, true, true),
	// 撤销发布
	RuleCancelReleaseItem(true, true, true),
	// 直接发布-不记录版本
	RuleDirectReleaseItem(true, true, true),
	// 提交审批
	RuleSubmitAppItem(true, true, true),
	// 重命名
	RuleRenameItem(true, true, true),
	// 查阅权限
	RuleLookItem(true, true, true),
	// 编辑制度信息
	RuleEditRuleInfoItem(true, true, true),
	// 关联文件
	RuleUploadRuleFileItem(true, true, true),
	// 更新制度文件
	RuleUpdateRuleFileItem(true, true, true),
	// 文控信息
	RuleHistoryItem(true, true, true),
	// 操作说明下载
	RuleDownloadOpDesItem(true, true, true),
	// 设置制度模板
	RuleSetRuleModeItem(true, true, true),
	// 重置编号
	// RulereNumItem(true,true,true),
	// 删除
	RuleDeleteItem(true, true, true),

	/************** 【标准树右键菜单】 ************/
	// 新建标准目录
	StadandardNewStandardItem(true, true, true),
	// 新建条款
	StadandardNewStanClauseItem(true, true, true),
	// 新建条款要求
	StadandardNewStanClauseRequireItem(true, true, true),
	// 编辑
	StadandardEditItem(true, true, true),
	// 属性
	StadandardPropertyItem(true, true, true),
	// 节点排序
	StadandardNodeSortItem(true, true, true),
	// 节点移动
	StadandardNodeMoveItem(true, true, true),
	// 刷新
	StadandardRefreshItem(true, true, true),
	// 更新
	StadandardUpdateStandardFileItem(true, true, true),
	// 重命名
	StadandardRenameItem(true, true, true),
	// 查阅权限
	StadandardLookItem(true, true, true),
	// 删除
	StadandardDeleteItem(true, true, true),
	// 关联流程
	StadandardSetRalationFlowItem(true, true, true),
	// 关联文件
	StadandardUploadStadandardFileItem(true, true, true);

	private boolean isAdmin;
	private boolean isDesigner;
	private boolean isSecondAdmin;

	private AuthorMenuEnum(boolean isAdmin, boolean isSecondAdmin, boolean isDesigner) {
		this.isAdmin = isAdmin;
		this.isSecondAdmin = isSecondAdmin;
		this.isDesigner = isDesigner;
	}

	public boolean isAdmin() {
		return isAdmin;
	}

	public boolean isDesigner() {
		return isDesigner;
	}

	public boolean isSecondAdmin() {
		return isSecondAdmin;
	}
}
