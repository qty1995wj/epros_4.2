package com.jecn.epros.server.action.web.dataImport.sync.excelorDB;

import java.io.File;
import java.io.InputStream;

import com.jecn.epros.server.common.BaseAction;
import com.jecn.epros.server.service.dataImport.sync.excelOrDb.buss.UserModelFileDownBuss;
import com.jecn.epros.server.service.dataImport.sync.excelOrDb.unit.SyncConstant;
import com.jecn.epros.server.service.dataImport.sync.excelOrDb.unit.SyncErrorInfo;

/**
 * 
 * 人员模板下载
 * 
 * @author Administrator
 * 
 */
public class UserModelFileDownAction extends BaseAction {
	/** 对应的业务处理类 */
	private UserModelFileDownBuss userModelFileDownBuss = null;

	/**
	 * 
	 * 执行下载
	 * 
	 * @return
	 * @throws Exception
	 */
	public String execute() throws Exception {
		return checkFile();
	}

	/**
	 * 
	 * 判读文件是否存在
	 * 
	 * @return
	 */
	private String checkFile() {
		File file = new File(userModelFileDownBuss.getOutFilepath());
		if (file.exists()) {
			return SyncConstant.RESULT_SUCCESS;
		} else {
			ResultHtml(SyncErrorInfo.FILE_SAVE_LOCAL_FAIL,
					SyncConstant.HELF_DATA_INPUT_ACTION);
			return null;
		}
	}

	/**
	 * 
	 * 获得下载文件的内容,可以直接读入一个物理文件或从数据库中获取内容
	 * 
	 * @return
	 * @throws Exception
	 */
	public InputStream getInputStream() {
		return userModelFileDownBuss.getInputStream();
	}

	/**
	 * 
	 * 对于配置中的 ${fileName}, 获得下载保存时的文件名
	 * 
	 * @return String 文件名称
	 */
	public String getFileName() {
		return userModelFileDownBuss.getFileName();
	}

	public UserModelFileDownBuss getUserModelFileDownBuss() {
		return userModelFileDownBuss;
	}

	public void setUserModelFileDownBuss(
			UserModelFileDownBuss userModelFileDownBuss) {
		this.userModelFileDownBuss = userModelFileDownBuss;
	}

}
