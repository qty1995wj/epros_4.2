package epros.designer.table;

import java.awt.Dimension;
import java.util.Vector;

import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;

import epros.designer.util.JecnProperties;
import epros.draw.gui.top.optionpane.JecnOptionPane;
import epros.draw.gui.ui.JecnBasicTableHeaderUI;
import epros.draw.util.JecnUIUtil;

public abstract class JecnTable extends JTable {

	public JecnTable() {
		refluseTable();
		// 设置默认背景色
		this.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		// 自定义表头UI
		this.getTableHeader().setUI(new JecnBasicTableHeaderUI());
	}

	public void refluseTable() {
		this.setModel(getTableModel());
		if (isSelectMutil()) {
			this.getSelectionModel().setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
		} else {
			this.getSelectionModel().setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		}
		int[] cols = gethiddenCols();
		if (cols != null && cols.length > 0) {
			TableColumnModel columnModel = this.getColumnModel();
			for (int col : cols) {
				TableColumn tableColumn = columnModel.getColumn(col);
				tableColumn.setMinWidth(0);
				tableColumn.setMaxWidth(0);
			}
		}
		this.updateUI();
	}

	/**
	 * @author yxw 2012-5-3
	 * @description:table的model
	 * @return
	 */
	public abstract JecnTableModel getTableModel();

	/**
	 * @author yxw 2012-5-3
	 * @description:多选
	 * @return
	 */
	public abstract boolean isSelectMutil();

	/**
	 * @author yxw 2012-5-3
	 * @description:获得隐藏列
	 * @return
	 */
	public abstract int[] gethiddenCols();

	/**
	 * @author yxw 2012-5-11
	 * @description:根据行号数姐删除表格里的数据
	 * @param rows
	 */
	public void removeRows(int[] rows) {
		for (int i = rows.length - 1; i >= 0; i--) {
			((JecnTableModel) this.getModel()).removeRow(rows[i]);
		}

	}

	/**
	 * @author yxw 2012-5-11
	 * @description:添加一行到表格的结尾。
	 * @param rowData
	 */
	public void addRow(Vector rowData) {
		((JecnTableModel) this.getModel()).addRow(rowData);
	}

	/**
	 * @author yxw 2012-5-11
	 * @description:为表格增加多行记录
	 * @param vector
	 */
	public void addRows(Vector<Vector<String>> vector) {
		for (Vector<String> v : vector) {
			((JecnTableModel) this.getModel()).addRow(v);
		}
	}

	/**
	 * @author yxw 2012-5-11
	 * @description:为表格重新增加数据，先清空，再增加
	 * @param vector
	 */
	public void reAddVectors(Vector<Vector<String>> vector) {
		// 清空
		this.remoeAll();
		// 增加
		for (Vector<String> v : vector) {
			((JecnTableModel) this.getModel()).addRow(v);
		}
	}

	/**
	 * @author yxw 2012-5-11
	 * @description:清空表格里的数据
	 */
	public void remoeAll() {
		// 清空
		for (int index = this.getModel().getRowCount() - 1; index >= 0; index--) {
			((JecnTableModel) this.getModel()).removeRow(index);
		}

	}

	/**
	 * @author yxw 2012-5-11
	 * @description:行上移移通用方法，支持多行移动
	 */
	public void moveUpRows() {
		int[] selectRows = this.getSelectedRows();
		if (selectRows.length == 0) {
			JecnOptionPane.showMessageDialog(this, JecnProperties.getValue("task_selectedRowsInfo"));
			return;
		}
		// 移动一行 移动的一行不是表格中的第一行时
		int start = selectRows[0];
		if (selectRows.length == 1 && start > 0) {
			((JecnTableModel) this.getModel()).moveRow(start, start, start - 1);
			this.setRowSelectionInterval(start - 1, start - 1);
		}
		// 移动多行 当移动多行并且移动的第一行不是表格中的第一行时
		if (selectRows.length > 1 && selectRows[0] > 0) {
			this.clearSelection();

			for (int i = 0; i < selectRows.length; i++) {
				start = selectRows[i];
				((JecnTableModel) this.getModel()).moveRow(start, start, start - 1);
				this.addRowSelectionInterval(start - 1, start - 1);

			}

		}

	}

	/**
	 * @author yxw 2012-5-11
	 * @description:行下移移通用方法，支持多行移动
	 */
	public void moveDownRows() {
		int[] selectRows = this.getSelectedRows();
		if (selectRows.length == 0) {
			JecnOptionPane.showMessageDialog(this, JecnProperties.getValue("task_selectedRowsInfo"));
			return;
		}
		int rowCount = ((JecnTableModel) this.getModel()).getRowCount();

		// 移动一行 移动的一行不是表格中的最后一行时
		int start = selectRows[0];
		if (selectRows.length == 1 && start < rowCount - 1) {
			((JecnTableModel) this.getModel()).moveRow(start, start, start + 1);
			this.setRowSelectionInterval(start + 1, start + 1);
		}
		// 移动多行 当移动多行并且移动的最后一行不是表格中的最后一行时
		if (selectRows.length > 1 && selectRows[selectRows.length - 1] < rowCount - 1) {

			this.clearSelection();
			for (int i = selectRows.length - 1; i >= 0; i--) {
				start = selectRows[i];
				((JecnTableModel) this.getModel()).moveRow(start, start, start + 1);
				this.addRowSelectionInterval(start + 1, start + 1);

			}
		}

	}

	/** 隐藏表头 */
	public void hiddenTableHeader() {
		this.getTableHeader().setVisible(false);
		DefaultTableCellRenderer renderer = new DefaultTableCellRenderer();
		renderer.setPreferredSize(new Dimension(0, 0));
		this.getTableHeader().setDefaultRenderer(renderer);
	}

	public void setTableColumn(int column, int width) {
		TableColumn tableColumn = this.getColumnModel().getColumn(column);
		tableColumn.setMinWidth(width);
		tableColumn.setMaxWidth(width);
	}
}
