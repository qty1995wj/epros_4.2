package com.jecn.epros.domain.process;

import com.jecn.epros.domain.LinkResource;
import io.swagger.annotations.ApiModelProperty;

import java.util.List;
import java.util.Map;

public class ProcessArchitectureCard {

    @ApiModelProperty(value = "架构卡所属流程")
    private ProcessBean architecture;

    @ApiModelProperty(value = "架构卡详细信息")
    private ProcessDetailInfo detailInfo;

    @ApiModelProperty(value = "上一层流程")
    private ProcessBean parent;

    @ApiModelProperty(value = "架构卡配置名称")
    private Map<String,Object> cardName;

    //是否显示上一层流程
    private boolean parentIsShow;

    @ApiModelProperty(value = "下一层流程")
    private List<ProcessBean> subProcesses;

    //是否显示下一层流程
    private boolean subProcessesIsShow;

    /**
     * 图片下载链接
     */
    private LinkResource linkImage;

    public Map<String, Object> getCardName() {
        return cardName;
    }

    public void setCardName(Map<String, Object> cardName) {
        this.cardName = cardName;
    }

    public boolean getParentIsShow() {
        return parentIsShow;
    }

    public void setParentIsShow(boolean parentIsShow) {
        this.parentIsShow = parentIsShow;
    }

    public boolean getSubProcessesIsShow() {
        return subProcessesIsShow;
    }

    public void setSubProcessesIsShow(boolean subProcessesIsShow) {
        this.subProcessesIsShow = subProcessesIsShow;
    }

    public ProcessBean getArchitecture() {
        return architecture;
    }

    public void setArchitecture(ProcessBean architecture) {
        this.architecture = architecture;
    }

    public ProcessDetailInfo getDetailInfo() {
        return detailInfo;
    }

    public void setDetailInfo(ProcessDetailInfo detailInfo) {
        this.detailInfo = detailInfo;
    }

    public ProcessBean getParent() {
        return parent;
    }

    public void setParent(ProcessBean parent) {
        this.parent = parent;
    }

    public List<ProcessBean> getSubProcesses() {
        return subProcesses;
    }

    public void setSubProcesses(List<ProcessBean> subProcesses) {
        this.subProcesses = subProcesses;
    }

    public LinkResource getLinkImage() {
        return linkImage;
    }

    public void setLinkImage(LinkResource linkImage) {
        this.linkImage = linkImage;
    }
}
