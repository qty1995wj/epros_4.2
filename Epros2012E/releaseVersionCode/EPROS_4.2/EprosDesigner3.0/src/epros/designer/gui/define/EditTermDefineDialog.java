package epros.designer.gui.define;

import org.apache.log4j.Logger;

import com.jecn.epros.server.bean.define.TermDefinitionLibrary;

import epros.designer.gui.common.JecnConstants;
import epros.designer.tree.JecnTree;
import epros.designer.tree.JecnTreeCommon;
import epros.designer.tree.JecnTreeNode;
import epros.designer.util.ConnectionPool;
import epros.designer.util.JecnProperties;

public class EditTermDefineDialog extends TermDefineDailog {
	private static Logger log = Logger.getLogger(EditTermDefineDialog.class);
	private TermDefinitionLibrary termDefinitionLibrary;

	public EditTermDefineDialog(JecnTreeNode selectNode, JecnTree jTree) {
		super(selectNode, jTree, JecnProperties.getValue("editTermDefine"));
		try {
			termDefinitionLibrary = ConnectionPool.getTermDefinitionAction().getTermDefinitionLibraryById(
					selectNode.getJecnTreeBean().getId());
		} catch (Exception e) {
			log.error(JecnProperties.getValue("getTermDefineObjectError"), e);
		}
		String defineName = "";
		String defineContent = "";
		if (termDefinitionLibrary != null) {
			defineName = termDefinitionLibrary.getName();
			defineContent = termDefinitionLibrary.getInstructions();
		}
		this.initLayout(defineName == null ? "" : defineName, defineContent == null ? "" : defineContent);
	}

	@Override
	protected boolean isExist(String name, JecnTreeNode selectNode) throws Exception {
		return ConnectionPool.getTermDefinitionAction().isExistEdit(name, selectNode.getJecnTreeBean().getId(), 1,
				selectNode.getJecnTreeBean().getPid());
	}

	@Override
	protected void save(String defineName, String defineContent, JecnTreeNode selectNode, JecnTree jTree)
			throws Exception {
		termDefinitionLibrary.setUpdatePersonId(JecnConstants.getUserId());
		termDefinitionLibrary.setName(defineName);
		termDefinitionLibrary.setInstructions(defineContent);
		selectNode.getJecnTreeBean().setContent(defineContent);
		ConnectionPool.getTermDefinitionAction().editTermDefine(termDefinitionLibrary);
		JecnTreeCommon.reNameNode(jTree, selectNode, defineName);
	}

}
