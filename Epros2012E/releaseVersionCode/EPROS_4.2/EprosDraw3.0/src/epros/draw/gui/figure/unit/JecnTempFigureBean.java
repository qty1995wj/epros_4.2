package epros.draw.gui.figure.unit;

import java.awt.Point;

import epros.draw.constant.JecnToolBoxConstant.MapElemType;

/**
 * 拖动流程元素获取图形基础数据（图形唯一标识、高度和宽度，旋转角度）
 * 
 * @author ZHANGXH
 * @date： 日期：Jul 6, 2012 时间：11:53:13 AM
 */
public class JecnTempFigureBean {
	/** 流程元素唯一标识 */
	private MapElemType mapElemType = null;
	/** 流程元素旋转角度 */
	private double currentAngle = 0;
	/** 流程元素宽度 */
	private int userWidth = 0;
	/** 流程元素高度 */
	private int userHeight = 0;
	/** 图形拖动后的位置点横坐标 */
	private int localX = 0;
	/** 图形拖动后的位置点纵坐标 */
	private int localY = 0;

	public JecnTempFigureBean() {

	}

	public MapElemType getMapElemType() {
		return mapElemType;
	}

	public void setMapElemType(MapElemType mapElemType) {
		this.mapElemType = mapElemType;
	}

	public double getCurrentAngle() {
		return currentAngle;
	}

	public void setCurrentAngle(double currentAngle) {
		this.currentAngle = currentAngle;
	}

	public int getUserWidth() {
		return userWidth;
	}

	public void setUserWidth(int userWidth) {
		this.userWidth = userWidth;
	}

	public int getUserHeight() {
		return userHeight;
	}

	public void setUserHeight(int userHeight) {
		this.userHeight = userHeight;
	}

	/**
	 * 
	 * 设置图形位置点和大小
	 * 
	 * @param localPoint
	 * @param width
	 * @param height
	 */
	public void reSetTempFigureBean(Point localPoint, int width, int height) {
		this.localX = localPoint.x;
		this.localY = localPoint.y;
		this.userWidth = width;
		this.userHeight = height;
	}

	public Point getLocalPoint() {
		return new Point(localX, localY);
	}

	public int getLocalX() {
		return localX;
	}

	public void setLocalX(int localX) {
		this.localX = localX;
	}

	public int getLocalY() {
		return localY;
	}

	public void setLocalY(int localY) {
		this.localY = localY;
	}
}
