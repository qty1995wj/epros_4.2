/**epros所有的元素类型*/
var eleType = {
	'ParallelLines' : 'ParallelLines', // 横分割线 1
	'VerticalLine' : ' ', // 竖分割线 2
	'CommonLine' : 'CommonLine', // 连接线 3
	'ManhattanLine' : 'ManhattanLine', // 连接线(带箭头) 4
	'OneArrowLine' : 'OneArrowLine', // 单向箭头 5
	'TwoArrowLine' : 'TwoArrowLine', // 双向箭头 6
	'DividingLine' : 'DividingLine', // 分割线 7
	'OvalSubFlowFigure' : 'OvalSubFlowFigure', // 流程图：子流程 8
	'Oval' : 'Oval', // 流程地图：圆形 9
	'Triangle' : 'Triangle', // 流程地图 三角形 10
	'FunctionField' : 'FunctionField', // 流程地图 矩形功能域 11
	'entagon' : 'entagon', // 五边形 12
	'RightHexagon' : 'RightHexagon', // 箭头六边形 13
	'ModelFigure' : 'ModelFigure', // 泳池 14
	'DottedRect' : 'DottedRect', // 协作框 15
	'FreeText' : 'FreeText', // 自由文本框 16
	'CommentText' : 'CommentText', // 注释框 17
	'CommentLine' : 'CommentLine', // 注释框小线段
	'IconFigure' : 'IconFigure', // 图标插入框 18
	'InputHandFigure' : 'InputHandFigure', // 手动输入 19
	'SystemFigure' : 'SystemFigure', // 制度 20
	'RoundRectWithLine' : 'RoundRectWithLine', // 活动 21
	'RoleFigure' : 'RoleFigure', // 角色 22
	'CustomFigure' : 'CustomFigure', // 客户 23
	'ReverseArrowhead' : 'ReverseArrowhead', // 返入 24
	'RightArrowhead' : 'RightArrowhead', // 返出 25
	'Rhombus' : 'Rhombus', // 决策 26
	'EventFigure' : 'EventFigure', // 事件 27
	'ImplFigure' : 'ImplFigure', // 接口 28
	'FileImage' : 'FileImage', // 文档 29
	'DataImage' : 'DataImage', // 信息系统 30
	'ToFigure' : 'ToFigure', // 跳转:转出 31
	'FromFigure' : 'FromFigure', // 跳转:转入 32
	'EndFigure' : 'EndFigure', // 结束 33
	'PAFigure' : 'PAFigure', // PA:问题区域 34
	'KSFFigure' : 'KSFFigure', // KSF:关键成功因素 35
	'KCPFigure' : 'KCPFigure', // KCP:关键控制点 36
	'XORFigure' : 'XORFigure', // XOR:多种情况下只能选中一种 37
	'ORFigure' : 'ORFigure', // OR:多种情况下只能选中一种或多种 38
	'FlowFigureStart' : 'FlowFigureStart', // 流程开始 39
	'FlowFigureStop' : 'FlowFigureStop', // 流程结束 40
	'AndFigure' : 'AndFigure', // AND 41
	'VirtualRoleFigure' : 'VirtualRoleFigure', // 虚拟角色 42
	// 以下画图工具没有
	'PositionFigure' : PositionFigure, // 岗位 43
	'Rect' : Rect, // 组织 44
	'PartitionFigure' : PartitionFigure, // 阶段分隔符 45
	'ITRhombus' : ITRhombus, // IT决策 46
//	'HDividingLine' : HDividingLine, // 横线
//	'VDividingLine' : VDividingLine, // 竖线
	'DateFreeText' : DateFreeText, // 最后更改时间文本框
//	'IsoscelesTrapezoidFigure' : IsoscelesTrapezoidFigure, // 等腰梯形
//	'FlowLevelFigureOne' : FlowLevelFigureOne, // 流程等级图形
//	'FlowLevelFigureTwo' : FlowLevelFigureTwo, // 流程等级图形
//	'FlowLevelFigureThree' : FlowLevelFigureThree, // 流程等级图形
//	'FlowLevelFigureFour' : FlowLevelFigureFour, // 流程等级图形
	'MapNameText' : MapNameText// 流程图/流程地图/组织图名称文本框
//	'KeyPointFigure' : KeyPointFigure,//关键控制点
//	'RiskPointFigure' : RiskPointFigure
//风险点
}
