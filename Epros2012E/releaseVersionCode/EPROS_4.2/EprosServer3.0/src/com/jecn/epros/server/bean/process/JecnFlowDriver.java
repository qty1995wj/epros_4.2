package com.jecn.epros.server.bean.process;

/**
 * 
 * 流程图时间驱动
 *
 */
public class JecnFlowDriver implements java.io.Serializable{
	private Long flowId;//流程ID作为此表主键
	private String frequency;//频率
	private String startTime;//开始时间
	private String endTime;//结束时间
	private String quantity;//时间类型：1:日，2:周，3:月，4:季，5:年
	private Long driverEmailPeopleId;//邮件通知人
	public Long getDriverEmailPeopleId() {
		return driverEmailPeopleId;
	}
	public void setDriverEmailPeopleId(Long driverEmailPeopleId) {
		this.driverEmailPeopleId = driverEmailPeopleId;
	}
	public Long getFlowId() {
		return flowId;
	}
	public void setFlowId(Long flowId) {
		this.flowId = flowId;
	}
	public String getFrequency() {
		return frequency;
	}
	public void setFrequency(String frequency) {
		this.frequency = frequency;
	}
	public String getStartTime() {
		return startTime;
	}
	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}
	public String getEndTime() {
		return endTime;
	}
	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}
	public String getQuantity() {
		return quantity;
	}
	public void setQuantity(String quantity) {
		this.quantity = quantity;
	}
}
