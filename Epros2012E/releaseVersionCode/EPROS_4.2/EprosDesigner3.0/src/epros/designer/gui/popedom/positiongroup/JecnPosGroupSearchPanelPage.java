package epros.designer.gui.popedom.positiongroup;

import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.Vector;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

import org.apache.log4j.Logger;

import com.jecn.epros.server.bean.system.JecnTablePageBean;
import com.jecn.epros.server.bean.tree.JecnTreeBean;

import epros.designer.gui.common.JecnConstants;
import epros.designer.gui.popedom.organization.OrgChooseDialog;
import epros.designer.gui.popedom.position.HighEfficiencyOrgPositionTree;
import epros.designer.gui.system.table.JecnTablePaging;
import epros.designer.tree.JecnTree;
import epros.designer.tree.JecnTreeCommon;
import epros.designer.util.ConnectionPool;
import epros.designer.util.JecnProperties;
import epros.draw.gui.swing.JecnPanel;
import epros.draw.gui.top.dialog.JecnDialog;
import epros.draw.util.DrawCommon;
import epros.draw.util.JecnUIUtil;

public class JecnPosGroupSearchPanelPage extends JecnPanel {
	private static Logger log = Logger.getLogger(JecnPosGroupSearchPanelPage.class);
	private Long groupId;
	/** 搜索名称 */
	private JLabel searchLabel = null;
	/** 搜索内容 */
	private JTextField searchText = null;
	/** 搜索名称 */
	private JLabel searchDepLabel;
	/** 搜索内容 */
	private JTextField searchDepText;

	private JecnDialog jecnDialog;
	Insets insets = new Insets(3, 3, 3, 3);
	private JecnTablePaging searchTable = null;

	private JecnTablePaging resultTable = null;

	/** 按钮面板 */
	private JecnPanel buttonPanel = new JecnPanel();
	/** 全部添加 */
	private JButton allAddBtn = new JButton(JecnProperties.getValue("addAll"));
	/** 选中 */
	private JButton onlySelectBtn = new JButton(JecnProperties.getValue("addOne"));

	private JButton selectBtn = new JButton(JecnProperties.getValue("select"));
	private JCheckBox checkBtn = new JCheckBox(JecnProperties.getValue("doesNodes"));

	/** 部门信息集合 */
	protected List<JecnTreeBean> listOrgBean = new ArrayList<JecnTreeBean>();

	/** 部门信息集合 */
	protected List<Long> orgIds = new ArrayList<Long>();;

	public JecnPosGroupSearchPanelPage(int row, JecnPanel infoPanel, Insets insets, JecnDialog jecnDialog, Long groupId) {
		this.setBorder(BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(), JecnProperties
				.getValue("search")));
		this.groupId = groupId;
		this.jecnDialog = jecnDialog;
		GridBagConstraints c = new GridBagConstraints(0, row, 1, 1, 1.0, 1.0, GridBagConstraints.WEST,
				GridBagConstraints.BOTH, insets, 0, 0);
		infoPanel.add(this, c);
		initCompotents();
		searchTable.refreshTable("home");
	}

	public void initCompotents() {

		this.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		this.setLayout(new GridBagLayout());
		initSearchPanel();
		GridBagConstraints c = new GridBagConstraints(0, 3, 4, 1, 0.0, 0.0, GridBagConstraints.WEST,
				GridBagConstraints.NONE, insets, 0, 0);
		this.add(buttonPanel, c);
		initButton();
	}

	private void initSearchPanel() {
		// 初始化搜索标签名称"岗位名称："
		searchLabel = new JLabel(JecnProperties.getValue("positionNameC"));
		// 搜索输入库
		searchText = new JTextField();
		// "部门名称："
		searchDepLabel = new JLabel(JecnProperties.getValue("deptNameC"));
		searchDepText = new JTextField();

		GridBagConstraints c = new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
				GridBagConstraints.NONE, insets, 0, 0);
		this.add(searchLabel, c);
		c = new GridBagConstraints(1, 0, 3, 1, 1.0, 0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, insets,
				0, 0);
		this.add(searchText, c);

		c = new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, insets, 0, 0);
		this.add(searchDepLabel, c);
		c = new GridBagConstraints(1, 1, 1, 1, 1.0, 0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, insets,
				0, 0);
		this.add(searchDepText, c);

		// 添加选择按钮
		c = new GridBagConstraints(2, 1, 1, 1, 0.0, 0, GridBagConstraints.WEST, GridBagConstraints.NONE, insets, 0, 0);
		this.add(selectBtn, c);

		c = new GridBagConstraints(3, 1, 1, 1, 0.0, 0, GridBagConstraints.WEST, GridBagConstraints.NONE, insets, 0, 0);
		this.add(checkBtn, c);

		searchTable = new SearchPanelPage(2, this, insets, jecnDialog);
		searchText.getDocument().addDocumentListener(new DocumentListener() {

			@Override
			public void removeUpdate(DocumentEvent e) {

				searchAction();

			}

			@Override
			public void insertUpdate(DocumentEvent e) {

				searchAction();

			}

			@Override
			public void changedUpdate(DocumentEvent e) {

			}
		});

		selectBtn.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				// 部门权限选择框
				OrgChooseDialog orgChooseDialog = new OrgChooseDialog(listOrgBean);
				orgChooseDialog.setVisible(true);
				if (orgChooseDialog.isOperation()) {
					if (listOrgBean != null && listOrgBean.size() > 0) {
						StringBuilder str = new StringBuilder();
						for (JecnTreeBean jecnTreeBean : listOrgBean) {
							orgIds.add(jecnTreeBean.getId());
							str.append(jecnTreeBean.getName());
							str.append("/");
						}
						str.deleteCharAt(str.length() - 1);
						searchDepText.setText(str.toString());
						searchDepText.setEditable(false);
						searchDeptAction();
					} else {
						searchDepText.setText("");
						searchDepText.setEditable(true);
						orgIds.clear();
						listOrgBean.clear();
					}
				}
			}
		});

		searchDepText.getDocument().addDocumentListener(new DocumentListener() {

			@Override
			public void removeUpdate(DocumentEvent e) {
				searchDeptAction();
			}

			@Override
			public void insertUpdate(DocumentEvent e) {
				searchDeptAction();
			}

			@Override
			public void changedUpdate(DocumentEvent e) {

			}
		});
		checkBtn.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				searchDeptAction();
			}
		});

	}

	public void setResultTable(JecnTablePaging resultTable) {
		this.resultTable = resultTable;
	}

	public JecnTablePaging getResultTable() {
		return resultTable;
	}

	public void initButton() {
		buttonPanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		buttonPanel.setLayout(new FlowLayout(FlowLayout.LEFT));
		buttonPanel.add(allAddBtn);
		buttonPanel.add(onlySelectBtn);
		allAddBtn.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				allAddBtnPerformed();
			}

		});
		onlySelectBtn.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				onlySelectBtnPerformed();
			}

		});
	}

	protected void onlySelectBtnPerformed() {
		Set<Long> selectIds = searchTable.getSelectIds();
		if (selectIds.size() > 0) {
			for (JecnTreeBean bean : resultTable.getList()) {
				if (selectIds.contains(bean.getId())) {
					selectIds.remove(bean.getId());
				}
			}
			try {
				// 执行数据库表的添加保存
				ConnectionPool.getPosGroup().AddSelectPoss(selectIds, groupId);
				resultTable.refreshTable("home");
			} catch (Exception e) {
				log.error("JecnPosGroupSearchPanelPage method onlySelectBtnPerformed  error", e);
			}
		}
	}

	protected void allAddBtnPerformed() {
		try {
			// // 执行数据库表的添加保存
			// ConnectionPool.getPosGroup().AddSearchResultPoss(resDepartSelectCommon.getIdResult(),
			// searchText.getText(),
			// groupId, JecnConstants.projectId);
			boolean isSelect = checkBtn.isSelected();
			if (orgIds == null || orgIds.isEmpty()) {
				ConnectionPool.getPosGroup().AddSearchResultPoss(searchDepText.getText(), searchText.getText(),
						groupId, JecnConstants.projectId, isSelect);
			} else {
				// 执行数据库表的添加保存
				ConnectionPool.getPosGroup().AddSearchResultPoss(orgIds, searchText.getText(), groupId,
						JecnConstants.projectId, isSelect);
			}
			resultTable.refreshTable("home");
		} catch (Exception e) {
			log.error("JecnPosGroupSearchPanelPage method allAddBtnPerformed  error", e);
		}
	}

	private void searchAction() {
		searchTable.refreshTable("home");
	}

	private void searchDeptAction() {
		searchTable.refreshTable("home");
	}

	class SearchPanelPage extends JecnTablePaging {

		public SearchPanelPage(int row, JecnPanel infoPanel, Insets insets, JecnDialog jecnDialog) {
			super(row, infoPanel, insets, jecnDialog);
		}

		@Override
		public Vector<Vector<String>> getContent(List<JecnTreeBean> list) {
			Vector<Vector<String>> content = new Vector<Vector<String>>();
			if (list != null) {
				for (JecnTreeBean jecnTreeBean : list) {
					Vector<String> data = new Vector<String>();
					data.add(jecnTreeBean.getId().toString());
					data.add(jecnTreeBean.getName());
					data.add(jecnTreeBean.getPname());
					content.add(data);
				}
			}
			return content;
		}

		@Override
		public JecnTablePageBean getJecnTablePageBean(String operationType, int pageSize) {
			try {
				boolean isSelect = checkBtn.isSelected();
				if (orgIds == null || orgIds.isEmpty()) {
					// 执行数据库表的添加保存
					return ConnectionPool.getPosGroup().searchPos(
							searchDepText.getText(),
							searchText.getText(),
							DrawCommon.isNullOrEmtryTrim(searchTable.nowPage.getText()) ? 0 : Integer
									.parseInt(searchTable.nowPage.getText()), operationType, pageSize, groupId,
							JecnConstants.projectId, isSelect);
				} else {
					// 执行数据库表的添加保存
					return ConnectionPool.getPosGroup().searchPos(
							orgIds,
							searchText.getText(),
							DrawCommon.isNullOrEmtryTrim(searchTable.nowPage.getText()) ? 0 : Integer
									.parseInt(searchTable.nowPage.getText()), operationType, pageSize, groupId,
							JecnConstants.projectId, isSelect);
				}
			} catch (Exception e) {
				log.error("SearchPanelPage method getJecnTablePageBean  error", e);
			}
			return null;
		}

		@Override
		public Vector<String> getTableTitle() {
			Vector<String> title = new Vector<String>();
			title.add("id");
			title.add(JecnProperties.getValue("positionName"));// 岗位名称
			title.add(JecnProperties.getValue("deptName"));// 部门名称
			return title;
		}

		@Override
		public void isClickTable() {
			Set<Long> selectIds = this.getSelectIds();
			if (selectIds == null) {
				return;
			}
			Iterator<Long> ids = selectIds.iterator();
			while (ids.hasNext()) {
				Long id = ids.next();
				try {
					JecnTreeBean treeBean = ConnectionPool.getOrganizationAction().getJecnTreeBeanByposId(id);
					if (treeBean == null) {
						return;
					}
					JecnTree tree = ((UpdatePosToGroupDialog) jecnDialog).getjTree();
					HighEfficiencyOrgPositionTree jTree = ((HighEfficiencyOrgPositionTree) tree);
					jTree.setAllowExpand(false);
					JecnTreeCommon.searchNodePos(treeBean, jTree);
					jTree.setAllowExpand(true);
				} catch (Exception e) {
					e.printStackTrace();
					log.error("PositionChooseDialog isClickTable is error！", e);
				}
			}
		}

	}
}
