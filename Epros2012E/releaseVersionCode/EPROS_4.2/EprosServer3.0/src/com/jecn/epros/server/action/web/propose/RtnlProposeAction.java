package com.jecn.epros.server.action.web.propose;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import net.sf.json.JSONArray;

import org.apache.log4j.Logger;

import com.jecn.epros.server.bean.popedom.JecnUser;
import com.jecn.epros.server.bean.process.JecnFlowStructure;
import com.jecn.epros.server.bean.propose.JecnFlowRtnlPropose;
import com.jecn.epros.server.bean.propose.JecnRtnlPropose;
import com.jecn.epros.server.bean.propose.JecnRtnlProposeCount;
import com.jecn.epros.server.bean.propose.JecnRtnlProposeCountDetail;
import com.jecn.epros.server.bean.propose.JecnRtnlProposeFile;
import com.jecn.epros.server.bean.rule.Rule;
import com.jecn.epros.server.bean.system.JecnConfigItemBean;
import com.jecn.epros.server.common.BaseAction;
import com.jecn.epros.server.common.JecnCommon;
import com.jecn.epros.server.common.JecnContants;
import com.jecn.epros.server.common.Pager;
import com.jecn.epros.server.common.PagerService;
import com.jecn.epros.server.common.JecnConfigContents.ConfigItemPartMapMark;
import com.jecn.epros.server.emailnew.BaseEmailBuilder;
import com.jecn.epros.server.emailnew.EmailBasicInfo;
import com.jecn.epros.server.emailnew.EmailBuilderFactory;
import com.jecn.epros.server.emailnew.EmailBuilderFactory.EmailBuilderType;
import com.jecn.epros.server.service.popedom.IPersonService;
import com.jecn.epros.server.service.process.IFlowStructureService;
import com.jecn.epros.server.service.propose.IRtnlProposeFileService;
import com.jecn.epros.server.service.propose.IRtnlProposeService;
import com.jecn.epros.server.service.reports.excel.ProposeDownExcelService;
import com.jecn.epros.server.service.rule.IRuleService;
import com.jecn.epros.server.service.system.IJecnConfigItemService;
import com.jecn.epros.server.util.JecnUtil;
import com.jecn.epros.server.webBean.WebLoginBean;
import com.opensymphony.xwork2.ActionContext;

public class RtnlProposeAction extends BaseAction {
	private static final Logger log = Logger.getLogger(RtnlProposeAction.class);

	private IRtnlProposeService rtnlProposeService;
	private IRtnlProposeFileService rtnlProposeFileService;
	/** 获取系统配置信息 */
	private IJecnConfigItemService configItemService;
	/** 人员操作接口 */
	private IPersonService personService;

	/** 制度service */
	private IRuleService ruleService;
	/** 流程service **/
	private IFlowStructureService flowStructureService;

	/** 流程ID、制度ID */
	private Long flowId;
	/** 流程名称、制度名称 */
	private String fileName;

	/** 流程编号、制度编号 */
	private String fileNum;

	/** 合理化建议管理 搜索 制度或流程的名称 */
	private String processName;
	/** 合理化建议管理 搜索 制度或流程的编号称 */
	private String processNum;

	/** 流程责任人Id */
	private Long resPeopleId = null;
	/** 当前登录用户ID */
	private Long loginUserId;
	/** 当前登录用户名称 */
	private String loginName;

	/** 是否是责任人 1是，0不是 */
	private int isResRuleposibleNum;

	/*** 全部建议数据总数 */
	private int allStateNum = 0;
	/** 已读状态下的建议总数 **/
	private int readStateNum = 0;
	/** 未读状态下的建议总数 **/
	private int unReadStateNum = 0;
	/** 采纳状态下的建议总数 **/
	private int acceptStateNum = 0;

	/** 是否选中我的建议/我处理的建议的复选框 0:没选中，1：选中 */
	private int isCheckedMyPropose = 0;
	/** 全部状态下的建议集合 */
	private List<JecnRtnlPropose> listAllStatePropose = null;
	/** 当前要查看的数据的状态 默认全部 -1全部 0 未读 1 已读 2 采纳 3拒绝 */
	private int nowState = -1;
	/** 是否显示全部已读按钮 0是不显示，1是显示并置灰，2是显示 */
	private int isShowAllReadImg = 0;

	// ***********************************************判断是否是流程责任人(第二个参数)
	// ***********************************************判断是否是查询我的建议即页面是否选中我的建议复选框(第三个参数)

	/** 定义PagerService对象，用于传到页面 */
	private PagerService pagerService;
	/** 定义Pager对象，用于传到页面 */
	private Pager pager;
	/** 得到所有行数（为了页面显示）初始值为空 */
	private String totalRows;
	/** 得到所有的行数（为了计算） */
	private int _totalRows;
	/** 记录当前页号 */
	private String currentPage = "1";
	/** 获取当前执行的方法，first:首页，previous:前一页，next:后一页，last:尾页 */
	private String pagerMethod;
	/** 全部已读按钮 0是不显示，1是置灰，2是显示 */
	private int operationReadBtnState = 0;

	/** 单个合理化建议主键ID */
	private String rtnlProposeId = null;
	/** 管理人员按钮图片操作标识：采纳：2，已读：1，未读：0，回复(回复后，状态变为已读)：1 */
	private int intflag = 0;
	/*** 回复/采纳内容 */
	private String strContent = null;

	/** 责任部门ID */
	private Long orgId;
	/** 提交人ID */
	private Long submitPeopleId;
	/** 开始时间 */
	private String startTime;
	/** 结束时间 */
	private String endTime;

	/*** 导航---合理化建议集合 */
	private List<JecnFlowRtnlPropose> flowRtnlProposeList;

	private JecnFlowRtnlPropose flowRtnlPropose;

	/** 界面我处理的建议是否选中： 0：否，1：是 */
	private int isSelectedHandle = 0;
	/** 界面我提交的建议是否选中： 0：否，1：是 */
	private int isSelectedSubmit = 0;

	/*** 是否是系统管理员 0:不是系统管理员，1：是系统管理员 */
	private int isAdiminLogin = 0;
	/** 流程制度标识：0：流程；1：制度 */
	private int isFlowRtnl = 0;
	/** 合理化建议 搜索类别：0：流程；1：制度 */
	private int rtnlTypeId = -1;
	/** 导航栏-合理化建议 ：制度流程名称搜索Name */
	private String ruleFlowName;
	/** 导航栏-合理化建议：制度流程搜索类型 type */
	private String searchType = null;
	/** 拒绝的合理化建议的数量 */
	private int refuseStateNum = 0;
	/** 访问是否来自邮件 **/
	private String fromEmail = "false";

	/** 合理化建议操作类型（邮件方式） */
	private String handleFlag;

	/** 搜索：提交人 */
	private String personIds;
	/** 搜索：部门ids **/
	private String orgIds;

	private int start = 0;
	private int limit = 20;

	/***
	 * 点击流程，打开合理化建议，显示建议总数(点击合理化建议的时候执行)
	 * 
	 * @return
	 */
	public String getAllRtnlPropose() {
		try {

			// 获取流程信息
			JecnFlowStructure flowStructure = rtnlProposeService.getFlowStructure(flowId);
			fileName = "";
			fileNum = "";
			if (flowStructure != null) {
				// 流程名称
				fileName = flowStructure.getFlowName();
				// 流程编号
				fileNum = flowStructure.getFlowIdInput();
			}
			isFlowRtnl = 0;
			getFIleRtnlPropose(flowId, 0, fileName, fileNum);
			totalRows = 10 + "";
		} catch (Exception e) {
			log.error("获取建议数据出错", e);
			return null;
		}
		return SUCCESS;
	}

	/***
	 *点击制度，打开合理化建议，显示建议总数
	 * 
	 * @Time 2014-10-27
	 * @return
	 */
	public String getAllRuleRtnlPropose() {
		try {

			// 获取制度信息
			Rule jecnRule = rtnlProposeService.getRuleInfo(flowId);
			String ruleName = "";
			String ruleNum = "";
			if (jecnRule != null) {
				// 流程名称
				ruleName = jecnRule.getRuleName();
				// 流程编号
				ruleNum = jecnRule.getRuleNumber();
			}
			isFlowRtnl = 1;
			getFIleRtnlPropose(flowId, 1, ruleName, ruleNum);
		} catch (Exception e) {
			log.error("获取建议数据出错", e);
			return null;
		}
		return SUCCESS;
	}

	/**
	 * 封装点击流程或制度的公用部分
	 * 
	 * @param fileId
	 * @throws Exception
	 */
	private void getFIleRtnlPropose(Long fileId, int isFlowRtnl, String fileName, String fileNum) throws Exception {
		// 登陆人主键ID
		JecnUser jecnUser = getJecnUser();
		// 登陆人Id
		loginUserId = jecnUser.getPeopleId();
		// 登陆人真实姓名
		loginName = jecnUser.getTrueName();
		// 名称
		this.fileName = fileName;
		// 编号
		this.fileNum = fileNum;
		// 是否是管理员
		if (isAdmin()) {
			this.isAdiminLogin = 1;
		} else {
			this.isAdiminLogin = 0;
		}

		/** 获取不同状态下建议数据的总数 */
		// 全部建议
		allStateNum = rtnlProposeService.getFlowProposeTotal(fileId, loginUserId, -1, isCheckedMyPropose, isFlowRtnl);
		// 未读建议
		unReadStateNum = rtnlProposeService.getFlowProposeTotal(fileId, loginUserId, 0, isCheckedMyPropose, isFlowRtnl);
		// 已读建议
		readStateNum = rtnlProposeService.getFlowProposeTotal(fileId, loginUserId, 1, isCheckedMyPropose, isFlowRtnl);
		// 采纳建议
		acceptStateNum = rtnlProposeService.getFlowProposeTotal(fileId, loginUserId, 2, isCheckedMyPropose, isFlowRtnl);
		// 拒绝建议
		refuseStateNum = rtnlProposeService.getFlowProposeTotal(fileId, loginUserId, 3, isCheckedMyPropose, isFlowRtnl);

		// 是否是责任人
		boolean isResPeople = rtnlProposeService.isRes(loginUserId, fileId, isFlowRtnl);
		if (isResPeople) {
			this.isResRuleposibleNum = 1;
		} else {
			this.isResRuleposibleNum = 0;
		}
		// 如果是管理员并且有处理建议权限
		if ((isAdmin() && JecnContants.proSystemRation) || (isResPeople && JecnContants.proFlowRefRation)) {
			this.isShowAllReadImg = 1;
			if (unReadStateNum > 0) {
				isShowAllReadImg = 2;
			}
		}
	}

	/***
	 * 获取点击流程、制度的合理化建议的内容
	 * 
	 */
	public String getAllStateProposeInfo() {
		try {
			getJspInfo();
			return SUCCESS;
		} catch (Exception e) {
			log.error("获取全部状态建议数据出错", e);
			return SUCCESS;
		}
	}

	/***
	 * 流程/制度 合理化建议 获取合理化建议界面显示数据
	 * 
	 */
	public boolean getJspInfo() {
		try {
			// 获取登录用户的ID
			JecnUser jecnUser = getJecnUser();
			loginUserId = jecnUser.getPeopleId();
			loginName = jecnUser.getTrueName();

			listAllStatePropose = new ArrayList<JecnRtnlPropose>();

			// 由于合理化建议总是有两个请求。而_totalRows在前一个请求中已经可以求出来了。所以_totalRows是从前台传过来的。
			if (_totalRows == 0) {
				_totalRows++;
			} else {
				// 得到所有行数
				this.totalRows = String.valueOf(_totalRows);
			}
			pager = pagerService.getPager(currentPage, pagerMethod, _totalRows);
			// 全部状态下 分页显示当前页的建议数据pager.getPageSize()
			listAllStatePropose = this.rtnlProposeService.getListFlowRtnlPropose(nowState, loginUserId, flowId,
					isCheckedMyPropose, isFlowRtnl, isAdmin(), pager.getStartRow(), pager.getPageSize());

			if (listAllStatePropose != null) {
				// 记录当前页号
				this.currentPage = String.valueOf(pager.getCurrentPage());

				return true;
			}
		} catch (Exception e) {
			log.error("获取全部状态建议数据出错", e);
			return false;
		}
		return false;
	}

	/***
	 * 更新所有的合理化建议未读状态为已读
	 * 
	 * @return
	 */
	public String updateProUnReadToRead() {

		try {
			String isEditStr = this.rtnlProposeService.updateAllUnReadToRead(flowId, isFlowRtnl);
			outJsonString(isEditStr);
		} catch (Exception e) {
			log.error("更新全部已读数据出错", e);
			return null;
		}
		return SUCCESS;
	}

	/****
	 * 更新单个合理化建议的状态 包括回复采纳所反馈的内容 rtnlProposeId
	 */
	public String updateSingleToRead() {
		try {
			JecnUser jecnUser = getJecnUser();
			loginUserId = jecnUser.getPeopleId();
			loginName = jecnUser.getTrueName();
			Date currentTime = new Date();
			SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
			String dateString = formatter.format(currentTime);
			String isSuccess = this.rtnlProposeService.updateProposeState(rtnlProposeId, intflag, loginUserId,
					strContent, dateString);

			outJsonString(isSuccess);

			// 如果采纳或者拒绝后给提交人发送邮件
			if (intflag == 2 || intflag == 3) {
				String value = getflowEndDateListMarkVlalue(ConfigItemPartMapMark.mailProposeToSubmit);
				if ("1".equals(value)) {
					handleSendEmail(jecnUser, rtnlProposeId, intflag);
				}
			}

		} catch (Exception e) {
			log.error("更新未读为已读数据出错", e);
			return null;
		}
		return SUCCESS;
	}

	private String getflowEndDateListMarkVlalue(ConfigItemPartMapMark mark) {
		String value = null;
		for (JecnConfigItemBean config : JecnContants.flowEndDateList) {
			if (config.getMark().toString().equals(mark.toString())) {
				value = config.getValue();
				break;
			}

		}

		return value;
	}

	/**
	 * 采纳或者拒绝发送邮件
	 * 
	 * @author hyl
	 * @param handleUser
	 *            处理人
	 * @param rtnlProposeId
	 *            合理化建议id
	 * @param intflag
	 *            操作
	 */
	private void handleSendEmail(JecnUser handleUser, String rtnlProposeId, int intflag) {

		try {
			JecnRtnlPropose jecnRtnlPropose = rtnlProposeService.getJecnRtnlPropose(rtnlProposeId);
			if (jecnRtnlPropose != null) {

				if (jecnRtnlPropose.getRtnlType() == 0) {// 流程

					JecnFlowStructure flowStructure = flowStructureService.findJecnFlowStructureById(jecnRtnlPropose
							.getRelationId());
					jecnRtnlPropose.setRelationName(flowStructure.getFlowName());
				} else if (jecnRtnlPropose.getRtnlType() == 1) {// 制度

					Rule rule = ruleService.getRule(jecnRtnlPropose.getRelationId());
					jecnRtnlPropose.setRelationName(rule.getRuleName());
				}

				JecnUser toUser = personService.get(jecnRtnlPropose.getCreatePersonId());
				if (toUser != null && toUser.getIsLock() == 0) {
					BaseEmailBuilder emailBuilder = EmailBuilderFactory
							.getEmailBuilder(EmailBuilderType.PROPOSE_HANDLER_RESULT);
					List<JecnUser> users = new ArrayList<JecnUser>();
					users.add(toUser);
					emailBuilder.setData(users, handleUser, jecnRtnlPropose, intflag);
					List<EmailBasicInfo> buildEmail = emailBuilder.buildEmail();
					JecnUtil.saveEmail(buildEmail);
					// TODO :EMAIL

				}
			}
		} catch (Exception e) {
			log.error("处理合理化建议给创建人发送处理结果异常", e);
		}

	}

	/***
	 * 我的建议/我处理的建议
	 * 
	 * @return
	 */
	public String getMyRtnlPropose() {
		try {

			String fileName = "";
			String fileNum = "";
			if (this.isFlowRtnl == 0) {
				// 获取流程信息
				JecnFlowStructure flowStructure = rtnlProposeService.getFlowStructure(flowId);
				if (flowStructure != null) {
					// 流程名称
					fileName = flowStructure.getFlowName();
					// 流程编号
					fileNum = flowStructure.getFlowIdInput();
				}
			} else if (this.isFlowRtnl == 1) {
				// 获取制度信息
				Rule jecnRule = rtnlProposeService.getRuleInfo(flowId);
				if (jecnRule != null) {
					// 流程名称
					fileName = jecnRule.getRuleName();
					// 流程编号
					fileNum = jecnRule.getRuleNumber();
				}
			}

			getFIleRtnlPropose(flowId, isFlowRtnl, fileName, fileNum);

			/** 获取不同状态下建议数据的总数 */
			List<String> list = new ArrayList<String>();
			pager = pagerService.getPager(currentPage, "first", allStateNum);
			list.add(String.valueOf(allStateNum));
			list.add(String.valueOf(readStateNum));
			list.add(String.valueOf(unReadStateNum));
			list.add(String.valueOf(acceptStateNum));
			list.add(String.valueOf(refuseStateNum));
			list.add(String.valueOf(pager.getTotalPages()));
			JSONArray jsonArray = JSONArray.fromObject(list);
			outJsonString(jsonArray.toString());

		} catch (Exception e) {
			log.error("获取我的建议/我处理的建议数据出错", e);
			return null;
		}
		return null;
	}

	/**
	 * 封装搜索条件
	 * 
	 * @return
	 * @throws Exception
	 */
	private JecnFlowRtnlPropose packSearch() throws Exception {
		JecnFlowRtnlPropose flowRtnlPropose = new JecnFlowRtnlPropose();
		// 流程名称
		if (processName != null && !"".equals(processName)) {
			flowRtnlPropose.setFlowName(processName);
		}
		// 流程编号
		if (processNum != null && !"".equals(processNum)) {
			flowRtnlPropose.setFlowNum(processNum);
		}
		// 责任部门ID
		if (orgId != null && !"".equals(orgId) && orgId != -1) {
			flowRtnlPropose.setOrgId(orgId);
		}
		// 责任人ID
		if (resPeopleId != null && !"".equals(resPeopleId) && resPeopleId != -1) {
			flowRtnlPropose.setResFlowId(resPeopleId);
		}
		// 合理化建议类型:0：流程；1：制度
		flowRtnlPropose.setRtnlType(rtnlTypeId);
		// 搜索起始和结束时间
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		Date dateStartTime = null;
		Date dateEndTime = null;
		if (startTime != null && !"".equals(startTime)) {
			dateStartTime = sdf.parse(startTime);
		}
		if (endTime != null && !"".equals(endTime)) {
			// endTime = endTime+" 23:59:59";
			dateEndTime = sdf.parse(endTime);
			dateEndTime.setHours(23);
			dateEndTime.setMinutes(59);
			dateEndTime.setSeconds(59);
		}
		flowRtnlPropose.setDateStartTime(dateStartTime);
		flowRtnlPropose.setDateEndTime(dateEndTime);
		loginUserId = getPeopleId();
		// 登陆人ID
		flowRtnlPropose.setLoginUserId(loginUserId);
		// 是否选中我的建议
		if (isSelectedSubmit == 0) {
			flowRtnlPropose.setSelectedSubmit(false);
		} else {
			flowRtnlPropose.setSelectedSubmit(true);
		}
		// 提交人
		flowRtnlPropose.setSubmitPeopleId(submitPeopleId);
		// 项目Id
		flowRtnlPropose.setProjectId(getProjectId());
		// 登陆人是否是管理员
		flowRtnlPropose.setAdmin(isAdmin());
		return flowRtnlPropose;
	}

	/***
	 * 导航===获取不同状态下合理化建议总数
	 * 
	 * @return
	 */
	public String getNavigationRtnlPropose() {
		try {

			flowRtnlPropose = packSearch();
			/** 获取不同状态下建议数据的总数 */
			List<String> list = new ArrayList<String>();
			// 全部建议
			allStateNum = rtnlProposeService.getNavigationProposeTotal(flowRtnlPropose, -1);
			// 未读建议
			unReadStateNum = rtnlProposeService.getNavigationProposeTotal(flowRtnlPropose, 0);
			// 已读建议
			readStateNum = rtnlProposeService.getNavigationProposeTotal(flowRtnlPropose, 1);
			// 采纳建议
			acceptStateNum = rtnlProposeService.getNavigationProposeTotal(flowRtnlPropose, 2);
			// 拒绝建议
			refuseStateNum = rtnlProposeService.getNavigationProposeTotal(flowRtnlPropose, 3);

			list.add(String.valueOf(allStateNum));
			list.add(String.valueOf(readStateNum));
			list.add(String.valueOf(unReadStateNum));
			list.add(String.valueOf(acceptStateNum));
			list.add(String.valueOf(refuseStateNum));
			JSONArray jsonArray = JSONArray.fromObject(list);
			outJsonString(jsonArray.toString());
		} catch (Exception e) {
			log.error("导航===获取不同状态下合理化建议总数", e);
			return null;
		}
		return null;
	}

	/***
	 * 删除合理化建议
	 * 
	 * @return
	 */
	public String deleteMyFlowPropose() {
		try {
			loginUserId = this.getPeopleId();
			JecnRtnlPropose jecnRtnlPropose = rtnlProposeService.getJecnRtnlPropose(rtnlProposeId);
			if (jecnRtnlPropose == null || jecnRtnlPropose.getRelationId() == null) {
				return null;
			}
			/** 删除合理化建议的人：0：是创建人，1：系统管理员或者流程责任人 **/
			int isCreator = 0;
			// 获取登录用户的ID
			boolean isResPeople = rtnlProposeService.isRes(loginUserId, jecnRtnlPropose.getRelationId(),
					jecnRtnlPropose.getRtnlType());
			if (isAdmin() || isResPeople) {
				isCreator = 1;
			}
			boolean isCanDel = this.rtnlProposeService.deleteFlowPropose(rtnlProposeId, flowId, 0, isCreator);

			if (!isCanDel) {
				if (isAdmin() || isResPeople) {
					outJsonString("1");
				}
				outJsonString("0");
				return null;
			}
		} catch (Exception e) {
			log.error("删除我提交的未读建议数据出错", e);
			return null;
		}
		outJsonString("1");
		return SUCCESS;
	}

	/***
	 * 删除合理化建议回复内容
	 * 
	 * @return
	 */

	public String deleteProposeReplyContent() {
		try {
			log.info("传入ID的值为：" + rtnlProposeId);
			String isSuccess = rtnlProposeService.updateJecnRtnlPropose(rtnlProposeId);
			outJsonString(isSuccess);
		} catch (Exception e) {
			log.info("删除合理化建议回复内容失败", e);
		}
		return null;
	}

	/***
	 * 编辑建议：是否符合显示DIV条件
	 */
	public void editDivIsShow() {
		try {
			boolean isEdit = this.rtnlProposeService.eidtIsShowDiv(rtnlProposeId, flowId, 0);
			if (!isEdit) {
				outJsonString("0");
			}
		} catch (Exception e) {
			log.error(e);
		}
		outJsonString("1");
	}

	/***
	 * 合理化建议搜索显示界面 1
	 * 
	 * @return
	 */
	public String searchProposeJsp() {
		try {
			flowRtnlPropose = this.packSearch();
			JecnUser jecnUser = getJecnUser();
			loginUserId = jecnUser.getPeopleId();
			loginName = jecnUser.getTrueName();

			/** 获取不同状态下建议数据的总数 */
			// 全部建议
			allStateNum = rtnlProposeService.getNavigationProposeTotal(flowRtnlPropose, -1);
			// 未读建议
			unReadStateNum = rtnlProposeService.getNavigationProposeTotal(flowRtnlPropose, 0);
			// 已读建议
			readStateNum = rtnlProposeService.getNavigationProposeTotal(flowRtnlPropose, 1);
			// 采纳建议
			acceptStateNum = rtnlProposeService.getNavigationProposeTotal(flowRtnlPropose, 2);
			// 拒绝建议
			acceptStateNum = rtnlProposeService.getNavigationProposeTotal(flowRtnlPropose, 3);
		} catch (Exception e) {
			log.error("导航---合理化建议不同状态下数据总数获取出错", e);
		}
		return SUCCESS;
	}

	/**
	 * 合理化建议 搜索界面 2
	 * 
	 * @author fuzhh 2013-9-18
	 * @return
	 */
	public String searchProposeShow() {
		try {
			flowRtnlPropose = this.packSearch();
			JecnUser jecnUser = getJecnUser();
			loginUserId = jecnUser.getPeopleId();
			loginName = jecnUser.getTrueName();

			// 由于合理化建议总是有两个请求。而_totalRows在前一个请求中已经可以求出来了。所以_totalRows是从前台传过来的。
			if (_totalRows == 0) {
				_totalRows++;
			} else {
				// 得到所有行数
				this.totalRows = String.valueOf(_totalRows);
			}
			pager = pagerService.getPager(currentPage, pagerMethod, _totalRows);

			listAllStatePropose = new ArrayList<JecnRtnlPropose>();
			// 全部状态下 分页显示当前页的建议数据pager.getPageSize()
			listAllStatePropose = this.rtnlProposeService.getListNavigationRtnlPropose(flowRtnlPropose, nowState, pager
					.getStartRow(), pager.getPageSize());
			flowRtnlProposeList = this.rtnlProposeService.searchListRtnlPropose(listAllStatePropose, flowRtnlPropose);
			// 记录当前页号
			this.currentPage = String.valueOf(pager.getCurrentPage());

		} catch (Exception e) {
			log.error("搜索合理化建议出错", e);
		}
		return SUCCESS;
	}

	public String selectRuleFlowName() {
		List<Object[]> list = new ArrayList<Object[]>();
		try {
			int type = 0;
			if ("flowchart".equals(searchType)) {
				type = 0;
			} else if ("rulechart".equals(searchType)) {
				type = 1;
			}
			list = rtnlProposeService.findJecnFlowStructureName(ruleFlowName, type);
		} catch (Exception e) {
			log.error("搜索制度、流程名称出错", e);
		}
		JSONArray json = JSONArray.fromObject(list);
		outJsonString(json.toString());
		return null;
	}

	/**
	 * 根据前台传回来的数据将单个的合理化建议更改为需要的值
	 * 
	 * @author hyl
	 * @return
	 */
	public String updateSingleState() {
		try {
			JecnUser jecnUser = getJecnUser();
			loginUserId = jecnUser.getPeopleId();
			loginName = jecnUser.getTrueName();
			Date currentTime = new Date();
			SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
			String dateString = formatter.format(currentTime);
			String isSuccess = this.rtnlProposeService.updateProposeState(rtnlProposeId, intflag, loginUserId,
					strContent, dateString);
			outJsonString(isSuccess);

			// 如果是采纳或者拒绝发邮件
			if (intflag == 2 || intflag == 3) {
				String value = getflowEndDateListMarkVlalue(ConfigItemPartMapMark.mailProposeToSubmit);
				if ("1".equals(value)) {
					handleSendEmail(jecnUser, rtnlProposeId, intflag);
				}
			}
		} catch (Exception e) {
			log.error("更新未读为已读数据出错", e);
		}
		return null;
	}

	/**
	 * 邮件方式处理合理化建议
	 * 
	 * @author hyl
	 * @return
	 */
	public String updateSingleStateFromEmail() {

		try {
			JecnUser jecnUser = getJecnUser();
			loginUserId = jecnUser.getPeopleId();
			loginName = jecnUser.getTrueName();
			Date currentTime = new Date();
			SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
			String dateString = formatter.format(currentTime);
			if (JecnCommon.isNullOrEmtryTrim(handleFlag) || JecnCommon.isNullOrEmtryTrim(rtnlProposeId)) {

				log.error("邮件方式处理合理化建议参数错误 rtnlProposeId：" + rtnlProposeId + " handleFlag" + handleFlag);
				return ERROR;
			}
			intflag = Integer.parseInt(handleFlag);
			String isSuccess = this.rtnlProposeService.updateProposeState(rtnlProposeId, intflag, loginUserId,
					strContent, dateString);
			if (isSuccess.equals("1")) {
				outJsonString("操作成功！");
			} else if (isSuccess.equals("4")) {
				outJsonString("该建议已被他人处理！");
			} else {
				outJsonString("操作失败！");
			}
			outJsonString(isSuccess);
			// 处理人处理完合理化建议给提交人发送邮件
			String value = getflowEndDateListMarkVlalue(ConfigItemPartMapMark.mailProposeToSubmit);
			if ("1".equals(value)) {
				handleSendEmail(jecnUser, rtnlProposeId, intflag);
			}
		} catch (Exception e) {
			log.error("更新未读为已读数据出错", e);
		}
		return null;
	}

	/**
	 * 获得单条合理化建议的基本信息
	 * 
	 * @author hyl
	 * @return
	 */
	public String getProposeInfo() {

		try {

			WebLoginBean webLoginBean = (WebLoginBean) ActionContext.getContext().getSession().get("webLoginBean");
			JecnRtnlPropose rtnlPropose = this.rtnlProposeService.getJecnRtnlPropose(rtnlProposeId);
			if (rtnlPropose == null) {
				return "delete";
			}
			JecnRtnlProposeFile jecnRtnlProposeFile = this.rtnlProposeService.getJecnRtnlProposeFile(rtnlPropose
					.getId());
			if (jecnRtnlProposeFile != null) {
				rtnlPropose.setListFileIds(jecnRtnlProposeFile.getId());
				rtnlPropose.setListFileNames(jecnRtnlProposeFile.getFileName());
			}
			// 创建人
			JecnUser jecnUser = personService.get(rtnlPropose.getCreatePersonId());
			if (jecnUser != null) {
				rtnlPropose.setCreateUpPeopoleName(jecnUser.getTrueName());
				rtnlPropose.setCreatePeopleState(jecnUser.getIsLock().intValue());
			} else {
				rtnlPropose.setCreatePeopleState(1);
			}
			// 回复人
			if (rtnlPropose.getReplyPersonId() != null) {
				jecnUser = personService.get(rtnlPropose.getReplyPersonId());
				if (jecnUser != null) {
					rtnlPropose.setReplyName(jecnUser.getTrueName());
					rtnlPropose.setReplyPeopleState(jecnUser.getIsLock().intValue());
				} else {
					rtnlPropose.setReplyPeopleState(1);
				}
			}

			listAllStatePropose = new ArrayList<JecnRtnlPropose>();
			listAllStatePropose.add(rtnlPropose);
			flowRtnlPropose = new JecnFlowRtnlPropose();
			flowRtnlPropose.setLoginUserId(getJecnUser().getPeopleId());
			flowRtnlPropose.setAdmin(webLoginBean.getIsAdmin());
			flowRtnlProposeList = this.rtnlProposeService.searchListRtnlPropose(listAllStatePropose, flowRtnlPropose);
		} catch (Exception e) {
			log.error("打开合理化建议出错", e);
		}

		return SUCCESS;
	}

	/**
	 * 创建合理化建议的人员的采纳率统计
	 * 
	 * @author hyl
	 * @return
	 */
	public String queryProposeCreate() {

		try {
			if (personIds != null) {
				int totalNum = personIds.split(",").length;
				if (totalNum > 0) {
					List<JecnRtnlProposeCountDetail> countList = this.rtnlProposeService.getProposeCreateCount(
							personIds, startTime, endTime, start, limit);
					JecnRtnlProposeCount proposeCount = new JecnRtnlProposeCount();
					proposeCount.setType(1);
					proposeCount.setDateStr(startTime + "---" + endTime);
					proposeCount.setCountDetailList(countList);

					ProposeDownExcelService propose = new ProposeDownExcelService();
					propose.setProposeCountBean(proposeCount);
					// 生成图片地址
					String imageUrl = propose.getPorposeCreateImage();
					JSONArray jsonArray = JSONArray.fromObject(countList);
					outJsonString("{\"root\":" + jsonArray.toString() + ",\"totalProperty\":" + totalNum
							+ ",\"imageUrl\":" + "\"" + imageUrl + "\"" + "}");
				}
			}

		} catch (Exception e) {
			log.error("创建合理化建议的人员的采纳率统计异常", e);
		}

		return null;

	}

	/**
	 * 创建合理化建议的部门的采纳率统计
	 * 
	 * @author hyl
	 * @return
	 */
	public String queryProposeOrg() {

		try {
			if (orgIds != null) {
				int totalNum = orgIds.split(",").length;
				if (totalNum > 0) {
					List<JecnRtnlProposeCountDetail> countList = this.rtnlProposeService.getProposeOrgCount(orgIds,
							startTime, endTime, start, limit);
					JecnRtnlProposeCount proposeCount = new JecnRtnlProposeCount();
					proposeCount.setType(2);
					proposeCount.setDateStr(startTime + "---" + endTime);
					proposeCount.setCountDetailList(countList);

					ProposeDownExcelService propose = new ProposeDownExcelService();
					propose.setProposeCountBean(proposeCount);
					// 生成图片地址
					String imageUrl = propose.getPorposeCreateImage();
					JSONArray jsonArray = JSONArray.fromObject(countList);
					outJsonString("{\"root\":" + jsonArray.toString() + ",\"totalProperty\":" + totalNum
							+ ",\"imageUrl\":" + "\"" + imageUrl + "\"" + "}");
				}
			}

		} catch (Exception e) {
			log.error("创建合理化建议的人员的采纳率统计异常", e);
		}

		return null;

	}

	/**
	 * 创建合理化建议的详细信息统计
	 * 
	 * @author hyl
	 * @return
	 */
	public String queryProposeDetail() {

		try {
			if (JecnCommon.isNullOrEmtryTrim(startTime) || JecnCommon.isNullOrEmtryTrim(endTime)) {
				log.error("合理化建议详细信息开始时间与结束时间为空，startTime" + startTime + " endTime" + endTime);
			}
			int totalNum = this.rtnlProposeService.getProposeDetailCountNum(personIds, orgIds, startTime, endTime);
			if (totalNum > 0) {
				List<JecnRtnlProposeCountDetail> countList = this.rtnlProposeService.getProposeDetailCountList(
						personIds, orgIds, startTime, endTime, start, limit);
				JecnRtnlProposeCount proposeCount = new JecnRtnlProposeCount();
				proposeCount.setType(3);
				proposeCount.setDateStr(startTime + "---" + endTime);
				proposeCount.setCountDetailList(countList);

				ProposeDownExcelService propose = new ProposeDownExcelService();
				propose.setProposeCountBean(proposeCount);

				JSONArray jsonArray = JSONArray.fromObject(countList);
				outJsonPage(jsonArray.toString(), totalNum);
			} else {
				outJsonPage("[]", 0);

			}

		} catch (Exception e) {
			log.error("创建合理化建议的人员的采纳率统计异常", e);
		}

		return null;

	}

	public String getFromEmail() {
		return fromEmail;
	}

	public void setFromEmail(String fromEmail) {
		this.fromEmail = fromEmail;
	}

	public IRtnlProposeService getRtnlProposeService() {
		return rtnlProposeService;
	}

	public void setRtnlProposeService(IRtnlProposeService rtnlProposeService) {
		this.rtnlProposeService = rtnlProposeService;
	}

	public IRtnlProposeFileService getRtnlProposeFileService() {
		return rtnlProposeFileService;
	}

	public void setRtnlProposeFileService(IRtnlProposeFileService rtnlProposeFileService) {
		this.rtnlProposeFileService = rtnlProposeFileService;
	}

	public IJecnConfigItemService getConfigItemService() {
		return configItemService;
	}

	public void setConfigItemService(IJecnConfigItemService configItemService) {
		this.configItemService = configItemService;
	}

	public IPersonService getPersonService() {
		return personService;
	}

	public void setPersonService(IPersonService personService) {
		this.personService = personService;
	}

	public Long getFlowId() {
		return flowId;
	}

	public void setFlowId(Long flowId) {
		this.flowId = flowId;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getFileNum() {
		return fileNum;
	}

	public void setFileNum(String fileNum) {
		this.fileNum = fileNum;
	}

	public String getProcessName() {
		return processName;
	}

	public void setProcessName(String processName) {
		this.processName = processName;
	}

	public String getProcessNum() {
		return processNum;
	}

	public void setProcessNum(String processNum) {
		this.processNum = processNum;
	}

	public Long getResPeopleId() {
		return resPeopleId;
	}

	public void setResPeopleId(Long resPeopleId) {
		this.resPeopleId = resPeopleId;
	}

	public Long getLoginUserId() {
		return loginUserId;
	}

	public void setLoginUserId(Long loginUserId) {
		this.loginUserId = loginUserId;
	}

	public String getLoginName() {
		return loginName;
	}

	public void setLoginName(String loginName) {
		this.loginName = loginName;
	}

	public int getIsResRuleposibleNum() {
		return isResRuleposibleNum;
	}

	public void setIsResRuleposibleNum(int isResRuleposibleNum) {
		this.isResRuleposibleNum = isResRuleposibleNum;
	}

	public int getAllStateNum() {
		return allStateNum;
	}

	public void setAllStateNum(int allStateNum) {
		this.allStateNum = allStateNum;
	}

	public int getReadStateNum() {
		return readStateNum;
	}

	public void setReadStateNum(int readStateNum) {
		this.readStateNum = readStateNum;
	}

	public int getUnReadStateNum() {
		return unReadStateNum;
	}

	public void setUnReadStateNum(int unReadStateNum) {
		this.unReadStateNum = unReadStateNum;
	}

	public int getAcceptStateNum() {
		return acceptStateNum;
	}

	public void setAcceptStateNum(int acceptStateNum) {
		this.acceptStateNum = acceptStateNum;
	}

	public int getIsCheckedMyPropose() {
		return isCheckedMyPropose;
	}

	public void setIsCheckedMyPropose(int isCheckedMyPropose) {
		this.isCheckedMyPropose = isCheckedMyPropose;
	}

	public List<JecnRtnlPropose> getListAllStatePropose() {
		return listAllStatePropose;
	}

	public void setListAllStatePropose(List<JecnRtnlPropose> listAllStatePropose) {
		this.listAllStatePropose = listAllStatePropose;
	}

	public int getNowState() {
		return nowState;
	}

	public void setNowState(int nowState) {
		this.nowState = nowState;
	}

	public int getIsShowAllReadImg() {
		return isShowAllReadImg;
	}

	public void setIsShowAllReadImg(int isShowAllReadImg) {
		this.isShowAllReadImg = isShowAllReadImg;
	}

	public PagerService getPagerService() {
		return pagerService;
	}

	public void setPagerService(PagerService pagerService) {
		this.pagerService = pagerService;
	}

	public Pager getPager() {
		return pager;
	}

	public void setPager(Pager pager) {
		this.pager = pager;
	}

	public String getTotalRows() {
		return totalRows;
	}

	public void setTotalRows(String totalRows) {
		this.totalRows = totalRows;
	}

	public int get_totalRows() {
		return _totalRows;
	}

	public void set_totalRows(int totalRows) {
		_totalRows = totalRows;
	}

	public String getCurrentPage() {
		return currentPage;
	}

	public void setCurrentPage(String currentPage) {
		this.currentPage = currentPage;
	}

	public String getPagerMethod() {
		return pagerMethod;
	}

	public void setPagerMethod(String pagerMethod) {
		this.pagerMethod = pagerMethod;
	}

	public int getOperationReadBtnState() {
		return operationReadBtnState;
	}

	public void setOperationReadBtnState(int operationReadBtnState) {
		this.operationReadBtnState = operationReadBtnState;
	}

	public String getRtnlProposeId() {
		return rtnlProposeId;
	}

	public void setRtnlProposeId(String rtnlProposeId) {
		this.rtnlProposeId = rtnlProposeId;
	}

	public int getIntflag() {
		return intflag;
	}

	public void setIntflag(int intflag) {
		this.intflag = intflag;
	}

	public String getStrContent() {
		return strContent;
	}

	public void setStrContent(String strContent) {
		this.strContent = strContent;
	}

	public Long getOrgId() {
		return orgId;
	}

	public void setOrgId(Long orgId) {
		this.orgId = orgId;
	}

	public Long getSubmitPeopleId() {
		return submitPeopleId;
	}

	public void setSubmitPeopleId(Long submitPeopleId) {
		this.submitPeopleId = submitPeopleId;
	}

	public String getStartTime() {
		return startTime;
	}

	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}

	public String getEndTime() {
		return endTime;
	}

	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}

	public List<JecnFlowRtnlPropose> getFlowRtnlProposeList() {
		return flowRtnlProposeList;
	}

	public void setFlowRtnlProposeList(List<JecnFlowRtnlPropose> flowRtnlProposeList) {
		this.flowRtnlProposeList = flowRtnlProposeList;
	}

	public JecnFlowRtnlPropose getFlowRtnlPropose() {
		return flowRtnlPropose;
	}

	public void setFlowRtnlPropose(JecnFlowRtnlPropose flowRtnlPropose) {
		this.flowRtnlPropose = flowRtnlPropose;
	}

	public int getIsSelectedHandle() {
		return isSelectedHandle;
	}

	public void setIsSelectedHandle(int isSelectedHandle) {
		this.isSelectedHandle = isSelectedHandle;
	}

	public int getIsSelectedSubmit() {
		return isSelectedSubmit;
	}

	public void setIsSelectedSubmit(int isSelectedSubmit) {
		this.isSelectedSubmit = isSelectedSubmit;
	}

	public int getIsAdiminLogin() {
		return isAdiminLogin;
	}

	public void setIsAdiminLogin(int isAdiminLogin) {
		this.isAdiminLogin = isAdiminLogin;
	}

	public int getIsFlowRtnl() {
		return isFlowRtnl;
	}

	public void setIsFlowRtnl(int isFlowRtnl) {
		this.isFlowRtnl = isFlowRtnl;
	}

	public int getRtnlTypeId() {
		return rtnlTypeId;
	}

	public void setRtnlTypeId(int rtnlTypeId) {
		this.rtnlTypeId = rtnlTypeId;
	}

	public String getRuleFlowName() {
		return ruleFlowName;
	}

	public void setRuleFlowName(String ruleFlowName) {
		this.ruleFlowName = ruleFlowName;
	}

	public String getSearchType() {
		return searchType;
	}

	public void setSearchType(String searchType) {
		this.searchType = searchType;
	}

	public int getRefuseStateNum() {
		return refuseStateNum;
	}

	public void setRefuseStateNum(int refuseStateNum) {
		this.refuseStateNum = refuseStateNum;
	}

	public String getHandleFlag() {
		return handleFlag;
	}

	public void setHandleFlag(String handleFlag) {
		this.handleFlag = handleFlag;
	}

	public IRuleService getRuleService() {
		return ruleService;
	}

	public void setRuleService(IRuleService ruleService) {
		this.ruleService = ruleService;
	}

	public IFlowStructureService getFlowStructureService() {
		return flowStructureService;
	}

	public void setFlowStructureService(IFlowStructureService flowStructureService) {
		this.flowStructureService = flowStructureService;
	}

	public String getPersonIds() {
		return personIds;
	}

	public void setPersonIds(String personIds) {
		this.personIds = personIds;
	}

	public String getOrgIds() {
		return orgIds;
	}

	public void setOrgIds(String orgIds) {
		this.orgIds = orgIds;
	}

	public int getStart() {
		return start;
	}

	public void setStart(int start) {
		this.start = start;
	}

	public int getLimit() {
		return limit;
	}

	public void setLimit(int limit) {
		this.limit = limit;
	}

}
