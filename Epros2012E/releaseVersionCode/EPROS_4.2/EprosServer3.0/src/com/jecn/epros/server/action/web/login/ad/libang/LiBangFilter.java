package com.jecn.epros.server.action.web.login.ad.libang;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import com.jecn.epros.server.action.web.login.ad.libang.LiBangAfterItem;
import com.jecn.epros.server.common.JecnConfigTool;

public class LiBangFilter implements Filter {
	protected static final Logger log = Logger.getLogger(LiBangFilter.class);

	public void init(FilterConfig config) {

	}

	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws ServletException,
			IOException {

		HttpServletRequest httpRequest = (HttpServletRequest) request;
		HttpServletResponse httpResponse = (HttpServletResponse) response;
		if (!JecnConfigTool.isLiBangLoginType() || checkFilteredUrl(httpRequest, httpResponse)) {
			chain.doFilter(httpRequest, httpResponse);
			return;
		}

		String uid = (String) httpRequest.getSession().getAttribute("uid");
		String user = (String) httpRequest.getSession().getAttribute("user");

		if (StringUtils.isNotBlank(uid) && StringUtils.isNotBlank(user)) {
			chain.doFilter(httpRequest, httpResponse);
			return;
		}

		// https://ssotest.nipponpaint.com.cn/profile/oauth2/authorize?client_id=YOUR_CLIENT_ID
		// &redirect_uri=YOUR_REGISTERED_REDIRECT_URI&response_type=code&target_uri=
		// USER_TARGET_REDIRECT_URI
		// 第一步
		String url = getAuthsUrl();
		log.info("立邦权限链接：" + url);
		httpResponse.sendRedirect(url);
	}

	private String getAuthsUrl() {
		StringBuilder b = new StringBuilder();
		b.append(LiBangAfterItem.AUTHS_URL);
		b.append("?");
		b.append("client_id=");
		b.append(LiBangAfterItem.CLIENT_ID);
		b.append("&");
		b.append("redirect_uri=");
		b.append(LiBangAfterItem.LOGIN_URL);
		b.append("&");
		b.append("response_type=code");
		return b.toString();
	}

	private boolean checkFilteredUrl(HttpServletRequest request, HttpServletResponse response) throws IOException,
			ServletException {
		String url = request.getRequestURI();
		if (url.indexOf("oauth_callback") > -1 || url.indexOf("oauth_error") > -1 || url.indexOf("/js") > -1
				|| url.indexOf("/css") > -1 || url.indexOf("/images") > -1 || url.indexOf("/favicon") > -1) {
			return true;
		}
		if (url.indexOf("/login.action") > -1 && request.getParameter("code") != null) {
			return true;
		}
		return false;
	}

	public void destroy() {

	}

}
