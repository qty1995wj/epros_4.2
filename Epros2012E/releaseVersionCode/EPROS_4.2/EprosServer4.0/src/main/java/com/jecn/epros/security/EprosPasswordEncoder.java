package com.jecn.epros.security;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.util.Arrays;

/**
 * 此加密类安全性低，不推荐使用。 但是如果改变加密策略，则需要对所有用户的数据进行升级。
 * 因此建议在适当的时候去掉此类，转用官方推荐的 @BCryptPasswordEncoder
 */
@Deprecated
public class EprosPasswordEncoder implements PasswordEncoder {

    private final Log logger = LogFactory.getLog(getClass());

    @Override
    public String encode(CharSequence rawPassword) {
        return encode(rawPassword.toString());
    }

    @Override
    public boolean matches(CharSequence rawPassword, String encodedPassword) {
        if (encodedPassword == null || encodedPassword.length() == 0) {
            logger.warn("加密后的密码为空");
            return false;
        }

        return encode(rawPassword.toString()).equals(encodedPassword);
    }

    String encode(String rawPassword) {
        String value;
        char[] A1 = new char[40];
        char[] A2 = new char[40];
        int t1[] = {6, 12, 5, 9, 10, 0, 13, 8, 15, 3, 14, 4, 2, 11, 1, 7};
        char t3[] = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F'};
        int i = 0, j = 0, x, y, k;
        int key = 32;
        byte temp;
        A1 = rawPassword.toCharArray();
        k = rawPassword.length();
        while (j < k) {
            temp = (byte) A1[j];
            temp = (byte) ((temp) ^ key);
            A1[j] = (char) temp;
            x = temp / 16;
            y = temp % 16;
            A2[i++] = t3[t1[x]];
            A2[i++] = t3[t1[y]];
            j++;
        }
        char[] result = Arrays.copyOf(A2, A2.length + 1);
        result[result.length - 1] = '\0';
        value = String.valueOf(A2).trim();
        return (value);
    }

}
