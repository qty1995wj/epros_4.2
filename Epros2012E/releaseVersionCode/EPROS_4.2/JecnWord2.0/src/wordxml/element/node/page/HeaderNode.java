package wordxml.element.node.page;

import org.dom4j.Element;

import wordxml.element.dom.IBodyElement;
import wordxml.element.dom.WordElementFactory;
import wordxml.element.dom.page.Header;

/**
 * 页眉dom
 * 
 * @author admin
 * 
 */
public class HeaderNode {
	/**
	 * 创建页眉数据
	 * 
	 * @param header
	 * @param sectPr
	 */
	protected static void createHeader(Header header, Element sectPr) {
		if (header == null || sectPr == null) {
			return;
		}
		Element hdr = sectPr.addElement("w:hdr").addAttribute("w:type",
				header.getType().toString());
		for (IBodyElement bodyEle : header.getBodyElements()) {
			WordElementFactory.produceElement(bodyEle, hdr);
		}
	}
}
