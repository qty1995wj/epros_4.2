package com.jecn.epros.server.common;

import java.util.List;

import org.apache.commons.lang3.StringUtils;

import com.jecn.epros.server.bean.system.JecnDictionary;
import com.jecn.epros.server.bean.system.JecnElementsLibrary;
import com.jecn.epros.server.common.JecnConfigContents.ConfigItemPartMapMark;
import com.jecn.epros.server.util.JecnUtil;
import com.jecn.epros.server.util.JecnDictionaryEnumConstant.DictionaryEnum;

public class JecnConfigTool {

	/**
	 * ture：单点登录类型为十所
	 * 
	 * @return
	 */
	public static boolean isCect10Login() {
		return "24".equals(getItemValue(ConfigItemPartMapMark.otherLoginType.toString()));
	}

	/**
	 * 九新药业
	 * 
	 * @return
	 */
	public static boolean isJiuXinLoginType() {
		return "47".equals(getItemValue(ConfigItemPartMapMark.otherLoginType.toString()));
	}

	public static String getItemValue(String mark) {
		return JecnContants.getValue(mark);
	}

	/**
	 * 是否显示记录保存的移交责任人和编号
	 */
	public static boolean isShowRecordTransferAndNum() {
		return true;
	}

	/**
	 * 任务交办
	 * 
	 * @return
	 */
	public static String isShowOperationAssigned() {
		return getItemValue(ConfigItemPartMapMark.taskOperationAssigned.toString());
	}

	/**
	 * 任务转批
	 * 
	 * @return
	 */
	public static String isShowOperationTransfer() {
		return getItemValue(ConfigItemPartMapMark.taskOperationTransfer.toString());
	}

	/**
	 * 重命名后直接发布
	 * 
	 * @return
	 */
	public static boolean isAllowedPubReName() {
		return isShowItem(ConfigItemPartMapMark.isAllowedPubReName);
	}

	/**
	 * true:显示
	 * 
	 * @param mark
	 * @return
	 */
	public static boolean isShowItem(ConfigItemPartMapMark mark) {
		String value = getItemValue(mark.toString());
		return "1".equals(value) ? true : false;
	}

	/**
	 * 立邦登录类型
	 * 
	 * @return
	 */
	public static boolean isLiBangLoginType() {
		return 28 == JecnContants.otherLoginType;
	}

	/**
	 * 是否显示流程架构-编号
	 * 
	 * @return true:显示，false 不显示
	 */
	public static boolean isShowProcessMapNum() {
		return isLiBangLoginType() ? false : true;
	}

	public static boolean isSendFlowDriverEmail() {
		return isShowItem(ConfigItemPartMapMark.mailFlowDriverTip);
	}
	
	public static boolean isSendFlowDriverEmailToTip() {
		return isShowItem(ConfigItemPartMapMark.mailFlowDriverProcessFileTip);
	}

	public static boolean favoriteUpdateSendEmail() {
		return isShowItem(ConfigItemPartMapMark.mailFavoriteUpdate);
	}

	/**
	 * 流程架构是否允许维护文档管理
	 * 
	 * @return
	 */
	public static boolean isArchitectureUploadFile() {
		return isShowItem(ConfigItemPartMapMark.isArchitectureUploadFile);
	}

	/**
	 * true：不启用4.0版本服务
	 * 
	 * @return
	 */
	public static boolean isNotEnableNewApp() {
		// return true;
		return !isShowItem(ConfigItemPartMapMark.isEbableNewApp);
	}

	/**
	 * 新版服务地址
	 * 
	 * @return
	 */
	public static String getNewAppContext() {
		return JecnContants.NEW_APP_CONTEXT;
	}

	/**
	 * 新版服务添加：cookie 地址
	 * 
	 * @return
	 */
	public static String getCookieDomain() {
		return JecnContants.COOKIE_DOMAIN;
	}

	public static String getNewProjectName() {
		return JecnContants.NEW_PROJECT_NAME;
	}

	/**
	 * 监护人是否显示
	 * 
	 * @return
	 */
	public static boolean flowGuardianPeopleIsShow() {
		String value = getItemValue(ConfigItemPartMapMark.guardianPeople.toString());
		if ("1".equals(value) || "2".equals(value)) {
			return true;
		}
		return false;
	}

	/**
	 * 制度监护人是否显示
	 * 
	 * @return
	 */
	public static boolean ruleGuardianPeopleIsShow() {
		String value = getItemValue(ConfigItemPartMapMark.ruleGuardianPeople.toString());
		if ("1".equals(value) || "2".equals(value)) {
			return true;
		}
		return false;
	}

	/**
	 * 流程拟制人是否显示
	 * 
	 * @return
	 */
	public static boolean fictionPeopleIsShow() {
		String value = getItemValue(ConfigItemPartMapMark.fictionPeople.toString());
		if ("1".equals(value) || "2".equals(value)) {
			return true;
		}
		return false;
	}
	
	/**
	 * 制度拟制人是否显示
	 * 
	 * @return
	 */
	public static boolean ruleFictionPeopleIsShow() {
		String value = getItemValue(ConfigItemPartMapMark.ruleFictionPeople.toString());
		if ("1".equals(value) || "2".equals(value)) {
			return true;
		}
		return false;
	}

	/**
	 * 指标来源是否显示支持一级指标是否显示相关度三属性是否显示
	 * 
	 * @return
	 */
	public static boolean kpiPropertyIsShowIsShow() {
		String value = getItemValue(ConfigItemPartMapMark.showKPIProperty.toString());
		if ("1".equals(value) || "2".equals(value)) {
			return true;
		}
		return false;
	}

	/**
	 * 东软医疗
	 * 
	 * @return
	 */
	public static boolean isDongRuanYiLiaoLoginType() {
		return 27 == JecnContants.otherOperaType;
	}

	/**
	 * 邮箱端口
	 * 
	 * @return
	 */
	public static String getEmailPort() {
		return JecnContants.getValue(ConfigItemPartMapMark.mailBoxOuterPort.toString());
	}

	/**
	 * 邮箱服务地址
	 * 
	 * @return
	 */
	public static String getEmailSmtp() {
		return JecnContants.getValue(ConfigItemPartMapMark.mailBoxOuterServerAdd.toString());
	}

	public static String getEmailInnerAdd() {
		return JecnContants.getValue(ConfigItemPartMapMark.mailBoxInnerMailAddr.toString());
	}

	/**
	 * 是否通过webservice 发布待办
	 * 
	 * @return
	 */
	public static boolean isRelatedTaskByWebService() {
		// 厦门金龙
		return JecnContants.otherLoginType == 31;
	}

	/**
	 * 3.0 服务URL
	 * 
	 * @return
	 */
	public static String getBaseEprosUrl() {
		return JecnContants.getValue(ConfigItemPartMapMark.basicEprosURL.toString());
	}

	/**
	 * 厦门金旅
	 * 
	 * @return
	 */
	public static boolean isGoldenDragonLoginType() {
		return 31 == JecnContants.otherLoginType;
	}

	/**
	 * 火狐浏览器跳转（默认IE浏览器请求，处理后跳转到火狐）
	 * 
	 * @return
	 */
	public static boolean isFirefoxRequest() {
		return 23 == JecnContants.otherLoginType || 39 == JecnContants.otherLoginType;
	}

	/**
	 * 操作说明适用范围
	 * 
	 * @return
	 */
	public static boolean applyScopeIsShow() {
		String value = getItemValue(ConfigItemPartMapMark.a2.toString());
		if ("1".equals(value) || "2".equals(value)) {
			return true;
		}
		return false;
	}

	/**
	 * 操作说明驱动规则
	 * 
	 * @return
	 */
	public static boolean driverIsShow() {
		String value = getItemValue(ConfigItemPartMapMark.a4.toString());
		if ("1".equals(value) || "2".equals(value)) {
			return true;
		}
		return false;
	}

	/**
	 * 是否自动编号
	 * 
	 * @return
	 */
	public static boolean isStandardizedAutoCode() {
		return JecnContants.otherLoginType == 35;
	}

	/**
	 * 人员同步，只同步人员
	 * 
	 * @return
	 */
	public static boolean isSyncUserOnly() {
		return isShowItem(ConfigItemPartMapMark.onlySyncUser);
	}

	public static boolean isShowOrgScope() {
		return isShowItem(ConfigItemPartMapMark.showApplicabilitySeleteDept);
	}

	public static boolean isYG() {
		return JecnContants.otherOperaType == 29;
	}

	/**
	 * 是否预览文件自动编号
	 * 
	 * @return
	 */
	public static boolean isMengNiuViewFileCode() {
		return isMengNiuLogin();
	}

	public static boolean isMengNiuLogin() {
		return 37 == JecnContants.otherLoginType;
	}

	/**
	 * 是否启用office
	 * 
	 * @return
	 */
	public static boolean isEnableOffice() {
		return isShowItem(ConfigItemPartMapMark.isOfficeTransformFile);
	}

	/**
	 * 是否显示下载的流程文件流水号
	 * 
	 * @return
	 */
	public static boolean isShowDownLoadWordIndex() {
		if (19 == JecnContants.otherLoginType) {// 烽火
			return false;
		}
		return true;
	}

	/**
	 * 办理时限-活动明细
	 */
	public static boolean showActTimeLine() {
		return isShowItem(ConfigItemPartMapMark.isShowActiveTimeValue);
	}

	/**
	 * 活动办理时限单位名称
	 * 
	 * @return
	 */
	public static String getActiveTimeLineUtil() {
		return getItemValue(ConfigItemPartMapMark.activeTimeLineUtil.toString());
	}

	public static boolean isAppRequest() {
		return isShowItem(ConfigItemPartMapMark.isAppRequest);
	}

	public static boolean isSyncPostionNotExistsDept() {
		return true;
	}

	public static boolean isJinFeng() {
		if (32 == JecnContants.otherLoginType) {// 金风
			return false;
		}
		return true;
	}

	/**
	 * 制度是否允许维护支撑文件
	 * 
	 * @return
	 */
	public static boolean isRuleArchitectureUploadFile() {
		return isShowItem(ConfigItemPartMapMark.isRuleUploadFile);
	}

	public static boolean isShowPosFullPath() {
		return true;
	}

	public static JecnDictionary getDictinary(DictionaryEnum item, Integer code) {
		List<JecnDictionary> dics = getDictionaryList(item);
		for (JecnDictionary dic : dics) {
			if (dic.getCode().equals(code)) {
				return dic;
			}
		}
		return null;
	}

	public static List<JecnDictionary> getDictionaryList(DictionaryEnum dictionaryEnum) {
		return JecnContants.dictionaryMap.get(dictionaryEnum);
	}

	/**
	 * true：岗位组，false ：岗位
	 * 
	 * @return
	 */
	public static boolean processFileShowPostionType() {
		return !isShowItem(ConfigItemPartMapMark.processFileShowPositionType);
	}

	public static String getRiskTypeStr(int riskType) {
		return getDicValue(DictionaryEnum.JECN_RISK_TYPE, riskType);
	}

	public static String getSectLevelStr(int sectLevel) {
		return getDicValue(DictionaryEnum.JECN_SECURITY_LEVEL, sectLevel);
	}

	public static String getDicValue(DictionaryEnum dicEnum, Integer code) {
		if (code == null) {
			return "";
		}
		JecnDictionary dic = JecnConfigTool.getDictinary(dicEnum, code);
		return dic == null ? "" : JecnUtil.nullToEmpty(dic.getValue());
	}

	/**
	 * 通过接口发送待办消息
	 * 
	 * @date 2018-1-29 10所、科大讯飞、和而泰、木林森、招商证券、万华
	 * @return
	 */
	public static boolean sendTaskByInterface() {
		return JecnContants.otherLoginType == 43 || JecnContants.otherLoginType == 41
				|| JecnContants.otherLoginType == 42 || JecnContants.otherLoginType == 46
				|| JecnConfigTool.isCect10Login() || JecnConfigTool.isMengNiuLogin()
				|| JecnContants.otherLoginType == 47 || JecnContants.otherLoginType == 19
				|| JecnContants.otherLoginType == 40 || JecnContants.otherLoginType == 54 || JecnContants.otherLoginType == 28 ||JecnConfigTool.isJinKe();
	}

	public static boolean newBackCollateEnable() {
		return backCollateEnable() && useNewBackCollate();
	}

	public static boolean oldBackCollateEnable() {
		return backCollateEnable() && useOldBackCollate();
	}

	/**
	 * 打回整理是否可用
	 * 
	 * @return
	 */
	public static boolean backCollateEnable() {
		return isShowItem(ConfigItemPartMapMark.enableNewCollate);
	}

	/**
	 * 使用新版本的打回整理
	 * 
	 * @return
	 */
	public static boolean useNewBackCollate() {
		return isShowItem(ConfigItemPartMapMark.taskOperationCollate);
	}

	/**
	 * 适用旧版本的打回整理
	 * 
	 * @return
	 */
	public static boolean useOldBackCollate() {
		return !useNewBackCollate();
	}

	/**
	 * 流程文件活动中是否显示IT系统
	 */
	public static boolean processFileActivityShowIT() {
		return isShowItem(ConfigItemPartMapMark.processFileActivityShowIT);
	}

	/**
	 * 发布给有查阅权限的是否发送邮件
	 * 
	 * @return
	 */
	public static boolean pubToPermSendEmail() {
		return isShowItem(ConfigItemPartMapMark.emailPulishCompetence);
	}

	/**
	 * 发布给固定收件人发送邮件
	 */
	public static boolean pubToFixedSendEmail() {
		return isShowItem(ConfigItemPartMapMark.mailPubToFixed);
	}

	/**
	 * 发布给管理员发送邮件
	 */
	public static boolean pubToAdminSendEmail() {
		return isShowItem(ConfigItemPartMapMark.mailPubToAdmin);
	}

	/**
	 * 流程元素是否启用
	 * 
	 * @return
	 */
	public static boolean elementEnable(IBaseDao baseDao, String mark) {
		try {
			String hql = "from JecnElementsLibrary where mark=?";
			JecnElementsLibrary e = (JecnElementsLibrary) baseDao.getObjectHql(hql, mark);
			return e.getValue() == 1;
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	public static boolean KCPFigureCompEnable(IBaseDao baseDao) {
		return elementEnable(baseDao, "KCPFigureComp");
	}

	/**
	 * 文件在全目录校验是否存在
	 * 
	 * @return
	 */
	public static boolean fileValidateGlobal() {
		return false;
	}

	/**
	 * 是否显示文本输入的logo
	 * 
	 * @return
	 */
	public static boolean isShowActiveTextPutLogo() {
		return isShowItem(ConfigItemPartMapMark.isShowActivtyTextInOutIcon);
	}

	/**
	 * 4.0服务配置项同步地址
	 * 
	 * @return
	 */
	public static String getServer4SystemConfigAddress() {
		return getNewAppServerAddress() + JecnContants.SERVER4_SYSTEM_CONFIG;
	}

	/**
	 * 新版服务后台地址
	 * 
	 * @return
	 */
	public static String getNewAppServerAddress() {
		return JecnContants.NEW_APP_SERVER_ADDRESS;
	}

	/**
	 * 是否使用新版本的输入输出
	 * 
	 * @return
	 */
	public static boolean useNewInout() {
		return isShowItem(ConfigItemPartMapMark.useNewInout);
		// return false;
	}

	public static boolean isOpenSyncFileFromOrg() {
		return isShowItem(ConfigItemPartMapMark.isOpenSyncFileFromOrg);
	}

	public static boolean isPrmissionisParentNode() {
		return isShowItem(ConfigItemPartMapMark.permissionisParentNode);
	}

	public static boolean expiryRecursionEnable() {
		return isShowItem(ConfigItemPartMapMark.expiryRecursion);
	}

	public static boolean isUseNewDriver() {
		return true;
	}

	/**
	 * 驱动规则是否显示时间
	 * 
	 * @return
	 */
	public static boolean isShowDriverTime() {
		return isShowItem(ConfigItemPartMapMark.isShowDateFlowFile);
	}

	/**
	 * 科大讯飞
	 * 
	 * @return
	 */
	public static boolean isIflytekLogin() {
		return 40 == JecnContants.otherLoginType;
	}

	public static boolean isJinKe() {
		return JecnContants.otherLoginType == 50;
	}

	public static String getEmailPlatName() {
		String result = JecnContants.getValue(ConfigItemPartMapMark.mailPlatName.toString());
		if (StringUtils.isBlank(result)) {
			return "EPROS流程体系管理平台";
		}
		return result;
	}

	/**
	 * 发布给参与人发邮件
	 * 
	 * @return
	 */
	public static boolean pubToJoinSendEmail() {
		return isShowItem(ConfigItemPartMapMark.emailPulishParticipants);
	}

	/**
	 * 元素链接显示位置：0 中上 ，1 右下 默认中上
	 * 
	 * @return
	 */
	public static String getElementLocation() {
		return JecnContants.getValue(ConfigItemPartMapMark.elementLocation.toString());
	}
	
	/**
	 * 浏览端是否显示 办理时限 时间轴
	 * 
	 * @return
	 */
	public static boolean isShowTimeLimit() {
		return isShowItem(ConfigItemPartMapMark.isShowTimeLimit);
	}

	/**
	 * 流程文件 定义是否 启用了 相关制度
	 * 
	 * @return
	 */
	public static boolean isEnableRule() {
		return isShowItem(ConfigItemPartMapMark.a14);
	}

	/**
	 * 流程文件定义中是否启用了 流程标准化文件
	 * 
	 * @return
	 */
	public static boolean isEnableStandardizedFile() {
		return isShowItem(ConfigItemPartMapMark.a37);
	}

	/**
	 * 流程文件定义中是否启用了 相关标准
	 * 
	 * @return
	 */
	public static boolean isEnableStandardized() {
		return isShowItem(ConfigItemPartMapMark.a21);
	}
}
