package epros.designer.gui.system.fileDescription;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;

import javax.swing.JLabel;

import epros.designer.gui.common.JecnTextArea;
import epros.designer.gui.process.guide.explain.GuideJScrollPane;
import epros.designer.util.JecnValidateCommon;
import epros.draw.gui.swing.JecnPanel;
import epros.draw.util.JecnUIUtil;

/**
 * 操作说明只包含一个JTextArea的通用组件，非操作说明不得继承
 * 
 * @author user
 * 
 */
public class JecnFileDescriptionArea extends JecnFileDescriptionComponent {

	protected JecnTextArea jecnArea = new JecnTextArea();

	private boolean isFourLenght = false;
	private String text = "";

	public JecnFileDescriptionArea(int index, String paragrapHeadingName, JecnPanel contentPanel, boolean isRequest,
			boolean isFourLenght, String text) {
		this(index, paragrapHeadingName, contentPanel, isRequest, isFourLenght, text, "");
	}

	/**
	 * 
	 * @param index
	 *            标题序号
	 * @param paragrapHeadingName
	 *            标题名称
	 * @param rows
	 *            行数
	 * @param insets
	 *            边距
	 * @param contentPanel
	 * @param isRequest
	 *            是否必填
	 * @param isFourLenght
	 *            是否是有4000长度限制
	 * @param text
	 *            内容
	 */
	public JecnFileDescriptionArea(int index, String paragrapHeadingName, JecnPanel contentPanel, boolean isRequest,
			boolean isFourLenght, String text, String tip) {
		super(index, paragrapHeadingName, isRequest, contentPanel, tip);
		jecnArea.setRows(4);
		jecnArea.setBorder(null);
		centerPanel.setLayout(new GridBagLayout());
		GridBagConstraints c = new GridBagConstraints(0, 0, 1, 1, 1.0, 1.0, GridBagConstraints.NORTHWEST,
				GridBagConstraints.BOTH, insets, 0, 0);
		GuideJScrollPane scrollPanel = new GuideJScrollPane();
		centerPanel.add(scrollPanel, c);
		scrollPanel.setViewportView(jecnArea);
		scrollPanel.getViewport().setBackground(JecnUIUtil.getDefaultBackgroundColor());
		this.isFourLenght = isFourLenght;
		this.text = text == null ? "" : text;
		jecnArea.setText(this.text);
	}

	public JecnFileDescriptionArea(int index, String paragrapHeadingName, JecnPanel contentPanel, boolean isRequest,
			boolean isEditAble, boolean isFourLenght, String text, String tip) {
		this(index, paragrapHeadingName, contentPanel, isRequest, isFourLenght, text, tip);
		jecnArea.setEditable(isEditAble);
	}

	public boolean isUpdate() {
		if (!text.equals(jecnArea.getText())) {
			return true;
		}
		return false;
	}

	public String getResultStr() {
		return jecnArea.getText().trim();
	}

	@Override
	public boolean isValidateNotPass(JLabel verfyLab) {
		if (isFourLenght) {
			return JecnValidateCommon.validateAreaNotPass(jecnArea.getText(), verfyLab, this.name);
		}
		return false;
	}

	public JecnTextArea getJecnArea() {
		return jecnArea;
	}
}
