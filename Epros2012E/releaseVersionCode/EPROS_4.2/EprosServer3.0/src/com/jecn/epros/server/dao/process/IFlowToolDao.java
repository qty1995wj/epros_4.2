package com.jecn.epros.server.dao.process;

import java.util.List;

import com.jecn.epros.server.bean.process.JecnFlowSustainTool;
import com.jecn.epros.server.common.IBaseDao;

public interface IFlowToolDao extends IBaseDao<JecnFlowSustainTool, Long> {

	List<Object[]> getFlowTools(Long flowId, String pub) throws Exception;
}
