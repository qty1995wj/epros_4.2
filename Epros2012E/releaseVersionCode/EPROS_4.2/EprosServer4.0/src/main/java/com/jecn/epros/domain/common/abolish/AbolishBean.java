package com.jecn.epros.domain.common.abolish;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.jecn.epros.domain.LinkResource;
import com.jecn.epros.domain.common.relatedfiles.RelatedFilesType;
import com.jecn.epros.service.TreeNodeUtils;
import com.jecn.epros.util.JecnProperties;

import java.util.Date;

/**
 * Created by Administrator on 2017\12\5 0005.
 */
public class AbolishBean {

    private Long id;//关联Id

    private String name; //名称

    private String type; //类型

    private String abolishType;//废止类型

    private String dir;//所属目录

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm", timezone = "GMT+08")
    private Date abolishTime;//废止时间

    private String orgName;//所属部门

    private Long taskId;//任务Id

    private Long historyId;//文控Id

    public LinkResource getLink() {
        return new LinkResource(id, name, LinkResource.ResourceType.valueOf(type), TreeNodeUtils.NodeType.Abolish);
    }

    public LinkResource getLinks() {
        //查阅
        return new LinkResource(taskId, JecnProperties.getValue("consult"), LinkResource.ResourceType.TASK_VIEW, TreeNodeUtils.NodeType.Abolish);
    }

    public String getAbolishType() {
        return abolishType;
    }

    public void setAbolishType(String abolishType) {
        this.abolishType = abolishType;
    }

    public Long getTaskId() {
        return taskId;
    }

    public void setTaskId(Long taskId) {
        this.taskId = taskId;
    }

    public Long getHistoryId() {
        return historyId;
    }

    public void setHistoryId(Long historyId) {
        this.historyId = historyId;
    }

    public String getDir() {
        return dir;
    }

    public void setDir(String dir) {
        this.dir = dir;
    }

    public Date getAbolishTime() {
        return abolishTime;
    }

    public void setAbolishTime(Date abolishTime) {
        this.abolishTime = abolishTime;
    }

    public String getOrgName() {
        return orgName;
    }

    public void setOrgName(String orgName) {
        this.orgName = orgName;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
