package com.jecn.epros.server.download.wordxml.zhuzhouzhongche;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import wordxml.constant.Constants;
import wordxml.constant.Constants.Align;
import wordxml.constant.Constants.DocGridType;
import wordxml.constant.Constants.HeaderOrFooterType;
import wordxml.constant.Constants.LineRuleType;
import wordxml.constant.Constants.LineStyle;
import wordxml.constant.Constants.PStyle;
import wordxml.element.bean.common.PositionBean;
import wordxml.element.bean.page.DocGrid;
import wordxml.element.bean.page.SectBean;
import wordxml.element.bean.paragraph.ParagraphBean;
import wordxml.element.bean.paragraph.ParagraphLineRule;
import wordxml.element.bean.table.TableBean;
import wordxml.element.dom.graph.Image;
import wordxml.element.dom.graph.LineGraph;
import wordxml.element.dom.page.Header;
import wordxml.element.dom.page.Sect;
import wordxml.element.dom.paragraph.Paragraph;
import wordxml.element.dom.table.Table;

import com.jecn.epros.server.bean.download.ProcessDownloadBean;
import com.jecn.epros.server.bean.process.ProcessAttributeBean;
import com.jecn.epros.server.bean.task.JecnTaskHistoryFollow;
import com.jecn.epros.server.bean.task.JecnTaskHistoryNew;
import com.jecn.epros.server.common.JecnContants;
import com.jecn.epros.server.download.wordxml.ProcessFileItem;
import com.jecn.epros.server.download.wordxml.ProcessFileModel;
import com.jecn.epros.server.util.JecnUtil;

/**
 * 株洲中车时代
 * 
 * @author admin
 * 
 */
public class ZZZCProcessModel extends ProcessFileModel {
	/*** 段落行距单倍倍行距 *****/
	private ParagraphLineRule vLine1 = new ParagraphLineRule(1F, LineRuleType.AUTO);

	private final String PROCESS_NUMBER_PREFIX = "Q/TEG";

	public ZZZCProcessModel(ProcessDownloadBean processDownloadBean, String path) {
		super(processDownloadBean, path, true);
		setDocStyle("", textTitleStyle());
		/***** 段落空格换行属性bean 宋体 小四 最小值16磅值 *******/
		// newLineBean = new ParagraphBean(Align.left, new FontBean("黑体",
		// Constants.wuhao, true));
		// newLineBean.setSpace(0f, 0f, vLine1);
		docProperty.setNewTblWidth(17.49F);
		docProperty.setTblTitleShadow(true);
	}

	/**
	 * 获取默认的节属性
	 * 
	 * @return
	 */
	private SectBean getSectBean() {
		SectBean sectBean = new SectBean();
		sectBean.setDocGrid(new DocGrid(DocGridType.LINES, 16.3F));
		// 设置页边距
		sectBean.setPage(1.5F, 2F, 1.5F, 1.7F, 1F, 1.2F);
		sectBean.setSize(21, 29.7F);
		return sectBean;
	}

	/*****
	 * 创建表格项
	 * 
	 * @param contentSect
	 * @param fs
	 * @param list
	 * @return
	 */
	@Override
	protected Table createTableItem(ProcessFileItem processFileItem, float[] fs, List<String[]> list) {
		// processFileItem.getBelongTo().createParagraph("", newLineBean);
		Table tab = super.createTableItem(processFileItem, fs, list);
		// processFileItem.getBelongTo().createParagraph("", newLineBean);
		return tab;
	}

	/**
	 * 创建文本项
	 * 
	 * @param titleName
	 * @param contentSect
	 * @param content
	 */
	@Override
	protected void createTextItem(ProcessFileItem processFileItem, String... content) {
		super.createTextItem(processFileItem, content);
		// processFileItem.getBelongTo().createParagraph("", newLineBean);
	}

	/**
	 * 添加封面节点数据
	 */
	private void addTitleSectContent() {

		Sect titleSect = docProperty.getTitleSect();
		// 文件编号
		String flowInputNum = nullToEmpty(processDownloadBean.getFlowInputNum());
		// 文件版本
		String flowVersion = nullToEmpty(processDownloadBean.getFlowVersion());
		// 　流程名称
		String flowName = nullToEmpty(processDownloadBean.getFlowName());
		// 公司名称
		String companyName = "株洲中车时代电气股份有限公司企业标准";
		// 宋体 粗号 居右对齐 单倍行距
		ParagraphBean pBean = new ParagraphBean(Align.right, "Times New Roman", Constants.sishiba, true);
		pBean.setSpace(0f, 0f, vLine1);
		titleSect.createParagraph("Q/TEG", pBean);

		// 宋体 一号 居中对齐 单倍行距
		pBean = new ParagraphBean(Align.center, "黑体", Constants.yihao, false);
		pBean.setSpace(0f, 0f, vLine1);
		titleSect.createParagraph(companyName, pBean);

		// Q/TEG 流程编号 黑体十四号
		pBean = new ParagraphBean(Align.right, "黑体", Constants.shisihao, false);
		docProperty.getTitleSect().createParagraph(nullToEmpty(flowInputNum), pBean);

		// 单线条
		LineGraph line = new LineGraph(LineStyle.single, 0.75F);
		line.setFrom(0.75F, 1.15F);
		line.setTo(500.2F, 1.15F);
		PositionBean positionBean = new PositionBean();
		positionBean.setMarginLeft(0);
		line.setPositionBean(positionBean);
		titleSect.createParagraph(line);

		titleSect.createMutilEmptyParagraph(5);

		// 竞争对手信息管理办法 黑体 26
		pBean = new ParagraphBean(Align.center, "黑体", 52, false);
		pBean.setSpace(0f, 0f, vLine1);
		titleSect.createParagraph(flowName, pBean);

		// （流程版本号V1.0）
		pBean = new ParagraphBean(Align.center, "黑体", 44, false);
		titleSect.createParagraph("（流程版本号" + flowVersion + "）", pBean);

		// 2016 - 04 - 11发布 黑体 14
		pBean = new ParagraphBean(Align.left, "黑体", 28, false);
		String pubDateStr = processDownloadBean.getReleaseDate();
		if (pubDateStr.length() > 10) {
			pubDateStr = pubDateStr.substring(0, 10);
		}
		// 2016 - 05 - 01实施 黑体 14
		String nextMonthFirstDay = "";
		if (StringUtils.isNotBlank(pubDateStr)) {

			try {
				// 下一月第一天
				SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
				Date pubDate = format.parse(pubDateStr);
				Calendar cal = Calendar.getInstance();
				cal.setTime(pubDate);
				cal.add(Calendar.MONTH, 1);
				cal.set(Calendar.DAY_OF_MONTH, 1);
				nextMonthFirstDay = format.format(cal.getTime());

			} catch (ParseException e) {
				log.error(e);
			}

		}

		// 换行
		titleSect.createMutilEmptyParagraph(21);

		// 14个\t
		String space = "                                         ";
		titleSect.createParagraph(pubDateStr + "发布" + space + nextMonthFirstDay + "实施", pBean);

		// 横线
		// 单线条
		line = new LineGraph(LineStyle.single, 0.75F);
		line.setFrom(0.75F, -5F);
		line.setTo(500.2F, -5F);
		positionBean = new PositionBean();
		positionBean.setMarginLeft(0);
		line.setPositionBean(positionBean);
		titleSect.createParagraph(line);

		// 株洲中车时代电气股份有限公司 发布 黑体14
		pBean = new ParagraphBean(Align.center, "黑体", 28, false);
		titleSect.createParagraph("株 洲 中 车 时 代 电 气 股 份 有 限 公 司        发   布", pBean);
	}

	private void addQianYanSectContent(Sect firstSect) {

		ParagraphBean pBean = new ParagraphBean(Align.center, "黑体", Constants.sanhao, false);
		ParagraphLineRule vLine = new ParagraphLineRule(4.5F, LineRuleType.AUTO);
		pBean.setSpace(0f, 0f, vLine);
		firstSect.createParagraph("前  言", pBean);

		ProcessAttributeBean responFlow = processDownloadBean.getResponFlow();
		String dutyOrg = nullToEmpty(responFlow.getDutyOrgName());
		String dutyUser = nullToEmpty(responFlow.getDutyUserName());
		// 部门审核人
		String orgApprove = "";
		// 评审人会审
		String huiShengApprove = "";
		// IT
		String itApprove = "";
		// 批准人
		String piZhunApprove = "";
		// 事业部
		String shiYeBuApprove = "";
		// 分隔符
		String splitChar = "，";
		JecnTaskHistoryNew curHistory = processDownloadBean.getLatestHistoryNew();
		if (curHistory != null && curHistory.getListJecnTaskHistoryFollow() != null) {
			for (JecnTaskHistoryFollow approve : curHistory.getListJecnTaskHistoryFollow()) {

				Integer mark = approve.getStageMark();
				String approveName = approve.getName();
				if (StringUtils.isBlank(approveName)) {
					continue;
				}
				if (mark == 2) {
					orgApprove += splitChar + approveName;
				} else if (mark == 7) {
					itApprove += splitChar + approveName;
				} else if (mark == 4) {
					piZhunApprove += splitChar + approveName;
				} else if (mark == 8) {
					shiYeBuApprove += splitChar + approveName;
				} else if (mark == 3) {
					huiShengApprove += splitChar + approveName;
				}

			}
		}

		orgApprove = orgApprove.replaceFirst(splitChar, "");
		itApprove = itApprove.replaceFirst(splitChar, "");
		piZhunApprove = piZhunApprove.replaceFirst(splitChar, "");
		shiYeBuApprove = shiYeBuApprove.replaceFirst(splitChar, "");
		huiShengApprove = huiShengApprove.replaceFirst(splitChar, "");
		String perVersion = "";
		if (processDownloadBean.getHistorys() != null && processDownloadBean.getHistorys().size() > 1) {
			perVersion = processDownloadBean.getHistorys().get(1).getVersionId();
		}
		// 流程编号+上个版本号
		String numberAndPerHistoryVersion = nullToEmpty(processDownloadBean.getFlowInputNum()) + " "
				+ nullToEmpty(perVersion);
		JecnTaskHistoryNew latestHistoryNew = processDownloadBean.getLatestHistoryNew();
		List<String> contents = new ArrayList<String>();
		contents.add("本流程按照GB/T 1.1-2009给出的规则起草。");
		contents.add("本流程代替" + numberAndPerHistoryVersion + "，本次修订的重点和主要变化如下：");
		contents.add("--" + nullToEmpty(latestHistoryNew == null ? "" : latestHistoryNew.getModifyExplain()));
		contents.add("本流程由" + dutyOrg + "提出并归口");
		contents.add("本流程的流程所有者：" + dutyUser);
		contents.add("本标准主要起草人：");
		contents.add("本流程的审核人：" + orgApprove + "  " + huiShengApprove);
		contents.add("本流程的会签人：" + itApprove + "  " + shiYeBuApprove);
		contents.add("本流程的审批人：" + piZhunApprove);

		pBean = new ParagraphBean(Align.left, "宋体", Constants.wuhao, false);
		vLine = new ParagraphLineRule(1F, LineRuleType.AUTO);
		pBean.setSpace(0f, 0.5f, vLine);

		firstSect.createParagraph(contents.toArray(new String[9]), pBean);

		// 添加分页符号
		firstSect.addParagraph(new Paragraph(true));
	}

	/**
	 * 添加第一节前面的数据
	 */
	private void addFirstSectContent() {
		// 流程名称
		String flowName = processDownloadBean.getFlowName();
		// 黑体 三号
		ParagraphBean pBean = new ParagraphBean(Align.center, "黑体", Constants.sanhao, false);
		ParagraphLineRule line = new ParagraphLineRule(1F, LineRuleType.AUTO);
		pBean.setSpace(1.5f, 1.5f, line);
		getDocProperty().getFirstSect().createParagraph(flowName, pBean);

	}

	/**
	 * 活动说明
	 * 
	 * @param _count
	 * @param name
	 * @param allActivityShowList
	 */
	@Override
	protected void a10(ProcessFileItem processFileItem, String[] titles, List<String[]> rowData) {

		// 段落显示 1:段落；0:表格（默认）
		if (processDownloadBean.getFlowFileType() == 1) {
			a10InParagraph(processFileItem, rowData);
			return;
		}
		// 修改内容项显示的顺序
		List<String[]> newRowData = new ArrayList<String[]>();
		String[] newRowArr;
		for (String[] arr : rowData) {
			newRowArr = new String[6];
			newRowArr[0] = arr[0];
			newRowArr[1] = arr[2];
			newRowArr[2] = arr[4];
			newRowArr[3] = arr[3];
			newRowArr[4] = arr[5];
			newRowArr[5] = arr[1];
			newRowData.add(newRowArr);
		}
		// 标题
		createTitle(processFileItem);
		titles = new String[] { "编号", "流程活动", "输入", "流程活动描述", "输出", "责任角色" };
		// 添加标题
		newRowData.add(0, titles);
		float[] width = new float[] { 1.12F, 1.8F, 2.4F, 6.8F, 2.4F, 1.8F };
		Table tbl = createTableItem(processFileItem, width, newRowData);
		// 设置第一行居中
		tbl.setRowStyle(0, docProperty.getTblTitleStyle(), docProperty.isTblTitleShadow(), docProperty
				.isTblTitleCrossPageBreaks());
	}

	@Override
	protected void initFirstSect(Sect firstSect) {
		// 添加前言节点数据
		firstSect.setSectBean(getSectBean());
		addQianYanSectContent(firstSect);
		addFirstSectContent();
		createCommhdr(firstSect);
	}

	@Override
	protected void initFlowChartSect(Sect flowChartSect) {
		SectBean sectBean = getSectBean();
		sectBean.setHorizontal(true);
		sectBean.setSize(29.7F, 21);
		flowChartSect.setSectBean(sectBean);
		createCommhdr(flowChartSect);
	}

	@Override
	protected void initSecondSect(Sect secondSect) {
		secondSect.setSectBean(getSectBean());
		createCommhdr(secondSect);
	}

	@Override
	protected void initTitleSect(Sect titleSect) {
		titleSect.setSectBean(getSectBean());
		// 添加封皮节点数据
		addTitleSectContent();
	}

	@Override
	public ParagraphBean tblContentStyle() {
		ParagraphBean tblContentStyle = new ParagraphBean("宋体", Constants.wuhao, false);
		tblContentStyle.setSpace(0f, 0f, vLine1);
		return tblContentStyle;
	}

	@Override
	public TableBean tblStyle() {
		TableBean tblStyle = new TableBean();// 默认表格样式
		// 所有边框 0.5 磅
		tblStyle.setBorder(0.5F);
		tblStyle.setCellMargin(0.1F, 0.2F, 0, 0.2F);
		// 表格左缩进0.01厘米
		tblStyle.setTableLeftInd(0.01F);
		return tblStyle;
	}

	@Override
	public ParagraphBean tblTitleStyle() {
		ParagraphBean tblTitileStyle = new ParagraphBean(Align.center, "宋体", Constants.wuhao, true);
		tblTitileStyle.setSpace(0f, 0f, vLine1);
		return tblTitileStyle;
	}

	@Override
	public ParagraphBean textContentStyle() {
		ParagraphBean textContentStyle = new ParagraphBean(Align.left, "宋体", Constants.wuhao, false);
		textContentStyle.setSpace(0f, 0f, vLine1);
		textContentStyle.setInd(0f, 0f, 0f, 0.7F);
		return textContentStyle;
	}

	@Override
	public ParagraphBean textTitleStyle() {
		ParagraphBean textTitleStyle = new ParagraphBean("黑体", Constants.wuhao, false);
		textTitleStyle.setSpace(1F, 1F, vLine1);
		textTitleStyle.setpStyle(PStyle.LVL1);
		return textTitleStyle;
	}

	/***
	 * 创建通用页眉
	 * 
	 * @param sect
	 */
	private void createCommhdr(Sect sect) {
		ParagraphBean pBean = new ParagraphBean(Align.right, "黑体", Constants.wuhao, false);
		pBean.setSpace(0f, 0f, vLine1);
		Header header = sect.createHeader(HeaderOrFooterType.odd);
		String processNumber = assembleProcessNumber(processDownloadBean.getFlowInputNum());
		header.createParagraph(processNumber, pBean);
	}

	private String assembleProcessNumber(String flowInputNum) {
		String processNumber = "";
		if (flowInputNum == null) {
			return processNumber;
		}
		// processNumber = flowInputNum.startsWith(PROCESS_NUMBER_PREFIX) ?
		// processNumber
		// : (PROCESS_NUMBER_PREFIX + " " + flowInputNum);
		return processNumber;
	}

	/**
	 * 角色职责
	 * 
	 * @param _count
	 * @param name
	 * @param roleList
	 */
	protected void a08(ProcessFileItem processFileItem, List<String[]> rowData) {
		// 标题
		createTitle(processFileItem);

		Sect sect = processFileItem.getBelongTo();
		ParagraphBean titleBean = new ParagraphBean("黑体", Constants.wuhao, false);
		titleBean.setSpace(0.5f, 0.5f, vLine1);
		titleBean.setInd(0, 0, 0, 0.7f);

		ParagraphBean contentBean = new ParagraphBean("宋体", Constants.xiaowu, false);
		contentBean.setSpace(0f, 0f, vLine1);
		contentBean.setInd(0, 0, 0, 0.7f);
		int index = 1;
		for (String[] rowArr : rowData) {
			sect.createParagraph(numberMark + "." + index + " " + nullToEmpty(rowArr[0]), titleBean);
			if (JecnContants.showPosNameBox) {
				sect.createParagraph(nullToEmpty(rowArr[1]), contentBean);
			}
			sect.createParagraph(nullToEmpty(rowArr[2]), contentBean);
			index++;
		}
	}

	@Override
	protected void a09(ProcessFileItem processFileItem, List<Image> images) {
		createTitle(processFileItem);
		ParagraphBean pBean = new ParagraphBean(Align.center);
		for (Image img : images) {
			resize(img);
			processFileItem.getBelongTo().createParagraph(img, pBean);
		}
	}

	private void resize(Image img) {
		final float maxHeight = 16f;
		final float maxWidth = 26f;
		float width = img.getWidth() > maxWidth ? maxWidth : img.getWidth();
		float height = img.getHeight() > maxHeight ? maxHeight : img.getHeight();
		img.setSize(width, height);
	}

	@Override
	protected void a14(ProcessFileItem processFileItem, List<String[]> rowData) {
		if (rowData == null || rowData.size() == 0) {
			createTextItem(processFileItem, blank);
			return;
		}
		createTitle(processFileItem);
		// 制度编号
		String number = JecnUtil.getValue("ruleNumber");
		// 制度名称
		String name = JecnUtil.getValue("ruleName");
		// 类型
		String type = JecnUtil.getValue("type");
		rowData.add(0, new String[] { number, name, type });
		float[] width = new float[] { 3.5f, 10.5f, 3.5F };
		Table tbl = createTableItem(processFileItem, width, rowData);
		ParagraphBean pBean = new ParagraphBean(Align.center);
		tbl.setCellStyle(0, pBean);
		tbl.setCellStyle(2, pBean);
		tbl.setRowStyle(0, docProperty.getTblTitleStyle(), docProperty.isTblTitleShadow(), docProperty
				.isTblTitleCrossPageBreaks());

	}

}
