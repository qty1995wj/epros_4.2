/**
 * 风险控制清单Json数据
 * @param {Object} id 选中节点ID
 * @param {Object} isShow 是否全部展现
 */
function riskInventoryJson(riskId, isShowAll) {
	Ext.Ajax.request( {
		url : "findRiskList.action",
		method : "post",
		timeout : 1000000,
		params : {
			riskId : riskId,
			showAll : isShowAll
		},
		success : function(response, options) {
			// 获取response中的数据
		var json = Ext.util.JSON.decode(response.responseText);
		// 封装Json数据
		riskInventoryGrid(json);
		document.getElementById("allRiskTotal").innerHTML = "(" + json.allTotal
				+ ")";
		document.getElementById("riskId").value = riskId;
	},
	failure : function(response, options) {
		alert(i_readRiskListException)
	}
	});
}

/**
 * Json数据封装
 * @param {Object} json 后台传入的Json数据
 */
function riskInventoryGrid(json) {
	// 数据封装
	riskInventoryGridStore = new Ext.data.JsonStore( {
		autoLoad : false,
		data : json.dataList,
		fields : json.stores
	});

	// 列信息
	riskInventoryColumnModel = new Ext.grid.ColumnModel( {
		defaults : {
			align : 'center',
			width : 100,
			sortable : true,
			menuDisabled : false
		},
		columns : json.columns
	});
    
	
	//解决IE6滚动条不显示问题
//	var vstr = $.browser.version;
	var extWidth;
//    if($.browser.msie&&(vstr =='6.0')||vstr=='7.0'){ //判断ie6、7浏览器
//    	extWidth=Ext.get("riskInventoryTabId").getWidth();
//    }else{
    	extWidth='100%';
//    }
	// Panel属性
	riskInventoryGridPanel = new Ext.grid.GridPanel( {
		id : "riskInventoryGridPanel_id",
		renderTo : "riskInventory_Div",
		height : document.documentElement.clientHeight - 150,
		autoScroll : true,
		width:extWidth,
		// 显示行的分隔符
		stripeRows : true,
		// 显示列的分隔线
		columnLines : true,
		enableColumnMove : false,
		// 让列的宽度和grid一样
		viewConfig : {
		//			forceFit : true
		},
		frame : false,
		store : riskInventoryGridStore,
		colModel : riskInventoryColumnModel
	});
}

// 显示全部
function showAllRiskList() {
	var riskId = document.getElementById("riskId").value;
	riskInventoryGridPanel.destroy();
	riskInventoryJson(riskId, true);
}

// 下载
function downloadRiskList() {
	var riskId = document.getElementById("riskId").value;
	window.open("downfindRiskList.action?riskId=" + riskId);
}