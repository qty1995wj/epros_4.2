package com.jecn.epros.server.dao.propose;

import com.jecn.epros.server.bean.propose.JecnRtnlProposeFile;
import com.jecn.epros.server.common.IBaseDao;

/***
 * 合理化 建议附件表dao
 * 2013-09-02
 *
 */
public interface IRtnlProposeFileDao  extends IBaseDao<JecnRtnlProposeFile, String> {

}
