package com.jecn.epros.server.action.web.login.ad.nfspring;

import org.apache.commons.lang.StringUtils;

import com.jecn.epros.server.action.web.login.LoginAction;
import com.jecn.epros.server.action.web.login.ad.JecnAbstractADLoginAction;
import com.jecn.epros.server.common.JecnContants;

public class NongFuSpringLoginAction extends JecnAbstractADLoginAction {

	public NongFuSpringLoginAction(LoginAction loginAction) {
		super(loginAction);
	}

	/**
	 * 
	 * 制度访问：http://127.0.0.1:8080/EprosServer3.0/loginMail.action?userNum=admin&accessType=mailOpenRule&id=742
	 * 
	 * 文件访问访问：http://127.0.0.1:8080/EprosServer3.0/loginMail.action?userNum=admin&accessType=mailOpenFile&id=521
	 */
	@Override
	public String login() {
		// 登录名称
		String name = this.loginAction.getLoginName();
		// 登录密码
		String password = this.loginAction.getPassword();

		// 用户名密码验证
		if (StringUtils.isNotBlank(name) && StringUtils.isNotBlank(password)) {
			return loginAction.userLoginGen(true);
		}

		// 登录名称单点登录
		String userNum = loginAction.getRequest().getParameter("userNum");
		if (StringUtils.isNotBlank(userNum)) {
			this.loginAction.getSession().setAttribute(JecnContants.SESSION_KEY, "");
			return this.loginByLoginName(userNum);
		}
		return LoginAction.INPUT;
	}

}
