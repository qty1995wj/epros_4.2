/**
 * SetItemValues.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.jecn.webservice.jinke.taskPub;

public class SetItemValues  implements java.io.Serializable {
    private java.lang.String userCode;

    private java.lang.String bizObjectSchemaCode;

    private java.lang.String bizObjectId;

    private com.jecn.webservice.jinke.taskPub.DataItemParam[] keyValues;

    public SetItemValues() {
    }

    public SetItemValues(
           java.lang.String userCode,
           java.lang.String bizObjectSchemaCode,
           java.lang.String bizObjectId,
           com.jecn.webservice.jinke.taskPub.DataItemParam[] keyValues) {
           this.userCode = userCode;
           this.bizObjectSchemaCode = bizObjectSchemaCode;
           this.bizObjectId = bizObjectId;
           this.keyValues = keyValues;
    }


    /**
     * Gets the userCode value for this SetItemValues.
     * 
     * @return userCode
     */
    public java.lang.String getUserCode() {
        return userCode;
    }


    /**
     * Sets the userCode value for this SetItemValues.
     * 
     * @param userCode
     */
    public void setUserCode(java.lang.String userCode) {
        this.userCode = userCode;
    }


    /**
     * Gets the bizObjectSchemaCode value for this SetItemValues.
     * 
     * @return bizObjectSchemaCode
     */
    public java.lang.String getBizObjectSchemaCode() {
        return bizObjectSchemaCode;
    }


    /**
     * Sets the bizObjectSchemaCode value for this SetItemValues.
     * 
     * @param bizObjectSchemaCode
     */
    public void setBizObjectSchemaCode(java.lang.String bizObjectSchemaCode) {
        this.bizObjectSchemaCode = bizObjectSchemaCode;
    }


    /**
     * Gets the bizObjectId value for this SetItemValues.
     * 
     * @return bizObjectId
     */
    public java.lang.String getBizObjectId() {
        return bizObjectId;
    }


    /**
     * Sets the bizObjectId value for this SetItemValues.
     * 
     * @param bizObjectId
     */
    public void setBizObjectId(java.lang.String bizObjectId) {
        this.bizObjectId = bizObjectId;
    }


    /**
     * Gets the keyValues value for this SetItemValues.
     * 
     * @return keyValues
     */
    public com.jecn.webservice.jinke.taskPub.DataItemParam[] getKeyValues() {
        return keyValues;
    }


    /**
     * Sets the keyValues value for this SetItemValues.
     * 
     * @param keyValues
     */
    public void setKeyValues(com.jecn.webservice.jinke.taskPub.DataItemParam[] keyValues) {
        this.keyValues = keyValues;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof SetItemValues)) return false;
        SetItemValues other = (SetItemValues) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.userCode==null && other.getUserCode()==null) || 
             (this.userCode!=null &&
              this.userCode.equals(other.getUserCode()))) &&
            ((this.bizObjectSchemaCode==null && other.getBizObjectSchemaCode()==null) || 
             (this.bizObjectSchemaCode!=null &&
              this.bizObjectSchemaCode.equals(other.getBizObjectSchemaCode()))) &&
            ((this.bizObjectId==null && other.getBizObjectId()==null) || 
             (this.bizObjectId!=null &&
              this.bizObjectId.equals(other.getBizObjectId()))) &&
            ((this.keyValues==null && other.getKeyValues()==null) || 
             (this.keyValues!=null &&
              java.util.Arrays.equals(this.keyValues, other.getKeyValues())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getUserCode() != null) {
            _hashCode += getUserCode().hashCode();
        }
        if (getBizObjectSchemaCode() != null) {
            _hashCode += getBizObjectSchemaCode().hashCode();
        }
        if (getBizObjectId() != null) {
            _hashCode += getBizObjectId().hashCode();
        }
        if (getKeyValues() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getKeyValues());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getKeyValues(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(SetItemValues.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://tempuri.org/", ">SetItemValues"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("userCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/", "userCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("bizObjectSchemaCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/", "bizObjectSchemaCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("bizObjectId");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/", "bizObjectId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("keyValues");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/", "keyValues"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://tempuri.org/", "DataItemParam"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        elemField.setItemQName(new javax.xml.namespace.QName("http://tempuri.org/", "DataItemParam"));
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
