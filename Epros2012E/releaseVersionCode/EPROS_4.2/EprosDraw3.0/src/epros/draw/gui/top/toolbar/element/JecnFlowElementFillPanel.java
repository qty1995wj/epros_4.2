package epros.draw.gui.top.toolbar.element;

import java.awt.GridBagConstraints;
import java.awt.event.ItemEvent;

import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.JLabel;
import javax.swing.JRadioButton;
import javax.swing.SwingConstants;

import epros.draw.constant.JecnWorkflowConstant.FillType;
import epros.draw.data.JecnAbstractFlowElementData;

/**
 * 
 * 填充颜色面板
 * 
 * @author ZHOUXY
 * 
 */
public class JecnFlowElementFillPanel extends JecnAbstarctFlowElementPanel {

	/** 无填充按钮 */
	private JRadioButton noFillButton = null;
	/** 单颜色填充按钮 */
	private JRadioButton oneColorFillButton = null;
	/** 双颜色填充按钮 */
	private JRadioButton twoColorFillButton = null;
	/** 第一个填充颜色 */
	private JLabel fillColorLabel = null;
	private JecnElementButtonPanel fillColorPanel = null;
	/** 第二种填充颜色 */
	private JLabel changeColorLabel = null;
	private JecnElementButtonPanel changeColorPanel = null;

	public JecnFlowElementFillPanel(JecnFlowElementPanel flowElementPanel) {
		super(flowElementPanel);
		init();
	}

	/**
	 * 
	 * 初始化组件
	 * 
	 */
	protected void initComponents() {

		ButtonGroup group = new ButtonGroup();

		// 无填充按钮
		noFillButton = new JRadioButton(flowElementPanel.getResourceManager()
				.getValue("noFill"));
		// 单颜色填充按钮
		oneColorFillButton = new JRadioButton(flowElementPanel
				.getResourceManager().getValue("oneFill"));
		// 双颜色填充按钮
		twoColorFillButton = new JRadioButton(flowElementPanel
				.getResourceManager().getValue("twoFill"));
		// 第一个填充颜色
		fillColorLabel = new JLabel(flowElementPanel.getResourceManager()
				.getValue("changeColor1"), SwingConstants.RIGHT);
		fillColorPanel = new JecnElementButtonPanel(flowElementPanel,
				flowElementPanel.getSizeColor(), flowElementPanel
						.getBTN_FILL_COLOR());

		// 第二种填充颜色
		changeColorLabel = new JLabel(flowElementPanel.getResourceManager()
				.getValue("changeColor2"), SwingConstants.RIGHT);
		changeColorPanel = new JecnElementButtonPanel(flowElementPanel,
				flowElementPanel.getSizeColor(), flowElementPanel
						.getBTN_CHANGE_COLOR());

		// 边框标题
		this.setBorder(BorderFactory.createTitledBorder(BorderFactory
				.createEtchedBorder(), flowElementPanel.getResourceManager()
				.getValue("fillColor")));

		// 无填充按钮
		noFillButton.setOpaque(false);
		// 单颜色填充按钮
		oneColorFillButton.setOpaque(false);
		// 双颜色填充按钮
		twoColorFillButton.setOpaque(false);

		group.add(noFillButton);
		group.add(oneColorFillButton);
		group.add(twoColorFillButton);

		// 事件
		noFillButton.addItemListener(flowElementPanel);
		oneColorFillButton.addItemListener(flowElementPanel);
		twoColorFillButton.addItemListener(flowElementPanel);

	}

	/**
	 * 
	 * 布局
	 * 
	 */
	protected void initLayout() {

		GridBagConstraints c;
		// ******************第一行******************//
		// 无填充按钮
		c = new GridBagConstraints(0, 0, 3, 1, 0.0, 0.0,
				GridBagConstraints.NORTHWEST, GridBagConstraints.NONE,
				flowElementPanel.getInsetsFirst(), 0, 0);
		this.add(noFillButton, c);
		// ******************第一行******************//

		// ******************第二行******************//
		// 单颜色填充按钮
		c = new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0,
				GridBagConstraints.NORTHWEST, GridBagConstraints.NONE,
				flowElementPanel.getInsetsFirst(), 0, 0);
		this.add(oneColorFillButton, c);
		// 第一个填充颜色 标签
		c = new GridBagConstraints(1, 1, 1, 1, 1.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				flowElementPanel.getInsetsLabel(), 0, 0);
		this.add(fillColorLabel, c);
		// 第一个填充颜色 内容
		c = new GridBagConstraints(2, 1, 1, 1, 0.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.NONE,
				flowElementPanel.getInsetsContent(), 0, 0);
		this.add(fillColorPanel, c);
		// ******************第二行******************//

		// ******************第三行******************//
		// 双颜色填充按钮
		c = new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0,
				GridBagConstraints.NORTHWEST, GridBagConstraints.NONE,
				flowElementPanel.getInsetsFirst(), 0, 0);
		this.add(twoColorFillButton, c);
		// 第二个填充颜色 标签
		c = new GridBagConstraints(1, 2, 1, 1, 1.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				flowElementPanel.getInsetsLabel(), 0, 0);
		this.add(changeColorLabel, c);
		// 第二个填充颜色 内容
		c = new GridBagConstraints(2, 2, 1, 1, 0.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.NONE,
				flowElementPanel.getInsetsContent(), 0, 0);
		this.add(changeColorPanel, c);
		// ******************第三行******************//
	}

	@Override
	public void initData(JecnAbstractFlowElementData flowElementData) {
		initFillData(flowElementData);
	}

	/**
	 * 
	 * 添加填充值
	 * 
	 * @param flowElementData
	 *            JecnAbstractFlowElementData流程元素属性值
	 */
	private void initFillData(JecnAbstractFlowElementData flowElementData) {
		if (flowElementData == null || flowElementData.getFillType() == null) {
			return;
		}
		switch (flowElementData.getFillType()) {
		case none:// 无填充
			this.getNoFillButton().setSelected(true);
			break;
		case one:// 单色填充
			this.getOneColorFillButton().setSelected(true);
			this.getFillColorPanel().setBackground(
					flowElementData.getFillColor());
			break;
		case two:// 双色填充
			this.getTwoColorFillButton().setSelected(true);
			// 颜色1
			this.getFillColorPanel().setBackground(
					flowElementData.getFillColor());
			// 颜色2
			this.getChangeColorPanel().setBackground(
					flowElementData.getChangeColor());
			break;
		default://
			break;
		}
	}

	/**
	 * 
	 * 单选框选中事件
	 * 
	 */
	public void itemStateChanged(ItemEvent e) {
		if (e.getSource() != noFillButton
				&& e.getSource() != oneColorFillButton
				&& e.getSource() != twoColorFillButton) {
			return;
		}
		itemStateChangedProcess(e);
	}

	private void itemStateChangedProcess(ItemEvent e) {
		if (e.getStateChange() == e.SELECTED) {
			if (e.getSource() == noFillButton) {// 无填充按钮
				// 颜色1隐藏
				this.fillColorPanel.setEnabled(false);
				// 颜色2隐藏
				this.changeColorPanel.setEnabled(false);

				// 3D效果按钮隐藏
				if (flowElementPanel.getEle3DPanel().getFlag3DRadioButton()
						.isSelected()) {
					flowElementPanel.getEle3DPanel().getFlag3DRadioButton()
							.setSelected(false);
				}
				flowElementPanel.getEle3DPanel().getFlag3DRadioButton()
						.setEnabled(false);
				// 无填充，阴影按钮至灰色
				flowElementPanel.getShadowMainPanel().getShadowButton()
						.setEnabled(false);
				// 转换无阴影
				flowElementPanel.getShadowMainPanel().getNoShadowButton()
						.doClick();
				// 数据层:填充类型
				flowElementPanel.getSelectedBasePanel().getFlowElementData()
						.setFillType(FillType.none);
			} else if (e.getSource() == oneColorFillButton) {// 单颜色填充按钮
				// 颜色1显示
				this.fillColorPanel.setEnabled(true);
				// 颜色2隐藏
				this.changeColorPanel.setEnabled(false);
				// 3D效果按钮显示
				flowElementPanel.getEle3DPanel().getFlag3DRadioButton()
						.setEnabled(true);
				// 阴影按钮
				flowElementPanel.getShadowMainPanel().getShadowButton()
						.setEnabled(true);

				// 数据层:填充类型
				flowElementPanel.getSelectedBasePanel().getFlowElementData()
						.setFillType(FillType.one);

			} else if (e.getSource() == twoColorFillButton) {// 双颜色填充按钮
				// 颜色1显示
				this.fillColorPanel.setEnabled(true);
				// 颜色2显示
				this.changeColorPanel.setEnabled(true);
				// 3D效果按钮显示
				flowElementPanel.getEle3DPanel().getFlag3DRadioButton()
						.setEnabled(true);
				// 阴影按钮
				flowElementPanel.getShadowMainPanel().getShadowButton()
						.setEnabled(true);
				// 数据层:填充类型
				flowElementPanel.getSelectedBasePanel().getFlowElementData()
						.setFillType(FillType.two);
			}

			this.fillColorPanel
					.setBackground(flowElementPanel.getSelectedBasePanel()
							.getFlowElementData().getFillColor());
			this.changeColorPanel.setBackground(flowElementPanel
					.getSelectedBasePanel().getFlowElementData()
					.getChangeColor());
		}
	}

	public JRadioButton getNoFillButton() {
		return noFillButton;
	}

	public JRadioButton getOneColorFillButton() {
		return oneColorFillButton;
	}

	public JRadioButton getTwoColorFillButton() {
		return twoColorFillButton;
	}

	public JLabel getFillColorLabel() {
		return fillColorLabel;
	}

	public JecnElementButtonPanel getFillColorPanel() {
		return fillColorPanel;
	}

	public JLabel getChangeColorLabel() {
		return changeColorLabel;
	}

	public JecnElementButtonPanel getChangeColorPanel() {
		return changeColorPanel;
	}

}
