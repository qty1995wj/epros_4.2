package com.jecn.epros.server.service.process.util;

import java.util.Set;

import org.apache.commons.lang.StringUtils;

import com.jecn.epros.server.common.JecnCommon;
import com.jecn.epros.server.common.JecnCommonSql;
import com.jecn.epros.server.common.JecnCommonSqlTPath;
import com.jecn.epros.server.common.JecnContants;
import com.jecn.epros.server.common.JecnCommonSql.DBType;
import com.jecn.epros.server.util.JecnCommonSqlConstant;
import com.jecn.epros.server.util.JecnCommonSqlConstant.TYPEAUTH;
import com.jecn.epros.server.webBean.AttenTionSearchBean;
import com.jecn.epros.server.webBean.WebLoginBean;
import com.jecn.epros.server.webBean.process.ProcessSearchBean;

public class MyFlowSqlUtil {

	/*
	 * 获取sql截取字符串函数封装
	 * 
	 * @return
	 */
	public static String getSqlFuncSubStrFlow() {
		if (JecnContants.dbType.equals(DBType.ORACLE)) {
			return " T.T_PATH = SUBSTR(MAF.T_PATH, 0, LENGTH(T.T_PATH))";
		} else if (JecnContants.dbType.equals(DBType.SQLSERVER)) {
			return " T.T_PATH = SUBSTRING(MAF.T_PATH, 0, LEN(T.T_PATH)+1)";
		} else if (JecnContants.dbType.equals(DBType.MYSQL)) {
			// return
			// " INNER JOIN MY_AUTH_FILES MAF ON SUBSTR(MAF.T_PATH, 0,
			// LENGTH(T.T_PATH))";
		}
		return "";
	}

	/**
	 * 流程树展开节点
	 * 
	 * @param isOnlyAccess
	 * @param peopleId
	 * @param projectId
	 * @return
	 */
	public static String getFlowTreeAppend(boolean isAdmin,Long flowMapId, Long projectId) {
		String sqlCommonResult = " select DISTINCT t.flow_id,t.flow_name,t.isflow,"
				+ " case when pub.flow_id is null then 0 else 1 end as count,"
				+ " t.flow_id_input,t.pre_flow_id, t.sort_id ";
		String sqlCommonFrom = " from jecn_flow_structure t "
				+ " left join jecn_flow_structure pub on t.flow_id=pub.pre_flow_id and pub.del_state=0";
		String sqlCommonSearch = " where t.pre_flow_id="+flowMapId+" AND t.DATA_TYPE=0 and t.projectid="+projectId+" and t.del_state=0 order by t.pre_flow_id,t.sort_id,t.flow_id";
		//管理员
		if(isAdmin){
			return sqlCommonResult+sqlCommonFrom+sqlCommonSearch;
		}
		// 流程架构全局公开
		if (JecnContants.isMainFlowAdmin) {
			if (JecnContants.isFlowAdmin) {
				return sqlCommonResult + sqlCommonFrom + sqlCommonSearch;
			} else {
				String sql=getFlowTreeAppendCommon(1, sqlCommonResult,
						sqlCommonFrom, sqlCommonSearch, projectId);
				return sql;
			}
		} else {
			if (JecnContants.isFlowAdmin) {
				String sql= getFlowTreeAppendCommon(0, sqlCommonResult,
						sqlCommonFrom, sqlCommonSearch, projectId);
				return sql;
			} else {
				String sql = "";
				// 不是管理员的时候需要加载能查阅的权限
				if (!isAdmin) {
					sql = MyFlowSqlUtil.getFlowTreeAppend(projectId);
				}
				sql = sql + sqlCommonResult + sqlCommonFrom;
				if (!isAdmin) {
					sql = sql + JecnCommonSqlTPath.INSTANCE.getSqlFuncSubStr();
				}
				sql = sql + sqlCommonSearch;
				return sql;
			}
		}
	}

	/**
	 * isFlow=1 流程架构全局公开，流程不全局公开
	 * isFlow=0 流程架构全局不公开，流程全局公开
	 * @param isFlow
	 * @param sqlCommonResult
	 * @param sqlCommonFrom
	 * @param sqlCommonSearch
	 * @param projectId
	 * @return
	 */
	public static String getFlowTreeAppendCommon(int isFlow,
			String sqlCommonResult, String sqlCommonFrom,
			String sqlCommonSearch, Long projectId) {
		WebLoginBean bean = JecnCommon.getWebLoginBean();
		if (bean == null) {
			throw new IllegalArgumentException(
					"JecnCommon.getWebLoginBean() 参数非法！");
		}
		String sql = "with MY_AUTH_FILES as ("
				+ getAuthSQLCommonFlow(projectId, bean, isFlow) + ")"
				+ sqlCommonResult + " ,case when t.isFlow=" + isFlow
				+ " and MAF.flow_id is null then 0 else 1 end as isView";
		sql = sql + sqlCommonFrom
				+ " left join  MY_AUTH_FILES MAF on T.isflow=" + isFlow
				+ " and " + getSqlFuncSubStrFlow();
		sql = sql + sqlCommonSearch;
		return sql;
	}

	/**
	 * 查阅权限公共
	 * 
	 * @param peopleId
	 * @param projectId
	 * @param typeAuth
	 * @return
	 */
	public static String getAuthSQLCommonFlow(Long projectId,
			WebLoginBean bean, int isFlow) {
		Set<Long> listFigureIds = bean.getListPosIds();
		Set<Long> listOrgPathPIds = bean.getListOrgPathPIds();
		String tableName = JecnCommonSqlConstant.FLOW_TABLE_NAME;
		String id = JecnCommonSqlConstant.FLOW_ID;
		String sqlCommon = " SELECT T.T_PATH ,T." + id + "   FROM " + tableName
				+ " T" + "   WHERE T.del_state=0" + "   AND T.projectid = "
				+ projectId + "   AND T.isFlow=" + isFlow
				+ "     AND T.IS_PUBLIC = 1";
		if (listFigureIds.size() == 0) {
			return sqlCommon;
		}
		String sql = "SELECT T.T_PATH ,T. " + id + "    FROM " + tableName
				+ " T" + "   INNER JOIN JECN_ORG_ACCESS_PERMISSIONS JOAP"
				+ "      ON JOAP.RELATE_ID = T." + id
				+ "   AND JOAP.ORG_ID IN  "
				+ JecnCommonSql.getIdsSet(listOrgPathPIds)
				+ "   WHERE T.del_state=0 and T.projectid = " + projectId
				+ "   AND T.isFlow=" + isFlow + "     AND JOAP.TYPE = 0"
				+ "  UNION ALL" + "  SELECT T.T_PATH, T." + id + "    FROM "
				+ tableName + " T"
				+ "   INNER JOIN JECN_ACCESS_PERMISSIONS JAP"
				+ "      ON JAP.RELATE_ID = T." + id
				+ "   WHERE T.del_state=0 and T.projectid = " + projectId
				+ "   AND T.isFlow=" + isFlow + "     AND JAP.FIGURE_ID IN "
				+ JecnCommonSql.getIdsSet(listFigureIds)
				+ "     AND JAP.TYPE = 0" + "  UNION ALL"
				+ "  SELECT T.T_PATH ,T." + id + "    FROM " + tableName + " T"
				+ "   INNER JOIN JECN_GROUP_PERMISSIONS JGP"
				+ "      ON JGP.RELATE_ID = T." + id
				+ "   INNER JOIN JECN_POSITION_GROUP_R JPGR"
				+ "      ON JGP.POSTGROUP_ID = JPGR.GROUP_ID"
				+ "   INNER JOIN JECN_FLOW_ORG_IMAGE JFOI"
				+ "      ON JFOI.FIGURE_ID = JPGR.FIGURE_ID"
				+ "   WHERE T.del_state=0 and T.projectid = " + projectId
				+ "   AND T.isFlow=" + isFlow + "     AND JGP.TYPE = 0"
				+ "     AND JFOI.FIGURE_ID IN "
				+ JecnCommonSql.getIdsSet(listFigureIds) + "  UNION "
				+ sqlCommon;
		if (isFlow == 1) {
			sql = sql + getFlowJoinRole(bean.getListPosIds());
		} else if (isFlow == 0){
			sql=sql+" UNION SELECT T_PATH,"+id+" FROM JECN_FLOW_STRUCTURE WHERE ISFLOW=1 AND projectid="+projectId;
		}
		return sql;
	}

	/**
	 * 获得流程架构搜索的公共sql
	 * 
	 * @param ruleSearchBean
	 * @return
	 */
	public static String getSqlProcessMapSearch(
			ProcessSearchBean processSearchBean, long peopleId, long projectId,
			boolean isCount, boolean isAdmin) {
		// 是不是搜索所有流程节点
		boolean isAll = false;
		if (JecnContants.isMainFlowAdmin || isAdmin) {
			isAll = true;
		}
		String sql = "select distinct t.flow_id,t.flow_name,t.flow_id_input,t.pre_flow_id,pflow.flow_name as p_flow_name,t.is_public,t.pub_time,t.sort_id,t.t_path";
		if (isCount) {
			sql = "select count(distinct t.flow_id)";
		}

		sql = sql + " from jecn_flow_structure t";
		if (!isAll) {
			sql = sql + " Inner join "
					+ MyFlowSqlUtil.getFlowSearch(peopleId, projectId)
					+ " on MY_AUTH_FILES.Flow_id=t.flow_id";
		}
		sql = sql
				+ " left join jecn_access_permissions jap on jap.type=0 and jap.relate_id=t.flow_id"
				+ " left join jecn_org_access_permissions joap on joap.type=0 and joap.relate_id=t.flow_id"
				+ " left join jecn_flow_structure pflow on pflow.flow_id = t.pre_flow_id"
				+ "" + " where t.isflow=0 and t.del_state=0 and t.projectid="
				+ processSearchBean.getProjectId();

		// 流程名称
		if (StringUtils.isNotBlank(processSearchBean.getProcessName())) {
			sql = sql + " and t.flow_name like '%"
					+ processSearchBean.getProcessName() + "%'";
		}
		// 制度编号
		if (StringUtils.isNotBlank(processSearchBean.getProcessNum())) {
			sql = sql + " and t.flow_id_input like '%"
					+ processSearchBean.getProcessNum() + "%'";
		}
		// 密级
		if (processSearchBean.getSecretLevel() != -1) {
			sql = sql + " and t.is_public="
					+ processSearchBean.getSecretLevel();
		}
		// 查阅岗位权限
		if (processSearchBean.getPosLookId() != -1) {
			sql = sql + " and jap.figure_id="
					+ processSearchBean.getPosLookId();
		}
		// 查阅部门权限
		if (processSearchBean.getOrgLookId() != -1) {
			sql = sql + " and joap.org_id=" + processSearchBean.getOrgLookId();
		}
		if (!isCount) {
			sql = sql
					+ " ORDER BY T.pub_time desc, T.pre_flow_id, T.sort_id,T.flow_id ";
		}
		return sql;
	}

	/**
	 * 获得流程搜索的公共sql
	 * 
	 * @param ruleSearchBean
	 * @return
	 */
	public static String getSqlProcessSearch(
			ProcessSearchBean processSearchBean, long peopleId, long projectId,
			boolean isCount, boolean isAdmin) {
		// 是不是搜索所有流程节点
		boolean isAll = false;
		if (JecnContants.isFlowAdmin || isAdmin) {
			isAll = true;
		}
		String sql = "select distinct t.flow_id,t.flow_name,t.flow_id_input,org.org_id,org.org_name"
				+ "       ,jfbi.type_res_people,jfbi.res_people_id,"
				+ "       case when jfbi.type_res_people=0 then ju.true_name"
				+ "       when jfbi.type_res_people=1 then jfoi.figure_text end as res_people"
				+ "       ,t.pub_time,t.is_public,t.create_date,t.pre_flow_id,t.sort_id,t.t_path";
		if (isCount) {
			sql = "select count(distinct t.flow_id)";
		}
		sql = sql
				+ " from jecn_flow_structure t ,jecn_flow_basic_info jfbi"
				+ " left join jecn_flow_related_org jfro on jfro.flow_id=jfbi.flow_id"
				+ " left join jecn_flow_org org on jfro.org_id=org.org_id and org.del_state=0"
				+ " left join jecn_flow_org_image jfoi on jfoi.figure_id=jfbi.res_people_id"
				+ " left join jecn_user ju on  ju.people_id=jfbi.res_people_id and ju.islock=0 "
				+ " left join jecn_access_permissions jap on jap.type=0 and jap.relate_id=jfbi.flow_id"
				+ " left join jecn_org_access_permissions joap on joap.type=0 and joap.relate_id=jfbi.flow_id";
		if (!isAll) {
			sql = sql + " Inner join "
					+ MyFlowSqlUtil.getFlowSearch(peopleId, projectId)
					+ " on MY_AUTH_FILES.Flow_id=jfbi.flow_id";
		}
		sql = sql
				+ " where t.isflow=1 and t.del_state=0 and t.flow_id=jfbi.flow_id and t.projectid="
				+ processSearchBean.getProjectId();
		// 参与岗位
		if (StringUtils.isNotBlank(processSearchBean.getPosName())) {
			if (processSearchBean.getPosId() != -1) {
				sql = sql
						+ " and (t.flow_id in"
						+ "       (select jfs.flow_id from jecn_flow_structure_image jfs,process_station_related psr"
						+ "               where psr.type='0' and psr.figure_flow_id=jfs.figure_id and psr.figure_position_id="
						+ processSearchBean.getPosId()
						+ ")"
						+ "       or t.flow_id in (select jfs.flow_id from jecn_flow_structure_image jfs,process_station_related psr,jecn_position_group_r jpgr"
						+ "               where psr.type='1' and psr.figure_flow_id=jfs.figure_id and psr.figure_position_id=jpgr.group_id and jpgr.figure_id="
						+ processSearchBean.getPosId() + "))";
			} else {
				sql = sql
						+ " and (t.flow_id in"
						+ "       (select jfs.flow_id from jecn_flow_structure_image jfs,process_station_related psr,jecn_flow_org_image jfoi"
						+ "               where psr.type='0' and psr.figure_flow_id=jfs.flow_id and psr.figure_position_id=jfoi.figure_id and jfoi.figure_text like '%?%')"
						+ "       or t.flow_id in (select jfs.flow_id from jecn_flow_structure_image jfs,process_station_related psr,jecn_position_group_r jpgr,jecn_flow_org_image jfoi"
						+ "               where psr.type='1' and psr.figure_flow_id=jfs.flow_id and psr.figure_position_id=jpgr.group_id and jpgr.figure_id=jfoi.figure_id and jfoi.figure_text like '%?%'))";
			}
		}
		// 流程名称
		if (StringUtils.isNotBlank(processSearchBean.getProcessName())) {
			sql = sql + " and t.flow_name like '%"
					+ processSearchBean.getProcessName() + "%'";
		}
		// 制度编号
		if (StringUtils.isNotBlank(processSearchBean.getProcessNum())) {
			sql = sql + " and t.flow_id_input like '%"
					+ processSearchBean.getProcessNum() + "%'";
		}
		// 责任部门
		if (processSearchBean.getOrgId() != -1) {
			sql = sql + " and org.org_id=" + processSearchBean.getOrgId();
		}
		// 类别
		if (processSearchBean.getProcessType() != -1) {
			sql = sql + " and jfbi.type_id="
					+ processSearchBean.getProcessType();
		}
		// 密级
		if (processSearchBean.getSecretLevel() != -1) {
			sql = sql + " and t.is_public="
					+ processSearchBean.getSecretLevel();
		}
		// 查阅岗位权限
		if (processSearchBean.getPosLookId() != -1) {
			sql = sql + " and jap.figure_id="
					+ processSearchBean.getPosLookId();
		}
		// 查阅部门权限
		if (processSearchBean.getOrgLookId() != -1) {
			sql = sql + " and joap.org_id=" + processSearchBean.getOrgLookId();
		}
		if (!isCount) {
			sql = sql
					+ " ORDER BY T.pub_time desc, T.pre_flow_id, T.sort_id,T.flow_id ";
		}
		return sql;
	}

	/**
	 * 流程数展开
	 * 
	 * @param peopleId
	 * @param projectId
	 * @return
	 */
	public static String getFlowTreeAppend(Long projectId) {
		WebLoginBean bean = JecnCommon.getWebLoginBean();
		if (bean == null) {
			throw new IllegalArgumentException(
					"JecnCommon.getWebLoginBean() 参数非法！");
		}

		// 添加查阅权限设置的流程节点集合
		return "with MY_AUTH_FILES as ( "
				+ JecnCommonSqlTPath.INSTANCE.getAuthSQLCommon(projectId,
						TYPEAUTH.FLOW, bean)
				+ getFlowJoinRole(bean.getListPosIds()) + " ) ";

	}

	/**
	 * 流程数搜索
	 * 
	 * @param peopleId
	 * @param projectId
	 * @return
	 */
	public static String getFlowSearch(Long peopleId, Long projectId) {
		WebLoginBean bean = JecnCommon.getWebLoginBean();
		if (bean == null) {
			throw new IllegalArgumentException(
					"JecnCommon.getWebLoginBean() 参数非法！");
		}
		// 添加查阅权限设置的流程节点集合
		return "( "
				+ JecnCommonSqlTPath.INSTANCE.getAuthSQLCommon(projectId,
						TYPEAUTH.FLOW, bean)
				+ getFlowJoinRole(bean.getListPosIds()) + " ) MY_AUTH_FILES";

	}

	/**
	 * 流程中参与角色
	 * 
	 * @param listFigureIds
	 * @param type
	 * @return
	 */
	public static String getFlowJoinRole(Set<Long> listFigureIds) {
		if (listFigureIds.size() == 0) {
			return "";
		}
		return " UNION "
				+ " SELECT DISTINCT FS.T_PATH ,FS.FLOW_ID"
				+ "    FROM JECN_FLOW_STRUCTURE FS"
				+ "   INNER JOIN JECN_FLOW_STRUCTURE_IMAGE FSI"
				+ "      ON FS.FLOW_ID = FSI.FLOW_ID"
				+ "   INNER JOIN (SELECT FIGURE_FLOW_ID, FIGURE_POSITION_ID FIGURE_ID"
				+ "                FROM PROCESS_STATION_RELATED"
				+ "               WHERE TYPE = '0'" + "              UNION"
				+ "              SELECT SR.FIGURE_FLOW_ID, PGR.FIGURE_ID"
				+ "                FROM PROCESS_STATION_RELATED SR"
				+ "               INNER JOIN JECN_POSITION_GROUP_R PGR"
				+ "                  ON SR.FIGURE_POSITION_ID = PGR.GROUP_ID"
				+ "               WHERE SR.TYPE = '1') ROLE_FIGURE"
				+ "      ON FSI.FIGURE_ID = ROLE_FIGURE.FIGURE_FLOW_ID"
				+ "   AND ROLE_FIGURE.FIGURE_ID IN "
				+ JecnCommonSql.getIdsSet(listFigureIds);
	}

	/**
	 * 我关注流程总数
	 * 
	 * @param attenTionSearchBean
	 * @param projectId
	 * @return
	 */
	public static String getAttenTionFlowCount(
			AttenTionSearchBean attenTionSearchBean, Long projectId) {
		String sql = "  SELECT count(T.FLOW_ID) FROM JECN_FLOW_STRUCTURE T,JECN_FLOW_BASIC_INFO JFBI"
				+ " INNER JOIN "
				+ MyFlowSqlUtil.getAttenTionProcessCommon(attenTionSearchBean
						.getUserId(), projectId)
				+ " on JFBI.FLOW_ID = MY_AUTH_FILES.FLOW_ID "
				+ " LEFT JOIN JECN_FLOW_RELATED_ORG RELATED_ORG ON RELATED_ORG.FLOW_ID = JFBI.FLOW_ID "
				+ " LEFT JOIN jecn_flow_org org on RELATED_ORG.org_id = org.org_id";
		// 查询条件
		sql = sql
				+ MyFlowSqlUtil.getAttenTionProcessSearch(attenTionSearchBean,
						projectId);
		return sql;
	}

	/**
	 * 我关注流程
	 * 
	 * @param attenTionSearchBean
	 * @param projectId
	 * @return
	 */
	public static String getAttenTionFlow(
			AttenTionSearchBean attenTionSearchBean, Long projectId) {
		// 结果sql
		String sql = "  SELECT distinct T.FLOW_ID, T.FLOW_NAME,T.FLOW_ID_INPUT,"
				+ " RELATED_ORG.ORG_ID, org.ORG_NAME," // 责任部门ID和责任人ID
				+ " JFBI.TYPE_RES_PEOPLE, JFBI.RES_PEOPLE_ID,"
				+ " CASE WHEN JFBI.TYPE_RES_PEOPLE = 0 THEN JU.TRUE_NAME  "
				+ " WHEN JFBI.TYPE_RES_PEOPLE = 1 THEN JFOI.FIGURE_TEXT END AS RES_PEOPLE," // 责任人
				+ " T.pub_time,  T.IS_PUBLIC, T.CREATE_DATE, T.PRE_FLOW_ID, T.SORT_ID,T.T_PATH"
				+ " FROM JECN_FLOW_STRUCTURE T,JECN_FLOW_BASIC_INFO JFBI"
				+ " INNER JOIN "
				+ MyFlowSqlUtil.getAttenTionProcessCommon(attenTionSearchBean
						.getUserId(), projectId)
				+ " on JFBI.FLOW_ID = MY_AUTH_FILES.FLOW_ID "
				+ " LEFT JOIN JECN_FLOW_RELATED_ORG RELATED_ORG ON RELATED_ORG.FLOW_ID = JFBI.FLOW_ID "
				+ " LEFT JOIN jecn_flow_org org on RELATED_ORG.org_id = org.org_id"
				+ " LEFT JOIN JECN_FLOW_ORG_IMAGE JFOI  ON JFOI.FIGURE_ID = JFBI.RES_PEOPLE_ID"
				+ " LEFT JOIN JECN_USER JU  ON JU.ISLOCK = 0"
				+ " AND JU.PEOPLE_ID = JFBI.RES_PEOPLE_ID";
		// 查询条件
		sql = sql
				+ MyFlowSqlUtil.getAttenTionProcessSearch(attenTionSearchBean,
						projectId);
		// 排序
		sql = sql
				+ " ORDER BY T.pub_time desc, T.pre_flow_id, T.sort_id,T.flow_id ";
		return sql;
	}

	/**
	 * 我关注流程架构总数
	 * 
	 * @param attenTionSearchBean
	 * @param projectId
	 * @return
	 */
	public static String getAttenTionFlowMapCount(
			AttenTionSearchBean attenTionSearchBean, Long projectId) {
		// 结果sql
		String sql = "SELECT count(T.FLOW_ID)"
				+ " FROM JECN_FLOW_STRUCTURE T INNER JOIN "
				+ MyFlowSqlUtil.getAttenTionProcessCommon(attenTionSearchBean
						.getUserId(), projectId)
				+ " on T.FLOW_ID = MY_AUTH_FILES.FLOW_ID ";
		// 查询条件
		sql = sql
				+ MyFlowSqlUtil.getAttenTionProcessSearch(attenTionSearchBean,
						projectId);
		// 排序
		return sql;
	}

	/**
	 * 我关注流程架构
	 * 
	 * @param attenTionSearchBean
	 * @param projectId
	 * @return
	 */
	public static String getAttenTionFlowMap(
			AttenTionSearchBean attenTionSearchBean, Long projectId) {
		// 结果sql
		String sql = "SELECT T.FLOW_ID, T.FLOW_NAME, T.FLOW_ID_INPUT,T.pub_time,T.IS_PUBLIC,T.CREATE_DATE, T.PRE_FLOW_ID, T.SORT_ID,T.T_PATH"
				+ " FROM JECN_FLOW_STRUCTURE T INNER JOIN "
				+ MyFlowSqlUtil.getAttenTionProcessCommon(attenTionSearchBean
						.getUserId(), projectId)
				+ " on T.FLOW_ID = MY_AUTH_FILES.FLOW_ID ";
		// 查询条件
		sql = sql
				+ MyFlowSqlUtil.getAttenTionProcessSearch(attenTionSearchBean,
						projectId);
		// 排序
		sql = sql
				+ " ORDER BY T.pub_time desc, T.pre_flow_id, T.sort_id,T.flow_id ";
		return sql;
	}

	/**
	 * 适用于我的关注流程
	 * 
	 * @param peopleId
	 * @param projectId
	 * @return
	 */
	public static String getAttenTionProcessCommon(Long peopleId, Long projectId) {
		WebLoginBean bean = JecnCommon.getWebLoginBean();
		if (bean == null) {
			throw new IllegalArgumentException(
					"JecnCommon.getWebLoginBean() 参数非法！");
		}
		// 添加查阅权限设置的流程节点集合
		return "( "
				+ JecnCommonSqlTPath.INSTANCE.getAuthSQLCommon(projectId,
						TYPEAUTH.FLOW, bean) + " ) MY_AUTH_FILES";

	}

	/**
	 * 关注流程
	 * 
	 * @param fileSearchBean
	 * @param projectId
	 * @return
	 */
	public static String getAttenTionProcessSearch(
			AttenTionSearchBean attenTionSearchBean, Long projectId) {
		StringBuffer sqlBuffer = new StringBuffer();
		sqlBuffer.append(" WHERE T.DEL_STATE = 0 and T.PROJECTID=" + projectId);

		// 是否是流程
		if (attenTionSearchBean.getIsFlow() == 1
				|| attenTionSearchBean.getIsFlow() == 0) {
			sqlBuffer
					.append(" and T.isflow=" + attenTionSearchBean.getIsFlow());
		}
		// 流程
		if (attenTionSearchBean.getIsFlow() == 1) {
			sqlBuffer.append(" and T.FLOW_ID = JFBI.FLOW_ID");
		}
		// 流程名称
		if (StringUtils.isNotBlank(attenTionSearchBean.getName())) {
			sqlBuffer.append(" and T.flow_name like '%"
					+ attenTionSearchBean.getName() + "%'");
		}
		// 流程编号
		if (StringUtils.isNotBlank(attenTionSearchBean.getNumber())) {
			sqlBuffer.append(" and T.flow_id_input like '%"
					+ attenTionSearchBean.getNumber() + "%'");
		}
		// 是不是公开
		if (attenTionSearchBean.getSecretLevel() != -1) {
			sqlBuffer.append(" and T.is_public="
					+ attenTionSearchBean.getSecretLevel());
		}
		if (attenTionSearchBean.getIsFlow() == 1) {// 流程地图无责任部门
			// 责任部门 orgId
			if (attenTionSearchBean.getOrgId() != -1) {
				sqlBuffer.append(" and RELATED_ORG.ORG_ID ="
						+ attenTionSearchBean.getOrgId());
			}
		}
		return sqlBuffer.toString();
	}

	/**
	 * 数据对接的：关注的流程 所有公开的流程sql
	 * 
	 * @return
	 */
	private static String buttPublicProcessSql(Long projectId) {
		return " SELECT T.FLOW_ID,"
				+ "      T.FLOW_NAME,"
				+ "      T.FLOW_ID_INPUT,"
				+ "      T.pub_time,"
				+ "      T.IS_PUBLIC,"
				+ "      T.CREATE_DATE,"
				+ "      T.PRE_FLOW_ID,"
				+ "      T.SORT_ID"
				+ " FROM JECN_FLOW_STRUCTURE T, JECN_FLOW_BASIC_INFO JFBI"
				+ " LEFT JOIN (SELECT JFRO.ORG_ID, JFO.ORG_NAME, JFRO.FLOW_ID"
				+ "              FROM JECN_FLOW_RELATED_ORG JFRO, JECN_FLOW_ORG JFO"
				+ "             WHERE JFRO.ORG_ID = JFO.ORG_ID"
				+ "               AND JFO.DEL_STATE = 0) ORG ON ORG.FLOW_ID = JFBI.FLOW_ID"
				+ " LEFT JOIN JECN_FLOW_ORG_IMAGE JFOI ON JFOI.FIGURE_ID = JFBI.RES_PEOPLE_ID"
				+ " LEFT JOIN JECN_USER JU ON JU.ISLOCK = 0"
				+ "                       AND JU.PEOPLE_ID = JFBI.RES_PEOPLE_ID"
				+ " WHERE T.DEL_STATE = 0" + "  AND T.PROJECTID = " + projectId
				+ "  AND T.ISFLOW = 1" + "  AND T.IS_PUBLIC = 1"
				+ "  AND T.FLOW_ID = JFBI.FLOW_ID ";

	}

	/**
	 * 数据对接：所有为密级的流程 sql
	 * 
	 * @return
	 */
	private static String buttNoPublicProcessSql(Long projectId, Long userId) {
		String sql = " SELECT T.FLOW_ID,"
				+ "        T.FLOW_NAME,"
				+ "        T.FLOW_ID_INPUT,"
				+ "        T.pub_time,"
				+ "        T.IS_PUBLIC,"
				+ "        T.CREATE_DATE,"
				+ "        T.PRE_FLOW_ID,"
				+ "        T.SORT_ID"
				+ "   FROM JECN_FLOW_STRUCTURE T, JECN_FLOW_BASIC_INFO JFBI"
				+ "   LEFT JOIN (SELECT JFRO.ORG_ID, JFO.ORG_NAME, JFRO.FLOW_ID"
				+ "                FROM JECN_FLOW_RELATED_ORG JFRO, JECN_FLOW_ORG JFO"
				+ "               WHERE JFRO.ORG_ID = JFO.ORG_ID"
				+ "                 AND JFO.DEL_STATE = 0) ORG ON ORG.FLOW_ID = JFBI.FLOW_ID"
				+ "   LEFT JOIN JECN_FLOW_ORG_IMAGE JFOI ON JFOI.FIGURE_ID ="
				+ "                                         JFBI.RES_PEOPLE_ID"
				+ "   LEFT JOIN JECN_USER JU ON JU.ISLOCK = 0"
				+ "                         AND JU.PEOPLE_ID = JFBI.RES_PEOPLE_ID"
				+ "  LEFT JOIN (SELECT GPT.RELATE_ID, PGR.FIGURE_ID "
				+ "  FROM JECN_GROUP_PERMISSIONS GPT "
				+ "  INNER JOIN JECN_POSITION_GROUP_R PGR ON GPT.POSTGROUP_ID = "
				+ "   PGR.GROUP_ID "
				+ "   AND GPT.TYPE = 0 "
				+ "  UNION "
				+ "  SELECT AP.RELATE_ID, AP.FIGURE_ID "
				+ "   FROM JECN_ACCESS_PERMISSIONS AP "
				+ "   WHERE AP.TYPE = 0) JAP "
				+ "   ON JAP.RELATE_ID = JFBI.FLOW_ID"
				+ "   LEFT JOIN JECN_ORG_ACCESS_PERMISSIONS JOAP ON JOAP.TYPE = 0"
				+ "                                             AND JOAP.RELATE_ID ="
				+ "                                                 JFBI.FLOW_ID"
				+ "  WHERE T.DEL_STATE = 0" + "    AND T.PROJECTID = "
				+ projectId + "    AND T.ISFLOW = 1"
				+ "    AND T.IS_PUBLIC = 0"
				+ "    AND T.FLOW_ID = JFBI.FLOW_ID";
		if (JecnContants.dbType.equals(DBType.ORACLE)) {
			sql = sql + " and (JAP.figure_id in ("
					+ JecnCommonSql.findPosSql(userId, projectId) + ")"
					+ " or JOAP.org_id in ("
					+ JecnCommonSql.findOrgSql(userId, projectId) + "))";
		}
		return sql;

	}

	/**
	 * 数据对接：我参与的的流程 sql
	 * 
	 * @return
	 */
	private static String buttParticipateProcessSql(Long projectId, Long userId) {
		String sql = "SELECT" + "       jfs.flow_id," + "       jfs.flow_name,"
				+ "       jfs.flow_id_input, jfs.pub_time,"
				+ "       jfs.IS_PUBLIC," + "       jfs.CREATE_DATE,"
				+ "      jfs.PRE_FLOW_ID," + "       jfs.SORT_ID"
				+ " from jecn_flow_structure_image  jfsi,"
				+ "       jecn_flow_structure        jfs,"
				+ "       jecn_flow_structure        pjfs,"
				+ "       process_station_related    prs,"
				+ "       jecn_flow_org              jfo,"
				+ "       jecn_flow_org_image        jfoi,"
				+ "       jecn_user_position_related jupr"
				+ " where jfsi.figure_id = prs.figure_flow_id"
				+ "   and prs.type = '0'"
				+ "   and prs.figure_position_id = jfoi.figure_id"
				+ "   and jfoi.org_id = jfo.org_id"
				+ "   and jfs.pre_flow_id = pjfs.flow_id"
				+ "   and jfo.del_state = 0" + "   and jfo.projectid = "
				+ projectId + "   and jfoi.figure_id = jupr.figure_id"
				+ "   and jupr.people_id = " + userId
				+ "   and jfsi.flow_id = jfs.flow_id"
				+ "   and jfs.del_state = 0" + "   and jfs.projectid = "
				+ projectId;
		return sql;
	}

	/**
	 * 数据对接：我关注的流程,我参与的流程sql
	 * 
	 * @param projectId
	 * @return
	 */
	public static void buttProcessSelectWithSql(Long projectId,
			StringBuffer sqlBuffer, Long userId) {
		sqlBuffer.append("WITH PUBLIC_PROCESS AS (");
		// 我关注的流程： 公开的流程
		sqlBuffer.append(buttPublicProcessSql(projectId));
		sqlBuffer.append(" UNION ");
		// 我关注的流程：秘密
		sqlBuffer.append(buttNoPublicProcessSql(projectId, userId));
		sqlBuffer.append(" UNION ");
		// 我参与的流程
		sqlBuffer.append(buttParticipateProcessSql(projectId, userId));
		sqlBuffer.append(")");

		if (JecnContants.dbType.equals(DBType.SQLSERVER)) {// sqlserver
			// 获取部门查阅权限时，人员岗位说书部门对应的所有父部门递归
			sqlBuffer
					.append(",TMP_ORG  AS (SELECT FO1.ORG_ID, FO1.PER_ORG_ID, FO1.ORG_NAME");
			sqlBuffer.append(" FROM JECN_FLOW_ORG FO1");

			sqlBuffer.append(" ),");
			sqlBuffer.append("  TMP_ORG2 AS (");
			sqlBuffer.append("  SELECT * FROM TMP_ORG WHERE ORG_ID in");
			sqlBuffer
					.append("  (SELECT POS.ORG_ID FROM JECN_USER_POSITION_RELATED JUPR, JECN_FLOW_ORG_IMAGE POS");
			sqlBuffer.append("  WHERE JUPR.PEOPLE_ID =" + userId);
			sqlBuffer.append(" AND JUPR.FIGURE_ID = POS.FIGURE_ID)");
			sqlBuffer.append("  UNION ALL ");
			sqlBuffer.append("  SELECT TMP_ORG.* FROM TMP_ORG2");
			sqlBuffer
					.append(" INNER JOIN TMP_ORG ON TMP_ORG2.PER_ORG_ID = TMP_ORG.org_id)");
		}
	}

	/**
	 * 数据对接：查询我关注的流程
	 * 
	 * @param projectId
	 * @return
	 */
	public static void buttBufferSelect(Long projectId, StringBuffer sqlBuffer) {
		sqlBuffer
				.append("SELECT distinct T.FLOW_ID, T.FLOW_NAME, T.FLOW_ID_INPUT,T.pub_time,");
		sqlBuffer
				.append("T.IS_PUBLIC,T.CREATE_DATE,T.PRE_FLOW_ID,T.SORT_ID FROM PUBLIC_PROCESS T ");
	}

	/**
	 * 数据对接：我关注的流程，我参与的流程 总数
	 * 
	 * @param projectId
	 * @return
	 */
	public static String buttProcessSqlCountSelect(Long projectId, Long userId) {
		StringBuffer sqlBuffer = new StringBuffer();
		buttProcessSelectWithSql(projectId, sqlBuffer, userId);
		sqlBuffer
				.append("SELECT  COUNT(DISTINCT T.FLOW_ID) FROM PUBLIC_PROCESS T ");
		return sqlBuffer.toString();
	}
}
