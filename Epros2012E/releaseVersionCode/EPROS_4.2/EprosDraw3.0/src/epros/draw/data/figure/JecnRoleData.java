package epros.draw.data.figure;

import java.util.ArrayList;
import java.util.List;

import epros.draw.constant.JecnToolBoxConstant.MapElemType;
import epros.draw.data.JecnAbstractFlowElementData;
import epros.draw.designer.JecnFlowStationT;

/**
 * 
 * 角色数据层
 * 
 * @author Administrator
 * 
 */
public class JecnRoleData extends JecnFigureData {
	/** 关联岗位或岗位组 */
	private List<JecnFlowStationT> flowStationList = new ArrayList<JecnFlowStationT>();
	/** 岗位名称 */
	private String posName = null;
	/** 岗位组名称 */
	private String posGroupName = null;
	/** 角色职责 */
	private String roleRes;
	/** 值为4时为主责岗位 数据据库ACTIVITY_ID(活动编号) 同用一个字段 */
	private String activityId;
	/** 角色活动是否对应 */
	private boolean isRefActive = false;

	/** 多个角色对应同一个活动 */
	private boolean isMoreRefActive = false;

	public JecnRoleData(MapElemType mapElemType) {
		super(mapElemType);
	}

	/**
	 * 实例化对象
	 * 
	 */
	public JecnRoleData newInstance() {
		return new JecnRoleData(mapElemType);
	}

	@Override
	/**
	 * 
	 * 克隆对象
	 * 
	 */
	public JecnRoleData clone() {
		return (JecnRoleData) this.currClone();
	}

	/**
	 * 
	 * 
	 * 复制流程元素属性
	 * 
	 * @param srcFlowElementData
	 *            JecnAbstractFlowElementData 源流程元素
	 * @param destFlowElementData
	 *            JecnAbstractFlowElementData 目标流程元素
	 */
	public void setFlowElementAttributes(JecnAbstractFlowElementData srcFlowElementData,
			JecnAbstractFlowElementData destFlowElementData) {

		if (!(srcFlowElementData instanceof JecnRoleData) || !(destFlowElementData instanceof JecnRoleData)) {
			return;
		}
		super.setFlowElementAttributes(srcFlowElementData, destFlowElementData);

		setActiveArrts(srcFlowElementData, destFlowElementData);
	}

	public void setActiveArrts(JecnAbstractFlowElementData srcFlowElementData,
			JecnAbstractFlowElementData destFlowElementData) {
		JecnRoleData src = (JecnRoleData) srcFlowElementData;
		JecnRoleData dest = (JecnRoleData) destFlowElementData;

		dest.setFigureText(src.getFigureText());
		// 角色职责
		dest.setRoleRes(src.getRoleRes());
		// 关联岗位或岗位组
//		for (JecnFlowStationT flowStationT : src.getFlowStationList()) {
//			dest.getFlowStationList().add(flowStationT.clone());
//		}
		// 值为4时为主责岗位 数据据库ACTIVITY_ID(活动编号) 同用一个字段
		dest.setActivityId(src.getActivityId());

		// 岗位名称 (画图工具专用)
		dest.setPosName(src.getPosName());
	}

	public List<JecnFlowStationT> getFlowStationList() {
		return flowStationList;
	}

	public void setFlowStationList(List<JecnFlowStationT> flowStationList) {
		if (flowStationList == null) {
			this.flowStationList = new ArrayList<JecnFlowStationT>();
			return;
		}
		this.flowStationList = flowStationList;
	}

	public String getActivityId() {
		return activityId;
	}

	public void setActivityId(String activityId) {
		this.activityId = activityId;
	}

	public String getRoleRes() {
		return roleRes;
	}

	public void setRoleRes(String roleRes) {
		this.roleRes = roleRes;
	}

	/**
	 * 
	 * 画图工具专用
	 * 
	 * @return String 岗位名称
	 */
	public String getPosName() {
		return posName;
	}

	/**
	 * 
	 * 画图工具专用
	 * 
	 * @return String 岗位名称
	 */
	public void setPosName(String posName) {
		this.posName = posName;
	}

	public boolean isRefActive() {
		return isRefActive;
	}

	public void setRefActive(boolean isRefActive) {
		this.isRefActive = isRefActive;
	}

	public boolean isMoreRefActive() {
		return isMoreRefActive;
	}

	public void setMoreRefActive(boolean isMoreRefActive) {
		this.isMoreRefActive = isMoreRefActive;
	}

	public String getPosGroupName() {
		return posGroupName;
	}

	public void setPosGroupName(String posGroupName) {
		this.posGroupName = posGroupName;
	}
}
