package epros.designer.gui.process.guide.guideTable;

import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;

import org.apache.log4j.Logger;

import com.jecn.epros.server.bean.process.JecnFlowRecordT;
import com.jecn.epros.server.bean.task.JecnTaskHistoryNew;

import epros.designer.gui.popedom.person.JecnPersonInfoPanel;
import epros.designer.util.ConnectionPool;
import epros.designer.util.JecnProperties;
import epros.draw.gui.ui.JecnBasicTableHeaderUI;
import epros.draw.util.JecnUIUtil;

/***
 * 记录保存
 * @author 2012-07-05
 *
 */
public class RecordSaveTable extends JTable {
	private static Logger log = Logger.getLogger(RecordSaveTable.class);
	/** 获得流程记录保存临时数据 */
	private List<JecnFlowRecordT> listFlowRecordT = new ArrayList<JecnFlowRecordT>();
	private Long flowId = null;
	public RecordSaveTable(Long flowId) {
		this.flowId = flowId;
		this.setModel(getTableModel());
		if (isSelectMutil()) {
			this.getSelectionModel().setSelectionMode(
					ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
		} else {
			this.getSelectionModel().setSelectionMode(
					ListSelectionModel.SINGLE_SELECTION);
		}
		int[] cols = gethiddenCols();
		if (cols != null && cols.length > 0) {
			TableColumnModel columnModel = this.getColumnModel();
			for (int col : cols) {
				TableColumn tableColumn = columnModel.getColumn(col);
				tableColumn.setMinWidth(0);
				tableColumn.setMaxWidth(0);
			}
		}

		// 设置默认背景色
		this.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		this.getTableHeader().setUI(new JecnBasicTableHeaderUI());
		this.getTableHeader().setReorderingAllowed(false);
	}

	public Vector<Vector<String>> getContent() {
		Vector<Vector<String>> vs = new Vector<Vector<String>>();
		// 获取流程记录保存数据
		try {
			listFlowRecordT = ConnectionPool.getProcessAction().getJecnFlowRecordTListByFlowId(flowId);
			for (int i = 0; i < listFlowRecordT.size(); i++) {
				JecnFlowRecordT flowRecordT = listFlowRecordT.get(i);
				Vector<String> row = new Vector<String>();
				row.add(String.valueOf(flowRecordT.getId()));
				//记录名称
				row.add(flowRecordT.getRecordName());
				//保存责任人
				row.add(flowRecordT.getRecordSavePeople());
				//保存场所
				row.add(flowRecordT.getSaveLaction());
				//归档时间
				row.add(flowRecordT.getFileTime());
				//保存期限
				row.add(flowRecordT.getSaveTime());
				//到期处理方式
				row.add(flowRecordT.getRecordApproach());
				vs.add(row);
			}

		} catch (Exception e) {
			log.error("RecordSaveTable getContent is error",e);
		}

		return vs;
	}
	
	public RecordSaveTableMode getTableModel() {
		Vector<String> title = new Vector<String>();
		title.add(JecnProperties.getValue("id"));
		/*
		 * 记录名称
		 */
		title.add(JecnProperties.getValue("recordName"));
		title.add(JecnProperties.getValue("saveResponsiblePersons"));
		title.add(JecnProperties.getValue("savPlace"));
		title.add(JecnProperties.getValue("filingTime"));
		/*
		 * 保存期限
		 */
		title.add(JecnProperties.getValue("storageLife"));
		title.add(JecnProperties.getValue("treatmentDue"));
		return new RecordSaveTableMode(getContent(),
				title);
	}

	public boolean isSelectMutil() {
		return false;
	}

	public int[] gethiddenCols() {

		return new int[] { 0 };
	}
	//获取数据
	public Vector<Vector<String>> updateTableContent(
			List<JecnTaskHistoryNew> list) {
		Vector<Vector<String>> vector = new Vector<Vector<String>>();
		return vector;
	}
	class RecordSaveTableMode extends DefaultTableModel {
		public RecordSaveTableMode(Vector<Vector<String>> data,
				Vector<String> title) {
			super(data, title);

		}

		public boolean isCellEditable(int rowindex, int colindex) {
			return false;
		}

	}
}
