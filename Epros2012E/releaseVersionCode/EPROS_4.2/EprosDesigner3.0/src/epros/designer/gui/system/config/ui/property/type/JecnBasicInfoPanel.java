package epros.designer.gui.system.config.ui.property.type;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JLabel;

import com.jecn.epros.server.bean.system.JecnConfigItemBean;
import com.jecn.epros.server.common.JecnConfigContents;
import com.jecn.epros.server.common.JecnConfigContents.ConfigItemPartMapMark;

import epros.designer.gui.common.JecnConstants;
import epros.designer.gui.system.config.bean.JecnConfigTypeDesgBean;
import epros.designer.gui.system.config.ui.comp.JecnConfigCheckBox;
import epros.designer.gui.system.config.ui.comp.JecnConfigRadioButton;
import epros.designer.gui.system.config.ui.comp.JecnConfigTextField;
import epros.designer.gui.system.config.ui.comp.JecnDocumentListener;
import epros.designer.gui.system.config.ui.dialog.JecnAbtractBaseConfigDialog;
import epros.designer.gui.system.config.ui.property.JecnAbstractPropertyBasePanel;
import epros.designer.gui.system.config.ui.property.table.JecnThreeTableScrollPane;
import epros.designer.util.JecnProperties;
import epros.draw.gui.swing.JecnPanel;
import epros.draw.gui.swing.JecnUserInfoTextArea;
import epros.draw.util.DrawCommon;
import epros.draw.util.JecnUIUtil;

/**
 * 
 * 基本信息面板
 * 
 * @author ZHOUXY
 * 
 */
public class JecnBasicInfoPanel extends JecnAbstractPropertyBasePanel implements ActionListener {

	/** 其他项面板 */
	private JecnPanel otherPanel = null;

	/** 流程责任人面板 */
	private JecnPanel flowResponsePanel = null;

	/** 流程责任人 start */
	/** 标签 */
	private JLabel flowResponseLab = null;
	/** 人员 */
	private JecnConfigRadioButton peopleRadio = null;
	/** 岗位 */
	private JecnConfigRadioButton posRadio = null;
	/** 流程责任人 end */

	/** 有效期Lab */
	private JLabel validityLab = new JLabel(JecnProperties.getValue("validMonthC"));

	/** 有效期Field */
	private JecnConfigTextField validityField = new JecnConfigTextField();
	/** 有效期提示信息 */
	private JecnUserInfoTextArea validityInfoArea = new JecnUserInfoTextArea();
	/** 流程有限期面板 */
	private JecnPanel validityPanel = new JecnPanel();
	/** 是否永久有效复选框 */
	private JecnConfigCheckBox validityCheckBox = new JecnConfigCheckBox(JecnProperties.getValue("foreverC"));
	/** 是否可编辑有效复选框 */
	private JecnConfigCheckBox validityCheckBoxEdit = new JecnConfigCheckBox(JecnProperties.getValue("edit"));
	
	public JecnBasicInfoPanel(JecnAbtractBaseConfigDialog dialog) {
		super(dialog);
		iniComponents();
		initLayout();
	}

	/**
	 * 
	 * 初始化组件
	 * 
	 */
	private void iniComponents() {
		// 表面板
		tableScrollPane = new JecnThreeTableScrollPane(dialog);
		// 其他项面板
		otherPanel = new JecnPanel();
		// 流程责任人面板
		flowResponsePanel = new JecnPanel();

		// **************流程责任人 start **************//
		// 标签
		flowResponseLab = new JLabel(JecnProperties.getValue("task_flowResponsePerC"));
		// 人员
		peopleRadio = new JecnConfigRadioButton(JecnProperties.getValue("person"));
		// 岗位
		posRadio = new JecnConfigRadioButton(JecnProperties.getValue("position"));
		// **************流程责任人 end **************//

		validityCheckBox.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		// 有效期
		validityCheckBox.addActionListener(this);
		
		validityCheckBoxEdit.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		// 有效期
		validityCheckBoxEdit.addActionListener(this);


		validityCheckBox.setSelected(false);
		
		validityField.setEditable(false);
		validityInfoArea.setForeground(Color.red);
		// 事件
		validityField.getDocument()
				.addDocumentListener(new JecnDocumentListener(this, validityField, validityInfoArea));
		validityPanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());

		// / 流程责任人 按钮实现 单选效果
		ButtonGroup bgpp = new ButtonGroup();
		bgpp.add(peopleRadio);
		bgpp.add(posRadio);

		// 流程责任人：人员
		peopleRadio.setOpaque(false);
		peopleRadio.setSelected(true);
		// 流程责任人：岗位
		posRadio.setOpaque(false);
		// 其他项面板
		otherPanel.setOpaque(false);
		otherPanel.setBorder(null);
		// 流程责任人面板
		flowResponsePanel.setOpaque(false);
		flowResponsePanel.setBorder(null);

		// 人员
		peopleRadio.addActionListener(this);
		// 岗位
		posRadio.addActionListener(this);
		// 是否存在浏览端，如果存在浏览端则隐藏 流程责任人类型
		if (!JecnConstants.isPubShow()) {
			// 流程责任人类型
			flowResponsePanel.setVisible(false);
		}
	}

	/**
	 * 
	 * 初始化布局
	 * 
	 */
	private void initLayout() {
		// 是否存在浏览端，如果存在浏览端则隐藏表格
		if (JecnConstants.isPubShow()) {
			this.add(tableScrollPane, BorderLayout.CENTER);
			this.add(otherPanel, BorderLayout.SOUTH);
		} else {
			this.add(otherPanel, BorderLayout.NORTH);
		}

		Insets insets = new Insets(5, 5, 5, 5);

		// 角色、活动上下限面板
		GridBagConstraints c = null;
		int index = 0;
		// 流程责任人面板
		c = new GridBagConstraints(0, index, 1, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				JecnUIUtil.getInsets0(), 0, 0);
		otherPanel.add(flowResponsePanel, c);
		index++;
		// 流程有效期
		c = new GridBagConstraints(0, index, 1, 1, 1.0, 0.0, GridBagConstraints.NORTHWEST,
				GridBagConstraints.HORIZONTAL, new Insets(0, 0, 0, 0), 0, 0);
		otherPanel.add(validityPanel, c);
		index++;
		// 有效期提示
		c = new GridBagConstraints(0, index, 1, 1, 1.0, 0.0, GridBagConstraints.EAST, GridBagConstraints.HORIZONTAL,
				new Insets(0, 80, 0, 0), 0, 0);
		otherPanel.add(validityInfoArea, c);
		index++;
		// **************流程责任人 start **************//
		index = 0;
		// 流程责任人:标签
		c = new GridBagConstraints(index, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, insets,
				0, 0);
		flowResponsePanel.add(flowResponseLab, c);
		index++;
		// 流程责任人:人员
		c = new GridBagConstraints(index, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, insets,
				0, 0);
		flowResponsePanel.add(peopleRadio, c);
		index++;
		// 流程责任人: 岗位
		c = new GridBagConstraints(index, 0, 1, 1, 0.0, 0.0, GridBagConstraints.EAST, GridBagConstraints.NONE, insets,
				0, 0);
		flowResponsePanel.add(posRadio, c);
		index++;
		// 空闲区域
		c = new GridBagConstraints(index, 0, 1, 1, 1.0, 0.0, GridBagConstraints.EAST, GridBagConstraints.HORIZONTAL,
				insets, 0, 0);
		flowResponsePanel.add(new JLabel(), c);
		index++;
		// **************流程责任人 end **************//

		// 添加流程属性
		addValidityPanel();
	}

	private void addValidityPanel() {
		validityPanel.setLayout(new GridBagLayout());
		GridBagConstraints c = new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
				GridBagConstraints.NONE, new Insets(0, 8, 0, 0), 0, 0);
		validityPanel.add(validityLab, c);
		c = new GridBagConstraints(2, 0, 1, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				new Insets(0, 0, 0, 5), 0, 0);
		validityPanel.add(validityField, c);

		c = new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(
				0, 0, 0, 5), 0, 0);
		validityPanel.add(validityCheckBox, c);
		
		c = new GridBagConstraints(3, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(
				0, 0, 0, 5), 0, 0);
		validityPanel.add(validityCheckBoxEdit, c);
		
		
	}

	/**
	 * 
	 * 加载数据
	 * 
	 * @param configTypeDesgBean
	 *            JecnConfigTypeDesgBean
	 */
	@Override
	public void initData(JecnConfigTypeDesgBean configTypeDesgBean) {
		this.configTypeDesgBean = configTypeDesgBean;

		if (configTypeDesgBean == null || !configTypeDesgBean.existsItemListData()) {
			return;
		}

		for (JecnConfigItemBean itemBean : configTypeDesgBean.getOtherItemList()) {
			// 唯一标识
			String mark = itemBean.getMark();
			// 值
			String value = itemBean.getValue();
			if (ConfigItemPartMapMark.basicDutyOfficer.toString().equals(mark)) {// 流程责任人类型：0:
				// 选择人
				// 1:选择岗位
				peopleRadio.setItemBean(itemBean);
				posRadio.setItemBean(itemBean);
				if (JecnConfigContents.ITEM_FLOW_USER.equals(value)) {// 流程责任人类型是人
					peopleRadio.setSelected(true);
				} else {
					posRadio.setSelected(true);
				}
			} else if (ConfigItemPartMapMark.processValidDefaultValue.toString().equals(mark)) {// 流程有效期
				validityField.setItemBean(itemBean);
				if ("0".equals(value)) {
					validityCheckBox.setSelected(true);
					validityField.setEditable(false);
				} else {
					validityCheckBox.setSelected(false);
					validityField.setEditable(true);
					validityField.setText(value);
				}
			} else if (ConfigItemPartMapMark.processValidForever.toString().equals(mark)) {// 流程有效期
				validityCheckBox.setItemBean(itemBean);
				if ("0".equals(value)) {
					validityCheckBox.setSelected(true);
					validityField.setEditable(false);
				} else {
					validityCheckBox.setSelected(false);
					validityField.setEditable(true);
				}
			} else if (ConfigItemPartMapMark.isShowProcessValidity.toString().equals(mark)) {// 流程有效期
				validityCheckBoxEdit.setItemBean(itemBean);
				if ("0".equals(value)) {
					validityCheckBoxEdit.setSelected(false);
				} else {
					validityCheckBoxEdit.setSelected(true);
				}
			}
			
		}

		// 排序过滤隐藏项
		List<JecnConfigItemBean> showTableItemList = tableScrollPane.getShowItem(configTypeDesgBean.getTableItemList(),
				JecnConfigContents.TYPE_SMALL_ITEM_BASICINFO);

		// 显示表数据
		tableScrollPane.initData(showTableItemList);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() instanceof JecnConfigRadioButton) {
			// 流程责任人类型：0: 选择人 1:选择岗位
			JecnConfigRadioButton configRadioButton = (JecnConfigRadioButton) e.getSource();
			if (configRadioButton.isSelected()) {// 选中
				if (configRadioButton == peopleRadio) {// 人员
					peopleRadio.getItemBean().setValue(JecnConfigContents.ITEM_FLOW_USER);
				} else if (configRadioButton == posRadio) {// 岗位
					peopleRadio.getItemBean().setValue(JecnConfigContents.ITEM_FLOW_POS);
				}
			}
		} else if (e.getSource() instanceof JecnConfigCheckBox) {
			if (e.getSource() == validityCheckBox) {// 1：选中；0不选中
				if (validityCheckBox.isSelected()) {// 选中 ：有效期永久
					validityField.setEditable(false);
					// 永久有效期值默认为：0
					validityCheckBox.getItemBean().setValue(JecnConfigContents.ITEM_AUTO_SAVE_NO);
					validityField.setText("");
					validityField.getItemBean().setValue("0");
				} else {
					validityField.setEditable(true);
					validityCheckBox.getItemBean().setValue(JecnConfigContents.ITEM_AUTO_SAVE_YES);
					// 不选中有效期永久且输入框中内容为空，设置JecnConfigItemBean中的有效期值为空
					if ("".equals(validityField.getText())) {
						validityField.getItemBean().setValue("9");
						validityField.setText("9");
					}
				}
			}
			if (e.getSource() == validityCheckBoxEdit) {// 1：选中；0不选中
				if (validityCheckBoxEdit.isSelected()) {// 选中 ：可编辑
					validityCheckBoxEdit.getItemBean().setValue(JecnConfigContents.ITEM_AUTO_SAVE_YES);
				} else {
					validityCheckBoxEdit.getItemBean().setValue(JecnConfigContents.ITEM_AUTO_SAVE_NO);
				}
			}
		}
	}

	/**
	 * 
	 * 校验数据
	 * 
	 * @return boolean 校验成功：true ；校验失败：false
	 * 
	 */
	@Override
	public boolean check() {
		// 有效期验证
		boolean isValidity = getNumCheckInfo(validityField.getText());

		if (isValidity) {// 验证失败
			return false;
		}

		if (!getOkJButton().isEnabled()) {// 验证成功
			getOkJButton().setEnabled(true);
		}
		getInfoLable().setText("");
		return true;
	}

	private JecnUserInfoTextArea getInfoLable() {
		return dialog.getOkCancelJButtonPanel().getInfoLabel();
	}

	private JButton getOkJButton() {
		return dialog.getOkCancelJButtonPanel().getOkJButton();
	}

	/**
	 * 判断字符串是否为数字
	 * 
	 * @param strValidity
	 * @param isSelect
	 * @return true:存在错误信息
	 */
	private boolean getNumCheckInfo(String strValidity) {
		// 获取有效期
		String reg = "^[0-9]*$";
		String tipStr = "";
		// 有效期 值
		if (!validityCheckBox.isSelected()) {
			if (strValidity == null || "".equals(strValidity)) {
				tipStr = JecnProperties.getValue("validNotNullC");
			} else if (!strValidity.matches(reg)) {
				tipStr = JecnProperties.getValue("validIsNumC");
			} else if (strValidity.length() > 5 || Integer.parseInt(strValidity) > 9000) {// 如果输入的有效期大于9000则提示重新输入
				// “strValidity.length() >
				// 5”判断是为了避免Integer.parseInt(strValidity)报错， 不能超过9000个月
				tipStr = JecnProperties.getValue("validSuperfluousMonthC");
			} else if (Integer.parseInt(strValidity) <= 0) {
				tipStr = JecnProperties.getValue("validSuperfluousZeroC");
			} else {
				tipStr = "";
			}
			if (DrawCommon.isNullOrEmtry(tipStr)) {
				return false;
			}
			getInfoLable().setText(tipStr);
			return true;
		}
		return false;
	}
}
