package com.jecn.epros.server.action.web.login.ad.mulinsen;

public class MulinsenSendMailToOA {
	/** 允许授权接入的app名称 */
	private String appName;
	/** 收件人工号，多个请使用英文逗号隔开 */
	private String to_employee_id;
	/** 邮件标题 */
	private String subject;
	/** 邮件正文内容 */
	private String content;

	private String sign;

	public String getAppName() {
		return appName;
	}

	public void setAppName(String appName) {
		this.appName = appName;
	}

	public String getTo_employee_id() {
		return to_employee_id;
	}

	public void setTo_employee_id(String toEmployeeId) {
		to_employee_id = toEmployeeId;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getSign() {
		return sign;
	}

	public void setSign(String sign) {
		this.sign = sign;
	}

}
