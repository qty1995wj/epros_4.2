package com.jecn.epros.server.bean.process;

/**
 * 获得样例表（临时）
 * 
 * @author Administrator
 * 
 */
public class JecnTempletT implements java.io.Serializable {
	/** 主键ID */
	private Long id;
	/** 输出的模板主键ID */
	private Long modeFileId;
	/** 文件Id */
	private Long fileId;
	/** 文件名称 */
	private String fileName;
	/** 文件编号 */
	private String fileNumber;
	private String modeUUID;// 输出元素索引
	private String UUID;

	public String getFileNumber() {
		return fileNumber;
	}

	public void setFileNumber(String fileNumber) {
		this.fileNumber = fileNumber;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getModeFileId() {
		return modeFileId;
	}

	public void setModeFileId(Long modeFileId) {
		this.modeFileId = modeFileId;
	}

	public Long getFileId() {
		return fileId;
	}

	public void setFileId(Long fileId) {
		this.fileId = fileId;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getModeUUID() {
		return modeUUID;
	}

	public void setModeUUID(String modeUUID) {
		this.modeUUID = modeUUID;
	}

	public String getUUID() {
		return UUID;
	}

	public void setUUID(String uUID) {
		UUID = uUID;
	}
}
