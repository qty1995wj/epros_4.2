package epros.draw.gui.workflow;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.SwingConstants;
import javax.swing.UIManager;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import epros.draw.constant.JecnToolbarConstant.TooBarTitleNumEnum;
import epros.draw.constant.JecnWorkflowConstant.EasyProcessType;
import epros.draw.gui.key.JecnKeyCheck;
import epros.draw.gui.top.frame.JecnFrame;
import epros.draw.gui.top.frame.listener.JecnFrameEvent;
import epros.draw.gui.top.frame.listener.JecnFrameListener;
import epros.draw.gui.top.toolbar.io.JecnFlowImport;
import epros.draw.gui.workflow.popup.JecnCloseableTabbedPopupMenu;
import epros.draw.system.JecnEprosVersion;
import epros.draw.system.JecnUserCheckInfoProcess;
import epros.draw.system.data.JecnSystemStaticData;
import epros.draw.util.JecnUIUtil;
import epros.draw.util.DrawCommon;

/**
 * 
 * 窗体 软件启动入口
 * 
 * @author ZHOUXY
 * 
 */
public class JecnMainJFrame {
	private static final Log log = LogFactory.getLog(JecnMainJFrame.class);
	/** 主窗体 */
	private static JecnMainJFrame mainJFrame = null;
	/** 信息提示标签 */
	private JLabel infoLabel = null;

	public JecnMainJFrame() {
		initCompotents();
		initInfoLabel();
	}

	private void initCompotents() {
		JecnFrame frame = new JecnFrame();
		// 主要解决弹出对话框（Jecndialog）模态不好使问题
		JecnSystemStaticData.setFrame(frame);

		// 信息提示标签
		infoLabel = new JLabel();
		frame.addCompToFreePanel(infoLabel);

		// 期望大小
		frame.setPreferredSize(JecnUIUtil.getEprosDrawSize());
		// 最小大小
		frame.setMinimumSize(JecnUIUtil.getEprosDrawSize());
		frame.setTitle(JecnEprosVersion.getEprosVersion().getFrameTitleName());
		// 居中
		frame.setLocationRelativeTo(null);
		// 关闭退出
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		// 默认最大化
		frame.setMaxFrameBounds();

		// 画图主界面
		JecnDrawMainPanel mainPanel = JecnDrawMainPanel.getMainPanel();
		// 添加工具栏标题按钮面板
		frame.addToolTitlePanel(mainPanel.getToolbarTitle());
		// 窗体空闲区域添加显示模板组合框
		frame.addRightBottomPanel(mainPanel.getModuleShowPanel());

		frame.getContentPane().add(mainPanel);

		frame.setVisible(true);

		// ************密钥验证************//
		// 密钥验证
		if(!"four".equals(JecnSystemStaticData.getTooBarTitleNumEnum().toString())){//EPROS-M不需要秘钥
			new JecnKeyCheck().process();
		}
		// ************密钥验证************//

		// 任务栏右键关闭
		frame.addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent e) {
				JecnCloseableTabbedPopupMenu.removeAllPanel(false);
			}
		});

		frame.addJecnFrameListener(new JecnFrameListener() {
			public boolean FrameCloseBefore(JecnFrameEvent e) {
				// 是否保存验证
				return JecnCloseableTabbedPopupMenu.removeAllPanel(true);
			}
		}, this);
	}

	/**
	 * 
	 * 初始化信息提示标签属性
	 * 
	 */
	private void initInfoLabel() {
		infoLabel.setOpaque(false);
		infoLabel.setFont(new Font(Font.DIALOG, Font.PLAIN, 15));
		infoLabel.setHorizontalAlignment(SwingConstants.CENTER);
		infoLabel.setVerticalAlignment(SwingConstants.CENTER);
		infoLabel.setForeground(Color.red);

	}

	/**
	 * 
	 * 获取主面板Frame
	 * 
	 * @return JecnMainJFrame
	 */
	public static JecnMainJFrame getMainJFrame() {
		return mainJFrame;
	}

	public JLabel getInfoLabel() {
		return infoLabel;
	}

	/**
	 * 
	 * 双击操作系统EPors后缀文件，使用epros软件打开
	 * 
	 * @param args
	 *            String[]
	 */
	private void localDClickOpenEprosFile(String[] args) {
		// 双击文件路径
		String path = "";
		for (int i = 0; i < args.length; i++) {
			log.info("args[i]：path=" + path);
			path = path + args[i] + " ";
		}
		log.info("双击操作系统EPors后缀文件打开路径：path=" + path);
		if (!DrawCommon.isNullOrEmtryTrim(path)) {
			// 去空
			path = path.trim();
			log.info("去空后路径：path=" + path);
			if (!DrawCommon.isNullOrEmtryTrim(path)) {
				// 打开文件
				JecnFlowImport flowImport = new JecnFlowImport();
				flowImport.inportEprosFile(path);
			}
		}
	}

	public static void main(String[] args) {

		// 屏蔽ABC输入法bug
		System.setProperty("java.awt.im.style", "on-the-spot");

		try {

			UIManager
					.setLookAndFeel("com.sun.java.swing.plaf.windows.WindowsLookAndFeel");
		} catch (Exception e) {
			log.error("", e);
		}

		// 加载本地资源文件
		JecnUserCheckInfoProcess.start();
		// 画图工具（不带操作说明等）
		JecnSystemStaticData
				.setEasyProcessType(EasyProcessType.easyProcessDrawer);
		// 工具栏标题个数
		JecnSystemStaticData.setTooBarTitleNumEnum(TooBarTitleNumEnum.four);

		// 启动组件
		mainJFrame = new JecnMainJFrame();

		// 双击操作系统EPors后缀文件，使用epros软件打开
		mainJFrame.localDClickOpenEprosFile(args);
	}

}
