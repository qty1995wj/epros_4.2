package epros.draw.data.line;

import epros.draw.constant.JecnToolBoxConstant.MapElemType;
import epros.draw.system.JecnSystemData;

public class JecnBaseVHDiviLineData extends JecnBaseLineData {
	/** 小线段数据对象 */
	protected JecnLineSegmentDataCloneable lineSegmentDataClone = null;

	public JecnBaseVHDiviLineData(MapElemType mapElemType) {
		super(mapElemType);

		this.lineSegmentDataClone = new JecnLineSegmentDataCloneable();
		// 初始化图形属性
		JecnSystemData.initCurrFlowElementData(this);
	}

	@Override
	public JecnBaseVHDiviLineData clone() {
		return new JecnBaseVHDiviLineData(mapElemType);
	}

	@Override
	public JecnBaseVHDiviLineData newInstance() {
		return (JecnBaseVHDiviLineData) this.currClone();
	}

	public JecnLineSegmentDataCloneable getLineSegmentDataClone() {
		return lineSegmentDataClone;
	}

}
