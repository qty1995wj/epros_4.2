package epros.designer.gui.process.guide;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;

import org.apache.log4j.Logger;

import com.jecn.epros.server.bean.process.JecnFlowStructureT;
import com.jecn.epros.server.bean.rule.RuleT;
import com.jecn.epros.server.bean.standard.StandardBean;
import com.jecn.epros.server.bean.tree.JecnTreeBean;
import com.jecn.epros.server.util.JecnTreeEnumConstant.TreeNodeType;

import epros.designer.gui.common.JecnConstants;
import epros.designer.gui.common.JecnDesignerCommon;
import epros.designer.gui.popedom.person.JecnPersonInfoPanel;
import epros.designer.gui.process.guide.guideTable.OrderTable;
import epros.designer.gui.rule.RuleChooseDialog;
import epros.designer.gui.standard.StandardChooseDialog;
import epros.designer.util.ConnectionPool;
import epros.designer.util.JecnProperties;
import epros.designer.util.JecnUtil;
import epros.draw.gui.swing.JecnPanel;
import epros.draw.gui.top.dialog.JecnDialog;
import epros.draw.gui.ui.JecnBasicTableHeaderUI;
import epros.draw.util.JecnUIUtil;

/**
 * 相关流程及制度,标准
 * 
 * @author 2012-07-04
 * 
 */
public class ProcessAndSystemDialog extends JecnDialog {
	private static Logger log = Logger.getLogger(ProcessAndSystemDialog.class);
	/** 流程ID */
	private Long flowId = null;// 6359L;
	/** 主面板 */
	private JecnPanel mainPanel = new JecnPanel();
	/** 信息面板 */
	private JecnPanel infoPanel = new JecnPanel();
	/** 流程processPanel */
	private JecnPanel processPanel = new JecnPanel();

	/** 流程滚动面板 */
	private JScrollPane processScrollPane = new JScrollPane();

	private ProcessTable processTable = null;

	/** 制度systemPanel */
	private JecnPanel systemPanel = new JecnPanel();

	/** 制度滚动面板 */
	private JScrollPane systemScrollPane = new JScrollPane();

	/** 制度表 */
	private SystemTable systemTable = null;

	/** 相关标准 */
	private JecnPanel orderPanel = new JecnPanel();

	/** 标准滚动面板 */
	private JScrollPane orderScrollPane = new JScrollPane();

	/** 标准表 */
	private OrderTable orderTable = null;

	/** 按钮面板 */
	private JecnPanel butPanel = new JecnPanel();

	/** 制度选择按钮 */
	private JButton systemBut = new JButton(JecnProperties
			.getValue("selectBtn"));

	/** 打开制度文件按钮 */
	private JButton openSystemBut = new JButton(JecnProperties
			.getValue("openBtn"));

	/** 标准选择按钮 */
	private JButton orderBut = new JButton(JecnProperties.getValue("selectBtn"));

	/** 打开标准文件按钮 */
	private JButton openOrderBut = new JButton(JecnProperties
			.getValue("openBtn"));

	/** 制度ID */
	private String ruleIds = null;
	/** 标准ID */
	private String standerIds = null;
	/** 制度 */
	protected List<JecnTreeBean> listRule = new ArrayList<JecnTreeBean>();
	/** 标准 */
	protected List<JecnTreeBean> listStander = new ArrayList<JecnTreeBean>();

	/** 确定 */
	private JButton okBut = new JButton(JecnProperties.getValue("okBtn"));

	/** 取消 */
	private JButton cancelBut = new JButton(JecnProperties
			.getValue("cancelBtn"));

	/** 验证提示tab */
	private JLabel verfyLab = new JLabel();

	/** 设置大小 */
	Dimension dimension = null;

	/** 获得流程相关制度数据 */
	// private List<RuleT> listRuleT = new ArrayList<RuleT>();
	private List<RuleT> listRuleBean = new ArrayList<RuleT>();
	/** 获得流程相关标准数据 */
	private List<StandardBean> listStrand = new ArrayList<StandardBean>();
	/** 获得流程相关流程数据 */
	private List<Object[]> listFlow = new ArrayList<Object[]>();
	private List<JecnFlowStructureT> listflowStrutureT = new ArrayList<JecnFlowStructureT>();

	public ProcessAndSystemDialog(Long flowId) {
		this.flowId = flowId;
		this.setSize(570, 520);
		this.setLocationRelativeTo(null);
		this.setTitle(JecnProperties.getValue("linkFlowAndInstBtn"));
		this.setModal(true);
		this.setResizable(true);

		mainPanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		infoPanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		processPanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		processScrollPane.getViewport().setBackground(
				JecnUIUtil.getDefaultBackgroundColor());
		systemPanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		systemScrollPane.getViewport().setBackground(
				JecnUIUtil.getDefaultBackgroundColor());
		orderScrollPane.getViewport().setBackground(
				JecnUIUtil.getDefaultBackgroundColor());
		verfyLab.setForeground(Color.red);
		try {
			// 相关流程
			listFlow = ConnectionPool.getProcessAction()
					.getFlowRelateByFlowIdDesign(flowId);
			// for(Object obj :listFlow){
			// JecTreeBean
			// }

			// 相关制度
			listRuleBean = ConnectionPool.getProcessAction().getRuleTByFlowId(
					flowId);
			for (RuleT ruleT : listRuleBean) {
				if (ruleIds == null) {
					ruleIds = String.valueOf(ruleT.getId());
				} else {
					ruleIds = ruleIds + "," + String.valueOf(ruleT.getId());
				}

				JecnTreeBean jecnTreeBean = new JecnTreeBean();
				jecnTreeBean.setId(ruleT.getId());
				jecnTreeBean.setName(ruleT.getRuleName());
				jecnTreeBean.setPid(ruleT.getPerId());
				jecnTreeBean.setNumberId(ruleT.getRuleNumber());
				if (ruleT.getIsDir() == 0) {
					jecnTreeBean.setTreeNodeType(TreeNodeType.ruleDir);
				} else if (ruleT.getIsDir() == 1) {
					jecnTreeBean.setTreeNodeType(TreeNodeType.ruleModeFile);
				} else if (ruleT.getIsDir() == 2) {
					jecnTreeBean.setTreeNodeType(TreeNodeType.ruleFile);
				}
				listRule.add(jecnTreeBean);

			}
			// 相关标准
			listStrand = ConnectionPool.getProcessAction()
					.getStandardBeanByFlowId(flowId);
			for (StandardBean standardBean : listStrand) {
				if (standerIds == null) {
					standerIds = String.valueOf(standardBean
							.getCriterionClassId());
				} else {
					standerIds = standerIds
							+ ","
							+ String
									.valueOf(standardBean.getCriterionClassId());
				}
				JecnTreeBean jecnTreeBean = new JecnTreeBean();
				jecnTreeBean.setId(standardBean.getCriterionClassId());
				jecnTreeBean.setName(standardBean.getCriterionClassName());
				jecnTreeBean.setPid(standardBean.getPreCriterionClassId());
				// 标准类型
				jecnTreeBean.setTreeNodeType(JecnUtil
						.getNodeTypeByRelaType(standardBean.getStanType()));
				jecnTreeBean.setRelationId(standardBean.getRelationId());
				listStander.add(jecnTreeBean);
			}
		} catch (Exception e1) {
			log.error("ProcessAndSystemDialog is error", e1);
		}
		systemBut.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				systemButPerformed();
			}

		});
		openSystemBut.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				openSystemButPerformed();
			}

		});
		orderBut.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				orderButPerformed();
			}

		});
		openOrderBut.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				openOrderButPerformed();
			}

		});
		okBut.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				okButPerformed();
			}

		});
		cancelBut.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				cancelButPerformed();
			}

		});
		initLayout();
		// 选中制度文件 ，双击制度表格，打开制度文件
		systemTable.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent evt) {
				if (evt.getClickCount() == 2) {
					//打开制度
					systemTableMousePressed();
				}
			}
		});
		// 选中标准文件 ，双击标准表格，打开标准文件
		orderTable.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent evt) {
				if (evt.getClickCount() == 2) {
					//打开标准
					orderTableMousePressed();
				}
			}
		});

	}

	private void initLayout() {
		mainPanel.setLayout(new GridBagLayout());
		GridBagConstraints c = null;

		Insets insets = new Insets(1, 1, 1, 1);
		c = new GridBagConstraints(0, 0, 1, 1, 1.0, 1.0,
				GridBagConstraints.CENTER, GridBagConstraints.BOTH, insets, 0,
				0);
		mainPanel.add(infoPanel, c);
		infoPanel.setLayout(new GridBagLayout());
		// 流程
		c = new GridBagConstraints(0, 0, 1, 1, 1.0, 1.0,
				GridBagConstraints.CENTER, GridBagConstraints.BOTH, insets, 0,
				0);
		infoPanel.add(processPanel, c);
		processPanel.setLayout(new GridBagLayout());
		processPanel.setBorder(BorderFactory.createTitledBorder(JecnProperties
				.getValue("relatedFlow")));
		// processPanel
		c = new GridBagConstraints(0, 0, 1, 1, 1.0, 1.0,
				GridBagConstraints.CENTER, GridBagConstraints.BOTH, insets, 0,
				0);
		processPanel.add(processScrollPane, c);
		processTable = new ProcessTable();
		// 设置默认背景色
		processTable.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		processTable.getTableHeader().setUI(new JecnBasicTableHeaderUI());
		processScrollPane.setViewportView(processTable);
		// 标准
		c = new GridBagConstraints(0, 1, 1, 1, 1.0, 1.0,
				GridBagConstraints.CENTER, GridBagConstraints.BOTH, insets, 0,
				0);
		infoPanel.add(orderPanel, c);
		orderPanel.setLayout(new GridBagLayout());
		orderPanel.setBorder(BorderFactory.createTitledBorder(JecnProperties
				.getValue("relatedOrder")));
		// systemPanel
		c = new GridBagConstraints(0, 0, 1, 2, 1.0, 1.0,
				GridBagConstraints.CENTER, GridBagConstraints.BOTH, insets, 0,
				0);
		orderPanel.add(orderScrollPane, c);
		orderTable = new OrderTable(flowId);
		// 设置默认背景色
		orderTable.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		orderTable.getTableHeader().setUI(new JecnBasicTableHeaderUI());
		orderScrollPane.setViewportView(orderTable);
		c = new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
				GridBagConstraints.NORTHEAST, GridBagConstraints.NONE, insets,
				0, 0);
		orderPanel.add(orderBut, c);
		// 打开标准文件按钮
		c = new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0,
				GridBagConstraints.NORTHEAST, GridBagConstraints.NONE, insets,
				0, 0);
		orderPanel.add(openOrderBut, c);
		// 制度
		c = new GridBagConstraints(0, 2, 1, 1, 1.0, 1.0,
				GridBagConstraints.CENTER, GridBagConstraints.BOTH, insets, 0,
				0);
		infoPanel.add(systemPanel, c);
		systemPanel.setLayout(new GridBagLayout());
		systemPanel.setBorder(BorderFactory.createTitledBorder(JecnProperties
				.getValue("relatedRule")));
		// systemPanel
		c = new GridBagConstraints(0, 0, 1, 2, 1.0, 1.0,
				GridBagConstraints.CENTER, GridBagConstraints.BOTH, insets, 0,
				0);
		systemPanel.add(systemScrollPane, c);
		systemTable = new SystemTable();
		// 设置默认背景色
		systemTable.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		systemTable.getTableHeader().setUI(new JecnBasicTableHeaderUI());
		systemScrollPane.setViewportView(systemTable);
		c = new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
				GridBagConstraints.NORTHEAST, GridBagConstraints.NONE, insets,
				0, 0);
		systemPanel.add(systemBut, c);
		// 打开制度文件按钮
		c = new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0,
				GridBagConstraints.NORTHEAST, GridBagConstraints.NONE, insets,
				0, 0);
		systemPanel.add(openSystemBut, c);
		// button
		c = new GridBagConstraints(0, 1, 1, 1, 1.0, 0.0,
				GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL,
				insets, 0, 0);
		mainPanel.add(butPanel, c);
		butPanel.setLayout(new FlowLayout(FlowLayout.RIGHT));
		butPanel.add(verfyLab);
		butPanel.add(okBut);
		butPanel.add(cancelBut);

		this.getContentPane().add(mainPanel);
	}

	/**
	 * 选择
	 * 
	 */
	private void systemButPerformed() {
		RuleChooseDialog ruleChooseDialog = new RuleChooseDialog(listRule, 1L);
		ruleChooseDialog.setVisible(true);
		if (ruleChooseDialog.isOperation()) {
			Vector<Vector<String>> data = ((DefaultTableModel) systemTable
					.getModel()).getDataVector();
			if (listRule != null && listRule.size() > 0) {
				if (data != null) {
					data.clear();
					ruleIds = null;
				}
				for (JecnTreeBean o : listRule) {
					if (ruleIds == null) {
						ruleIds = String.valueOf(o.getId());
					} else {
						ruleIds = ruleIds + "," + String.valueOf(o.getId());
					}

					Vector<String> v = new Vector<String>();
					v.add(String.valueOf(o.getId()));
					v.add(o.getNumberId());
					v.add(o.getName());
					if (TreeNodeType.ruleFile.toString().equals(
							o.getTreeNodeType().toString())) {
						// 获取文件ID
						try {
							RuleT rulet = ConnectionPool.getRuleAction()
									.getRuleT(o.getId());
							v.add(String.valueOf(rulet.getFileId()));
							v.add("2");
							v.add(JecnProperties.getValue("ruleFile"));
						} catch (Exception e) {
							log.error("ProcessAndSystemDialog systemButPerformed is error", e);
						}
					} else if (TreeNodeType.ruleDir.toString().equals(
							o.getTreeNodeType().toString())) {
						v.add("");
						v.add("0");
						v.add("0");
					} else if (TreeNodeType.ruleModeFile.toString().equals(
							o.getTreeNodeType().toString())) {
						v.add("");
						v.add("1");
						v.add(JecnProperties.getValue("ruleModeFile"));
					}
					((DefaultTableModel) this.systemTable.getModel()).addRow(v);
				}
			} else {
				// 清空表格
				// data.clear();
				((DefaultTableModel) systemTable.getModel()).setRowCount(0);
				ruleIds = null;
			}
		}

	}

	/***************************************************************************
	 * 打开制度按钮
	 */
	private void openSystemButPerformed() {
		verfyLab.setText("");
		int selectRow = systemTable.getSelectedRow();
		if (selectRow == -1) {
			verfyLab.setText(JecnProperties.getValue("chooseOpenFiles"));
			return;
		}
		// 获取制度类型
		TreeNodeType nodeType = null;

		if ("1".equals(systemTable.getValueAt(selectRow, 4).toString())) {
			nodeType = TreeNodeType.ruleModeFile;
		} else if ("2".equals(systemTable.getValueAt(selectRow, 4).toString())) {
			nodeType = TreeNodeType.ruleFile;
		}
		// 打开制度（判定权限）
		JecnDesignerCommon.openRuleInfoDialog(Long.valueOf(systemTable.getValueAt(
				selectRow, 0).toString()), nodeType, verfyLab);
	}

	/***************************************************************************
	 * 打开标准文件按钮
	 */
	private void openOrderButPerformed() {
		verfyLab.setText("");
		int[] selectRows = orderTable.getSelectedRows();
		if (selectRows.length == 1) {
			int relateType = Integer.parseInt(orderTable.getValueAt(
					selectRows[0], 3).toString());
			long standardId = Long.valueOf(orderTable.getValueAt(selectRows[0],
					0).toString());
			// 打开标准
			JecnDesignerCommon.openStandard(standardId, relateType);
		} else {
			verfyLab.setText(JecnProperties.getValue("chooseOpenFiles"));
			return;
		}
	}
	/**
	 * 标准选择
	 */
	private void orderButPerformed() {
		StandardChooseDialog standardChooseDialog = new StandardChooseDialog(
				listStander, 0);
		standardChooseDialog.setVisible(true);
		if (standardChooseDialog.isOperation()) {
			Vector<Vector<String>> data = ((DefaultTableModel) orderTable
					.getModel()).getDataVector();
			if (listStander != null && listStander.size() > 0) {
				if (data != null) {
					data.clear();
					standerIds = null;
				}
				for (JecnTreeBean o : listStander) {
					if (standerIds == null) {
						standerIds = String.valueOf(o.getId());
					} else {
						standerIds = standerIds + ","
								+ String.valueOf(o.getId());
					}
					Vector<String> v = new Vector<String>();
					v.add(String.valueOf(o.getId()));
					v.add(o.getName());
					if (o.getRelationId() != null) {
						v.add(o.getRelationId().toString());
					} else {
						v.add("");
					}
					int type = JecnUtil.getRelaType(o.getTreeNodeType());
					v.add(String.valueOf(type));
					v.add(JecnUtil.getStandardTypeName(type));
					((DefaultTableModel) this.orderTable.getModel()).addRow(v);
				}
			} else {
				// 清空表格
				// data.clear();
				((DefaultTableModel) orderTable.getModel()).setRowCount(0);
				standerIds = null;
			}
		}
	}

	/***************************************************************************
	 * 制度表格双击事件 systemTableMousePressed
	 */
	private void systemTableMousePressed() {
		openSystemButPerformed();
	}

	/***************************************************************************
	 * 标准表格双击事件 orderTableMousePressed
	 */
	private void orderTableMousePressed() {
		openOrderButPerformed();
	}

	/***************************************************************************
	 * 确定
	 */
	protected void okButPerformed() {
		try {
			// 更新相关制度，相关标准
			ConnectionPool.getProcessAction().updateFlowRelateRuleT(flowId,
					ruleIds, standerIds,
					JecnConstants.loginBean.getJecnUser().getPeopleId());
			this.dispose();
		} catch (Exception e) {
			log.error("ProcessAndSystemDialog okButPerformed is error", e);
			
		}

	}

	/**
	 * 取消
	 */
	private void cancelButPerformed() {
		this.dispose();
	}

	class ProcessTable extends JTable {

		ProcessTable() {
			this.setModel(getTableModel());
			if (isSelectMutil()) {
				this.getSelectionModel().setSelectionMode(
						ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
			} else {
				this.getSelectionModel().setSelectionMode(
						ListSelectionModel.SINGLE_SELECTION);
			}
			
			//设置单元格高度
			TableColumnModel columnModel = this.getColumnModel();
			TableColumn tableColumn4 = columnModel.getColumn(2);
			tableColumn4.setMinWidth(100);
			tableColumn4.setMaxWidth(100);
			int[] cols = gethiddenCols();
			if (cols != null && cols.length > 0) {
				for (int col : cols) {
					TableColumn tableColumn = columnModel.getColumn(col);
					tableColumn.setMinWidth(0);
					tableColumn.setMaxWidth(0);
				}
			}

			// 设置默认背景色
			this.setBackground(JecnUIUtil.getDefaultBackgroundColor());
			this.getTableHeader().setReorderingAllowed(false);
		}

		public ProcessTableMode getTableModel() {
			Vector<String> title = new Vector<String>();
			title.add("id");
			title.add(JecnProperties.getValue("flowName"));
			title.add(JecnProperties.getValue("type"));
			return new ProcessTableMode(getContentFlowStrutureT(), title);
		}

		public Vector<Vector<String>> getContentFlowStrutureT() {
			Vector<Vector<String>> vs = new Vector<Vector<String>>();
			// 获取流程记录保存数据
			try {
				// listRuleT =
				// ConnectionPool.getProcessAction().getRuleTByFlowId(
				// flowId);
				for (Object[] obj : listFlow) {
					Vector<String> row = new Vector<String>();
					// 关联流程ID
					if (obj[0] != null) {
						row.add(obj[0].toString());
					}else{
						row.add("");
					}
					// 关联流程名称
					row.add(obj[1].toString());
					// 关联流程类型
					if ("1".equals(obj[2].toString())) {
						row.add(JecnProperties.getValue("topFlow"));
					} else if ("2".equals(obj[2].toString())) {
						row.add(JecnProperties.getValue("bottomFlow"));
					} else if ("3".equals(obj[2].toString())) {
						row.add(JecnProperties.getValue("interfaceFlow"));
					} else if ("4".equals(obj[2].toString())) {
						row.add(JecnProperties.getValue("sonFlow"));
					}
					vs.add(row);
				}
			} catch (Exception e) {
				log.error("ProcessAndSystemDialog ProcessTable is error", e);
				
			}

			return vs;
		}

		public boolean isSelectMutil() {
			return false;
		}

		public int[] gethiddenCols() {

			return new int[] { 0 };
		}

		class ProcessTableMode extends DefaultTableModel {
			public ProcessTableMode(Vector<Vector<String>> data,
					Vector<String> title) {
				super(data, title);

			}

			public boolean isCellEditable(int rowindex, int colindex) {
				return false;
			}

		}

	}

	class SystemTable extends JTable {

		SystemTable() {
			this.setModel(getTableModel());
			if (isSelectMutil()) {
				this.getSelectionModel().setSelectionMode(
						ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
			} else {
				this.getSelectionModel().setSelectionMode(
						ListSelectionModel.SINGLE_SELECTION);
			}
			TableColumnModel columnModel = this.getColumnModel();
			TableColumn tableColumn5 = columnModel.getColumn(5);
			tableColumn5.setMinWidth(100);
			tableColumn5.setMaxWidth(100);
			int[] cols = gethiddenCols();
			if (cols != null && cols.length > 0) {
				for (int col : cols) {
					TableColumn tableColumn = columnModel.getColumn(col);
					tableColumn.setMinWidth(0);
					tableColumn.setMaxWidth(0);
				}
			}

			// 设置默认背景色
			this.setBackground(JecnUIUtil.getDefaultBackgroundColor());
			this.getTableHeader().setReorderingAllowed(false);
		}

		public Vector<Vector<String>> getContentRuleT() {
			Vector<Vector<String>> vs = new Vector<Vector<String>>();
			// 获取流程记录保存数据
			try {
				// listRuleT =
				// ConnectionPool.getProcessAction().getRuleTByFlowId(
				// flowId);
				for (int i = 0; i < listRuleBean.size(); i++) {
					RuleT ruleT = listRuleBean.get(i);
					Vector<String> row = new Vector<String>();
					row.add(String.valueOf(ruleT.getId()));
					// 制度编号
					row.add(ruleT.getRuleNumber());
					// 制度名称
					row.add(ruleT.getRuleName());
					// 制度文件ID
					row.add(String.valueOf(ruleT.getFileId()));
					// 制度类别：制度文件，制度，制度目录
					row.add(String.valueOf(ruleT.getIsDir()));
					row.add(JecnDesignerCommon.ruleTypeVal(ruleT.getIsDir()));
					vs.add(row);
				}

			} catch (Exception e) {
				log.error("ProcessAndSystemDialog getContentRuleT is error", e);
				
			}

			return vs;
		}

		public SystemTableMode getTableModel() {
			Vector<String> title = new Vector<String>();
			title.add(JecnProperties.getValue("id"));
			title.add(JecnProperties.getValue("ruleNum"));
			title.add(JecnProperties.getValue("ruleName"));
			title.add("fileId");
			title.add(JecnProperties.getValue("type"));
			title.add(JecnProperties.getValue("type"));
			return new SystemTableMode(getContentRuleT(), title);
		}

		public boolean isSelectMutil() {
			return false;
		}

		public int[] gethiddenCols() {

			return new int[] { 0,3,4 };
		}

		class SystemTableMode extends DefaultTableModel {
			public SystemTableMode(Vector<Vector<String>> data,
					Vector<String> title) {
				super(data, title);

			}

			public boolean isCellEditable(int rowindex, int colindex) {
				return false;
			}

		}

	}

	public List<JecnTreeBean> getListRule() {
		return listRule;
	}
	public void setListRule(List<JecnTreeBean> listRule) {
		this.listRule = listRule;
	}
	public List<JecnTreeBean> getListStander() {
		return listStander;
	}
	public void setListStander(List<JecnTreeBean> listStander) {
		this.listStander = listStander;
	}
	
}
