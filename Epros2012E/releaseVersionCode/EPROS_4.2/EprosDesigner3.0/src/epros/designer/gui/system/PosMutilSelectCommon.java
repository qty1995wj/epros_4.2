package epros.designer.gui.system;

import java.awt.Insets;
import java.util.List;

import com.jecn.epros.server.bean.tree.JecnTreeBean;

import epros.designer.gui.common.JecnSelectChooseDialog;
import epros.designer.gui.popedom.position.PositionChooseDialog;
import epros.designer.util.JecnProperties;
import epros.draw.gui.swing.JecnPanel;
import epros.draw.gui.top.dialog.JecnDialog;

public class PosMutilSelectCommon extends MutilSelectCommon {

	public PosMutilSelectCommon(int rows, JecnPanel infoPanel, Insets insets, List<JecnTreeBean> list,
			JecnDialog jecnDialog, boolean isRequest) {
		super(rows, infoPanel, insets, list, JecnProperties.getValue("posCompetenceC"), jecnDialog, isRequest);
	}
	
	public PosMutilSelectCommon(int rows, JecnPanel infoPanel, Insets insets, List<JecnTreeBean> list,
			JecnDialog jecnDialog, boolean isRequest,boolean isDownload) {
		super(rows, infoPanel, insets, list, JecnProperties.getValue("posCompetenceC"), jecnDialog, isRequest,isDownload);
	}


	public PosMutilSelectCommon(int rows, JecnPanel infoPanel, Insets insets, List<JecnTreeBean> list,
			JecnDialog jecnDialog, boolean isRequest, String lableName) {
		super(rows, infoPanel, insets, list, lableName, jecnDialog, isRequest,false);
	}

	@Override
	protected JecnSelectChooseDialog getJecnSelectChooseDialog(List<JecnTreeBean> list, JecnDialog jecnDialog) {
		return new PositionChooseDialog(list, jecnDialog,true);
	}

}
