package epros.draw.gui.workflow;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;

import javax.swing.BorderFactory;
import javax.swing.JComponent;
import javax.swing.JPanel;
import javax.swing.JSplitPane;

import epros.draw.constant.JecnToolBoxConstant.MapType;
import epros.draw.data.JecnFlowMapData;
import epros.draw.event.JecnEventProcess;
import epros.draw.gui.box.JecnToolBoxMainPanel;
import epros.draw.gui.figure.unit.JecnPaintFigureUnit;
import epros.draw.gui.swing.JecnBaseFlowElementPanel;
import epros.draw.gui.swing.JecnPanel;
import epros.draw.gui.swing.JecnSplitPane;
import epros.draw.gui.top.header.JecnModuleShowPanel;
import epros.draw.gui.top.shortcut.JecnShortCutKeyProcess;
import epros.draw.gui.top.toolbar.JecnToolBarPanel;
import epros.draw.gui.top.toolbar.JecnToolbarTitlePanel;
import epros.draw.gui.workflow.popup.JecnFlowElemPopupMenu;
import epros.draw.gui.workflow.popup.JecnWorkflowPopupMenu;
import epros.draw.gui.workflow.tabbedPane.JecnDrawDesktopPane;
import epros.draw.gui.workflow.tabbedPane.JecnDrawTabbedPane;
import epros.draw.gui.workflow.tabbedPane.JecnTabbedTitleButton;
import epros.draw.gui.workflow.util.JecnSyncTollBar;
import epros.draw.gui.workflow.util.JecnWorkflowUtil;
import epros.draw.system.data.JecnSystemStaticData;
import epros.draw.util.JecnResourceUtil;
import epros.draw.util.JecnUIUtil;

/**
 * 
 * 画图工具的主界面
 * 
 * @author ZHOUXY
 * 
 */
public class JecnDrawMainPanel extends JPanel implements ActionListener, MouseListener, MouseMotionListener {

	/** 主界面实例 */
	private static JecnDrawMainPanel mainPnael = null;

	// ****************数据层 开始****************//
	// /** 存放所有流程图或流程地图的集合 */
	// private JecnMapListData listMapObj = null;
	// ****************数据层 结束****************//

	// ****************窗体区 开始****************//
	/** 窗体右上角组合框面板 */
	private JecnModuleShowPanel moduleShowPanel = null;
	// ****************窗体区 结束****************//

	// ****************工具区 开始****************//
	/** 工具栏 */
	private JecnToolBarPanel toolbar = null;
	// ****************工具区 结束****************//

	// ****************流程元素库 开始****************//
	/** 流程元素库容器 */
	private JecnToolBoxMainPanel boxPanel = null;
	// ****************流程元素库 结束****************//

	// ****************画图区 开始****************//
	/** 画板与流程元素panel的父panel:leftPanel的容器：显示流程元素库时使用 */
	private JecnSplitPane splitPane = null;

	private JecnSplitPane leftSplitPane = null;

	/** tabbedPane的容器 */
	private JPanel leftPanel = null;
	/** 元素属性面板 */
	private JecnPanel eleFigurePanel = null;
	/** drawScrollPane的容器 */
	private JecnDrawTabbedPane tabbedPane = null;
	/** 当前画图画板 */
	private JecnDrawDesktopPane workflow = null;

	// ****************画图区 结束****************//

	/** 按钮事件处理类 */
	private JecnEventProcess actionProcess = null;

	// ****************菜单****************//
	/** 流程图面板右键菜单 */
	private JecnWorkflowPopupMenu partMapWorkflowPopupMenu = null;
	/** 流程地图面板右键菜单 */
	private JecnWorkflowPopupMenu totalMapWorkflowPopupMenu = null;
	/** 组织图面板右键菜单 */
	private JecnWorkflowPopupMenu orgMapWorkflowPopupMenu = null;
	/** 集成关系图面板右键菜单 */
	private JecnWorkflowPopupMenu mapRelatedWorkflowPopupMenu = null;

	/** 流程图流程元素右键菜单 */
	private JecnFlowElemPopupMenu partMapFlowElemPopupMenu = null;
	/** 流程地图流程元素右键菜单 */
	private JecnFlowElemPopupMenu totalMapFlowElemPopupMenu = null;
	/** 组织图流程元素右键菜单 */
	private JecnFlowElemPopupMenu orgMapFlowElemPopupMenu = null;

	// ****************菜单****************//

	/** 实例化资源文件管理对象 */
	private JecnResourceUtil resourceManager = null;

	private JecnDrawMainPanel() {
	}

	/**
	 * 
	 * 获取唯一主界面
	 * 
	 */
	public static JecnDrawMainPanel getMainPanel() {
		if (mainPnael == null) {
			mainPnael = new JecnDrawMainPanel();
			// 初始化
			mainPnael.initComponents();
		}
		return mainPnael;
	}

	/**
	 * 
	 * 设置画图面板上的流程元素层级
	 * 
	 * @param flowElementPanel
	 */
	public void setFlowElementLayer(JecnBaseFlowElementPanel flowElementPanel) {
		if (this.workflow == null || flowElementPanel == null) {
			return;
		}

		// 设置层级
		this.workflow.setLayer(flowElementPanel, this.workflow.getMaxZOrderIndex());
	}

	/**
	 * 
	 * 获取当前画图面板的放大倍数
	 * 
	 * @return double 放大倍数，如果当前画图面板不存在返回1.0D
	 */
	public double getCurrWorkflowScale() {
		if (JecnDrawMainPanel.getMainPanel().getWorkflow() == null) {
			return 1.0D;
		}
		return JecnDrawMainPanel.getMainPanel().getWorkflow().getWorkflowScale();
	}

	public void setCurrWorkflowScale(double scale) {
		if (JecnDrawMainPanel.getMainPanel().getWorkflow() == null) {
			return;
		}
		JecnDrawMainPanel.getMainPanel().getWorkflow().setWorkflowScale(scale);
	}

	/**
	 * 
	 * 移除流程图或流程地图
	 * 
	 */
	public void removeWolkflow(JecnTabbedTitleButton tabbedTitleButton) {
		if (workflow == null || tabbedTitleButton == null) {
			return;
		}
		// 是-否-取消 true: OK-NO-CANCEL；false：OK-NO
		if (JecnWorkflowUtil.isSaveWorkFlow(workflow, true)) {// 是否执行面板保存处理
			return;
		}
		// 移除选项卡
		tabbedPane.removeDrawPanelFromTabble(tabbedTitleButton);

		// 没有流程图或流程地图，移除tabbedPane对象，目的背景颜色和主界面保持一致
		if (tabbedPane.getTabCount() == 0) {
			// 移除页面面板
			// leftPanel.remove(tabbedPane);

			leftPanel.remove(leftSplitPane);
			// 没有新建或打开的流程图或流程地图
			this.setWorkflow(null);

			leftPanel.repaint();
		}
	}

	/**
	 * 
	 * 添加流程图或流程地图
	 * 
	 * @param JecnFlowMapData
	 *            mapData
	 */
	public void addWolkflow(JecnFlowMapData mapData) {

		if (mapData == null) {
			return;
		}
		if (tabbedPane.getTabCount() == 0) {
			// 第一次添加图，要先把tabbedPane面板添加到leftPanel面板中
			// leftPanel.add(tabbedPane);
			leftPanel.add(leftSplitPane);
			leftPanel.updateUI();
			// 没有新建或打开的流程图或流程地图
			this.setWorkflow(null);
		}

		// 添加画图面板到JTabblePane中
		tabbedPane.addDrawPanelToTabble(mapData);

		if (tabbedPane.getTabCount() == 0) {
			// 第一次添加图，要先把tabbedPane面板添加到leftPanel面板中
			leftPanel.remove(tabbedPane);
			// 没有新建或打开的流程图或流程地图
			this.setWorkflow(null);
		}

		// if (mapData.isAuthSave() != 1) {
		JecnPaintFigureUnit.addOpenTipPanel();

		mapData.setHiddenLineEditPoint(JecnSystemStaticData.isHiddenLineEditPoint());
		// }
		// getToolbar().getEditToolBarItemPanel().getLineEditCheckBox().setSelected(mapData.isHiddenLineEditPoint());
	}

	/**
	 * 
	 * 初始化组件
	 * 
	 */
	private void initComponents() {
		// 实例化资源文件管理对象
		resourceManager = JecnResourceUtil.getJecnResourceUtil();
		// 存放所有流程图或流程地图的集合
		// listMapObj = new JecnMapListData();
		// 工具栏
		toolbar = new JecnToolBarPanel();
		// tabbedPane的容器
		leftPanel = new JPanel();

		// 元素属性容器
		eleFigurePanel = new JecnPanel();
		eleFigurePanel.setBorder(BorderFactory.createTitledBorder("元素属性"));
		// 流程元素库容器
		boxPanel = new JecnToolBoxMainPanel();

		// drawScrollPane的容器
		tabbedPane = new JecnDrawTabbedPane();
		// 按钮事件处理类
		actionProcess = new JecnEventProcess();

		// 窗体右上角组合框面板
		moduleShowPanel = new JecnModuleShowPanel();

		// 画图面板与流程元素库面板的容器
		splitPane = new JecnSplitPane(JSplitPane.HORIZONTAL_SPLIT, this, leftPanel, boxPanel);

		leftSplitPane = new JecnSplitPane(JSplitPane.VERTICAL_SPLIT, null, tabbedPane, eleFigurePanel);

		leftSplitPane.showLeftPanel();
		// 上7下3
		leftSplitPane.setResizeWeight(0.8);

		// 流程图面板右键菜单 */
		partMapWorkflowPopupMenu = new JecnWorkflowPopupMenu(MapType.partMap);
		// 流程地图面板右键菜单 */
		totalMapWorkflowPopupMenu = new JecnWorkflowPopupMenu(MapType.totalMap);
		// 组织图面板右键菜单 */
		orgMapWorkflowPopupMenu = new JecnWorkflowPopupMenu(MapType.orgMap);
		mapRelatedWorkflowPopupMenu = new JecnWorkflowPopupMenu(MapType.totalMapRelation);

		// 流程图流程元素右键菜单
		partMapFlowElemPopupMenu = new JecnFlowElemPopupMenu(MapType.partMap);
		// 流程地图流程元素右键菜单
		totalMapFlowElemPopupMenu = new JecnFlowElemPopupMenu(MapType.totalMap);
		// 组织图流程元素右键菜单
		orgMapFlowElemPopupMenu = new JecnFlowElemPopupMenu(MapType.orgMap);

		// 主界面
		// 期望大小
		this.setPreferredSize(JecnUIUtil.getEprosDrawSize());
		// 最小大小
		this.setMinimumSize(JecnUIUtil.getEprosDrawSize());
		// 背景
		this.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		// 布局管理器
		this.setLayout(new BorderLayout());

		// tabbedPane的容器
		// leftPanel.setOpaque(false);
		leftPanel.setLayout(new BorderLayout());
		leftPanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		leftPanel.setBorder(JecnUIUtil.getTootBarBorder());

		eleFigurePanel.setLayout(new BorderLayout());
		eleFigurePanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		eleFigurePanel.setBorder(JecnUIUtil.getTootBarBorder());

		// 布局
		this.add(toolbar, BorderLayout.NORTH);
		if (!JecnSystemStaticData.isEprosDesigner()) {// 不是设计器
			this.add(splitPane, BorderLayout.CENTER);
		}

		// 注册快捷键
		JecnShortCutKeyProcess.getShortCutKeyProcess().initMainPanelKeyAction();
	}

	/**
	 * 
	 * 所有按钮点击处理
	 * 
	 */
	public void actionPerformed(ActionEvent paramActionEvent) {
		if (actionProcess != null) {
			if (workflow != null && !workflow.isEditFail()) {// 正在编辑且内容不合法
				return;
			}
			actionProcess.actionPerformed(paramActionEvent);
		}
	}

	/** ********************所有鼠标事件处理 开始********************* */
	public void mouseClicked(MouseEvent e) {
	}

	public void mouseEntered(MouseEvent e) {
		if (actionProcess != null) {
			actionProcess.mouseEvent(e);
		}
	}

	public void mouseExited(MouseEvent e) {
		if (actionProcess != null) {
			actionProcess.mouseEvent(e);
		}
	}

	public void mousePressed(MouseEvent e) {
		if (workflow != null && !workflow.isEditFail()) {// 正在编辑且内容不合法
			return;
		}
		if (actionProcess != null) {
			actionProcess.mouseEvent(e);
		}
	}

	public void mouseReleased(MouseEvent e) {
		if (workflow != null && !workflow.isEditFail()) {// 正在编辑且内容不合法
			return;
		}
		if (actionProcess != null) {
			actionProcess.mouseEvent(e);
		}
	}

	public void mouseDragged(MouseEvent e) {
		if (actionProcess != null) {
			actionProcess.mouseEvent(e);
		}
	}

	public void mouseMoved(MouseEvent e) {
		if (actionProcess != null) {
			actionProcess.mouseEvent(e);
		}
	}

	/** ********************所有鼠标事件处理 结束********************* */
	public JecnToolBarPanel getToolbar() {
		return toolbar;
	}

	public JecnToolBoxMainPanel getBoxPanel() {
		return boxPanel;
	}

	/**
	 * 
	 * 获取当前画板
	 * 
	 * 但没有新建或打开的流程图或流程地图时此值为空
	 * 
	 * @return JecnDrawDesktopPane 当前画板
	 */
	public JecnDrawDesktopPane getWorkflow() {
		return this.workflow;
	}

	/**
	 * 
	 * 设置给定组件的层级，此层级为当前画图面板最大层级
	 * 
	 * @param comp
	 *            JComponent 组件
	 */
	public void setFlowElemPanelLayer(JComponent comp) {
		if (workflow == null) {
			return;
		}
		this.workflow.setFlowElemPanelLayer(comp);
	}

	/**
	 * 
	 * 中断快速添加图形功能
	 * 
	 */
	public void interruptLinkFigure() {
		if (workflow == null) {
			return;
		}
		this.workflow.interruptLinkFigure();
	}

	/**
	 * 
	 * 设置当前选中画图面板
	 * 
	 * 当前画图面板为null时报错
	 * 
	 * @param currDesktopPane
	 *            workflow 当前画图面板
	 */
	public void setWorkflow(JecnDrawDesktopPane workflow) {
		if (this.workflow != null && this.workflow == workflow) {
			return;
		}
		this.workflow = workflow;

		// 同步工具栏
		if (workflow == null) {
			// 清空面板同步处理
			JecnSyncTollBar.syncClearWorkPanel();
			// 没有当前选中画图面板时不显示
			this.toolbar.getOftenPartPanel().currWorkflowNotNullEnabledFalse();
		} else {
			this.toolbar.getOftenPartPanel().currWorkflowNotNullEnabledTrue();
		}

	}

	public JecnDrawTabbedPane getTabbedPane() {
		return tabbedPane;
	}

	public JPanel getLeftPanel() {
		return leftPanel;
	}

	public JecnModuleShowPanel getModuleShowPanel() {
		return moduleShowPanel;
	}

	public JecnSplitPane getSplitPane() {
		return splitPane;
	}

	public JecnToolbarTitlePanel getToolbarTitle() {
		return this.getToolbar().getToolbarTitle();
	}

	public JecnWorkflowPopupMenu getPartMapWorkflowPopupMenu() {
		return partMapWorkflowPopupMenu;
	}

	public JecnWorkflowPopupMenu getTotalMapWorkflowPopupMenu() {
		return totalMapWorkflowPopupMenu;
	}

	public JecnWorkflowPopupMenu getOrgMapWorkflowPopupMenu() {
		return orgMapWorkflowPopupMenu;
	}

	public JecnFlowElemPopupMenu getPartMapFlowElemPopupMenu() {
		return partMapFlowElemPopupMenu;
	}

	public JecnFlowElemPopupMenu getTotalMapFlowElemPopupMenu() {
		return totalMapFlowElemPopupMenu;
	}

	public JecnFlowElemPopupMenu getOrgMapFlowElemPopupMenu() {
		return orgMapFlowElemPopupMenu;
	}

	public JecnResourceUtil getResourceManager() {
		return resourceManager;
	}

	public JecnWorkflowPopupMenu getMapRelatedWorkflowPopupMenu() {
		return mapRelatedWorkflowPopupMenu;
	}

	/**
	 * 添加Visio导入导出功能
	 */
	public void addVisioInOutMenus() {
		partMapWorkflowPopupMenu.addVisioOption();
		totalMapWorkflowPopupMenu.addVisioOption();
		orgMapWorkflowPopupMenu.addVisioOption();
	}

	public JecnPanel getEleFigurePanel() {
		return eleFigurePanel;
	}

	public JecnSplitPane getLeftSplitPane() {
		return leftSplitPane;
	}

}
