package com.jecn.epros.server.service.tree.data;

import com.jecn.epros.bean.ByteFileResult;

public interface INodeImportService<T> {

	public ByteFileResult save();

}
