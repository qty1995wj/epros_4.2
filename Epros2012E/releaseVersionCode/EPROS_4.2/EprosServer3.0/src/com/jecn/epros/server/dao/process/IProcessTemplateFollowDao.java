package com.jecn.epros.server.dao.process;

import com.jecn.epros.server.bean.process.JecnProcessTemplateFollowBean;
import com.jecn.epros.server.common.IBaseDao;

public interface IProcessTemplateFollowDao extends IBaseDao<JecnProcessTemplateFollowBean, Long> {

}
