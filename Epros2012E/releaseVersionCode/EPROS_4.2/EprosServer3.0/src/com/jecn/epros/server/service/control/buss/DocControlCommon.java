package com.jecn.epros.server.service.control.buss;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.jecn.epros.server.bean.task.JecnTaskHistoryFollow;
import com.jecn.epros.server.bean.task.JecnTaskHistoryNew;
import com.jecn.epros.server.common.JecnContants;
import com.jecn.epros.server.common.JecnFinal;
import com.jecn.epros.server.util.JecnPath;

public class DocControlCommon {
	/** 系统默认文件路径，和webRoot同路径 测试用，可变更 */
	public static final String DOC_FILE_PATH = JecnPath.ROOT_PATH
			+ "docControFile";

	/** 流程目录路径 */
	public static final String FLOW_DIR = "flow";
	/** 流程地图目录路径 */
	public static final String FLOW_MAP_DIR = "flowMap";
	/** 制度目录路径 */
	public static final String RULE_DIR_DIR = "flowDir";
	/** 制度文件目录路径 */
	public static final String RULE_FILE_DIR = "ruleFileDir";

	/**
	 * 创建文控信息文件前验证时间文件目录是否存在，不存在创建
	 * 
	 * @return
	 */
	public static String CreateFileDir() {
		File fileDir = new File(DOC_FILE_PATH);
		if (!fileDir.exists()) {// 目录不存在
			// 创建文件目录
			fileDir.mkdir();
		}

		// 获取字符串格式时间 yyyy/MM/dd
		String dateStr = getStringbyDate(new Date());

		String dateDir = DOC_FILE_PATH + "/" + dateStr;
		// 验证日期文件格式是否存在
		File dateFileDir = new File(dateDir);
		if (!dateFileDir.exists()) {// 如果日期文件夹不存在创建
			dateFileDir.mkdirs();
		}
		return dateFileDir.getPath();
	}

	/**
	 * 
	 * 给定参数先去空再判断是否为null或空
	 * 
	 * @param str
	 * @return
	 */
	public static boolean isNullOrEmtryTrim(String str) {
		return (str == null || "".equals(str.trim())) ? true : false;
	}

	/**
	 * 
	 * 验证目录是否存在，不存在侧创建目录
	 * 
	 * @param fileDir
	 */
	public static void isFileDirExists(String fileDir) {
		if (isNullOrEmtryTrim(fileDir)) {// 目录为空 返回
			return;
		}
		File file = new File(fileDir);
		if (!file.exists()) {
			file.mkdirs();
		}
	}

	/**
	 * 日期格式转换成时间格式字符串
	 * 
	 * @param date
	 *            传入的时间格式字符串
	 * @return
	 */
	public static String getStringbyDate(Date date) {
		return new SimpleDateFormat("yyyy/MM/dd").format(date);
	}

	/**
	 * 
	 * 根据文件类型获取文件目录
	 * 
	 * @param type
	 *            文控类型 0是流程图、1是文件,2是制度模板文件,3流程地图，4 制度文件
	 * @param path
	 * @return
	 */
	public static String getDateAndFilePath(int type) {
		// 文件目录
		String filePath = "";
		if (type == 0) {// 流程
			filePath = DocControlCommon.FLOW_DIR;
		} else if (type == 2) {// 2是制度目录
			filePath = DocControlCommon.RULE_DIR_DIR;
		} else if (type == 3) {// 流程地图
			filePath = DocControlCommon.FLOW_MAP_DIR;
		} else if (type == 4) {// 4制度文件
			filePath = DocControlCommon.RULE_FILE_DIR;
		}
		// 获取日期目录
		String datePath = JecnFinal.getStringbyDate(new Date());
		// 日期+ 类型目录
		return datePath + "/" + filePath;
	}

	/**
	 * 文件存储在本地的全目录
	 * 
	 * @param filePath
	 *            存储数据库的文件路径
	 * @return String 文件相对本地的全路径
	 */
	public static String getFilePatnFull(String filePath) {
		return DOC_FILE_PATH + "/" + filePath;
	}

	// /**
	// * 把文件转化成字节
	// *
	// * @param f
	// * @return
	// */
	// public static byte[] getBytesFromFile(File f) {
	// try {
	// FileInputStream fis = new FileInputStream(f);
	// byte[] content = new byte[fis.available()];
	// fis.read(content);
	// fis.close();
	// return content;
	// } catch (IOException e) {
	// System.out.println("");
	// }
	// return null;
	// }

	/**
	 * 
	 * 复制文控信息
	 * 
	 * @param taskHistoryNew
	 *            文控信息
	 * @return JecnTaskHistoryNew
	 */
	public static JecnTaskHistoryNew getJecnTaskHistoryNew(
			JecnTaskHistoryNew taskHistoryNew) {
		JecnTaskHistoryNew newTaskHistoryNew = new JecnTaskHistoryNew();
		newTaskHistoryNew.setApproveCount(taskHistoryNew.getApproveCount());
		newTaskHistoryNew.setEndTime(taskHistoryNew.getEndTime());
		newTaskHistoryNew.setStartTime(taskHistoryNew.getStartTime());
		newTaskHistoryNew.setModifyExplain(taskHistoryNew.getModifyExplain());
		newTaskHistoryNew.setPublishDate(taskHistoryNew.getPublishDate());
		newTaskHistoryNew.setTestRunNumber(taskHistoryNew.getTestRunNumber());
		newTaskHistoryNew.setNumberId(taskHistoryNew.getNumberId());
		newTaskHistoryNew.setDraftPerson(taskHistoryNew.getDraftPerson());
		// 默认编号
		newTaskHistoryNew.setVersionId(JecnContants.theDefaultVersionNumber);

		if (taskHistoryNew.getListJecnTaskHistoryFollow().size() > 0) {// 记录文控信息从表信息
			List<JecnTaskHistoryFollow> list = new ArrayList<JecnTaskHistoryFollow>();
			for (JecnTaskHistoryFollow jecnTaskHistoryFollow : taskHistoryNew
					.getListJecnTaskHistoryFollow()) {
				JecnTaskHistoryFollow historyFollow = new JecnTaskHistoryFollow();
				historyFollow.setAppellation(jecnTaskHistoryFollow
						.getAppellation());
				historyFollow.setName(jecnTaskHistoryFollow.getName());
				historyFollow.setSort(jecnTaskHistoryFollow.getSort());
				list.add(historyFollow);
			}
			newTaskHistoryNew.setListJecnTaskHistoryFollow(list);
		}
		return newTaskHistoryNew;
	}
}
