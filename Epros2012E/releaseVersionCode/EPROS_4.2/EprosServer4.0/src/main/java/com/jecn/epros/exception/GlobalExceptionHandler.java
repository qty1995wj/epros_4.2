package com.jecn.epros.exception;

import com.jecn.epros.exception.domain.EprosBussinessException;
import com.jecn.epros.exception.domain.ErrorInfo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.NoHandlerFoundException;

import javax.servlet.http.HttpServletRequest;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.SQLException;

@ControllerAdvice
public class GlobalExceptionHandler {

    private static final Logger logger = LoggerFactory.getLogger(GlobalExceptionHandler.class);

    @ExceptionHandler(value = SQLException.class)
    public ResponseEntity<ErrorInfo> handlerSqlException(HttpServletRequest request, SQLException e) {
        return handlerException(request, e);
    }

    @ExceptionHandler(value = NoHandlerFoundException.class)
    public ResponseEntity<ErrorInfo> handlerNoHandlerFoundException(HttpServletRequest request,
                                                                    NoHandlerFoundException e) {
        ErrorInfo error = new ErrorInfo();
        error.setUrl(request.getRequestURL().toString());
        error.setStatusCode(HttpStatus.FORBIDDEN);
        error.setMsg("不存在此访问接口！");
        error.setCause(e);
        return getResponseAndLog(error);
    }

    @ExceptionHandler(value = AccessDeniedException.class)
    public ResponseEntity<ErrorInfo> handlerAccessDeniedException(HttpServletRequest request,
                                                                  AccessDeniedException e) {
        ErrorInfo error = new ErrorInfo();
        error.setUrl(request.getRequestURL().toString());
        error.setStatusCode(HttpStatus.FORBIDDEN);
        error.setMsg("没有访问此接口的权限！");
        error.setCause(e);
        return getResponseAndLog(error);
    }


    @ExceptionHandler(value = FileNotFoundException.class)
    public ResponseEntity<ErrorInfo> handlerFileNotFoundException(HttpServletRequest request, FileNotFoundException e) {
        ErrorInfo error = new ErrorInfo();
        error.setUrl(request.getRequestURL().toString());
        error.setStatusCode(HttpStatus.NOT_FOUND);
        error.setMsg("请求的资源未找到！");
        error.setCause(e);
        return getResponseAndLog(error);
    }

    @ExceptionHandler(value = IllegalArgumentException.class)
    public ResponseEntity<ErrorInfo> handlerIllegalArgumentException(HttpServletRequest request,
                                                                     IllegalArgumentException e) {
        ErrorInfo error = new ErrorInfo();
        error.setUrl(request.getRequestURL().toString());
        error.setStatusCode(HttpStatus.BAD_REQUEST);
        error.setMsg("请求的参数不合法！");
        error.setCause(e);
        return getResponseAndLog(error);
    }

    @ExceptionHandler(value = IOException.class)
    public ResponseEntity<ErrorInfo> handlerIOException(HttpServletRequest request, IOException e) {
        ErrorInfo error = new ErrorInfo();
        error.setUrl(request.getRequestURL().toString());
        error.setStatusCode(HttpStatus.INTERNAL_SERVER_ERROR);
        error.setMsg("请求失败，请稍后重试！");
        error.setCause(e);
        return getResponseAndLog(error);
    }

    @ExceptionHandler(value = EprosBussinessException.class)
    public ResponseEntity<ErrorInfo> handlerEprosBussinessException(HttpServletRequest request,
                                                                    EprosBussinessException e) {
        ErrorInfo error = new ErrorInfo();
        error.setUrl(request.getRequestURL().toString());
        error.setStatusCode(HttpStatus.INTERNAL_SERVER_ERROR);
        error.setMsg(e.getMessage());
        error.setCause(e);
        return getResponseAndLog(error);
    }

    @ExceptionHandler(value = Exception.class)
    public ResponseEntity<ErrorInfo> handlerException(HttpServletRequest request, Exception e) {
        ErrorInfo error = new ErrorInfo();
        error.setUrl(request.getRequestURL().toString());
        error.setStatusCode(HttpStatus.INTERNAL_SERVER_ERROR);
        error.setMsg("系统服务异常，请联系 API 提供者。");
        error.setCause(e);
        return getResponseAndLog(error);
    }

    private ResponseEntity<ErrorInfo> getResponseAndLog(ErrorInfo error) {
        if (error.getStatusCode() == HttpStatus.INTERNAL_SERVER_ERROR) {
            logger.error(error.getUrl(), error.getCause());
        } else {
            logger.warn(error.getUrl(), error.getCause());
        }
        return new ResponseEntity<>(error, error.getStatusCode());
    }

}
