<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>


<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	</head>

	<body>
		<div id="ruleAttribute" style="width: 98%;">
			<table width="100%;">
				<tr>
					<td style="font: bold 13px ;" colspan="2" align="left">
						<!-- 岗位基本信息 -->
						<s:text name="postBasicInformation" />
					</td>
				</tr>
				<tr>
					<td width="100px;" class="td_dashed">
						<!-- 岗位名称：-->
						<s:text name="positionNameC" />
					</td>
					<td class="td_dashed">
						&nbsp;
						<span id="posName_show"><s:property
								value="descriptionBean.posName" />
						</span>
					</td>
				</tr>

				<tr>
					<td class="td_dashed">
						<!-- 上级岗位：-->
						<s:text name="superiorPositionC" />
					</td>
					<td class="td_dashed">
						&nbsp;
						<s:iterator value="descriptionBean.upPositionList">
							<a href="orgSpecification.action?posId=<s:property value='id'/>"
								target='_blank' style="cursor: pointer;" class="table"> [<s:property
									value="name" />] </a>
						</s:iterator>
					</td>
				</tr>

				<tr>
					<td class="td_dashed">
						<!-- 下属岗位：-->
						<s:text name="subordinatePositionC" />
					</td>
					<td class="td_dashed">
						&nbsp;
						<s:iterator value="descriptionBean.lowerPositionList">
							<a href="orgSpecification.action?posId=<s:property value='id'/>"
								target='_blank' style="cursor: pointer;" class="table"> [<s:property
									value="name" />] </a>
						</s:iterator>
					</td>
				</tr>

				<tr>
					<td class="td_dashed">
						<!-- 岗位类别： -->
						<s:text name="postCategoryC" />
					</td>
					<td class="td_dashed">
						&nbsp;
						<s:property value="descriptionBean.positionsNature" escape="false" />
					</td>
				</tr>
				<tr>
					<td class="td_dashed">
						<!-- 核心价值： -->
						<s:text name="coreValue" />
					</td>
					<td class="td_dashed">
						&nbsp;
						<s:property value="descriptionBean.schoolExperience"
							escape="false" />
					</td>
				</tr>
				<tr>
					<td class="td_padding">
						<!-- 岗位说明书附件： -->
						<s:text name="accessories" />
					</td>
					<td style="word-break: break-all;" class="td_padding">
							&nbsp;
							<a target='_blank' 
								href="downloadFile.action?fileId=<s:property
									value='descriptionBean.fileId' />&isPub=false"
								class="table" style="cursor: pointer;"> <s:property
									value="descriptionBean.fileName" /> </a>
					</td>
				</tr>
			</table>
			<s:if test="#application.versionType">
				<hr style="border: 1px solid #E8E8E8;">
				<table width="100%;">
					<tr>
						<td style="font: bold 13px ;" colspan="2" align="left">
							<!-- 与本岗位相关制度 -->
							<s:text name="andThisPositionRelevantSystem" />
						</td>
					</tr>
					<tr>
						<td width="120px;" class="td_dashed">
							<!-- 部门内相关制度：-->
							<s:text name="withinTheDepartmentOfTheRelevantSystemC" />
						</td>
						<td class="td_dashed">
							&nbsp;
							<s:iterator value="descriptionBean.inListRule">
								<s:if test="relationId == 1">
									<a
										href="rule.action?type=ruleModeFile&ruleId=<s:property value='id'/>"
										style="cursor: pointer;" target='_blank' class="table"> [<s:property
											value="name" />] </a>
								</s:if>
								<s:elseif test="relationId == 2">
									<a
										href="rule.action?type=ruleFile&ruleId=<s:property value='id'/>"
										style="cursor: pointer;" target='_blank' class="table"> [<s:property
											value="name" />] </a>
								</s:elseif>
								<s:else>
									     [<s:property value="name" />]
						</s:else>
							</s:iterator>
						</td>
					</tr>
					<tr>
						<td class="td_padding">
							<!-- 部门外相关制度：-->
							<s:text name="theDepartmentOfTheRelevantSystemC" />
						</td>
						<td class="td_padding">
							&nbsp;
							<s:iterator value="descriptionBean.outListRule">
								<s:if test="relationId == 1">
									<a
										href="rule.action?type=ruleModeFile&ruleId=<s:property value='id'/>"
										style="cursor: pointer;" target='_blank' class="table"> [<s:property
											value="name" />] </a>
								</s:if>
								<s:elseif test="relationId == 2">
									<a
										href="rule.action?type=ruleFile&ruleId=<s:property value='id'/>"
										style="cursor: pointer;" target='_blank' class="table"> [<s:property
											value="name" />] </a>
								</s:elseif>
								<s:else>
									     [<s:property value="name" />]
								</s:else>
							</s:iterator>
						</td>
					</tr>
				</table>
			</s:if>
			<hr style="border: 1px solid #E8E8E8;">
			<table width="100%;">
				<tr>
					<td style="font: bold 13px ;" colspan="2" align="left">
						<!-- 任职条件 -->
						<s:text name="qualifications" />
					</td>
				</tr>
				<tr>
					<td width="100px;" class="td_dashed">
						<!-- 学历：-->
						<s:text name="degreeC" />
					</td>
					<td style="word-break: break-all;" class="td_dashed">
						<s:property value="descriptionBean.knowledge" escape="false" />
					</td>
				</tr>
				<tr>
					<td class="td_dashed">
						<!-- 知识：-->
						<s:text name="knowledgeC" />
					</td>
					<td style="word-break: break-all;" class="td_dashed">
						<s:property value="descriptionBean.positionSkill" escape="false" />
					</td>
				</tr>
				<tr>
					<td class="td_dashed">
						<!-- 技能：-->
						<s:text name="skillsC" />
					</td>
					<td style="word-break: break-all;" class="td_dashed">
						<s:property value="descriptionBean.positionImportance"
							escape="false" />
					</td>
				</tr>
				<tr>
					<td class="td_padding">
						<!-- 素质模型：-->
						<s:text name="qualityModelC" />
					</td>
					<td style="word-break: break-all;" class="td_padding">
						<s:property value="descriptionBean.positionDiathesisModel"
							escape="false" />
					</td>
				</tr>
			</table>
			<hr style="border: 1px solid #E8E8E8;">
			<table width="100%">
				<tr>
					<td style="font: bold 13px ;">
						<!-- 岗位职责 -->
						<s:text name="responsibilities" />
					</td>
				</tr>
				<tr>
					<td>
						<div id="responsibilitiesDiv" style="width: 98%;" align="center">
							<table width="98%" border="0" cellpadding="0" cellspacing="1"
								align="center" style="background-color: #BFBFBF"
								style="table-layout: fixed; ">
								<tr class="FixedTitleRow" align="center">
									<td width="2%" class="gradient">
									</td>
									<td width="15%" height="25" class="gradient"
										style="word-break: break-all;">
										<!-- 执行角色 -->
										<s:text name="executiveRole" />
									</td>
									<td width="20%" height="25" class="gradient"
										style="word-break: break-all;">
										<!-- 岗位职责内容 -->
										<s:text name="responsibilitiesContent" />
									</td>
									<td width="15%" height="25" class="gradient"
										style="word-break: break-all;">
										<!-- 流程 -->
										<s:text name="flow" />
									</td>
									<td width="15%" height="25" class="gradient"
										style="word-break: break-all;">
										<!-- 活动 -->
										<s:text name="activities" />
									</td>
									<td width="18%" height="25" class="gradient"
										style="word-break: break-all;">
										<!-- KPI -->
										KPI
									</td>
									<td width="13%" height="25" class="gradient"
										style="word-break: break-all;">
										<!-- 目标值 -->
										<s:text name="targetValue" />
									</td>
									<td width="2%" class="gradient">
									</td>
								</tr>
								<s:set id="count" value="1" />
								<s:iterator value="descriptionBean.jecnRoleProcessBean.roleList"
									status="sta">
									<tr align="center" style="background-color: #ffffff"
										onmouseover="style.backgroundColor='#EFEFEF';"
										onmouseout="style.backgroundColor='#ffffff';">
										<td align="right" class="gradient">
											<s:property value="#count" />
											<s:set id="count" value="#count+1" />
										</td>
										<td class="t5" style="word-break: break-all;" width="15%"
											<s:if test="activeList.size>0">rowspan="<s:property value='activeList.size'/>"</s:if>>
											<s:property value="roleName" />
										</td>
										<td class="t5" style="word-break: break-all;" width="20%"
											<s:if test="activeList.size>0">rowspan="<s:property value='activeList.size'/>"</s:if>>
											<s:property value="roleRes" />
										</td>
										<td class="t5" style="word-break: break-all;" width="15%"
											<s:if test="activeList.size>0">rowspan="<s:property value='activeList.size'/>"</s:if>>
											<a
												href="process.action?type=process&flowId=<s:property value='flowId'/>"
												target='_blank' style="cursor: pointer;" class="table">
												<s:property value="flowName" /> </a>
										</td>
										<s:if test="activeList.size>0">
											<s:iterator value="activeList" status="st">
												<s:if test="#st.index==0">
													<td class="t5" style="word-break: break-all;" width="15%">
														<a
															href="processActive.action?activeId=<s:property value='activeFigureId'/>"
															target='_blank' style="cursor: pointer;" class="table">
															<s:property value="activeName" /> <br> </a>
													</td>
													<td class="t5" style="word-break: break-all;" width="18%">
														<s:iterator value="indicatorsList">
															<s:property value="indicatorName" />
															<br>
														</s:iterator>
													</td>
													<td class="t5" style="word-break: break-all;" width="13%">
														<s:iterator value="indicatorsList">
															<s:property value="indicatorValue" />
															<br>
														</s:iterator>
													</td>
												</s:if>
											</s:iterator>
										</s:if>
										<s:else>
											<td class="t5" style="word-break: break-all;" width="15%">
											</td>
											<td class="t5" style="word-break: break-all;" width="18%">
											</td>
											<td class="t5" style="word-break: break-all;" width="13%">
											</td>
										</s:else>
										<td class="t5" style="word-break: break-all;" width="2%">
										</td>
									</tr>
									<s:iterator value="activeList" status="st">
										<s:if test="#st.index!=0">
											<tr align="center" style="background-color: #ffffff"
												onmouseover="style.background='#EFEFEF';"
												onmouseout="style.background='#ffffff';">
												<td align="right" class="gradient">
													<s:property value="#count" />
													<s:set id="count" value="#count+1" />
												</td>
												<td class="t5" style="word-break: break-all;" width="15%">
													<a
														href="processActive.action?activeId=<s:property value='activeFigureId'/>"
														target='_blank' style="cursor: pointer;" class="table">
														<s:property value="activeName" /> <br> </a>
												</td>
												<td class="t5" style="word-break: break-all;" width="18%">
													<s:iterator value="indicatorsList">
														<s:property value="indicatorName" />
														<br>
													</s:iterator>
												</td>
												<td class="t5" style="word-break: break-all;" width="13%">
													<s:iterator value="indicatorsList">
														<s:property value="indicatorValue" />
														<br>
													</s:iterator>
												</td>
												<td class="t5" style="word-break: break-all;" width="2%">
												</td>
											</tr>
										</s:if>
									</s:iterator>
								</s:iterator>
							</table>
						</div>
					</td>
				</tr>
			</table>
		</div>
	</body>
</html>