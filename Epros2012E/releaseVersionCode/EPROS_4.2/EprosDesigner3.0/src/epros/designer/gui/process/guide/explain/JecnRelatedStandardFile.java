package epros.designer.gui.process.guide.explain;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.table.DefaultTableModel;

import org.apache.log4j.Logger;

import com.jecn.epros.server.bean.tree.JecnTreeBean;

import epros.designer.gui.file.FileChooseDialog;
import epros.designer.gui.system.fileDescription.JecnFileDescriptionTable;
import epros.designer.table.JecnTableModel;
import epros.designer.util.ConnectionPool;
import epros.designer.util.JecnProperties;
import epros.designer.util.JecnUtil;
import epros.draw.gui.swing.JecnPanel;

/**
 * 流程标准化文件
 * 
 * @author user
 * 
 */
public class JecnRelatedStandardFile extends JecnFileDescriptionTable {

	private Logger log = Logger.getLogger(JecnRelatedStandardFile.class);
	protected Long flowId = null;
	// 文件选择按钮初始化
	protected JButton selectBut = new JButton(JecnProperties.getValue("select"));
	// 流程标准化文件
	protected List<JecnTreeBean> listStrandFiles = new ArrayList<JecnTreeBean>();
	protected List<Long> listIds = new ArrayList<Long>();

	public JecnRelatedStandardFile(int index, String name, boolean isRequest, JecnPanel contentPanel, Long flowId,
			String tip) {
		super(index, name, isRequest, contentPanel, tip);
		this.flowId = flowId;
		titlePanel.add(selectBut);
		try {
			listStrandFiles = ConnectionPool.getProcessAction().getStandardizedFiles(flowId);
		} catch (Exception e) {
			log.error("JecnRelatedStandardFile is error", e);
		}
		selectBut.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				select();
			}
		});
		initTable();
		table.setTableColumn(1, 180);
	}

	protected void select() {
		FileChooseDialog chooseDialog = new FileChooseDialog(listStrandFiles);
		chooseDialog.setVisible(true);
		if (chooseDialog.isOperation()) {// 操作
			Vector<Vector<String>> data = ((JecnTableModel) getTable().getModel()).getDataVector();
			if (listStrandFiles != null && listStrandFiles.size() > 0) {
				if (data != null) {
					data.clear();
				}
				for (JecnTreeBean jecnTreeBean : listStrandFiles) {
					((DefaultTableModel) table.getModel()).addRow(this.getRowData(jecnTreeBean));
				}
			} else {
				// 清空表格
				((DefaultTableModel) table.getModel()).setRowCount(0);
			}
		}
	}

	@Override
	protected void dbClickMethod() {
		select();
	}

	@Override
	protected JecnTableModel getTableModel() {
		Vector<String> title = new Vector<String>();
		title.add("id");
		title.add(JecnProperties.getValue("fileNum"));
		title.add(JecnProperties.getValue("fileName"));
		return new JecnTableModel(title, getContent());
	}

	private Vector<Vector<String>> getContent() {
		// 相关标准
		Vector<Vector<String>> orderContent = new Vector<Vector<String>>();
		for (JecnTreeBean jecnTreeBean : listStrandFiles) {
			listIds.add(jecnTreeBean.getId());
			orderContent.add(getRowData(jecnTreeBean));
		}
		return orderContent;
	}

	private Vector<String> getRowData(JecnTreeBean jecnTreeBean) {
		Vector<String> vector = new Vector<String>();
		vector.add(String.valueOf(jecnTreeBean.getId()));
		vector.add(jecnTreeBean.getNumberId());
		vector.add(jecnTreeBean.getName());
		return vector;
	}

	@Override
	protected int[] gethiddenCols() {
		return new int[] { 0 };

	}

	@Override
	protected boolean isSelectMutil() {
		return false;
	}

	@Override
	public boolean isUpdate() {
		return JecnUtil.isChangeListLong(getResultListLong(), listIds);
	}

	@Override
	public boolean isValidateNotPass(JLabel verfyLab) {
		return false;
	}

}
