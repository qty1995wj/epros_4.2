package com.jecn.epros.domain.inventory;

/**
 * 内控指引知识库
 *
 * @author user
 */
public class RiskInventiryInfoBean {

    //JecnControlGuide JECN_RISK_GUIDE
    private Long infoId;
    /**
     * 名称
     */
    private String name;
    /**
     * 详细描述
     */
    private String description;

    public Long getInfoId() {
        return infoId;
    }

    public void setInfoId(Long infoId) {
        this.infoId = infoId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
