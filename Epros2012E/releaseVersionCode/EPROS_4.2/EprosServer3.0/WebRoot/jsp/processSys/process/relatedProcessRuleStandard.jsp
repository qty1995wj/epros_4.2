<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%
	response.setHeader("Pragma", "No-cache");
	response.setHeader("Cache-Control", "no-cache");
%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<script type="text/javascript">
document.body.onresize = function() {
	setShowTaskProcessTemplate('relatedProcessRuleStandardId');
}
</script>
	</head>

	<body>
		<div id="relatedProcessRuleStandardId" style="overflow-y: scroll;" align="center">
			<div id="process"
				style="margin-top: 5px; margin-left: 15px; margin-right: 15px; margin-bottom: 15px;">
				<table width="98%">
					<tr>
						<td
							style="font: bold 15px ; padding-left: 12px; padding-top: 8px; padding-bottom: 5px;">
							<s:text name="relatedProcess" />
						</td>
					</tr>
				</table>
				<table width="98%" border="0" cellpadding="0" cellspacing="1"
					align="center" bgcolor="#D0D0D0" style="table-layout: fixed;">
					<tr class="FixedTitleRow" align="center">
						<td width="2%" class="gradient">
						</td>
						<td width="20%" height="25" class="gradient"
							style="word-break: break-all;">
							<!-- 流程名称 -->
							<s:text name="processName" />
						</td>
						<td width="8%" height="25" class="gradient"
							style="word-break: break-all;">
							<!-- 流程编号 -->
							<s:text name="processNumber" />
						</td>
						<td width="20%" height="25" class="gradient"
							style="word-break: break-all;">
							<!-- 责任人 -->
							<s:text name="responsiblePersons" />
						</td>
						<td width="20%" height="25" class="gradient"
							style="word-break: break-all;">
							<!-- 责任部门 -->
							<s:text name="responsibilityDepartment" />
						</td>
						<td width="20%" height="25" class="gradient"
							style="word-break: break-all;">
							<!-- 发布日期 -->
							<s:text name="releaseDate" />
						</td>
						<td width="8%" height="25" class="gradient"
							style="word-break: break-all;">
							<!-- 类型 -->
							<s:text name="type" />
						</td>
						<td width="2%" class="gradient">
						</td>
					</tr>
					<s:iterator value="relatedProcessRuleStandard.relatedProcessList"
						status="st">
						<tr align="center" bgcolor="#ffffff"
							onmouseover="style.backgroundColor='#EFEFEF';"
							onmouseout="style.backgroundColor='#ffffff';" height="20">
							<td align="right" class="gradient">
								<s:property value="#st.index+1" />
							</td>
							<td class="t5" style="word-break: break-all;">
							    <s:if test="flowId==''">
							   		 <s:property value="flowName" />
							    </s:if><s:else>
							   		 <a href="process.action?type=process&flowId=<s:property value='flowId'/>&isPub=${isPub}"
										target='_blank' style="cursor: pointer;" class="table"> <s:property value="flowName" /> </a>
							    </s:else>
							</td>
							<td class="t5" style="word-break: break-all;">
								<s:property value="flowIdInput" />
							</td>
							<td class="t5" style="word-break: break-all;">
								<s:property value="resPeopleName" />
							</td>
							<td class="t5" style="word-break: break-all;">
								<a href="organization.action?orgId=<s:property value='orgId'/>"
									target='_blank' style="cursor: pointer;" class="table"> <s:property
										value="orgName" /> </a>
							</td>
							<td class="t5" style="word-break: break-all;">
								<s:property value="pubDate" />
							</td>
							<td class="t5" style="word-break: break-all;">
								<s:if test="flowType==1">
									<s:text name="upstreamProcess" />
								</s:if>
								<s:elseif test="flowType==2">
									<s:text name="downstreamProcess" />
								</s:elseif>
								<s:elseif test="flowType==3">
									<s:text name="processFlow" />
								</s:elseif>
								<s:elseif test="flowType==4">
									<s:text name="childProcess" />
								</s:elseif>
							</td>
							<td></td>
						</tr>
					</s:iterator>
				</table>
			</div>

			<!--  相关标准 -->
			<s:if test="#application.isHiddenStandard==0">
				<div id="relatedProcess"
					style="margin-top: 10px; margin-left: 15px; margin-right: 15px; margin-bottom: 15px;">
					<table width="98%">
						<tr>
							<td
								style="font: bold 15px ; padding-left: 12px; padding-top: 8px; padding-bottom: 5px;">
								<s:text name="relatedStandard" />
							</td>
						</tr>
					</table>
					<table width="98%" border="0" cellpadding="0" cellspacing="1"
						align="center" bgcolor="#D0D0D0" style="table-layout: fixed;">
						<tr class="FixedTitleRow" align="center">
							<td width="2%" class="gradient">
							</td>
							<td bgcolor="#DDDDDD" width="48%" height="25" class="gradient"
								style="word-break: break-all;">
								<!-- 标准名称 -->
								<s:text name="standardName" />
	
							</td>
							<td bgcolor="#DDDDDD" width="48%" height="25" class="gradient"
								style="word-break: break-all;">
								<!-- 类型 -->
								<s:text name="type" />
							</td>
							<td width="2%" class="gradient">
							</td>
						</tr>
						<s:iterator value="relatedProcessRuleStandard.relatedStandardList"
							status="st">
							<tr align="center" bgcolor="#ffffff"
								onmouseover="style.backgroundColor='#EFEFEF';"
								onmouseout="style.backgroundColor='#ffffff';" height="20">
								<td align="right" class="gradient">
									<s:property value="#st.index+1" />
								</td>
								<td class="t5" style="word-break: break-all;">
									<s:if test="type == 0">
										<!-- 目录 -->
										<s:property value="name" />
									</s:if>
										<a
											href="${basePath}standard.action?from=process&standardType=<s:property value='type'/>&reqType=public&standardId=<s:property value='id'/>&fileId=<s:property value='fileId'/>"
											style="cursor: pointer;" target='_blank' class="table"> <s:property
												value="name" /> </a>
	
								</td>
								<td class="t5" style="word-break: break-all;">
									<s:if test="type == 0">
										<!-- 目录 -->
										<s:text name="directory" />
									</s:if>
									<s:elseif test="type == 1">
										<!-- 文件 -->
										<s:text name="file" />
									</s:elseif>
									<s:elseif test="type == 2">
										<!-- 流程图 -->
										<s:text name="process" />
									</s:elseif>
									<s:elseif test="type == 3">
										<!-- 流程地图 -->
										<s:text name="processMap" />
									</s:elseif>
									<s:elseif test="type == 4">
										<!-- 标准条款 -->
										<s:text name="standarClause" />
									</s:elseif>
									<s:elseif test="type == 5">
										<!-- 条款要求 -->
										<s:text name="clauseRequest" />
									</s:elseif>
								</td>
								<td></td>
							</tr>
						</s:iterator>
					</table>
				</div>
			</s:if>
			<!--  相关制度 -->
			<s:if test="#application.isHiddenSystem==0">
				<div id="rule"
					style="margin-top: 10px; margin-left: 15px; margin-right: 15px; margin-bottom: 15px;">
					<table width="98%">
						<tr>
							<td
								style="font: bold 15px ; padding-left: 12px; padding-top: 8px; padding-bottom: 5px;">
								<s:text name="relatedRule" />
							</td>
						</tr>
					</table>
					<table width="98%" border="0" cellpadding="0" cellspacing="1"
						align="center" bgcolor="#BFBFBF" style="table-layout: fixed;">
						<tr class="FixedTitleRow" align="center">
							<td width="2%" class="gradient">
							</td>
							<td width="26%" height="25" class="gradient"
								style="word-break: break-all;">
								<!-- 制度名称 -->
								<s:text name="ruleName" />
							</td>
							<td width="20%" height="25" class="gradient"
								style="word-break: break-all;">
								<!-- 制度编号 -->
								<s:text name="ruleNumber" />
	
							</td>
							<td width="25%" height="25" class="gradient"
								style="word-break: break-all;">
								<!-- 责任部门 -->
								<s:text name="responsibilityDepartment" />
							</td>
							<s:if test="#request.ruleTypeIsShow">
							<td width="25%" height="25" class="gradient"
								style="word-break: break-all;">
								<!-- 制度类别 -->
								<s:text name="ruleType" />
							</td>
							</s:if>
							<td width="2%" class="gradient">
							</td>
						</tr>
						<s:iterator value="relatedProcessRuleStandard.relatedRuleList"
							status="st">
							<tr align="center" bgcolor="#ffffff"
								onmouseover="style.backgroundColor='#EFEFEF';"
								onmouseout="style.backgroundColor='#ffffff';" height="20">
								<td align="right" class="gradient">
									<s:property value="#st.index+1" />
								</td>
								<td class="t5" style="word-break: break-all;">
									<s:if test="isDir == 1">
										<a
											href="rule.action?from=process&type=ruleModeFile&reqType=public&ruleId=<s:property value='ruleId'/>&isPub=${isPub}"
											style="cursor: pointer;" target='_blank' class="table"> <s:property
												value="ruleName" /> </a>
										<br>
									</s:if>
									<s:elseif test="isDir == 2">
									    <a
											href="${basePath}rule.action?from=process&type=ruleFile&reqType=public&ruleId=<s:property value='ruleId'/>&isPub=${isPub}"
											style="cursor: pointer;" target='_blank' class="table">
											   <s:property value="ruleName" /> </a>
										<br>
									</s:elseif>
									<s:else>
										<s:property value="ruleName" />
										<br>
									</s:else>
								</td>
								<td class="t5" style="word-break: break-all;">
									<s:property value="ruleNum" />
								</td>
								<td class="t5" style="word-break: break-all;">
									<a
										href="organization.action?orgId=<s:property value='dutyOrgId'/>"
										target='_blank' style="cursor: pointer;" class="table"> <s:property
											value="dutyOrg" /> </a>
								</td>
								<s:if test="#request.ruleTypeIsShow">
								<td class="t5" style="word-break: break-all;">
									<s:property value="ruleTypeName" />
								</td>
								</s:if>
								<td></td>
							</tr>
						</s:iterator>
					</table>
				</div>
			</s:if>
			
			<!-- 相关风险 -->
			<s:if test="#application.isHiddenRisk==0">
				<div id="risk"
					style="margin-top: 10px; margin-left: 15px; margin-right: 15px; margin-bottom: 15px;">
					<table width="98%">
						<tr>
							<td
								style="font: bold 15px ; padding-left: 12px; padding-top: 8px; padding-bottom: 5px;">
								<s:text name="ruleRelateRisk" />
							</td>
						</tr>
					</table>
					<table width="98%" border="0" cellpadding="0" cellspacing="1"
						align="center" bgcolor="#D0D0D0" style="table-layout: fixed;">
						<tr class="FixedTitleRow" align="center">
							<td width="2%" class="gradient">
							</td>
							<td bgcolor="#DDDDDD" width="48%" height="25" class="gradient"
								style="word-break: break-all;">
								<!-- 风险编号 -->
								<s:text name="riskNum" />
	
							</td>
							<td bgcolor="#DDDDDD" width="48%" height="25" class="gradient"
								style="word-break: break-all;">
								<!-- 风险描述 -->
								<s:text name="riskName" />
							</td>
							<td width="2%" class="gradient">
							</td>
						</tr>
						<s:iterator value="relatedProcessRuleStandard.relatedRiskList"
							status="st">
							<tr align="center" bgcolor="#ffffff"
								onmouseover="style.backgroundColor='#EFEFEF';"
								onmouseout="style.backgroundColor='#ffffff';" height="20">
								<td align="right" class="gradient">
									<s:property value="#st.index+1" />
								</td>
								<td class="t5" style="word-break: break-all;">
									<a href="${basePath}getRiskById.action?riskDetailId=<s:property value='id'/>&reqType=public"
											target='_blank' style="cursor: pointer;" class="table">
										<s:property value="riskCode" />
									</a>
								</td>
								<td class="t5" style="word-break: break-all;">
									<s:property value="riskName" />
								</td>
								<td></td>
							</tr>
						</s:iterator>
					</table>
				</div>
			</s:if>	
		</div>
	</body>
</html>