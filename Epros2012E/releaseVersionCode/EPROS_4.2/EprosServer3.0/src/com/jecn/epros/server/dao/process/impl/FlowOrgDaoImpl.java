package com.jecn.epros.server.dao.process.impl;

import java.util.List;
import java.util.Set;

import com.jecn.epros.server.bean.popedom.JecnUser;
import com.jecn.epros.server.bean.process.FlowOrgT;
import com.jecn.epros.server.common.AbsBaseDao;
import com.jecn.epros.server.common.JecnCommonSql;
import com.jecn.epros.server.dao.process.IFlowOrgDao;
import com.jecn.epros.server.util.JecnUtil;

public class FlowOrgDaoImpl extends AbsBaseDao<FlowOrgT, Long> implements IFlowOrgDao {

	@Override
	public List<JecnUser> listOrgUsers(Set<Long> orgs) {
		String ids = JecnUtil.concatIds(orgs);
		String sql = "select ju.* from jecn_user ju"
				+ " inner join jecn_user_position_related jupr on jupr.people_id=ju.people_id"
				+ " inner join jecn_flow_org_image jfoi on jfoi.figure_id=jupr.figure_id" + " inner join ("
				+ " select c.*" + "  from jecn_flow_org p" + "  left join jecn_flow_org c"
				+ "    on c.t_path like p.t_path " + JecnCommonSql.getConcatChar() + " '%'" + " where p.org_id in "
				+ ids + " )jfo on jfo.org_id=jfoi.org_id";
		return (List<JecnUser>) this.listNativeAddEntity(sql, JecnUser.class);
	}

}
