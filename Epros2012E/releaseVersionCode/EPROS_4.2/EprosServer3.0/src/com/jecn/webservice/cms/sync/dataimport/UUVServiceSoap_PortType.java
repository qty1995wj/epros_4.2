/**
 * UUVServiceSoap_PortType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.jecn.webservice.cms.sync.dataimport;

public interface UUVServiceSoap_PortType extends java.rmi.Remote {

    /**
     * 查询请求用户有权限访问的组织列表
     */
    public java.lang.String getAllOrganizations(java.lang.String IASName, java.lang.String requestUser) throws java.rmi.RemoteException;

    /**
     * 查询指定组织(包括子组织)下的用户
     */
    public java.lang.String getUserListByOrgID(java.lang.String IASName, java.lang.String requestUser, java.lang.String orgID) throws java.rmi.RemoteException;

    /**
     * 查询指定组织(不包括子组织)下的用户
     */
    public java.lang.String getCompactUserListByOrgID(java.lang.String IASName, java.lang.String requestUser, java.lang.String orgID) throws java.rmi.RemoteException;

    /**
     * 查询指定组织(不包括子组织,包括离司用户)下的用户
     */
    public java.lang.String getCompactUserListByOrgIDAndUpdateDate(java.lang.String IASName, java.lang.String requestUser, java.lang.String orgID, java.lang.String lastUpdateDate) throws java.rmi.RemoteException;

    /**
     * 查询指定用户属性信息
     */
    public java.lang.String getUserInfoByUserAccount(java.lang.String IASName, java.lang.String requestUser, java.lang.String queryUser) throws java.rmi.RemoteException;

    /**
     * 查询指定用户及所属部门相关信息
     */
    public java.lang.String getUserInfoAllByUserAccount(java.lang.String IASName, java.lang.String requestUser, java.lang.String queryUser) throws java.rmi.RemoteException;

    /**
     * 查询指定用户属性信息
     */
    public java.lang.String getUserInfoByUserId(java.lang.String IASName, java.lang.String requestUser, java.lang.String queryUserId) throws java.rmi.RemoteException;

    /**
     * 获取指定用户的头像
     */
    public byte[] getUserImageByUserAccount(java.lang.String IASName, java.lang.String requestUser, java.lang.String queryUser) throws java.rmi.RemoteException;

    /**
     * 获取指定用户的头像
     */
    public byte[] getUserImageByUserId(java.lang.String IASName, java.lang.String requestUser, java.lang.String queryUserId) throws java.rmi.RemoteException;

    /**
     * 获取个人通讯录的自创建组列表
     */
    public java.lang.String getCustomAddressGroupByUserAccount(java.lang.String IASName, java.lang.String requestUser) throws java.rmi.RemoteException;

    /**
     * 获取个人通讯录的人员列表
     */
    public java.lang.String getCustomAddressByUserAccountAndGroupId(java.lang.String IASName, java.lang.String requestUser, java.lang.String groupId) throws java.rmi.RemoteException;

    /**
     * 获取个人通讯录的人员详情
     */
    public java.lang.String getCustomAddressDetailByUserId(java.lang.String IASName, java.lang.String requestUser, java.lang.String extUserId) throws java.rmi.RemoteException;

    /**
     * 添加个人通讯录联系人（可批量）
     */
    public java.lang.String importCustomAddressBook(java.lang.String IASName, java.lang.String requestUser, java.lang.String batchXML) throws java.rmi.RemoteException;

    /**
     * 添加个人通讯录联系人（可批量）
     */
    public java.lang.String importCustomAddressBookToGroup(java.lang.String IASName, java.lang.String requestUser, java.lang.String groupName, java.lang.String batchXML) throws java.rmi.RemoteException;

    /**
     * 通讯录人员查询
     */
    public java.lang.String searchAddressBookByKeyword(java.lang.String IASName, java.lang.String requestUser, java.lang.String keyword) throws java.rmi.RemoteException;

    /**
     * 个人信息设置
     */
    public java.lang.String userPersonalInfoSetting(java.lang.String IASName, java.lang.String requestUser, java.lang.String dataXML) throws java.rmi.RemoteException;

    /**
     * 用户头像设置
     */
    public java.lang.String userImageSetting(java.lang.String IASName, java.lang.String requestUser, byte[] imageByte) throws java.rmi.RemoteException;

    /**
     * 通讯录选人、选部门
     */
    public java.lang.String departmentCustomSelect(java.lang.String IASName, java.lang.String requestUser, java.lang.String filter) throws java.rmi.RemoteException;

    /**
     * 根据ID（用户ID,部门ID,群组ID,可以传入多个ID以逗号组合）获取用户
     */
    public java.lang.String getAllUserAccountByID(java.lang.String IASName, java.lang.String recipient) throws java.rmi.RemoteException;

    /**
     * 验证用户密码是否正确
     */
    public java.lang.String userValidate(java.lang.String IASName, java.lang.String userAcccount, java.lang.String passWord) throws java.rmi.RemoteException;

    /**
     * 验证用户密码是否正确，密码RSA加密
     */
    public java.lang.String userValidateRSA(java.lang.String IASName, java.lang.String userAcccount, java.lang.String passWord) throws java.rmi.RemoteException;

    /**
     * 获取用户短信验证码 返回值XML
     */
    public java.lang.String getMsgValidateCode(java.lang.String IASName, java.lang.String userAcccount, java.lang.String IPAddress) throws java.rmi.RemoteException;

    /**
     * 获取用户短信验证码 返回值XML
     */
    public java.lang.String getMsgYHTValidateCode(java.lang.String IASName, java.lang.String userAcccount) throws java.rmi.RemoteException;

    /**
     * 通过手机号码获取短信验证码 返回值XML
     */
    public java.lang.String getSMSValidateCodeByMobile(java.lang.String IASName, java.lang.String mobilePhoneNo) throws java.rmi.RemoteException;

    /**
     * 获取通讯录最新联系人
     */
    public java.lang.String getLastContactsByAccount(java.lang.String IASName, java.lang.String requestUser) throws java.rmi.RemoteException;

    /**
     * 获取邮件最新联系人,LastDate：大于时间段的数据，Count:获取数据条数
     */
    public java.lang.String getContactsMailByAccount(java.lang.String IASName, java.lang.String requestUser, java.lang.String lastDate, int count) throws java.rmi.RemoteException;

    /**
     * 查询指定用户属性信息
     */
    public java.lang.String getUserInfoByAccount(java.lang.String IASName, java.lang.String requestUser, java.lang.String queryUser) throws java.rmi.RemoteException;

    /**
     * 判断IP是否为内网IP
     */
    public java.lang.String isIntranetIPAddress(java.lang.String IASName, java.lang.String IPAddress) throws java.rmi.RemoteException;
}
