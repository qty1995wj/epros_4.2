package epros.draw.data.line;

import java.awt.Dimension;
import java.awt.Point;

import epros.draw.constant.JecnLogConstants;
import epros.draw.data.JecnXYData;
import epros.draw.util.DrawCommon;

/**
 * 连接线文本编辑框数据对象
 * 
 * @author Administrator
 * @date： 日期：Aug 8, 2012 时间：1:46:13 PM
 */
public class JecnLineTextData {
	/** 矩形左上角坐标对象(位置点) */
	private JecnXYData leftUpXY = new JecnXYData();
	/** 图片宽度 */
	private double width = 0.0D;
	/** 图片高度 */
	private double height = 0.0D;

	/**
	 * 
	 * 重置FigureCloneable对象的属性,位置点，大小都改变
	 * 
	 * @param localPoint
	 *            图片的位置点
	 * @param size
	 *            图片的大小
	 */
	public void reSetAttributes(Point localPoint, Dimension size,
			double multiple) {
		if (localPoint == null || size == null) {// 正常逻辑下不走此分支
			JecnLogConstants.LOG_JecnFigureDataCloneable
					.error("JecnFigureDataProcess类reSetAttributes方法：参数不能为null. localPoint="
							+ localPoint + ";size=" + size);
			return;
		}
		initCreateOriginalArributs(localPoint, size, multiple);
	}

	/**
	 * 
	 * 获取图片对应数据类克隆放大缩小对象
	 * 
	 * @param zooMmultiple
	 *            double 放大缩小倍数
	 * @return JecnLineTextData 放大缩小后的对象，此对象是克隆出来的对象
	 */
	public JecnLineTextData getZoomLineTextData(double zoomMultiple) {
		// 克隆此对象
		JecnLineTextData lineTextData = this.clone();
		// 位置点
		lineTextData.leftUpXY.zoomXYCloneable(zoomMultiple);

		// 大小
		lineTextData.width = lineTextData.width * zoomMultiple;
		lineTextData.height = lineTextData.height * zoomMultiple;

		return lineTextData;
	}

	/**
	 * 
	 * 创建初始化原始数据,根据放大缩小倍数换算原始状态数据
	 * 
	 * @param localPoint
	 *            Point 位置点
	 * @param size
	 *            Dimension 大小
	 * @param multiple
	 *            double 放大缩小倍数
	 */
	private void initCreateOriginalArributs(Point localPoint, Dimension size,
			double multiple) {
		// 获取原始位置点
		Point localOriginalPoint = getOriginalXY(localPoint, multiple);

		double originalWidth = size.getWidth() / multiple;

		double originalHeight = size.getHeight() / multiple;
		initOriginalArributs(localOriginalPoint.getX(), localOriginalPoint
				.getY(), originalWidth, originalHeight);
	}

	/**
	 * 
	 * 根据图形位置点和大小初始化所有相应的点的原始数据，根据放大缩小倍数换算原始状态数据
	 * 
	 * @param x
	 *            double X坐标
	 * @param y
	 *            double Y坐标
	 * @param width
	 *            double 宽
	 * @param height
	 *            double 高
	 */
	private void initOriginalArributs(double x, double y, double width,
			double height) {

		// 位置点
		this.leftUpXY.setX(x);
		this.leftUpXY.setY(y);

		// 宽度
		this.width = width;
		// 高度
		this.height = height;

	}

	/**
	 * 
	 * 获取原始位置点坐标
	 * 
	 * @param localPoint
	 *            Point 位置点
	 * @param multiple
	 *            double 放大缩小倍数
	 * @return Point 给定位置点对应的原始位置点
	 */
	private Point getOriginalXY(Point localPoint, double multiple) {
		Point point = new Point();
		point.setLocation(localPoint.getX() / multiple, localPoint.getY()
				/ multiple);
		return point;
	}

	/**
	 * 
	 * 获取原始大小下位置点
	 * 
	 * @return
	 */
	public JecnXYData getLeftUpXY() {
		return leftUpXY;
	}

	/**
	 * 
	 * 获取位置点X坐标
	 * 
	 * @return int 位置点X坐标
	 */
	public int getX() {

		return DrawCommon.convertDoubleToInt(this.leftUpXY.getX());
	}

	/**
	 * 
	 * 获取位置点Y坐标
	 * 
	 * @return int 位置点Y坐标
	 */
	public int getY() {
		return DrawCommon.convertDoubleToInt(this.leftUpXY.getY());
	}

	/**
	 * 
	 * 获取宽度
	 * 
	 * @return
	 */
	public int getWidth() {
		return DrawCommon.convertDoubleToInt(width);
	}

	/**
	 * 
	 * 获取高度
	 * 
	 * @return
	 */
	public int getHeight() {
		return DrawCommon.convertDoubleToInt(height);
	}

	/**
	 * 克隆对象
	 */
	public JecnLineTextData clone() {
		JecnLineTextData lineTextData = new JecnLineTextData();
		// 位置点
		lineTextData.leftUpXY = this.leftUpXY.clone();
		// 图形高度和宽度
		lineTextData.width = this.width;
		lineTextData.height = this.height;
		return lineTextData;
	}
}
