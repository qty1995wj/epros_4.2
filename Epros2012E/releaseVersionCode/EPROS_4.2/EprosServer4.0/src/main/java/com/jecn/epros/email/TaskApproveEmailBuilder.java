package com.jecn.epros.email;

import com.jecn.epros.domain.JecnUser;
import com.jecn.epros.domain.email.EmailBasicInfo;
import com.jecn.epros.domain.task.TaskBean;

public class TaskApproveEmailBuilder extends DynamicContentEmailBuilder {

    @Override
    protected EmailBasicInfo getEmailBasicInfo(JecnUser user) {

        Object[] data = getData();
        TaskBean taskBeanNew = (TaskBean) data[0];
        int toPeopleType = Integer.valueOf(data[1].toString());

        return createEmail(user, taskBeanNew, toPeopleType);
    }

    /**
     * 任务审批过程中发送邮件
     *
     * @param peopleId     审批或者拟稿人id
     * @param taskBeanNew  任务主表
     * @param toPeopleType 0、为审批人员 1、为拟稿人
     */
    public EmailBasicInfo createEmail(JecnUser user,
                                      TaskBean taskBeanNew, int toPeopleType) {

        // 邮件内容
        String taskContent = ToolEmail.getTaskDesc(taskBeanNew);
        EmailContentBuilder builder = new EmailContentBuilder(getTip());
        builder.setContent(taskContent);
        builder.setResourceUrl(getUrl(toPeopleType, taskBeanNew.getTaskId(),
                user.getId()));
        EmailBasicInfo basicInfo = new EmailBasicInfo();
        basicInfo.setSubject(getSubject(toPeopleType, taskBeanNew));
        basicInfo.setContent(builder.buildContent());
        basicInfo.addRecipients(user);

        return basicInfo;

    }

    private String getSubject(int toPeopleType, TaskBean taskBeanNew) {
        String subject = "";
        if (0 == toPeopleType) {
            // 审批人 邮件标题 任务xxx已提交，请审批！
            subject = ToolEmail.getTaskSubject(taskBeanNew);
        } else if (1 == toPeopleType) {
            // 拟稿人 邮件标题 xxx正在审批
            subject = ToolEmail.getTaskSubjectForPeopleMake(taskBeanNew);
        }
        return subject;
    }

    private String getUrl(int toPeopleType, long taskId, long toPeopleId) {

        // 获取任务审批 指定下一个操作人的连接地址
        String httpUrl = "";
        if (toPeopleType == 0) {
            httpUrl = ToolEmail.getTaskHttpUrl(taskId, toPeopleId);
        } else if (toPeopleType == 1) {
            httpUrl = ToolEmail.getTestRunHttpUrl();
        }

        return httpUrl;

    }

}
