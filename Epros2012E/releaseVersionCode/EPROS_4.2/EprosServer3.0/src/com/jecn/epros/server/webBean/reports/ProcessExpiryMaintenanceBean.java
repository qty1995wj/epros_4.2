package com.jecn.epros.server.webBean.reports;

import java.util.ArrayList;
import java.util.List;

public class ProcessExpiryMaintenanceBean {

	/** 流程有效期维护list */
	private List<ProcessExpiryMaintenanceWebBean> expiryMaintenanceWebs = new ArrayList<ProcessExpiryMaintenanceWebBean>();
	/** 当前的数据是否为错误数据的集合 */
	private boolean isError = false;
	/** 流程监护人是否显示 */
	private boolean guardianPeopleIsShow = false;
	/** 流程拟制人是否显示 */
	private boolean artificialPersonIsShow = false;
	private int type = 0;

	public boolean isGuardianPeopleIsShow() {
		return guardianPeopleIsShow;
	}

	public void setGuardianPeopleIsShow(boolean guardianPeopleIsShow) {
		this.guardianPeopleIsShow = guardianPeopleIsShow;
	}

	public boolean isArtificialPersonIsShow() {
		return artificialPersonIsShow;
	}

	public void setArtificialPersonIsShow(boolean artificialPersonIsShow) {
		this.artificialPersonIsShow = artificialPersonIsShow;
	}

	public List<ProcessExpiryMaintenanceWebBean> getExpiryMaintenanceWebs() {
		return expiryMaintenanceWebs;
	}

	public void setExpiryMaintenanceWebs(List<ProcessExpiryMaintenanceWebBean> expiryMaintenanceWebs) {
		this.expiryMaintenanceWebs = expiryMaintenanceWebs;
	}

	public boolean isError() {
		return isError;
	}

	public void setError(boolean isError) {
		this.isError = isError;
	}

	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}
	

}
