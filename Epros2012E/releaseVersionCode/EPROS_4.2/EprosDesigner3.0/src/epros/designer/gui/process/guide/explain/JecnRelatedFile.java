package epros.designer.gui.process.guide.explain;

import java.util.List;
import java.util.Vector;

import javax.swing.JLabel;

import com.jecn.epros.server.bean.download.ProcessDownloadBaseBean;
import com.jecn.epros.server.bean.file.FileWebBean;
import com.jecn.epros.server.bean.process.JecnFlowBasicInfoT;

import epros.designer.gui.process.guide.ProcessOperationDialog;
import epros.designer.gui.system.fileDescription.JecnFileDescriptionTable;
import epros.designer.table.JecnTableModel;
import epros.designer.util.ConnectionPool;
import epros.designer.util.JecnProperties;
import epros.designer.util.JecnUtil;
import epros.draw.gui.swing.JecnPanel;

/**
 * 流程相关文件 包含输入，输出的表单，操作说明
 * 
 * @author hyl
 * 
 */
public class JecnRelatedFile extends JecnFileDescriptionTable {

	private ProcessOperationDialog processOperationDialog;
	private JecnFlowBasicInfoT flowBasicInfoT;

	public JecnRelatedFile(int index, String name, boolean isRequest, JecnPanel contentPanel,
			ProcessOperationDialog processOperationDialog, String tip, JecnFlowBasicInfoT flowBasicInfoT) {
		super(index, name, isRequest, contentPanel, tip);
		this.processOperationDialog = processOperationDialog;
		this.flowBasicInfoT = flowBasicInfoT;
		initTable();
		table.setTableColumn(1, 180);
	}

	@Override
	protected void dbClickMethod() {

	}

	@Override
	protected JecnTableModel getTableModel() {
		Vector<String> title = new Vector<String>();
		title.add(JecnProperties.getValue("id"));
		title.add(JecnProperties.getValue("fileNum"));
		title.add(JecnProperties.getValue("fileName"));
		return new JecnTableModel(title, getContent());
	}

	private Vector<Vector<String>> getContent() {
		Vector<Vector<String>> content = new Vector<Vector<String>>();
		try {
			// 为了数据顺序一致性只能再次查询数据库
			ProcessDownloadBaseBean downloadBean = ConnectionPool.getProcessAction().getProcessDownloadBean(
					flowBasicInfoT.getFlowId(), false, "a35");
			List<FileWebBean> relatedFiles = downloadBean.getRelatedFiles();
			for (FileWebBean bean : relatedFiles) {
				Vector<String> vs = new Vector<String>();
				vs.add("0");
				vs.add(JecnUtil.nullToEmpty(bean.getFileNum()));
				vs.add(JecnUtil.nullToEmpty(bean.getFileName()));
				content.add(vs);
			}
		} catch (Exception e) {
			throw new RuntimeException("", e);
		}
		// 去重，显示的文件不能重复
		// 由于页面可以取到相关文件所以没有从数据库取值，加快显示速度
		// Set<Long> ids = new HashSet<Long>();
		// List<JecnActivityShowBean> activitys =
		// processOperationDialog.getFlowOperationBean().getListAllActivityShow();
		// for (JecnActivityShowBean activity : activitys) {
		// JecnActiveData activeData =
		// activity.getActiveFigure().getFlowElementData();
		// List<JecnActivityFileT> activeFiles =
		// activeData.getListJecnActivityFileT();
		// for (JecnActivityFileT activeFile : activeFiles) {
		// if (!ids.contains(activeFile.getFileSId())) {
		// content.add(createLineData(activeFile.getFileSId(),
		// activeFile.getFileName(), activeFile
		// .getFileNumber()));
		// ids.add(activeFile.getFileSId());
		// }
		// }
		//
		// List<JecnModeFileT> modeFiles = activeData.getListModeFileT();
		// for (JecnModeFileT modeFile : modeFiles) {
		// if (!ids.contains(modeFile.getFileMId())) {
		// content
		// .add(createLineData(modeFile.getFileMId(), modeFile.getModeName(),
		// modeFile.getFileNumber()));
		// ids.add(modeFile.getFileMId());
		// }
		// }
		// }

		return content;
	}

	private Vector<String> createLineData(Long fileId, String fileName, String fileNumber) {
		Vector<String> content = new Vector<String>();
		// ID
		content.add(String.valueOf(fileId));
		// 编号
		content.add(fileNumber);
		// 名称
		content.add(fileName);
		return content;
	}

	@Override
	protected int[] gethiddenCols() {
		return new int[] { 0 };
	}

	@Override
	protected boolean isSelectMutil() {
		return false;
	}

	@Override
	public boolean isUpdate() {
		return false;
	}

	@Override
	public boolean isValidateNotPass(JLabel verfyLab) {
		return false;
	}

}
