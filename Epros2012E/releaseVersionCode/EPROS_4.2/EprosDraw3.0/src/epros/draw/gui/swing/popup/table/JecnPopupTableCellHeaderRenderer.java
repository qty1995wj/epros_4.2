package epros.draw.gui.swing.popup.table;

import java.awt.Color;
import java.awt.Component;
import java.awt.GradientPaint;
import java.awt.Graphics;
import java.awt.Graphics2D;

import javax.swing.JTable;
import javax.swing.SwingConstants;
import javax.swing.border.Border;

import sun.swing.table.DefaultTableCellHeaderRenderer;
import epros.draw.util.JecnUIUtil;

/**
 * 
 * 渲染器
 * 
 * @author ZHOUXY
 * 
 */

class JecnPopupTableCellHeaderRenderer extends DefaultTableCellHeaderRenderer {
	//********* 选中渐变颜色 *********//
	private final Color topNoColor = new Color(237, 237, 237);
	private final Color buttomNoColor = new Color(207, 207, 207);
	//********* 选中渐变颜色 *********//

	public JecnPopupTableCellHeaderRenderer() {
		this.setOpaque(false);
		this.setBorder(null);
		this.setHorizontalAlignment(SwingConstants.CENTER);
		this.setForeground(Color.BLACK);
	}

	public Component getTableCellRendererComponent(JTable table, Object value,
			boolean isSelected, boolean hasFocus, int row, int column) {
		super.getTableCellRendererComponent(table, value, isSelected, hasFocus,
				row, column);
		return this;
	}

	/**
	 * 
	 * 绘制组件
	 * 
	 */
	public void paint(Graphics g) {

		Graphics2D g2d = (Graphics2D) g;

		int w = this.getWidth();
		int h = this.getHeight();

		// 渐变
		g2d.setPaint(new GradientPaint(0, 0, topNoColor, 0, h / 2,
				buttomNoColor));
		g2d.fillRect(0, 0, w, h);

		// 边框颜色
		g2d.setColor(JecnUIUtil.getDialogBorderColor());
		// 右边框线
		g2d.drawLine(this.getWidth() - 1, 0, this.getWidth() - 1, this
				.getHeight());

		super.paint(g);
	}

	public void setBorder(Border paramBorder) {
	}
}