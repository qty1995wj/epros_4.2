package com.jecn.epros.server.emailnew;

public class EmailResource {

	private Long id;
	private String name;
	/**
	 * 0 流程 1文件 2制度
	 */
	private Integer type;
	/**
	 * 按照文件类型细分 如流程分为流程和架构 制度文件分为制度文件制度模板文件等
	 * 
	 * 0)流程文件  1)文件  	2)制度模板  3)制度文件  4)流程地图
	 */
	private Integer subType;

	private EmailResource(Long id, String name, Integer type) {
		this.id = id;
		this.name = name;
		this.type = type;
	}

	public EmailResource(Long id, String name, Integer type, Integer subType) {
		this(id, name, type);
		this.subType = subType;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	public Integer getSubType() {
		return subType;
	}

	public void setSubType(Integer subType) {
		this.subType = subType;
	}

}
