package epros.designer.gui.rule;

import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.jecn.epros.server.bean.tree.JecnTreeBean;
import com.jecn.epros.server.util.JecnTreeEnumConstant.TreeNodeType;

import epros.designer.gui.common.JecnConstants;
import epros.designer.tree.JecnRoutineTree;
import epros.designer.tree.JecnTreeCommon;
import epros.designer.tree.JecnTreeModel;
import epros.designer.tree.JecnTreeNode;
import epros.designer.util.ConnectionPool;
import epros.designer.util.JecnProperties;

public class RoutineRuleMoveTree extends JecnRoutineTree {
	private Logger log = Logger.getLogger(RoutineRuleMoveTree.class);

	public RoutineRuleMoveTree(List<Long> listIds) {
		super(listIds);
	}
	@Override
	public JecnTreeModel getTreeModel() {
		List<JecnTreeBean> list = new ArrayList<JecnTreeBean>();
		try {
			list = ConnectionPool.getRuleAction().getAllRuleDir(JecnConstants.projectId);
		} catch (Exception e) {
			log.error("RoutineRuleMoveTree getTreeModel  is error", e);
		}
		
		// 根节点 "制度"
		JecnTreeNode rootNode = JecnTreeCommon.createTreeRoot(
				TreeNodeType.ruleRoot,JecnProperties.getValue("rule"));
		//
		JecnTreeCommon.addNLevelMoveNodes(list, rootNode, this.getListIds());
		return new JecnTreeModel(rootNode);
	}

	@Override
	public boolean isSelectMutil() {
		return false;
	}

	@Override
	public void jTreeMousePressed(MouseEvent evt) {
	}

}
