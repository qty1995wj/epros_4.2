package com.jecn.epros.server.dao.process;

import com.jecn.epros.server.bean.process.JecnProcessTemplateBean;
import com.jecn.epros.server.common.IBaseDao;

public interface IProcessTemplateDao extends IBaseDao<JecnProcessTemplateBean, Long>{

}
