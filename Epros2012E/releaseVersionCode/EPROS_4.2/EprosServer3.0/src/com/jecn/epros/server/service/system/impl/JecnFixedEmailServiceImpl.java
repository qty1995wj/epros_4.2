package com.jecn.epros.server.service.system.impl;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.transaction.annotation.Transactional;

import com.jecn.epros.server.bean.system.JecnFixedEmail;
import com.jecn.epros.server.bean.tree.JecnTreeBean;
import com.jecn.epros.server.common.JecnCommonSql;
import com.jecn.epros.server.dao.system.IJecnFixedEmailDao;
import com.jecn.epros.server.service.system.IJecnFixedEmailService;
import com.jecn.epros.server.util.JecnTreeEnumConstant.TreeNodeType;
/***
 * 发布 固定人员处理
 * 2014-04-09
 *
 */
@Transactional(rollbackFor = Exception.class)
public class JecnFixedEmailServiceImpl implements IJecnFixedEmailService{
	private final Log log = LogFactory.getLog(JecnFixedEmailServiceImpl.class);

	/** Dao层对象 */
	private IJecnFixedEmailDao fixedEmailDao = null;

	public IJecnFixedEmailDao getFixedEmailDao() {
		return fixedEmailDao;
	}

	public void setFixedEmailDao(IJecnFixedEmailDao fixedEmailDao) {
		this.fixedEmailDao = fixedEmailDao;
	}

	@Override
	public void AddFixedEmail(List<JecnFixedEmail> fixedEmailList) throws Exception {
		for(JecnFixedEmail fixedEmail : fixedEmailList){
			this.fixedEmailDao.save(fixedEmail);
		}
	}

	@Override
	public void deleteFixedEmail(List<String> fixedEmailIds) throws Exception {
		String sql = "delete from JECN_FIXED_EMAIL where ID in "+JecnCommonSql.getStrs(fixedEmailIds);
		this.fixedEmailDao.execteNative(sql);
	}

	@Override
	public List<JecnFixedEmail> getFixedEmailList() throws Exception {
		List<Object[]> listObject = new ArrayList<Object[]>(); 
		String sql = "select fe.*,ju.true_name from jecn_fixed_email fe,jecn_user ju where fe.person_id = ju.people_id";
		listObject =  this.fixedEmailDao.listNativeSql(sql);
		
		return getJecnFixedEmailByObject(listObject);
	}
	private List<JecnFixedEmail> getJecnFixedEmailByObject(List<Object[]> list) {
		List<JecnFixedEmail> listResult = new ArrayList<JecnFixedEmail>();
		for (Object[] obj : list) {
			if (obj == null || obj[0] == null || obj[1] == null
					|| obj[2] == null) {
				continue;
			}
			JecnFixedEmail jecnFixedEmail = new JecnFixedEmail();
			jecnFixedEmail.setId(obj[0].toString());
			jecnFixedEmail.setPersonId(Long.valueOf(obj[1].toString()));
			jecnFixedEmail.setType(Integer.parseInt(obj[2].toString()));
			jecnFixedEmail.setPersonName(obj[3].toString());
			listResult.add(jecnFixedEmail);
		}
		return listResult;
	}
	
}
