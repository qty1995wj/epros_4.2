package epros.designer.gui.define;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.ImageIcon;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;

import org.apache.log4j.Logger;

import com.jecn.epros.server.bean.tree.JecnTreeBean;
import com.jecn.epros.server.util.JecnTreeEnumConstant.TreeNodeType;

import epros.designer.gui.common.JecnConstants;
import epros.designer.gui.common.JecnDesignerCommon;
import epros.designer.tree.JecnTree;
import epros.designer.tree.JecnTreeCommon;
import epros.designer.tree.JecnTreeNode;
import epros.designer.util.ConnectionPool;
import epros.designer.util.JecnProperties;
import epros.draw.gui.top.optionpane.JecnOptionPane;
import epros.draw.system.data.JecnSystemStaticData;

public class JecnTermDefineTreeMenu extends JPopupMenu {
	private static Logger log = Logger.getLogger(JecnTermDefineTreeMenu.class);
	/** 创建目录 */
	private JMenuItem createDir;
	/** 重命名 */
	private JMenuItem reName;
	/** 新建术语 */
	private JMenuItem createTermDefine;
	/** 编辑术语 */
	private JMenuItem editTermDefine;
	/** 刷新 */
	private JMenuItem refresh;
	/** 删除 */
	private JMenuItem deleteMenu;
	/** 节点移动 */
	private JMenuItem treeMove;
	/** 节点排序 */
	private JMenuItem treeSort;

	/**
	 * 是否单选
	 */
	private boolean isSingleSelect;

	/***
	 * 单选节点
	 */
	private JecnTreeNode selectNode;

	private List<JecnTreeNode> selectNodes;

	private TreeNodeType treeNodeType;

	private JecnTree jTree = null;

	public JecnTermDefineTreeMenu(JecnTree jTree, JecnTreeNode selectNode) {
		this.jTree = jTree;
		isSingleSelect = true;
		this.selectNode = selectNode;
		selectNodes = new ArrayList<JecnTreeNode>();
		selectNodes.add(selectNode);
		treeNodeType = selectNode.getJecnTreeBean().getTreeNodeType();
		initCompotents();
		singleSelectInitMenu();
		isAuthButton();
	}

	public JecnTermDefineTreeMenu(JecnTree jTree, List<JecnTreeNode> selectNodes, TreeNodeType treeNodeType) {
		this.jTree = jTree;
		this.selectNodes = selectNodes;
		this.treeNodeType = treeNodeType;
		initCompotents();
		mutilSelectInitMenu();
		isAuthButton();
	}

	private void initCompotents() {
		// 创建目录
		createDir = new JMenuItem(JecnProperties.getValue("createDir"),
				new ImageIcon("images/menuImage/newRuleDir.gif"));
		// 重命名
		reName = new JMenuItem(JecnProperties.getValue("rename"), new ImageIcon("images/menuImage/rename.gif"));
		// 新建术语
		createTermDefine = new JMenuItem(JecnProperties.getValue("addTermDefine"), new ImageIcon(
				"images/menuImage/termDefine.gif"));
		// 编辑术语
		editTermDefine = new JMenuItem(JecnProperties.getValue("editTermDefine"), new ImageIcon(
				"images/menuImage/termDefine.gif"));
		// 刷新
		refresh = new JMenuItem(JecnProperties.getValue("refresh"), new ImageIcon("images/menuImage/refresh.gif"));
		// 删除
		deleteMenu = new JMenuItem(JecnProperties.getValue("delete"), new ImageIcon("images/menuImage/delete.gif"));
		// 节点移动
		treeMove = new JMenuItem(JecnProperties.getValue("nodeMove"), new ImageIcon("images/menuImage/nodeMove.gif"));

		// 节点排序
		treeSort = new JMenuItem(JecnProperties.getValue("nodeSort"), new ImageIcon("images/menuImage/nodeSort.gif"));
		// 创建目录
		createDir.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				createDirPerformed();
			}
		});
		// 重命名
		reName.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				reNamePerformed();
			}
		});
		// 编辑术语
		editTermDefine.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				editTermDefinePerformed();
			}
		});
		// 新建术语
		createTermDefine.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				createTermDefinePerformed();
			}
		});
		// 刷新
		refresh.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				refreshDefinePerformed();
			}
		});
		// 删除
		deleteMenu.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				deleteMenuPerformed();
			}
		});

		// 移动
		treeMove.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				treeMovePerformed();
			}
		});

		// 排序
		treeSort.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				treeSortPerformed();
			}
		});
	}

	/**
	 * 单选菜单
	 */
	private void singleSelectInitMenu() {
		switch (treeNodeType) {
		case termDefineRoot:
			this.add(createDir);
			this.add(refresh);
			this.add(treeSort);
			break;
		case termDefineDir:
			this.add(createDir);
			this.add(reName);
			this.add(createTermDefine);
			this.add(refresh);
			this.add(deleteMenu);
			this.add(treeMove);
			this.add(treeSort);
			break;
		case termDefine:
			this.add(editTermDefine);
			this.add(refresh);
			this.add(deleteMenu);
			this.add(treeMove);
		}
	}

	private void isAuthButton() {
		int authType = JecnTreeCommon.isAuthNode(selectNodes.get(0));
		if (selectNodes.get(0).getJecnTreeBean().getTreeNodeType() == TreeNodeType.termDefineDir
				|| selectNodes.get(0).getJecnTreeBean().getTreeNodeType() == TreeNodeType.termDefine) {
			switch (authType) {
			case 0:
				this.remove(createDir);
				this.remove(treeSort);
				this.remove(createTermDefine);
				this.remove(editTermDefine);
			case 1:
				this.remove(treeMove);
				this.remove(deleteMenu);
				this.remove(reName);
				break;
			default:
				break;
			}
		}
		// 根目录下只有管理员有创建目录权限 与制度相同
		if (selectNodes.get(0).getJecnTreeBean().getTreeNodeType() == TreeNodeType.termDefineRoot) {
			if (!JecnDesignerCommon.isAdmin()) {
				this.remove(createDir);
				this.remove(treeSort);
			}
		}

	}

	private void mutilSelectInitMenu() {
		this.add(deleteMenu);
		this.add(treeMove);
		this.add(refresh);
	}

	protected void treeSortPerformed() {
		if (selectNode != null) {
			JecnTermDefineSortDialog jecnTermDefineSortDialog = new JecnTermDefineSortDialog(selectNode, jTree);
			jecnTermDefineSortDialog.setVisible(true);
		}
	}

	protected void treeMovePerformed() {
		if (!JecnDesignerCommon.validateRepeatNodesName(selectNodes)) {
			return;
		}
		JecnTermDefineMoveDailog jecnTermDefineMoveDailog = new JecnTermDefineMoveDailog(selectNodes, jTree);
		jecnTermDefineMoveDailog.setVisible(true);

	}

	protected void deleteMenuPerformed() {
		if (selectNodes == null || selectNodes.size() == 0) {
			return;
		}
		// 提示是否删除
		int option = JecnOptionPane.showConfirmDialog(JecnSystemStaticData.getFrame(), JecnProperties
				.getValue("isDelete"), null, JecnOptionPane.YES_NO_OPTION, JecnOptionPane.INFORMATION_MESSAGE);
		if (option == JecnOptionPane.NO_OPTION) {
			return;
		}
		List<Long> listIds = new ArrayList<Long>();
		for (JecnTreeNode node : selectNodes) {
			listIds.add(node.getJecnTreeBean().getId());
		}
		// 删除节点
		try {
			ConnectionPool.getTermDefinitionAction().delete(listIds, JecnConstants.getUserId());
		} catch (Exception e) {
			log.error(JecnProperties.getValue("deleteTermDefineError"), e);
		}
		/** 删除节点 */
		JecnTreeCommon.removeNodes(jTree, selectNodes);
	}

	protected void refreshDefinePerformed() {
		if (selectNode == null) {
			return;
		}
		Long id = selectNode.getJecnTreeBean().getId();
		try {
			List<JecnTreeBean> list = ConnectionPool.getTermDefinitionAction().getChilds(id);
			JecnTreeCommon.refreshNode(selectNode, list, jTree);
		} catch (Exception ex) {
			log.error(JecnProperties.getValue("termDefineNodeRefrashError"), ex);
		}
	}

	protected void editTermDefinePerformed() {
		if (selectNode != null) {
			EditTermDefineDialog editTermDefineDialog = new EditTermDefineDialog(selectNode, jTree);
			editTermDefineDialog.setVisible(true);
		}
	}

	protected void createTermDefinePerformed() {
		if (selectNode != null) {
			AddTermDefineDialog addTermDefineDailog = new AddTermDefineDialog(selectNode, jTree);
			addTermDefineDailog.setVisible(true);
		}
	}

	protected void reNamePerformed() {
		if (selectNode != null) {
			EditTermDefineDirDailog editTermDefineDirDailog = new EditTermDefineDirDailog(selectNode, jTree);
			editTermDefineDirDailog.setVisible(true);
		}
	}

	protected void createDirPerformed() {
		if (selectNode != null) {
			AddTermDefineDirDailog addTermDefineDirDailog = new AddTermDefineDirDailog(selectNode, jTree);
			addTermDefineDirDailog.setVisible(true);
		}
	}
}
