// 天
var day = "\u5929";
// '周'
var week = "\u5468";
// '月'
var month = "\u6708";
// '季度'
var season = "\u5b63\u5ea6";
// '年'
var year = "\u5e74";
function selectDate(isStart) {
	var kpiType = document.getElementById("kpiHorType").value;
	var dateFmt;
	if (day == kpiType) {
		dateFmt = null;
	} else if (week == kpiType) {
		dateFmt = {
			dateFmt : "yyyy"
		};
	} else if (month == kpiType) {
		dateFmt = {
			dateFmt : "yyyy-MM"
		};
	} else if (season == kpiType) {
		dateFmt = {
			dateFmt : "yyyy"
		};
	} else if (year == kpiType) {
		dateFmt = {
			dateFmt : "yyyy"
		};
	}
	if (dateFmt == null) {
		WdatePicker();
	} else {
		WdatePicker(dateFmt);
	}
	if (week == kpiType) {// 选择的是周
		addWeekSelect(isStart);
	} else if (season == kpiType) {// 季度
		addSeasonSelect(isStart);
	}
}
/**
 * 动态添加季度
 * 
 */
function addSeasonSelect(isStart) {
	var seasonValue = 4;
	var seasonObj;
	if (isStart) {
		seasonObj = document.getElementById("startKpiSeason");
	} else {
		seasonObj = document.getElementById("endKpiSeason");
	}
	// 清除所有
	seasonObj.options.length = 0;
	for ( var i = 1; i <= seasonValue; i++) {// 动态添加季度
		seasonObj.options.add(new Option(i, i));
	}
}
/**
 * 动态添加周（select） isStart boolean True为开始时间
 */
function addWeekSelect(isStart) {
	// 当前选中的时间
	var date;
	// 周的select对象
	var weekObj;
	if (isStart) {
		date = document.getElementById("startTime").value;
		weekObj = document.getElementById("startKpiWeek");
	} else {
		date = document.getElementById("endTime").value;
		weekObj = document.getElementById("endKpiWeek");
	}
	// 当前时间的周
	var weekValue = getNumOfWeeks(date);
	// 清除所有
	weekObj.options.length = 0;
	for ( var i = 1; i <= weekValue; i++) {// 动态添加周
		if (i < 10) {
			i = '0' + i;
		}
		weekObj.options.add(new Option(i, i));
	}
}
/**
 * KPI类型选择
 * 
 */
function kpiNameChange() {
	var obj = document.getElementById("kpiType");
	// 序号，取当前选中选项的序号
	var index = obj.selectedIndex;
	var val = obj.options[index].value;
	document.getElementById("kpiHorType").value = val;
	// 计算时间
	var curDate = new Date();
	var strYear = curDate.getFullYear() - 1;
	var strDay = curDate.getDate();
	var strMonth = curDate.getMonth() + 1;
	if (strMonth < 10) {
		strMonth = "0" + strMonth;
	}
	if (strDay < 10) {
		strDay = "0" + strDay;
	}
	var preDate = strYear + "-" + strMonth + "-" + strDay;
	// 重新填充开始时间和结束时间
	document.getElementById("startTime").value = preDate;
	document.getElementById("endTime").value = curDate.format('Y-m-d');
	document.getElementById("flow_kpi").style.display = "none";
	// if (day == val) {
	// document.getElementById("startKpiWeek").style.display = "none";
	// document.getElementById("startKpiSeason").style.display = "none";
	// document.getElementById("endKpiWeek").style.display = "none";
	// document.getElementById("endKpiSeason").style.display = "none";
	// } else if (week == val) {
	// document.getElementById("startKpiWeek").style.display = '';
	// document.getElementById("startKpiSeason").style.display = "none";
	// document.getElementById("endKpiWeek").style.display = "";
	// document.getElementById("endKpiSeason").style.display = "none";
	// } else if (month == val) {
	// document.getElementById("startKpiWeek").style.display = "none";
	// document.getElementById("startKpiSeason").style.display = "none";
	// document.getElementById("endKpiWeek").style.display = "none";
	// document.getElementById("endKpiSeason").style.display = "none";
	// } else if (season == val) {
	// document.getElementById("startKpiWeek").style.display = "none";
	// document.getElementById("startKpiSeason").style.display = '';
	// document.getElementById("endKpiWeek").style.display = "none";
	// document.getElementById("endKpiSeason").style.display = "";
	// } else if (year == val) {
	// document.getElementById("startKpiWeek").style.display = "none";
	// document.getElementById("startKpiSeason").style.display = "none";
	// document.getElementById("endKpiWeek").style.display = "none";
	// document.getElementById("endKpiSeason").style.display = "none";
	// }
}
/**
 * 计算一年存在多少周 算法按元旦后第一个星期日才算第一周计算，一年只有52或53周
 */
function getNumOfWeeks(year) {
	var d = new Date(year, 0, 1);
	// 瑞年存在365天
	var yt = ((year % 4 == 0 && year % 100 != 0) || year % 400 == 0) ? 366
			: 365;
	return Math.ceil((yt - d.getDay()) / 7);
}
function searchKPI() {
	var flowId = document.getElementById("flowId").value;
	var obj = document.getElementById("kpiType");
	// 序号，取当前选中选项的序号
	var index = obj.selectedIndex;
	if (index == 0) {// 没有选择KPI类型
		alert(i_pleaseSelectToViewKpis);
		return;
	}
	// 获取文本（选中的KPI名称）
	var selectKpiName = obj.options[index].text;

	var kpiNameIdObj = document.getElementById("kpiNameId");
	// KPI名称ID
	var kpiNameId = kpiNameIdObj.options[index].text;
	// KPI类型 0 月份 1季度
	var kpiHorType = document.getElementById("kpiHorType").value;
	// 开始时间
	var startTime = document.getElementById("startTime").value;
	// 结束时间
	var endTime = document.getElementById("endTime").value;
	if (startTime == null || startTime == "" || endTime == null
			|| endTime == "") {
		// "提示" + "搜索时间不能为空！"
		Ext.MessageBox.alert(i_tip, i_KPISearchNotNull);
		return;
	}
	if (startTime > endTime) {// 结束时间大于开始时间
		// "开始时间应小于结束时间！"
		Ext.Msg.alert(i_tip, i_startTimeIsLessEndTime);
		return;
	}

	var m = spaceMonths(startTime, endTime);
	if (m > 60)// 超过五年
	{
		Ext.Msg.alert(i_tip, i_intervalTimeCannotBigThan + 5 + i_year);
		return;
	}

	document.getElementById("kpiInputOutPut").style.visibility = "hidden";

	$("#kpiTableId").empty();
	// 初始化列表
	processKpiGrid();

	var store = Ext.getCmp('kpiGridPanel').store;
	store.load( {
		params : {
			"searchKpiBean.kpiName" : selectKpiName,
			"searchKpiBean.startTime" : startTime,
			"searchKpiBean.endTime" : endTime,
			"searchKpiBean.flowId" : flowId,
			"searchKpiBean.kpiId" : kpiNameId,
			"searchKpiBean.kpiHorType" : kpiHorType
		},
		callback : function(r, options, success) {

			if (success) {
				var pngUrl = store.reader.jsonData.imageUrl;
				// 从store中获得图片的url
		$("#chart").attr("src", basePath + pngUrl + "?" + new Date());
	} else if (!success) {
		$("#chart").attr("src", "");

	}
}

	});

	// 设置是否table与图片显示
	document.getElementById("flow_kpi").style.display = "";

	// 查看是否有操作KPI的权限
	$
			.post("showKpiOption.action",
					{
						"kpiAndId" : kpiNameId
					},
					function(v) {
						if (v == 1) {
							// 导入导出显示
					document.getElementById("kpiInputOutPut").style.visibility = "visible";
					Ext.getCmp('btn_sub').setDisabled(false);
					Ext.getCmp('btn_sub').setVisible(true);
				}
			});

	document.getElementById("kpiId").value = kpiNameId;
	document.getElementById("startTime1").value = startTime;
	document.getElementById("endTime1").value = endTime;
}

/**
 * 清空KPI选项
 */
function cancelButton() {
	// 复选框还原 KPI名称
	if (document.getElementById("kpiType").options[0]) {
		document.getElementById("kpiType").options[0].selected = true;
	}

	if (document.getElementById("startKpiWeek").options[0]) {
		document.getElementById("startKpiWeek").innerHTML = '';
	}
	if (document.getElementById("endKpiWeek").options[0]) {
		document.getElementById("endKpiWeek").innerHTML = '';
	}
	if (document.getElementById("startKpiSeason").options[0]) {
		document.getElementById("startKpiSeason").innerHTML = '';
	}
	if (document.getElementById("endKpiSeason").options[0]) {
		document.getElementById("endKpiSeason").innerHTML = '';
	}
	document.getElementById("startTime").value = '';
	document.getElementById("endTime").value = '';
}
// KPI grip
function processKpiGrid() {
	processGridStore = new Ext.data.Store( {
		proxy : new Ext.data.HttpProxy( {
			url : "searchFlowKpi.action",
			method : 'POST'
		}),
		reader : new Ext.data.JsonReader( {
			totalProperty : "totalProperty",
			imageUrl : 'imageUrl',
			root : "root",
			id : "id"
		}, [ {
			name : "kpiId",
			mapping : "kpiId"
		}, {
			name : "kpiAndId",
			mapping : "kpiAndId"
		}, {
			name : "flowId",
			mapping : "flowId"
		}, {
			name : "kpiHorVlaue",
			mapping : "kpiHorVlaue"
		}, {
			name : "kpiValue",
			mapping : "kpiValue"
		} ])

	});
	var checkbox = new Ext.grid.CheckboxSelectionModel(); // 复选框
	processGridColumnModel = new Ext.grid.ColumnModel( [
			new Ext.grid.RowNumberer( {
				width : 30
			}), {
				header : i_kpiHorVlaue,
				dataIndex : "kpiHorVlaue",
				width : 140,
				renderer : renderDate('Y-m'),
				align : "center",
				sortable : false
			}, {
				header : i_kpiValue,
				dataIndex : "kpiValue",
				width : 160,
				align : "center",
				sortable : false,
				editor : new Ext.form.NumberField( {
					allowBlank : true,
					allowNegative : false,
					maxValue : 1000000000,
					regex : /^\d+(?:\.\d{1,2})?$/,
					regexText : i_theMostCanEnterTwoDecimalPlaces, // 正则表达式错误提示
					// 最多只能输入两位小数！
					decimalPrecision : 10
				// 允许输入十位小数(正则表达式只允许两位，综合之下如果输入超过两位无法保存且不会四舍五入)
						})

			} ]);
	processGridBbar = new Ext.PagingToolbar( {
		pageSize : pageSize,
		store : processGridStore,
		displayInfo : true,
		displayMsg : "{0}--{1} " + i_twig + i_together + " {2}" + i_twig,
		emptyMsg : i_noData
	});

	processGridPanel = new Ext.grid.EditorGridPanel(
			{
				id : "kpiGridPanel",
				renderTo : 'kpiTableId',
				width : 350,
				height : Ext.getCmp("processKPI_id").getInnerHeight() - 150,
				autoScroll : true,// 滚动条
				stripeRows : true,// 显示行的分隔符
				// columnLines:true,//显示列的分隔线
				viewConfig : {
					forceFit : true
				// 让列的宽度和grid一样
				},
				tbar : new Ext.Toolbar(
						{
							buttonAlign : 'center',
							items : [
							'->',
									{
										id : "btn_sub",
										disabled : true,
										hidden : true,
										text : i_submit,
										handler : (function(btn, e) {
											var mr = processGridStore
													.getModifiedRecords();// 获取所有更新过的记录
											var recordCount = processGridStore
													.getCount();// 获取数据集中记录的数量
											if (mr.length == 0) { // 确认修改记录数量
												return;
											}
											var arrayKpi = "[";
											for ( var i = 0; i < mr.length; i++) {
												var tempPrrKpi = "{";
												tempPrrKpi += "\"kpiId\":"
														+ mr[i].data["kpiId"];
												tempPrrKpi += ",\"kpiValue\":'"
														+ mr[i].data["kpiValue"]
														+ "'";
												tempPrrKpi += ",\"kpiAndId\":"
														+ mr[i].data["kpiAndId"];
												tempPrrKpi += ",\"flowId\":"
														+ mr[i].data["flowId"];
												var kpiHorVlaue = mr[i].data["kpiHorVlaue"];
												var kpiHorVlaueStr = (kpiHorVlaue.year + 1900)
														+ "-"
														+ (kpiHorVlaue.month + 1)
														+ "-"
														+ kpiHorVlaue.date;
												tempPrrKpi += ",\"kpiHorVlaueStr\":"
														+ kpiHorVlaueStr;
												if (i == (mr.length - 1)) {
													tempPrrKpi += "}";
												} else {
													tempPrrKpi += "},";
												}
												arrayKpi += tempPrrKpi;
											}
											arrayKpi += "]";
											Ext.Ajax
													.request( {
														url : 'updateFlowKpiImage.action',
														method : 'POST',
														timeout : 1000000,
														params : {
															arrKpi : arrayKpi
														},
														success : function(
																response,
																options) {
															searchKPI();
														},
														failure : function(
																response,
																options) {
															Ext.Msg
																	.alert(
																			i_tip,
																			i_submitFalse);
														}
													});

										}).createDelegate(this)
									},
									{
										text : i_kpiDatil,
										// iconCls : 'btn-add-icon',
										handler : (function(btn, e) {
											var kpiAndId = document
													.getElementById("kpiId").value;
											kpiDatil(kpiAndId);
										}).createDelegate(this)
									} ]

						}),
//				tbar : [
//						"->",
//						{
//							id : "btn_sub",
//							disabled : true,
//							hidden : true,
//							text : i_submit,
//							handler : (function(btn, e) {
//								var mr = processGridStore.getModifiedRecords();// 获取所有更新过的记录
//							var recordCount = processGridStore.getCount();// 获取数据集中记录的数量
//							if (mr.length == 0) { // 确认修改记录数量
//								return;
//							}
//							var arrayKpi = "[";
//							for ( var i = 0; i < mr.length; i++) {
//
//								var tempPrrKpi = "{";
//								tempPrrKpi += "\"kpiId\":"
//										+ mr[i].data["kpiId"];
//								tempPrrKpi += ",\"kpiValue\":'"
//										+ mr[i].data["kpiValue"] + "'";
//								tempPrrKpi += ",\"kpiAndId\":"
//										+ mr[i].data["kpiAndId"];
//								tempPrrKpi += ",\"flowId\":"
//										+ mr[i].data["flowId"];
//								var kpiHorVlaue = mr[i].data["kpiHorVlaue"];
//								var kpiHorVlaueStr = (kpiHorVlaue.year + 1900)
//										+ "-" + (kpiHorVlaue.month + 1) + "-"
//										+ kpiHorVlaue.date;
//								tempPrrKpi += ",\"kpiHorVlaueStr\":"
//										+ kpiHorVlaueStr;
//								if (i == (mr.length - 1)) {
//									tempPrrKpi += "}";
//								} else {
//									tempPrrKpi += "},";
//								}
//								arrayKpi += tempPrrKpi;
//							}
//							arrayKpi += "]";
//							Ext.Ajax.request( {
//								url : 'updateFlowKpiImage.action',
//								method : 'POST',
//								timeout : 1000000,
//								params : {
//									arrKpi : arrayKpi
//								},
//								success : function(response, options) {
//									searchKPI();
//								},
//								failure : function(response, options) {
//									Ext.Msg.alert(i_tip, i_submitFalse);
//								}
//							});
//
//						}).createDelegate(this)
//						},
//						{
//							text : i_kpiDatil,
//							// iconCls : 'btn-add-icon',
//							handler : (function(btn, e) {
//								var kpiAndId = document.getElementById("kpiId").value;
//								kpiDatil(kpiAndId);
//							}).createDelegate(this)
//						} ],
				bodyStyle : 'border-left: 0px;',
				frame : false,
				store : processGridStore,
				colModel : processGridColumnModel
			// ,
			// bbar : processGridBbar
			})

}
// 格式化日期
function renderDate(format) {
	return function(v) {
		var JsonDateValue;
		if (Ext.isEmpty(v))
			return '';
		else if (Ext.isEmpty(v.time))
			JsonDateValue = new Date(v);
		else
			JsonDateValue = new Date(v.time);
		return JsonDateValue.format(format || 'Y-m-d H:i:s');
	};
};

// 移除KPI 值

function removeKpiValue(kpiId) {
	var url = "deleteFlowKpiImage.action?jecnFlowKpi.kpiId=" + kpiId;
	$.get(url, function(v) {
		if (v == "SUCCESS") {
			searchKPI();
		}
	});
}
// kpi导入
function KPIinput() {
	var obj = document.getElementById("kpiType");
	// 序号，取当前选中选项的序号
	var index = obj.selectedIndex;
	if (index == 0) {// 没有选择KPI类型
		alert(i_qingxuanzekpi);
		return;
	}
	// kpi统计类型 0：月 1：季度
	var kpiHorType = document.getElementById("kpiHorType").value;
	// 开始时间
	var startTime = document.getElementById("startTime").value
	var endTime = document.getElementById("endTime").value

	if (startTime == null || startTime == "" || endTime == null
			|| endTime == "") {
		// "提示" + "搜索时间不能为空！"
		Ext.MessageBox.alert(i_tip, i_KPISearchNotNull);
		return;
	}
	if (startTime > endTime) {// 结束时间大于开始时间
		// "开始时间应小于结束时间！"
		Ext.Msg.alert(i_tip, i_startTimeIsLessEndTime);
		return;
	}

	var m = spaceMonths(startTime, endTime);
	if (m > 60)// 超过五年
	{
		Ext.Msg.alert(i_tip, i_intervalTimeCannotBigThan + 5 + i_year);
		return;
	}

	var kpiNameIdObj = document.getElementById("kpiNameId");
	// KPI名称ID
	var kpiNameId = kpiNameIdObj.options[index].text;
	document.getElementById("kpiId").value = kpiNameId;
	var excelFilePath = document.getElementById("excelFilePath").value;
	if (excelFilePath == '') { // 请选择需要导入的Excel文件！
		Ext.Msg.alert(i_tip, i_needImpordExcelFile);
		return;
	} // 检查文件格式(.xls)
	if (!/\.xls$/.test(excelFilePath)) { // 请选择正确的EXCEL文件(后缀为xls)！
		Ext.Msg.alert(i_tip, i_selectRightExcelFile);
		return;
	}
	var flowId = $("#flowId").val();
	$.ajaxFileUpload( {
		url : 'excelImporteFlowKpiImage.action?flowId=' + flowId + "&kpiAndId="
				+ kpiNameId + "&kpiHorType=" + kpiHorType + "&startTime="
				+ startTime + "&endTime=" + endTime, // 上传文件的服务端
		secureuri : true, // 是否启用安全提交
		dataType : 'text', // 数据类型
		fileElementId : 'excelFilePath', // 表示文件域ID
		// 提交成功后处理函数 html为返回值，status为执行的状态
		success : function(html, status) {

			if (html != null && html != "")// 导入成功
			{
				alert(html);
				searchKPI();
			} else {// 导入失败
				alert(i_inportFail);
			}

		},
		// 提交失败处理函数
		error : function(html, status, e) {
			alert(i_upfail);
		}
	});
}

// 导出KPI
function KPIOutPut() {
	var form = document.forms[0];
	form.action = "downLoadflowKpi.action";
	form.submit();
}
// 编辑KPI值
function editKpiValue(kpiId, kpiAndId, flowId, kpiHorVlaue, kpiValue) {
	var kpiFormPanel = new Ext.form.FormPanel( {
		defaultType : "textfield",
		labelAlign : "right",
		bodyStyle : "padding:5px",
		labelWidth : 80,
		baseCls : "x-plain",
		defaults : {
			width : 200
		},
		items : [ {
			id : 'kpiId',
			name : 'kpiId',
			value : kpiId,
			xtype : 'hidden'
		}, {
			id : 'kpiAndId',
			name : 'kpiAndId',
			value : kpiAndId,
			xtype : 'hidden'
		}, {
			id : 'flowId',
			name : 'flowId',
			value : flowId,
			xtype : 'hidden'
		}, {
			id : 'kpiHorVlaue',
			name : 'kpiHorVlaue',
			fieldLabel : i_kpiHorVlaue,
			xtype : 'textfield',
			value : kpiHorVlaue,
			readOnly : true
		}, {
			id : 'kpiValue',
			name : 'kpiValue',
			value : kpiValue,
			xtype : "numberfield",
			fieldLabel : i_kpiValue
		} ]
	});
	var kpiWindow = new Ext.Window( {
		title : i_updateKpiValue,
		height : 150,
		width : 320,
		closeAction : "close",
		resizable : false,
		modal : true,
		items : [ kpiFormPanel ],
		buttons : [ {
			text : i_ok,
			handler : function() {
				kpiFormPanel.getForm().submit( {
					url : 'updateFlowKpiImage.action',
					success : function(form, action) {
						searchKPI();
						Ext.Msg.alert(i_tip, i_editKpiSucess);
						kpiWindow.close();
					},
					failure : function(form, action) {// 加载失败的处理函数
						Ext.Msg.alert(i_tip, i_editKpifail);
					}
				});
			}
		}, {
			text : i_close,
			type : 'button',
			scope : this,
			handler : function(btn, e) {
				kpiWindow.close();
			}
		} ]
	});
	kpiWindow.show();
}
// KPI详细
function kpiDatil(kpiAndId) {

	var data, items;
	// 访问服务获取数据
	Ext.Ajax.request({
				url : 'showKpidatil.action',
				params : {
					kpiAndId : kpiAndId
				},
				method : 'GET',
				success : function(response, options) {
					
					var result=eval("("+response.responseText+")");
					
					data = result.data;
					items = result.items;
					var kpiDatilPanel = new Ext.form.FormPanel({
								border : false,
								layout : 'form',
								defaultType : "textfield",
								labelAlign : "right",
								bodyStyle : "padding:5px",
								labelWidth : 120,
								baseCls : 'x-panel-mc',
								defaults:{autoScroll: true},
								items : items
							});
					var kpiDatiWin = new Ext.Window({
								title : i_kpiDatil,
								height : 500,
								defaults:{autoScroll: true},
								layout :"fit",
								width : 480,
								closeAction : "close",
								resizable : false,
								modal : true,
								shadow : false,
								items : [kpiDatilPanel],
								buttons : [{
											text : i_close,
											type : 'button',
											scope : this,
											handler : function(btn, e) {
												kpiDatiWin.close();
											}
										}]
							});
					// 设置值
					kpiDatilPanel.getForm().setValues(data);
					kpiDatiWin.show();
				},
				failure : function(response, options) {
					Ext.MessageBox.alert('数据获取失败');
				}
			});

	// kpiDatilPanel.form.load( {
	// url : 'showKpidatil.action',
	// params : {
	// kpiAndId : kpiAndId
	// },
	// method : 'GET',
	// success : function(form, action) {
	// var showKPIProperty = action.result.data.showKPIProperty;
	// var allowSupplierKPIData = action.result.data.allowSupplierKPIData;
	// if (showKPIProperty == 1) {
	// Ext.getCmp("firstTargetId").setVisible(true);
	// Ext.getCmp("kpiRelevance").setVisible(true);
	// Ext.getCmp("kpiTargetType").setVisible(true);
	// }
	// if (allowSupplierKPIData == 1) {
	// Ext.getCmp("kpiDataPeopleId").setVisible(true);
	//			}
	//			kpiDatiWin.show();
	//		},
	//		failure : function(form, action) {
	//			Ext.Msg.alert('提示', '数据加载失败');
	//			kpiDatiWin.show();
	//		}
	//	});
}