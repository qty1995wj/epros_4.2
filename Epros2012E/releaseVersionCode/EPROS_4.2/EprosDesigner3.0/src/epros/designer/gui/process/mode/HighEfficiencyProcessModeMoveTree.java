package epros.designer.gui.process.mode;

import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.jecn.epros.server.bean.tree.JecnTreeBean;
import com.jecn.epros.server.util.JecnTreeEnumConstant.TreeNodeType;

import epros.designer.tree.JecnHighEfficiencyTree;
import epros.designer.tree.JecnTreeCommon;
import epros.designer.tree.JecnTreeListener;
import epros.designer.tree.JecnTreeModel;
import epros.designer.tree.JecnTreeNode;
import epros.designer.util.ConnectionPool;
import epros.designer.util.JecnProperties;

public class HighEfficiencyProcessModeMoveTree extends JecnHighEfficiencyTree{
	private Logger log = Logger.getLogger(HighEfficiencyProcessModeMoveTree.class);
	public HighEfficiencyProcessModeMoveTree(List<Long> listIds) {
		super(listIds);
	}
	@Override
	public JecnTreeListener getTreeExpansionListener(
			JecnHighEfficiencyTree jTree) {
		return new ProcessModeMoveTreeListener(this.getListIds(), jTree);
	}

	@Override
	public JecnTreeModel getTreeModel() {
		List<JecnTreeBean> list = new ArrayList<JecnTreeBean>();
		try {
			list = ConnectionPool.getProcessModeAction().getFlowModels(Long.valueOf(0));
		} catch (Exception e) {
			log.error("HighEfficiencyProcessModeMoveTree getTreeModel is error", e);
		}
		
		// 根节点 "流程模板"
		JecnTreeNode rootNode = JecnTreeCommon.createTreeRoot(
				TreeNodeType.processModeRoot,JecnProperties.getValue("processMode"));
		//
		JecnTreeCommon.addNLevelMoveNodes(list, rootNode, this.getListIds());
		return new JecnTreeModel(rootNode);
	}

	@Override
	public boolean isSelectMutil() {
		return false;
	}

	@Override
	public void jTreeMousePressed(MouseEvent evt) {
		
	}
}
