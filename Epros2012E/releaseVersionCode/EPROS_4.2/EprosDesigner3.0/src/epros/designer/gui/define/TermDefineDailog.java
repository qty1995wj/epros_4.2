package epros.designer.gui.define;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;

import epros.designer.gui.system.ButtonPanelCommon;
import epros.designer.gui.system.area.TextAreaFour;
import epros.designer.gui.system.field.TextFieldRequest;
import epros.designer.tree.JecnTree;
import epros.designer.tree.JecnTreeNode;
import epros.designer.util.JecnProperties;
import epros.draw.gui.swing.JecnPanel;
import epros.draw.gui.top.dialog.JecnDialog;
import epros.draw.gui.top.dialog.listener.JecnDialogEvent;
import epros.draw.gui.top.dialog.listener.JecnDialogListener;

public abstract class TermDefineDailog extends JecnDialog {

	/** 名称 */
	private TextFieldRequest defineField;
	/** 定义 */
	private TextAreaFour textAreaFour;
	/** 按钮 */
	private ButtonPanelCommon buttonPanelCommon = null;
	/** 主面板 */
	private JecnPanel mainPanel = new JecnPanel();

	/** 信息面板 */
	private JecnPanel infoPanel = new JecnPanel();

	private JecnTreeNode selectNode = null;
	private JecnTree jTree = null;

	public TermDefineDailog(JecnTreeNode selectNode, JecnTree jTree, String titleName) {
		this.selectNode = selectNode;
		this.jTree = jTree;
		this.setSize(510, 250);
		this.setTitle(titleName);
		this.setResizable(true);
		this.setModal(true);
		this.setLocationRelativeTo(null);
	}

	protected void initLayout(String defineName, String defineContent) {
		mainPanel.setLayout(new GridBagLayout());
		GridBagConstraints c = null;

		Insets insets = new Insets(3, 1, 3, 1);
		// 中心控件显示面板
		c = new GridBagConstraints(0, 0, 1, 1, 1.0, 1.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, insets, 0,
				0);
		mainPanel.add(infoPanel, c);
		infoPanel.setBorder(BorderFactory.createTitledBorder(""));
		infoPanel.setLayout(new GridBagLayout());
		int rows = 0;
		defineField = new TextFieldRequest(rows, infoPanel, insets, JecnProperties.getValue("nameC"), defineName,
				JecnProperties.getValue("name"));
		rows++;
		textAreaFour = new TextAreaFour(rows, infoPanel, insets, JecnProperties.getValue("defineC"), defineContent,
				JecnProperties.getValue("define"));
		buttonPanelCommon = new ButtonPanelCommon(mainPanel, insets);
		this.getContentPane().add(mainPanel);
		addAction();
	}

	private void addAction() {
		buttonPanelCommon.getOkBut().addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				okActionPerformed();
			}
		});
		buttonPanelCommon.getCancelBut().addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				cancelButPerformed();
			}
		});
		// 添加监听
		this.addJecnDialogListener(new JecnDialogListener() {
			@Override
			public boolean dialogCloseBefore(JecnDialogEvent e) {
				cancelButPerformed();
				return false;
			}
		});
	}

	private void okActionPerformed() {
		if (defineField.validateStr(buttonPanelCommon.getVerfyLab())) {
			return;
		}
		if (isUpdate()) {
			try {
				if (isExist(defineField.getResultStr(), selectNode)) {
					buttonPanelCommon.getVerfyLab().setText(JecnProperties.getValue("nameHaved"));
					return;
				}
				if (textAreaFour.isValidateNotPass(buttonPanelCommon.getVerfyLab())) {
					return;
				}
				this.save(defineField.getResultStr(), textAreaFour.getResultStr(), selectNode, jTree);
			} catch (Exception e) {
				log.error(JecnProperties.getValue("addOrEditTermDefineError"), e);
			}
		}
		this.setVisible(false);
	}

	protected abstract boolean isExist(String name, JecnTreeNode selectNode) throws Exception;

	protected abstract void save(String defineName, String defineContent, JecnTreeNode selectNode, JecnTree jTree)
			throws Exception;

	/**
	 * 取消
	 */
	protected void cancelButPerformed() {
		if (!isUpdate()) {// 没有更新
			this.setVisible(false);
		} else {
			int optionTig = this.dialogCloseBeforeMsgTipAction();
			if (optionTig == 2) {
				okActionPerformed();
			} else if (optionTig == 1) {
				this.setVisible(false);
			}
		}
	}

	private boolean isUpdate() {
		if (defineField.isUpdate()) {
			return true;
		}
		if (textAreaFour.isUpdate()) {
			return true;
		}
		return false;
	}

	protected String getDefineName() {
		return defineField.getResultStr();
	}

	protected String getDefineContent() {
		return textAreaFour.getResultStr();
	}
}
