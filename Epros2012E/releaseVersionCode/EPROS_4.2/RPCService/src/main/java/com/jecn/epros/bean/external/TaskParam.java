package com.jecn.epros.bean.external;

import io.swagger.annotations.ApiParam;

import java.io.Serializable;
import java.util.List;

public class TaskParam implements Serializable {
    @ApiParam(value = "任务唯一标识", required = true)
    private String taskId;
    @ApiParam(value = "-1删除 5完成", required = true)
    private int taskOp;
    @ApiParam(value = "审批阶段", required = false)
    private List<TaskState> states;
    @ApiParam(value = "登录名称，也就是人员的唯一标识,当前请求的处理人", required = false)
    private String loginName;

    public String getTaskId() {
        return taskId;
    }

    public void setTaskId(String taskId) {
        this.taskId = taskId;
    }

    public int getTaskOp() {
        return taskOp;
    }

    public void setTaskOp(int taskOp) {
        this.taskOp = taskOp;
    }

    public List<TaskState> getStates() {
        return states;
    }

    public void setStates(List<TaskState> states) {
        this.states = states;
    }

    public String getLoginName() {
        return loginName;
    }

    public void setLoginName(String loginName) {
        this.loginName = loginName;
    }
}
