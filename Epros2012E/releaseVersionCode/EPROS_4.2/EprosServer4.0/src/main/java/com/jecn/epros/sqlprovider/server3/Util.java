package com.jecn.epros.sqlprovider.server3;

import com.jecn.epros.domain.JecnUser;
import com.jecn.epros.domain.email.*;
import com.jecn.epros.mapper.ConfigItemMapper;
import com.jecn.epros.mapper.EmailMapper;
import com.jecn.epros.service.ConfigItemBean;
import com.jecn.epros.sqlprovider.server3.ConfigContents.ConfigItemPartMapMark;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.*;

public class Util {
    private final static Log log = LogFactory.getLog(Util.class);

    /**
     * 把一个文件转化为字节
     *
     * @param file
     * @return byte[]
     * @throws Exception
     */
    public static byte[] getByte(File file) {
        byte[] bytes = null;
        if (file != null) {
            InputStream is = null;
            try {
                is = new FileInputStream(file);
                int length = (int) file.length();
                if (length > Integer.MAX_VALUE) // 当文件的长度超过了int的最大值
                {
                    return null;
                }
                bytes = new byte[length];
                int offset = 0;
                int numRead = 0;
                while (offset < bytes.length && (numRead = is.read(bytes, offset, bytes.length - offset)) >= 0) {
                    offset += numRead;
                }
                // 如果得到的字节长度和file实际的长度不一致就可能出错了
                if (offset < bytes.length) {
                    return null;
                }
            } catch (Exception e) {
                log.error("文件转换为字节流异常！", e);
            } finally {
                if (is != null) {
                    try {
                        is.close();
                    } catch (IOException e) {
                        log.error("关闭输入流异常！", e);
                    }
                }
            }
        }
        return bytes;
    }


    /**
     * 获得落款等信息
     *
     * @return
     */
    public static EmailTip getEmailTip(ConfigItemMapper mapper) {
        EmailTip tip = new EmailTip();
        StringBuffer buf = new StringBuffer();
        buf.append(" ('");
        buf.append(ConfigItemPartMapMark.basicEprosURL.toString());
        buf.append("','");
        buf.append(ConfigItemPartMapMark.mailAddrTip.toString());
        buf.append("','");
        buf.append(ConfigItemPartMapMark.mailPwdTip.toString());
        buf.append("','");
        buf.append(ConfigItemPartMapMark.mailEndContent.toString());
        buf.append("') ");
        String marks = buf.toString();
        Map<String, Object> paramMap = new HashMap<String, Object>();
        paramMap.put("marks", marks);
        List<ConfigItemBean> items = mapper.findConfigItemsByInMark(paramMap);
        for (ConfigItemBean item : items) {
            if (ConfigItemPartMapMark.basicEprosURL.toString().equals(item.getMark())) {
                tip.setAddress(item.getValue());
            } else if (ConfigItemPartMapMark.mailAddrTip.toString().equals(item.getMark())) {
                tip.setAddressTip(item.getValue());
            } else if (ConfigItemPartMapMark.mailPwdTip.toString().equals(item.getMark())) {
                tip.setPasswordTip(item.getValue());
            } else if (ConfigItemPartMapMark.mailEndContent.toString().equals(item.getMark())) {
                tip.setInscribe(item.getValue());
            }
        }
        return tip;
    }

    public static void saveEmails(List<EmailBasicInfo> basicInfos, EmailMapper emailMapper) {

        // 邮件创建时间
        Date createDate = new Date();
        Email email = null;
        EmailContent emailContent = null;
        EmailAttachment emailAttachment = null;
        EmailRelatedAttachment related = null;
        /** 附件id的集合 */
        List<String> attachmentIds = null;
        for (EmailBasicInfo basicInfo : basicInfos) {

            // 没有人员收取邮件
            if (basicInfo.getRecipients() == null || basicInfo.getRecipients().isEmpty()) {
                continue;
            }

            // 保存附件表
            attachmentIds = new ArrayList<String>();
            for (AttachmentFile attachment : basicInfo.getAttachments()) {
                if (attachment.getAttachment() == null) {
                    continue;
                }
                emailAttachment = new EmailAttachment();
                emailAttachment.setId(Common.getSortUUID());
                emailAttachment.setAttachmentName(attachment.getAttachmentName());
                emailAttachment.setAttachment(Util.getByte(attachment.getAttachment()));
                emailMapper.saveEmailAttachment(emailAttachment);
                attachmentIds.add(emailAttachment.getId());
            }

            // 保存邮件内容
            emailContent = new EmailContent();
            emailContent.setId(Common.getSortUUID());
            emailContent.setSubject(basicInfo.getSubject());
            emailContent.setContent(basicInfo.getContent());
            emailMapper.saveEmailContent(emailContent);

            // 保存邮件主表
            List<JecnUser> recipients = basicInfo.getRecipients();
            for (JecnUser user : recipients) {
                email = new Email();
                email.setId(Common.getSortUUID());
                email.setPeopleId(user.getId());
                email.setCreateDate(createDate);
                email.setSendState(0);
                email.setContentId(emailContent.getId());
                emailMapper.saveEmail(email);

                // 保存邮件和附件关联中间表
                for (String attachmentId : attachmentIds) {
                    related = new EmailRelatedAttachment();
                    related.setId(Common.getSortUUID());
                    related.setEmailId(email.getId());
                    related.setAttachmentId(attachmentId);
                    emailMapper.saveEmailRelatedAttachment(related);
                }
            }
        }
    }

}
