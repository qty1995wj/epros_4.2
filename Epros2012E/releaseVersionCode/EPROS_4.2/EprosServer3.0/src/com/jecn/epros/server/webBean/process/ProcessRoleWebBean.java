package com.jecn.epros.server.webBean.process;

import java.util.List;

import com.jecn.epros.server.bean.tree.JecnTreeBean;

public class ProcessRoleWebBean {
	/** 角色Id */
	private Long roleId;
	/** 角色名称 */
	private String roleName;
	/** 角色职责 */
	private String roleRes;
	/** 流程名称 */
	private String flowName;
	/** 流程ID */
	private Long flowId;
	/** 流程编号 */
	private String flowNumber;
	/**父节点 ID*/
	private Long pflowId;
	/**父节点名称*/
	private String pflowName;
	/**父节点类型*/
	private Long ptype;
	/** 参与岗位 */
	private List<JecnTreeBean> listPos;
	/** 参与活动的bean */
	private List<ProcessActiveWebBean> activeList;
	/**相关制度*/
	private List<Object[]> ruleList;
	/**相关标准*/
	private List<Object[]> stanList;
	/**相关风险*/
	private List<Object[]> riskList;
	/**应用人数*/
	private int totalUser;
	public Long getRoleId() {
		return roleId;
	}

	public void setRoleId(Long roleId) {
		this.roleId = roleId;
	}

	public String getRoleName() {
		return roleName;
	}

	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}

	public String getFlowName() {
		return flowName;
	}

	public void setFlowName(String flowName) {
		this.flowName = flowName;
	}

	public Long getFlowId() {
		return flowId;
	}

	public void setFlowId(Long flowId) {
		this.flowId = flowId;
	}

	public String getFlowNumber() {
		return flowNumber;
	}

	public void setFlowNumber(String flowNumber) {
		this.flowNumber = flowNumber;
	}

	public List<JecnTreeBean> getListPos() {
		return listPos;
	}

	public void setListPos(List<JecnTreeBean> listPos) {
		this.listPos = listPos;
	}

	public List<ProcessActiveWebBean> getActiveList() {
		return activeList;
	}

	public void setActiveList(List<ProcessActiveWebBean> activeList) {
		this.activeList = activeList;
	}

	public String getRoleRes() {
		return roleRes;
	}

	public void setRoleRes(String roleRes) {
		this.roleRes = roleRes;
	}

	public int getTotalUser() {
		return totalUser;
	}

	public void setTotalUser(int totalUser) {
		this.totalUser = totalUser;
	}

	public List<Object[]> getRuleList() {
		return ruleList;
	}

	public void setRuleList(List<Object[]> ruleList) {
		this.ruleList = ruleList;
	}

	public List<Object[]> getStanList() {
		return stanList;
	}

	public void setStanList(List<Object[]> stanList) {
		this.stanList = stanList;
	}

	public List<Object[]> getRiskList() {
		return riskList;
	}

	public void setRiskList(List<Object[]> riskList) {
		this.riskList = riskList;
	}

	public Long getPflowId() {
		return pflowId;
	}

	public void setPflowId(Long pflowId) {
		this.pflowId = pflowId;
	}

	public String getPflowName() {
		return pflowName;
	}

	public void setPflowName(String pflowName) {
		this.pflowName = pflowName;
	}

	public Long getPtype() {
		return ptype;
	}

	public void setPtype(Long ptype) {
		this.ptype = ptype;
	}
}
