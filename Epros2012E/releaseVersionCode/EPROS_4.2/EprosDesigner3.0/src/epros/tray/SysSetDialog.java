package epros.tray;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Properties;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.filechooser.FileFilter;

import org.apache.log4j.Logger;

import epros.designer.util.JecnProperties;
import epros.draw.gui.top.dialog.JecnDialog;
import epros.draw.gui.top.optionpane.JecnOptionPane;
import epros.draw.util.DrawCommon;
import epros.draw.util.JecnFileUtil;
import epros.draw.util.JecnUIUtil;

public class SysSetDialog extends JecnDialog {
	private static Logger log = Logger.getLogger(SysSetDialog.class);
	/** 主面板 */
	private JPanel mainPanel = null;
	/** 数据库配置面板 */
	private JPanel dataPanel = null;
	/** 数据初始化 */
	private JPanel initDataPanel = null;
	/** 服务器设置面板 */
	private JPanel serverPanel = null;
	/** 按钮面板 */
	private JPanel buttonPanel = null;
	/** 密钥面板 */
	private JPanel keyPanel = null;

	/** 数据库管理员用户名标签 */
	private JLabel userAdminNameLab = null;
	/** 数据库管理员用户名内容 */
	private JecnTextField userAdminNameFiled = null;

	/** 数据库管理员密码标签 */
	private JLabel userAdminPasswordLab = null;
	/** 数据库管理员密码内容 */
	private JPasswordField userAdminPasswordFiled = null;

	/** 设计器端口标签 */
	private JLabel desPortLab = null;
	private JecnTextField desginport = null;

	/** 密钥内容 */
	private JTextArea keyArea = null;
	private String strKeyArea = null;

	/** 上传标签 */
	private JButton keyload = null;
	/** 上传路径 */
	private String comFilePath = null;

	/** 数据初始化 */
	private JButton initDataButton = null;

	/** 确定 */
	private JButton findButton = null;
	/** 取消 */
	private JButton cancelButton = null;

	/** 项的内容大小 */
	private Dimension contentSize = null;

	/** 最内部面板的第一个组件的间距 */
	private Insets insetsFirst = null;

	/** 标签间距 */
	private Insets insetsLabel = null;

	/** 内容间距 */
	private Insets insetsContent = null;

	/** 只为数字验证码 */
	private String reg = "^[0-9]*$";// "[0-9]*";//

	/** IP验证码 */
	private String test = "([1-9]|[1-9]\\d|1\\d{2}|2[0-1]\\d|22[0-3])(\\.(\\d|[1-9]\\d|1\\d{2}|2[0-4]\\d|25[0-5])){3}";
	private Pattern pattern = Pattern.compile(test);
	private Matcher matcher = null;

	public SysSetDialog() {
		// 初始化组件
		initComponents();
		// 布局
		initLayout();
		this.setModal(true);
		// 设置面板不能手动拉伸放大缩小
		this.setResizable(false);
		//系统设置
		this.setTitle(JecnProperties.getValue("systemSetLab"));
		this.setIconImage(Toolkit.getDefaultToolkit().getImage("images/icons/trayIcon.gif"));
		this.setSize(new Dimension(540, 550));
		this.setLocationRelativeTo(null);
	}

	/**
	 * 
	 * 初始化组件
	 * 
	 */
	private void initComponents() {
		// 项的内容大小
		contentSize = new Dimension(160, 25);
		// 最内部面板的第一个组件的间距
		insetsFirst = new Insets(9, 3, 9, 3);
		// 标签间距
		insetsLabel = new Insets(9, 11, 9, 3);
		// 内容间距
		insetsContent = new Insets(9, 0, 9, 7);
		// 主面板
		this.mainPanel = new JPanel();
		// 数据库配置面板
		dataPanel = new JPanel();
		// 数据初始化面板
		initDataPanel = new JPanel();
		// 服务器设置面板
		serverPanel = new JPanel();
		// 按钮面板
		buttonPanel = new JPanel();
		// 密钥面板
		keyPanel = new JPanel();

		userAdminNameLab = new JLabel(JecnProperties.getValue("dbManagerC"));// 数据库管理员：
		userAdminNameFiled = new JecnTextField();
		userAdminNameFiled.setText("system");
		userAdminNameFiled.setEditable(false);

		userAdminPasswordLab = new JLabel(
				JecnProperties.getValue("managerPassword"));// 管理员密码：
		userAdminPasswordFiled = new JPasswordField();
		Dimension d = new Dimension(150, 20);
		userAdminPasswordFiled.setPreferredSize(d);
		userAdminPasswordFiled.setMaximumSize(d);
		userAdminPasswordFiled.setMinimumSize(d);
		// 设计器端口标签
		desPortLab = new JLabel(JecnProperties.getValue("designerProtC"));// 设计器端口：
		desginport = new JecnTextField();

		// 密钥标签
		keyArea = new JTextArea();

		// 上传传秘钥标签
		keyload = new JButton(JecnProperties.getValue("uploadPassword"));// 上传密钥

		// 数据初始化
		initDataButton = new JButton(JecnProperties.getValue("dbInit"));
		// 确定
		findButton = new JButton(JecnProperties.getValue("okBtn"));
		// 取消
		cancelButton = new JButton(JecnProperties.getValue("cancelBtn"));

		this.mainPanel.setLayout(new GridBagLayout());

		// 数据库配置面板
		dataPanel.setBorder(BorderFactory.createTitledBorder(JecnProperties
				.getValue("dbSet")));// 数据库设置
		dataPanel.setLayout(new GridBagLayout());

		// 数据库配置面板
		initDataPanel.setBorder(BorderFactory.createTitledBorder(JecnProperties
				.getValue("dbInit")));// 数据初始化
		initDataPanel.setLayout(new GridBagLayout());

		// 服务器设置面板
		serverPanel.setBorder(BorderFactory.createTitledBorder(JecnProperties
				.getValue("serverSet")));// 服务器设置
		serverPanel.setLayout(new GridBagLayout());

		// 按钮面板
		buttonPanel.setPreferredSize(new Dimension(520, 35));
		buttonPanel.setLayout(new FlowLayout(FlowLayout.RIGHT));
		// 密钥面板
		keyPanel.setLayout(new GridBagLayout());

		Dimension keyAreaSize = new Dimension(340, 115);
		keyArea.setPreferredSize(keyAreaSize);
		keyArea.setMinimumSize(keyAreaSize);
		keyArea.setBorder(BorderFactory.createLineBorder(Color.LIGHT_GRAY));
		keyArea.setEditable(false);

		// 设置各面板的默认背景色
		mainPanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		dataPanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		serverPanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		keyPanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		buttonPanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());

		initDataButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				initDataAction(e);
			}
		});
		// 确定
		findButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				findActionPerformedProcess(e);
			}
		});
		// 取消
		cancelButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				cancelActionPerformedProcess(e);

			}
		});
	}

	/**
	 * 布局
	 */
	private void initLayout() {
		GridBagConstraints cc = null;
		Insets insets = new Insets(5, 5, 5, 5);
		// *************第一层布局 start*************//
		// 数据库配置面板
		cc = new GridBagConstraints(0, 0, 1, 1, 1.0, 0.0,
				GridBagConstraints.NORTHWEST, GridBagConstraints.HORIZONTAL,
				insets, 0, 0);
		this.mainPanel.add(dataPanel, cc);

		// 服务器设置面板
		cc = new GridBagConstraints(0, 1, 1, 1, 1.0, 1.0,
				GridBagConstraints.NORTHWEST, GridBagConstraints.BOTH, insets,
				0, 0);
		this.mainPanel.add(serverPanel, cc);
		// 按钮面板
		cc = new GridBagConstraints(0, 2, 1, 1, 1.0, 0.0,
				GridBagConstraints.NORTHWEST, GridBagConstraints.HORIZONTAL,
				insets, 0, 0);
		this.mainPanel.add(buttonPanel, cc);
		// *************第一层布局 end*************//

		// *************数据库配置面板start*************//
		// // 第一行
		// addRowToPanel(dataPanel, dataTypeLab, databaseType, dataIPLab,
		// databaseip, 0);
		// // 第二行
		// addRowToPanel(dataPanel, dataPortLab, databasePort, dataNameLab,
		// databaseName, 1);
		// // 第三行
		// addRowToPanel(dataPanel, userLab, username, pwdLab, userpwd, 2);
		addRowToPanel(dataPanel, userAdminNameLab, userAdminNameFiled,
				userAdminPasswordLab, userAdminPasswordFiled, 0);
		cc = new GridBagConstraints(4, 4, 1, 1, 0.0, 0.0,
				GridBagConstraints.NORTHEAST, GridBagConstraints.NONE,
				insets, 0, 0);

		dataPanel.add(initDataButton, cc);
		// *************数据库配置面板 end*************//

		// *************服务器设置面板 start*************//
		// 第一行
		// addRowToPanel(serverPanel, desPortLab, desginport, serverPortLab,
		// serversport, 0);
		cc = new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
				GridBagConstraints.CENTER, GridBagConstraints.NONE, insets, 0,
				0);
		serverPanel.add(desPortLab, cc);
		cc = new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
				GridBagConstraints.NORTHWEST, GridBagConstraints.NONE, insets,
				0, 0);
		serverPanel.add(desginport, cc);
		// 密钥按钮
		cc = new GridBagConstraints(2, 0, 2, 1, 0.0, 0.0,
				GridBagConstraints.NORTHEAST, GridBagConstraints.NONE, insets,
				0, 0);
		serverPanel.add(keyload, cc);
		// 第三行：密钥面板
		cc = new GridBagConstraints(0, 1, 4, 1, 1.0, 1.0,
				GridBagConstraints.NORTHWEST, GridBagConstraints.BOTH, insets,
				0, 0);
		serverPanel.add(keyPanel, cc);

		// *************服务器设置面板 end*************//

		// *************密钥面板 start*************//
		// 密钥内容
		cc = new GridBagConstraints(0, 0, 1, 1, 1.0, 1.0,
				GridBagConstraints.NORTHWEST, GridBagConstraints.BOTH, insets,
				0, 0);
		keyPanel.add(keyArea, cc);

		// *************密钥面板 end*************//

		// *************按钮面板 start*************//
		// 确定
		buttonPanel.add(findButton);
		// 取消
		buttonPanel.add(cancelButton);

		this.getContentPane().add(mainPanel);
		// *************按钮面板 end*************//
		/** 上传秘钥 */
		keyload.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				/** 上传文件的控件* */
				JFileChooser fileChooser = new JFileChooser();
				/** 限制上传文件的格式* */
				// fileChooser.setDialogTitle("上传文件");
				/** 设置文件上传控制的标题* */
				fileChooser.setDialogTitle(JecnProperties
						.getValue("fileChoose"));
				fileChooser.setFileFilter(new FileFilter() {

					@Override
					public boolean accept(File f) {
						return f.isDirectory()
								|| (f.isFile() && (f.getName()
										.endsWith(".properties")));
					}

					@Override
					public String getDescription() {
						return null;
					}

				});
				int i = fileChooser.showSaveDialog(SysSetDialog.this);
				if (i == javax.swing.JFileChooser.APPROVE_OPTION) {
					File file = fileChooser.getSelectedFile();
					// String fileDirectory =
					// fileChooser.getCurrentDirectory().getPath();
					Properties properties = new Properties();
					InputStream in = null;
					FileInputStream fileInput = null;
					try {
						String paths = file.toString();
						comFilePath = paths;
						fileInput = new FileInputStream(paths);
						in = new BufferedInputStream(fileInput);
						properties.load(in);
						strKeyArea = properties.getProperty("key.license");
						keyArea.setText(strKeyArea);
					} catch (Exception e) {
						log.error("", e);
					}
				}
			}
		});
	}

	/**
	 * 
	 * 在流程元素面板上添加一行子组件
	 * 
	 * @param panel
	 *            JPanel 面板
	 * @param leftLabel
	 *            JLabel 左面标签
	 * @param leftCom
	 *            JComponent 左面内容
	 * @param rightLabel
	 *            JLabel 右面标签
	 * @param rightCom
	 *            JComponent 右面内容
	 * @param row
	 *            子组件的行索引 从0开始的
	 */
	private void addRowToPanel(JPanel panel, JLabel leftLabel,
			JComponent leftCom, JLabel rightLabel, JComponent rightCom, int row) {
		// 左面元素
		GridBagConstraints c = new GridBagConstraints(0, row, 1, 1, 0.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.NONE, insetsFirst,
				0, 0);
		panel.add(leftLabel, c);
		c = new GridBagConstraints(1, row, 1, 1, 0.0, 0.0,
				GridBagConstraints.NORTHWEST, GridBagConstraints.NONE,
				insetsContent, 0, 0);
		panel.add(leftCom, c);

		// 空闲区域
		c = new GridBagConstraints(2, 0, 1, 1, 1.0, 0.0,
				GridBagConstraints.EAST, GridBagConstraints.HORIZONTAL,
				insetsLabel, 0, 0);
		panel.add(new JLabel(), c);

		// 右面元素
		c = new GridBagConstraints(3, row, 1, 1, 0.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.NONE, insetsLabel,
				0, 0);
		panel.add(rightLabel, c);
		c = new GridBagConstraints(4, row, 1, 1, 0.0, 0.0,
				GridBagConstraints.NORTHWEST, GridBagConstraints.NONE,
				insetsContent, 0, 0);
		panel.add(rightCom, c);
	}

	public void initData() {
		// dataBaseBean.readUrl();
		// // 数据库类型
		// databaseType.setSelectedItem(dataBaseBean.getDbTypeEnum().toString());
		// // 数据库IP
		// databaseip.setText(dataBaseBean.getDbIP());
		// // 数据库名称
		// databaseName.setText(dataBaseBean.getDbName());
		// // 数据库端口号
		// databasePort.setText(dataBaseBean.getDbPort());

		// 设计器端口
		String defDesPort = TrayUtil.dataBaseBean.getDataDesinPort().toString();
		if (defDesPort == null || "".equals(defDesPort)) {
			defDesPort = "2088";
		}
		desginport.setText(defDesPort);
		// // 用户名
		// username.setText(dataBaseBean.getUsname());
		// // 密码
		// userpwd.setText(dataBaseBean.getUspwd());
		// // 方言
		// dialectext.setText(dataBaseBean.getDataDialect());
		// // 驱动
		// drivertext.setText(dataBaseBean.getDataDriver());

		keyArea.setText(JecnKey.getkeyEncrypt());
	}

	/**
	 * @author yxw 2013-8-6
	 * @description:数据导入
	 * @param e
	 */
	private void initDataAction(ActionEvent e) {
		Connection conn = TrayUtil.getConnection();
		FileOutputStream fos = null;
		PrintWriter pw = null;
		Process p = null;
		String path = JecnFileUtil.getSyncPath("/").substring(1);
		String pass = new String(userAdminPasswordFiled.getPassword());
		if (DrawCommon.isNullOrEmtry(pass)) {
			JecnOptionPane.showMessageDialog(this,
					JecnProperties.getValue("passwordNoEmpty"));// 密码不能为空！
			return;
		}
		if (conn != null) {
			try {
				conn.close();
			} catch (SQLException e1) {
				log.error("数据库连接关闭失败", e1);
			}
			int option = JecnOptionPane.showConfirmDialog(this,
					JecnProperties.getValue("theDataHasBeenInitialized"), null,
					JecnOptionPane.YES_NO_OPTION,
					JecnOptionPane.INFORMATION_MESSAGE);
			if (option == JecnOptionPane.NO_OPTION) {
				return;
			}
			try {

				String content = "sqlplus system/" + pass + "  \"@" + path
						+ "initData/del.sql\"" + "\r\n" + "exit;";
				fos = new FileOutputStream(new File(
						JecnFileUtil.getSyncPath("/") + "/initData/del.bat"));
				pw = new PrintWriter(fos);
				pw.write(content.toCharArray());
				pw.flush();
			} catch (IOException e1) {
				// TODO 自动生成 catch 块
				e1.printStackTrace();
			} finally {
				if (pw != null) {
					pw.close();
				}
				if (fos != null) {
					try {
						fos.close();
					} catch (IOException e1) {
						log.error("删除用户和表空间生成bat文件出错", e1);
					}
				}
			}

			try {

				String content = "drop user epros cascade;"
						+ "\r\n"
						+ "DROP TABLESPACE jecn INCLUDING CONTENTS AND DATAFILES;"
						+ "\r\n" + "exit;";
				fos = new FileOutputStream(new File(
						JecnFileUtil.getSyncPath("/") + "/initData/del.sql"));
				pw = new PrintWriter(fos);
				pw.write(content.toCharArray());
				pw.flush();
			} catch (IOException e1) {
				log.error("删除用户和表空间生成sql文件出错", e1);
			} finally {
				if (pw != null) {
					pw.close();
				}
				if (fos != null) {
					try {
						fos.close();
					} catch (IOException e1) {
						e1.printStackTrace();
					}
				}
			}

			try {
				String batPath = JecnFileUtil.getSyncPath("/").substring(1)
						+ "initData/del.bat";
				batPath = batPath.replaceAll(" ", "\" \"");
				p = Runtime.getRuntime().exec("cmd.exe /C start " + batPath);
				Scanner ins = new Scanner(p.getInputStream());
				Scanner insError = new Scanner(p.getErrorStream());
				while (ins.hasNextLine()) {

					String s = ins.nextLine();
					log.info(s);
				}
				while (insError.hasNextLine()) {
					String s = insError.nextLine();
					log.info(s);
				}
			} catch (IOException e1) {
				log.error("", e1);
			} finally {
				TrayUtil.closeProcess(p);
			}
			return;
		}

		try {

			String content = "sqlplus system/" + pass + "  \"@" + path
					+ "initData/init.sql\"" + "\r\n"
					+ "imp epros/epros file=\\\"" + path
					+ "initData/jecn.dmp\\\" full=y" + "\r\n"
					+ "sqlplus epros/epros  \"@" + path
					+ "initData/config.sql\"" + "\r\n" + "\r\n" + "exit;";
			fos = new FileOutputStream(new File(JecnFileUtil.getSyncPath("/")
					+ "/initData/init.bat"));
			pw = new PrintWriter(fos);
			pw.write(content.toCharArray());
			pw.flush();
		} catch (IOException e1) {
			log.error("数据导入生成bat文件出错", e1);
		} finally {
			if (pw != null) {
				pw.close();
			}
			if (fos != null) {
				try {
					fos.close();
				} catch (IOException e1) {
					e1.printStackTrace();
				}
			}
		}
		try {
			String dataPath = JecnFileUtil.getSyncPath("/").substring(1, 4)
					+ "eprosdata";
			File file = new File(dataPath);
			if (!file.exists()) {
				file.mkdir();
			} else {
				// 判断是否存在epros.dbf文件，如果存在则删除文件
				file = new File(dataPath + "/epros.dbf");
				if (file.exists()) {
					file.delete();
				}
			}
			String content = "create tablespace jecn datafile '"
					+ dataPath
					+ "/epros.dbf' size 100m autoextend on next 50m;"
					+ "\r\n"
					+ "create user epros identified by epros default tablespace jecn temporary tablespace temp;"
					+ "\r\n" + "grant connect,resource,dba to epros;" + "\r\n"
					+ "exit;";
			fos = new FileOutputStream(new File(JecnFileUtil.getSyncPath("/")
					+ "/initData/init.sql"));
			pw = new PrintWriter(fos);
			pw.write(content.toCharArray());
			pw.flush();
		} catch (IOException e1) {
			// TODO 自动生成 catch 块
			e1.printStackTrace();
		} finally {
			if (pw != null) {
				pw.close();
			}
			if (fos != null) {
				try {
					fos.close();
				} catch (IOException e1) {
					e1.printStackTrace();
				}
			}
		}

		try {
			String content = "update jecn_config_item set value='"
					+ JecnFileUtil.getSyncPath("/").substring(1, 4)
					+ "' where id=128;" + "\r\n" + "commit;" + "\r\n" + "exit;";
			fos = new FileOutputStream(new File(JecnFileUtil.getSyncPath("/")
					+ "/initData/config.sql"));
			pw = new PrintWriter(fos);
			pw.write(content.toCharArray());
			pw.flush();
		} catch (IOException e1) {
			// TODO 自动生成 catch 块
			e1.printStackTrace();
		} finally {
			if (pw != null) {
				pw.close();
			}
			if (fos != null) {
				try {
					fos.close();
				} catch (IOException e1) {
					log.error("", e1);
				}
			}
		}

		try {
			String batPath = JecnFileUtil.getSyncPath("/").substring(1)
					+ "initData/init.bat";
			batPath = batPath.replaceAll(" ", "\" \"");
			p = Runtime.getRuntime().exec("cmd.exe /C start " + batPath);
			Scanner ins = new Scanner(p.getInputStream());
			Scanner insError = new Scanner(p.getErrorStream());
			while (ins.hasNextLine()) {

				String s = ins.nextLine();
				log.info(s);
			}
			while (insError.hasNextLine()) {
				String s = insError.nextLine();
				log.info(s);
			}
		} catch (IOException e1) {
			log.error("", e1);
		} finally {
			TrayUtil.closeProcess(p);
		}

		// if (!savePanelToBean()) {
		// return;
		// }
		// // 验证数据库连接
		// if (TrayUtil.checkDBConn(dataBaseBean.getDataUrl(),
		// userAdminNameFiled
		// .getText().trim(), userAdminPasswordFiled.getText().trim(),
		// dataBaseBean.getDataDriver())) {
		// TrayUtil.importData(dataBaseBean.getDataUrl(), userAdminNameFiled
		// .getText().trim(), userAdminPasswordFiled.getText().trim(),
		// dataBaseBean.getDataDriver(), JecnFileUtil.getSyncPath("/")
		// + "init.sql");
		//
		// } else {
		// JOptionPane.showMessageDialog(this, "数据库连接异常，请检查配置！");
		// }

	}

	/**
	 * 
	 * 确定按钮动作事件
	 * 
	 * @param e
	 *            ActionEvent 事件
	 */

	private void findActionPerformedProcess(ActionEvent e) {

		try {
			if (!Pattern.matches("^[1-9][0-9]{1,3}$", desginport.getText()
					.trim())) {
				JecnOptionPane.showMessageDialog(this,
						JecnProperties.getValue("designerPortLimit"));// 设计器端口只能输入非0开头的数字,至少两位,最多4位,如：2088
				return;
			}
			String keyAreaValu = keyArea.getText().trim();
			if ("".equals(keyAreaValu)) {
				JecnOptionPane.showMessageDialog(this,
						JecnProperties.getValue("keyNoExistentUpload"));// 密钥不存在，请上传密钥！
				return;
			}
			// 获得解密的密钥
			String keyDecrypt = JecnKey.getKeyDecrypt(keyAreaValu);

			String[] str = keyDecrypt.split(",");
			if (str.length != 4 && str.length != 2) {
				JecnOptionPane.showMessageDialog(this,
						JecnProperties.getValue("keyErrorUpload"));// 密钥不正确，请重新上传！
				return;
			}
			// 验证密钥信息
			if (!JecnKey.isRegisterSuccess(str)) {
				JecnOptionPane.showMessageDialog(this,
						JecnProperties.getValue("keyErrorUpload"));
				return;
			}
			if (str.length == 4 && !JecnKey.isDateCheck(str)) {// 试用密钥
				// 验证密钥是否到期
				JecnOptionPane.showMessageDialog(this,
						JecnProperties.getValue("keyBecomeDue"));// 密钥到期，请重新上传！
				return;
			}
			// 保存密钥信息
			if (!JecnKey.loadKeyList(keyAreaValu)) {
				JecnOptionPane.showMessageDialog(this,
						JecnProperties.getValue("keySaveError"));// 密钥保存失败，请重新上传！
				return;
			}
			TrayUtil.dataBaseBean.setDataDesinPort(desginport.getText().trim());
			if (!writeDataBaseInfo()) {
				JecnOptionPane.showMessageDialog(this,
						JecnProperties.getValue("saveError"));// 保存失败！
				return;
			}
			this.setVisible(false);
		} catch (Exception ex) {
			log.error("", ex);
		}

	}

	/**
	 * 
	 * 取消按钮动作事件
	 * 
	 * @param e
	 *            ActionEvent 事件
	 */
	private void cancelActionPerformedProcess(ActionEvent e) {
		this.setVisible(false);
	}

	/**
	 * 
	 * 往数据库配置文件中存储数据信息
	 * 
	 * @return boolean 存储过程中出错返回false，负责返回true
	 */
	public boolean writeDataBaseInfo() {
		// 文件输出流
		FileOutputStream fos = null;

		String dbPath = TrayUtil.getRootPath()
				+ JecnTrayConstants.DB_CONFIG_FILE;
		try {
			TrayUtil.props.setProperty("rmi.port",
					TrayUtil.dataBaseBean.getDataDesinPort());

			fos = new FileOutputStream(dbPath);
			// 将Properties集合保存到流中
			TrayUtil.props.store(fos, "database.properties");
		} catch (FileNotFoundException ex) {
			log.error("DataBaseBuss类writeDataBaseInfo方法：文件找不到。文件路径=" + dbPath,
					ex);
			return false;
		} catch (IOException ex) {
			log.error("DataBaseBuss类writeDataBaseInfo方法：写数据库配置文件异常。文件路径="
					+ dbPath, ex);
			return false;
		} finally {

			if (fos != null) {
				try {
					// 关闭流
					fos.close();
				} catch (IOException e) {
					log.error(
							"DataBaseBuss类writeDataBaseInfo方法：关闭FileOutputStream对象异常。 文件路径="
									+ dbPath, e);
					return false;
				}
			}

		}
		return true;
	}

	class JecnTextField extends JTextField {

		public JecnTextField() {
			initComponents();
		}

		private void initComponents() {
			this.setPreferredSize(contentSize);
			this.setMinimumSize(contentSize);
			this.setMaximumSize(contentSize);
		}
	}

}
