package com.jecn.epros.server.webBean.dataImport.sync.excelOrDB;

import java.io.Serializable;

import com.jecn.epros.server.service.dataImport.sync.excelOrDb.unit.SyncTool;

public abstract class AbstractBaseBean implements Serializable {
	/** 主键 ID */
	protected Long numID = null;
	/** 插入或更新 PRO_FLAG 0：插入 1：更新 */
	protected int proFlag = 0;
	/** 错误信息 ERROR */
	protected String error = null;
	/** 是否有错误信息 IS_ERROR */
	protected int isError = 0;

	public Long getNumID() {
		return numID;
	}

	public void setNumID(Long numID) {
		this.numID = numID;
	}

	public int getProFlag() {
		return proFlag;
	}

	public void setProFlag(int proFlag) {
		this.proFlag = proFlag;
	}

	public String getError() {
		return error;
	}

	public void setError(String error) {
		this.error = error;
	}

	public int getIsError() {
		return isError;
	}

	public void setIsError(int isError) {
		this.isError = isError;
	}

	/**
	 * 
	 * 拼装字符串 把源字符串拼装到目标字符串上
	 * 
	 * @param srcInfo
	 *            目标字符串
	 * @param info
	 *            源字符串
	 * @return String 拼装后的字符串
	 */
	public void addError(String info) {
		if (SyncTool.isNullOrEmtryTrim(error)) {
			if (!SyncTool.isNullOrEmtryTrim(info)) {
				error = info;
			} else {
				error = null;
			}
		} else {
			if (!SyncTool.isNullOrEmtryTrim(info)) {
				if (!contains(error, info)) {
					error += ";" + info;
				}
			}
		}
	}

	/**
	 * 
	 * 参数error是否包含info，包含返回true，不包含返回false
	 * 
	 * @param error
	 *            String
	 * @param info
	 *            String
	 * @return boolean
	 */
	protected boolean contains(String error, String info) {
		if (SyncTool.isNullOrEmtryTrim(error)
				|| SyncTool.isNullOrEmtryTrim(info)) {
			return false;
		} else {
			String[] strArr = error.split(";");
			for (String subStr : strArr) {
				if (info.equals(subStr)) {
					return true;
				}
			}
			return false;
		}
	}

	/**
	 * 
	 * 返回属性信息
	 * 
	 * @return
	 */
	public abstract String toInfo();
}
