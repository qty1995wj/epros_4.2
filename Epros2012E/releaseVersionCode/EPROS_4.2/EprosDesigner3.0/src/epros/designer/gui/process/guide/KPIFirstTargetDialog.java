package epros.designer.gui.process.guide;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Vector;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.table.DefaultTableModel;

import org.apache.log4j.Logger;

import com.jecn.epros.server.bean.process.JecnKPIFirstTargetBean;

import epros.designer.gui.common.JecnConstants;
import epros.designer.gui.common.JecnTextArea;
import epros.designer.gui.process.guide.guideTable.KPIFirstTargetTable;
import epros.designer.table.JecnTableModel;
import epros.designer.util.ConnectionPool;
import epros.designer.util.JecnProperties;
import epros.draw.gui.swing.JecnPanel;
import epros.draw.gui.top.dialog.JecnDialog;
import epros.draw.gui.top.optionpane.JecnOptionPane;
import epros.draw.util.DrawCommon;
import epros.draw.util.JecnUIUtil;

/***
 * 
 * @author Administrator
 * 
 */
public class KPIFirstTargetDialog extends JecnDialog {
	private static Logger log = Logger.getLogger(KPIFirstTargetDialog.class);
	/** 主面板 */
	private JecnPanel mainPanel = new JecnPanel(450, 330);
	/** 信息面板 */
	private JecnPanel infoPanel = new JecnPanel();
	/** 按钮Panel */
	private JecnPanel buttonPanel = new JecnPanel();
	/** 确定取消按钮Panel */
	private JecnPanel okClosebutPanel = new JecnPanel();
	/** 一级指标Table */
	protected KPIFirstTargetTable firstTargetTable = null;

	/** 控制目标滚动面板 */
	private JScrollPane firstTargetScrollPane = null;
	/** 控制目标ids */
	protected String firstTargetIds = "";
	/** 提示Lab */
	private JLabel verfyLab = new JLabel("");
	/** 上移Button */
	private JButton moveUpBut = new JButton(JecnProperties.getValue("upBtn"));
	/** 下移Button */
	private JButton moveDownBut = new JButton(JecnProperties
			.getValue("downBtn"));
	/** 添加Button */
	private JButton addBut = new JButton(JecnProperties.getValue("addBtn"));
	/** 编辑Button */
	private JButton editBut = new JButton(JecnProperties.getValue("editBtn"));
	/** 删除Button */
	private JButton deleteBut = new JButton(JecnProperties
			.getValue("deleteBtn"));

	/** 新建一级指标面板 */
	private JecnPanel newFirstTargetPanel = new JecnPanel();
	/** 新建一级指标内容显示面板 */
	private JecnPanel contentPanel = new JecnPanel();
	/** 新建一级指标====按钮面板 **/
	private JecnPanel contrTargetButPanel = new JecnPanel();
	/** 新建一级指标Area */
	protected JecnTextArea addfirstTargetArea = new JecnTextArea();// 400,180
	/** 新建一级指标滚动面板 */
	private JScrollPane addFirstTargetScrollPane = new JScrollPane(
			addfirstTargetArea);
	private JLabel firstTargetRequired = new JLabel(JecnProperties.getValue("required"));
	/** 新建一级指标验证提示信息 */
	protected JLabel firstTargetVerfy = new JLabel();

	/** 新建编辑一级指标**********"确定"************* 按钮*/
	private JButton saveBut = new JButton(JecnProperties.getValue("saveBut"));

	/** 新建编辑一级指标**********"取消"**********按钮 */
	private JButton cancelBut = new JButton(JecnProperties
			.getValue("cancelBtn"));
	/** 判断是添加还是编辑控制目标 0：添加，1：编辑 **/
	protected int isAddOrUpdate = 0;

	/** 一级指标数据集合 */
	private List<JecnKPIFirstTargetBean> firstTargetList = new ArrayList<JecnKPIFirstTargetBean>();
	/** 设置大小 */
	Dimension dimension = null;

	/** 确定 */
	private JButton okBut = new JButton(JecnProperties.getValue("okBtn"));

	/** 取消 */
	private JButton closeBut = new JButton(JecnProperties
			.getValue("cancelBtn"));
	
	private String targetDes = null;
	private String targetId = null;
	/**判断标识：true:确定按钮；false：取消按钮*/
	private boolean isOk = false;
	
	
	public KPIFirstTargetDialog() {
		// 设置窗体大小
		this.setSize(670, 500);
		this.setTitle(JecnProperties.getValue("firstTargetTitle"));
		this.setModal(true);
		// 设置窗体不可编辑
		this.setResizable(true);
		// 设置窗体居中显示
		this.setLocationRelativeTo(null);
		// 初始化数据
		initComponents();
		// 初始化界面
		initLayout();
	}

	/***
	 * 初始化数据
	 */
	private void initComponents() {
		//获取一级指标数据集合firstTargetList
		try {
			firstTargetList = ConnectionPool.getProcessAction().getAllFirstTarget();
		} catch (Exception e1) {
			log.error("KPIFirstTargetDialog initComponents is error",e1);
		}
		firstTargetTable = new KPIFirstTargetTable(firstTargetList);
		firstTargetScrollPane = new JScrollPane();
		firstTargetScrollPane.setViewportView(firstTargetTable);
		firstTargetScrollPane.getViewport().setBackground(
				JecnUIUtil.getDefaultBackgroundColor());

		mainPanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		infoPanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		buttonPanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		okClosebutPanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		newFirstTargetPanel.setBackground(JecnUIUtil
				.getDefaultBackgroundColor());
		contrTargetButPanel.setBackground(JecnUIUtil
				.getDefaultBackgroundColor());
		contentPanel.setBackground(JecnUIUtil.getDefaultBackgroundColor());
		// 控制目标
		addfirstTargetArea.setBorder(null);
		addFirstTargetScrollPane.setBorder(BorderFactory
				.createLineBorder(Color.gray));
		addFirstTargetScrollPane
				.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		addFirstTargetScrollPane
				.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
		// 换行
		addfirstTargetArea.setLineWrap(true);
		firstTargetVerfy.setForeground(Color.red);
		firstTargetRequired.setForeground(Color.red);

		newFirstTargetPanel.setVisible(false);
		verfyLab.setForeground(Color.red);

		moveUpBut.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				moveUpButPerformed();
			}
		});
		moveDownBut.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				moveDownButPerformed();
			}
		});
		addBut.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				addButPerformed();
			}
		});
		editBut.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				editButPerformed();
			}
		});
		deleteBut.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				deleteButPerformed();
			}
		});
		saveBut.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				saveButPerformed();
			}
		});
		cancelBut.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				cancelButPerformed();
			}
		});
		okBut.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				okButPerformed();
			}
		});
		closeBut.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				closeButPerformed();
			}
		});
		firstTargetTable.addMouseListener(new MouseAdapter() {
			public void mouseReleased(MouseEvent evt) {
				firstTargetTablemouseReleased(evt);
			}
		});
	}

	/***
	 * 初始化界面
	 */
	private void initLayout() {
		mainPanel.setLayout(new GridBagLayout());
		GridBagConstraints c = null;
		Insets insets = new Insets(5, 5, 5, 5);

		// 信息面板
		c = new GridBagConstraints(0, 1, 1, 1, 1.0, 1.0,
				GridBagConstraints.NORTHWEST, GridBagConstraints.BOTH, insets,
				0, 0);
		mainPanel.add(infoPanel, c);
		infoPanel.setLayout(new GridBagLayout());

		// 一级指标Table集合
		c = new GridBagConstraints(0, 0, 1, 1, 1.0, 1.0,
				GridBagConstraints.NORTHWEST, GridBagConstraints.BOTH, insets,
				0, 0);
		infoPanel.add(firstTargetScrollPane, c);
		infoPanel.setBorder(BorderFactory.createTitledBorder(""));
		// 新建一级指标面板
		c = new GridBagConstraints(0, 0, 1, 1, 1.0, 1.0,
				GridBagConstraints.NORTHWEST, GridBagConstraints.BOTH,
				new Insets(0, 5, 0, 5), 0, 0);
		mainPanel.add(newFirstTargetPanel, c);
		newFirstTargetPanel.setLayout(new GridBagLayout());
		// 新建一级指标内容显示
		c = new GridBagConstraints(0, 0, 1, 1, 1.0, 1.0,
				GridBagConstraints.NORTHWEST, GridBagConstraints.BOTH,
				new Insets(0, 5, 0, 5), 0, 0);
		newFirstTargetPanel.add(contentPanel, c);

		contentPanel.setLayout(new GridBagLayout());

		// 一级指标显示框
		c = new GridBagConstraints(0, 1, 1, 1, 1.0, 1.0,
				GridBagConstraints.WEST, GridBagConstraints.BOTH, insets, 0, 0);
		contentPanel.add(addFirstTargetScrollPane, c);
		c = new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0,
				GridBagConstraints.NORTHWEST, GridBagConstraints.NONE,
				new Insets(5, 0, 5, 0), 0, 0);
		contentPanel.add(firstTargetRequired, c);

		// 新建一级指标按钮面板
		c = new GridBagConstraints(0, 1, 1, 1, 1.0, 0.0,
				GridBagConstraints.NORTHWEST, GridBagConstraints.HORIZONTAL,
				new Insets(0, 5, 0, 5), 0, 0);
		newFirstTargetPanel.add(contrTargetButPanel, c);
		contrTargetButPanel.setLayout(new FlowLayout(FlowLayout.RIGHT));
		contrTargetButPanel.add(firstTargetVerfy);
		contrTargetButPanel.add(saveBut);
		contrTargetButPanel.add(cancelBut);

		// 按钮面板
		c = new GridBagConstraints(0, 1, 1, 1, 1.0, 0.0,
				GridBagConstraints.NORTHWEST, GridBagConstraints.HORIZONTAL,
				new Insets(0, 5, 0, 5), 0, 0);
		infoPanel.add(buttonPanel, c);
		buttonPanel.setLayout(new FlowLayout(FlowLayout.LEFT));

//		buttonPanel.add(moveUpBut);
//		buttonPanel.add(moveDownBut);
		buttonPanel.add(addBut);
		buttonPanel.add(editBut);
		buttonPanel.add(deleteBut);
		buttonPanel.add(verfyLab);

		c = new GridBagConstraints(0, 2, 1, 1, 1.0, 0.0,
				GridBagConstraints.NORTHWEST, GridBagConstraints.HORIZONTAL,
				new Insets(0, 5, 0, 5), 0, 0);
		mainPanel.add(okClosebutPanel, c);
		okClosebutPanel.setLayout(new FlowLayout(FlowLayout.RIGHT));
		okClosebutPanel.add(okBut);
		okClosebutPanel.add(closeBut);

		this.getContentPane().add(mainPanel);
	}

	/**
	 * 上移
	 */
	private void moveUpButPerformed() {

	}

	/**
	 * 下移
	 */
	private void moveDownButPerformed() {
	}

	/**
	 * 添加
	 */
	private void addButPerformed() {
		isAddOrUpdate=0;
		newFirstTargetPanel.setBorder(BorderFactory.createTitledBorder(JecnProperties.getValue("addBtn")));
		newFirstTargetPanel.setVisible(true);
		verfyLab.setText("");
		addfirstTargetArea.setText("");
	}

	/**
	 * 编辑
	 */
	private void editButPerformed() {
		verfyLab.setText("");
		isAddOrUpdate = 1;
		newFirstTargetPanel.setBorder(BorderFactory.createTitledBorder(JecnProperties.getValue("editBtn")));
		//提示请选中行
		int[] selectRows = firstTargetTable.getSelectedRows();
		if (selectRows.length != 1) {
			verfyLab.setText(JecnProperties.getValue("chooseOneRow"));
			return;
		}
		String targetDes = (String) firstTargetTable.getModel().getValueAt(firstTargetTable.getSelectedRow(), 2);
		if(targetDes!= null){
			this.addfirstTargetArea.setText(targetDes);
		}
		newFirstTargetPanel.setVisible(true);
	}
	
	
	/**
	 * 反选结果集
	 * @param id
	 */
	public void selectTableResult(String id){
		if(!"".equals(id)){
			for (int i = 0; i < firstTargetTable.getRowCount(); i++) {
				JecnTableModel model = (JecnTableModel) this.firstTargetTable.getModel();
				if(id.equals(model.getValueAt(i, 0).toString())){
					firstTargetTable.getSelectionModel().setSelectionInterval(i, i);
					firstTargetTable.updateUI();
					break;
				}
			}
		}
	}

	/**
	 * 删除
	 */
	private void deleteButPerformed() {
		verfyLab.setText("");
		//提示请选中行
		int[] selectRows = firstTargetTable.getSelectedRows();
		if (selectRows.length <= 0) {
			verfyLab.setText(JecnProperties.getValue("task_selectedRowsInfo"));
			return;
		}
		int option = JecnOptionPane.showConfirmDialog(this,
				JecnProperties.getValue("isDelete"), null,
				JecnOptionPane.YES_NO_OPTION,
				JecnOptionPane.INFORMATION_MESSAGE);
		if (option == JecnOptionPane.NO_OPTION) {
			return;
		}
		// 删除table选中的数据
		for (int i = selectRows.length - 1; i >= 0; i--) {
			((DefaultTableModel) firstTargetTable.getModel())
					.removeRow(selectRows[i]);
		}
		JecnTableModel model = (JecnTableModel) this.firstTargetTable.getModel();
		int rows = model.getRowCount();

		// 重设序号
		for (int i = 0; i < rows; i++) {
			model.setValueAt(String.valueOf(i + 1), i, 1);
		}
		verfyLab.setText("");
		this.addfirstTargetArea.setText("");
		newFirstTargetPanel.setVisible(false);
	}

	/**
	 * 新建编辑一级指标--保存
	 */
	private void saveButPerformed() {
		firstTargetVerfy.setText("");
		String firstTargetDesc = this.addfirstTargetArea.getText();
		//一级指标长度验证
		if(DrawCommon.isNullOrEmtryTrim(firstTargetDesc)){
			firstTargetVerfy.setText(JecnProperties.getValue("firstTargetTitle")+JecnProperties.getValue("isNotEmpty"));
			return;
		}
		if(firstTargetDesc!= null && !"".equals(firstTargetDesc)){
			if(DrawCommon.checkNoteLength(firstTargetDesc)){
				firstTargetVerfy.setText(JecnProperties.getValue("firstTargetTitle") + JecnProperties.getValue("lengthNotOut"));
				return;
			}
		}
		if(isAddOrUpdate == 0){
			Vector<String> v = new Vector<String>();
			v.add("");
			v.add(String.valueOf(firstTargetTable.getModel().getRowCount()+1));
			v.add(firstTargetDesc);
			firstTargetTable.addRow(v);
			addfirstTargetArea.setText("");
		}else if(isAddOrUpdate == 1){
			//编辑一级指标点击保存操作
			if(firstTargetTable.getSelectedRow() == -1){
				verfyLab.setText(JecnProperties.getValue("chooseOneRow"));
				return;
			}
			firstTargetTable.getModel().setValueAt(this.addfirstTargetArea.getText(),
					firstTargetTable.getSelectedRow(), 2);
		}
	}

	/**
	 * 新建编辑一级指标--取消
	 */
	private void cancelButPerformed() {
		isOk = false;
		firstTargetVerfy.setText("");
		newFirstTargetPanel.setVisible(false);
	}
	
	/**
	 * 确定
	 */
	private void okButPerformed() {
		isOk = true;
		//一级指标数据
		Vector<Vector<String>> data = ((DefaultTableModel) firstTargetTable.getModel()).getDataVector();
		List<JecnKPIFirstTargetBean> listFirstTarget = new ArrayList<JecnKPIFirstTargetBean>();
		//判断如果选中行，则显示在父显示框中
		int[] selectRows = firstTargetTable.getSelectedRows();
		if (selectRows.length == 1) {
			targetDes = (String) firstTargetTable.getModel().getValueAt(firstTargetTable.getSelectedRow(), 2);
			//选中的一级指标为更新数据，存在ID
			String targetIdStr = (String)firstTargetTable.getModel().getValueAt(firstTargetTable.getSelectedRow(), 0);
			if(targetIdStr != null && !"".equals(targetIdStr)){
				targetId = targetIdStr;
			}
		}else if(selectRows.length >1){
			verfyLab.setText(JecnProperties.getValue("chooseOneRow"));
			return;
		}
		if(data.size()>0){
			for(Vector<String> str : data){
				//新建一级指标
				if(str.get(0) == null || "".equals(str.get(0))){
					JecnKPIFirstTargetBean kPIFirstTargetBean = new JecnKPIFirstTargetBean();
					//内容
					kPIFirstTargetBean.setTargetContent(str.get(2));
					//序号
//					kPIFirstTargetBean.setSort(Integer.parseInt(str.get(1)));
					// 更新人
					kPIFirstTargetBean.setUpdatePeopleId(JecnConstants.loginBean.getJecnUser()
							.getPeopleId());
					//更新时间
					kPIFirstTargetBean.setUpdateTime(new Date());
					// 创建人
					kPIFirstTargetBean.setCreatePeopleId(JecnConstants.loginBean.getJecnUser()
							.getPeopleId());
					//创建时间
					kPIFirstTargetBean.setCreateTime(new Date());
					if((firstTargetTable.getSelectedRow()+1) == Integer.parseInt(str.get(1))){
						kPIFirstTargetBean.setSortId(1);
					}
					listFirstTarget.add(kPIFirstTargetBean);
				}else{//更新一级指标
					for(JecnKPIFirstTargetBean jecnKPIFirstTargetBean : firstTargetList){
							if(jecnKPIFirstTargetBean.getId().equals(str.get(0).toString())){
								//内容
								jecnKPIFirstTargetBean.setTargetContent(str.get(2));
								// 更新人
								jecnKPIFirstTargetBean.setUpdatePeopleId(JecnConstants.loginBean.getJecnUser()
										.getPeopleId());
								//更新时间
								jecnKPIFirstTargetBean.setUpdateTime(new Date());
								//序号
//								jecnKPIFirstTargetBean.setSort(Integer.parseInt(str.get(1)));
								listFirstTarget.add(jecnKPIFirstTargetBean);
								break;
							}
					}
				}
			}
		}
		//保存一级指标
		try {
			String targetIdS = ConnectionPool.getProcessAction().addFirstTarget(listFirstTarget);
			//选中的一级指标为新建数据，不存在ID
			if (targetId == null) {
				targetId = targetIdS;
			}
			//关闭窗体
			this.dispose();
		} catch (Exception e) {
			log.error("KPIFirstTargetDialog is error",e);
		}
	}

	/**
	 * 取消
	 */
	private void closeButPerformed() {
		this.dispose();
	}

	/**
	 * @description:单击Table行
	 */
	private void firstTargetTablemouseReleased(MouseEvent evt) {
		if (isAddOrUpdate == 1) {
			if (evt.getClickCount() == 1) {
				int row = firstTargetTable.getSelectedRow();
				if (row != -1) {
					// 显示选中行将数据显示到下方的controlTargetDescArea中
					String desc = this.firstTargetTable.getModel().getValueAt(
							row, 2).toString();
					this.addfirstTargetArea.setText(desc);
				}
			}
		}
	}

	public String getTargetDes() {
		return targetDes;
	}

	public String getTargetId() {
		return targetId;
	}

	public boolean isOk() {
		return isOk;
	}
	
}
