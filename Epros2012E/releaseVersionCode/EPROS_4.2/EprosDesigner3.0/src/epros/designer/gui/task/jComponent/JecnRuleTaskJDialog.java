package epros.designer.gui.task.jComponent;

import com.jecn.epros.server.action.designer.rule.IRuleAction;
import com.jecn.epros.server.action.designer.task.IJecnTaskRecordAction;
import com.jecn.epros.server.action.designer.task.approve.IJecnCreateTaskAction;
import com.jecn.epros.server.bean.rule.RuleT;
import com.jecn.epros.server.bean.system.ProceeRuleTypeBean;
import com.jecn.epros.server.bean.task.JecnTempCreateTaskBean;

import epros.designer.gui.task.buss.TaskApproveConstant;
import epros.designer.tree.JecnTree;
import epros.designer.tree.JecnTreeNode;
import epros.designer.util.ConnectionPool;
import epros.draw.gui.top.optionpane.JecnOptionPane;

/**
 * 制度任务窗口
 * 
 * @author ZHANGXIAOHU
 * @date： 日期：Nov 15, 2012 时间：2:05:29 PM
 */
public class JecnRuleTaskJDialog extends JecnTaskJDialog {

	private Long ruleId = null;
	/** 制度信息 */
	private RuleT ruleT = null;

	public JecnRuleTaskJDialog(JecnTree jTree, JecnTreeNode treeNode, int approveType) {
		super(jTree, treeNode, approveType);
		this.treeNode = treeNode;
		this.ruleId = treeNode.getJecnTreeBean().getId();
		init();
	}

	/** 检查版本号存不存在 */
	protected boolean checkRevsion() {
		if (ruleId != null && !"".equals(ruleId)) {
			IJecnTaskRecordAction taskRecordAction = ConnectionPool.getTaskRecordAction();
			try {
				// 类型 0 流程，1文件，2制度
				if (taskRecordAction.isExistVersionbyFlowId(ruleId, this.jTextFieldVersion.getText(), 2)) {
					JecnOptionPane.showMessageDialog(this, TaskApproveConstant.docIdExist);
					return false;
				}
			} catch (Exception ex) {
				log.error("JecnRuleTaskJDialog checkRevsion is error", ex);
				JecnOptionPane.showMessageDialog(null, TaskApproveConstant.access_server_error);
				return false;
			}
		}
		return true;
	}

	/**
	 * 初始化其他显示
	 */
	protected void initElseShow() {
		taskName = treeNode.getJecnTreeBean().getName();
		jTextFieldTaskName.setText(treeNode.getJecnTreeBean().getName());
		version = this.jTextFieldVersion.getText();
		changeDescription = this.jTextAreaChangeDescription.getText();
		IRuleAction ruleAction = ConnectionPool.getRuleAction();
		try {
			ruleT = ruleAction.getRuleT(treeNode.getJecnTreeBean().getId());
		} catch (Exception ex) {
			JecnOptionPane.showMessageDialog(null, TaskApproveConstant.access_server_error);
			log.error("JecnRuleTaskJDialog initElseShow is error！", ex);
			return;
		}
		if (this.jecnTaskApplication.getFileType() != 0 || jecnTaskApplication.getIsPublic() != 0
				|| (this.jecnTaskApplication.getIsFlowNumber() != 0)) {
			if (ruleT == null) {
				JecnOptionPane.showMessageDialog(null, TaskApproveConstant.access_server_error);
				log.error("JecnRuleTaskJDialog ruleT is null！");
				return;
			}
			if (jecnTaskApplication.getFileType() != 0) {
				// 制度类别 jComboBoxClassify
				addComboxItem();
				if (ruleT.getTypeId() != null) {
					typeIds = ruleT.getTypeId().toString();
				}
			}
			if (jecnTaskApplication.getIsPublic() != 0) {
				if (ruleT.getIsPublic() != null && ruleT.getIsPublic() != 0) {// 0秘密；1公开
					this.jComboBoxSecurityClassification.setSelectedIndex(1);
				} else if (ruleT.getIsPublic() != null && ruleT.getIsPublic() == 0) {
					jComboBoxSecurityClassification.setSelectedIndex(0);
				}
			}
			if (this.jecnTaskApplication.getIsFlowNumber() != 0) {
				if (ruleT.getRuleNumber() != null) {
					this.jTextFieldNumber.setText(ruleT.getRuleNumber());
				}
			}
		}
	}

	/**
	 * 确定
	 * 
	 */
	protected boolean submitTask() {
		JecnTempCreateTaskBean createTaskBean = new JecnTempCreateTaskBean();
		/** 制度类别 */
		if (this.jecnTaskApplication.getFileType() != 0) {// 制度类别
			String typeName = (String) jComboBoxClassify.getSelectedItem();
			Long typeId = 0L;
			for (ProceeRuleTypeBean proceeRuleTypeBean : listTypeBean) {
				if (proceeRuleTypeBean.getTypeName().equals(typeName)) {
					typeId = proceeRuleTypeBean.getTypeId();
					typeIds = proceeRuleTypeBean.getTypeId().toString();
				}
			}
			ruleT.setTypeId(typeId);
		}
		/** 密级 */
		if (jecnTaskApplication.getIsPublic() != 0) {
			ruleT.setIsPublic(getIsPublic());
		}
		/** 编号 */
		if (this.jecnTaskApplication.getIsFlowNumber() != 0) {
			ruleT.setRuleNumber(this.jTextFieldNumber.getText());
		}
		try {
			IJecnCreateTaskAction designerTaskAction = ConnectionPool.getDesignerTaskAction();
			// 组装待处理信息对象
			setJecnTempCreateTaskBean(createTaskBean);
			// 制度基本信息
			createTaskBean.setRuleT(ruleT);

			designerTaskAction.createPRFTask(createTaskBean);
			JecnOptionPane.showMessageDialog(this, TaskApproveConstant.create_app_sucess);
		} catch (Exception ex) {
			log.error("JecnRuleTaskJDialog submitTask is error", ex);
			JecnOptionPane.showMessageDialog(this, TaskApproveConstant.create_app_error);
			return false;
		}
		return true;
	}
}
