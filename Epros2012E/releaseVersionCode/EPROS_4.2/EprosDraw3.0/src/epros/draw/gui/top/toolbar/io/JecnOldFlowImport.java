package epros.draw.gui.top.toolbar.io;

import java.awt.Color;
import java.awt.Point;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.swing.JOptionPane;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import epros.draw.constant.JecnLogConstants;
import epros.draw.constant.JecnToolBoxConstant.MapElemType;
import epros.draw.constant.JecnToolBoxConstant.MapType;
import epros.draw.constant.JecnWorkflowConstant.EditPointType;
import epros.draw.constant.JecnWorkflowConstant.FillColorChangType;
import epros.draw.constant.JecnWorkflowConstant.FillType;
import epros.draw.constant.JecnWorkflowConstant.LineDirectionEnum;
import epros.draw.data.figure.JecnActiveData;
import epros.draw.data.figure.JecnCommentLineData;
import epros.draw.data.figure.JecnFigureData;
import epros.draw.data.line.JecnBaseDivedingLineData;
import epros.draw.data.line.JecnManhattanLineData;
import epros.draw.data.line.JecnManhattanRouterResultData;
import epros.draw.data.line.JecnVHLineData;
import epros.draw.gui.figure.JecnBaseFigurePanel;
import epros.draw.gui.figure.shape.part.CommentText;
import epros.draw.gui.figure.unit.JecnFlowTemplate;
import epros.draw.gui.figure.unit.JecnPaintFigureUnit;
import epros.draw.gui.line.JecnBaseDividingLinePanel;
import epros.draw.gui.line.JecnBaseManhattanLinePanel;
import epros.draw.gui.line.shape.DividingLine;
import epros.draw.gui.line.shape.HDividingLine;
import epros.draw.gui.line.shape.ParallelLines;
import epros.draw.gui.line.shape.VDividingLine;
import epros.draw.gui.line.shape.VerticalLine;
import epros.draw.gui.swing.JecnBaseFlowElementPanel;
import epros.draw.gui.workflow.JecnDrawMainPanel;
import epros.draw.gui.workflow.tabbedPane.JecnDrawDesktopPane;
import epros.draw.gui.workflow.util.JecnAddFlowElementUnit;
import epros.draw.gui.workflow.util.JecnCreateFlowElement;
import epros.draw.util.DrawCommon;

/**
 * 流程图导入
 * 
 * @author Administrator
 * 
 */
public class JecnOldFlowImport {

	/**
	 * 旧版的导入图形到面板
	 * 
	 * @param doc
	 * @param workType
	 */
	public void oldReadXML(Document doc) {
		// 分割线
		gainParallelLineData(doc);
		gainVerticalLineData(doc);

		NodeList figures = doc.getElementsByTagName("figures");
		// 获取所有图形集合
		List<JecnBaseFlowElementPanel> baseFlowElementList = gainFiguresDate(figures);
		// 图形数据重新排序
		JecnFlowTemplate.sortAllFlowElementCloneList(baseFlowElementList);
		// 存储线段对应的小线段
		Map<JecnBaseManhattanLinePanel, List<JecnManhattanRouterResultData>> manLineList = new HashMap<JecnBaseManhattanLinePanel, List<JecnManhattanRouterResultData>>();
		// 连接线
		NodeList commonLines = doc.getElementsByTagName("CommonLines");
		gainConnectingLine(commonLines, baseFlowElementList, manLineList);

		// 连接线（带箭头）
		NodeList manLines = doc.getElementsByTagName("manhattanLines");
		gainConnectingLine(manLines, baseFlowElementList, manLineList);

		if (baseFlowElementList != null) {
			// 图形层级排序
			JecnFlowTemplate.sortAllFlowElementCloneList(baseFlowElementList);
			for (JecnBaseFlowElementPanel baseFlowElement : baseFlowElementList) {
				if (baseFlowElement instanceof JecnBaseFigurePanel) {
					JecnBaseFigurePanel figurePanel = (JecnBaseFigurePanel) baseFlowElement;
					// 给数据层赋值
					figurePanel.getFlowElementData().setZOrderIndex(getWorkflow().getMaxZOrderIndex());
					// 面板最大层级数加一
					getWorkflow().setMaxZOrderIndex(getWorkflow().getMaxZOrderIndex() + 2);
					// 面板添加流程元素
					JecnAddFlowElementUnit.addFigure(figurePanel);
				}
			}
		}

		Iterator<JecnBaseManhattanLinePanel> iterator = manLineList.keySet().iterator();
		while (iterator.hasNext()) {
			JecnBaseManhattanLinePanel jecnBaseManhattanLinePanel = (JecnBaseManhattanLinePanel) iterator.next();
			// 获取小线段数据
			List<JecnManhattanRouterResultData> datas = manLineList.get(jecnBaseManhattanLinePanel);
			// 排序
			// datas = jecnBaseManhattanLinePanel.reOrderLinePoint(datas);

			for (JecnManhattanRouterResultData jecnManhattanRouterResultData : datas) {
				jecnBaseManhattanLinePanel.addLineSegment(jecnManhattanRouterResultData.getStartPoint(),
						jecnManhattanRouterResultData.getEndPint());
			}
			jecnBaseManhattanLinePanel.getFlowElementData().setZOrderIndex(getWorkflow().getMaxZOrderIndex());
			// 面板最大层级加一
			getWorkflow().setMaxZOrderIndex(getWorkflow().getMaxZOrderIndex() + 2);
			// 给线段赋层级
			jecnBaseManhattanLinePanel.reSetFlowElementZorder();
			if (datas.size() == 0) {// 存在线段，不存在小线段数据层时
				// 异常，异常原因可能是图形端点和线段端点匹配不上，像素误差
				// 根据路由算法画曼哈顿线段
				JecnAddFlowElementUnit.addManhattinLine(jecnBaseManhattanLinePanel);

				JecnLogConstants.LOG_FLOW_IMPORT.info("导入老板流程图时，出现线段端点和图形端点未匹配！");
			} else {
				// 面板添加流程元素
				JecnAddFlowElementUnit.addImportManLine(jecnBaseManhattanLinePanel);
			}
		}

		// 斜线，横线，竖线
		NodeList dividing = doc.getElementsByTagName("dividLines");
		List<JecnBaseDividingLinePanel> diviLineList = getBaseDivedingLineData(dividing);
		for (JecnBaseDividingLinePanel jecnBaseDividingLinePanel : diviLineList) {
			JecnAddFlowElementUnit.importAddDiviLine(jecnBaseDividingLinePanel);
		}
	}

	/**
	 * 设置连接线的开始图形和结束图形
	 * 
	 * @param panleList
	 *            List<JecnBaseFlowElementPanel>XML读取的所有图形元素
	 * @param manLine
	 *            XML读取的当前曼哈顿线段
	 */
	public void setManLineRefFigure(List<JecnBaseFlowElementPanel> panleList, JecnBaseManhattanLinePanel manLine) {
		if (panleList == null || manLine == null) {
			return;
		}
		JecnManhattanLineData lineData = manLine.getFlowElementData();
		if (lineData.getStartFigureUUID() == null || lineData.getEndFigureUUID() == null) {
			return;
		}
		// System.out.println("开始图形唯一标示 ：" + lineData.getStartFigureUUID());

		// System.out.println("结束图形唯一标示 ：" + lineData.getEndFigureUUID());
		for (JecnBaseFlowElementPanel flowElementPanel : panleList) {
			if (!(flowElementPanel instanceof JecnBaseFigurePanel)) {
				continue;
			}
			JecnFigureData figureData = (JecnFigureData) flowElementPanel.getFlowElementData();
			// System.out.println(figureData.getFlowElementUUID());
			if (lineData.getStartFigureUUID().equals(figureData.getFlowElementUUID())) {
				manLine.setStartFigure((JecnBaseFigurePanel) flowElementPanel);
			} else if (lineData.getEndFigureUUID().equals(figureData.getFlowElementUUID())) {
				manLine.setEndFigure((JecnBaseFigurePanel) flowElementPanel);
			}
		}
	}

	/**
	 * 读取XML figures节点的数据
	 */
	public List<JecnBaseFlowElementPanel> gainFiguresDate(NodeList figures) {
		// 存储图形的List
		List<JecnBaseFlowElementPanel> baseFlowElementList = new ArrayList<JecnBaseFlowElementPanel>();
		if (figures.getLength() > 0) {
			String figureType = "";
			for (int i = 0; i < figures.getLength(); i++) {
				NamedNodeMap attributes = figures.item(i).getAttributes();
				// 图形的标识
				Node figureTypeNode = attributes.getNamedItem("FigureType");
				if (figureTypeNode != null) {
					figureType = figureTypeNode.getNodeValue();
				}
				if ("KPFigure".equals(figureType)) {
					figureType = "KCPFigure";
				} else if ("RoleFigure".equals(figureType)) {
					// 判断是否为虚拟角色
					Node linkTypeNode = attributes.getNamedItem("LinkType");
					if (linkTypeNode != null) {
						String LinkType = linkTypeNode.getNodeValue();
						if (LinkType != null && !"".equals(LinkType)) {
							figureType = "VirtualRoleFigure";
						}
					}
				} else if ("Oval".equals(figureType)) { // 圆在地图中为 圆行，在流程图中为 子流程
					if (JecnDrawMainPanel.getMainPanel().getWorkflow().getFlowMapData().getMapType() == MapType.partMap) {
						figureType = "OvalSubFlowFigure";
					}
				} else if ("ANDFigure".equals(figureType)) { // and连接
					if (JecnDrawMainPanel.getMainPanel().getWorkflow().getFlowMapData().getMapType() == MapType.partMap) {
						figureType = "AndFigure";
					}
				}

				// 判断图形的标识是否为空
				if (DrawCommon.isNullOrEmtryTrim(figureType)) {
					JOptionPane.showMessageDialog(null, "读取的数据有误，请确认后在打开！", "提示", JOptionPane.ERROR_MESSAGE);
				}
				// 创建图形数据对象
				JecnFigureData figureData = JecnCreateFlowElement.createFigureData(MapElemType.valueOf(figureType));
				// 横向或纵向标识
				Node isXorYNode = attributes.getNamedItem("isXorY");
				if (isXorYNode != null) {
					String isXorY = isXorYNode.getNodeValue();
					boolean isHFlag = true;
					if ("1".equals(isXorY)) {
						isHFlag = false;
					}
					JecnDrawMainPanel.getMainPanel().getWorkflow().getFlowMapData().setHFlag(isHFlag);
				}

				// 图形上添加的字符
				Node figureTextNode = attributes.getNamedItem("FigureText");
				if (figureTextNode != null) {
					String figureText = figureTextNode.getNodeValue();
					figureData.setFigureText(figureText);
				}

				// 图形的位置点
				Point point = new Point();
				// 图形的X
				double poLongx = 0;
				// 图形的X轴
				Node poLongxNode = attributes.getNamedItem("PoLongx");
				if (poLongxNode != null) {
					if (!DrawCommon.isNullOrEmtryTrim(poLongxNode.getNodeValue())) {
						poLongx = Double.valueOf(poLongxNode.getNodeValue());
					}
				}
				// 图形的Y
				double poLongy = 0;
				// 图形的Y轴
				Node poLongyNode = attributes.getNamedItem("PoLongy");
				if (poLongyNode != null) {
					if (!DrawCommon.isNullOrEmtryTrim(poLongyNode.getNodeValue())) {
						poLongy = Double.valueOf(poLongyNode.getNodeValue());
					}
				}
				point.setLocation(poLongx < 1 ? 1 : poLongx, poLongy < 1 ? 1 : poLongy);

				// 图形的宽度
				Node widthNode = attributes.getNamedItem("Width");
				if (widthNode != null) {
					String width = widthNode.getNodeValue();
					if (!DrawCommon.isNullOrEmtryTrim(width)) {
						figureData.setFigureSizeX(Integer.valueOf(width));
					}

				}

				Node heightNode = attributes.getNamedItem("Height");
				// 图型的高度
				if (heightNode != null) {
					String height = heightNode.getNodeValue();
					if (!DrawCommon.isNullOrEmtryTrim(height)) {
						figureData.setFigureSizeY(Integer.valueOf(height));
					}
				}

				// 字体的大小
				Node fontSizeNode = attributes.getNamedItem("FontSize");
				if (fontSizeNode != null) {
					String fontSize = fontSizeNode.getNodeValue();
					if (!DrawCommon.isNullOrEmtryTrim(fontSize)) {
						figureData.setFontSize(Integer.valueOf(fontSize));
					}
				}
				// 图形的层级
				Node orderIndexNode = attributes.getNamedItem("OrderIndex");
				if (orderIndexNode != null) {
					String orderIndex = orderIndexNode.getNodeValue();
					if (!DrawCommon.isNullOrEmtryTrim(orderIndex)) {
						figureData.setZOrderIndex(Integer.valueOf(orderIndex));
						// 面板最大层级数加一
						getWorkflow().setMaxZOrderIndex(getWorkflow().getMaxZOrderIndex() + 2);
					}
				}

				// 不为自由文本框
				if (!MapElemType.FreeText.toString().equals(figureType)) {
					// 图形填充色效果标识
					Node backColorFlagNode = attributes.getNamedItem("BackColorFlag");

					String BackColorFlag = null;
					if (backColorFlagNode != null) {
						BackColorFlag = backColorFlagNode.getNodeValue();
						if (!DrawCommon.isNullOrEmtryTrim(BackColorFlag)) {
							if ("0".equals(BackColorFlag)) {
								figureData.setFillType(FillType.none);
							} else if ("1".equals(BackColorFlag)) {
								figureData.setFillType(FillType.one);
							} else {
								figureData.setFillType(FillType.two);
							}
						}
					}

					// 图形的填充颜色
					Node bgColorNode = attributes.getNamedItem("BGColor");
					if (bgColorNode != null) {
						String BGColor = bgColorNode.getNodeValue();
						if (!DrawCommon.isNullOrEmtryTrim(BGColor)) {
							String[] backColors = BGColor.split(",");
							if (backColors.length == 3) {// 单色效果
								Color newBackColor = new Color(Integer.parseInt(backColors[0]), Integer
										.parseInt(backColors[1]), Integer.parseInt(backColors[2]));
								figureData.setFillColor(newBackColor);

								if (DrawCommon.isNullOrEmtryTrim(BackColorFlag)) {
									figureData.setFillType(FillType.one);
								}
							} else if (backColors.length == 7) {// 双色效果+渐变方向
								Color oneBackColor = new Color(Integer.parseInt(backColors[0]), Integer
										.parseInt(backColors[1]), Integer.parseInt(backColors[2]));
								Color twoBackColor = new Color(Integer.parseInt(backColors[3]), Integer
										.parseInt(backColors[4]), Integer.parseInt(backColors[5]));
								figureData.setFillColor(oneBackColor);
								figureData.setChangeColor(twoBackColor);
								if (DrawCommon.isNullOrEmtryTrim(BackColorFlag)) {
									figureData.setFillType(FillType.two);
								}

								String colorChangType = backColors[6];
								if ("1".equals(colorChangType)) {
									// 向上倾斜
									figureData.setFillColorChangType(FillColorChangType.topTilt);
								} else if ("2".equals(colorChangType)) {
									// 向下倾斜
									figureData.setFillColorChangType(FillColorChangType.bottom);
								} else if ("3".equals(colorChangType)) {
									// 垂直
									figureData.setFillColorChangType(FillColorChangType.vertical);
								} else {
									// 水平
									figureData.setFillColorChangType(FillColorChangType.horizontal);
								}

							}
						} else {
							figureData.setFillType(FillType.none);
						}
					}
				}
				// 字体颜色
				Node fontColorNode = attributes.getNamedItem("FontColor");
				if (fontColorNode != null) {
					String fontColor = fontColorNode.getNodeValue();
					if (!DrawCommon.isNullOrEmtryTrim(fontColor)) {
						String[] backColors = fontColor.split(",");
						Color newBackColor = new Color(Integer.parseInt(backColors[0]),
								Integer.parseInt(backColors[1]), Integer.parseInt(backColors[2]));
						figureData.setFontColor(newBackColor);
					}
				}
				// 字体是否加粗0为正常，1为加粗，2为斜体
				Node fontBodyNode = attributes.getNamedItem("FontBody");
				if (fontBodyNode != null) {
					String fontBody = fontBodyNode.getNodeValue();
					if (!DrawCommon.isNullOrEmtryTrim(fontBody)) {
						figureData.setFontStyle(Integer.valueOf(fontBody));
					}
				}
				// 字体类型:默认为（宋体）
				Node fontTypeNode = attributes.getNamedItem("FontType");
				if (fontTypeNode != null) {
					String fontType = fontTypeNode.getNodeValue();
					if (!DrawCommon.isNullOrEmtryTrim(fontType)) {
						figureData.setFontName(fontType);
					}
				}
				// 设置连接ID
				Node linkflowIdNode = attributes.getNamedItem("LinkflowId");
				if (linkflowIdNode != null) {
					String LinkflowId = linkflowIdNode.getNodeValue();
				}
				// 图形ID唯一标识
				Node figureImageIdNode = attributes.getNamedItem("FigureImageId");
				if (figureImageIdNode != null) {
					String figureImageId = figureImageIdNode.getNodeValue();
					if (!DrawCommon.isNullOrEmtryTrim(figureImageId)) {
						figureData.setFlowElementUUID(figureImageId);
					}
				}
				// 字体位置
				Node fontPositionNode = attributes.getNamedItem("FontPosition");
				if (fontPositionNode != null) {
					String fontPosition = fontPositionNode.getNodeValue();
					if (!DrawCommon.isNullOrEmtryTrim(fontPosition)) {
						figureData.setFontPosition(Integer.valueOf(fontPosition));
					}
				}
				// 是否存在阴影
				Node havaShadowNode = attributes.getNamedItem("HavaShadow");
				if (havaShadowNode != null) {
					String favaShadow = havaShadowNode.getNodeValue();
					if (!DrawCommon.isNullOrEmtryTrim(favaShadow)) {
						if ("1".equals(favaShadow)) {
							figureData.setShadowsFlag(true);
						} else {
							figureData.setShadowsFlag(false);
						}
					}
				}
				// 旋转角度值
				Node circumgyrateNode = attributes.getNamedItem("Circumgyrate");
				if (circumgyrateNode != null) {
					String circumgyrate = circumgyrateNode.getNodeValue();
					if (!DrawCommon.isNullOrEmtryTrim(circumgyrate)) {
						if (figureType.equals(MapElemType.ReverseArrowhead.toString())) {// 返入
							// 2
							figureData.setCurrentAngle(reverseArrowhead(circumgyrate));
						} else if (figureType.equals(MapElemType.RightArrowhead.toString())) {// 返出
							figureData.setCurrentAngle(rightArrowhead(circumgyrate));
						} else if (figureType.equals(MapElemType.CommentText.toString())) {// 注释框
							if ("90.0".equals(circumgyrate)) {
								circumgyrate = "270.0";
							} else if ("270.0".equals(circumgyrate)) {
								circumgyrate = "90.0";
							}
							figureData.setCurrentAngle(Double.valueOf(circumgyrate));
						} else {
							figureData.setCurrentAngle(Double.valueOf(circumgyrate));
						}
					}
				}
				// 边框样式
				Node bodyLineNode = attributes.getNamedItem("BodyLine");
				if (bodyLineNode != null) {
					String bodyLine = bodyLineNode.getNodeValue();
					if (!DrawCommon.isNullOrEmtryTrim(bodyLine)) {
						figureData.setBodyStyle(Integer.valueOf(bodyLine));
					}
				}
				// 如果老版为阶段分隔符，默认为黑色
				if ("PartitionFigure".equals(figureType)) {
					figureData.setBodyColor(new Color(0, 0, 0));
				} else {// 边框颜色
					Node bodyColorNode = attributes.getNamedItem("BodyColor");
					if (bodyColorNode != null) {
						String bodyColor = bodyColorNode.getNodeValue();
						if (!DrawCommon.isNullOrEmtryTrim(bodyColor)) {
							String[] backColors = bodyColor.split(",");
							Color newBackColor = new Color(Integer.parseInt(backColors[0]), Integer
									.parseInt(backColors[1]), Integer.parseInt(backColors[2]));
							figureData.setBodyColor(newBackColor);
						}
					}
				}
				if (!MapElemType.FreeText.toString().equals(figureType)) {
					// 边框是否加粗
					Node frameBodyNode = attributes.getNamedItem("FrameBody");
					if (frameBodyNode != null) {
						String frameBody = frameBodyNode.getNodeValue();
						if (frameBody != null && !"".equals(frameBody)) {
							figureData.setBodyWidth(Integer.valueOf(frameBody));
						}
					}
				}
				// 字体显示为横排还是竖排
				Node isErectNode = attributes.getNamedItem("IsErect");
				if (isErectNode != null) {
					String isErect = isErectNode.getNodeValue();
					if (!DrawCommon.isNullOrEmtryTrim(isErect)) {
						if ("1".equals(isErect)) {
							figureData.setFontUpRight(true);
						} else {
							figureData.setFontUpRight(false);
						}
					}
				}
				// 是否显示3D
				Node figure3DNode = attributes.getNamedItem("Figure3D");
				if (figure3DNode != null) {
					String figure3D = figure3DNode.getNodeValue();
					if (!DrawCommon.isNullOrEmtryTrim(figure3D)) {
						if ("1".equals(figure3D)) {
							figureData.setFlag3D(true);
						} else {
							figureData.setFlag3D(false);
						}
					}
				}
				// 边框的深浅
				Node lineThicknessNode = attributes.getNamedItem("LineThickness");
				if (lineThicknessNode != null) {
					String lineThickness = lineThicknessNode.getNodeValue();
					if (!DrawCommon.isNullOrEmtryTrim(lineThickness)) {
						figureData.setBodyWidth(Double.valueOf(lineThickness).intValue());
					}
				}
				// 判断是否为活动
				if (DrawCommon.isActive(MapElemType.valueOf(figureType))) {
					JecnActiveData activeData = (JecnActiveData) figureData;
					// 判断是否为关键活动
					Node activityIdNode = attributes.getNamedItem("ActivityId");
					// 活动编号
					if (activityIdNode != null) {
						String activityId = activityIdNode.getNodeValue();
						activeData.setActivityNum(activityId);
					}
					// 活动说明
					Node activityShowNode = attributes.getNamedItem("ActivityShow");
					if (activityShowNode != null) {
						String activityShow = activityShowNode.getNodeValue();
						activeData.setAvtivityShow(activityShow);
					}
					// 关键活动
					Node avtivityShowAndControlNode = attributes.getNamedItem("AvtivityShowAndControl");
					if (avtivityShowAndControlNode != null) {
						String avtivityShowAndControl = avtivityShowAndControlNode.getNodeValue();
						activeData.setAvtivityShowAndControl(avtivityShowAndControl);
					}
				}
				// 判断是否为园图形
				// if (attributes.getNamedItem("ActivityShow") != null) {
				// String ActivityShow = attributes.getNamedItem(
				// "ActivityShow").getNodeValue();
				// }
				if (MapElemType.ModelFigure.toString().equals(figureType)) {// 如果为泳池
					// 双色
					figureData.setFillType(FillType.two);
					// 设置填充色
					figureData.setFillColor(new Color(169, 209, 208));
					figureData.setChangeColor(new Color(247, 251, 252));
					// 水平渐变
					figureData.setFillColorChangType(FillColorChangType.horizontal);
				}

				if ("CommentText".equals(figureType)) {// 如果为注释框
					JecnCommentLineData commentData = (JecnCommentLineData) figureData;
					// 获取连接线数据对象
					JecnBaseDivedingLineData lineData = new JecnBaseDivedingLineData(MapElemType.CommentLine);

					double startX = 0;
					double startY = 0;
					double endX = 0;
					double endY = 0;
					// 注释框小线段开始点
					Node getStartPosX = attributes.getNamedItem("getStartPosX");

					if (getStartPosX != null) {
						if (!DrawCommon.isNullOrEmtryTrim(getStartPosX.getNodeValue())) {
							startX = Double.valueOf(getStartPosX.getNodeValue());
						}
					}
					// 注释框小线段开始点
					Node getStartPosY = attributes.getNamedItem("getStartPosY");

					if (getStartPosY != null) {
						if (!DrawCommon.isNullOrEmtryTrim(getStartPosY.getNodeValue())) {
							startY = Double.valueOf(getStartPosY.getNodeValue());
						}
					}
					// 注释框小线段开始点
					Node getEndPosX = attributes.getNamedItem("getEndPosX");

					if (getEndPosX != null) {
						if (!DrawCommon.isNullOrEmtryTrim(getEndPosX.getNodeValue())) {
							endX = Double.valueOf(getEndPosX.getNodeValue());
						}
					}
					// 注释框小线段开始点
					Node getEndPosY = attributes.getNamedItem("getEndPosY");

					if (getEndPosY != null) {
						if (!DrawCommon.isNullOrEmtryTrim(getEndPosY.getNodeValue())) {
							endY = Double.valueOf(getEndPosY.getNodeValue());
						}
					}

					Point startPoint = new Point((int) startX, (int) startY);
					Point endPoint = new Point((int) endX, (int) endY);
					// 设置线段大小 记录线段原始状态下数值
					lineData.getDiviLineCloneable().initOriginalArributs(startPoint, endPoint,
							figureData.getBodyWidth(), getWorkflow().getWorkflowScale());

					commentData.setLineData(lineData);
				}

				// 判断是否为角色,是否设置主责岗位
				Node linkMainTypeNode = attributes.getNamedItem("LinkMainType");
				if (linkMainTypeNode != null) {
					String LinkMainType = linkMainTypeNode.getNodeValue();
				}
				JecnBaseFigurePanel baseFigureElement = (JecnBaseFigurePanel) JecnCreateFlowElement.getFlowFigure(
						MapElemType.valueOf(figureType), figureData);
				if (MapElemType.CommentText == figureData.getMapElemType()) {// 判断是否为注释框
					CommentText commentText = (CommentText) baseFigureElement;
					// 不执行旋转操作
					commentText.isDefault();
				}
				// 设置图形的位置
				baseFigureElement.setSizeAndLocation(point);
				// 把图形放入List中
				baseFlowElementList.add(baseFigureElement);

			}
		}
		return baseFlowElementList;
	}

	/**
	 * 
	 * 3.0以前老版反入计算方法
	 * 
	 * @param circumgyrate
	 * @return
	 */
	private double reverseArrowhead(String circumgyrate) {
		double cir = Math.abs(Double.valueOf(circumgyrate) + 90.0);
		if (cir >= 360.0) {
			cir = cir % 360.0;
		}
		return cir;

	}

	/**
	 * 
	 * 3.0以前老版反出计算方法
	 * 
	 * @param circumgyrate
	 * @return
	 */
	private double rightArrowhead(String circumgyrate) {
		double cir = Math.abs(Double.valueOf(circumgyrate) + 270.0);
		if (cir >= 360.0) {
			cir = cir % 360.0;
		}
		return cir;
	}

	/**
	 * NodeList Lines 读取XML 线的数据
	 * 
	 * @param attributes
	 *            读取XML 数据
	 */
	private void gainConnectingLine(NodeList manLines, List<JecnBaseFlowElementPanel> baseFlowElementList,
			Map<JecnBaseManhattanLinePanel, List<JecnManhattanRouterResultData>> manLineList) {
		// 存储线段的List
		for (int i = 0; i < manLines.getLength(); i++) {
			NamedNodeMap attributes = manLines.item(i).getAttributes();
			// 线的标识
			String lineFlag = "";
			Node figureTypeNode = attributes.getNamedItem("FigureType");
			if (figureTypeNode != null) {
				lineFlag = figureTypeNode.getNodeValue();
			}
			if (lineFlag == null) {
				return;
			}
			// 获取连接线的数据对象
			JecnManhattanLineData manhattanLineData = new JecnManhattanLineData(MapElemType.valueOf(lineFlag));

			// 线的宽度
			Node lineSizeNode = attributes.getNamedItem("LineSize");
			if (lineSizeNode != null) {
				String lineSize = lineSizeNode.getNodeValue();
				if (!DrawCommon.isNullOrEmtryTrim(lineSize)) {
					manhattanLineData.setBodyWidth(Integer.valueOf(lineSize));
				}
			} else {
				manhattanLineData.setBodyWidth(1);
			}

			// 开始图形的ID
			Node startFigureNode = attributes.getNamedItem("StartFigure");
			if (startFigureNode != null) {
				String startFigureId = startFigureNode.getNodeValue();
				if (!DrawCommon.isNullOrEmtryTrim(startFigureId)) {
					manhattanLineData.setStartFigureUUID(startFigureId);
				}
			}
			// 结束图形的ID
			Node endFigureNode = attributes.getNamedItem("EndFigure");
			if (endFigureNode != null) {
				String endFigureId = endFigureNode.getNodeValue();
				if (!DrawCommon.isNullOrEmtryTrim(endFigureId)) {
					manhattanLineData.setEndFigureUUID(endFigureId);
				}
			}
			// 获取开始点的边界点
			Node poLongxNode = attributes.getNamedItem("PoLongx");
			if (poLongxNode != null) {
				String poLongx = poLongxNode.getNodeValue();
				if (!DrawCommon.isNullOrEmtryTrim(poLongx)) {
					EditPointType editPointType = DrawCommon.getLineType(poLongx);
					manhattanLineData.setStartEditPointType(editPointType);
				}
			}
			// 获取结束点的边界点
			Node poLongyNode = attributes.getNamedItem("PoLongy");
			if (poLongyNode != null) {
				String poLongy = poLongyNode.getNodeValue();
				if (!DrawCommon.isNullOrEmtryTrim(poLongy)) {
					EditPointType editPointType = DrawCommon.getLineType(poLongy);
					manhattanLineData.setEndEditPointType(editPointType);
				}
			}
			// 边框颜色
			Node lineColorNode = attributes.getNamedItem("LineColor");
			if (lineColorNode != null) {
				String bodyColor = lineColorNode.getNodeValue();
				if (!DrawCommon.isNullOrEmtryTrim(bodyColor)) {
					String[] backColors = bodyColor.split(",");
					Color newBackColor = new Color(Integer.parseInt(backColors[0]), Integer.parseInt(backColors[1]),
							Integer.parseInt(backColors[2]));
					manhattanLineData.setBodyColor(newBackColor);
				}
			}
			// 获取层级
			Node orderIndexNode = attributes.getNamedItem("OrderIndex");
			if (orderIndexNode != null) {
				String orderIndex = orderIndexNode.getNodeValue();
				if (!DrawCommon.isNullOrEmtryTrim(orderIndex)) {
					manhattanLineData.setZOrderIndex(Integer.valueOf(orderIndex));
					// 面板最大层级数加一
					getWorkflow().setMaxZOrderIndex(getWorkflow().getMaxZOrderIndex() + 2);
				}
			}

			// 根据标识位获取连接线对象
			JecnBaseManhattanLinePanel baseManhattanLine = JecnPaintFigureUnit.getJecnBaseManhattanLinePanel(lineFlag,
					manhattanLineData);

			// 设置连接线的开始图形和结束图形
			setManLineRefFigure(baseFlowElementList, baseManhattanLine);

			// 获取小线段
			List<JecnManhattanRouterResultData> resultDataList = getManhattanRouterResultData((Element) manLines
					.item(i), baseManhattanLine);
			manLineList.put(baseManhattanLine, resultDataList);
		}
	}

	/**
	 * 获取线段下的小线段的集合
	 * 
	 * @param line
	 * @return
	 */
	private List<JecnManhattanRouterResultData> getManhattanRouterResultData(Element line,
			JecnBaseManhattanLinePanel linePanel) {
		NodeList lineSegments = line.getChildNodes();
		List<JecnManhattanRouterResultData> resultDatas = new ArrayList<JecnManhattanRouterResultData>();
		for (int k = 0; k < lineSegments.getLength(); k++) {
			NamedNodeMap lineSegmentAttributes = lineSegments.item(k).getAttributes();
			if (lineSegmentAttributes == null) {
				continue;
			}
			// 小线段开始点的X
			double startX = 0;
			Node startPtXNode = lineSegmentAttributes.getNamedItem("StartPtX");
			if (startPtXNode != null) {
				String startXstr = startPtXNode.getNodeValue();
				if (!DrawCommon.isNullOrEmtryTrim(startXstr)) {
					startX = Double.valueOf(startXstr) < 1 ? 1 : Double.valueOf(startXstr);
				}
			}
			// 小线段结束点的Y
			double startY = 0;
			Node startPtYNode = lineSegmentAttributes.getNamedItem("StartPtY");
			if (startPtYNode != null) {
				String startYstr = startPtYNode.getNodeValue();
				if (!DrawCommon.isNullOrEmtryTrim(startYstr)) {
					startY = Double.valueOf(startYstr) < 1 ? 1 : Double.valueOf(startYstr);
				}
			}

			// 小线段结束点的X
			double endX = 0;
			Node endPtXNode = lineSegmentAttributes.getNamedItem("EndPtX");
			if (endPtXNode != null) {
				String endXstr = endPtXNode.getNodeValue();
				if (!DrawCommon.isNullOrEmtryTrim(endXstr)) {
					endX = Double.valueOf(endXstr) < 1 ? 1 : Double.valueOf(endXstr);
				}
			}
			// 小线段结束点的Y
			double endY = 0;
			Node endPtYNode = lineSegmentAttributes.getNamedItem("EndPtY");
			if (endPtYNode != null) {
				String endYstr = endPtYNode.getNodeValue();
				if (!DrawCommon.isNullOrEmtryTrim(endYstr)) {
					endY = Double.valueOf(endYstr) < 1 ? 1 : Double.valueOf(endYstr);
				}
			}
			Point startPoint = new Point();
			startPoint.setLocation(startX, startY);

			Point endPoint = new Point();
			endPoint.setLocation(endX, endY);
			// 添加小线段到线段集合
			JecnManhattanRouterResultData data = new JecnManhattanRouterResultData();
			data.setStartPoint(startPoint);
			data.setEndPint(endPoint);
			resultDatas.add(data);
		}

		// 排序
		Point startFiguerPoint = linePanel.getStartFigure().getJecnFigureDataCloneable().getSelectedOriginalEditPoint(
				linePanel.getStartEditPointType());
		resultDatas = reformSegments(resultDatas, startFiguerPoint);
		return resultDatas;
	}

	/**
	 * 
	 * 排序线段 规定线的组合顺序开始--结束点(为下一线段的开始点)--结束点()
	 * 
	 */
	public List<JecnManhattanRouterResultData> reformSegments(List<JecnManhattanRouterResultData> oldLost, Point mStartP) {
		List<JecnManhattanRouterResultData> list = new ArrayList<JecnManhattanRouterResultData>();
		// 开始点位置方向
		Point newStartP = null;
		for (int i = 0; i < oldLost.size(); i++) {
			JecnManhattanRouterResultData lineSegment = oldLost.get(i);
			// 开始点
			Point startP = lineSegment.getStartPoint();
			// 结束点
			Point endP = lineSegment.getEndPint();
			if (startP.equals(mStartP)) {
				// 线段开始点和线的开始位置点相等
				list.add(lineSegment);
				newStartP = lineSegment.getEndPint();
				oldLost.remove(i);
				break;
			}
			if (endP.equals(mStartP)) {
				// 线段结束点和线的开始位置点相等
				lineSegment.setStartPoint(mStartP);
				lineSegment.setEndPint(startP);
				list.add(lineSegment);
				newStartP = lineSegment.getEndPint();
				oldLost.remove(i);
				break;
			}
		}
		if (newStartP != null) {
			reformSegmentsChild(oldLost, list, newStartP);
		}
		return list;
	}

	public void reformSegmentsChild(List<JecnManhattanRouterResultData> oldList,
			List<JecnManhattanRouterResultData> list, Point pStartP) {
		Point newStartP = null;
		for (int i = 0; i < oldList.size(); i++) {
			JecnManhattanRouterResultData lineSegment = oldList.get(i);
			Point startP = lineSegment.getStartPoint();
			Point endP = lineSegment.getEndPint();
			if (startP.equals(pStartP)) {
				list.add(lineSegment);
				newStartP = lineSegment.getEndPint();
				oldList.remove(i);
				break;
			}
			if (endP.equals(pStartP)) {
				lineSegment.setStartPoint(pStartP);
				lineSegment.setEndPint(startP);
				// 更换
				// stsData(lineSegment);
				oldList.remove(i);
				newStartP = lineSegment.getEndPint();
				list.add(lineSegment);
				break;
			}
		}
		if (newStartP != null) {
			reformSegmentsChild(oldList, list, newStartP);
		}
	}

	/**
	 * 获取横分割线
	 */
	private void gainParallelLineData(Document doc) {
		// 横分割线
		NodeList parallelLines = doc.getElementsByTagName("ParallelLines");
		for (int i = 0; i < parallelLines.getLength(); i++) {
			JecnVHLineData vhLineData = new JecnVHLineData(MapElemType.ParallelLines, LineDirectionEnum.horizontal);
			NamedNodeMap attributes = parallelLines.item(i).getAttributes();
			double poLongy = 0;
			Node poLongyNode = attributes.getNamedItem("PoLongy");
			if (poLongyNode != null) {
				String poLongyStr = poLongyNode.getNodeValue();
				if (!DrawCommon.isNullOrEmtryTrim(poLongyStr)) {
					poLongy = Double.valueOf(poLongyStr) < 1 ? 1 : Double.valueOf(poLongyStr);
				}
			}
			vhLineData.setZOrderIndex(getWorkflow().getMaxZOrderIndex());
			ParallelLines parallelLine = new ParallelLines(vhLineData);
			JecnAddFlowElementUnit.addVHLine(parallelLine, new Point(0, (int) poLongy));
			// 面板最大层级数加一
			getWorkflow().setMaxZOrderIndex(getWorkflow().getMaxZOrderIndex() + 2);
		}

	}

	/**
	 * 获取竖分割线
	 */
	private void gainVerticalLineData(Document doc) {
		// 竖分割线
		NodeList verticalLines = doc.getElementsByTagName("VerticalLines");
		for (int i = 0; i < verticalLines.getLength(); i++) {
			JecnVHLineData vhLineData = new JecnVHLineData(MapElemType.VerticalLine, LineDirectionEnum.vertical);
			NamedNodeMap attributes = verticalLines.item(i).getAttributes();
			double poLongx = 0;
			Node poLongxNode = attributes.getNamedItem("PoLongx");
			if (poLongxNode != null) {
				String poLongxStr = poLongxNode.getNodeValue();
				if (!DrawCommon.isNullOrEmtryTrim(poLongxStr)) {
					poLongx = Double.valueOf(poLongxStr) < 1 ? 1 : Double.valueOf(poLongxStr);
				}
			}
			vhLineData.setZOrderIndex(getWorkflow().getMaxZOrderIndex());
			VerticalLine verticalLine = new VerticalLine(vhLineData);
			JecnAddFlowElementUnit.addVHLine(verticalLine, new Point((int) poLongx, 0));
			verticalLine.reSetFlowElementZorder();
			// 面板最大层级数加一
			getWorkflow().setMaxZOrderIndex(getWorkflow().getMaxZOrderIndex() + 2);
		}

	}

	/**
	 * 获取待添加的分割线
	 * 
	 * @param baseDivedingLine
	 *            分割线节点
	 * @return List<JecnBaseDividingLinePanel>添加的分割线集合
	 */
	public List<JecnBaseDividingLinePanel> getBaseDivedingLineData(NodeList baseDivedingLine) {
		List<JecnBaseDividingLinePanel> baseDivedingLineList = new ArrayList<JecnBaseDividingLinePanel>();
		// 存储线段的List
		for (int i = 0; i < baseDivedingLine.getLength(); i++) {
			JecnBaseDivedingLineData baseDivedingLineData = null;
			NamedNodeMap attributes = baseDivedingLine.item(i).getAttributes();
			if (attributes == null) {
				continue;
			}
			// 线条的标识
			String figureType = null;
			Node figureTypeNode = attributes.getNamedItem("FigureType");
			if (figureTypeNode != null) {
				figureType = figureTypeNode.getNodeValue();
				if (!DrawCommon.isNullOrEmtryTrim(figureType)) {
					baseDivedingLineData = new JecnBaseDivedingLineData(MapElemType.valueOf(figureType));
				}
			}
			if (baseDivedingLineData == null) {
				return null;
			}
			// 开始点X
			int startX = 0;
			Node startXNode = attributes.getNamedItem("StartFigure");
			if (startXNode != null) {
				String startXStr = startXNode.getNodeValue();
				if (!DrawCommon.isNullOrEmtryTrim(startXStr)) {
					startX = Integer.valueOf(startXStr) < 1 ? 1 : Integer.valueOf(startXStr);
				}
			}
			int startY = 0;
			Node startYNode = attributes.getNamedItem("EndFigure");
			if (startYNode != null) {
				String startYStr = startYNode.getNodeValue();
				if (!DrawCommon.isNullOrEmtryTrim(startYStr)) {
					startY = Integer.valueOf(startYStr) < 1 ? 1 : Integer.valueOf(startYStr);
				}
			}

			// 结束点
			int endX = 0;
			Node endXNode = attributes.getNamedItem("PoLongx");
			if (endXNode != null) {
				String endXStr = endXNode.getNodeValue();
				if (!DrawCommon.isNullOrEmtryTrim(endXStr)) {
					endX = Integer.valueOf(endXStr) < 1 ? 1 : Integer.valueOf(endXStr);
				}
			}
			int endY = 0;
			Node endYNode = attributes.getNamedItem("PoLongy");
			if (endYNode != null) {
				String endYStr = endYNode.getNodeValue();
				if (!DrawCommon.isNullOrEmtryTrim(endYStr)) {
					endY = Integer.valueOf(endYStr) < 1 ? 1 : Integer.valueOf(endYStr);
				}
			}
			// 线条颜色
			Node lineColorNode = attributes.getNamedItem("LineColor");
			if (lineColorNode != null) {
				String lineColor = lineColorNode.getNodeValue();
				if (!DrawCommon.isNullOrEmtryTrim(lineColor)) {
					String[] backColors = lineColor.split(",");
					Color newBackColor = new Color(Integer.parseInt(backColors[0]), Integer.parseInt(backColors[1]),
							Integer.parseInt(backColors[2]));
					baseDivedingLineData.setBodyColor(newBackColor);
				}
			}

			// 线条大小
			int lineSize = 0;
			Node lineSizeNode = attributes.getNamedItem("LineSize");
			if (lineSizeNode != null) {
				String lineSizeStr = lineSizeNode.getNodeValue();
				if (!DrawCommon.isNullOrEmtryTrim(lineSizeStr)) {
					lineSize = Integer.valueOf(lineSizeStr);
				}
			}
			baseDivedingLineData.setBodyWidth(lineSize);

			// 边框样式
			Node bodyLineNode = attributes.getNamedItem("BodyLine");
			if (bodyLineNode != null) {
				String bodyLine = bodyLineNode.getNodeValue();
				if (!DrawCommon.isNullOrEmtryTrim(bodyLine)) {
					baseDivedingLineData.setBodyStyle(Integer.valueOf(bodyLine));
				}
			}

			JecnBaseDividingLinePanel dividingLinePanel = getJecnBaseDiviLinePanel(baseDivedingLineData);

			Point startPoint = new Point(startX, startY);
			Point endPoint = new Point(endX, endY);
			if (dividingLinePanel instanceof HDividingLine || dividingLinePanel instanceof VDividingLine) {
				// 记录100%状态下位置点
				dividingLinePanel.reSetAttributes(startPoint, endPoint);
				// 设置图形位置点
				dividingLinePanel.setBounds(startPoint, endPoint);
			}
			if (dividingLinePanel instanceof DividingLine) {// 如果添加的是分割线，记录分割线鼠标点击和释放的坐标值
				DividingLine dividingLine = (DividingLine) dividingLinePanel;
				// 记录分割线100%状态下的克隆值
				dividingLine.originalCloneData(startPoint, endPoint);
			}
			baseDivedingLineList.add(dividingLinePanel);
		}
		return baseDivedingLineList;
	}

	/**
	 * 根据标识位获取连接线对象
	 * 
	 * @param lineFlag
	 * @param manhattanLineData
	 * @return
	 */
	private JecnBaseDividingLinePanel getJecnBaseDiviLinePanel(JecnBaseDivedingLineData diviLineData) {
		switch (diviLineData.getMapElemType()) {
		case DividingLine:// 分隔符（分割线）
			return new DividingLine(diviLineData);
		case HDividingLine:// 分隔符（分割线）
			return new HDividingLine(diviLineData);
		case VDividingLine:// 分隔符（分割线）
			return new VDividingLine(diviLineData);
		}
		return null;
	}

	/**
	 * 获取面板
	 * 
	 * @return 当前面板
	 */
	private JecnDrawDesktopPane getWorkflow() {
		return JecnDrawMainPanel.getMainPanel().getWorkflow();
	}
}
