package com.jecn.epros.server.service.email;

import java.util.List;

import com.jecn.epros.server.common.IBaseService;
import com.jecn.epros.server.email.bean.JecnEmail;
import com.jecn.epros.server.email.buss.EmailMessageBean;
import com.jecn.epros.server.emailnew.EmailBasicInfo;

public interface IEmailService extends IBaseService<JecnEmail, String> {

	/**
	 * 保存邮件信息
	 * @param basicInfos 邮件基本信息集合
	 * @throws Exception
	 */
	public List<EmailMessageBean> saveEmail(List<EmailBasicInfo> basicInfos) throws Exception;

	/**
	 * 需要发送的邮件key为inner或者outer分别表示内网或者外网
	 * @return
	 * @throws Exception
	 */
	public List<EmailMessageBean> getEmailMessageList()
			throws Exception;

	/**
	 * 更新邮件发送状态
	 * @param state 0未发送，1已发送，2发送失败
	 * @param emailId 邮件主表id
	 * @throws Exception
	 */
	public void updateEmailSendState(int state, String emailId) throws Exception;

	/**
	 *  更新邮件的发送成功的信息
	 * @param emailId 邮件主表id
	 * @throws Exception
	 */
	public void updateEmailSuccessState(String emailId) throws Exception;

}
