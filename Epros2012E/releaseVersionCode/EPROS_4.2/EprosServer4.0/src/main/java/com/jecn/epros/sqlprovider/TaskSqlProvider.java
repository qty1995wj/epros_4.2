package com.jecn.epros.sqlprovider;

import com.jecn.epros.domain.task.TaskApprovePeopleConn;
import com.jecn.epros.domain.task.TaskForRecodeNew;
import com.jecn.epros.domain.task.TaskPeopleNew;
import com.jecn.epros.domain.task.TaskSearchTempBean;
import com.jecn.epros.security.AuthenticatedUserUtil;
import com.jecn.epros.security.JecnContants;
import com.jecn.epros.service.web.Common.IfytekCommon;
import com.jecn.epros.service.web.parameter.IflytekParameter;
import com.jecn.epros.sqlprovider.server3.Common;
import com.jecn.epros.sqlprovider.server3.SqlCommon;
import com.jecn.epros.sqlprovider.server3.SqlConstant;
import com.jecn.epros.util.ConfigUtils;
import org.apache.commons.lang3.StringUtils;

import java.util.Map;

public class TaskSqlProvider extends BaseSqlProvider {

    public String fetchTasks(Map<String, Object> parmsMap) {

        TaskSearchTempBean taskSearchTempBean = (TaskSearchTempBean) parmsMap.get("taskSearchTempBean");
        Long peopleId = (Long) parmsMap.get("curPeopleId");
        Long roleId = (Long) parmsMap.get("roleId");
        Boolean isAdmin = (Boolean) parmsMap.get("isAdmin");
        Boolean isSecondAdmin = (Boolean) parmsMap.get("isSecondAdmin");

        String searchSql = getSearchSql(taskSearchTempBean, roleId, peopleId, isAdmin, isSecondAdmin, true);
        int languageType = AuthenticatedUserUtil.isLanguageType();

        String sql = "SELECT ALL_TASK.ID TASK_ID," + "               ALL_TASK.TASK_NAME,"
                + "               ALL_TASK.STATE," + "               ALL_TASK.TASK_ELSE_STATE,"
                + "               ALL_TASK.TASK_TYPE," + "               ALL_TASK.CREATE_PERSON_ID,"
                + "               JU." + IfytekCommon.getUserField() + ",               ALL_TASK.UPDATE_TIME,"
                + "               ALL_TASK.START_TIME," + "               ALL_TASK.END_TIME,"
                + "               ALL_TASK.UP_STATE," + "               ALL_TASK.FROM_PERSON_ID,"
                + " CASE WHEN JTS.STAGE_NAME IS NULL " + " AND ALL_TASK.STATE = 5 THEN" + "    " +
                (languageType == 0 ? "    '完成'" : "    'END'")
                + "                ELSE" + "             " +
                (languageType == 0 ? "    JTS.STAGE_NAME " : "    JTS.EN_NAME" + "  ") +
                "    END AS STAGE_NAME,"
                + getCaseResPeople() + " ,CASE" +
                "                 WHEN jthn.PUBLISH_DATE is null " +
                " then " +
                " ALL_TASK.UPDATE_TIME " +
                " else" +
                " jthn.PUBLISH_DATE end PUBLISH_DATE,ALL_TASK.approve_Type" + " from (select * from(" + searchSql + ") a"
                + getDelayCondityonSql(taskSearchTempBean.isDelayTask()) + " where a.is_lock = 1)ALL_TASK"
                + " LEFT JOIN JECN_TASK_STAGE JTS ON JTS.TASK_ID=ALL_TASK.ID AND JTS.STAGE_MARK=ALL_TASK.STATE"
                + " left join jecn_task_history_new jthn on jthn.task_id=all_task.id"
                + " LEFT JOIN JECN_USER JU ON JU.PEOPLE_ID=ALL_TASK.CREATE_PERSON_ID" + addResPeopleType(); // 添加流程责任人

        String resultSql = getTaskResPeoleSql(sql);
        return resultSql;
    }

    public String updateTaskPeopleConn(Map<String, Object> parmsMap) {

        Long fromPeopleId = (Long) parmsMap.get("fromPeopleId");
        Long toPeopleId = (Long) parmsMap.get("toPeopleId");

        String sql = "update jecn_task_people_conn_new  set approve_pid=" + toPeopleId + " where id in("
                + " select jtpn.id from jecn_task_people_conn_new jtpn"
                + " inner join jecn_task_stage jts on jtpn.stage_id=jts.id"
                + " inner join jecn_task_bean_new jtbn on jts.task_id=jtbn.id" + " where " + " jtpn.approve_pid="
                + fromPeopleId + " and jtbn.state <>5" + " )";

        return sql;
    }

    public String updateTaskPeople(Map<String, Object> parmsMap) {

        Long fromPeopleId = (Long) parmsMap.get("fromPeopleId");
        Long toPeopleId = (Long) parmsMap.get("toPeopleId");

        // 更新当前阶段审批人表
        String sql = "update jecn_task_people_new  set approve_pid=" + toPeopleId + " where approve_pid="
                + fromPeopleId;

        return sql;
    }

    public String updateTaskCreatePeople(Map<String, Object> parmsMap) {

        Long fromPeopleId = (Long) parmsMap.get("fromPeopleId");
        Long toPeopleId = (Long) parmsMap.get("toPeopleId");

        // 更新任务主表 如果来源人是拟稿人
        String sql = " update jecn_task_bean_new  set create_person_id=" + toPeopleId + " where create_person_id="
                + fromPeopleId + " and state <>5 ";

        return sql;
    }

    public String updateTaskFromPeople(Map<String, Object> parmsMap) {

        Long fromPeopleId = (Long) parmsMap.get("fromPeopleId");
        Long toPeopleId = (Long) parmsMap.get("toPeopleId");

        // 更新任务主表 如果目标人是想要替换的人
        String sql = " update jecn_task_bean_new  set from_person_id=" + toPeopleId + " where from_person_id="
                + fromPeopleId + " and state <>5 ";

        return sql;
    }

    private String getDelayCondityonSql(boolean isDelay) {
        if (!isDelay) {
            return "";
        }
        String sql = " inner join (select jts.task_id " + " from jecn_task_stage jts "
                + " where jts.is_selected_user = 1 " + " AND JTS.STAGE_MARK != 10 " + " and (SELECT COUNT(JTPCN.id) "
                + " FROM JECN_TASK_PEOPLE_CONN_NEW  JTPCN, "
                + " jecn_user                  ju " + " WHERE JTPCN.STAGE_ID = jts.id "
                + " and JTPCN.approve_pid = ju.people_id "
                + " and ju.islock = 0) = 0 " + " union " + " select jts.task_id " + " from jecn_task_stage jts "
                + " where jts.is_selected_user = 1 " + " AND JTS.STAGE_MARK = 3 "
                + " and (SELECT COUNT(distinct JTPCN.approve_pid) " + " FROM JECN_TASK_PEOPLE_CONN_NEW  JTPCN, "
                + " jecn_user                  ju "
                + " WHERE JTPCN.STAGE_ID = jts.id " + " and JTPCN.approve_pid = ju.people_id " + " and ju.islock = 0) <> " + " (select COUNT(JTPCN.id) "
                + " FROM JECN_TASK_PEOPLE_CONN_NEW JTPCN " + " WHERE JTPCN.STAGE_ID = jts.id)" + " union " // 最新的操作人没有
                + " select distinct jtbn.id" + " from jecn_task_bean_new jtbn" + " left join jecn_task_people_new jtpn "
                + " on jtbn.id =jtpn.task_id" + " left join jecn_user u "
                + " on jtpn.approve_pid =u.people_id" + " where (jtpn.id is null or u.people_id is null or u.islock=1)"
                + "  and jtbn.state <> 5" + "  and jtbn.is_lock = 1" + " )"
                + " tmp on a.id = tmp.task_id AND A.STATE<>5";
        return sql;
    }

    public String getTaskManageCountBySearch(Map<String, Object> parmsMap) {

        TaskSearchTempBean taskSearchTempBean = (TaskSearchTempBean) parmsMap.get("taskSearchTempBean");
        Long peopleId = (Long) parmsMap.get("curPeopleId");
        Long roleId = (Long) parmsMap.get("roleId");
        Boolean isAdmin = (Boolean) parmsMap.get("isAdmin");
        Boolean isSecondAdmin = (Boolean) parmsMap.get("isSecondAdmin");

        // 拼装查询条件
        String searchSql = getSearchSql(taskSearchTempBean, roleId, peopleId, isAdmin, isSecondAdmin, true);
        // 根据查询条件获取返回的总数
        // 如果搁置任务
        String resultSql = "select  count(a.id) from (" + searchSql + ") a "
                + getDelayCondityonSql(taskSearchTempBean.isDelayTask());
        return resultSql;

    }

    public String getMyTaskCount(Map<String, Object> parmsMap) {

        TaskSearchTempBean taskSearchTempBean = (TaskSearchTempBean) parmsMap.get("taskSearchTempBean");
        Long loginPeopleId = (Long) parmsMap.get("curPeopleId");
        Boolean isAppMesTrue = (Boolean) parmsMap.get("isAppMesTrue");

        String sql = getMyTaskSql(loginPeopleId, isAppMesTrue, taskSearchTempBean, true);

        return sql;

    }

    public String fetchMyTasks(Map<String, Object> paramMap) {

        TaskSearchTempBean taskSearchTempBean = (TaskSearchTempBean) paramMap.get("taskSearchTempBean");
        Long loginPeopleId = (Long) paramMap.get("curPeopleId");
        Boolean isAppMesTrue = (Boolean) paramMap.get("isAppMesTrue");

        String sql = getMyTaskSql(loginPeopleId, isAppMesTrue, taskSearchTempBean, false);

        return sql;

    }

    private String getMyTaskSql(Long loginPeopleId, Boolean isAppMesTrue, TaskSearchTempBean taskSearchTempBean,
                                boolean isCount) {

        String taskSql = getSearchSql(taskSearchTempBean, null, loginPeopleId, false, false, false);
        String conditionSql = "";
        if (isCount) {
            conditionSql = "select count(1)";
        } else {
            conditionSql = "select " + " ALL_TASK.ID TASK_ID," + " ALL_TASK.TASK_NAME," + " ALL_TASK.STATE,"
                    + " ALL_TASK.TASK_ELSE_STATE," + " ALL_TASK.TASK_TYPE," + " ALL_TASK.TRUE_NAME,"
                    + " ALL_TASK.CREATE_PERSON_ID," + " ALL_TASK.UPDATE_TIME," + " ALL_TASK.START_TIME,"
                    + " ALL_TASK.END_TIME," + " ALL_TASK.FROM_PERSON_ID," + " ALL_TASK.UP_STATE," + "STAGE_NAME,EN_NAME,"
                    + getCaseResPeople() + ",null publish_date, approve_Type";
        }

        String sql = conditionSql + " from (" // 下边的是主要的语句将所有数据作为子查询数据
                + "         SELECT A.ID," + "                A.TASK_NAME," + "                A.STATE,"
                + "                A.TASK_ELSE_STATE," + "                A.TASK_TYPE," + "                C.TRUE_NAME,"
                + "                A.CREATE_PERSON_ID," + "                A.UPDATE_TIME,"
                + "                A.start_time," + "                A.end_time," + "                A.FROM_PERSON_ID,"
                + "                A.UP_STATE," + "				   JTS.STAGE_NAME,JTS.EN_NAME," + "				   A.R_ID,A.approve_Type "
                + "           FROM (" + taskSql + ")A"
                + "			 INNER JOIN JECN_TASK_STAGE JTS ON A.ID=JTS.TASK_ID AND A.STATE=JTS.STAGE_MARK"
                + "          INNER JOIN JECN_TASK_PEOPLE_NEW B ON A.ID = B.TASK_ID"
                + "                                           AND B.APPROVE_PID = " + loginPeopleId
                + "          INNER JOIN JECN_USER C ON A.CREATE_PERSON_ID = C.PEOPLE_ID"
                + "          WHERE A.STATE <> 5" + "         UNION" + "         SELECT A.ID,"
                + "                A.TASK_NAME," + "                A.STATE," + "                A.TASK_ELSE_STATE,"
                + "                A.TASK_TYPE," + "                C.TRUE_NAME,"
                + "                A.CREATE_PERSON_ID," + "                A.UPDATE_TIME,"
                + "                A.start_time," + "                A.end_time," + "                A.FROM_PERSON_ID,"
                + "                A.UP_STATE," + "				   JTS.STAGE_NAME,JTS.EN_NAME," + "				   A.R_ID,A.approve_Type "
                + "           FROM (" + taskSql + ")A"
                + "			 INNER JOIN JECN_TASK_STAGE JTS ON A.ID=JTS.TASK_ID AND A.STATE=JTS.STAGE_MARK"
                + "          INNER JOIN JECN_USER C ON A.CREATE_PERSON_ID = " + loginPeopleId
                + "                                AND C.PEOPLE_ID = A.CREATE_PERSON_ID"
                + "          WHERE A.STATE <> 5";

        // 是否存在撤回
        if (isAppMesTrue) {
            sql = sql + reBackString(loginPeopleId, taskSql);
        }
        // 根据任务获取流程责任人类型，责任人ID
        sql = sql + ") ALL_TASK";

        if (!isCount) {
            sql += addResPeopleType();
            // 根据任务结果获取任务相关信息，责任人类型获取责任人
            sql = getMyTaskResPeoleSql(sql);
        }

        return sql;
    }

    /**
     * 存在撤回操作，获取源人
     *
     * @param loginPeopleId
     * @return
     */
    private String reBackString(long loginPeopleId, String taskSql) {
        return "         UNION" + "         SELECT A.ID," + "                A.TASK_NAME," + "                A.STATE,"
                + "                A.TASK_ELSE_STATE," + "                A.TASK_TYPE," + "                C.TRUE_NAME,"
                + "                A.CREATE_PERSON_ID," + "                A.UPDATE_TIME,"
                + "                A.start_time," + "                A.end_time," + "                A.FROM_PERSON_ID,"
                + "                A.UP_STATE," + "				   JTS.STAGE_NAME,JTS.EN_NAME," + "				   A.R_ID,A.approve_Type"
                + "           FROM (" + taskSql + ") A"
                + "			 INNER JOIN JECN_TASK_STAGE JTS ON A.ID=JTS.TASK_ID AND A.STATE=JTS.STAGE_MARK"
                + "          INNER JOIN JECN_USER C ON A.FROM_PERSON_ID = " + loginPeopleId
                + "                                AND C.PEOPLE_ID = A.CREATE_PERSON_ID"
                + "          WHERE A.STATE <> 5 AND (A.STATE <> 10 or A.TASK_ELSE_STATE=9)"// 整理意见阶段的操作人只能为创建人
                // 审批记录MAX值唯一， 用in 子查询太慢，写的MAX
                + "            AND A.FROM_PERSON_ID =" + "                               (SELECT MAX(FR.FROM_PERSON_ID)"
                + "                                  FROM JECN_TASK_FOR_RECODE_NEW FR"
                + "                                 WHERE FR.UP_STATE < 10 and FR.UP_STATE<>3 and FR.SORT_ID ="
                + "                                       (SELECT MAX(TR.SORT_ID)"
                + "                                          FROM JECN_TASK_FOR_RECODE_NEW TR"
                + "                                         WHERE TR.TASK_ID = A.ID) AND FR.TASK_ID = A.ID)";
    }

    /**
     * 查询责任人case判断
     *
     * @return
     */
    private String getCaseResPeople() {
        String value = getSqlValueDefault();
        return "               CASE" + "                 WHEN TASK_TYPE = 0 THEN" + value + "(FBIT.RES_PEOPLE_ID, -1)"
                + "                 WHEN TASK_TYPE = 2 THEN" + value + "(JRT.RES_PEOPLE_ID, -1)"
                + "                 WHEN TASK_TYPE = 3 THEN" + value + "(JRT.RES_PEOPLE_ID, -1)"
                + "                 ELSE" + "                  -1" + "               END RES_PEOPLE_ID,"
                + "               CASE" + "                 WHEN TASK_TYPE = 0 THEN" + value
                + "(FBIT.TYPE_RES_PEOPLE, -1)" + "                 WHEN TASK_TYPE = 2 THEN" + value
                + "(JRT.TYPE_RES_PEOPLE, -1)" + "                 WHEN TASK_TYPE = 3 THEN" + value
                + "(JRT.TYPE_RES_PEOPLE, -1)" + "                 ELSE" + "                  -1"
                + "               END TYPE_RES_PEOPLE";
    }

    private String addResPeopleType() {
        return " LEFT JOIN JECN_FLOW_BASIC_INFO_T FBIT" + "           ON ALL_TASK.R_ID = FBIT.FLOW_ID"
                + "         LEFT JOIN JECN_RULE_T JRT" + "           ON ALL_TASK.R_ID = JRT.ID"
                + "         LEFT JOIN JECN_FILE_T JFT" + "           ON ALL_TASK.R_ID = JFT.FILE_ID";
    }

    /**
     * 任务中相关责任人查询 我的任务中
     *
     * @param sql
     * @return
     */
    private String getMyTaskResPeoleSql(String sql) {
        String resSql = "SELECT TASK_ID," + "       TASK_NAME," + "       STATE," + "       TASK_ELSE_STATE,"
                + "       TASK_TYPE," + "       A.TRUE_NAME," + "       CREATE_PERSON_ID," + "       UPDATE_TIME,"
                + "       START_TIME," + "       END_TIME," + "       FROM_PERSON_ID," + "       UP_STATE,"
                + "       STAGE_NAME,EN_NAME," + "       CASE" + "         WHEN TYPE_RES_PEOPLE = 0 THEN"
                + "          JU.TRUE_NAME" + "         WHEN TYPE_RES_PEOPLE = 1 THEN" + "          OI.FIGURE_TEXT"
                + "       END RES_PEOPLE" + " ,publish_date ,approve_Type" + "  FROM (" + sql + ") A" + "  LEFT JOIN JECN_USER JU"
                + "    ON A.RES_PEOPLE_ID = JU.PEOPLE_ID" + "  LEFT JOIN JECN_FLOW_ORG_IMAGE OI"
                + "    ON A.RES_PEOPLE_ID = OI.FIGURE_ID ORDER BY A.UPDATE_TIME DESC";

        return resSql;
    }

    /**
     * 任务中相关责任人查询
     *
     * @param sql
     * @return
     */
    private String getTaskResPeoleSql(String sql) {
        String resSql = "SELECT TASK_ID," + "       TASK_NAME," + "       STATE," + "       TASK_ELSE_STATE,"
                + "       TASK_TYPE," + "       A." + IfytekCommon.getUserField() + ",       CREATE_PERSON_ID," + "       UPDATE_TIME,"
                + "       START_TIME," + "       END_TIME," + "       FROM_PERSON_ID," + "       UP_STATE,"
                + "       STAGE_NAME," + "       CASE" + "         WHEN TYPE_RES_PEOPLE = 0 THEN"
                + "   JU." + IfytekCommon.getUserField() + "        WHEN TYPE_RES_PEOPLE = 1 THEN" + "          OI.FIGURE_TEXT"
                + "       END RES_PEOPLE" + " ,publish_date ,approve_Type,TYPE_RES_PEOPLE " + "  FROM (" + sql + ") A" + "  LEFT JOIN JECN_USER JU"
                + "    ON A.RES_PEOPLE_ID = JU.PEOPLE_ID" + "  LEFT JOIN JECN_FLOW_ORG_IMAGE OI"
                + "    ON A.RES_PEOPLE_ID = OI.FIGURE_ID ORDER BY A.UPDATE_TIME DESC";

        return resSql;
    }

    private String getSearchSql(TaskSearchTempBean searchTempBean, Long roleId, Long peopleId, boolean isAdmin,
                                boolean isSecondAdmin, boolean isTaskManage) {

        String searchSql = "";

        if (isTaskManage) {

            if (isAdmin) {// 管理员

                String condition = getTaskTypeCondition(searchTempBean);
                searchSql = " select a.* from  jecn_task_bean_new a where a.is_lock=1 " + condition;

            } else if (isSecondAdmin) {// 二级管理员
                if (searchTempBean.getTaskType() != -1) {// 任务类型
                    searchSql = getSecondAdminTaskSqlByTaskType(roleId, peopleId, searchTempBean.getTaskType());
                } else {
                    searchSql = getSecondAdminTaskSql(roleId, peopleId);
                }
            } else {// 普通人员

                String condition = getTaskTypeCondition(searchTempBean);

                searchSql = " select a.* from  jecn_task_bean_new a where a.is_lock=1 " + condition
                        + " AND EXISTS (SELECT 1 FROM JECN_TASK_FOR_RECODE_NEW FRN WHERE FRN.TASK_ID = a.ID AND FRN.TO_PERSON_ID = "
                        + peopleId + ")";

            }

        } else {

            String condition = getTaskTypeCondition(searchTempBean);
            searchSql = " select a.* from  jecn_task_bean_new a where a.is_lock=1 " + condition;
        }
        if (!isAdmin && isSecondAdmin) {
            if (searchTempBean.getTaskStage() != -1) {// 任务阶段
                searchSql = searchSql + " where  A.state = " + searchTempBean.getTaskStage();
            }
            if (StringUtils.isNotBlank(searchTempBean.getTaskName()) || searchTempBean.getTaskStage() != -1) {// 任务名称模糊查询
                searchSql = searchSql + " where A.task_name like " + "'%" + searchTempBean.getTaskName() + "%'";
            } else if (StringUtils.isNotBlank(searchTempBean.getTaskName()) && searchTempBean.getTaskStage() != -1) {
                searchSql = searchSql + " and A.task_name like " + "'%" + searchTempBean.getTaskName() + "%'";
            }
            if (searchTempBean.getCreatePeopleId() != null && (searchTempBean.getTaskStage() != -1 || StringUtils.isNotBlank(searchTempBean.getTaskName()))) {// 创建人模糊查询
                searchSql = searchSql + " where  A.CREATE_PERSON_ID = " + searchTempBean.getCreatePeopleId();
            } else if (searchTempBean.getCreatePeopleId() != null && (searchTempBean.getTaskStage() != -1 || StringUtils.isNotBlank(searchTempBean.getTaskName()))) {
                searchSql = searchSql + " and  A.CREATE_PERSON_ID = " + searchTempBean.getCreatePeopleId();
            }
        } else {
            if (searchTempBean.getTaskStage() != -1) {// 任务阶段
                searchSql = searchSql + " and  A.state = " + searchTempBean.getTaskStage();
            }
            if (StringUtils.isNotBlank(searchTempBean.getTaskName())) {// 任务名称模糊查询
                searchSql = searchSql + " and A.task_name like " + "'%" + searchTempBean.getTaskName() + "%'";
            }
            if (searchTempBean.getCreatePeopleId() != null) {// 创建人模糊查询
                searchSql = searchSql + " and A.CREATE_PERSON_ID = " + searchTempBean.getCreatePeopleId();
            }
        }
        if (StringUtils.isNotBlank(searchTempBean.getStartTime())
                && StringUtils.isNotBlank(searchTempBean.getEndTime())) {

            // 字符串转换成时间拼接时、分、秒
            String startTime = searchTempBean.getStartTime() + " 00:00:00";
            String endTime = searchTempBean.getEndTime() + " 23:59:59";

            searchSql = searchSql + " and A.UPDATE_TIME>=" + getStrToDBFunction(startTime) + " and A.UPDATE_TIME<="
                    + getStrToDBFunction(endTime);
        }

        return searchSql;

    }

    private String getTaskTypeCondition(TaskSearchTempBean searchTempBean) {
        if (searchTempBean.getTaskType() == -1) {
            return "";
        } else {
            if (searchTempBean.getTaskType() == 2 || searchTempBean.getTaskType() == 3) {
                return " and a.task_type in (2,3) ";
            } else {
                return " and a.task_type=" + searchTempBean.getTaskType() + " ";
            }
        }
    }

    private String getSecondAdminTaskSqlByTaskType(Long roleId, Long peopleId, int type) {

        String ownerSql = getSecondAdminOwnerTaskSql(peopleId, type);
        String permSql = "";
        /** 0是流程图、1是文件,2是制度模板文件,3制度文件，4流程地图 */
        switch (type) {
            case 0:
                permSql = getSecondAdminFlowTaskSql(roleId);
                break;
            case 1:
                permSql = getSecondAdminFileTaskSql(roleId);
                break;
            case 2:
            case 3:
                permSql = getSecondAdminRuleTaskSql(roleId);
                break;
            case 4:
                permSql = getSecondAdminFlowMapTaskSql(roleId);
                break;
            default:
                break;
        }
        String sql = "select distinct a.* from (" + ownerSql + " union " + permSql + ")a";

        return sql;
    }

    private String getSecondAdminTaskSql(Long roleId, Long peopleId) {

        String ownerSql = getSecondAdminOwnerTaskSql(peopleId, -1);

        String permSql = "select a.* from ( " + getSecondAdminFileTaskSql(roleId) + " union "
                + getSecondAdminFlowMapTaskSql(roleId) + " union " + getSecondAdminFlowTaskSql(roleId) + " union "
                + getSecondAdminRuleTaskSql(roleId) + ")a ";

        String sql = "select distinct a.* from (" + ownerSql + " union " + permSql + ") a";

        return sql;

    }

    private String getSecondAdminOwnerTaskSql(Long peopleId, int type) {

        String condition = type == -1 ? "" : " and a.task_type=" + type;
        String ownerSql = " select a.* from  jecn_task_bean_new a where a.is_lock=1 " + condition
                + "  AND EXISTS (SELECT 1 FROM JECN_TASK_FOR_RECODE_NEW FRN WHERE FRN.TASK_ID = a.ID AND FRN.TO_PERSON_ID = "
                + peopleId + ")";

        return ownerSql;
    }

    private String getSecondAdminFlowTaskSql(Long roleId) {

        String sql = " select a.*" + "  from jecn_task_bean_new a," + "       jecn_flow_structure_t   c,"
                + "       jecn_flow_structure_t   p," + "       jecn_role_content     r"
                + " where c.t_path like p.t_path " + getConcatChar() + "'%'" + "   and p.flow_id = r.relate_id"
                + "   and r.role_id = " + roleId +
                // " and c.project_id=" + projectId +
                "   and r.type = 0" + "   and c.del_state = 0" + "   and p.del_state = 0" + "   and a.r_id = c.flow_id"
                + "   and a.is_lock=1 " + "   and a.task_type = 0";

        return sql;

    }

    private String getSecondAdminFlowMapTaskSql(Long roleId) {

        String sql = " select a.*" + "  from jecn_task_bean_new a," + "       jecn_flow_structure_t   c,"
                + "       jecn_flow_structure_t   p," + "       jecn_role_content     r"
                + " where c.t_path like p.t_path " + getConcatChar() + "'%'" + "   and p.flow_id = r.relate_id"
                + "   and r.role_id = " + roleId +
                // " and c.project_id=" + projectId +
                "   and r.type = 0" + "   and c.del_state = 0" + "   and p.del_state = 0" + "   and a.r_id = c.flow_id"
                + "   and a.is_lock=1 " + "   and a.task_type = 4";

        return sql;

    }

    private String getSecondAdminFileTaskSql(Long roleId) {

        String sql = " select a.*" + "  from jecn_task_bean_new a," + "       JECN_FILE_t             c,"
                + "       JECN_FILE_t             p," + "       jecn_role_content     r"
                + " where c.t_path like p.t_path " + getConcatChar() + "'%'" + "   and p.file_id = r.relate_id"
                + "   and r.role_id = " + roleId +
                // " and c.project_id=" + projectId +
                "   and r.type = 1" + "   and c.del_state = 0" + "   and p.del_state = 0" + "   and a.r_id = c.file_id"
                + "   and a.is_lock=1 " + "   and a.task_type = 1";

        return sql;

    }

    private String getSecondAdminRuleTaskSql(Long roleId) {

        String sql = " select a.*" + "  from jecn_task_bean_new a," + "       JECN_RULE_t             c,"
                + "       JECN_RULE_t             p," + "       jecn_role_content     r"
                + " where c.t_path like p.t_path " + getConcatChar() + "'%'" + "   and r.role_id = " + roleId +
                // " and c.project_id=" + projectId +
                "   and r.type = 3" + "   and a.r_id = c.id" + "   and a.is_lock=1 " + "   and a.task_type in (2, 3)";

        return sql;

    }

    public String saveTaskForRecodeNew(TaskForRecodeNew record) {

        String id = "";
        String idValue = "";
        if (isOracle()) {
            id = "id,";
            idValue = "#{id},";

        }

        String sql = "insert into jecn_task_for_recode_new(" + id + "TASK_ID," + "START_TIME," + "END_TIME," + "STATE,"
                + "UP_STATE," + "TASK_ELSE_STATE," + "IS_LOCK," + "OPINION," + "TASK_FIXED_DESC," + "SORT_ID,"
                + "CREATE_PERSON_ID," + "CREATE_TIME," + "UPDATE_TIME," + "FROM_PERSON_ID," + "TO_PERSON_ID,"
                + "REVIEW_COUNTS," + "APPROVE_NO_COUNTS" + ")" + "values(" + idValue
                + "#{taskId},#{startTime},#{endTime},#{state}"
                + ",#{upState},#{taskElseState},#{isLock},#{opinion},#{taskFixedDesc},#{sortId},#{createPersonId},"
                + "#{createTime},#{updateTime},#{fromPeopleId},#{toPeopleId},#{revirewCounts},#{approveNoCounts})";

        return sql;

    }

    public String saveTaskPeopleNew(TaskPeopleNew taskPeople) {

        String id = "";
        String idValue = "";
        if (isOracle()) {
            id = "id,";
            idValue = "#{id},";

        }

        String sql = "insert into jecn_task_people_new(" + id + "TASK_ID," + "APPROVE_PID" + ")" + "values("
                + idValue + "#{taskId},#{approvePid})";

        return sql;

    }

    public String saveTaskPeopleConn(TaskApprovePeopleConn approvePeopleConn) {

        String id = "";
        String idValue = "";
        if (isOracle()) {
            id = "id,";
            idValue = "#{id},";

        }

        String sql = "insert into JECN_TASK_PEOPLE_CONN_NEW(" + id + "APPROVE_PID," + "STAGE_ID" + ")" + "values("
                + idValue + "#{approvePid},#{stageId})";

        return sql;

    }

    public String findAbolish(Map<String, Object> map) {
        int type = Integer.valueOf(map.get("type").toString());
        String name = map.get("name").toString();
        boolean count = (boolean) map.get("count");
        Long orgId = map.get("orgId") == null ? 0 : Long.valueOf(map.get("orgId").toString());
        String sql = "select";
        if (count) {
            sql += " count(1) ";
        } else {
            sql += " abo.RELATIONID_ID id," +
                    "       abo.name," +
                    "       case abo.type" +
                    "         when '0' then" +
                    "          'PROCESS'" +
                    "         when '1' then" +
                    "          'FILE'" +
                    "         when '2' then" +
                    "          'RULE'" +
                    "         when '3' then" +
                    "          'RULE'" +
                    "         when '4' then" +
                    "          'PROCESS_MAP'" +
                    "       end type," +
                    "       abo.dir," +
                    "       abo.ABOLISH_TIME abolishTime," +
                    "       org.org_name orgName," +
                    "       abo.TASK_ID taskId," +
                    "       abo.HISTORY_ID historyId,";
            if (AuthenticatedUserUtil.isLanguageType() == 0) {
                sql += "       case abo.type" +
                        "         when '0' then" +
                        "          '流程'" +
                        "         when '1' then" +
                        "          '文件'" +
                        "         when '2' then" +
                        "          '制度'" +
                        "         when '3' then" +
                        "          '制度文件'" +
                        "         when '4' then" +
                        "          '流程架构'";
            } else {
                sql += "       case abo.type" +
                        "         when '0' then" +
                        "          'Process'" +
                        "         when '1' then" +
                        "          'File'" +
                        "         when '2' then" +
                        "          'Institution'" +
                        "         when '3' then" +
                        "          'Institution File'" +
                        "         when '4' then" +
                        "          'Process Map'";
            }
            sql += "       end abolishType ";
        }
        sql += "  from jecn_abolish abo" +
                "  left join jecn_file_t f" +
                "    on abo.relationid_id = f.file_id" +
                "  left join jecn_rule_t r" +
                "    on abo.relationid_id = r.id" +
                "  left join jecn_flow_related_org por" +
                "    on abo.relationid_id = por.flow_id" +
                "  left join jecn_flow_org org" +
                "    on org.del_state = 0" +
                "   and org.org_id = r.org_id" +
                "   or org.org_id = f.org_id" +
                "   or org.org_id = por.org_id" +
                "   where 1 = 1";
        if (type != -1) {
            sql += " and abo.type = " + type;
        }
        if (!"".equals(name) && name != null) {
            sql += "and  ( abo.name LIKE '%" + name + "%' or abo.keyword LIKE '%" + name + "%' or abo.doc_id LIKE  '%" + name + "%')";
        }
        if (orgId != null && orgId != 0) {
            sql += " and org.org_id =" + orgId;
        }

        return sql;
    }

    public String findTaskIflytekByUserCode(IflytekParameter parameter) {
        String sql = "SELECT DISTINCT A.ID," +
                "       A.TASK_NAME," +
                "       JTS.STAGE_NAME," +
                "       U.LOGIN_NAME CREATOR," +
                "       A.CREATE_TIME," +
                "       A.TASK_ELSE_STATE," +
                "       A.STATE," +
                "       A.UPDATE_TIME," +
                "       A.Approve_Type," +
                "       a.task_type " +
                "  FROM JECN_TASK_BEAN_NEW A" +
                " INNER JOIN JECN_TASK_STAGE JTS" +
                "    ON A.ID = JTS.TASK_ID" +
                "   AND A.STATE = JTS.STAGE_MARK";
        if ("101".equals(parameter.getHandleStatus())) {
            sql += " INNER JOIN JECN_TASK_FOR_RECODE_NEW B " +
                    "    ON A.ID = B.TASK_ID" +
                    " INNER JOIN JECN_USER C" +
                    "   ON B.FROM_PERSON_ID = C.PEOPLE_ID " +
                    "  AND C.ISLOCK = 0";
        } else {
            sql += " INNER JOIN JECN_TASK_PEOPLE_NEW B " +
                    "    ON A.ID = B.TASK_ID" +
                    " INNER JOIN JECN_USER C" +
                    "    ON B.APPROVE_PID = C.PEOPLE_ID " +
                    "   AND C.ISLOCK = 0";
        }
        sql += "  LEFT JOIN JECN_USER U" +
                "    ON A.CREATE_PERSON_ID = U.PEOPLE_ID" +
                "   AND U.ISLOCK = 0" +
                " WHERE A.IS_LOCK = 1" +
                "   AND A.STATE <> 5" +
                "   AND C.LOGIN_NAME = #{receiver}";
        if (StringUtils.isNotBlank(parameter.getFlowId())) { //任务ID
            sql += " AND A.ID = #{flowId} ";
        }
        //根据 开始时间 结束时间查询
        if (StringUtils.isNotBlank(parameter.getStartDate()) && StringUtils.isNotBlank(parameter.getEndDate())) {//创建时间
            // 字符串转换成时间拼接时、分、秒
            String startTime = parameter.getStartDate() + " 00:00:00";
            String endTime = parameter.getEndDate() + " 00:00:00";
            sql = sql + " and A.CREATE_TIME>=" + getStrToDBFunction(parameter.getStartDate()) + " and A.CREATE_TIME<="
                    + getStrToDBFunction(parameter.getEndDate());
        }
        return sql;

    }

}
