package com.jecn.epros.server.webBean.dataImport.sync.excelOrDB;

/**
 * 原始部门数据 （excel或试图获取 供数据校验）
 * 
 * @author ZHANGXIAOHU
 * @date： 日期：2013-5-29 时间：下午07:07:11
 */
public class JecnTmpOriDept extends AbstractBaseBean {
	/** 部门编号 DEPT_NUM */
	private String deptNum = null;
	/** 部门名称 DEPT_NAME */
	private String deptName = null;
	/** 父编号 P_DEPT_NUM */
	private String perDeptNum = null;
	/** 存储顺序 */
	private int sort;

	public String getDeptNum() {
		return deptNum;
	}

	public void setDeptNum(String deptNum) {
		this.deptNum = deptNum;
	}

	public String getDeptName() {
		return deptName;
	}

	public void setDeptName(String deptName) {
		this.deptName = deptName;
	}

	public String getPerDeptNum() {
		return perDeptNum;
	}

	public void setPerDeptNum(String perDeptNum) {
		this.perDeptNum = perDeptNum;
	}

	public int getSort() {
		return sort;
	}

	public void setSort(int sort) {
		this.sort = sort;
	}

	public String getError() {
		return error;
	}

	public void setError(String error) {
		this.error = error;
	}

	@Override
	public String toInfo() {
		// TODO Auto-generated method stub
		return null;
	}
}
