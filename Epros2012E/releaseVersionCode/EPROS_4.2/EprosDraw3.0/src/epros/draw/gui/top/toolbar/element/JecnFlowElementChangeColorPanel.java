package epros.draw.gui.top.toolbar.element;

import java.awt.FlowLayout;
import java.awt.event.ItemEvent;

import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.JRadioButton;

import epros.draw.constant.JecnWorkflowConstant.FillColorChangType;
import epros.draw.data.JecnAbstractFlowElementData;

/**
 * 
 * 流程元素渐变色设置类
 * 
 * @author ZHOUXY
 * 
 */
public class JecnFlowElementChangeColorPanel extends
		JecnAbstarctFlowElementPanel {

	/** 向上倾斜 */
	private JRadioButton changeUpRadioButton = null;
	/** 向下倾斜 */
	private JRadioButton changeDownRadioButton = null;
	/** 垂直 */
	private JRadioButton changeVerticalRadioButton = null;
	/** 水平 */
	private JRadioButton changeHorizontalRadioButton = null;

	public JecnFlowElementChangeColorPanel(JecnFlowElementPanel flowElementPanel) {
		super(flowElementPanel);
		init();
	}

	@Override
	protected void initComponents() {

		ButtonGroup group = new ButtonGroup();

		// 向上倾斜
		changeUpRadioButton = new JRadioButton(flowElementPanel
				.getResourceManager().getValue("changeUp"));
		// 向下倾斜
		changeDownRadioButton = new JRadioButton(flowElementPanel
				.getResourceManager().getValue("changeDown"));
		// 垂直
		changeVerticalRadioButton = new JRadioButton(flowElementPanel
				.getResourceManager().getValue("changeVertical"));
		// 水平
		changeHorizontalRadioButton = new JRadioButton(flowElementPanel
				.getResourceManager().getValue("changeHorizontal"));

		// 边框标题
		this.setBorder(BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(),flowElementPanel
				.getResourceManager().getValue("changeTitle")));

		// 布局
		this.setLayout(new FlowLayout(FlowLayout.LEFT, flowElementPanel
				.getInsetsFirst().left, flowElementPanel.getInsetsFirst().top));

		// 向上倾斜
		changeUpRadioButton.setOpaque(false);
		changeUpRadioButton.addItemListener(flowElementPanel);
		// 向下倾斜
		changeDownRadioButton.setOpaque(false);
		changeDownRadioButton.addItemListener(flowElementPanel);
		// 垂直
		changeVerticalRadioButton.setOpaque(false);
		changeVerticalRadioButton.addItemListener(flowElementPanel);
		// 水平
		changeHorizontalRadioButton.setOpaque(false);
		changeHorizontalRadioButton.addItemListener(flowElementPanel);

		group.add(changeUpRadioButton);
		group.add(changeDownRadioButton);
		group.add(changeVerticalRadioButton);
		group.add(changeHorizontalRadioButton);

	}

	@Override
	protected void initLayout() {
		// 向上倾斜
		this.add(changeUpRadioButton);
		// 向下倾斜
		this.add(changeDownRadioButton);
		// 垂直
		this.add(changeVerticalRadioButton);
		// 水平
		this.add(changeHorizontalRadioButton);
	}

	@Override
	public void initData(JecnAbstractFlowElementData flowElementData) {
		if (flowElementData == null
				|| flowElementData.getFillColorChangType() == null) {
			return;
		}
		switch (flowElementData.getFillColorChangType()) {
		case topTilt: // 1：向上倾斜 默认
			changeUpRadioButton.setSelected(true);
			break;
		case bottom: // 2：向下倾斜
			changeDownRadioButton.setSelected(true);
			break;
		case vertical: // 3：垂直
			changeVerticalRadioButton.setSelected(true);
			break;
		case horizontal:// 4：水平
			changeHorizontalRadioButton.setSelected(true);
			break;
		}
	}

	public void itemStateChanged(ItemEvent e) {
		itemStateChangedProcess(e);
	}

	private void itemStateChangedProcess(ItemEvent e) {

		if (e.getStateChange() == e.SELECTED) {//先判断是否选中
			if (e.getSource() == changeUpRadioButton) {// 向上倾斜
				// 数据层：渐变类型
				flowElementPanel.getSelectedBasePanel().getFlowElementData()
						.setFillColorChangType(FillColorChangType.topTilt);
			} else if (e.getSource() == changeDownRadioButton) {// 向下倾斜
				// 数据层：渐变类型
				flowElementPanel.getSelectedBasePanel().getFlowElementData()
						.setFillColorChangType(FillColorChangType.bottom);
			} else if (e.getSource() == changeVerticalRadioButton) {// 垂直
				// 数据层：渐变类型
				flowElementPanel.getSelectedBasePanel().getFlowElementData()
						.setFillColorChangType(FillColorChangType.vertical);
			} else if (e.getSource() == changeHorizontalRadioButton) {// 水平
				// 数据层：渐变类型
				flowElementPanel.getSelectedBasePanel().getFlowElementData()
						.setFillColorChangType(FillColorChangType.horizontal);
			}
		}
	}

	public JRadioButton getChangeUpRadioButton() {
		return changeUpRadioButton;
	}

	public JRadioButton getChangeDownRadioButton() {
		return changeDownRadioButton;
	}

	public JRadioButton getChangeVerticalRadioButton() {
		return changeVerticalRadioButton;
	}

	public JRadioButton getChangeHorizontalRadioButton() {
		return changeHorizontalRadioButton;
	}

}
