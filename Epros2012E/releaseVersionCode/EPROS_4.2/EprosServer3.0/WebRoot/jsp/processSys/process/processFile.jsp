<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%
	response.setHeader("Pragma", "No-cache");
	response.setHeader("Cache-Control", "no-cache");
%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<script type="text/javascript">
var basePath = "${basePath}";
document.body.onresize = function() {
	setShowProcessFileDivWH('processFileDiv');
}
</script>
	</head>

	<body>
		<div id="div" class="div" align="center">
			<div id="process"
				style="margin-top: 10px; margin-left: 15px; margin-right: 15px; margin-bottom: 15px; width: 95%">
				<table width="98%">
					<s:if test="processFileDownloadIsShow==true">
						<tr>
							<td align="right">
								<a
									href="${basePath}printFlowDoc.action?flowId=<s:property value='processShowFile.flowId' />&isMap=false&isPub=${isPub}"
									style="cursor: pointer;" target='_blank' class="table"><img
										src="${basePath}images/common/downLoadWord.gif" border="0"></img>
								</a>
								<a
									href="${basePath}downActiveRole.action?flowId=<s:property value='processShowFile.flowId'/>&isMap=false&isPub=${isPub}"
									style="cursor: pointer;" target='_blank' class="table"><img
										src="${basePath}images/common/downLoadExcel.gif" border="0"></img>
								</a>
							</td>
						</tr>
					</s:if>
					<tr>
						<td align="center"
							style="background-color: #E4E4E4; height: 30px; font: bold 15px ;">
							<!-- 流程名称 -->
							<s:property value="processShowFile.flowName" />
						</td>
					</tr>
				</table>
			</div>
			<div id="processFileDiv"
				style="margin-top: 10px; margin-left: 15px; margin-right: 15px; margin-bottom: 15px; width: 95%; overflow-y: scroll;">
				<table width="98%">
					<s:set name="index" value="1" />
					<s:iterator value="processShowFile.jecnConfigItemBean">
						<!-- 目的 -->
						<s:if test='mark=="a1" && value=="1"'>
							<tr
								style="background-color: #F8F8F8; font: bold 12px ; height: 20px;"
								align="left">
								<td>
									<s:property value="#index" />、<s:property value="name" />
									<s:set name="index" value="#index+1" />
								</td>
							</tr>
							<tr>
								<td style="padding-left: 10px; word-break: break-all;"
									align="left">
									<s:if test="processShowFile.flowPurpose == ''">
										<s:text name="none" />
									</s:if>
									<s:else>
										<s:property value="processShowFile.flowPurpose" escape="false" />
									</s:else>
								</td>
							</tr>
						</s:if>
						<!-- 适用范围 -->
						<s:if test='mark=="a2" && value=="1"'>
							<tr
								style="background-color: #F8F8F8; font: bold 12px ; height: 20px;"
								align="left">
								<td>
									<s:property value="#index" />、<s:property value="name" />
									<s:set name="index" value="#index+1" />
								</td>
							</tr>
							<tr>
								<td style="padding-left: 10px; word-break: break-all;"
									align="left">
									<s:if test="processShowFile.applicability == ''">
										<s:text name="none" />
									</s:if>
									<s:else>
										<s:property value="processShowFile.applicability"
											escape="false" />
									</s:else>
								</td>
							</tr>
						</s:if>
						<!-- 术语定义 -->
						<s:if test='mark=="a3" && value=="1"'>
							<tr
								style="background-color: #F8F8F8; font: bold 12px ; height: 20px;"
								align="left">
								<td>
									<s:property value="#index" />、<s:property value="name" />
									<s:set name="index" value="#index+1" />
								</td>
							</tr>
							<tr>
								<td style="padding-left: 10px; word-break: break-all;"
									align="left">
									<s:if test="processShowFile.noutGlossary == ''">
										<s:text name="none" />
									</s:if>
									<s:else>
										<s:property value="processShowFile.noutGlossary"
											escape="false" />
									</s:else>
								</td>
							</tr>
						</s:if>
						<!-- 流程驱动规则 -->
						<s:if test='mark=="a4" && value=="1"'>
							<tr
								style="background-color: #F8F8F8; font: bold 12px ; height: 20px;"
								align="left">
								<td>
									<s:property value="#index" />、<s:property value="name" />
									<s:set name="index" value="#index+1" />
								</td>
							</tr>
							<s:if test="processShowFile.flowDriver.driveType!=null">
								<tr
									style="background-color: #F8F8F8; font: 12px ; height: 20px;"
									align="left">
									<td style="padding-left: 10px;" align="left">
										<table width="100%" border="0" cellpadding="0" cellspacing="1"
											bgcolor="#BFBFBF" style="table-layout: fixed;">
											<s:if test='processShowFile.flowDriver.driveType=="1"'>
												<tr>
													<td width="12%" height="30" bgcolor="#DDDDDD" class="t6">
														<!-- 驱动类型 -->
														<s:text name="drivingType" />
													</td>
													<td bgcolor="#FFFFFF" style="word-break: break-all;">
														<!-- 时间驱动 -->
														<s:text name="timeDrive" />
													</td>
												</tr>
												<tr>
													<td width="12%" height="30" bgcolor="#DDDDDD" class="t6">
														<!-- 驱动规则 -->
														<s:text name="drivingRules" />
													</td>
													<td bgcolor="#FFFFFF" style="word-break: break-all;">
														<s:property value="processShowFile.flowDriver.driveRules"
															escape="false" />
													</td>
												</tr>
												<s:if test="processShowFile.flowDrivenRuleTimeConfig==1">
												<tr>
													<td width="12%" height="30" bgcolor="#DDDDDD" class="t6">
														<!-- 频率 -->
														<s:text name="frequency" />
													</td>
													<td bgcolor="#FFFFFF" style="word-break: break-all;">
														<s:property value="processShowFile.flowDriver.frequency" />
													</td>
												</tr>
												<tr>
													<td width="12%" height="30" bgcolor="#DDDDDD" class="t6">
														<!-- 开始时间 -->
														<s:text name="startTime" />
													</td>
													<td bgcolor="#FFFFFF" style="word-break: break-all;">
														<s:property value="processShowFile.flowDriver.startTime" />
													</td>
												</tr>
												<tr>
													<td width="12%" height="30" bgcolor="#DDDDDD" class="t6">
														<!-- 结束时间 -->
														<s:text name="endTime" />
													</td>
													<td bgcolor="#ffffff" style="word-break: break-all;">
														<s:property value="processShowFile.flowDriver.endTime" />
													</td>
												</tr>
												<tr>
													<td width="12%" height="30" bgcolor="#DDDDDD" class="t6">
														<!-- 类型 -->
														<s:text name="type" />
													</td>
													<td bgcolor="#ffffff" style="word-break: break-all;">
														<s:if test="processShowFile.flowDriver.quantity==1">
															<!-- 日 -->
															<s:text name="day" />
														</s:if>
														<s:elseif test="processShowFile.flowDriver.quantity==2">
															<!-- 周 -->
															<s:text name="weeks" />
														</s:elseif>
														<s:elseif test="processShowFile.flowDriver.quantity==3">
															<!-- 月 -->
															<s:text name="month" />
														</s:elseif>
														<s:elseif test="processShowFile.flowDriver.quantity==4">
															<!-- 季 -->
															<s:text name="season" />
														</s:elseif>
														<s:elseif test="processShowFile.flowDriver.quantity==5">
															<!-- 年 -->
															<s:text name="years" />
														</s:elseif>
													</td>
												</tr>
												</s:if>
											</s:if>
											<s:elseif test='processShowFile.flowDriver.driveType=="0"'>
												<tr>
													<td width="12%" height="30" bgcolor="#DDDDDD" class="t6">
														<!-- 驱动类型 -->
														<s:text name="drivingType" />
													</td>
													<td bgcolor="#FFFFFF" style="word-break: break-all;">
														<!-- 事件驱动 -->
														<s:text name="eventDriven" />
													</td>
												</tr>
												<tr>
													<td width="12%" height="30" bgcolor="#DDDDDD" class="t6">
														<!-- 驱动规则 -->
														<s:text name="drivingRules" />
													</td>
													<td bgcolor="#FFFFFF" style="word-break: break-all;">
														<s:property value="processShowFile.flowDriver.driveRules" escape="false" />
													</td>
												</tr>
											</s:elseif>
											<s:elseif test='processShowFile.flowDriver.driveType=="2"'>
												<tr>
													<td width="12%" height="30" bgcolor="#DDDDDD" class="t6">
														<!-- 驱动类型 -->
														<s:text name="drivingType" />
													</td>
													<td bgcolor="#FFFFFF" style="word-break: break-all;">
														<!-- 时间驱动/事件驱动 -->
														<s:text name="timeAndEventDriven" />
													</td>
												</tr>
												<tr>
													<td width="12%" height="30" bgcolor="#DDDDDD" class="t6">
														<!-- 驱动规则 -->
														<s:text name="drivingRules" />
													</td>
													<td bgcolor="#FFFFFF" style="word-break: break-all;">
														<s:property value="processShowFile.flowDriver.driveRules" escape="false" />
													</td>
												</tr>
											</s:elseif>
										</table>
									</td>
								</tr>
							</s:if>
							<s:else>
								<tr>
									<td style="padding-left: 10px;" align="left">
										<s:text name="none" />
									</td>
								</tr>
							</s:else>
						</s:if>
						<!-- 输入 -->
						<s:if test='mark=="a5" && value=="1"'>
							<tr
								style="background-color: #F8F8F8; font: bold 12px ; height: 20px;"
								align="left">
								<td>
									<s:property value="#index" />、<s:property value="name" />
									<s:set name="index" value="#index+1" />
								</td>
							</tr>
							<tr>
								<td style="padding-left: 10px; word-break: break-all;"
									align="left">
									<s:if test="processShowFile.flowInput == ''">
										<s:text name="none" />
									</s:if>
									<s:else>
										<s:property value="processShowFile.flowInput" escape="false" />
									</s:else>
								</td>
							</tr>
						</s:if>
						<!-- 输出 -->
						<s:if test='mark=="a6" && value=="1"'>
							<tr
								style="background-color: #F8F8F8; font: bold 12px ; height: 20px;"
								align="left">
								<td>
									<s:property value="#index" />、<s:property value="name" />
									<s:set name="index" value="#index+1" />
								</td>
							</tr>
							<tr>
								<td style="padding-left: 10px; word-break: break-all;"
									align="left">
									<s:if test="processShowFile.flowOutput == ''">
										<s:text name="none" />
									</s:if>
									<s:else>
										<s:property value="processShowFile.flowOutput" escape="false" />
									</s:else>
								</td>
							</tr>
						</s:if>
						
						<!-- 输入和输出 -->
						<s:if test='mark=="a32" && value=="1"'>
							<tr
								style="background-color: #F8F8F8; font: bold 12px ; height: 20px;"
								align="left">
								<td>
									<s:property value="#index" />、<s:property value="name" />
									<s:set name="index" value="#index+1" />
								</td>
							</tr>
							<tr>
								<td style="padding-left: 10px; word-break: break-all;"
									align="left">
									<table>
									   <tr>
									      <td style="width: 40px;" valign="top"><s:text name="inputC" /></td>
									      <td>
												<s:if test="processShowFile.flowInput == ''">
													<s:text name="none" />
												</s:if>
												<s:else>
													<s:property value="processShowFile.flowInput"
														escape="false" />
												</s:else>
											</td>
									   </tr>
									   <tr>
									      <td style="width: 40px;" valign="top"><s:text name="outputC" /></td>
											<td>
												<s:if test="processShowFile.flowOutput == ''">
													<s:text name="none" />
												</s:if>
												<s:else>
													<s:property value="processShowFile.flowOutput"
														escape="false" />
												</s:else>
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</s:if>
						
						
						<!-- 关键活动 -->
						<s:if test='mark=="a7" && value=="1"'>
							<tr
								style="background-color: #F8F8F8; font: bold 12px ; height: 20px;"
								align="left">
								<td>
									<s:property value="#index" />、<s:property value="name" />
									<s:set name="index" value="#index+1" />
								</td>
							</tr>
							<tr>
								<td style="padding-left: 10px; word-break: break-all;"
									align="left">
									<s:if test="processShowFile.flowFileType == 0">
										<s:if test="processShowFile.paActivityShowList.size > 0">
											<!-- 关键活动 -->
											<b> <s:text name="problemAreas" /> </b>
											<br />
											<table width="100%" border="0" cellpadding="0"
												cellspacing="1" align="center" bgcolor="#BFBFBF"
												style="table-layout: fixed;">
												<tr class="FixedTitleRow" align="center">
													<td bgcolor="#DDDDDD" width="10%" height="25" class="t8"
														style="word-break: break-all;">
														<!-- 活动编号 -->
														<s:text name="activitynumbers" />
													</td>
													<td bgcolor="#DDDDDD" width="10%" height="25" class="t8"
														style="word-break: break-all;">
														<!-- 活动名称-->
														<s:text name="activityTitle" />
													</td>
													<td bgcolor="#DDDDDD" width="10%" height="25" class="t8"
														style="word-break: break-all;">
														<!-- 活动说明 -->
														<s:text name="activityIndicatingThat" />
													</td>
													<td bgcolor="#DDDDDD" width="10%" height="25" class="t8"
														style="word-break: break-all;">
														<!-- 注意事项 -->
														<s:text name="note" />
													</td>
												</tr>
												<s:iterator value="processShowFile.paActivityShowList"
													status="st">
													<tr align="center" bgcolor="#ffffff"
														onmouseover="style.backgroundColor='#EFEFEF';"
														onmouseout="style.backgroundColor='#ffffff';" height="20">
														<td class="t5" style="word-break: break-all;">
															<s:property value="activeId" />
														</td>
														<td class="t5" style="word-break: break-all;">
															<s:if test="activityFlowType==1">
																<s:if test="linkFlowId!=null">
																	<a href="${basePath}process.action?type=process&flowId=<s:property value='linkFlowId'/>&isPub=${isPub}"
																			target='_blank' style="cursor: pointer;" class="table">
																			<s:property value="activeName" /> </a>
																</s:if>
																<s:else>
																		<s:property value="activeName" />
																</s:else>
															</s:if>
															<s:else>
																<a href="${basePath}processActive.action?activeId=<s:property value='activeFigureId'/>"
																	style="cursor: pointer;" target='_blank' class="table">
																	<s:property value="activeName" /> </a>
															</s:else>
														</td>
														<td class="t5" style="word-break: break-all;">
															<s:property value="activeShow" escape="false" />
														</td>
														<td class="t5" style="word-break: break-all;">
															<s:property value="keyActiveShow" escape="false" />
														</td>
													</tr>
												</s:iterator>
											</table>
										</s:if>
										<s:if test="processShowFile.ksfActivityShowList.size > 0">
											<!-- 关键活动 -->
											<b> <s:text name="keySuccessFactors" /> </b>
											<br />
											<table width="100%" border="0" cellpadding="0"
												cellspacing="1" align="center" bgcolor="#BFBFBF"
												style="table-layout: fixed;">
												<tr class="FixedTitleRow" align="center">
													<td bgcolor="#DDDDDD" width="10%" height="25" class="t8"
														style="word-break: break-all;">
														<!-- 活动编号 -->
														<s:text name="activitynumbers" />
													</td>
													<td bgcolor="#DDDDDD" width="10%" height="25" class="t8"
														style="word-break: break-all;">
														<!-- 活动名称-->
														<s:text name="activityTitle" />
													</td>
													<td bgcolor="#DDDDDD" width="10%" height="25" class="t8"
														style="word-break: break-all;">
														<!-- 活动说明-->
														<s:text name="activityIndicatingThat" />
													</td>
													<td bgcolor="#DDDDDD" width="10%" height="25" class="t8"
														style="word-break: break-all;">
														<!-- 关键成功因素 -->
														<s:text name="keySuccessFactors" />
													</td>
												</tr>
												<s:iterator value="processShowFile.ksfActivityShowList"
													status="st">
													<tr align="center" bgcolor="#ffffff"
														onmouseover="style.backgroundColor='#EFEFEF';"
														onmouseout="style.backgroundColor='#ffffff';" height="20">
														<td class="t5" style="word-break: break-all;">
															<s:property value="activeId" />
														</td>
														<td class="t5" style="word-break: break-all;">
															<s:if test="activityFlowType==1">
																<s:if test="linkFlowId!=null">
																	<a href="${basePath}process.action?type=process&flowId=<s:property value='linkFlowId'/>&isPub=${isPub}"
																			target='_blank' style="cursor: pointer;" class="table">
																			<s:property value="activeName" /> </a>
																</s:if>
																<s:else>
																		<s:property value="activeName" />
																</s:else>
															</s:if>
															<s:else>
																<a href="${basePath}processActive.action?activeId=<s:property value='activeFigureId'/>"
																	style="cursor: pointer;" target='_blank' class="table">
																	<s:property value="activeName" /> </a>
															</s:else>
														</td>
														<td class="t5" style="word-break: break-all;">
															<s:property value="activeShow" escape="false" />
														</td>
														<td class="t5" style="word-break: break-all;">
															<s:property value="keyActiveShow" escape="false" />
														</td>
													</tr>
												</s:iterator>
											</table>
										</s:if>
										<s:if test="processShowFile.kcpActivityShowList.size > 0">
											<!-- 关键活动 -->
											<b> <s:text name="keyControlPoint" /> </b>
											<br />
											<table width="100%" border="0" cellpadding="0"
												cellspacing="1" align="center" bgcolor="#BFBFBF"
												style="table-layout: fixed;">
												<tr class="FixedTitleRow" align="center">
													<td bgcolor="#DDDDDD" width="10%" height="25" class="t8"
														style="word-break: break-all;">
														<!-- 活动编号 -->
														<s:text name="activitynumbers" />
													</td>
													<td bgcolor="#DDDDDD" width="10%" height="25" class="t8"
														style="word-break: break-all;">
														<!--活动名称 -->
														<s:text name="activityTitle" />
													</td>
													<td bgcolor="#DDDDDD" width="10%" height="25" class="t8"
														style="word-break: break-all;">
														<!-- 活动说明 -->
														<s:text name="activityIndicatingThat" />
													</td>
													<td bgcolor="#DDDDDD" width="10%" height="25" class="t8"
														style="word-break: break-all;">
														<!-- 关键控制点 -->
														<s:text name="keyControlPoint" />
													</td>
												</tr>
												<s:iterator value="processShowFile.kcpActivityShowList"
													status="st">
													<tr align="center" bgcolor="#ffffff"
														onmouseover="style.backgroundColor='#EFEFEF';"
														onmouseout="style.backgroundColor='#ffffff';" height="20">
														<td class="t5" style="word-break: break-all;">
															<s:property value="activeId" />
														</td>
														<td class="t5" style="word-break: break-all;">
															<s:if test="activityFlowType==1">
																<s:if test="linkFlowId!=null">
																	<a href="${basePath}process.action?type=process&flowId=<s:property value='linkFlowId'/>&isPub=${isPub}"
																			target='_blank' style="cursor: pointer;" class="table">
																			<s:property value="activeName" /> </a>
																</s:if>
																<s:else>
																		<s:property value="activeName" />
																</s:else>
															</s:if>
															<s:else>
																<a href="${basePath}processActive.action?activeId=<s:property value='activeFigureId'/>"
																	style="cursor: pointer;" target='_blank' class="table">
																	<s:property value="activeName" /> </a>
															</s:else>
														</td>
														<td class="t5" style="word-break: break-all;">
															<s:property value="activeShow" escape="false" />
														</td>
														<td class="t5" style="word-break: break-all;">
															<s:property value="keyActiveShow" escape="false" />
														</td>
													</tr>
												</s:iterator>
											</table>
										</s:if>
									</s:if>
									<s:if test="processShowFile.flowFileType == 1">
										<s:if test="processShowFile.kcpActivityShowList.size > 0">
											<!-- 关键活动 -->
											<b> <s:text name="problemAreas" /> </b>
											<br />
											<s:iterator value="processShowFile.paActivityShowList"
												status="status">
												<!-- PA -->
												<table width="98%" style="margin-left: 10px;">
													<tr>
														<td width="100px;" style="font-weight: bold;">
															<s:property value="activeId" />
														</td>
														<td style="font-weight: bold;">
															<s:if test='activeName==""'>
																<s:text name="none" />
															</s:if>
															<s:else>
																<s:if test="linkFlowId!=null">
																		<a href="${basePath}process.action?type=process&flowId=<s:property value='linkFlowId'/>&isPub=${isPub}"
																				target='_blank' style="cursor: pointer;" class="table">
																				<s:property value="activeName" /> </a>
																</s:if>
																<s:else>
																		<s:property value="activeName" />
																</s:else>
																<s:else>
																	<a href="${basePath}processActive.action?activeId=<s:property value='activeFigureId'/>"
																		style="cursor: pointer;" target='_blank' class="table">
																		<s:property value="activeName" /> </a>
																</s:else>
															</s:else>
														</td>
													</tr>
													<tr>
														<td style="font-weight: bold;">
															<!-- 活动说明 -->
															<s:text name="activityIndicatingThatC" />
														</td>
														<td>
															<s:if test='activeShow==""'>
																<s:text name="none" />
															</s:if>
															<s:else>
																<s:property value="activeShow" escape="false" />
															</s:else>
														</td>
													</tr>
													<tr>
														<td style="font-weight: bold;">
															<!-- 注意事项 -->
															<s:text name="noteC" />
														</td>
														<td>
															<s:if test='keyActiveShow==""'>
																<s:text name="none" />
															</s:if>
															<s:else>
																<s:property value="keyActiveShow" escape="false" />
															</s:else>
														</td>
													</tr>
												</table>
												<hr />
											</s:iterator>
										</s:if>

										<s:if test="processShowFile.ksfActivityShowList.size > 0">
											<b> <s:text name="keySuccessFactors" /> </b>
											<br />
											<s:iterator value="processShowFile.ksfActivityShowList"
												status="status">
												<table width="98%" style="margin-left: 10px;">
													<tr>
														<td width="100px;" style="font-weight: bold;">
															<s:property value="activeId" />
														</td>
														<td style="font-weight: bold;">
															<s:if test='activeName==""'>
																<s:text name="none" />
															</s:if>
															<s:else>
																<s:if test="activityFlowType==1">
																	<s:if test="linkFlowId!=null">
																		<a href="${basePath}process.action?type=process&flowId=<s:property value='linkFlowId'/>&isPub=${isPub}"
																				target='_blank' style="cursor: pointer;" class="table">
																				<s:property value="activeName" /> </a>
																	</s:if>
																	<s:else>
																			<s:property value="activeName" />
																	</s:else>
																</s:if>
																<s:else>
																	<a href="${basePath}processActive.action?activeId=<s:property value='activeFigureId'/>"
																		style="cursor: pointer;" target='_blank' class="table">
																		<s:property value="activeName" /> </a>
																</s:else>
															</s:else>
														</td>
													</tr>
													<tr>
														<td style="font-weight: bold;">
															<!-- 活动说明 -->
															<s:text name="activityIndicatingThatC" />
														</td>
														<td>
															<s:if test='activeShow==""'>
																<s:text name="none" />
															</s:if>
															<s:else>
																<s:property value="activeShow" escape="false" />
															</s:else>
														</td>
													</tr>
													<tr>
														<td style="font-weight: bold;">
															<!-- 关键成功因素 -->
															<s:text name="keySuccessFactorsC" />
														</td>
														<td>
															<s:if test='keyActiveShow==""'>
																<s:text name="none" />
															</s:if>
															<s:else>
																<s:property value="keyActiveShow" escape="false" />
															</s:else>
														</td>
													</tr>
												</table>
												<hr />
											</s:iterator>
										</s:if>


										<s:if test="processShowFile.kcpActivityShowList.size > 0">
											<b> <s:text name="keyControlPoint" /> </b>
											<br />
											<s:iterator value="processShowFile.kcpActivityShowList"
												status="status">
												<table width="98%" style="margin-left: 10px;">
													<tr>
														<td width="100px;" style="font-weight: bold;">
															<s:property value="activeId" />
														</td>
														<td style="font-weight: bold;">
															<s:if test='activeName==""'>
																<s:text name="none" />
															</s:if>
															<s:else>
																<s:if test="activityFlowType==1">
																	<s:if test="linkFlowId!=null">
																		<a href="${basePath}process.action?type=process&flowId=<s:property value='linkFlowId'/>&isPub=${isPub}"
																				target='_blank' style="cursor: pointer;" class="table">
																				<s:property value="activeName" /> </a>
																	</s:if>
																	<s:else>
																			<s:property value="activeName" />
																	</s:else>
																</s:if>
																<s:else>
																	<a href="${basePath}processActive.action?activeId=<s:property value='activeFigureId'/>"
																		style="cursor: pointer;" target='_blank' class="table">
																		<s:property value="activeName" /> </a>
																</s:else>
															</s:else>
														</td>
													</tr>
													<tr>
														<td style="font-weight: bold;">
															<!-- 活动说明 -->
															<s:text name="activityIndicatingThatC" />
														</td>
														<td>
															<s:if test='activeShow==""'>
																<s:text name="none" />
															</s:if>
															<s:else>
																<s:property value="activeShow" escape="false" />
															</s:else>
														</td>
													</tr>
													<tr>
														<td style="font-weight: bold;">
															<!-- 关键控制点 -->
															<s:text name="keyControlPointC" />
														</td>
														<td>
															<s:if test='keyActiveShow==""'>
																<s:text name="none" />
															</s:if>
															<s:else>
																<s:property value="keyActiveShow" escape="false" />
															</s:else>
															<br />
														</td>
													</tr>
												</table>
												<hr />
											</s:iterator>
										</s:if>
									</s:if>
									<s:if
										test="processShowFile.paActivityShowList.size == 0 
								              && processShowFile.ksfActivityShowList.size == 0
								              && processShowFile.kcpActivityShowList.size == 0">
										<s:text name="none" />
									</s:if>
								</td>
							</tr>
						</s:if>

						<!-- 角色/职责 -->
						<s:if test='mark=="a8" && value=="1"'>
							<tr
								style="background-color: #F8F8F8; font: bold 12px ; height: 20px;"
								align="left">
								<td>
									<s:property value="#index" />、<s:property value="name" />
									<s:set name="index" value="#index+1" />
								</td>
							</tr>
							<tr>
								<td align="left" style="padding-left: 10px;">
									<s:if test="processShowFile.roleList.size>0">
										<table width="100%" border="0" cellpadding="0" cellspacing="1"
											align="center" bgcolor="#BFBFBF" style="table-layout: fixed;">
											<tr class="FixedTitleRow" align="center">
												<td bgcolor="#DDDDDD" width="10%" height="25" class="t8"
													style="word-break: break-all;">
													<!-- 角色名称 -->
													<s:text name="roleName" />
												</td>
												<!-- 根据配置设置是否显示岗位名称 -->
												<s:if test="processShowFile.showPosNameBox">
													<td bgcolor="#DDDDDD" width="10%" height="25" class="t8"
														style="word-break: break-all;">
														<!-- 岗位名称 -->
														<s:text name="positionName" />
													</td>
												</s:if>
												<td bgcolor="#DDDDDD" width="10%" height="25" class="t8"
													style="word-break: break-all;">
													<!-- 角色职责 -->
													<s:text name="responsibilities" />
												</td>
											</tr>
											<s:iterator value="processShowFile.roleList" status="st">
												<tr align="center" bgcolor="#ffffff"
													onmouseover="style.backgroundColor='#EFEFEF';"
													onmouseout="style.backgroundColor='#ffffff';" height="20">
													<td class="t5" style="word-break: break-all;">
														<s:property value="roleName" />
													</td>
													<!-- 根据配置设置是否显示岗位名称 -->
													<s:if test="processShowFile.showPosNameBox">
														<td class="t5" style="word-break: break-all;">
															<s:iterator value="listPos">
																<a
																	href="${basePath}orgSpecification.action?posId=<s:property value='id'/>&isPub=${isPub}"
																	target='_blank' style="cursor: pointer;" class="table">
																	<s:property value="name" /> <br> </a>
															</s:iterator>
														</td>
													</s:if>
													<td class="t5" style="word-break: break-all;">
														<s:property value="roleRes" escape="false" />
													</td>
												</tr>
											</s:iterator>
										</table>
									</s:if>
									<s:else>
										<s:text name="none" />
									</s:else>
								</td>
							</tr>
						</s:if>

						<!-- 流程图 -->
						<s:if test='mark=="a9" && value=="1"'>
							<tr
								style="background-color: #F8F8F8; font: bold 12px ; height: 20px;"
								align="left">
								<td>
									<s:property value="#index" />、<s:property value="name" />
									<s:set name="index" value="#index+1" />
								</td>
							</tr>
							<tr>
								<td style="padding-left: 10px;" align="left">
								<s:if test='isPub=="true"'>
									<a
										href="${basePath}process.action?type=process&flowId=<s:property value='processShowFile.flowId'/>&isPub=${isPub}"
										target='_blank' style="cursor: pointer;" class="table"> <s:property
											value="processShowFile.flowName" /> </a>
								</s:if>
								<s:else>
									<a
										href="${basePath}process.action?type=processT&reqType=public&flowId=<s:property value='processShowFile.flowId'/>&isPub=${isPub}"
										target='_blank' style="cursor: pointer;" class="table"> <s:property
											value="processShowFile.flowName" /> </a>
								</s:else>			
								</td>
							</tr>
						</s:if>

						<!-- 活动说明 -->
						<s:if test='mark=="a10" && value=="1"'>
							<tr
								style="background-color: #F8F8F8; font: bold 12px ; height: 20px;"
								align="left">
								<td>
									<s:property value="#index" />、<s:property value="name" />
									<s:set name="index" value="#index+1" />
								</td>
							</tr>
							<tr>
								<td align="left" style="padding-left: 10px;">
									<s:if test="processShowFile.allActivityShowList.size>0">
										<s:if test="processShowFile.flowFileType == 0">
											<table width="100%" border="0" cellpadding="0"
												cellspacing="1" align="center" bgcolor="#BFBFBF"
												style="table-layout: fixed;">
												<tr class="FixedTitleRow" align="center">
													<td bgcolor="#DDDDDD" width="10%" height="25" class="t8"
														style="word-break: break-all;">
														<!-- 活动编号 -->
														<s:text name="activitynumbers" />
													</td>
													<td bgcolor="#DDDDDD" width="10%" height="25" class="t8"
														style="word-break: break-all;">
														<!-- 执行角色 -->
														<s:text name="executiveRole" />
													</td>
													<td bgcolor="#DDDDDD" width="10%" height="25" class="t8"
														style="word-break: break-all;">
														<!-- 活动名称 -->
														<s:text name="activityTitle" />
													</td>
													<td bgcolor="#DDDDDD" width="10%" height="25" class="t8"
														style="word-break: break-all;">
														<!-- 活动说明 -->
														<s:text name="activityIndicatingThat" />
													</td>
													<s:if test="otherLoginType == 4">
														<td bgcolor="#DDDDDD" width="10%" height="25" class="t8"
															style="word-break: break-all;">
															<!-- 对应内控矩阵风险点 -->
															<s:text name="innerControlRisk" />
														</td>
														<td bgcolor="#DDDDDD" width="10%" height="25" class="t8"
															style="word-break: break-all;">
															<!-- 对应标准条款 -->
															<s:text name="standardConditions" />
														</td>
													</s:if>
													<s:else>
														<!-- 根据配置是否显示输入输出 -->
														<s:if test="processShowFile.showActInOutBox">
														<td bgcolor="#DDDDDD" width="10%" height="25" class="t8"
															style="word-break: break-all;">
															<!-- 输入 -->
															<s:text name="input" />
														</td>
														<td bgcolor="#DDDDDD" width="10%" height="25" class="t8"
															style="word-break: break-all;">
															<!-- 输出 -->
															<s:text name="output" />
														</td>
														</s:if>
													</s:else>
												</tr>
												<s:iterator value="processShowFile.allActivityShowList"
													status="st">
													<tr align="center" bgcolor="#ffffff"
														onmouseover="style.backgroundColor='#EFEFEF';"
														onmouseout="style.backgroundColor='#ffffff';" height="20">
														<td class="t5" style="word-break: break-all;">
															<s:property value="activeId" />
														</td>
														<td class="t5" style="word-break: break-all;">
															<s:property value="roleName" />
														</td>
														<td class="t5" style="word-break: break-all;">
															<s:if test="activityFlowType==1">
																	<s:if test="linkFlowId!=null">
																		<a href="${basePath}process.action?type=process&flowId=<s:property value='linkFlowId'/>&isPub=${isPub}"
																				target='_blank' style="cursor: pointer;" class="table">
																				<s:property value="activeName" /> </a>
																	</s:if>
																	<s:else>
																			<s:property value="activeName" />
																	</s:else>
																</s:if>
																<s:else>
																	<a href="${basePath}processActive.action?activeId=<s:property value='activeFigureId'/>"
																		style="cursor: pointer;" target='_blank' class="table">
																		<s:property value="activeName" /> </a>
															</s:else>
														</td>
														<td class="t5" style="word-break: break-all;">
															<s:property value="activeShow" escape="false" />
														</td>
														<s:if test="otherLoginType == 4">
															<td class="t5" style="word-break: break-all;">
																<s:property value="innerControlRisk" />
															</td>
															<td class="t5" style="word-break: break-all;">
																<s:property value="standardConditions" />
															</td>
														</s:if>
														<s:else>
															<s:if test="processShowFile.showActInOutBox">
															<td class="t5" style="word-break: break-all;">
															<!--输入说明-->
															<s:if test="processShowFile.activityNoteShow==1">
															<s:property value="activityInput" />
															<br>
															</s:if>
																<s:iterator value="listInput">
																	<a
																		href="${basePath}file.action?from=process&reqType=public&visitType=downFile&fileId=<s:property value='fileId'/>&isPub=${isPub}"
																		style="cursor: pointer;" target='_blank' class="table">
																		<s:property value="fileName" /> </a>
																	<br>
																</s:iterator>
															</td>
															<td class="t5" style="word-break: break-all;">
															<!--输出说明-->
															<s:if test="processShowFile.activityNoteShow==1">
															<s:property value="activityOutput" />
															<br>
															</s:if>
																<s:iterator value="listMode">
																	<a
																		href="${basePath}file.action?from=process&reqType=public&visitType=downFile&fileId=<s:property value='modeFile.fileId'/>&isPub=${isPub}"
																		style="cursor: pointer;" target='_blank' class="table">
																		<s:property value="modeFile.fileName" /> </a>
																	<br>
																</s:iterator>
															</td>
															</s:if>
														</s:else>
													</tr>
												</s:iterator>
											</table>
										</s:if>
										<s:elseif test="processShowFile.flowFileType == 1">
											<s:iterator value="processShowFile.allActivityShowList"
												status="status">
												<table width="98%" style="margin-left: 10px;">
													<tr>
														<td width="100px;" style="font-weight: bold;">
															<s:property value="activeId" />
														</td>
														<td style="font-weight: bold;">
															<s:if test='activeName==""'>
																<s:text name="none" />
															</s:if>
															<s:else>
																<s:if test="activityFlowType==1">
																	<s:if test="linkFlowId!=null">
																		<a href="${basePath}process.action?type=process&flowId=<s:property value='linkFlowId'/>&isPub=${isPub}"
																				target='_blank' style="cursor: pointer;" class="table">
																				<s:property value="activeName" /> </a>
																	</s:if>
																	<s:else>
																			<s:property value="activeName" />
																	</s:else>
																</s:if>
																<s:else>
																	<a href="${basePath}processActive.action?activeId=<s:property value='activeFigureId'/>"
																		style="cursor: pointer;" target='_blank' class="table">
																		<s:property value="activeName" /> </a>
																</s:else>
															</s:else>
														</td>
													</tr>
													<tr>
														<td style="font-weight: bold;">
															<!-- 执行角色 -->
															<s:text name="executiveRoleC" />
														</td>
														<td>
															<s:if test='roleName==""'>
																<s:text name="none" />
															</s:if>
															<s:else>
																<s:property value="roleName" />
															</s:else>
														</td>
													</tr>
													<tr>
														<td style="font-weight: bold;">
															<!-- 活动说明 -->
															<s:text name="activityIndicatingThatC" />
														</td>
														<td style="word-break: break-all;">
															<s:if test='activeShow==""'>
																<s:text name="none" />
															</s:if>
															<s:else>
																<s:property value="activeShow" escape="false" />
															</s:else>
														</td>
													</tr>
													<s:if test="otherLoginType == 4">
														<tr>
															<td style="font-weight: bold;">
																<!-- 对应内控矩阵风险点 -->
																<s:text name="innerControlRisk" />
															</td>
															<td style="word-break: break-all;">
																<s:if test='innerControlRisk==""'>
																	<s:text name="none" />
																</s:if>
																<s:else>
																	<s:property value="innerControlRisk" escape="false" />
																</s:else>
															</td>
														</tr>

														<tr>
															<td style="font-weight: bold;">
																<!-- 对应标准条款 -->
																<s:text name="standardConditions" />
															</td>
															<td style="word-break: break-all;">
																<s:if test='standardConditions==""'>
																	<s:text name="none" />
																</s:if>
																<s:else>
																	<s:property value="standardConditions" escape="false" />
																</s:else>
															</td>
														</tr>
													</s:if>
													<s:else>
														<tr>
															<td style="font-weight: bold;">
																<!-- 输入 -->
																<s:text name="inputC" />
															</td>
															<td style="word-break: break-all;">
															<!--输入说明-->
															<s:if test="processShowFile.activityNoteShow==1">
															<s:property value="activityInput" /><br>
															</s:if>
																<s:if test="listInput.size>0">
																	<s:iterator value="listInput">
												                   [<a
																			href="${basePath}file.action?from=process&reqType=public&visitType=downFile&fileId=<s:property value='fileId'/>&isPub=${isPub}"
																			style="cursor: pointer;" target='_blank'
																			class="table"> <s:property value="fileName" /> </a>]
											                   </s:iterator>
																</s:if>
																<s:else>
																	<s:text name="none" />
																</s:else>
															</td>
														</tr>
														<tr>
															<td style="font-weight: bold;">
																<!-- 输出：-->
																<s:text name="outputC" />
															</td>
															<td style="word-break: break-all;">
															<!--输出说明-->
															<s:if test="processShowFile.activityNoteShow==1">
															<s:property value="activityOutput" /><br></s:if>
																<s:if test="listMode.size>0">
																	<s:iterator value="listMode">
												                       [<a
																			href="${basePath}file.action?from=process&reqType=public&visitType=downFile&fileId=<s:property value='modeFile.fileId'/>&isPub=${isPub}"
																			style="cursor: pointer;" target='_blank'
																			class="table"> <s:property
																				value="modeFile.fileName" /> </a>]
											                    </s:iterator>
																</s:if>
																<s:else>
																	<s:text name="none" />
																</s:else>
															</td>
														</tr>
													</s:else>
												</table>
												<hr />
											</s:iterator>
										</s:elseif>
									</s:if>
									<s:else>
										<s:text name="none" />
									</s:else>
								</td>
							</tr>
						</s:if>

						<!-- 流程记录 -->
						<s:if test='mark=="a11" && value=="1"'>
							<tr
								style="background-color: #F8F8F8; font: bold 12px ; height: 20px;"
								align="left">
								<td>
									<s:property value="#index" />、<s:property value="name" />
									<s:set name="index" value="#index+1" />
								</td>
							</tr>
							<tr>
								<td style="padding-left: 10px;" align="left">
									<s:if test="processShowFile.processRecordList.size>0">
										<table width="100%" border="0" cellpadding="0" cellspacing="1"
											align="center" bgcolor="#BFBFBF" style="table-layout: fixed;">
											<tr class="FixedTitleRow" align="center">
												<td bgcolor="#DDDDDD" width="10%" height="25" class="t8"
													style="word-break: break-all;">
													<!-- 文件编号 -->
													<s:text name="fileNumber" />
												</td>
												<td bgcolor="#DDDDDD" width="10%" height="25" class="t8"
													style="word-break: break-all;">
													<!-- 文件名称 -->
													<s:text name="fileName" />
												</td>
											</tr>
											<s:iterator value="processShowFile.processRecordList"
												status="st">
												<tr align="center" bgcolor="#ffffff"
													onmouseover="style.backgroundColor='#EFEFEF';"
													onmouseout="style.backgroundColor='#ffffff';" height="20">
													<td class="t5" style="word-break: break-all;">
														<s:property value="fileNum" />
													</td>
													<td class="t5" style="word-break: break-all;">
														<a
															href="${basePath}file.action?from=process&reqType=public&visitType=downFile&fileId=<s:property value='fileId'/>&isPub=${isPub}"
															target='_blank' style="cursor: pointer;" class="table">
															<s:property value="fileName" /> </a>
													</td>
												</tr>
											</s:iterator>
										</table>
									</s:if>
									<s:else>
										<s:text name="none" />
									</s:else>
								</td>
							</tr>
						</s:if>

						<!-- 操作规范 -->
						<s:if test='mark=="a12" && value=="1"'>
							<tr
								style="background-color: #F8F8F8; font: bold 12px ; height: 20px;"
								align="left">
								<td>
									<s:property value="#index" />、<s:property value="name" />
									<s:set name="index" value="#index+1" />
								</td>
							</tr>
							<tr>
								<td style="padding-left: 10px;" align="left">
									<s:if test="processShowFile.operationTemplateList.size>0">
										<table width="100%" border="0" cellpadding="0" cellspacing="1"
											align="center" bgcolor="#BFBFBF" style="table-layout: fixed;">
											<tr class="FixedTitleRow" align="center">
												<td bgcolor="#DDDDDD" width="10%" height="25" class="t8"
													style="word-break: break-all;">
													<!-- 文件编号 -->
													<s:text name="fileNumber" />
												</td>
												<td bgcolor="#DDDDDD" width="10%" height="25" class="t8"
													style="word-break: break-all;">
													<!-- 文件名称 -->
													<s:text name="fileName" />
												</td>
											</tr>
											<s:iterator value="processShowFile.operationTemplateList"
												status="st">
												<tr align="center" bgcolor="#ffffff"
													onmouseover="style.backgroundColor='#EFEFEF';"
													onmouseout="style.backgroundColor='#ffffff';" height="20">
													<td class="t5" style="word-break: break-all;">
														<s:property value="fileNum" />
													</td>
													<td class="t5" style="word-break: break-all;">
														<a
															href="${basePath}file.action?from=process&reqType=public&visitType=downFile&fileId=<s:property value='fileId'/>&isPub=${isPub}"
															style="cursor: pointer;" target='_blank' class="table">
															<s:property value="fileName" /> </a>
													</td>
												</tr>
											</s:iterator>
										</table>
									</s:if>
									<s:else>
										<s:text name="none" />
									</s:else>
								</td>
							</tr>
						</s:if>

						<!-- 相关流程 -->
						<s:if test='mark=="a13" && value=="1"'>
							<tr
								style="background-color: #F8F8F8; font: bold 12px ; height: 20px;"
								align="left">
								<td>
									<s:property value="#index" />、<s:property value="name" />
									<s:set name="index" value="#index+1" />
								</td>
							</tr>
							<tr>
								<td style="padding-left: 10px;" align="left">
									<s:if test="processShowFile.relatedProcessList.size>0">
										<table width="100%" border="0" cellpadding="0" cellspacing="1"
											bgcolor="#BFBFBF" style="table-layout: fixed;">
											<tr>
												<td width="20%" height="25" bgcolor="#DDDDDD" class="t6">
													&nbsp;
												</td>
												<td width="30%" height="25" bgcolor="#DDDDDD" class="t6">
													<!-- 流程编号 -->
													<s:text name="processNumber" />
												</td>
												<td width="50%" height="25" bgcolor="#DDDDDD" class="t6">
													<!-- 流程名称 -->
													<s:text name="processName" />
												</td>
											</tr>

											<s:set name="upperFlowType" value="0" />
											<s:set name="upperFlowCount" value="0" />
											<s:iterator value="processShowFile.relatedProcessList">
												<s:if test="linecolor==1">
													<s:set name="upperFlowCount" value="#upperFlowCount+1" />
												</s:if>
											</s:iterator>
											<s:if test="#upperFlowCount == 0">
												<s:set name="upperFlowCount" value="1" />
											</s:if>
											<s:iterator value="processShowFile.relatedProcessList"
												status="status">
												<s:if test="linecolor==1">
													<tr>
														<s:if test="#upperFlowType==0">
															<td width="20%" height="25" bgcolor="#DDDDDD" class="t6"
																rowspan="<s:property value='#upperFlowCount'/>">
																<s:set name="upperFlowType" value="1" />
																<!-- 上游流程 -->
																<s:text name="upstreamProcess" />
															</td>
														</s:if>
														<td width="30%" height="25" bgcolor="#FFFFFF">
															<s:if test="flowNumber!=null && flowNumber!=''">
																<s:property value="flowNumber" />
																<br>
															</s:if>
														</td>
														<td width="50%" height="25" bgcolor="#FFFFFF">
															<s:if test="flowId!=null && flowId!=''">
																<a
																	href="${basePath}process.action?type=process&isPub=${isPub}&flowId=<s:property value='flowId'/>"
																	target='_blank' style="cursor: pointer;" class="table">
																	<s:property value="flowName" /> </a>
																<br>
															</s:if><s:else>
																<s:property value="flowName" />
															</s:else>
														</td>
													</tr>
												</s:if>
											</s:iterator>

											<s:set name="lowerFlowType" value="0" />
											<s:set name="lowerFlowCount" value="0" />
											<s:iterator value="processShowFile.relatedProcessList">
												<s:if test="linecolor==2">
													<s:set name="lowerFlowCount" value="#lowerFlowCount+1" />
												</s:if>
											</s:iterator>
											<s:if test="#lowerFlowCount == 0">
												<s:set name="lowerFlowCount" value="1" />
											</s:if>
											<s:iterator value="processShowFile.relatedProcessList"
												status="status">
												<s:if test="linecolor==2">
													<tr>
														<s:if test="#lowerFlowType==0">
															<td width="20%" height="25" bgcolor="#DDDDDD" class="t6"
																rowspan="<s:property value='#lowerFlowCount'/>">
																<s:set name="lowerFlowType" value="1" />
																<!-- 下游流程 -->
																<s:text name="downstreamProcess" />
															</td>
														</s:if>
														<td width="30%" height="25" bgcolor="#FFFFFF">
															<s:if test="flowNumber!=null && flowNumber!=''">
																<s:property value="flowNumber" />
																<br>
															</s:if>
														</td>
														<td width="50%" height="25" bgcolor="#FFFFFF">
															<s:if test="flowId!=null && flowId!=''">
																<a
																	href="${basePath}process.action?type=process&isPub=${isPub}&flowId=<s:property value='flowId'/>"
																	target='_blank' style="cursor: pointer;" class="table">
																	<s:property value="flowName" /> </a>
																<br>
															</s:if><s:else>
																<s:property value="flowName" />
															</s:else>
														</td>
													</tr>
												</s:if>
											</s:iterator>

											<s:set name="subFlowType" value="0" />
											<s:set name="subFlowCount" value="0" />
											<s:iterator value="processShowFile.relatedProcessList">
												<s:if test="linecolor==3">
													<s:set name="subFlowCount" value="#subFlowCount+1" />
												</s:if>
											</s:iterator>
											<s:if test="#subFlowCount == 0">
												<s:set name="subFlowCount" value="1" />
											</s:if>
											<s:iterator value="processShowFile.relatedProcessList"
												status="status">
												<s:if test="linecolor==3">
													<tr>
														<s:if test="#subFlowType==0">
															<td width="20%" height="25" bgcolor="#DDDDDD" class="t6"
																rowspan="<s:property value='#subFlowCount'/>">
																<s:set name="subFlowType" value="1" />
																<!-- 过程流程 -->
																<s:text name="processFlow" />
															</td>
														</s:if>
														<td width="30%" height="25" bgcolor="#FFFFFF">
															<s:if test="flowNumber!=null && flowNumber!=''">
																<s:property value="flowNumber" />
																<br>
															</s:if>
														</td>
														<td width="50%" height="25" bgcolor="#FFFFFF">
															<s:if test="flowId!=null && flowId!=''">
																<a
																	href="${basePath}process.action?type=process&isPub=${isPub}&flowId=<s:property value='flowId'/>"
																	target='_blank' style="cursor: pointer;" class="table">
																	<s:property value="flowName" /> </a>
																<br>
															</s:if><s:else>
																<s:property value="flowName" />
															</s:else>
														</td>
													</tr>
												</s:if>
											</s:iterator>

											<s:set name="implFlowType" value="0" />
											<s:set name="implFlowCount" value="0" />
											<s:iterator value="processShowFile.relatedProcessList">
												<s:if test="linecolor==4">
													<s:set name="implFlowCount" value="#implFlowCount+1" />
												</s:if>
											</s:iterator>
											<s:if test="#implFlowCount == 0">
												<s:set name="implFlowCount" value="1" />
											</s:if>
											<s:iterator value="processShowFile.relatedProcessList"
												status="status">
												<s:if test="linecolor==4">
													<tr>
														<s:if test="#implFlowType==0">
															<td width="20%" height="25" bgcolor="#DDDDDD" class="t6"
																rowspan="<s:property value='#implFlowCount'/>">
																<s:set name="implFlowType" value="1" />
																<!-- 子流程 -->
																<s:text name="childProcess" />
															</td>
														</s:if>
														<td width="30%" height="25" bgcolor="#FFFFFF">
															<s:if test="flowNumber!=null && flowNumber!=''">
																<s:property value="flowNumber" />
																<br>
															</s:if>
														</td>
														<td width="50%" height="25" bgcolor="#FFFFFF">
															<s:if test="flowId!=null && flowId!=''">
																<a
																	href="${basePath}process.action?type=process&isPub=${isPub}&flowId=<s:property value='flowId'/>"
																	target='_blank' style="cursor: pointer;" class="table">
																	<s:property value="flowName" /> </a>
																<br>
															</s:if><s:else>
																<s:property value="flowName" />
															</s:else>
															<br>
														</td>
													</tr>
												</s:if>
											</s:iterator>
										</table>
									</s:if>
									<s:else>
										<s:text name="none" />
									</s:else>
								</td>
							</tr>
						</s:if>

						<!-- 相关制度 -->
						<s:if test='mark=="a14" && value=="1"'>
							<tr
								style="background-color: #F8F8F8; font: bold 12px ; height: 20px;"
								align="left">
								<td>
									<s:property value="#index" />、<s:property value="name" />
									<s:set name="index" value="#index+1" />
								</td>
							</tr>
							<tr>
								<td style="padding-left: 10px;" align="left">
									<s:if test="processShowFile.ruleNameList.size>0">
										<table width="100%" border="0" cellpadding="0" cellspacing="1"
											align="center" bgcolor="#BFBFBF" style="table-layout: fixed;">
											<tr class="FixedTitleRow" align="center">
											   <td bgcolor="#DDDDDD" width="10%" height="25" class="t8"
													style="word-break: break-all;">
													<!--制度编号 -->
													制度编号
												</td>
												<td bgcolor="#DDDDDD" width="10%" height="25" class="t8"
													style="word-break: break-all;">
													<!--制度名称 -->
													<s:text name="ruleName" />
												</td>
												<td bgcolor="#DDDDDD" width="10%" height="25" class="t8"
													style="word-break: break-all;">
													<!-- 类型 -->
													<s:text name="type" />
												</td>
											</tr>
											<s:iterator value="processShowFile.ruleNameList">
												<tr align="center" bgcolor="#ffffff"
													onmouseover="style.backgroundColor='#EFEFEF';"
													onmouseout="style.backgroundColor='#ffffff';" height="20">
													
													
													<td class="t5" style="word-break: break-all;">
													    <s:property value="ruleNumber" />
													</td>
													<td class="t5" style="word-break: break-all;">
														<s:if test="isDir == 1">
															<a
																href="${basePath}rule.action?from=process&type=ruleModeFile&reqType=public&isPub=${isPub}&ruleId=<s:property value='id'/>"
																style="cursor: pointer;" target='_blank' class="table">
																<s:property value="ruleName" /> </a>
															<br>
														</s:if>
														<s:elseif test="isDir == 2">
															<a
																href="${basePath}rule.action?from=process&type=ruleFile&reqType=public&isPub=${isPub}&ruleId=<s:property value='id'/>"
																style="cursor: pointer;" target='_blank' class="table">
																<s:property value="ruleName" /> </a>
															<br>
														</s:elseif>
														<s:else>
															<s:property value="ruleName" />
														</s:else>
													</td>
													<td class="t5" style="word-break: break-all;">
														<s:if test="isDir == 1">
															<s:text name="ruleModeFile" />
														</s:if>
														<s:elseif test="isDir == 2">
															<s:text name="ruleFile" />
														</s:elseif>
													</td>
												</tr>
											</s:iterator>
										</table>
									</s:if>
									<s:else>
										<s:text name="none" />
									</s:else>
								</td>
							</tr>
						</s:if>

						<!-- 流程关键测评指标板 -->
						<s:if test='mark=="a15" && value=="1"'>
							<tr
								style="background-color: #F8F8F8; font: bold 12px ; height: 20px;"
								align="left">
								<td>
									<s:property value="#index" />、<s:property value="name" />
									<s:set name="index" value="#index+1" />
								</td>
							</tr>
							<tr>
								<td style="padding-left: 10px;" align="left">
									<s:if test="processShowFile.kpiShowValues!=null">
										<table width="100%" border="0" cellpadding="0" cellspacing="1"
											align="center" bgcolor="#BFBFBF" style="table-layout: fixed;">
											<tr class="FixedTitleRow" align="center">
											   <s:iterator value="processShowFile.kpiShowValues.kpiTitles" id="kpiTitle" status="st">
											      <td bgcolor="#DDDDDD" width="10%" height="25" class="t8"
													style="word-break: break-all;">
													    <s:property value="kpiTitle"/>
													</td>
											   </s:iterator>
											</tr>
											<s:iterator value="processShowFile.kpiShowValues.kpiRowValues" id="rowValues" status="st">
												<tr align="center" bgcolor="#ffffff"
													onmouseover="style.backgroundColor='#EFEFEF';"
													onmouseout="style.backgroundColor='#ffffff';" height="20">
													<s:iterator value="processShowFile.kpiShowValues.kpiRowValues[#st.index]" id="cellValue" status="st">
													    <td class="t5" style="word-break: break-all;">
														   <s:property value="cellValue"/>
														</td>
													</s:iterator>
												</tr>
											</s:iterator>
										</table>
									</s:if>
									<s:else>
										<s:text name="none" />
									</s:else>
								</td>
							</tr>
						</s:if>

						<!-- 客户 -->
						<s:if test='mark=="a16" && value=="1"'>
							<tr
								style="background-color: #F8F8F8; font: bold 12px ; height: 20px;"
								align="left">
								<td>
									<s:property value="#index" />、<s:property value="name" />
									<s:set name="index" value="#index+1" />
								</td>
							</tr>
							<tr>
								<td style="padding-left: 10px; word-break: break-all;"
									align="left">
									<s:if test="processShowFile.flowCustom == ''">
										<s:text name="none" />
									</s:if>
									<s:else>
										<s:property value="processShowFile.flowCustom" escape="false" />
									</s:else>
								</td>
							</tr>
						</s:if>

						<!-- 不适用范围 -->
						<s:if test='mark=="a17" && value=="1"'>
							<tr
								style="background-color: #F8F8F8; font: bold 12px ; height: 20px;"
								align="left">
								<td>
									<s:property value="#index" />、<s:property value="name" />
									<s:set name="index" value="#index+1" />
								</td>
							</tr>
							<tr>
								<td style="padding-left: 10px; word-break: break-all;"
									align="left">
									<s:if test="processShowFile.noApplicability == ''">
										<s:text name="none" />
									</s:if>
									<s:else>
										<s:property value="processShowFile.noApplicability"
											escape="false" />
									</s:else>
								</td>
							</tr>
						</s:if>

						<!-- 概述 -->
						<s:if test='mark=="a18" && value=="1"'>
							<tr
								style="background-color: #F8F8F8; font: bold 12px ; height: 20px;"
								align="left">
								<td>
									<s:property value="#index" />、<s:property value="name" />
									<s:set name="index" value="#index+1" />
								</td>
							</tr>
							<tr>
								<td style="padding-left: 10px; word-break: break-all;"
									align="left">
									<s:if test="processShowFile.flowSummarize == ''">
										<s:text name="none" />
									</s:if>
									<s:else>
										<s:property value="processShowFile.flowSummarize"
											escape="false" />
									</s:else>
								</td>
							</tr>
						</s:if>

						<!-- 补充说明 -->
						<s:if test='mark=="a19" && value=="1"'>
							<tr
								style="background-color: #F8F8F8; font: bold 12px ; height: 20px;"
								align="left">
								<td>
									<s:property value="#index" />、<s:property value="name" />
									<s:set name="index" value="#index+1" />
								</td>
							</tr>
							<tr>
								<td style="padding-left: 10px; word-break: break-all;"
									align="left">
									<s:if test="processShowFile.flowSupplement == ''">
										<s:text name="none" />
									</s:if>
									<s:else>
										<s:property value="processShowFile.flowSupplement"
											escape="false" />
									</s:else>
								</td>
							</tr>
						</s:if>

						<!-- 记录保存 -->
						<s:if test='mark=="a20" && value=="1"'>
							<tr
								style="background-color: #F8F8F8; font: bold 12px ; height: 20px;"
								align="left">
								<td>
									<s:property value="#index" />、<s:property value="name" />
									<s:set name="index" value="#index+1" />
								</td>
							</tr>
							<tr>
								<td style="padding-left: 10px;" align="left">
									<s:if test="processShowFile.flowRecordList.size>0">
										<table width="100%" border="0" cellpadding="0" cellspacing="1"
											align="center" bgcolor="#BFBFBF" style="table-layout: fixed;">
											<tr class="FixedTitleRow" align="center">
												<td bgcolor="#DDDDDD" width="10%" height="25" class="t8"
													style="word-break: break-all;">
													<!-- 记录名称 -->
													<s:text name="recordName" />
												</td>
												<s:if test="processShowFile.showRecordTransferAndNum">
												<td bgcolor="#DDDDDD" width="10%" height="25" class="t8"
													style="word-break: break-all;">
													<!-- 编号 -->
													<s:text name="prfNumber" />
												</td>
												<td bgcolor="#DDDDDD" width="10%" height="25" class="t8"
													style="word-break: break-all;">
													<!-- 移交责任人 -->
													<s:text name="transferPeople" />
												</td>
												</s:if>
												<td bgcolor="#DDDDDD" width="10%" height="25" class="t8"
													style="word-break: break-all;">
													<!-- 保存责任人 -->
													<s:text name="saveResponsiblePersons" />
												</td>
												<td bgcolor="#DDDDDD" width="10%" height="25" class="t8"
													style="word-break: break-all;">
													<!-- 保存场所 -->
													<s:text name="savePlace" />
												</td>
												<td bgcolor="#DDDDDD" width="10%" height="25" class="t8"
													style="word-break: break-all;">
													<!-- 归档时间 -->
													<s:text name="filingTime" />
												</td>
												<td bgcolor="#DDDDDD" width="10%" height="25" class="t8"
													style="word-break: break-all;">
													<!-- 保存期限 -->
													<s:text name="storageLife" />
												</td>
												<td bgcolor="#DDDDDD" width="10%" height="25" class="t8"
													style="word-break: break-all;">
													<!-- 到期处理方式 -->
													<s:text name="treatmentDue" />
												</td>
											</tr>
											<s:iterator value="processShowFile.flowRecordList"
												status="st">
												<tr align="center" bgcolor="#ffffff"
													onmouseover="style.backgroundColor='#EFEFEF';"
													onmouseout="style.backgroundColor='#ffffff';" height="20">
													<td class="t5" style="word-break: break-all;">
														<s:property value="recordName" />
													</td>
													<s:if test="processShowFile.showRecordTransferAndNum">
													<td class="t5" style="word-break: break-all;">
														<s:property value="docId" />
													</td>
													<td class="t5" style="word-break: break-all;">
														<s:property value="recordTransferPeople" />
													</td>
													</s:if>
													<td class="t5" style="word-break: break-all;">
														<s:property value="recordSavePeople" />
													</td>
													<td class="t5" style="word-break: break-all;">
														<s:property value="saveLaction" />
													</td>
													<td class="t5" style="word-break: break-all;">
														<s:property value="fileTime" />
													</td>
													<td class="t5" style="word-break: break-all;">
														<s:property value="saveTime" />
													</td>
													<td class="t5" style="word-break: break-all;">
														<s:property value="recordApproach" />
													</td>
												</tr>
											</s:iterator>
										</table>
									</s:if>
									<s:else>
										<s:text name="none" />
									</s:else>
								</td>
							</tr>
						</s:if>

						<!-- 相关标准 -->
						<s:if test='mark=="a21" && value=="1"'>
							<tr
								style="background-color: #F8F8F8; font: bold 12px ; height: 20px;"
								align="left">
								<td>
									<s:property value="#index" />、<s:property value="name" />
									<s:set name="index" value="#index+1" />
								</td>
							</tr>
							<tr>
								<td style="padding-left: 10px;" align="left">
									<s:if test="processShowFile.standardList.size>0">
										<table width="100%" border="0" cellpadding="0" cellspacing="1"
											align="center" bgcolor="#BFBFBF" style="table-layout: fixed;">
											<tr class="FixedTitleRow" align="center">
												<s:if test="#application.isHiddenBDF==true">
												<td bgcolor="#DDDDDD" width="100%" height="25" class="t8"
													style="word-break: break-all;">
													<!-- 文件名称 -->
													<s:text name="fileName" />
												</td>
												</s:if>
												<s:else>
												<td bgcolor="#DDDDDD" width="10%" height="25" class="t8"
													style="word-break: break-all;">
													<!-- 标准名称 -->
													<s:text name="standardName" />
												</td>
												<td bgcolor="#DDDDDD" width="10%" height="25" class="t8"
													style="word-break: break-all;">
													<!-- 类型 -->
													<s:text name="type" />
												</td>
												</s:else>
											</tr>
											<s:iterator value="processShowFile.standardList">
												<tr align="center" bgcolor="#ffffff"
													onmouseover="style.backgroundColor='#EFEFEF';"
													onmouseout="style.backgroundColor='#ffffff';" height="20">
													<td class="t5" style="word-break: break-all;">
														<a
															href="${basePath}standard.action?from=process&standardType=<s:property value='stanType'/>&reqType=public&standardId=<s:property value='criterionClassId'/>&fileId=<s:property value='relationId'/>"
															style="cursor: pointer;" target='_blank' class="table">
															<s:property value="criterionClassName" /> </a>
													</td>
													<s:if test="#application.isHiddenBDF==false">
													<td class="t5" style="word-break: break-all;">
														<s:if test="stanType == 0">
															<!-- 目录 -->
															<s:text name="directory" />
														</s:if>
														<s:elseif test="stanType == 1">
															<!-- 文件 -->
															<s:text name="fileStandar" />
														</s:elseif>
														<s:elseif test="stanType == 2">
															<!-- 流程图 -->
															<s:text name="flowStandar" />
														</s:elseif>
														<s:elseif test="stanType == 3">
															<!-- 流程地图 -->
															<s:text name="flowMapStandar" />
														</s:elseif>
														<s:elseif test="stanType == 4">
															<!-- 标准条款 -->
															<s:text name="standarClause" />
														</s:elseif>
														<s:elseif test="stanType == 5">
															<!-- 条款要求 -->
															<s:text name="clauseRequest" />
														</s:elseif>
													</td>
													</s:if>
												</tr>
											</s:iterator>
										</table>
									</s:if>
									<s:else>
										<s:text name="none" />
									</s:else>
								</td>
							</tr>
						</s:if>
						<!-- 流程边界 -->
						<s:if test='mark=="a22" && value=="1"'>
						   <tr
								style="background-color: #F8F8F8; font: bold 12px ; height: 20px;"
								align="left">
								<td>
									<s:property value="#index" />、<s:property value="name" />
									<s:set name="index" value="#index+1" />
								</td>
							</tr>
							<tr >
							   <td style="padding-left: 10px;" align="left">
								   <table width="100%" border="0" cellpadding="0" cellspacing="1"
										bgcolor="#BFBFBF" style="table-layout: fixed;">
										<tr>
											<td width="20%" height="25" bgcolor="#DDDDDD" class="t6">
												<!-- 起始活动 -->
												<s:text name="initialActivity" />
											</td>
											<td bgcolor="#FFFFFF" style="word-break: break-all;">
											   <s:property value="processShowFile.startEndActive[0]"/>
											</td>
										</tr>
										<tr>
										    <td width="20%" height="25" bgcolor="#DDDDDD" class="t6">
												<!-- 终止活动 -->
												<s:text name="ceasingActivity" />
											</td>
											<td bgcolor="#FFFFFF" style="word-break: break-all;">
											    <s:property value="processShowFile.startEndActive[1]"/>
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</s:if>
                        <!-- 流程责任属性 -->
						<s:if test='mark=="a23" && value=="1"'>
						   <tr
								style="background-color: #F8F8F8; font: bold 12px ; height: 20px;"
								align="left">
								<td>
									<s:property value="#index" />、<s:property value="name" />
									<s:set name="index" value="#index+1" />
								</td>
							</tr>
							<tr >
							   <td style="padding-left: 10px;" align="left">
								   <table width="100%" border="0" cellpadding="0" cellspacing="1"
										bgcolor="#BFBFBF" style="table-layout: fixed;">
										<!-- 流程监护人 -->
										<s:if test="artificialPersonValue == 1">
											<tr>
												<td width="20%" height="25" bgcolor="#DDDDDD" class="t6">
													<!-- 流程监护人 -->
													<s:text name="processTheGuardian" />
												</td>
												<td bgcolor="#FFFFFF" style="word-break: break-all;">
													<s:property value="processShowFile.responFlow.guardianName" />
												</td>
											</tr>
										</s:if>

										<tr>
										    <td width="20%" height="25" bgcolor="#DDDDDD" class="t6">
												<!-- 流程责任人 -->
												<s:text name="processResponsiblePersons" />
											</td>
											<td bgcolor="#FFFFFF" style="word-break: break-all;">
											    <s:property value="processShowFile.responFlow.dutyUserName"/>
											</td>
										</tr>
										 <!-- 流程拟制人 -->
										<s:if test="responsibilityPeopleValue == 1">
											<tr>
												<td width="20%" height="25" bgcolor="#DDDDDD" class="t6">
													<!-- 流程拟制人 -->
													<s:text name="processArtificialPerson" />
												</td>
												<td bgcolor="#FFFFFF" style="word-break: break-all;">
													<s:property
														value="processShowFile.responFlow.fictionPeopleName" />
												</td>
											</tr>
										</s:if>
										<tr>
										    <td width="20%" height="25" bgcolor="#DDDDDD" class="t6">
												<!-- 流程责任部门 -->
												<s:text name="processResponsibilityDepartment" />
											</td>
											<td bgcolor="#FFFFFF" style="word-break: break-all;">
											    <s:property value="processShowFile.responFlow.dutyOrgName"/>
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</s:if>
						
						<!-- 流程文控信息 -->
						<s:if test='mark=="a24" && value=="1"'>
						   <tr
								style="background-color: #F8F8F8; font: bold 12px ; height: 20px;"
								align="left">
								<td>
									<s:property value="#index" />、<s:property value="name" />
									<s:set name="index" value="#index+1" />
								</td>
							</tr>
							<tr>
								<td style="padding-left: 10px;" align="left">
									<s:if test="processShowFile.documentList.size>0">
										<table width="100%" border="0" cellpadding="0" cellspacing="1"
											align="center" bgcolor="#BFBFBF" style="table-layout: fixed;">
											<tr class="FixedTitleRow" align="center">
												<td bgcolor="#DDDDDD" width="10%" height="25" class="t8"
													style="word-break: break-all;">
													<!-- 版本号 -->
													<s:text name="versionNumber" />
												</td>
												<td bgcolor="#DDDDDD" width="10%" height="25" class="t8"
													style="word-break: break-all;">
													<!-- 变更说明 -->
													<s:text name="changeThat" />
												</td>
												<td bgcolor="#DDDDDD" width="10%" height="25" class="t8"
													style="word-break: break-all;">
													<!-- 发布日期 -->
													<s:text name="releaseDate" />
												</td>
												
												<td bgcolor="#DDDDDD" width="10%" height="25" class="t8"
													style="word-break: break-all;">
													<s:if test="otherOperaType==1">
													<!-- 流程拟制人 -->
													<s:text name="processArtificialPerson" />
													</s:if>
													<s:else>
													    <!--拟稿人-->
													    <s:text name="peopleMakeADraft" />
													</s:else>
												</td>
												<td bgcolor="#DDDDDD" width="10%" height="25" class="t8"
													style="word-break: break-all;">
													<!-- 审批人-->
													<s:text name="theApprover" />
												</td>
											</tr>
											<s:iterator value="processShowFile.documentList" id="st" >
												<tr align="center" bgcolor="#ffffff"
													onmouseover="style.backgroundColor='#EFEFEF';"
													onmouseout="style.backgroundColor='#ffffff';" height="20">
													<td class="t5" style="word-break: break-all;">
														<s:property value="#st[0]" /> 
													</td>
													<td class="t5" style="word-break: break-all;">
														<s:property value="#st[1]" escape="false"/>
													</td>
													<td class="t5" style="word-break: break-all;">
														<s:property value="#st[2]" />
													</td>
													<td class="t5" style="word-break: break-all;">
														<s:property value="#st[3]" />
													</td>
													<td class="t5" style="word-break: break-all;" align="left">
														<s:property value="#st[4]" escape="false"/>
													</td>
												</tr>
											</s:iterator>
										</table>
									</s:if>
									<s:else>
										<s:text name="none" />
									</s:else>
								</td>
							</tr>
						</s:if>
						
						
						<!-- 相关风险 -->
						<s:if test='mark=="a25" && value=="1"'>
							<tr
								style="background-color: #F8F8F8; font: bold 12px ; height: 20px;"
								align="left">
								<td>
									<s:property value="#index" />、<s:property value="name" />
									<s:set name="index" value="#index+1" />
								</td>
							</tr>
							<tr>
								<td style="padding-left: 10px;" align="left">
									<s:if test="processShowFile.riskList.size>0">
										<table width="100%" border="0" cellpadding="0" cellspacing="1"
											align="center" bgcolor="#BFBFBF" style="table-layout: fixed;">
											<tr class="FixedTitleRow" align="center">
												<td bgcolor="#DDDDDD" width="10%" height="25" class="t8"
													style="word-break: break-all;">
													<!-- 风险编号 -->
													<s:text name="riskNum" />
												</td>
												<td bgcolor="#DDDDDD" width="10%" height="25" class="t8"
													style="word-break: break-all;">
													<!-- 风险描述 -->
													<s:text name="riskName" />
												</td>
											</tr>
											<s:iterator value="processShowFile.riskList" status="st">
												<tr align="center" bgcolor="#ffffff"
													onmouseover="style.backgroundColor='#EFEFEF';"
													onmouseout="style.backgroundColor='#ffffff';" height="20">
													<td class="t5" style="word-break: break-all;">
														<a
															href="${basePath}getRiskById.action?riskDetailId=<s:property value='id'/>"
															target='_blank' style="cursor: pointer;" class="table">
															<s:property value="riskCode" /> </a>
													</td>
													<td class="t5" style="word-break: break-all;">
														<s:property value="riskName" />
													</td>
												</tr>
											</s:iterator>
										</table>
									</s:if>
									<s:else>
										<s:text name="none" />
									</s:else>
								</td>
							</tr>
						</s:if>
						<!-- 自定义要素1 -->
						<s:if test='mark=="a27" && value=="1"'>
							<tr
								style="background-color: #F8F8F8; font: bold 12px ; height: 20px;"
								align="left">
								<td>
									<s:property value="#index" />、<s:property value="name" />
									<s:set name="index" value="#index+1" />
								</td>
							</tr>
							<tr>
								<td style="padding-left: 10px; word-break: break-all;"
									align="left">
									<s:if test="processShowFile.flowCustomOne == ''">
										<s:text name="none" />
									</s:if>
									<s:else>
										<s:property value="processShowFile.flowCustomOne" escape="false" />
									</s:else>
								</td>
							</tr>
						</s:if>
						<!-- 自定义要素2 -->
						<s:if test='mark=="a28" && value=="1"'>
							<tr
								style="background-color: #F8F8F8; font: bold 12px ; height: 20px;"
								align="left">
								<td>
									<s:property value="#index" />、<s:property value="name" />
									<s:set name="index" value="#index+1" />
								</td>
							</tr>
							<tr>
								<td style="padding-left: 10px; word-break: break-all;"
									align="left">
									<s:if test="processShowFile.flowCustomTwo == ''">
										<s:text name="none" />
									</s:if>
									<s:else>
										<s:property value="processShowFile.flowCustomTwo" escape="false" />
									</s:else>
								</td>
							</tr>
						</s:if>
						<!-- 自定义要素3 -->
						<s:if test='mark=="a29" && value=="1"'>
							<tr
								style="background-color: #F8F8F8; font: bold 12px ; height: 20px;"
								align="left">
								<td>
									<s:property value="#index" />、<s:property value="name" />
									<s:set name="index" value="#index+1" />
								</td>
							</tr>
							<tr>
								<td style="padding-left: 10px; word-break: break-all;"
									align="left">
									<s:if test="processShowFile.flowCustomThree == ''">
										<s:text name="none" />
									</s:if>
									<s:else>
										<s:property value="processShowFile.flowCustomThree" escape="false" />
									</s:else>
								</td>
							</tr>
						</s:if>
						<!-- 自定义要素4 -->
						<s:if test='mark=="a30" && value=="1"'>
							<tr
								style="background-color: #F8F8F8; font: bold 12px ; height: 20px;"
								align="left">
								<td>
									<s:property value="#index" />、<s:property value="name" />
									<s:set name="index" value="#index+1" />
								</td>
							</tr>
							<tr>
								<td style="padding-left: 10px; word-break: break-all;"
									align="left">
									<s:if test="processShowFile.flowCustomFour == ''">
										<s:text name="none" />
									</s:if>
									<s:else>
										<s:property value="processShowFile.flowCustomFour" escape="false" />
									</s:else>
								</td>
							</tr>
						</s:if>
						<!-- 自定义要素5 -->
						<s:if test='mark=="a31" && value=="1"'>
							<tr
								style="background-color: #F8F8F8; font: bold 12px ; height: 20px;"
								align="left">
								<td>
									<s:property value="#index" />、<s:property value="name" />
									<s:set name="index" value="#index+1" />
								</td>
							</tr>
							<tr>
								<td style="padding-left: 10px; word-break: break-all;"
									align="left">
									<s:if test="processShowFile.flowCustomFive == ''">
										<s:text name="none" />
									</s:if>
									<s:else>
										<s:property value="processShowFile.flowCustomFive" escape="false" />
									</s:else>
								</td>
							</tr>
						</s:if>
						<!-- 流程范围 -->
						<s:if test='mark=="a33" && value=="1"'>
							<tr
								style="background-color: #F8F8F8; font: bold 12px ; height: 20px;"
								align="left">
								<td>
									<s:property value="#index" />、<s:property value="name" />
									<s:set name="index" value="#index+1" />
								</td>
							</tr>
							<tr>
							   <td style="padding-left: 10px;" align="left">
								   <table width="100%" border="0" cellpadding="0" cellspacing="1"
										bgcolor="#BFBFBF" style="table-layout: fixed;">
										<tr>
											<td width="20%" height="25" bgcolor="#DDDDDD" class="t6">
												<!-- 起始活动 -->
												<s:text name="initialActivity" />
											</td>
											<td bgcolor="#FFFFFF" style="word-break: break-all;">
											   <s:property value="processShowFile.startEndActive[0]"/>
											</td>
										</tr>
										<tr>
										    <td width="20%" height="25" bgcolor="#DDDDDD" class="t6">
												<!-- 终止活动 -->
												<s:text name="ceasingActivity" />
											</td>
											<td bgcolor="#FFFFFF" style="word-break: break-all;">
											    <s:property value="processShowFile.startEndActive[1]"/>
											</td>
										</tr>
										<tr>
										    <td width="20%" height="25" bgcolor="#DDDDDD" class="t6">
												<!-- 输入 -->
												<s:text name="input" />
											</td>
											<td bgcolor="#FFFFFF" style="word-break: break-all;">
											    <s:property value="processShowFile.flowInput"/>
											</td>
										</tr>
										<tr>
										    <td width="20%" height="25" bgcolor="#DDDDDD" class="t6">
												<!-- 输出 -->
												<s:text name="output" />
											</td>
											<td bgcolor="#FFFFFF" style="word-break: break-all;">
											    <s:property value="processShowFile.flowOutput"/>
											</td>
										</tr>
										
									</table>
								</td>
							</tr>
						</s:if>
						<!-- 术语 -->
						<s:if test='mark=="a34" && value=="1"'>
							<tr
								style="background-color: #F8F8F8; font: bold 12px ; height: 20px;"
								align="left">
								<td>
									<s:property value="#index" />、<s:property value="name" />
									<s:set name="index" value="#index+1" />
								</td>
							</tr>
							<tr>
								<td style="padding-left: 10px;" align="left">
									<s:if test="processShowFile.definitions.size>0">
										<table width="100%" border="0" cellpadding="0" cellspacing="1"
											align="center" bgcolor="#BFBFBF" style="table-layout: fixed;">
											<tr class="FixedTitleRow" align="center">
												<td bgcolor="#DDDDDD" width="10%" height="25" class="t8"
													style="word-break: break-all;">
													<!-- 名称 -->
													<s:text name="name" />
												</td>
												<td bgcolor="#DDDDDD" width="10%" height="25" class="t8"
													style="word-break: break-all;">
													<!-- 定义 -->
													<s:text name="definition" />
												</td>
											</tr>
											<s:iterator value="processShowFile.definitions"
												status="st">
												<tr align="center" bgcolor="#ffffff"
													onmouseover="style.backgroundColor='#EFEFEF';"
													onmouseout="style.backgroundColor='#ffffff';" height="20">
													<td class="t5" style="word-break: break-all;">
														<s:property value="name" />
													</td>
													<td class="t5" style="word-break: break-all;">
														<s:property value="instructions" />
													</td>
												</tr>
											</s:iterator>
										</table>
									</s:if>
									<s:else>
										<s:text name="none" />
									</s:else>
								</td>
							</tr>
						</s:if>
						<!-- 流程相关文件 -->
						<s:if test='mark=="a35" && value=="1"'>
							<tr
								style="background-color: #F8F8F8; font: bold 12px ; height: 20px;"
								align="left">
								<td>
									<s:property value="#index" />、<s:property value="name" />
									<s:set name="index" value="#index+1" />
								</td>
							</tr>
							<tr>
								<td style="padding-left: 10px;" align="left">
									<s:if test="processShowFile.relatedFiles.size>0">
										<table width="100%" border="0" cellpadding="0" cellspacing="1"
											align="center" bgcolor="#BFBFBF" style="table-layout: fixed;">
											<tr class="FixedTitleRow" align="center">
												<td bgcolor="#DDDDDD" width="10%" height="25" class="t8"
													style="word-break: break-all;">
													<!-- 名称 -->
													<s:text name="fileName" />
												</td>
												<td bgcolor="#DDDDDD" width="10%" height="25" class="t8"
													style="word-break: break-all;">
													<!-- 编号 -->
													<s:text name="fileCode" />
												</td>
											</tr>
											<s:iterator value="processShowFile.relatedFiles"
												status="st">
												<tr align="center" bgcolor="#ffffff"
													onmouseover="style.backgroundColor='#EFEFEF';"
													onmouseout="style.backgroundColor='#ffffff';" height="20">
													<td class="t5" style="word-break: break-all;">
														<s:property value="fileName" />
													</td>
													<td class="t5" style="word-break: break-all;">
														<s:property value="fileNum" />
													</td>
												</tr>
											</s:iterator>
										</table>
									</s:if>
									<s:else>
										<s:text name="none" />
									</s:else>
								</td>
							</tr>
						</s:if>
						<!-- 流程接口描述 -->
						<s:if test='mark=="a36" && value=="1"'>
							<tr
								style="background-color: #F8F8F8; font: bold 12px ; height: 20px;"
								align="left">
								<td>
									<s:property value="#index" />、<s:property value="name" />
									<s:set name="index" value="#index+1" />
								</td>
							</tr>
							<tr
								style="background-color: #F8F8F8; font: bold 12px ; height: 20px;"
								align="left">
								<td>
								    *<s:text name="correspondingUpProcess" />
								</td>
							</tr>
							<tr>
								<td style="padding-left: 10px;" align="left">
									<s:if test="processShowFile.processInterfacesDescriptionBean.pid!=0">
										<table width="100%" border="0" cellpadding="0" cellspacing="1"
											align="center" bgcolor="#BFBFBF" style="table-layout: fixed;">
											<tr class="FixedTitleRow" align="center">
												<td bgcolor="#DDDDDD" width="10%" height="25" class="t8"
													style="word-break: break-all;">
													<!-- 名称 -->
													<s:text name="processFileName" />
												</td>
												<td bgcolor="#DDDDDD" width="10%" height="25" class="t8"
													style="word-break: break-all;">
													<!-- 编号 -->
													<s:text name="fileCode" />
												</td>
											</tr>
											<tr align="center" bgcolor="#ffffff"
												onmouseover="style.backgroundColor='#EFEFEF';"
												onmouseout="style.backgroundColor='#ffffff';" height="20">
												<td class="t5" style="word-break: break-all;">
													<s:property value="processShowFile.processInterfacesDescriptionBean.pname" />
												</td>
												<td class="t5" style="word-break: break-all;">
													<s:property value="processShowFile.processInterfacesDescriptionBean.pnumber" />
												</td>
											</tr>
										</table>
									</s:if>
									<s:else>
										<s:text name="none" />
									</s:else>
								</td>
							</tr>
							
							<!-- 输入的流程接口描述 -->
							<tr
								style="background-color: #F8F8F8; font: bold 12px ; height: 20px;"
								align="left">
								<td>
								    *<s:text name="inProcessDescribe" />
								</td>
							</tr>
							<tr>
								<td style="padding-left: 10px;" align="left">
									<s:if test="processShowFile.processInterfacesDescriptionBean.uppers.size>0">
										<table width="100%" border="0" cellpadding="0" cellspacing="1"
											align="center" bgcolor="#BFBFBF" style="table-layout: fixed;">
											<tr class="FixedTitleRow" align="center">
												<td bgcolor="#DDDDDD" width="10%" height="25" class="t8"
													style="word-break: break-all;">
													<!-- 流程接口 -->
													<s:text name="flowInterface" />
												</td>
												<td bgcolor="#DDDDDD" width="10%" height="25" class="t8"
													style="word-break: break-all;">
													<!-- 上游流程 -->
													<s:text name="topFlow" />
												</td>
												<td bgcolor="#DDDDDD" width="10%" height="25" class="t8"
													style="word-break: break-all;">
													<!-- 流程要求 -->
													<s:text name="processRequired" />
												</td>
												<td bgcolor="#DDDDDD" width="10%" height="25" class="t8"
													style="word-break: break-all;">
													<!-- 备注 -->
													<s:text name="remark" />
												</td>
											</tr>
											<s:iterator value="processShowFile.processInterfacesDescriptionBean.uppers"
												status="st">
												<tr align="center" bgcolor="#ffffff"
													onmouseover="style.backgroundColor='#EFEFEF';"
													onmouseout="style.backgroundColor='#ffffff';" height="20">
													<td class="t5" style="word-break: break-all;">
														<s:property value="implName" />
													</td>
													<td class="t5" style="word-break: break-all;">
														<s:property value="rname" />
													</td>
													<td class="t5" style="word-break: break-all;">
														<s:property value="processRequirements" />
													</td>
													<td class="t5" style="word-break: break-all;">
														<s:property value="implNote" />
													</td>
												</tr>
											</s:iterator>
										</table>
									</s:if>
									<s:else>
										<s:text name="none" />
									</s:else>
								</td>
							</tr>
							
							<!-- 输出的流程接口描述 -->
							<tr
								style="background-color: #F8F8F8; font: bold 12px ; height: 20px;"
								align="left">
								<td>
								    *<s:text name="outProcessDescribed" />
								</td>
							</tr>
							<tr>
								<td style="padding-left: 10px;" align="left">
									<s:if test="processShowFile.processInterfacesDescriptionBean.lowers.size>0">
										<table width="100%" border="0" cellpadding="0" cellspacing="1"
											align="center" bgcolor="#BFBFBF" style="table-layout: fixed;">
											<tr class="FixedTitleRow" align="center">
												<td bgcolor="#DDDDDD" width="10%" height="25" class="t8"
													style="word-break: break-all;">
													<!-- 流程接口 -->
													<s:text name="flowInterface" />
												</td>
												<td bgcolor="#DDDDDD" width="10%" height="25" class="t8"
													style="word-break: break-all;">
													<!-- 下游流程 -->
													<s:text name="bottomFlow" />
												</td>
												<td bgcolor="#DDDDDD" width="10%" height="25" class="t8"
													style="word-break: break-all;">
													<!-- 流程要求 -->
													<s:text name="processRequired" />
												</td>
												<td bgcolor="#DDDDDD" width="10%" height="25" class="t8"
													style="word-break: break-all;">
													<!-- 备注 -->
													<s:text name="remark" />
												</td>
											</tr>
											<s:iterator value="processShowFile.processInterfacesDescriptionBean.lowers"
												status="st">
												<tr align="center" bgcolor="#ffffff"
													onmouseover="style.backgroundColor='#EFEFEF';"
													onmouseout="style.backgroundColor='#ffffff';" height="20">
													<td class="t5" style="word-break: break-all;">
														<s:property value="implName" />
													</td>
													<td class="t5" style="word-break: break-all;">
														<s:property value="rname" />
													</td>
													<td class="t5" style="word-break: break-all;">
														<s:property value="processRequirements" />
													</td>
													<td class="t5" style="word-break: break-all;">
														<s:property value="implNote" />
													</td>
												</tr>
											</s:iterator>
										</table>
									</s:if>
									<s:else>
										<s:text name="none" />
									</s:else>
								</td>
							</tr>
							
							
							<!-- 过程接口 -->
							<tr
								style="background-color: #F8F8F8; font: bold 12px ; height: 20px;"
								align="left">
								<td>
								    *<s:text name="interfaceFlow" />
								</td>
							</tr>
							<tr>
								<td style="padding-left: 10px;" align="left">
									<s:if test="processShowFile.processInterfacesDescriptionBean.procedurals.size>0">
										<table width="100%" border="0" cellpadding="0" cellspacing="1"
											align="center" bgcolor="#BFBFBF" style="table-layout: fixed;">
											<tr class="FixedTitleRow" align="center">
												<td bgcolor="#DDDDDD" width="10%" height="25" class="t8"
													style="word-break: break-all;">
													<!-- 流程接口 -->
													<s:text name="flowInterface" />
												</td>
												<td bgcolor="#DDDDDD" width="10%" height="25" class="t8"
													style="word-break: break-all;">
													<!-- 过程接口流程 -->
													<s:text name="interfaceFlow" />
												</td>
												<td bgcolor="#DDDDDD" width="10%" height="25" class="t8"
													style="word-break: break-all;">
													<!-- 流程要求 -->
													<s:text name="processRequired" />
												</td>
												<td bgcolor="#DDDDDD" width="10%" height="25" class="t8"
													style="word-break: break-all;">
													<!-- 备注 -->
													<s:text name="remark" />
												</td>
											</tr>
											<s:iterator value="processShowFile.processInterfacesDescriptionBean.procedurals"
												status="st">
												<tr align="center" bgcolor="#ffffff"
													onmouseover="style.backgroundColor='#EFEFEF';"
													onmouseout="style.backgroundColor='#ffffff';" height="20">
													<td class="t5" style="word-break: break-all;">
														<s:property value="implName" />
													</td>
													<td class="t5" style="word-break: break-all;">
														<s:property value="rname" />
													</td>
													<td class="t5" style="word-break: break-all;">
														<s:property value="processRequirements" />
													</td>
													<td class="t5" style="word-break: break-all;">
														<s:property value="implNote" />
													</td>
												</tr>
											</s:iterator>
										</table>
									</s:if>
									<s:else>
										<s:text name="none" />
									</s:else>
								</td>
							</tr>
							
							
							<!-- 子流程 -->
							<tr
								style="background-color: #F8F8F8; font: bold 12px ; height: 20px;"
								align="left">
								<td>
								    *<s:text name="sonFlow" />
								</td>
							</tr>
							<tr>
								<td style="padding-left: 10px;" align="left">
									<s:if test="processShowFile.processInterfacesDescriptionBean.sons.size>0">
										<table width="100%" border="0" cellpadding="0" cellspacing="1"
											align="center" bgcolor="#BFBFBF" style="table-layout: fixed;">
											<tr class="FixedTitleRow" align="center">
												<td bgcolor="#DDDDDD" width="10%" height="25" class="t8"
													style="word-break: break-all;">
													<!-- 流程接口 -->
													<s:text name="flowInterface" />
												</td>
												<td bgcolor="#DDDDDD" width="10%" height="25" class="t8"
													style="word-break: break-all;">
													<!-- 子流程 -->
													<s:text name="sonFlow" />
												</td>
												<td bgcolor="#DDDDDD" width="10%" height="25" class="t8"
													style="word-break: break-all;">
													<!-- 流程要求 -->
													<s:text name="processRequired" />
												</td>
												<td bgcolor="#DDDDDD" width="10%" height="25" class="t8"
													style="word-break: break-all;">
													<!-- 备注 -->
													<s:text name="remark" />
												</td>
											</tr>
											<s:iterator value="processShowFile.processInterfacesDescriptionBean.sons"
												status="st">
												<tr align="center" bgcolor="#ffffff"
													onmouseover="style.backgroundColor='#EFEFEF';"
													onmouseout="style.backgroundColor='#ffffff';" height="20">
													<td class="t5" style="word-break: break-all;">
														<s:property value="implName" />
													</td>
													<td class="t5" style="word-break: break-all;">
														<s:property value="rname" />
													</td>
													<td class="t5" style="word-break: break-all;">
														<s:property value="processRequirements" />
													</td>
													<td class="t5" style="word-break: break-all;">
														<s:property value="implNote" />
													</td>
												</tr>
											</s:iterator>
										</table>
									</s:if>
									<s:else>
										<s:text name="none" />
									</s:else>
								</td>
							</tr>
							
						</s:if>
					</s:iterator>
					<tr></tr>
				</table>
			</div>
		</div>
	</body>
</html>