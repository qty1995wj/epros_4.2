package epros.draw.gui.workflow.aid;

import javax.swing.JScrollPane;

import epros.draw.constant.JecnLogConstants;
import epros.draw.error.ErrorInfoConstant;
import epros.draw.gui.swing.JecnBaseFlowElementPanel;
import epros.draw.gui.workflow.JecnDrawMainPanel;
import epros.draw.gui.workflow.tabbedPane.JecnDrawDesktopPane;
import epros.draw.util.DrawCommon;

/**
 * 
 * 画图面板的辅助组件
 * 
 * 
 * @author ZHOUXY
 * 
 */
public class JecnAidCompCollection {
	/** 画图面板 */
	private JecnDrawDesktopPane workflow = null;

	/** 点击面板拖动鼠标显示选中区域框 */
	private JecnSelectAreaPanel selecedAreaPanel = null;
	/** 双击流程元素图形进入编辑使用的文本框 */
	private JecnTextArea editTextArea = null;
	/** 快速添加图形 */
	private JecnLinkFigureMainProcess linkFigureMainProcess = null;
	/** 复制粘贴 选中图形区域X坐标最大值(原始状态下的值) */
	private static int leftMinX = 0;
	/** 复制粘贴 选中图形区域Y坐标最大值(原始状态下的值) */
	private static int leftMinY = 0;
	
	/** 单击TO元素显示的面板**/
	private JecnElemRelatedActivePanel relatedActivePanel=null;

	public JecnAidCompCollection(JecnDrawDesktopPane workflow) {
		if (workflow == null) {
			JecnLogConstants.log_JecnAidCompCollection
					.error("JecnAidCompCollection类构造函数：参数为null");
			throw new IllegalArgumentException(ErrorInfoConstant.PARA_OBJ_NULL);
		}
		this.workflow = workflow;
		initComponents();
	}

	/**
	 * 
	 * 初始化组件
	 * 
	 */
	private void initComponents() {
		// 点击面板拖动鼠标显示选中区域框
		selecedAreaPanel = new JecnSelectAreaPanel(workflow);
		// 双击流程元素图形进入编辑使用的文本框
		editTextArea = new JecnTextArea(workflow);

		// 快速添加图形
		linkFigureMainProcess = new JecnLinkFigureMainProcess(this);
		
		// TO元素的单击显示面板
		relatedActivePanel = new JecnElemRelatedActivePanel(workflow);
	}

	public JecnElemRelatedActivePanel getRelatedActivePanel() {
		return relatedActivePanel;
	}

	public JecnSelectAreaPanel getSelecedAreaPanel() {
		return selecedAreaPanel;
	}

	public JecnLinkFigureMainProcess getLinkFigureMainProcess() {
		return linkFigureMainProcess;
	}

	public JecnDrawDesktopPane getWorkflow() {
		return workflow;
	}

	public JecnTextArea getEditTextArea() {
		return editTextArea;
	}

	public void setEditTextArea(JecnTextArea editTextArea) {
		this.editTextArea = editTextArea;
	}

	/**
	 * 
	 * 剪切复制
	 * 
	 * 当前倍数下leftMinX值
	 * 
	 * @return
	 */
	public int getCurrLeftMinX() {
		JecnDrawDesktopPane workflow = JecnDrawMainPanel.getMainPanel()
				.getWorkflow();
		if (workflow == null || workflow.getCurrentSelectElement() == null) {
			return leftMinX;
		}
		return DrawCommon.convertDoubleToInt(leftMinX * workflow.getWorkflowScale());
	}

	/**
	 * 
	 * 剪切复制
	 * 
	 * 当前倍数下leftMinY值
	 * 
	 * @return
	 */
	public int getCurrLeftMinY() {
		JecnDrawDesktopPane workflow = JecnDrawMainPanel.getMainPanel()
				.getWorkflow();
		if (workflow == null || workflow.getCurrentSelectElement() == null) {
			return leftMinY;
		}
		return DrawCommon.convertDoubleToInt(leftMinY * workflow.getWorkflowScale());
	}

	public int getLeftMinX() {
		return leftMinX;
	}

	public void setLeftMinX(int leftMinX) {
		JecnAidCompCollection.leftMinX = leftMinX;
	}

	public int getLeftMinY() {
		return leftMinY;
	}

	public void setLeftMinY(int leftMinY) {
		JecnAidCompCollection.leftMinY = leftMinY;
	}

	/**
	 * 
	 * 移除编辑输入框所关联的流程元素
	 * 
	 * @return boolean true:移除成功；false:移除不成功
	 */
	public boolean removeFlowElemPanel() {
		return editTextArea.removeFlowElemPanel();
	}

	/**
	 * 
	 * 添加编辑输入框所关联的流程元素
	 * 
	 * @param flowElementPanel
	 *            JecnBaseFlowElementPanel 所关联的流程元素
	 */
	public void addTextAreaRefFlowElemPanel(
			JecnBaseFlowElementPanel flowElementPanel) {
		editTextArea.addFlowElementPanel(flowElementPanel);
	}

	public JScrollPane getEditScrollPane() {
		return editTextArea.getEditScrollPane();
	}

}
