package com.jecn.epros.server.service.define.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.log4j.Logger;
import org.springframework.transaction.annotation.Transactional;

import com.jecn.epros.server.bean.define.TermDefinitionLibrary;
import com.jecn.epros.server.bean.tree.JecnTreeBean;
import com.jecn.epros.server.bean.tree.JecnTreeDragBean;
import com.jecn.epros.server.common.AbsBaseService;
import com.jecn.epros.server.common.JecnCommon;
import com.jecn.epros.server.common.JecnCommonSql;
import com.jecn.epros.server.common.JecnCommonSqlTPath;
import com.jecn.epros.server.common.JecnContants;
import com.jecn.epros.server.dao.define.ITermDefinitionDao;
import com.jecn.epros.server.service.define.ITermDefinitionService;
import com.jecn.epros.server.util.JecnUtil;
import com.jecn.epros.server.util.JecnTreeEnumConstant.TreeNodeType;

@Transactional
public class TermDefinitionServiceImpl extends AbsBaseService<TermDefinitionLibrary, Long> implements
		ITermDefinitionService {
	private Logger log = Logger.getLogger(TermDefinitionServiceImpl.class);
	private ITermDefinitionDao termDefinitionDao;

	public ITermDefinitionDao getTermDefinitionDao() {
		return termDefinitionDao;
	}

	public void setTermDefinitionDao(ITermDefinitionDao termDefinitionDao) {
		this.termDefinitionDao = termDefinitionDao;
	}

	@Override
	public TermDefinitionLibrary createDir(TermDefinitionLibrary termDefinitionLibrary) throws Exception {
		termDefinitionDao.save(termDefinitionLibrary);
		updateTpathAndTLevel(termDefinitionLibrary);
		JecnUtil.saveJecnJournal(termDefinitionLibrary.getId(), termDefinitionLibrary.getName(), 15, 1,
				termDefinitionLibrary.getUpdatePersonId(), termDefinitionDao);
		return termDefinitionLibrary;

	}

	@Override
	public TermDefinitionLibrary createTermDefine(TermDefinitionLibrary termDefinitionLibrary) throws Exception {
		termDefinitionDao.save(termDefinitionLibrary);
		updateTpathAndTLevel(termDefinitionLibrary);
		JecnUtil.saveJecnJournal(termDefinitionLibrary.getId(), termDefinitionLibrary.getName(), 15, 1,
				termDefinitionLibrary.getUpdatePersonId(), termDefinitionDao);
		return termDefinitionLibrary;
	}

	@Override
	public void delete(List<Long> listIds, Long updatePersonId) throws Exception {
		String sql = JecnCommonSql.getChildObjectsSqlByType(listIds, TreeNodeType.termDefineDir);
		List<Object[]> listAll = termDefinitionDao.listNativeSql(sql);
		Set<Long> setIds = new HashSet<Long>();
		for (Object[] obj : listAll) {
			if (obj == null || obj[0] == null || obj[1] == null || obj[2] == null) {
				continue;
			}
			setIds.add(Long.valueOf(obj[0].toString()));
		}

		if (setIds.size() > 0) {
			// 添加日志
			JecnUtil.saveJecnJournals(setIds, 15, 3, updatePersonId, termDefinitionDao);
			// 制度标题-设计器
			sql = "delete from TERM_DEFINITION_LIBRARY where ID in";
			List<String> listStr = JecnCommonSql.getSetSqls(sql, setIds);
			for (String str : listStr) {
				termDefinitionDao.execteNative(str);
			}
			// 删除与术语元素的关联关系
			// 临时表
			sql = "update jecn_flow_structure_image_t set link_flow_id = null where  figure_type = 'TermFigureAR' and link_flow_id in ";
			List<String> listImageStrT = JecnCommonSql.getSetSqls(sql, setIds);
			for (String str : listImageStrT) {
				termDefinitionDao.execteNative(str);
			}
			// 正式表
			sql = "update jecn_flow_structure_image set link_flow_id = null where  figure_type = 'TermFigureAR' and link_flow_id in ";
			List<String> listImageStr = JecnCommonSql.getSetSqls(sql, setIds);
			for (String str : listImageStr) {
				termDefinitionDao.execteNative(str);
			}

		}
	}

	/**
	 * 更新tpath 和tlevel
	 * 
	 * @param jecnFlowStructureT
	 * @throws Exception
	 */
	private void updateTpathAndTLevel(TermDefinitionLibrary termDefinitionLibrary) throws Exception {
		// 获取上级节点
		TermDefinitionLibrary preTermDefinition = termDefinitionDao.get(termDefinitionLibrary.getParentId());
		if (preTermDefinition == null) {
			termDefinitionLibrary.settLevel(0);
			termDefinitionLibrary.settPath(termDefinitionLibrary.getId() + "-");
			termDefinitionLibrary.setViewSort(JecnUtil.concatViewSort("", termDefinitionLibrary.getSortId()));
		} else {
			termDefinitionLibrary.settLevel(preTermDefinition.gettLevel() + 1);
			termDefinitionLibrary.settPath(preTermDefinition.gettPath() + termDefinitionLibrary.getId() + "-");
			termDefinitionLibrary.setViewSort(JecnUtil.concatViewSort(preTermDefinition.getViewSort(),
					termDefinitionLibrary.getSortId()));
		}
		// 更新
		termDefinitionDao.update(termDefinitionLibrary);
		termDefinitionDao.getSession().flush();
	}

	@Override
	public TermDefinitionLibrary editTermDefine(TermDefinitionLibrary termDefinitionLibrary) throws Exception {
		termDefinitionLibrary.setUpdateTime(new Date());
		termDefinitionDao.update(termDefinitionLibrary);
		JecnUtil.saveJecnJournal(termDefinitionLibrary.getId(), termDefinitionLibrary.getName(), 15, 2,
				termDefinitionLibrary.getUpdatePersonId(), termDefinitionDao);

		// 更新流程图术语元素名称
		updateFlowStructureTremEle(termDefinitionLibrary);
		return termDefinitionLibrary;
	}

	/**
	 * 重命名后，更新术语元素 显示名称
	 */
	private void updateFlowStructureTremEle(TermDefinitionLibrary termDefinitionLibrary) {
		// 更新流程图术语元素名称
		String sql = "UPDATE JECN_FLOW_STRUCTURE_IMAGE_T SET FIGURE_TEXT = ? WHERE LINK_FLOW_ID = ? AND FIGURE_TYPE="
				+ JecnCommonSql.getTermFigureAR() + " AND FIGURE_TEXT<>?";
		termDefinitionDao.execteNative(sql, termDefinitionLibrary.getName(), termDefinitionLibrary.getId(),
				termDefinitionLibrary.getName());
		sql = "UPDATE JECN_FLOW_STRUCTURE_IMAGE SET FIGURE_TEXT = ? WHERE LINK_FLOW_ID = ? AND FIGURE_TYPE="
				+ JecnCommonSql.getTermFigureAR() + " AND FIGURE_TEXT<>?";
		termDefinitionDao.execteNative(sql, termDefinitionLibrary.getName(), termDefinitionLibrary.getId(),
				termDefinitionLibrary.getName());
	}

	@Override
	public List<JecnTreeBean> getChilds(Long pId) throws Exception {
		String sql = "SELECT DISTINCT T.ID,T.NAME,T.PARENT_ID,T.IS_DIR,"
				+ "    CASE WHEN SUB.ID IS NULL THEN 0 ELSE 1 END AS COUNT," + "T.SORT_ID,T.INSTRUCTIONS"
				+ "    FROM TERM_DEFINITION_LIBRARY T"
				+ "    LEFT JOIN TERM_DEFINITION_LIBRARY SUB ON SUB.PARENT_ID=T.ID"
				+ "    WHERE T.PARENT_ID=? ORDER BY T.SORT_ID";
		List<Object[]> list = termDefinitionDao.listNativeSql(sql, pId);
		return getListJecnTreeBean(list);
	}

	/**
	 * 文件对象转换成JecnTreeBean
	 * 
	 * @param obj
	 * @return
	 */
	private List<JecnTreeBean> getListJecnTreeBean(List<Object[]> list) {
		List<JecnTreeBean> listResult = new ArrayList<JecnTreeBean>();
		JecnTreeBean jecnTreeBean = null;
		for (Object[] obj : list) {
			jecnTreeBean = this.getJecnTreeBeanByObject(obj);
			if (jecnTreeBean != null) {
				listResult.add(jecnTreeBean);
			}
		}
		return listResult;
	}

	/**
	 * 文件对象转换成JecnTreeBean
	 * 
	 * @param obj
	 * @return
	 */
	private JecnTreeBean getJecnTreeBeanByObject(Object[] obj) {
		if (obj == null || obj[0] == null || obj[1] == null || obj[2] == null || obj[3] == null || obj[4] == null) {
			return null;
		}

		JecnTreeBean jecnTreeBean = new JecnTreeBean();

		// ID
		jecnTreeBean.setId(Long.valueOf(obj[0].toString()));
		// 名称
		jecnTreeBean.setName(obj[1].toString());
		// 父ID
		jecnTreeBean.setPid(Long.valueOf(obj[2].toString()));

		if ("0".equals(obj[3].toString())) {
			// 目录
			jecnTreeBean.setTreeNodeType(TreeNodeType.termDefineDir);
		} else {
			// 术语定义
			jecnTreeBean.setTreeNodeType(TreeNodeType.termDefine);
		}
		if (Integer.parseInt(obj[4].toString()) > 0) {
			// 文件节点是否有子节点
			jecnTreeBean.setChildNode(true);
		} else {
			jecnTreeBean.setChildNode(false);
		}

		if (obj[6] != null) {
			jecnTreeBean.setContent(obj[6].toString());
		}

		return jecnTreeBean;
	}

	@Override
	public boolean isExistAdd(String name, Long id, int type) throws Exception {
		String sql = "SELECT COUNT(T.ID) FROM TERM_DEFINITION_LIBRARY T WHERE T.PARENT_ID=? AND T.NAME=? AND T.IS_DIR=?";
		return termDefinitionDao.countAllByParamsNativeSql(sql, id, name, type) > 0 ? true : false;
	}

	@Override
	public boolean isExistEdit(String name, Long id, int type, Long pId) throws Exception {
		String sql = "SELECT COUNT(T.ID) FROM TERM_DEFINITION_LIBRARY T WHERE T.PARENT_ID=? AND T.NAME=? AND T.IS_DIR=? AND T.ID<>?";
		return termDefinitionDao.countAllByParamsNativeSql(sql, pId, name, type, id) > 0 ? true : false;
	}

	@Override
	public void moveNodes(List<Long> listIds, Long pId, Long updatePersonId, TreeNodeType moveNodeType)
			throws Exception {
		String t_path = "";
		int level = -1;
		if (pId.intValue() != 0) {
			TermDefinitionLibrary termDefinitionLibrary = termDefinitionDao.get(pId);
			t_path = termDefinitionLibrary.gettPath();
			level = termDefinitionLibrary.gettLevel();
		}
		JecnCommon.updateNodesChildTreeBeans(listIds, pId, "", new Date(), updatePersonId, t_path, level,
				termDefinitionDao, moveNodeType, 1);
		for (Long id : listIds) {
			// 添加日志
			JecnUtil.saveJecnJournal(id, null, 15, 4, updatePersonId, termDefinitionDao);
		}
	}

	@Override
	public void reName(String name, Long id, Long updatePeopleId) throws Exception {
		TermDefinitionLibrary termDefinitionLibrary = termDefinitionDao.get(id);
		termDefinitionLibrary.setName(name);
		termDefinitionLibrary.setUpdatePersonId(updatePeopleId);
		termDefinitionLibrary.setUpdateTime(new Date());
		termDefinitionDao.update(termDefinitionLibrary);

		JecnUtil.saveJecnJournal(termDefinitionLibrary.getId(), termDefinitionLibrary.getName(), 15, 2,
				termDefinitionLibrary.getUpdatePersonId(), termDefinitionDao, 4);

		// 更新流程图术语元素名称
		updateFlowStructureTremEle(termDefinitionLibrary);
	}

	@Override
	public List<JecnTreeBean> searchByName(String name) throws Exception {
		String sql = "SELECT DISTINCT T.ID,T.NAME,T.PARENT_ID,T.IS_DIR," + "             0 AS COUNT,"
				+ "             T.SORT_ID,T.INSTRUCTIONS" + "             FROM TERM_DEFINITION_LIBRARY T"
				+ "             LEFT JOIN TERM_DEFINITION_LIBRARY PUB ON PUB.ID=T.PARENT_ID"
				+ "             WHERE T.NAME LIKE ? AND T.IS_DIR=1 ORDER BY T.PARENT_ID,T.SORT_ID";
		List<Object[]> list = termDefinitionDao.listNativeSql(sql, 0, 20, "%" + name + "%");
		return getListJecnTreeBean(list);
	}

	@Override
	public void updateSortNodes(List<JecnTreeDragBean> list, Long pId, Long updatePersonId) throws Exception {
		String hql = "from TermDefinitionLibrary where parentId=? ";
		List<TermDefinitionLibrary> listTermDefinitionLibrary = termDefinitionDao.listHql(hql, pId);
		List<TermDefinitionLibrary> listUpdate = new ArrayList<TermDefinitionLibrary>();
		for (JecnTreeDragBean jecnTreeDragBean : list) {
			for (TermDefinitionLibrary termDefinitionLibrary : listTermDefinitionLibrary) {
				if (jecnTreeDragBean.getId().equals(termDefinitionLibrary.getId())) {
					if (jecnTreeDragBean.getSortId() != termDefinitionLibrary.getSortId()) {
						termDefinitionLibrary.setSortId(jecnTreeDragBean.getSortId());
						listUpdate.add(termDefinitionLibrary);
						break;
					}
				}
			}
		}
		for (TermDefinitionLibrary termDefinitionLibrary : listUpdate) {
			hql = "update TermDefinitionLibrary set sortId=? where id=?";
			termDefinitionDao.execteHql(hql, termDefinitionLibrary.getSortId(), termDefinitionLibrary.getId());
			JecnUtil.saveJecnJournal(termDefinitionLibrary.getId(), termDefinitionLibrary.getName(), 15, 5,
					updatePersonId, termDefinitionDao);
		}
	}

	@Override
	public TermDefinitionLibrary getTermDefinitionLibraryById(Long id) throws Exception {
		return termDefinitionDao.get(id);
	}

	@Override
	public List<JecnTreeBean> getChildDirs(Long pId) throws Exception {
		String sql = "SELECT DISTINCT T.ID,T.NAME,T.PARENT_ID,T.IS_DIR,"
				+ "    CASE WHEN SUB.ID IS NULL THEN 0 ELSE 1 END AS COUNT," + "    T.SORT_ID,T.INSTRUCTIONS"
				+ "    FROM TERM_DEFINITION_LIBRARY T"
				+ "    LEFT JOIN TERM_DEFINITION_LIBRARY SUB ON SUB.PARENT_ID=T.ID AND SUB.IS_DIR=0"
				+ "    WHERE T.PARENT_ID=? AND T.IS_DIR=0 ORDER BY T.SORT_ID";
		List<Object[]> list = termDefinitionDao.listNativeSql(sql, pId);
		return getListJecnTreeBean(list);
	}

	@Override
	public List<JecnTreeBean> getPnodes(Long id) throws Exception {
		try {
			TermDefinitionLibrary termDefinitionLibrary = termDefinitionDao.get(id);
			Set<Long> set = JecnCommon.getPositionTreeIds(termDefinitionLibrary.gettPath(), id);
			String sql = "SELECT DISTINCT T.ID,T.NAME,T.PARENT_ID,T.IS_DIR,"
					+ "    CASE WHEN SUB.ID IS NULL THEN 0 ELSE 1 END AS COUNT," + "    T.SORT_ID,T.INSTRUCTIONS"
					+ "    FROM TERM_DEFINITION_LIBRARY T"
					+ "    LEFT JOIN TERM_DEFINITION_LIBRARY SUB ON SUB.PARENT_ID=T.ID AND T.IS_DIR=0"
					+ "    WHERE T.PARENT_ID in " + JecnCommonSql.getIdsSet(set)
					+ " ORDER BY T.PARENT_ID,T.SORT_ID,T.ID";
			List<Object[]> list = termDefinitionDao.listNativeSql(sql);
			return getListJecnTreeBean(list);
		} catch (Exception e) {
			log.error("", e);
			throw e;
		}
	}

	@Override
	public List<TermDefinitionLibrary> listTermsByIds(List<Long> idList) throws Exception {
		String hql = "from TermDefinitionLibrary where id in " + JecnCommonSql.getIds(idList);
		return termDefinitionDao.listHql(hql);
	}

	@Override
	public List<JecnTreeBean> getRoleAuthChilds(long pId, Long projectId, long userId) {
		List<Object[]> list = termDefinitionDao.getRoleAuthChilds(pId, projectId, userId);
		return getListJecnTreeBean(list);
	}

	@Override
	public List<JecnTreeBean> searchRoleAuthByName(String name, Long peopleId, Long projectId) {
		String sql = "SELECT DISTINCT T.ID,T.NAME,T.PARENT_ID,T.IS_DIR," + "             0 AS COUNT,"
				+ "             T.SORT_ID,T.INSTRUCTIONS" + "             FROM TERM_DEFINITION_LIBRARY T"
				+ "             LEFT JOIN TERM_DEFINITION_LIBRARY PUB ON PUB.ID=T.PARENT_ID";
		sql = sql + JecnCommonSqlTPath.INSTANCE.getRoleAuthSqlCommon(8, peopleId, projectId);
		sql = sql + "             WHERE T.NAME LIKE ? AND T.IS_DIR=1 ORDER BY T.PARENT_ID,T.SORT_ID";
		List<Object[]> list = termDefinitionDao.listNativeSql(sql, 0, 20, "%" + name + "%");
		return getListJecnTreeBean(list);
	}

}
