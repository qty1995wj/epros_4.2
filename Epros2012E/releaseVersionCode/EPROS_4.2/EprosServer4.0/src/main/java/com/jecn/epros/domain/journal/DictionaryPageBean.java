package com.jecn.epros.domain.journal;

import com.jecn.epros.domain.system.Dictionary;

import java.util.List;

/**
 * java字典项分页查询
 *
 * @Author: Angus
 * @CreateDate: 2018/7/29 15:10
 * @Description: descroption
 **/
public class DictionaryPageBean {

    private int total;
    private List<Dictionary> data;

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public List<Dictionary> getData() {
        return data;
    }

    public void setData(List<Dictionary> data) {
        this.data = data;
    }
}
