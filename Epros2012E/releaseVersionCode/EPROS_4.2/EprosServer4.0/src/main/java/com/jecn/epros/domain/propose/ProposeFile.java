package com.jecn.epros.domain.propose;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.util.Date;


/***
 * 建议附件表
 * 2013-09-02
 *
 */
public class ProposeFile implements java.io.Serializable {

    //主键ID
    private String id;
    //关联ID
    private String proposeId;
    //文件名称
    private String fileName;
    //文件内容
    private byte[] fileStream;
    //创建人
    private Long createPersonId;
    //创建日期
    @JsonFormat(pattern = "yyyy-MM-dd HH-mm-ss", timezone = "GMT+08")
    private Date createTime;
    //更新人
    private Long updatePersonId;
    //更新日期
    @JsonFormat(pattern = "yyyy-MM-dd HH-mm-ss", timezone = "GMT+08")
    private Date updateTime;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getProposeId() {
        return proposeId;
    }

    public void setProposeId(String proposeId) {
        this.proposeId = proposeId;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public byte[] getFileStream() {
        return fileStream;
    }

    public void setFileStream(byte[] fileStream) {
        this.fileStream = fileStream;
    }

    public Long getCreatePersonId() {
        return createPersonId;
    }

    public void setCreatePersonId(Long createPersonId) {
        this.createPersonId = createPersonId;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Long getUpdatePersonId() {
        return updatePersonId;
    }

    public void setUpdatePersonId(Long updatePersonId) {
        this.updatePersonId = updatePersonId;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }


}
