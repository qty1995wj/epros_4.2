<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<script type="text/javascript" src="${basePath}js/rtnlPropose.js">
</script>
		<script type="text/javascript"
			src="${basePath}js/rule/singleRuleRtnlPropose.js">
</script>
		<script type="text/javascript" src="${basePath}js/rule/ruleSys.js">
</script>
		<link rel="stylesheet" type="text/css" href="${basePath}css/jecn.css" />

		<script type="text/javascript">
var basePath = "${basePath}";
document.body.onresize = function() {
	setAllStateProposeDivWH('flowAllProposeJspId', true);
}
</script>
	</head>
	<%-- 流程，制度合理化建议下边显示--%>
	<body>
		<div id="flowAllProposeInfoJspId">
			<div id="flowAllProposeJspId"
				style="overFlow-x: hidden; overFlow-y: auto; width: 100%; position: relative;">
				<s:iterator value="listAllStatePropose" status="st">
					<TABLE id="tablePropose<s:property value="id" />"
						style="width: 100%; border-width: 1px; border-style: solid; border-color: RGB(199, 199, 199); margin-top: 10px; margin-bottom: 15px;"
						border="0" cellspacing="0" cellpadding="0">
						<tr>
							<td colspan="3" style="width: 15%; padding-top: 7px;">
								<!-- 建议创建人 -->
								<span style="color: gray; padding-left: 20px; padding-top: 7px;">
									<s:if test="createUpPeopoleName == null">
										<s:text name="peopleGone" />
									</s:if> <s:else>
										<s:property value="createUpPeopoleName" />
									</s:else> </span> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								<!-- 建议创建时间 -->
								<span style="color: gray;"> <s:date name="createTime"
										format="yyyy-MM-dd" /> </span> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								<!-- 建议状态 -->
								<span style="color: gray;"> <span
									id="proStateId<s:property value="id" />"> <s:if
											test="state == 0">
											<s:text name="noReadPropose" />
										</s:if> <s:elseif test="state==1">
											<s:text name="readedPropose" />
										</s:elseif> 
										<s:elseif test="state == 2">
											<s:text name="acceptPropose" />
										</s:elseif> 
										<s:elseif test="state == 3">
											<s:text name="refusePropose" />
										</s:elseif> 
										
										</span> </span>
							</td>
						</tr>
						<!-- 建议内容 -->
						<tr>
							<td align="left" style="padding-top: 7px; vertical-align: top;">
								<div
									style="hight: 1px; border-top: 1px solid #e8e8e8; padding-bottom: 5px;width:60px;"></div>
								<span style="padding-left: 20px; color: gray; padding-top: 7px;"><s:text
										name="mesnonullgcontent" /> </span>
							</td>
							<td colspan="2"
								style="width: 94%; padding-top: 7px; word-break: break-all; padding-right: 10px;">
								<div
									style="hight: 1px; border-top: 1px solid #e8e8e8; padding-bottom: 5px;"></div>
								<s:if test="proposeContent == null || proposeContent == ''">
									<span style="text-align: left;"><s:text name="none" />
									</span>
								</s:if>
								<s:else>
									<span style="text-align: left;"><s:property
											value="proposeContent" escape="false" /> </span>
								</s:else>
							</td>

						</tr>
						<!-- 附件 -->
						<tr>
							<s:if test="listFileNames == null">
								<td colspan="3">
									<div style="height: 10px; border-right: 1px #e8e8e8 solid;"></div>
								</td>
							</s:if>
							<s:else>
								<td colspan="3">
									<br>
									<span style="padding-left: 20px; color: gray;"><s:text
											name="accessories" /> </span>
									<a
										href="downloadProposeFile.action?proposeFileId=<s:property  value="listFileIds" />"
										class="flowPropose" target='_blank'><s:property
											value="listFileNames" /> </a>
									<input type="hidden"
										value="<s:property  value="listFileIds" />" id="hideFileId" />
								</td>
							</s:else>
						</tr>
						<!-- 按钮显示开始-->
						<tr>
							<td align="right" style="padding-right: 10px;" colspan="3">
								<s:if
									test="buttonShowConfig.acceptIsShow">
									<!-- 采纳置灰 -->
									<s:if
										test="buttonShowConfig.acceptIsGray">
										<img src="${basePath}images/propose/acceptGray.gif"
											id="acceptImgId<s:property value="id" />" />
									</s:if>
									<s:else>
										<img src="${basePath}images/propose/accept.gif"
											style="cursor: pointer;"
											id="acceptImgId<s:property value="id" />"
											onclick="updateToAccept('<s:property  value="id" />',2)" />
									</s:else>
								</s:if>	
									&nbsp;&nbsp;
									
						<s:if
										test="buttonShowConfig.cancelAcceptIsShow">
									<!-- 拒绝采纳置灰 -->
									<s:if test="buttonShowConfig.cancelAcceptIsGray">
											<img src="${basePath}images/propose/cancelAcceptGray.gif"
												id="cancelAcceptImgId<s:property value="id" />" />
									</s:if>
									<s:else>
										<img src="${basePath}images/propose/cancelAccept.gif"
											style="cursor: pointer;"
											id="cancelAcceptImgId<s:property value="id" />"
											onclick="updateToCancelAccept('<s:property  value="id" />',3)">
									</s:else>
						</s:if>			
									
									&nbsp;&nbsp;
									
						<s:if test="buttonShowConfig.replayIsShow">
												<!-- 回复置灰 暂时没有-->
												<s:if
													test="buttonShowConfig.replayIsGray">

												</s:if>

												<s:else>
													<img src="${basePath}images/propose/replay.gif"
														style="cursor: pointer;"
														id="replyImgId<s:property value="id" />"
														onclick="replyContentFunction('<s:property  value="id" />')">
												</s:else>
						</s:if>	
						
								&nbsp;&nbsp;	
					
						<s:if test="buttonShowConfig.readIsShow">
												<!-- 已读置灰-->
												<s:if
													test="buttonShowConfig.readIsGray">

													<img src="${basePath}images/propose/readGray.gif"
														id="readeImgId<s:property value="id" />">

												</s:if>
												<!-- 已读-->
												<s:else>
													<img src="${basePath}images/propose/readed.gif"
														style="cursor: pointer;"
														id="readeImgId<s:property value="id" />"
														onclick="updateSingleUnToRead('<s:property  value="id" />',1)" />
												</s:else>
						</s:if>
						
						&nbsp;&nbsp;
						
						<s:if test="buttonShowConfig.editIsShow">
								<!-- 编辑置灰-->
								<s:if
									test="buttonShowConfig.editIsGray">
									<img src="${basePath}images/propose/editGray.gif"
										style="cursor: pointer;"
										id="editImgId<s:property value="id" />" />
									
								</s:if>
									
								<s:else>
									    <span style="display: none"
											id="getProposeContent<s:property  value="id" />"><s:property
												value="proposeContent" escape="false" />
										</span>
										<img src="${basePath}images/propose/edit.gif"
											style="cursor: pointer;"
											id="editImgId<s:property value="id" />"
											onclick="showEditDiv('<s:property  value="id" />','<s:property  value="listFileIds" />','<s:property  value="listFileNames" />','<s:property value="isResposibleNum" />','<s:property value="isAdiminLogin" />','<s:property  value="isFlowRtnl" />','<s:property  value="processName" />','<s:property  value="flowId" />',0)">
								</s:else>
					    </s:if>
						&nbsp;&nbsp;
						
						<s:if test="buttonShowConfig.delIsShow">
								<!-- 删除置灰-->
								<s:if test="buttonShowConfig.delIsGray">
								    <img src="${basePath}images/propose/deleteGray.gif"
									style="cursor: pointer;"
									id="deleteImgId<s:property value="id"/> "/>
								</s:if>
									<s:else>
										<img src="${basePath}images/propose/delete.gif"
											style="cursor: pointer;"
									id="deleteImgId<s:property value="id" />"
									onclick="deleteMyPropose('<s:property value="id" />')" />
								</s:else>
						</s:if>
							</td>
						</tr>

						<!-- 按钮显示结束-->
						
						<tr>
							<td style="padding-top: 7px; vertical-align: top;">
								<div
									style="hight: 1px; border-top: 1px solid #e8e8e8; padding-bottom: 5px;"></div>
								<span style="padding-left: 20px;"
									id="replyFlagShow<s:property value="id" />"> <s:if
										test="replyName == null"></s:if> <s:else>
										<span style="color: gray;"><s:text name="replyProposeC" />
										</span>
									</s:else> </span>
							</td>
							<td colspan="2"
								style="width: 94%; padding-top: 7px; word-break: break-all; padding-right: 10px;">
								<div
									style="hight: 1px; border-top: 1px solid #e8e8e8; padding-bottom: 5px;"></div>
								<span id="spanContentId<s:property value="id" />"> <s:if
										test="replyName == null"></s:if> <s:else>
										<s:property value="replyContent" escape="false" />
									</s:else> </span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								
									<a href="#" class="flowPropose"
										onclick="deleteProposeContent('<s:property  value="id" />')">
										<span id="deleteReplyHrefId<s:property value="id" />">
											
											<s:if test="buttonShowConfig.curPeopelIsCanDelAnswer">
											<span>
											  <s:text name="delete" />
											  </span>
											</s:if>
												
											 </span> </a>
								
							</td>
						</tr>

						<tr>
							<td colspan="3"
								style="width: 15%; padding-top: 7px; padding-left: 20px;">
								<span style="color: gray;"
									id="replyNameShow<s:property value="id" />"> <s:if
										test="replyName == null"></s:if> <s:elseif
										test="replyPeopleState == 1">
										<s:text name="peopleGone" />
									</s:elseif> <s:else>
										<s:property value="replyName" />
									</s:else> </span> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								<span style="color: gray;"
									id="replyTimeShow<s:property value="id" />"> <s:if
										test="replyName == null"></s:if> <s:else>
										<s:date name="replyTime" format="yyyy-MM-dd" />
									</s:else> </span>
								<div style="height: 15px;"></div>
							</td>
						</tr>

						<tr>
							<td colspan="3" style="width: 90%; padding-right: 50px;"
								align="right">
								<input type="hidden" id="isShowId<s:property value="id" />"
									value="0" />
								<input type="hidden" id="isReply<s:property value="id" />"
									value="<s:property value="state" />" />
								<div style="width: 60%; display: none;" align="right"
									id="replyContentDivId<s:property value="id" />">
									<table
										style="width: 100%; border-width: 1px; border-style: solid; border-color: RGB(199, 199, 199); margin-top: 10px; margin-bottom: 15px;">
										<tr>
											<td colspan="3" align="left" style="padding: 5px;">
												<s:text name="replyPropose" />
												<s:property value="createUpPeopoleName" />
											</td>
										</tr>
										<tr>
											<td style="width: 10px;"></td>
											<td>
												<textarea id="textContentId<s:property value="id" />"
													style="width: 100%; height: 80px; border: 1px RGB(199, 199, 199) solid;"></textarea>
											</td>
											<td style="width: 10px;"></td>
										</tr>
										<tr>
											<td colspan="3" align="right" style="padding: 5px;">
												<img src="${basePath}images/propose/ok.gif"
													style="cursor: pointer;"
													onclick="submitContent('<s:property  value="id" />',1)">
												&nbsp;&nbsp;
												<img src="${basePath}images/propose/cancel.gif"
													style="cursor: pointer;"
													onclick="cancelReply('<s:property  value="id" />')">
											</td>
										</tr>
									</table>
								</div>
							</td>
						</tr>
					</TABLE>

				</s:iterator>
				<s:if
					test="nowState == 0 && (isResposibleNum == 1 || isAdiminLogin == 1) && isShowAllReadImg ==1">
					<div align="right" style="padding-right: 5px;">
						<img src="${basePath}images/propose/allRead.gif"
							style="cursor: pointer;" onclick="updateToRead()">
					</div>
				</s:if>
			</div>
			<div id="paginationId"
				style="background-color: #eaeaea; border-top: 1px RGB(199, 199, 199) solid; width: 100%; position: relative; margin-top: 1px;">
				<table width="100%">
					<tr>
						<td align="left">
							<table>
								<tr>
									<td>
										<img id="previousPage"
											src="${basePath}images/propose/before1.gif"
											style="cursor: pointer;"
											onclick="onclickChangePage('<s:property  value="currentPage" />','first','<s:property  value="flowId" />','<s:property  value="nowState" />','<s:property value="isResposibleNum" />','<s:property value="pager.totalPages" />',0,'<s:property value="isAdiminLogin" />',event,'<s:property  value="isFlowRtnl" />')">
									</td>
									<td>
										<img id="homePage" src="${basePath}images/propose/before2.gif"
											style="cursor: pointer;"
											onclick="onclickChangePage('<s:property  value="currentPage" />','previous','<s:property  value="flowId" />','<s:property  value="nowState" />','<s:property value="isResposibleNum" />','<s:property value="pager.totalPages" />',0,'<s:property value="isAdiminLogin" />',event,'<s:property  value="isFlowRtnl" />')">
									</td>
									<td
										style="font-size: 11px; border-left: 1px RGB(199, 199, 199) solid; border-right: 1px RGB(199, 199, 199) solid; padding-left: 3px; padding-right: 3px;">
										<s:text name="inTheFirst" />
										<input type="text" style="width: 33px; text-align: right;"
											id="jumpToCurrentPageId"
											value="<s:property value="currentPage" />"
											onkeypress="onclickChangePage('<s:property  value="currentPage" />','','<s:property  value="flowId" />','<s:property  value="nowState" />','<s:property value="isResposibleNum" />','<s:property value="pager.totalPages" />',1,'<s:property value="isAdiminLogin" />',event,'<s:property  value="isFlowRtnl" />')" />
										<s:text name="page" />
										,
										<s:text name="general" />
										<s:property value="pager.totalPages" />
										<s:text name="page" />
									</td>
									<td>
										<img id="nextPage" src="${basePath}images/propose/after3.gif"
											style="cursor: pointer;"
											onclick="onclickChangePage('<s:property  value="currentPage" />','next','<s:property  value="flowId" />','<s:property  value="nowState" />','<s:property value="isResposibleNum" />','<s:property value="pager.totalPages" />',0,'<s:property value="isAdiminLogin" />',event,'<s:property  value="isFlowRtnl" />')">
									</td>
									<td>
										<img id="endPage" src="${basePath}images/propose/after4.gif"
											style="cursor: pointer;"
											onclick="onclickChangePage('<s:property  value="currentPage" />','last','<s:property  value="flowId" />','<s:property  value="nowState" />','<s:property value="isResposibleNum" />','<s:property value="pager.totalPages" />',0,'<s:property value="isAdiminLogin" />',event,'<s:property  value="isFlowRtnl" />')">
									</td>
									<td
										style="border-left: 1px RGB(199, 199, 199) solid; padding-left: 3px;">
										<img src="${basePath}images/propose/refresh.gif"
											style="cursor: pointer;"
											onclick="onclickChangePage('<s:property  value="currentPage" />','','<s:property  value="flowId" />','<s:property  value="nowState" />','<s:property value="isResposibleNum" />','<s:property value="pager.totalPages" />',0,'<s:property value="isAdiminLogin" />',event,'<s:property  value="isFlowRtnl" />')">
									</td>
								</tr>
							</table>
						</td>
						<td align="right">
							<s:if test="totalRows == null">
								<s:text name="noTakeNotes" />
							</s:if>
							<s:else>
								<s:property value="pager.startRow+1" />
								-
								<s:property value="pager.endRow" />
								<s:text name="item" />
								<s:text name="general" />
								<s:property value="totalRows" />
								<s:text name="item" />
							</s:else>
						</td>
					</tr>
				</table>
				<%--<input type="hidden" id="listProposeIds"
					value="<s:property value="strProposeId" />" />
				--%>
				
				<input type="hidden" id="flowId"
					value="<s:property value="flowId" />" />
				<input type="hidden" id="isFlowRtnl"
					value="<s:property value="isFlowRtnl" />" />
				<input type="hidden" id="loginName"
					value="<s:property value="loginName" />" />
				<input type="hidden" id="totalPages"
					value="<s:property value="pager.totalPages" />">
				<input type="hidden" id="isPop">
			</div>
		</div>
	</body>
</html>