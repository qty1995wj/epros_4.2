package epros.designer.gui.popedom.role;

import java.awt.Insets;

import javax.swing.JTree;

import org.apache.log4j.Logger;

import epros.designer.gui.common.JecnConstants;
import epros.designer.gui.common.JecnEditNameDialog;
import epros.designer.tree.JecnTreeCommon;
import epros.designer.tree.JecnTreeNode;
import epros.designer.util.ConnectionPool;
import epros.designer.util.JecnProperties;

/**
 * 角色目录重命名
 * @author fuzhh Apr 9, 2013
 *
 */
public class EditRoleDirDialog extends JecnEditNameDialog {
	private static Logger log = Logger.getLogger(EditRoleDirDialog.class);
	private JecnTreeNode selectNode = null;
	private JTree jTree = null;

	public EditRoleDirDialog(JecnTreeNode selectNode, JTree jTree) {
		this.selectNode = selectNode;
		this.jTree = jTree;
		this.setLocationRelativeTo(null);
		this.setName(selectNode.getJecnTreeBean().getName());

	}


	// Dialog标题
	@Override
	public String getDialogTitle() {
		return JecnProperties.getValue("rename");
	}


	// 角色名称Lab
	@Override
	public String getNameLab() {
		return JecnProperties.getValue("name");
	}

	@Override
	public boolean validateNodeRepeat(String name) {
		return JecnTreeCommon.validateRepeatNameEidt(name, selectNode);
	}

	@Override
	public void saveData() {
		try {
			ConnectionPool.getJecnRole().reRoleName(this.getName(),
					selectNode.getJecnTreeBean().getId(),
					JecnConstants.loginBean.getJecnUser().getPeopleId());
			// 重命名
			JecnTreeCommon.reNameNode(jTree, selectNode, this.getName());
			this.dispose();
		} catch (Exception e) {
			log.error("EditRoleDirDialog saveData is error!", e);
		}

	}
	protected void validateEnglishName() {// 验证英文名称

	}

	protected void addEnNamePanel(Insets insets) {
	}
}
