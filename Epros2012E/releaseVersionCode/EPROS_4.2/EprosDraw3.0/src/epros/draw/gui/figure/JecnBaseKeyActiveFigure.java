/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package epros.draw.gui.figure;

import java.awt.Graphics;
import java.awt.Graphics2D;

import epros.draw.constant.JecnToolBoxConstant.MapElemType;
import epros.draw.data.figure.JecnFigureData;

/**
 * 
 * 关键活动
 * 
 * @author xiaohu
 */
public class JecnBaseKeyActiveFigure extends JecnBaseFigurePanel {
	public JecnBaseKeyActiveFigure(JecnFigureData keyActiveData) {
		super(keyActiveData);
		initComponents();
	}

	private void initComponents() {
		// 透明
		this.setOpaque(false);
	}

	public JecnFigureData getFlowElementData() {
		return (JecnFigureData) flowElementData;
	}

	public void paintComponent(Graphics g) {
		userWidth = this.getWidth() - 1;
		userHeight = this.getHeight() - 1;

		// 画图
		paintFigure(g);
	}

	/**
	 * 无填充色
	 * 
	 */
	public void paintNoneFigure(Graphics2D g2d) {
		g2d.setPaint(flowElementData.getBodyColor());
		g2d.setStroke(flowElementData.getBasicStroke());
		// x - 要填充椭圆的左上角的 x 坐标。y - 要填充椭圆的左上角的 y 坐标。width - 要填充椭圆的宽度。height -
		// 要填充椭圆的高度
		g2d.drawOval(Math.min(0, userWidth), Math.min(0, userHeight), Math
				.abs(0 - userWidth), Math.abs(0 - userHeight));
	}

	/**
	 * 无阴影无3D 普通效果
	 * 
	 */
	public void paintActiveFigureTop(Graphics2D g2d, int shadowCount) {
//		g2d.setColor(flowElementData.getFillColor());
		g2d.fillOval(Math.min(0, userWidth), Math.min(0, userHeight),
				userWidth, userHeight);
		g2d.setPaint(flowElementData.getBodyColor());
		g2d.setStroke(flowElementData.getBasicStroke());
		g2d.drawOval(Math.min(0, userWidth), Math.min(0, userHeight), Math
				.abs(0 - userWidth), Math.abs(0 - userHeight));
	}

	/**
	 * 无阴影无3D 普通效果
	 * 
	 */
	public void paintGeneralFigurePart(Graphics2D g2d) {
		// 顶层 原图，榜值均为零
		paintActiveFigureTop(g2d, 0);
	}

	/**
	 * 
	 * 判断关键活动的类型是否是给定的流程元素类型
	 * 
	 * @param mapElemType
	 *            MapElemType 流程元素类型 不能为NULL
	 * @return true：关键活动流程元素类型是给定的流程元素类型；false：关键活动流程元素类型不是给定的流程元素类型
	 */
	public boolean checkMapElemTypeSame(MapElemType mapElemType) {
		if (mapElemType == null) {
			return false;
		}
		// 获取给定的关键活动的流程元素类型
		MapElemType keyActiveType = this.getFlowElementData().getMapElemType();

		return (mapElemType.equals(keyActiveType)) ? true : false;
	}

	/**
	 * 
	 * 判断给定的关键活动与当前关键活动是否是同一个对象
	 * 
	 * @param keyActiveFigure
	 *            JecnBaseKeyActiveFigure 关键活动
	 * @return boolean 是同一个对象返回true；反之返回false
	 */
	public boolean checkKeyActiveFigureSame(
			JecnBaseKeyActiveFigure keyActiveFigure) {
		if (keyActiveFigure == null) {
			return false;
		}
		return true;
	}

	/**
	 * 关键活动创建克隆对象
	 * 
	 */
	public JecnBaseKeyActiveFigure clone() {
		return (JecnBaseKeyActiveFigure) this.currClone();
	}
}
