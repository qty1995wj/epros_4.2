package com.jecn.epros.server.webBean.dataImport.sync.excelOrDB;

/**
 * 
 * 工号和域账号对应Bean
 * 
 * @author chehuanbo
 * 
 * */
public class UserDomainNameBean extends AbstractBaseBean {
  
    /**登录名称*/	
	private String loginName;
	/**域帐号名称*/
	private String UserName;
	
	
	
	public String getLoginName() {
		return loginName;
	}

	public void setLoginName(String loginName) {
		this.loginName = loginName;
	}

	public String getUserName() {
		return UserName;
	}

	public void setUserName(String userName) {
		UserName = userName;
	}





	@Override
	public String toInfo() {
		// TODO Auto-generated method stub
		return null;
	}

}
