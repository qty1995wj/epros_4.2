/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package epros.designer.table;

import java.awt.Component;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

import javax.swing.DefaultCellEditor;
import javax.swing.JCheckBox;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

import epros.designer.gui.recycle.RecyclePanel;

public class RecycleCheckButtonEditor extends DefaultCellEditor implements
		ItemListener {
	private static final long serialVersionUID = 198567563L;
	private JCheckBox button;
	/** 全选框是否选中标识 */
	private boolean isFlag = false;
	/** 回收站数据表格 */
	private RecyclePanel recyclePanel;
	/** 回收站数据表格 */
	private JecnRecyleTable recyTable;
	private DefaultTableModel model;
	private int countModel;

	public RecycleCheckButtonEditor(JCheckBox checkBox,
			RecyclePanel recyclePanel) {
		super(checkBox);
		this.recyclePanel = recyclePanel;
		recyTable=recyclePanel.getJecnRecyleTable();
	}

	public Component getTableCellEditorComponent(JTable table, Object value,
			boolean isSelected, int row, int column) {
		if (value == null)
			return null;
		button = (JCheckBox) value;
		button.addItemListener(this);
		return (Component) value;
	}

	public Object getCellEditorValue() {
		button.removeItemListener(this);
		return button;
	}

	public void itemStateChanged(ItemEvent e) {
		super.fireEditingStopped();
		model = (DefaultTableModel) this.recyTable.getModel();
		// 判断复选框是否有选中的，没有选中提示，请勾选复选框
		countModel = model.getRowCount();
		// 取消选中状态
		if (e.getStateChange() == e.DESELECTED) {
			// 取消全选框选中状态
			recyclePanel.getSelectAllBox().setSelected(false);
		} else {
			// 未选中的行数
			int selectNum = 0;
			for (int k = 0; k < countModel; k++) {
				if (!((JCheckBox) model.getValueAt(k, 0)).isSelected()) {
					selectNum++;
				}
			}
			if (selectNum >= 1) {
				isFlag = true;
			}
			if (!isFlag) {
				// 设置全选框选中
				recyclePanel.getSelectAllBox().setSelected(true);
			}
		}
		isFlag = false;
	}
}