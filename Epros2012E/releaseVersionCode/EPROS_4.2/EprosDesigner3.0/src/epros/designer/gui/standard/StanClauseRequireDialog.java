package epros.designer.gui.standard;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JTree;

import epros.designer.gui.common.JecnTextArea;
import epros.designer.tree.JecnTreeNode;
import epros.designer.util.JecnProperties;
import epros.draw.gui.swing.JecnPanel;
import epros.draw.gui.top.dialog.JecnDialog;
import epros.draw.gui.top.dialog.listener.JecnDialogEvent;
import epros.draw.gui.top.dialog.listener.JecnDialogListener;
/**
 * 标准条款要求内容对话框
 * @author user
 *
 */
public abstract class StanClauseRequireDialog extends JecnDialog{
	
	/** 主面板 */
	protected JecnPanel mainPanel;
	/** 内容面板 */
	protected JecnPanel infoPanel;
	/** 按钮面板 */
	protected JecnPanel buttonPanel;
	/** 条款要求Lab */
	protected JLabel clauseRequireLab;
	/**必填(*)*/
	protected JLabel requiredLab;
	/** 条款要求内容 */
	protected JecnTextArea clauseRequireArea;
	/** 条款要求内容滚动面板 */
	protected JScrollPane clauseRequireScrollPane;
	/** 确定 */
	protected JButton okButton = null;
	/** 取消 */
	protected JButton cancelButton = null;
	/** 验证提示 */
	protected JLabel tipLabel = null;
	protected JecnTreeNode node = null;
	protected JTree jTree = null;
	public StanClauseRequireDialog(JecnTreeNode node,JTree jTree) {
		this.node=node;
		this.jTree=jTree;
		initCompotents();
		initLayout();
		buttonActionInit();
		this.setModal(true);
		this.setLocationRelativeTo(null);
	}
	
	public StanClauseRequireDialog(){
		initCompotents();
		initLayout();
		buttonActionInit();
		this.setModal(true);
		this.setLocationRelativeTo(null);
	}
	/**
	 * @author yxw 2013-11-13
	 * @description:组件初始化
	 */
	private void initCompotents() {
		this.setSize(getWidthMax(), 300);
		this.setMinimumSize(new Dimension(450, 370));
		mainPanel = new JecnPanel();
		infoPanel = new JecnPanel();
		buttonPanel = new JecnPanel();
		clauseRequireLab=new JLabel(JecnProperties.getValue("stanClauseRequireC"));
		requiredLab=new JLabel(JecnProperties.getValue("required"));
		clauseRequireArea=new JecnTextArea();
		clauseRequireScrollPane=new JScrollPane(clauseRequireArea);
		okButton = new JButton(JecnProperties.getValue("okBtn"));
		cancelButton = new JButton(JecnProperties.getValue("cancelBtn"));
		tipLabel = new JLabel();
		requiredLab.setForeground(Color.red);
		tipLabel.setForeground(Color.red);
		clauseRequireArea.setBorder(null);
		clauseRequireArea.setLineWrap(true);
	}
	/**
	 * @author yxw 2013-11-13
	 * @description: 页面布局
	 */
	private void initLayout() {
		Insets insets = new Insets(5, 5, 5, 5);
		GridBagConstraints c = new GridBagConstraints(0, 0, 1, 1, 1.0, 1.0,
				GridBagConstraints.CENTER, GridBagConstraints.BOTH, insets, 0,
				0);
		mainPanel.setLayout(new GridBagLayout());
		mainPanel.add(infoPanel,c);
		 c = new GridBagConstraints(0, 1, 1, 1, 1.0, 0.0,
					GridBagConstraints.CENTER, GridBagConstraints.BOTH, insets, 0,
					0);
		mainPanel.add(buttonPanel,c);
		
		infoPanel.setLayout(new GridBagLayout());
		c = new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
				GridBagConstraints.NORTHWEST, GridBagConstraints.NONE, insets, 0,
				0);
		infoPanel.add(clauseRequireLab,c);
		c = new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
				GridBagConstraints.NORTHWEST, GridBagConstraints.NONE, new Insets(5, 1, 5, 1), 0,
				0);
		infoPanel.add(requiredLab,c);
		c = new GridBagConstraints(2, 0, 1, 1, 1.0, 1.0,
				GridBagConstraints.CENTER, GridBagConstraints.BOTH, insets, 0,
				0);
		infoPanel.add(clauseRequireScrollPane,c);
		buttonPanel.setLayout(new FlowLayout(FlowLayout.RIGHT));
		buttonPanel.add(tipLabel);
		buttonPanel.add(okButton);
		buttonPanel.add(cancelButton);
		this.getContentPane().add(mainPanel);
	}
	
	private void buttonActionInit() {
		
		this.addJecnDialogListener(new JecnDialogListener() {
			@Override
			public boolean dialogCloseBefore(JecnDialogEvent e) {
				cancelButPerformed();
				return false;
			}
		});
		
		okButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				okButtonAction();
			}
		});
		cancelButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				cancelButPerformed();
			}
		});
	}
	
	public abstract boolean isUpdate();
	
	public abstract void okButtonAction();
	
	/**
	 * 取消
	 */
	protected void cancelButPerformed() {
		if (!isUpdate()) {// 没有更新
			this.setVisible(false);
		} else {
			int optionTig = this.dialogCloseBeforeMsgTipAction();
			if (optionTig == 2) {
				okButtonAction();
			} else if (optionTig == 1) {
				this.setVisible(false);
			}
		}
	}
}
