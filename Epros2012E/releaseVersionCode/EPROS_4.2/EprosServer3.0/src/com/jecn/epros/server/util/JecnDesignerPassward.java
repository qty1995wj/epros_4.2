package com.jecn.epros.server.util;


/**
 * 
 * 
 * 
 * @author ZHOUXY
 * 
 */
public class JecnDesignerPassward {
	private static final String HEX_MATCH = "0123456789ABCDEF";
	private static final char KEY='J';

	/**
	 * 
	 * 设计器密码保存本地加密解密算法
	 * 
	 * 可逆的加密算法
	 * 
	 * @param pwd
	 *            String
	 * @return String 加密后的十六进制字符串
	 * 
	 */
	public static String getDesignerEncrypt(String pwd) {
		if (isNullOrEmtryTrim(pwd)) {
			return null;
		}
		char[] a = pwd.toCharArray();
		for (int i = 0; i < a.length; i++) {
			a[i] = (char) (a[i] ^ KEY);
		}
		return byteToHexString(new String(a).getBytes());
	}

	/**
	 * 
	 * 设计器密码保存本地加密解密算法
	 * 
	 * getDesignerEncrypt的解密算法
	 * 
	 * @param pwd
	 *            String 解密后的字符串
	 */
	public static String getDesignerDecrypt(String pwd) {
		if (isNullOrEmtryTrim(pwd)) {
			return null;
		}

		String pwdNew = new String(hexStringToByte(pwd));
		char[] a = pwdNew.toCharArray();
		for (int i = 0; i < a.length; i++) {
			a[i] = (char) (a[i] ^ KEY);
		}
		return new String(a);
	}

	/**
	 * * 将16进制字符串转换成字节数组 *
	 * 
	 * @param str
	 *            String
	 * @return byte[]
	 */
	public static byte[] hexStringToByte(String str) {
		int len = (str.length() / 2);
		byte[] result = new byte[len];
		char[] charArray = str.toCharArray();
		for (int i = 0; i < len; i++) {
			int pos = i * 2;
			result[i] = (byte) (HEX_MATCH.indexOf(charArray[pos]) << 4 | HEX_MATCH
					.indexOf(charArray[pos + 1]));
		}
		return result;
	}

	/**
	 * * 将指定byte数组转换成16进制字符串 *
	 * 
	 * @param b
	 *            byte[]
	 * @return String
	 */
	public static String byteToHexString(byte[] b) {
		StringBuffer buff = new StringBuffer();
		for (int i = 0; i < b.length; i++) {
			String str = Integer.toHexString(b[i] & 0xFF);
			if (str.length() == 1) {
				str = '0' + str;
			}
			buff.append(str.toUpperCase());
		}
		return buff.toString();
	}
	
	/**
	 * 
	 * 给定参数先去空再判断是否为null或空
	 * 
	 * @param str
	 *            String
	 * @return boolean str为空返回ture，不为空返回false
	 */
	private static boolean isNullOrEmtryTrim(String str) {
		return (str == null || str.trim().isEmpty()) ? true : false;
	}
}
