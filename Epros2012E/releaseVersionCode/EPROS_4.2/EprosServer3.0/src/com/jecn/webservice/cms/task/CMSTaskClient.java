package com.jecn.webservice.cms.task;

import java.net.URL;
import java.util.Calendar;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

import org.codehaus.xfire.client.Client;

import com.jecn.epros.server.action.web.login.ad.cms.JecnCMSAfterItem;
import com.jecn.webservice.cms.task.bean.AddPendingTask;

/**
 * 任务代办
 * 
 * @author ZXH
 * 
 */
public enum CMSTaskClient {
	INSTANCE;

	private UnifiedMessageServiceLocator serviceLocator = null;

	private UnifiedMessageServiceSoap_PortType serviceSoapPortType = null;

	/**
	 * 创建代办
	 * 
	 * @param sn
	 */
	public Object[] AddPendingTask(AddPendingTask task) throws Exception {
		// try {
		// if (serviceSoapPortType == null) {
		// serviceLocator = new UnifiedMessageServiceLocator();
		// serviceSoapPortType = serviceLocator.getUnifiedMessageServiceSoap();
		// }
		// return serviceSoapPortType.addPendingTask(task.getSn(),
		// task.getProcessName(), task.getActivityName(), task
		// .getOriginator(), task.getOriginatorName(),
		// task.getOriginatorDeptId(), task
		// .getOriginatorDeptName(), task.getSendTo(), task.getRedirectUser(),
		// task.getSubject(), task
		// .getUrl(), task.getProcessBuildDate(), task.getCreateDate(),
		// task.getInComingSysName(), task
		// .getComments(), task.getExtendCol1(), task.getExtendCol2(),
		// task.getExtendCol3(), task
		// .getExtendCol4(), task.getExtendCol5());
		// } catch (Exception e) {
		// e.printStackTrace();
		// throw e;
		// }
		try {
			Client client = new Client(new URL(JecnCMSAfterItem.getValue("TASK_SERVICE") + "?wsdl"));
			Object[] sendInfoParams = { task.getSn(), task.getProcessName(), task.getActivityName(),
					task.getOriginator(), task.getOriginatorName(), task.getOriginatorDeptId(),
					task.getOriginatorDeptName(), task.getSendTo(), task.getRedirectUser(), task.getSubject(),
					task.getUrl(), dateToXmlDate(task.getProcessBuildDate()), dateToXmlDate(task.getCreateDate()),
					task.getInComingSysName(), task.getComments(), task.getExtendCol1(), task.getExtendCol2(),
					task.getExtendCol3(), task.getExtendCol4(), task.getExtendCol5() };
			return client.invoke("AddPendingTask", sendInfoParams);
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}

	}

	public XMLGregorianCalendar dateToXmlDate(Calendar cal) {
		DatatypeFactory dtf = null;
		try {
			dtf = DatatypeFactory.newInstance();
		} catch (DatatypeConfigurationException e) {
		}
		XMLGregorianCalendar dateType = dtf.newXMLGregorianCalendar();
		dateType.setYear(cal.get(Calendar.YEAR));
		// 由于Calendar.MONTH取值范围为0~11,需要加1
		dateType.setMonth(cal.get(Calendar.MONTH) + 1);
		dateType.setDay(cal.get(Calendar.DAY_OF_MONTH));
		dateType.setHour(cal.get(Calendar.HOUR_OF_DAY));
		dateType.setMinute(cal.get(Calendar.MINUTE));
		dateType.setSecond(cal.get(Calendar.SECOND));
		return dateType;
	}

	/**
	 * 代办转已办
	 * 
	 * @param sn
	 * @param sysname
	 * @param ActivityName
	 * @param SendTo
	 * @param viewUrl
	 * @return
	 * @throws Exception
	 */
	public ProMessageOjbect ChangeStatusToHandled(String sn, String sysname, String ActivityName, String SendTo,
			String viewUrl) throws Exception {
		try {
			if (serviceSoapPortType == null) {
				serviceLocator = new UnifiedMessageServiceLocator();
				serviceSoapPortType = serviceLocator.getUnifiedMessageServiceSoap();
			}
			return serviceSoapPortType.changeStatusToHandledByAUrl(sn, sysname, ActivityName, SendTo, viewUrl);
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	public ProMessageOjbect DeletePendingTask(String sn, String sysname, String activityName, String sendTo)
			throws Exception {
		try {
			if (serviceSoapPortType == null) {
				serviceLocator = new UnifiedMessageServiceLocator();
				serviceSoapPortType = serviceLocator.getUnifiedMessageServiceSoap();
			}
			return serviceSoapPortType.deletePendingTask(sn, sysname, activityName, sendTo);
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}
}
