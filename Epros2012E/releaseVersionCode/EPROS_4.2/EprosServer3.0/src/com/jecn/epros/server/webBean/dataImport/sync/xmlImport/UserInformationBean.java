package com.jecn.epros.server.webBean.dataImport.sync.xmlImport;

import java.io.Serializable;
import java.util.List;

/**
 * <li>Title: UserInformationBean.java</li>
 * <li>Project: epros</li>
 * <li>Package: com.jecn.system.bean</li>
 * <li>Description: user information bean
 * <li>
 * <li>Copyright: Copyright (c) 2008</li>
 * <li>Company: JECN Technologies </li>
 * <li>Created on Jun 27, 2008, 2:14:48 PM</li>
 *
 * @author chen_zhang
 * @version Epros V1.0
 */
public class UserInformationBean implements Serializable {
	private String userId;
	private String userNumber;
	private String roleId;
	private String stationId;
	private String departmentId;
	private String loginName;
	private String trueName;
	private String orgName;
	private String userPosition;
	private String roleName;
	private String phone;
	private String email;
	private String islock;
	private String password;
	private List<String> list;
	private String isLogin;
	private String phoneStr;
	// 邮箱内外网标识
	private String emailType;
	
	public String getEmailType() {
		return emailType;
	}
	public void setEmailType(String emailType) {
		this.emailType = emailType;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getUserNumber() {
		return userNumber;
	}
	public void setUserNumber(String userNumber) {
		this.userNumber = userNumber;
	}
	public String getRoleId() {
		return roleId;
	}
	public void setRoleId(String roleId) {
		this.roleId = roleId;
	}
	public String getStationId() {
		return stationId;
	}
	public void setStationId(String stationId) {
		this.stationId = stationId;
	}
	public String getDepartmentId() {
		return departmentId;
	}
	public void setDepartmentId(String departmentId) {
		this.departmentId = departmentId;
	}
	public String getLoginName() {
		return loginName;
	}
	public void setLoginName(String loginName) {
		this.loginName = loginName;
	}
	public String getTrueName() {
		return trueName;
	}
	public void setTrueName(String trueName) {
		this.trueName = trueName;
	}
	public String getOrgName() {
		return orgName;
	}
	public void setOrgName(String orgName) {
		this.orgName = orgName;
	}
	public String getUserPosition() {
		return userPosition;
	}
	public void setUserPosition(String userPosition) {
		this.userPosition = userPosition;
	}
	public String getRoleName() {
		return roleName;
	}
	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getIslock() {
		return islock;
	}
	public void setIslock(String islock) {
		this.islock = islock;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public List<String> getList() {
		return list;
	}
	public void setList(List<String> list) {
		this.list = list;
	}
	public String getIsLogin() {
		return isLogin;
	}
	public void setIsLogin(String isLogin) {
		this.isLogin = isLogin;
	}
	public String getPhoneStr() {
		return phoneStr;
	}
	public void setPhoneStr(String phoneStr) {
		this.phoneStr = phoneStr;
	}
}
