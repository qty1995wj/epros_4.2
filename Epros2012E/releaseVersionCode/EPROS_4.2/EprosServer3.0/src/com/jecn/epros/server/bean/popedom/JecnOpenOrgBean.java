package com.jecn.epros.server.bean.popedom;

import java.io.Serializable;
import java.util.List;

public class JecnOpenOrgBean implements Serializable {
	private JecnFlowOrg  jecnFlowOrg=null;
	private List<JecnFlowOrgImage> flowOrgImageList=null;
	public JecnFlowOrg getJecnFlowOrg() {
		return jecnFlowOrg;
	}
	public void setJecnFlowOrg(JecnFlowOrg jecnFlowOrg) {
		this.jecnFlowOrg = jecnFlowOrg;
	}
	public List<JecnFlowOrgImage> getFlowOrgImageList() {
		return flowOrgImageList;
	}
	public void setFlowOrgImageList(List<JecnFlowOrgImage> flowOrgImageList) {
		this.flowOrgImageList = flowOrgImageList;
	}
}
