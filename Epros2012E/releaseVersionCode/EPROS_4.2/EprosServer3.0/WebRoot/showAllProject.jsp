<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>

<%@ taglib prefix="s" uri="/struts-tags"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	 <head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<title><s:text name="projectList" /></title><!-- 项目列表： -->
		<link href="css/top.css" rel="stylesheet" type="text/css" />
		<link rel="stylesheet" type="text/css" href="${basePath}css/jecn.css" />
		<link href="css/global.css" rel="stylesheet" type="text/css" />
		<link href="css/style.css" rel="stylesheet" type="text/css" />
		<script type="text/javascript" src="jquery/jquery-1.6.2.js" charset="UTF-8"></script>
		<script type="text/javascript" src="js/common.js"></script>
		<script language="JavaScript">
              window.onresize=function(){
			    setProjectWH('myProject_id');
			 }
			 function openProj(pro, projectId) {
				projectId.value = projectId;
				pro.value = pro;
				$.ajax( {
					type : 'post',
					url : 'openSelProject.action',
					data : {
						selprojectId : projectId,
						projectName : pro
					},
					success : function(msg) {
						// 刷新父页面
					if (window.opener) {
						window.opener.location.reload();
					}
					// 关闭当前页面
					 window.close();
				}
				})
			}
        </script>
        
	</head>
	
  <body onload="setProjectWH('myProject_id')">
		<div id="myProject_id"  style="overflow-y: auto;width: 100%;" >
			<div style="width: 96%;" align="center">
				<table width="98%" border="0" cellpadding="0" cellspacing="1"
					align="center" style="background-color: #BFBFBF" style="table-layout: fixed;">
					<tr class="FixedTitleRow" align="center" style="background-color: #EAEAEA;">
						<td width="98%" height="25" class="gradient"
							style="word-break: break-all;">
							<s:text name="projectName" /><!-- 项目名称： -->
						</td>
					</tr>
				</table>
			</div>
			<div style="width: 96%;" align="center">
				<table width="98%" border="0" cellpadding="0" cellspacing="1"
					align="center" style="background-color: #BFBFBF" style="table-layout: fixed;">
					<s:iterator value="listProject" status="st">
						<tr align="center" style="background-color: #ffffff"
							onmouseover="style.backgroundColor='#EFEFEF';"
							onmouseout="style.backgroundColor='#ffffff';"
							height="20">
							<td align="right" class="gradient" style="word-break: break-all;" width="2%">
								<s:property value="#st.index+1" />
							</td>
							<td class="t5" style="word-break: break-all;" width="98%">
								<a href="javascript:openProj('<s:property value="projectName" />','<s:property value="projectId"/>');" style="cursor: pointer;" class="table">
								<s:property value="projectName" escape="false"/>
							    </a>
							</td>
						</tr>
					</s:iterator>
				</table>
			</div>
		</div>
		
  </body>
</html>
