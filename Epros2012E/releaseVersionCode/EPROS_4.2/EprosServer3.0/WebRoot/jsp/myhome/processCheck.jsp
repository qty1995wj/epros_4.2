<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
	<head>
	</head>

	<body>
		<div id="div" class="div">
			<div id="processCheckSearch" style="padding: 5px 0px 5px 20px;">
				<!-- 检错类型： -->
				<s:text name="checkType" />
				<select style="width: 150px;" id="prcessCheck_searchType"
					onchange="processCheckGridSearch();">
					<option value="-1">
						<!-- 全部 -->
						<s:text name="all" />
					</option>
					<option value="0">
						<!-- 责任人不存在 -->
						<s:text name="thoseResponsibleForDoesNotExist" />
					</option>
					<option value="1">
						<!-- 责任部门不存在 -->
						<s:text name="responsibilityDepartmentDoesNotExist" />
					</option>
					<option value="2">
						<!-- 角色没有对应岗位 -->
						<s:text name="noCorrespondingPostRole" />
					</option>
				</select>
			</div>
		</div>

		<div id="processCheckGrid_id"></div>
	</body>
</html>
