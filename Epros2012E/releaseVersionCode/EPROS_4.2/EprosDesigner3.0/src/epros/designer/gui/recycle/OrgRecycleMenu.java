package epros.designer.gui.recycle;

import org.apache.log4j.Logger;

import epros.designer.gui.common.JecnConstants;
import epros.designer.gui.common.JecnRecycleMenu;
import epros.designer.util.ConnectionPool;

/*******************************************************************************
 * 文件回收站右键菜单
 * 
 * @author Administrator
 * @date 2015-03-10
 */
public class OrgRecycleMenu extends JecnRecycleMenu {
	private static Logger log = Logger.getLogger(OrgRecycleMenu.class);

	public OrgRecycleMenu(RecycleTree jTree) {
		super(jTree);
	}

	@Override
	protected boolean doRecycleFullData() {
		boolean flag = true;
		try {
			// 恢复当前节点以及子节点
			ConnectionPool.getOrganizationAction().recoverCurAndChild(
					super.getRecycleNodesIdList(), JecnConstants.projectId);
		} catch (Exception e) {
			flag = false;
			log.error("OrgRecycleMenu doRecycleFullData is error", e);
		}

		return flag;
	}

	@Override
	protected boolean doRecycleData() {
		boolean flag = true;
		try {
			// 恢复当前节点
			ConnectionPool.getOrganizationAction().recoverCur(
					super.getRecycleNodesIdList());
		} catch (Exception e) {
			flag = false;
			log.error("OrgRecycleMenu doRecycleData is error", e);
		}
		return flag;
	}

	@Override
	protected boolean trueDelete() {
		boolean flag = true;
		try {
			ConnectionPool.getOrganizationAction().trueDelete(
					super.getRecycleNodesIdList(), JecnConstants.getUserId());
		} catch (Exception e) {
			flag = false;
			log.error("OrgRecycleMenu trueDelete is error", e);
		}
		return flag;
	}
}
