<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>


<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	</head>
    <!-- 流程建设覆盖度3.06废弃 -->
	<body>
		<div id="div" class="div" style="height: 99%;">
			<form action="" id="ruleForm">
				<div id="search" class="divBorder" align="center">
					<div id="processConstructionSearchText" style="overflow: hidden;"
						class="minWidth">
						<table width="100%">
							<tr>
								<td width="10%" height="25px" align="right">
									<!-- 责任部门: -->
									<s:text name="responsibilityDepartmentC" />
								</td>
								<td width="35%" align="left">
									<input type="text" style="width: 63%; vertical-align: middle;"
										id="orgDutyName_construction_id"
										onkeyup="search(orgDutyName_construction_id,orgDutyId_construction_id,search_construction_orgDutyName,'org');"
										onblur="divNode(search_construction_orgDutyName)"
										maxLength="32" />
									<!-- 选择 -->
									<span style="cursor: pointer;"
										onclick='selectOrgPosWindow("organization","orgDutyName_construction_id","orgDutyId_construction_id")'><img
											style="vertical-align: middle;"
											src="${basePath}images/common/select.gif" /> </span>
									<br />
									<div id="search_construction_orgDutyName"
										style="display: none; z-index: 1; position: absolute;"></div>
								</td>


								<td width="10%" align="right">
									<!-- 流程名称： -->
									<s:text name="processNameC"></s:text>
								</td>
								<td width="35%" align="left">
									<input type="text" style="width: 63%; vertical-align: middle;"
										id="processName_construction_id" maxLength="32" readonly="readonly"/>
									<!-- 选择 -->
									<span style="cursor: pointer;"
										onclick='selectProcessOrMaWindow("processName_construction_id","processId_construction_id")'><img
											style="vertical-align: middle;"
											src="${basePath}images/common/select.gif" /> </span>
								</td>

								<td style="vertical-align: middle; width: 10%;" align="center">
									<a onClick="toggle(processConstructionSearchText)"
										style="cursor: pointer;" class="packUp"> <img id="packUp"
											src="${basePath}images/iParticipateInProcess/on.gif"
											align="middle" /> <!-- 收起 --> <span id="searchVal"><s:text
												name="packUp" />
									</span> </a>
								</td>
							</tr>

							<tr>
								<td height="25px" align="right">
									<!-- 状态: -->
									<s:text name="stateC" />
								</td>
								<td align="left">
									<select style="width: 80%" id="state_id">
										<option value="0">
											<!-- 全部 -->
											<s:text name="all" />
										</option>
										<option value="1">
											<!-- 审批 -->
											<s:text name="examinationAndApprovalOf" />
										</option>
										<option value="2">
											<!-- 发布 -->
											<s:text name="released" />
										</option>
										<option value="3">
											<!-- 待建 -->
											<s:text name="toBeBuilt" />
										</option>
									</select>
								</td>

								<td></td>
								<td></td>

								<td></td>
							</tr>
						</table>
					</div>

					<div id="searchBut" align="center">
						<img src="${basePath}images/iParticipateInProcess/search.gif"
							style="cursor: pointer;"
							onclick="processConstructionGridSearch()" />
						<img src="${basePath}images/iParticipateInProcess/reset.gif"
							style="cursor: pointer;"
							onclick="processConstructionSearchReset()" />
					</div>
				</div>
				<!-- 责任部门ID -->
				<input type="hidden" id="orgDutyId_construction_id" value="-1" />
				<!-- 流程ID -->
				<input type="hidden" id="processId_construction_id" value="-1" />
			</form>
			<table width="100%">
				<tr>
					<td width="30%">
						<div id="sysGrid_id" align="center"></div>
					</td>
					<td width="70%">
						<div id="processConstructionGrid_id" style="margin-right: 15px;"></div>
					</td>
				</tr>
				<tr>
					<td></td>
					<td align="center" style="padding-top: 5px;">
						<a onclick="processConstructionDownloadExcel()"
							style="cursor: pointer; color: #005EA7;" class="table"><s:text
								name="downLoadExcel" /> </a>
					</td>
				</tr>
			</table>

		</div>
	</body>
</html>