package com.jecn.epros.server.service.project;

import java.util.List;
import java.util.Set;

import com.jecn.epros.server.bean.project.JecnCurProject;
import com.jecn.epros.server.bean.project.JecnProject;
import com.jecn.epros.server.common.IBaseService;

/**
 * 项目管理处理层接口
 * @author 2012-05-23
 *
 */
public interface IProjectService extends IBaseService<JecnProject, Long> {
	
	/***
	 * 获得所有的项目数据
	 * @return List<JecnProject>
	 * @throws Exception
	 */
	public List<JecnProject> getListProject() throws Exception;
	
	/***
	 * 删除项目
	 * @param listIds
	 * @throws Exception
	 */
	public void deleteProject(List<Long> listIds) throws Exception;
	
	/***
	 * 重命名
	 * @param newName  新名字
	 * @param id	   项目ID
	 */
	public void reProjectName(String newName, Long id) throws Exception;
	
	/**
	 * 打开项目
	 * @param proId
	 */
	public void openProject(Long proId) throws Exception;
	
	/**
	 * 
	 * @author zhangchen Aug 6, 2012
	 * @description： 回收站-获得项目下删除的项目
	 * @param projectId
	 * @return
	 * @throws Exception
	 */
	public List<Object[]> getDelsProject() throws Exception;

	/**
	 * 
	 * @author zhangchen Aug 6, 2012
	 * @description：回收站-恢复删除
	 * @param id
	 * @throws Exception
	 */
	public void updateRecoverDelProject(List<Long> ids) throws Exception;

	/**
	 * @author zhangchen Aug 6, 2012
	 * @description：回收站--删除项目
	 * @param setIds
	 * @throws Exception
	 */
	public void deleteIdsProject(Set<Long> setIds) throws Exception;
	/**
	 * @author yxw 2012-9-5
	 * @description:新增判断是否重名
	 * @param name
	 * @return true 重名      false不重名
	 * @throws Exception
	 */
	public boolean validateAddName(String name) throws Exception;
	/**
	 * @author yxw 2012-9-5
	 * @description:修改判断是否重名
	 * @param name
	 * @param id
	 * @return   true 重名      false不重名
	 * @throws Exception
	 */
	public boolean validateUpdateName(String name, long id)throws Exception;
	

}
