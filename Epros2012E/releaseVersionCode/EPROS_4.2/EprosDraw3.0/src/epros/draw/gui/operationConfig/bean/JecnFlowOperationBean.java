package epros.draw.gui.operationConfig.bean;

import java.util.List;

import epros.draw.designer.JecnFlowInoutT;
import epros.draw.gui.figure.JecnBaseRoleFigure;

/**
 * 流程操作说明基本信息
 * 
 * @author ZHANGXH
 * @date： 日期：Aug 27, 2012 时间：11:32:28 AM
 */
public class JecnFlowOperationBean {
	/** 流程名称 */
	private String flowName;
	/** 流程客户 */
	private String flowCustom;
	/** 目的 */
	private String flowPurpose;
	/** 适用范围 */
	private String applicability;
	/** 术语定义 */
	private String noutGlossary;
	/** 流程输入 */
	private String flowInput;
	/** 流程输出 */
	private String flowOutput;
	/** 不适用范围 */
	private String noApplicability;
	/** 流程编号 */
	private String flowInputNum;
	/** 补充说明 */
	private String flowSupplement;
	/** 概况信息 */
	private String flowSummarize;
	/** 流程记录 */
	private String flowFileStr;
	/** 操作规范 */
	private String practicesStr;
	/** 相关流程 */
	private String relatedFlowStr;
	/** 相关制度 */
	private String relatedRuleStr;
	/** 流程评测及关键指标 */
	private String keyEvaluationIndiStr;
	/** 所有的活动对象 */
	private List<JecnActivityShowBean> listAllActivityShow = null;
	/** 关键活动 问题区域集合 */
	private List<JecnActivityShowBean> listPAActivityShow = null;
	/** 关键活动 关键成功因素集合 */
	private List<JecnActivityShowBean> listKSFActivityShow = null;
	/** 关键活动 关键控制点集合 */
	private List<JecnActivityShowBean> listKCPActivityShow = null;
	/** 关键活动 关键控制点集合（合规） */
	private List<JecnActivityShowBean> listKCPActivityAllowShow = null;
	/** 角色图形集合 */
	private List<JecnBaseRoleFigure> baseRoleFigureList = null;
	/** 相关文件 */
	private String flowRelatedFile;
	/** 自定义要素1 */
	private String customOne;
	/** 自定义要素2 */
	private String customTwo;
	/** 自定义要素3 */
	private String customThree;
	/** 自定义要素4 */
	private String customFour;
	/** 自定义要素5 */
	private String customFive;
	/** 起始活动 */
	private String flowStartActive;
	/** 终止活动 */
	private String flowEndActive;
	/** 0事件驱动,1是时间驱动,2：时间/事件驱动 */
	private String driveType;
	/** 驱动规则 */
	private String driveRules;

	private List<JecnFlowInoutT> flowInTs;

	private List<JecnFlowInoutT> flowOutTs;

	public String getFlowStartActive() {
		return flowStartActive;
	}

	public void setFlowStartActive(String flowStartActive) {
		this.flowStartActive = flowStartActive;
	}

	public String getFlowEndActive() {
		return flowEndActive;
	}

	public void setFlowEndActive(String flowEndActive) {
		this.flowEndActive = flowEndActive;
	}

	public String getCustomTwo() {
		return customTwo;
	}

	public void setCustomTwo(String customTwo) {
		this.customTwo = customTwo;
	}

	public String getCustomThree() {
		return customThree;
	}

	public void setCustomThree(String customThree) {
		this.customThree = customThree;
	}

	public String getCustomFour() {
		return customFour;
	}

	public void setCustomFour(String customFour) {
		this.customFour = customFour;
	}

	public String getCustomFive() {
		return customFive;
	}

	public void setCustomFive(String customFive) {
		this.customFive = customFive;
	}

	public String getCustomOne() {
		return customOne;
	}

	public void setCustomOne(String customOne) {
		this.customOne = customOne;
	}

	public String getFlowRelatedFile() {
		return flowRelatedFile;
	}

	public void setFlowRelatedFile(String flowRelatedFile) {
		this.flowRelatedFile = flowRelatedFile;
	}

	public String getFlowName() {
		return flowName;
	}

	public void setFlowName(String flowName) {
		this.flowName = flowName;
	}

	public String getFlowPurpose() {
		return flowPurpose;
	}

	public void setFlowPurpose(String flowPurpose) {
		this.flowPurpose = flowPurpose;
	}

	public String getApplicability() {
		return applicability;
	}

	public void setApplicability(String applicability) {
		this.applicability = applicability;
	}

	public String getNoutGlossary() {
		return noutGlossary;
	}

	public void setNoutGlossary(String noutGlossary) {
		this.noutGlossary = noutGlossary;
	}

	public String getFlowInput() {
		return flowInput;
	}

	public void setFlowInput(String flowInput) {
		this.flowInput = flowInput;
	}

	public String getFlowOutput() {
		return flowOutput;
	}

	public void setFlowOutput(String flowOutput) {
		this.flowOutput = flowOutput;
	}

	public String getNoApplicability() {
		return noApplicability;
	}

	public void setNoApplicability(String noApplicability) {
		this.noApplicability = noApplicability;
	}

	public String getFlowSupplement() {
		return flowSupplement;
	}

	public void setFlowSupplement(String flowSupplement) {
		this.flowSupplement = flowSupplement;
	}

	public List<JecnActivityShowBean> getListAllActivityShow() {
		return listAllActivityShow;
	}

	public void setListAllActivityShow(List<JecnActivityShowBean> listAllActivityShow) {
		this.listAllActivityShow = listAllActivityShow;
	}

	public List<JecnActivityShowBean> getListPAActivityShow() {
		return listPAActivityShow;
	}

	public void setListPAActivityShow(List<JecnActivityShowBean> listPAActivityShow) {
		this.listPAActivityShow = listPAActivityShow;
	}

	public List<JecnActivityShowBean> getListKSFActivityShow() {
		return listKSFActivityShow;
	}

	public void setListKSFActivityShow(List<JecnActivityShowBean> listKSFActivityShow) {
		this.listKSFActivityShow = listKSFActivityShow;
	}

	public List<JecnActivityShowBean> getListKCPActivityShow() {
		return listKCPActivityShow;
	}

	public void setListKCPActivityShow(List<JecnActivityShowBean> listKCPActivityShow) {
		this.listKCPActivityShow = listKCPActivityShow;
	}

	public List<JecnBaseRoleFigure> getBaseRoleFigureList() {
		return baseRoleFigureList;
	}

	public void setBaseRoleFigureList(List<JecnBaseRoleFigure> baseRoleFigureList) {
		this.baseRoleFigureList = baseRoleFigureList;
	}

	public String getFlowInputNum() {
		return flowInputNum;
	}

	public void setFlowInputNum(String flowInputNum) {
		this.flowInputNum = flowInputNum;
	}

	public String getFlowCustom() {
		return flowCustom;
	}

	public void setFlowCustom(String flowCustom) {
		this.flowCustom = flowCustom;
	}

	public String getFlowSummarize() {
		return flowSummarize;
	}

	public void setFlowSummarize(String flowSummarize) {
		this.flowSummarize = flowSummarize;
	}

	public String getFlowFileStr() {
		return flowFileStr;
	}

	public void setFlowFileStr(String flowFileStr) {
		this.flowFileStr = flowFileStr;
	}

	public String getPracticesStr() {
		return practicesStr;
	}

	public void setPracticesStr(String practicesStr) {
		this.practicesStr = practicesStr;
	}

	public String getRelatedFlowStr() {
		return relatedFlowStr;
	}

	public void setRelatedFlowStr(String relatedFlowStr) {
		this.relatedFlowStr = relatedFlowStr;
	}

	public String getRelatedRuleStr() {
		return relatedRuleStr;
	}

	public void setRelatedRuleStr(String relatedRuleStr) {
		this.relatedRuleStr = relatedRuleStr;
	}

	public String getKeyEvaluationIndiStr() {
		return keyEvaluationIndiStr;
	}

	public void setKeyEvaluationIndiStr(String keyEvaluationIndiStr) {
		this.keyEvaluationIndiStr = keyEvaluationIndiStr;
	}

	public String getDriveType() {
		return driveType;
	}

	public void setDriveType(String driveType) {
		this.driveType = driveType;
	}

	public String getDriveRules() {
		return driveRules;
	}

	public void setDriveRules(String driveRules) {
		this.driveRules = driveRules;
	}

	public List<JecnActivityShowBean> getListKCPActivityAllowShow() {
		return listKCPActivityAllowShow;
	}

	public void setListKCPActivityAllowShow(List<JecnActivityShowBean> listKCPActivityAllowShow) {
		this.listKCPActivityAllowShow = listKCPActivityAllowShow;
	}

	public List<JecnFlowInoutT> getFlowInTs() {
		return flowInTs;
	}

	public void setFlowInTs(List<JecnFlowInoutT> flowInTs) {
		this.flowInTs = flowInTs;
	}

	public List<JecnFlowInoutT> getFlowOutTs() {
		return flowOutTs;
	}

	public void setFlowOutTs(List<JecnFlowInoutT> flowOutTs) {
		this.flowOutTs = flowOutTs;
	}

}
