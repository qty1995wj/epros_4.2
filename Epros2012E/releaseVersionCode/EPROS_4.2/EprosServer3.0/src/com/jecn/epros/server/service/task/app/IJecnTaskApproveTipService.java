package com.jecn.epros.server.service.task.app;

import com.jecn.epros.server.bean.popedom.JecnUser;
import com.jecn.epros.server.common.IBaseService;

/**
 * @author weidp
 * @date： 日期：2014-5-14 时间：下午04:55:44
 */
public interface IJecnTaskApproveTipService extends
		IBaseService<JecnUser, Long> {

	/**
	 * 发送提醒邮件
	 * 
	 * @author weidp
	 * @date： 日期：2014-5-14 时间：下午04:59:39
	 */
	public void sendTipEmail() throws Exception;

}
