package epros.draw.event.flowElement;

import java.awt.Dimension;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.InputEvent;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import javax.swing.JPanel;
import javax.swing.JScrollBar;
import javax.swing.SwingUtilities;

import epros.draw.constant.JecnToolBoxConstant.MapElemType;
import epros.draw.constant.JecnToolBoxConstant.MapType;
import epros.draw.constant.JecnWorkflowConstant.EditPointType;
import epros.draw.event.JecnKeyHandler;
import epros.draw.event.JecnTabbedEventProcess;
import epros.draw.gui.box.JecnToolBoxMainPanel;
import epros.draw.gui.designer.JecnDesignerProcess;
import epros.draw.gui.figure.JecnAdjacentFigure;
import epros.draw.gui.figure.JecnBaseFigurePanel;
import epros.draw.gui.figure.MapLineFigure;
import epros.draw.gui.figure.shape.JecnTempFigurePanel;
import epros.draw.gui.figure.shape.part.CommentText;
import epros.draw.gui.figure.shape.part.FromFigure;
import epros.draw.gui.figure.shape.part.ToFigure;
import epros.draw.gui.figure.shape.total.ModelFigure;
import epros.draw.gui.figure.unit.JecnDraggedFiguresPaintLine;
import epros.draw.gui.figure.unit.JecnFlowElementCopyAndPaste;
import epros.draw.gui.figure.unit.JecnPaintFigureUnit;
import epros.draw.gui.figure.unit.JecnSelectFigureXY;
import epros.draw.gui.figure.unit.JecnTempFigureBean;
import epros.draw.gui.figure.unit.SnapToGrid;
import epros.draw.gui.line.JecnBaseDividingLinePanel;
import epros.draw.gui.line.JecnBaseManhattanLinePanel;
import epros.draw.gui.line.shape.DividingLine;
import epros.draw.gui.line.shape.LineSegment;
import epros.draw.gui.line.shape.TempDividingLine;
import epros.draw.gui.swing.JecnBaseFlowElementPanel;
import epros.draw.gui.top.toolbar.JecnElementAttributesUtil;
import epros.draw.gui.workflow.JecnDrawMainPanel;
import epros.draw.gui.workflow.aid.JecnLinkFigureMainProcess;
import epros.draw.gui.workflow.popup.JecnFlowElemPopupMenu;
import epros.draw.gui.workflow.tabbedPane.JecnDrawDesktopPane;
import epros.draw.gui.workflow.util.JecnWorkflowUtil;
import epros.draw.unredo.JecnUnRedoProcess;
import epros.draw.unredo.JecnUnRedoProcessFactory;
import epros.draw.unredo.data.JecnUndoRedoData;
import epros.draw.util.DrawCommon;

/**
 * 流程元素鼠标事件
 * 
 * @author ZHANGXH
 * @date： 日期：Apr 19, 2012 时间：10:42:26 AM
 */
public class JecnFlowElementEventProcess {
	/** 记录鼠标点击图形时鼠标相对图形的坐标点 */
	public static Point oldPoint = null;
	/** 鼠标点击选中的所有图形 */
	public static List<JecnBaseFigurePanel> figureList = null;

	/** 是否执行拖动 */
	public static boolean isDragged = false;
	/** 获取拖动图形边框的最开始位置 */
	public static Point oldTempFigurePoint = null;
	/** 鼠标圈选范围最大坐标和最小坐标值记录对象 */
	public static JecnSelectFigureXY selectXY = null;

	/** 拖动是否执行边界处理 true:不执行边界处理 */
	public static boolean isBound = false;

	/**
	 * 图形的鼠标移动事件
	 * 
	 * @param e
	 */
	public void mouseMoved(MouseEvent e) {
		// 面板鼠标进入面板事件处理
		JecnTabbedEventProcess.mouseeEntered(e);
		JecnDrawDesktopPane desktopPane = JecnDrawMainPanel.getMainPanel().getWorkflow();
		if (desktopPane == null || !(e.getSource() instanceof JecnBaseFlowElementPanel)
				|| desktopPane.getJecnTempFigurePanel() == null) {
			return;
		}
		JecnBaseFlowElementPanel souseFigure = (JecnBaseFlowElementPanel) e.getSource();
		if (desktopPane.getJecnTempFigurePanel() == null) {
			return;
		}

		// 事件传递 (图形鼠标点击事件转换为面板鼠标点击事件)
		MouseEvent desMouseEvent = SwingUtilities.convertMouseEvent(souseFigure, e, desktopPane);
		JecnTabbedEventProcess.mouseMoved(desMouseEvent);

		// 面板放大，滚动条跟随移动
		reSetWorkFlowSizeAndScroolFollow(desktopPane.getJecnTempFigurePanel());
		desktopPane.updateUI();
	}

	/**
	 * 
	 * 
	 * @param souseFigure
	 *            鼠标移动的源图形
	 * @param e
	 *            MouseEvent鼠标事件
	 * @param tempFigureBean
	 *            拖动流程元素获取图形基础数据（图形唯一标识、高度和宽度，旋转角度）
	 */
	public Point getCenterPoint(JecnBaseFlowElementPanel souseFigure, MouseEvent e, JecnTempFigureBean tempFigureBean) {
		JecnDrawDesktopPane desktopPane = JecnDrawMainPanel.getMainPanel().getWorkflow();
		// 事件传递 (图形鼠标点击事件转换为面板鼠标点击事件)
		MouseEvent desMouseEvent = SwingUtilities.convertMouseEvent(souseFigure, e, desktopPane);
		// 获取鼠标相对图形中心点
		Point centerPoint = JecnTabbedEventProcess.getCenterPoint(desMouseEvent.getPoint(), tempFigureBean
				.getUserWidth(), tempFigureBean.getUserHeight());

		// 记录图形位置点
		tempFigureBean.setLocalX(centerPoint.x);
		tempFigureBean.setLocalY(centerPoint.y);

		// 获取待添加图形类型
		MapElemType mapElemType = tempFigureBean.getMapElemType();
		switch (mapElemType) {
		case ParallelLines:
			centerPoint = new Point(0, desMouseEvent.getY());
			break;
		case VerticalLine:
			centerPoint = new Point(desMouseEvent.getX(), 0);
			break;
		case HDividingLine:
			centerPoint = new Point(desMouseEvent.getX(), desMouseEvent.getY());
			break;
		case VDividingLine:
		default:
			break;
		}
		return centerPoint;
	}

	/**
	 * 流程元素鼠标按下事件处理
	 * 
	 * @param e
	 *            MouseEvent 鼠标事件
	 */
	public void flowFigurePressed(MouseEvent e) {
		// BUTTON1(1：左键）,BUTTON2（2：中键）,BUTTON3（3：右键）
		// e.getModifiersEx()4:right, 16:left 18:Ctrl+左键

		// 获取面板
		JecnDrawDesktopPane desktopPane = JecnDrawMainPanel.getMainPanel().getWorkflow();
		if (desktopPane == null) {
			return;
		}

		if (!isDragged && e.getButton() == MouseEvent.BUTTON1) {
			// 点击是否为连接线 True:点击未连接线，false 点击不为连接线
			boolean isManLine = clickManLine(e);
			/** 鼠标点击位置是否在线段区域内，是，则选中线段 */
			boolean isSelectManLine = isSelectManLine(e);
			if (isManLine || isSelectManLine) {
				oldPoint = e.getPoint();
				return;
			}
			// 执行双击编辑动作 true：执行成功；false：没有执行
			boolean edit = flowFigureDoubleClicked(e);
			if (edit) {
				return;
			}
		}

		// 元素库点击的连接线，画图面板将添加连接线,鼠标点击元素，说明要在元素上添加连线
		if (JecnTabbedEventProcess.isShowLineNoFigure()) {
			// 事件传递 (图形鼠标点击事件转换为面板鼠标点击事件)
			MouseEvent desMouseEvent = SwingUtilities.convertMouseEvent((JecnBaseFlowElementPanel) e.getSource(), e,
					desktopPane);
			desktopPane.setStartPoint(desMouseEvent.getPoint());
		}

		// 流程元素
		JecnBaseFlowElementPanel flowElementPanel = (JecnBaseFlowElementPanel) e.getSource();
		if (((e.getModifiersEx() & InputEvent.BUTTON3_DOWN_MASK) != 0)
				&& ((e.getModifiersEx() & InputEvent.BUTTON1_DOWN_MASK) != 0)) {
			// 左右键同时按住时,不设置按下位置点
		} else {
			// 记录鼠标点击的点
			oldPoint = e.getPoint();
		}

		// 不添加流程元素情况下，选中流程元素
		// 流程元素库
		JecnToolBoxMainPanel boxPanel = JecnDrawMainPanel.getMainPanel().getBoxPanel();

		// 是否按住Ctrl true：按住Ctrl false：为按住
		boolean isCtrl = JecnWorkflowUtil.isCtrl(e);

		if (MapElemType.guide.equals(boxPanel.getSelectedBtnType())) {// 如果鼠标状态为指针
			if (!desktopPane.isContainsSelectList(flowElementPanel)) {// 不包含
				desktopPane.setSelectArea(false);
				// 画图面板上选中流程元素集合中没有包含当前点击的流程元素时，清除之前画图面板上所有选中状态，添加此流程元素
				JecnWorkflowUtil.clearListAndAddElement(flowElementPanel, isCtrl);
			} else if (flowElementPanel instanceof ModelFigure) {// 泳池
				ModelFigure modelFigure = (ModelFigure) flowElementPanel;
				if (modelFigure.isChickEditText(e.getPoint())) {// 鼠标是否在泳池的编辑区域内
					JecnPaintFigureUnit.showResizeHandles(modelFigure);
				}
			} else if (flowElementPanel instanceof ToFigure || flowElementPanel instanceof FromFigure) {
				if (e.getButton() == MouseEvent.BUTTON3) {// 如果是右键
					// 隐藏TO FROM 面板
					JecnPaintFigureUnit.hiddenToOrFromLinkActive();
				} else {
					JecnPaintFigureUnit.showToOrFromLinkActive(flowElementPanel);
				}
			}
		}
	}

	/**
	 * 鼠标点击流程元素是否为连接线
	 * 
	 * @param e
	 *            鼠标事件
	 * @return true:鼠标点击为连接线，false：鼠标点击不为连接线
	 */
	private boolean clickManLine(MouseEvent e) {
		// 获取面板
		JecnDrawDesktopPane workflow = JecnDrawMainPanel.getMainPanel().getWorkflow();

		if (workflow == null || e.getSource() == null || !(e.getSource() instanceof LineSegment)) {
			return false;
		}
		LineSegment lineSegment = (LineSegment) e.getSource();
		// 是否按住Ctrl true：按住Ctrl false：为按住
		boolean isCtrl = JecnWorkflowUtil.isCtrl(e);
		if (!isCtrl) {
			// 清空选中的图形元素集合的所有显示点
			JecnWorkflowUtil.hideAllResizeHandle();
		}
		// 添加小线段所在的线段到面板选中集合
		JecnWorkflowUtil.setCurrentSelect(lineSegment.getBaseManhattanLine());
		if (workflow.getCurrentSelectElement().size() == 1) {
			// 修改工具栏的状态
			JecnElementAttributesUtil.updateEditTool(lineSegment.getBaseManhattanLine());
		}
		return true;
	}

	/**
	 * 是否选中连接线
	 * 
	 * @param e
	 * @return
	 */
	private boolean isSelectManLine(MouseEvent e) {
		JPanel jPanel = null;
		if (e.getSource() instanceof LineSegment) {
			LineSegment lineSegment = (LineSegment) e.getSource();
			jPanel = lineSegment.getBaseManhattanLine();
		} else {
			jPanel = (JecnBaseFlowElementPanel) e.getSource();
		}
		JecnDrawDesktopPane desktopPane = JecnDrawMainPanel.getMainPanel().getWorkflow();
		// 事件传递 (图形鼠标点击事件转换为面板鼠标点击事件)
		MouseEvent desMouseEvent = SwingUtilities.convertMouseEvent(jPanel, e, desktopPane);
		// 判断是否点击的连接线 周围区域
		return JecnTabbedEventProcess.isMousePressedLine(desMouseEvent.getPoint(), desMouseEvent);
	}

	/**
	 * 点击图形鼠标释放事件
	 * 
	 * @param e
	 */
	public void flowFigureReleased(MouseEvent e) {
		// 获取面板
		JecnDrawDesktopPane desktopPane = JecnDrawMainPanel.getMainPanel().getWorkflow();
		if (desktopPane == null || e.getSource() == null) {
			return;
		}

		// 添加连接线，连接线不依赖元素单独存在处理
		if (JecnTabbedEventProcess.isShowLineNoFigure()) {
			MouseEvent desMouseEvent = SwingUtilities.convertMouseEvent((JecnBaseFlowElementPanel) e.getSource(), e,
					desktopPane);
			JecnTabbedEventProcess.drawDesktopPaneMouseReleased(desMouseEvent);
			return;
		}

		// ***************1-2.更新工具栏状态***************//
		JecnBaseFlowElementPanel flowElemetnPanel = null;
		if (e.getSource() instanceof LineSegment) {// 如果点击的为连接线
			LineSegment lineSegment = (LineSegment) e.getSource();

			flowElemetnPanel = lineSegment.getBaseManhattanLine();
		} else {
			// 流程元素
			flowElemetnPanel = (JecnBaseFlowElementPanel) e.getSource();

			// ***************2.添加流程元素***************//
			// 选中流程元素库中流程图标，点击画图面板上图形添加流程元素
			boolean isFigure = mousePressedFigure(flowElemetnPanel, e);
			if (isFigure) {
				// 单选时清楚 图形自动对齐连接线
				JecnAdjacentFigure.removeTempLine();
				return;
			}

			if (desktopPane.getTemplateId() != null && e.getButton() == MouseEvent.BUTTON3) {// 右键取消条件
				// 添加完成后，选中指针
				JecnDrawMainPanel.getMainPanel().getBoxPanel().initGuide();
				// 拖动标识设为默认值
				isDragged = false;
				// 删除拖动生成的虚拟图形
				JecnTabbedEventProcess.removeTempFigure();
				return;
			} else {
				// 面板添加片段
				boolean isTemplate = JecnTabbedEventProcess.addTemplatePanel();
				if (isTemplate) {
					// 拖动标识设为默认值
					isDragged = false;
					// 删除拖动生成的虚拟图形
					JecnTabbedEventProcess.removeTempFigure();
					return;
				}
			}
		}

		// 格式刷复制
		if (desktopPane.getFormatPainterData() != null) {
			JecnFlowElementCopyAndPaste.singleFormatPainterFigureAttrs(flowElemetnPanel);
			return;
		}
		// ***************3.Ctrl+左键复制选中流程元素***************//
		if ((e.getModifiersEx() & InputEvent.BUTTON1_DOWN_MASK) != 0 && e.getButton() == MouseEvent.BUTTON3) {// 右键释放且左键还处于按住状态
			return;
		}
		if ((e.getModifiersEx() & InputEvent.BUTTON3_DOWN_MASK) != 0 && e.getButton() == MouseEvent.BUTTON1) {// 左键释放且右键还处于按住状态
			return;
		}

		// 是否按住Ctrl true：按住Ctrl false：为按住
		boolean isCtrl = JecnWorkflowUtil.isCtrl(e);
		// 选中流程元素个数
		int count = desktopPane.getCurrentSelectElement().size();
		if (count == 0) {// 无选中直接返回
			return;
		}

		// 元素属性特殊处理
		JecnDesignerProcess.getDesignerProcess().showElementAttributePanel();

		if (!isDragged) {// 没有执行拖动中菜单
			if (!isCtrl && e.getButton() == MouseEvent.BUTTON3 && e.isPopupTrigger()) {// 没有ctrl键
				// 并执行的是鼠标右键
				// ***************5.右键菜单显示***************//
				rightPopupMenuProcess(e);
			} else {
				if (desktopPane.getCurrentSelectElement().size() == 1) {
					// 修改工具栏的状态
					JecnElementAttributesUtil.updateEditTool(desktopPane.getCurrentSelectElement().get(0));
				}
			}
			return;
		}
		// 画图面板选中流程元素
		List<JecnBaseFlowElementPanel> selectList = desktopPane.getCurrentSelectElement();

		if (selectList.size() == 1) {
			// 泳池特殊处理
			selectList = JecnWorkflowUtil.selectModelFigureContainsEles(selectList.get(0));
		}

		// ***************4.移动流程元素***************//
		if (isCtrl) {// Ctrl+左键：拖动复制
			mouseReleaseCopyProcess(e.getPoint(), selectList);
			// 单选时清楚 图形自动对齐连接线
			JecnAdjacentFigure.removeTempLine();
		} else {
			// 拖动图形鼠标释放后画图处理
			mouseReleaseDrawPanel(selectList);
		}
		if (desktopPane.getCurrentSelectElement().size() == 1) {// 有且只有选中一个图形时
			// 修改工具栏的状态
			JecnElementAttributesUtil.updateEditTool(desktopPane.getCurrentSelectElement().get(0));
			// 单选时情况,图形自动对齐红色连接线
			// 获取拖动图形时，指向附近图形的小线段，便于提示用户拖动图形与附近图形中心点对齐功能
			JecnAdjacentFigure.removeTempLine();
		}

		if (!isCtrl && figureList != null && figureList.size() > 0) {// 存在选中图形集合
			// 重画图形相关的连接线
			JecnDraggedFiguresPaintLine.paintLineByFigures(figureList);
		}

		// 拖动标识设为默认值
		isDragged = false;
		// 删除拖动生成的虚拟图形
		JecnTabbedEventProcess.removeTempFigure();
		// 清空临时线段
		if (desktopPane.getTempLine() != null) {
			desktopPane.getTempLine().disposeLines();
		}
		desktopPane.getCurrDrawPanelSizeByPanelList();
		desktopPane.updateUI();
	}

	/**
	 * 点击图形鼠标拖动事件
	 * 
	 * @param e
	 */
	public static void flowFigureDragged(MouseEvent e) {
		// 画图面板
		JecnDrawDesktopPane workflow = JecnDrawMainPanel.getMainPanel().getWorkflow();

		if (e.getSource() instanceof JecnBaseFlowElementPanel
				&& (JecnTabbedEventProcess.isShowLineNoFigure() || workflow.getCurrentSelectElement().size() == 0)) {
			// 事件传递 (图形鼠标点击事件转换为面板鼠标点击事件)
			MouseEvent desMouseEvent = SwingUtilities.convertMouseEvent((JecnBaseFlowElementPanel) e.getSource(), e,
					workflow);
			JecnTabbedEventProcess.buttonMouseDragged(desMouseEvent);
			return;
		}

		// 是否存在面板，面板上是否存在选中元素
		if (workflow == null || oldPoint == null || workflow.getCurrentSelectElement().size() == 0) {
			return;
		}

		// **************清除快捷添加图形显示小三角形**************//
		// 清除快捷添加图形显示小三角形
		JecnDrawMainPanel.getMainPanel().interruptLinkFigure();
		// **************清除快捷添加图形显示小三角形**************//
		// 清除to和from快捷设置活动编号面板
		JecnPaintFigureUnit.hiddenToOrFromLinkActive();
		if ((e.getModifiersEx() & InputEvent.BUTTON1_DOWN_MASK) != 0 && e.getButton() == MouseEvent.BUTTON3) {// 鼠标左键按住同时拖动时右键，右键事件拖动不执行
			return;
		}

		// 画图面板选中流程元素
		List<JecnBaseFlowElementPanel> selectList = workflow.getCurrentSelectElement();

		// 判断是否只拖动的连线（流程架构-连线不包含元素）
		boolean isDraggedManLine = isOnlyDraggedManLine(selectList);
		// 鼠标移动距离
		int scrollX = e.getX() - oldPoint.getLocation().x;
		int scrollY = e.getY() - oldPoint.getLocation().y;

		if (scrollX == 0 && scrollY == 0) {
			return;
		}

		// 根据网格获取鼠标坐标点 拖动鼠标位移按照网格取值
		Point tempPoint = SnapToGrid.checkIsGrid(scrollX, scrollY);
		if (!isBound) {
			// 虚拟图形拖动到边界处理
			dragToBound(tempPoint);
		}
		for (JecnBaseFlowElementPanel curFigurePanel : selectList) {
			// 泳池特殊处理
			selectList = JecnWorkflowUtil.selectModelFigureContainsEles(curFigurePanel);
			break;
		}
		if (!isDragged) {// 拖动前处理
			// 获取选中图形集合对象
			figureList = new ArrayList<JecnBaseFigurePanel>();
			// 设置拖动图形虚拟边框大小
			reSetJecnTempFigurePanel(selectList);
			// 设置拖动状态
			isDragged = true;
		}

		if (isDraggedManLine) {// 只拖动的连线（流程架构-连线不包含元素）
			JecnBaseFlowElementPanel elementPanel = selectList.get(0);
			if (elementPanel instanceof JecnBaseManhattanLinePanel) {
				selectList = JecnKeyHandler.addManLineFigure(new ArrayList<JecnBaseFigurePanel>(), elementPanel);
				selectList.add(elementPanel);
			}
		}
		// 获取虚拟拖动图形的集合
		JecnTempFigurePanel tempFigurePanel = workflow.getJecnTempFigurePanel();

		if (tempFigurePanel == null) {
			return;
		}

		// 设置虚拟图形位置
		tempFigurePanel.setLocation(getTempPoint(oldTempFigurePoint, tempPoint));
		int selectCount = selectList.size();
		// 选中流程元素集合处理
		for (JecnBaseFlowElementPanel curFigurePanel : selectList) {
			if (curFigurePanel instanceof JecnBaseFigurePanel) {// 是否为图形
				JecnBaseFigurePanel curFigure = (JecnBaseFigurePanel) curFigurePanel;
				// 设置虚拟图形边框数据
				reSetTempFigureBean(curFigure, getTempPoint(curFigure.getLocation(), tempPoint));
				if (!figureList.contains(curFigure)) {// 记录选中的所有图形
					figureList.add(curFigure);
				}
				if (selectCount == 1) {
					// 拖动单个虚拟图形边框生成相邻图形间的对齐虚拟直线
					drawTempFigureLine(curFigure);
				}
			} else if (curFigurePanel instanceof JecnBaseDividingLinePanel) {
				JecnBaseDividingLinePanel curDiviLine = (JecnBaseDividingLinePanel) curFigurePanel;
				if (curDiviLine instanceof DividingLine) {
					// 添加虚拟分隔符线段
					addTempDiviLine(curDiviLine, tempPoint);
					if (!workflow.getJecnTempFigurePanel().getTempFigureList()
							.contains(curDiviLine.getTempFigureBean())) {// 记录选中的所有图形
						workflow.getJecnTempFigurePanel().getTempFigureList().add(curDiviLine.getTempFigureBean());
					}
				} else {
					reSetTempFigureBean(curDiviLine, getTempPoint(curDiviLine.getLocation(), tempPoint));
				}
			} else if (isDraggedManLine && curFigurePanel instanceof JecnBaseManhattanLinePanel) {
				JecnBaseManhattanLinePanel linePanel = (JecnBaseManhattanLinePanel) curFigurePanel;
				if (workflow.getTempLine() == null) {
					// 创建连接线临时线段对象
					JecnEditPortEventProcess.creatTempLine(workflow);
				}
				Point tmpStartP = linePanel.getStartFigure().getTempFigureBean().getLocalPoint();
				Point tmpEndP = linePanel.getEndFigure().getTempFigureBean().getLocalPoint();
				workflow.getTempLine().setArrowStartPoint(tmpStartP);
				workflow.getTempLine().setArrowEndPoint(tmpEndP);
				drawTmpLine(tmpStartP, linePanel.getStartEditPointType(), tmpEndP, linePanel.getEndEditPointType());
			}
		}

		if (selectList.size() == 1) {
			reSetWorkFlowSizeAndScroolFollow(tempFigurePanel);
		}
		workflow.updateUI();
	}

	/**
	 * 判断是否只拖动的连线（流程架构-连线不包含元素）
	 * 
	 * @param selectList
	 * @return
	 */
	private static boolean isOnlyDraggedManLine(List<JecnBaseFlowElementPanel> selectList) {
		if (selectList.size() == 1) {
			JecnBaseFlowElementPanel elementPanel = selectList.get(0);
			if (elementPanel instanceof JecnBaseManhattanLinePanel) {
				JecnBaseManhattanLinePanel linePanel = (JecnBaseManhattanLinePanel) elementPanel;
				if (linePanel.getStartFigure() instanceof MapLineFigure
						&& linePanel.getEndFigure() instanceof MapLineFigure) {
					return true;
				}
			}
		}
		return false;
	}

	private static void drawTmpLine(Point fromPt, EditPointType fromDir, Point toPt, EditPointType toDir) {
		JecnDrawDesktopPane desktopPane = JecnDrawMainPanel.getMainPanel().getWorkflow();
		// 清空临时线段
		desktopPane.getTempLine().disposeLines();
		// 拖动时生成虚拟连接线线段
		desktopPane.getTempLine().paintDraggedLinePort(fromPt, fromDir, toPt, toDir);
		desktopPane.getWorkflowZoomProcess().zoomManhattanLine(desktopPane.getTempLine());
	}

	/**
	 * 面板放大，滚动条跟随移动
	 * 
	 * @param tempFigurePanel
	 * @return void
	 * @throws
	 */
	public static void reSetWorkFlowSizeAndScroolFollow(JecnTempFigurePanel tempFigurePanel) {
		if (tempFigurePanel.getTempFigureList().isEmpty()) {
			return;
		}
		if (MapElemType.ModelFigure != tempFigurePanel.getTempFigureList().get(0).getMapElemType()) {
			JecnFlowElementEventProcess.getReSetDrawPanelSize(tempFigurePanel.getLocation(),
					tempFigurePanel.getWidth(), tempFigurePanel.getHeight());
		}

		JecnFlowElementEventProcess.draggedScrollPanel(tempFigurePanel.getLocation(), tempFigurePanel.getWidth(),
				tempFigurePanel.getHeight());
	}

	/** 横向滚动条 */
	private static JScrollBar hBar = null;
	/** 纵向滚动条 */
	private static JScrollBar vBar = null;

	/** 滚动条位移 **/
	private static int scrollOff = 20;

	/**
	 * 拖动连接线结束点面板横竖滚动条跟随移动
	 * 
	 * @param tempEndPoint
	 *            Point 连接线结束点
	 */
	public static void draggedScrollPanel(Point tempEndPoint, int width, int height) {
		JecnDrawDesktopPane desktopPane = JecnDrawMainPanel.getMainPanel().getWorkflow();
		if (tempEndPoint == null || desktopPane == null) {
			return;
		}
		// 获取横向滚动条
		hBar = desktopPane.getScrollPanle().getHorizontalScrollBar();
		// 获取横向滚动条
		vBar = desktopPane.getScrollPanle().getVerticalScrollBar();
		// 横向滚动条的值
		int left = hBar.getValue();
		// 横向滚动条值 + 横向滚动条宽度
		int right = left
				+ DrawCommon.convertDoubleToInt(desktopPane.getScrollPanle().getViewportBorderBounds().getWidth());
		// 纵向滚动条的值
		int head = vBar.getValue();
		// 纵向滚动条值 + 纵向滚动条高度
		int bottom = vBar.getValue()
				+ DrawCommon.convertDoubleToInt(desktopPane.getScrollPanle().getViewportBorderBounds().getHeight());

		if (tempEndPoint.x + width >= right) {// 如果连接线结束点横坐标大于等于横向滚动条边界值，滚动条值右移50
			hBar.setValue(hBar.getValue() + scrollOff);
		}
		if (tempEndPoint.x <= left) {// 如果连接线结束点横坐标小于或等于滚动条值，滚动条左移50
			hBar.setValue(hBar.getValue() - scrollOff);
		}
		if (tempEndPoint.y + height >= bottom) {// 如果连接线结束点横坐标大于等于纵向向滚动条边界值，滚动条值下移50
			vBar.setValue(vBar.getValue() + scrollOff);
		}
		if (tempEndPoint.y <= head) {// 如果连接线结束点横坐标大于等于横向滚动值，滚动条值上移50
			vBar.setValue(vBar.getValue() - scrollOff);
		}
	}

	/**
	 * 面板自动放大
	 * 
	 * @param location
	 * @param width
	 * @param height
	 */
	public static void getReSetDrawPanelSize(Point location, int width, int height) {
		// 画图面板
		JecnDrawDesktopPane workflow = JecnDrawMainPanel.getMainPanel().getWorkflow();
		int workWidth = workflow.getWidth();
		int workHeight = workflow.getHeight();

		int maxX = 0;
		int maxY = 0;
		if (workWidth < location.x + width) {
			maxX = location.x + width + 1000;
		} else {
			maxX = workWidth;
		}

		if (workHeight <= location.y + height) {
			maxY = location.y + height + 500;
		} else {
			maxY = workHeight;
		}
		if (workWidth < maxX || workHeight < maxY) {
			workflow.setPreferredSize(new Dimension(maxX, maxY));
		}
		workflow.repaint();
	}

	/**
	 * 虚拟图形拖动到边界处理
	 * 
	 * @param scrollPoint
	 *            Point拖动鼠标移动的位移坐标
	 */
	public static void dragToBound(Point scrollPoint) {
		// 画图面板
		JecnDrawDesktopPane workflow = JecnDrawMainPanel.getMainPanel().getWorkflow();

		// 是否存在面板，面板上是否存在选中元素
		if (workflow == null || scrollPoint == null || oldTempFigurePoint == null
				|| workflow.getJecnTempFigurePanel() == null) {
			return;
		}
		if (scrollPoint.x + oldTempFigurePoint.x <= 0) {
			scrollPoint.x = -oldTempFigurePoint.x;
		}
		if (scrollPoint.y + oldTempFigurePoint.y <= 0) {
			scrollPoint.y = -oldTempFigurePoint.y;
		}
	}

	/**
	 * 
	 * 设置虚拟图形边框数据
	 * 
	 * @param flowElementPanel
	 * @param localPoint
	 */
	public static void reSetTempFigureBean(JecnBaseFlowElementPanel flowElementPanel, Point localPoint) {
		JecnTempFigureBean tempFigureBean = flowElementPanel.getTempFigureBean();
		JecnDrawDesktopPane desktopPane = JecnDrawMainPanel.getMainPanel().getWorkflow();
		if (desktopPane == null || tempFigureBean == null || localPoint == null) {
			return;
		}

		tempFigureBean.setCurrentAngle(flowElementPanel.getFlowElementData().getCurrentAngle());

		// 设置图形位置点和大小
		tempFigureBean.reSetTempFigureBean(localPoint, flowElementPanel.getWidth(), flowElementPanel.getHeight());

		if (!desktopPane.getJecnTempFigurePanel().getTempFigureList().contains(tempFigureBean)) {// 记录选中的所有图形
			desktopPane.getJecnTempFigurePanel().getTempFigureList().add(tempFigureBean);
		}
	}

	/**
	 * 拖动单个虚拟图形边框生成相邻图形间的对齐虚拟直线
	 * 
	 * @param adjacentFigure
	 *            JecnAdjacentFigure获取拖动图形时，指向附近图形的小线段，便于提示用户拖动图形与附近图形中心点对齐功能
	 * @param baseFigurePanel
	 *            拖动的目标图形
	 */
	public static void drawTempFigureLine(JecnBaseFigurePanel baseFigurePanel) {
		// 单选时清楚 图形自动对齐连接线
		JecnAdjacentFigure.removeTempLine();
		// 相邻图形生成线段标齐
		Rectangle rectangle = JecnDrawMainPanel.getMainPanel().getWorkflow().getJecnTempFigurePanel().getBounds();
		// 相邻图形生成线段标齐
		JecnAdjacentFigure.getRefFigure(baseFigurePanel, rectangle);
		JecnDrawMainPanel.getMainPanel().getWorkflow().getJecnTempFigurePanel().setLocation(rectangle.getLocation());
		// 拖动流程元素获取图形基础数据记录标准线生成的新位置点
		baseFigurePanel.getTempFigureBean().setLocalX(rectangle.getLocation().x);
		baseFigurePanel.getTempFigureBean().setLocalY(rectangle.getLocation().y);
	}

	public void isCoverFlowSelect(JecnBaseFlowElementPanel curFlowElement, MouseEvent e) {
		if (JecnDrawMainPanel.getMainPanel().getWorkflow() == null || e == null || curFlowElement == null) {
			return;
		}
		// 获取当前画图面板
		JecnDrawDesktopPane desktopPane = JecnDrawMainPanel.getMainPanel().getWorkflow();
		// 判断图形是否存在相交的图形（包含也算相交）
		Set<JecnBaseFlowElementPanel> coverFlowList = desktopPane.getGraphicsLevel().figureOverlapList(curFlowElement);
		// 如果存在相交图形
		if (coverFlowList != null && coverFlowList.size() > 0) {
			// 获取图形相对面板的点
			Point tempPoint = SwingUtilities.convertPoint(curFlowElement, e.getPoint(), desktopPane);
			List<JecnBaseFlowElementPanel> temList = new ArrayList<JecnBaseFlowElementPanel>();
			// 判断鼠标点击图形范围是否存在相交图形
			for (JecnBaseFlowElementPanel flowElementPanel : coverFlowList) {
				if (flowElementPanel instanceof JecnBaseFigurePanel) {
					if ((tempPoint.x > flowElementPanel.getLocation().x && tempPoint.x < flowElementPanel.getLocation().x
							+ flowElementPanel.getWidth())
							&& (tempPoint.y > flowElementPanel.getLocation().y && tempPoint.y < flowElementPanel
									.getLocation().y
									+ flowElementPanel.getHeight())) {// point点在当前图形上
						temList.add(flowElementPanel);
					}
				}
			}
			// 获取层级最接近order的图形并且该图形的层级比order小
			JecnBaseFlowElementPanel tempCurElement = maxZorderFlowElement(temList, curFlowElement.getFlowElementData()
					.getZOrderIndex());
			// isMousePressedLine
			if (tempCurElement == null) {
				curFlowElement = minZorderFlowElement(temList, curFlowElement.getFlowElementData().getZOrderIndex());
			}
			if (tempCurElement != null) {
				JecnWorkflowUtil.hideAllResizeHandle();
				JecnWorkflowUtil.setCurrentSelect(tempCurElement);
			} else {
				JecnTabbedEventProcess.isMousePressedLine(tempPoint, e);
			}
		}
	}

	/**
	 * 获取层级最接近order的图形并且该图形的层级比order小
	 * 
	 * @param temList
	 * @param order
	 */
	public JecnBaseFlowElementPanel maxZorderFlowElement(List<JecnBaseFlowElementPanel> temList, int order) {
		if (temList == null) {
			return null;
		}
		// 记录当前循环图形的层级中距离当前点击图形层级最近的值
		int maxOrder = 0;
		JecnBaseFlowElementPanel maxElementPanel = null;
		for (JecnBaseFlowElementPanel flowElementPanel : temList) {
			int curOrder = flowElementPanel.getFlowElementData().getZOrderIndex();
			if (maxOrder < order && maxOrder <= curOrder) {
				maxOrder = curOrder;
				maxElementPanel = flowElementPanel;
			}
		}
		return maxElementPanel;
	}

	/**
	 * 获取层级最接近当order的最小值，当前图形层级大于order
	 * 
	 * @param temList
	 * @param order
	 * @return
	 */
	public JecnBaseFlowElementPanel minZorderFlowElement(List<JecnBaseFlowElementPanel> temList, int order) {
		if (temList == null) {
			return null;
		}
		// 记录当前循环图形的层级中距离当前点击图形层级最近的值
		int minOrder = 0;
		JecnBaseFlowElementPanel minElementPanel = null;
		for (JecnBaseFlowElementPanel flowElementPanel : temList) {
			int curOrder = flowElementPanel.getFlowElementData().getZOrderIndex();
			if (minOrder == 0) {
				minOrder = curOrder;
			}
			if (minOrder > order && minOrder <= curOrder) {
				minOrder = curOrder;
				minElementPanel = flowElementPanel;
			}
		}
		return minElementPanel;
	}

	/**
	 * 鼠标点击的是图形
	 * 
	 * @return boolean True:为图形（鼠标不是指针状态，也不是连接线或分割线状态）；false：流程元素库不是图形
	 */
	public boolean mousePressedFigure(JecnBaseFlowElementPanel souseFigure, MouseEvent e) {
		if (JecnDrawMainPanel.getMainPanel().getWorkflow() == null) {// 画图面板不存在
			return false;
		}
		// 流程元素面板
		JecnToolBoxMainPanel boxPanel = JecnDrawMainPanel.getMainPanel().getBoxPanel();
		// 判断元素是否为图形 boolean True:图形；false：不是图形
		boolean isFigure = JecnPaintFigureUnit.getFigurePanel(boxPanel.getSelectedBtnType());

		if (isFigure && e.getButton() == MouseEvent.BUTTON3) {// 右键取消条件
			// 添加完成后，选中指针
			JecnDrawMainPanel.getMainPanel().getBoxPanel().initGuide();
			return true;
		}
		if (!isFigure) {// 不是图形返回
			return false;
		}

		// ******************以下功能实现是：点击图形添加新图形功能******************//

		JecnTabbedEventProcess.addFlowElement(e.getPoint());
		// 添加完成后，选中指针
		JecnDrawMainPanel.getMainPanel().getBoxPanel().initGuide();

		return true;
	}

	/**
	 * 
	 * CTRL+左键实现：拖动复制功能
	 * 
	 */
	public static void mouseReleaseCopyProcess(Point point, List<JecnBaseFlowElementPanel> selectList) {
		JecnDrawDesktopPane workflow = JecnDrawMainPanel.getMainPanel().getWorkflow();
		if (workflow == null || point == null || workflow.getJecnTempFigurePanel() == null) {
			return;
		}

		// 选中流程元素集合添加到复制集合中
		workflow.addCurCopyList(selectList);
		// 复制粘贴 选中图形区域X坐标最大值
		workflow.getAidCompCollection().setLeftMinX(0);
		workflow.getAidCompCollection().setLeftMinY(0);

		// 计算粘贴待处理时的图形基准坐标:拖动间距
		int tempX = workflow.getJecnTempFigurePanel().getLocation().x - selectXY.getMinX();

		int tempY = workflow.getJecnTempFigurePanel().getLocation().y - selectXY.getMinY();

		// 复制的位置点
		workflow.setStartPoint(new Point(tempX, tempY));

		// 执行粘贴功能
		JecnFlowElementCopyAndPaste.flowElementPaste();

		// 清空所有虚拟图形
		workflow.removeTempListAll();

		JecnWorkflowUtil.hideAllResizeHandle();
		if (workflow.getCurCopyList() != null) {
			for (JecnBaseFlowElementPanel baseFlowElement : workflow.getCurCopyList()) {
				// 设置新添加的图形为选中状态
				JecnWorkflowUtil.setCurrentSelect(baseFlowElement);
			}
		}

		// 点击的是Ctrl清空面板选中元素集合
		JecnWorkflowUtil.hideAllResizeHandle();

		workflow.repaint();

		workflow.getCurCopyList().clear();
		workflow.setStartPoint(null);

	}

	/**
	 * 拖动图形鼠标释放后画图处理
	 * 
	 */
	public static void mouseReleaseDrawPanel(List<JecnBaseFlowElementPanel> listSelectAll) {

		JecnDrawDesktopPane desktopPane = JecnDrawMainPanel.getMainPanel().getWorkflow();
		if (desktopPane == null) {
			return;
		}

		if (listSelectAll.size() == 0) {
			return;
		}
		// 判断是否只拖动的连线（流程架构-连线不包含元素）
		boolean isDraggedManLine = isOnlyDraggedManLine(listSelectAll);
		if (isDraggedManLine) {
			JecnBaseFlowElementPanel elementPanel = listSelectAll.get(0);
			if (elementPanel instanceof JecnBaseManhattanLinePanel) {
				listSelectAll = JecnKeyHandler.addManLineFigure(new ArrayList<JecnBaseFigurePanel>(), elementPanel);
				listSelectAll.add(elementPanel);
			}
		}
		// 操作前数据
		JecnUndoRedoData undoData = new JecnUndoRedoData();
		// 操作后数据
		JecnUndoRedoData redoData = new JecnUndoRedoData();

		// 集合处理
		for (JecnBaseFlowElementPanel curFigurePanel : listSelectAll) {
			if (curFigurePanel instanceof JecnBaseFigurePanel) {
				// 如果是注释框
				if (curFigurePanel instanceof CommentText) {
					CommentText commentText = (CommentText) curFigurePanel;
					// 小线段位移算法
					commentText.isMove();
				}
				// 记录操作前数据
				undoData.recodeFlowElement(curFigurePanel);

				// 获取图形元素对象
				JecnBaseFigurePanel curFigure = (JecnBaseFigurePanel) curFigurePanel;
				// 拖动图形重设图形位置点、大小
				curFigure.moveFigureResetLocation();

				// 重置显示点
				JecnPaintFigureUnit.showResizeHandles(curFigure);

				// 记录操作后数据
				redoData.recodeFlowElement(curFigurePanel);

			} else if (curFigurePanel instanceof JecnBaseDividingLinePanel) {
				JecnBaseDividingLinePanel linePanel = (JecnBaseDividingLinePanel) curFigurePanel;

				// 记录操作前数据
				undoData.recodeFlowElement(linePanel);

				// 拖动分割线重置分割线开始点和结束点，记录分割线原始大小
				linePanel.moveLineResetPoint();

				// 记录操作后数据
				redoData.recodeFlowElement(linePanel);
			}
		}
		// 清空所有虚拟图形
		desktopPane.removeTempListAll();
		// 清空虚拟图形
		desktopPane.updateUI();

		// *************************撤销恢复*************************//
		JecnUnRedoProcess unredoProcess = JecnUnRedoProcessFactory.createEdit(undoData, redoData);
		desktopPane.getUndoRedoManager().addEdit(unredoProcess);
		// *************************撤销恢复*************************//
	}

	/**
	 * 设置拖动图形虚拟边框大小
	 * 
	 */
	public static void reSetJecnTempFigurePanel(List<JecnBaseFlowElementPanel> selectList) {
		if (JecnDrawMainPanel.getMainPanel().getWorkflow() == null) {
			return;
		}
		JecnDrawDesktopPane drawDesktopPane = JecnDrawMainPanel.getMainPanel().getWorkflow();

		JecnTempFigurePanel tempFigurePanel = null;

		if (drawDesktopPane.getJecnTempFigurePanel() == null) {
			tempFigurePanel = new JecnTempFigurePanel();

			drawDesktopPane.setJecnTempFigurePanel(tempFigurePanel);
		} else {
			tempFigurePanel = drawDesktopPane.getJecnTempFigurePanel();
		}

		// 获取选中区域大小重新设置选中区域位置，大小
		selectXY = JecnWorkflowUtil.getMaxSize(drawDesktopPane, selectList);
		if (selectList.size() == 1) {// 如果是单选图形
			JecnBaseFlowElementPanel elementPanel = selectList.get(0);

			// 单个图形虚拟边框无缩进值
			selectXY.setMinX(selectXY.getMinX() + 10);
			selectXY.setMinY(selectXY.getMinY() + 10);

			if (elementPanel instanceof DividingLine) {// 如果是分割线（斜线）位置点默认为线的最小坐标值，斜线所在区域panel的左上角
				tempFigurePanel.setBounds(new Point(selectXY.getMinX(), selectXY.getMinY()), elementPanel.getWidth(),
						elementPanel.getHeight());

			} else {
				tempFigurePanel
						.setBounds(elementPanel.getLocation(), elementPanel.getWidth(), elementPanel.getHeight());
			}
			if (elementPanel instanceof JecnBaseManhattanLinePanel) {
				oldTempFigurePoint = getManLineMinPoint((JecnBaseManhattanLinePanel) elementPanel);
			} else {
				// 记录拖动前虚拟图形位置点
				oldTempFigurePoint = tempFigurePanel.getLocation();
			}
		} else {// 多选图形
			// 计算拖动生成的虚拟图形长和宽
			int userWidth = selectXY.getMaxX() - selectXY.getMinX();
			int userHeight = selectXY.getMaxY() - selectXY.getMinY();
			tempFigurePanel.setBounds(new Point(selectXY.getMinX(), selectXY.getMinY()), userWidth, userHeight);// 记录拖动前虚拟图形位置点
			oldTempFigurePoint = tempFigurePanel.getLocation();
		}

		// 添加虚拟图形到面板
		drawDesktopPane.add(tempFigurePanel);
		// 设置虚拟图形相对面板的层级
		drawDesktopPane.setLayer(tempFigurePanel, drawDesktopPane.getMaxZOrderIndex() + 1);
	}

	private static Point getManLineMinPoint(JecnBaseManhattanLinePanel linePanel) {
		if (linePanel.getStartFigure() == null || linePanel.getStartFigure() == null) {
			return new Point(0, 0);
		}
		Point tmpStartP = linePanel.getStartFigure().getTempFigureBean().getLocalPoint();
		Point tmpEndP = linePanel.getEndFigure().getTempFigureBean().getLocalPoint();
		if (tmpStartP.x == 0 && tmpStartP.y == 0 && tmpEndP.x == 0 && tmpEndP.y == 0) {
			// 首次初始化
			tmpStartP = linePanel.getStartFigure().getLocation();
			tmpEndP = linePanel.getEndFigure().getLocation();
		}

		Point minP = new Point();
		if (tmpStartP.x >= tmpEndP.x) {
			minP.x = tmpEndP.x;
		} else {
			minP.x = tmpStartP.x;
		}
		if (tmpStartP.y >= tmpEndP.y) {
			minP.y = tmpEndP.y;
		} else {
			minP.y = tmpStartP.y;
		}
		return minP;
	}

	/**
	 * 根据鼠标拖动的位移获取图形新的坐标点
	 * 
	 * @param point
	 *            Point图形坐标点（图形元素为位置点，分割线为开始点或结束点）
	 * @param tempPoint
	 *            Point根据网格获取的拖动位移坐标
	 * @return
	 */
	public static Point getTempPoint(Point point, Point tempPoint) {
		return new Point(point.x + tempPoint.x, point.y + tempPoint.y);
	}

	/**
	 * 点击图形鼠标进入事件
	 * 
	 * @param e
	 */
	public void flowFigureEnter(MouseEvent e) {
		// 当前画图面板
		JecnDrawDesktopPane workflow = JecnDrawMainPanel.getMainPanel().getWorkflow();
		if (workflow == null) {
			return;
		}

		// 做添加流程元素时不显示快捷添加图形功能
		MapElemType selectedBtnType = JecnDrawMainPanel.getMainPanel().getBoxPanel().getSelectedBtnType();
		if (MapElemType.guide != selectedBtnType) {
			return;
		}

		if (MapType.partMap.equals(workflow.getFlowMapData().getMapType())) {// 流程图:快捷添加图形
			// 快捷添加图形
			JecnLinkFigureMainProcess panel = workflow.getAidCompCollection().getLinkFigureMainProcess();
			if (e.getSource() instanceof JecnBaseFigurePanel) {// 图形
				JecnBaseFigurePanel figure = (JecnBaseFigurePanel) e.getSource();
				if (JecnWorkflowUtil.isCtrl(e)) {
					return;
				}
				if (figure.getFlowElementData().getSortAddFigure()) {// 否支持快速添加图形：isSortAddFigur
					panel.showSmallTriangles((JecnBaseFigurePanel) e.getSource());
				}
			}
		}
	}

	/**
	 * 点击图形鼠标退出事件
	 * 
	 * @param e
	 */
	public void flowFigureExit(MouseEvent e) {

		if (MapType.partMap.equals(JecnDrawMainPanel.getMainPanel().getWorkflow().getFlowMapData().getMapType())) {// 流程图:快捷添加图形
			if (e.getSource() instanceof JecnBaseFigurePanel) {// 图形
				// 快捷添加图形
				JecnLinkFigureMainProcess panel1 = JecnDrawMainPanel.getMainPanel().getWorkflow()
						.getAidCompCollection().getLinkFigureMainProcess();
				panel1.hideFigureLinkFigures();
			}
		}
	}

	/**
	 * 
	 * 执行菜单操作
	 * 
	 * @param e
	 *            MouseEvent 鼠标动作
	 * @return boolean 执行菜单操作返回true ，否则返回false
	 */
	public static boolean rightPopupMenuProcess(MouseEvent e) {
		if (e.getButton() == MouseEvent.BUTTON3 && e.isPopupTrigger()) {// 流程元素右键操作且此鼠标事件是否为该平台的弹出菜单触发事件
			JecnBaseFlowElementPanel flowElementPanel = null;
			if (e.getSource() instanceof JecnBaseFlowElementPanel) {
				flowElementPanel = (JecnBaseFlowElementPanel) e.getSource();
			} else if (e.getSource() instanceof LineSegment) {
				LineSegment lineSegment = (LineSegment) e.getSource();
				flowElementPanel = lineSegment.getBaseManhattanLine();
				// 事件传递 (图形鼠标点击事件转换为面板鼠标点击事件)
				e = SwingUtilities.convertMouseEvent(lineSegment, e, JecnDrawMainPanel.getMainPanel().getWorkflow());
			}
			// 获取面板
			JecnDrawDesktopPane desktopPane = JecnDrawMainPanel.getMainPanel().getWorkflow();

			Point tempPoint = SwingUtilities.convertPoint(flowElementPanel, e.getPoint(), desktopPane);
			// 记录右键菜单点击图形相关点
			JecnFlowElementCopyAndPaste.setEditPastePoint(tempPoint);

			// boolean true:点击Ctrl键，false：点击的不是Ctrl键
			boolean isCtrl = JecnWorkflowUtil.isPressCtril(e);
			// 选中图形是否包含当前鼠标点击图形 不包含添加
			JecnWorkflowUtil.clearListAndAddElement(flowElementPanel, isCtrl);

			// 画图面板类型
			MapType mapType = desktopPane.getFlowMapData().getMapType();
			JecnFlowElemPopupMenu flowElemPopupMenu = null;
			switch (mapType) {
			case partMap:// 流程图
				flowElemPopupMenu = JecnDrawMainPanel.getMainPanel().getPartMapFlowElemPopupMenu();
				break;
			case totalMap:// 流程地图
			case totalMapRelation:// 集成关系图
				flowElemPopupMenu = JecnDrawMainPanel.getMainPanel().getTotalMapFlowElemPopupMenu();
				break;
			case orgMap:// 组织图
				flowElemPopupMenu = JecnDrawMainPanel.getMainPanel().getOrgMapFlowElemPopupMenu();
				break;
			default:
				return true;
			}

			if (desktopPane.getCurrentSelectElement().size() == 1) {
				// 修改工具栏的状态
				JecnElementAttributesUtil.updateEditTool(flowElementPanel);

				// 设计器使用：菜单项隐藏/显示实现方法
				JecnDesignerProcess.getDesignerProcess().beforeFlowElemPopupShow(flowElementPanel);
				// 显示弹出菜单
				flowElemPopupMenu.getFlowElemPopupMenu().show(flowElementPanel, e.getX(), e.getY());
			} else {
				// 当前点击流程元素传递到弹出菜单对象中
				flowElemPopupMenu.setElementPanel(flowElementPanel);

				// 设计器使用：菜单项隐藏/显示实现方法
				JecnDesignerProcess.getDesignerProcess().beforeFlowElemMutilsPopupShow();
				// 显示弹出菜单
				flowElemPopupMenu.getFlowElemPopupMenu().show(flowElementPanel, e.getX(), e.getY());
			}
			return true;
		}
		return false;
	}

	/**
	 * 
	 * 点击流程元素点击事件处理
	 * 
	 * @param e
	 *            MouseEvent 鼠标事件
	 * @return boolean true:执行双击编辑动作 false:没有执行双击编辑
	 */
	private boolean flowFigureDoubleClicked(MouseEvent e) {
		// 获取面板
		JecnDrawDesktopPane workflow = JecnDrawMainPanel.getMainPanel().getWorkflow();

		// 双击编辑功能
		if (e.getClickCount() == 2 && e.getButton() == MouseEvent.BUTTON1) { // 左键点击且流程元素点击次数为2操作

			// 清除快捷添加图形显示小三角形
			JecnDrawMainPanel.getMainPanel().interruptLinkFigure();
			// 双击编辑清空选中状态
			JecnWorkflowUtil.hideAllResizeHandle();
			if (e.getSource() instanceof JecnBaseFigurePanel) {
				JecnBaseFigurePanel figurePanel = (JecnBaseFigurePanel) e.getSource();
				if (!figurePanel.getFlowElementData().getDoubleClick()) {// 图形是否允许双击编辑
					return true;
				}
				// 图形
				workflow.getAidCompCollection().addTextAreaRefFlowElemPanel(figurePanel);
			} else if (e.getSource() instanceof LineSegment) {
				// 小线段
				LineSegment lineSegment = (LineSegment) e.getSource();
				lineSegment.getBaseManhattanLine().doubleClick();
			} else {
				return false;
			}
			return true;
		}

		return false;
	}

	/**
	 * 添加虚拟分隔符线段
	 * 
	 * @param tempPoint
	 *            根据网格获取鼠标坐标点 拖动鼠标位移按照网格取值 Point tempPoint
	 * 
	 * @param curDiviLine
	 *            JecnBaseDividingLinePanel拖动的当前分隔符线段
	 */
	public static void addTempDiviLine(JecnBaseDividingLinePanel curDiviLine, Point tempPoint) {
		if (JecnDrawMainPanel.getMainPanel().getWorkflow() == null || curDiviLine == null || tempPoint == null) {
			return;
		}
		JecnDrawDesktopPane workflow = JecnDrawMainPanel.getMainPanel().getWorkflow();
		// 获取分割线虚拟图形对象
		TempDividingLine tempDividing = curDiviLine.createTempLinePanel();
		tempDividing.setStartPoint(curDiviLine.getStartPoint());
		tempDividing.setEndPoint(curDiviLine.getEndPoint());
		// 拖动分割线端点到达边界处理
		tempDividing.draggedTempLinePoint(tempPoint);
		if (workflow.getTempDividingList().contains(tempDividing)) {
			return;
		}
		workflow.getTempDividingList().add(tempDividing);
	}
}
