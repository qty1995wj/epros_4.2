package com.jecn.epros.server.download.wordxml.zhuhaicusuanxianwei;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.regex.Pattern;

import org.apache.commons.lang.StringUtils;

import wordxml.constant.Constants;
import wordxml.constant.Constants.Align;
import wordxml.constant.Constants.DocGridType;
import wordxml.constant.Constants.HeaderOrFooterType;
import wordxml.constant.Constants.LineRuleType;
import wordxml.constant.Constants.LineStyle;
import wordxml.constant.Constants.PStyle;
import wordxml.element.bean.common.PositionBean;
import wordxml.element.bean.page.DocGrid;
import wordxml.element.bean.page.SectBean;
import wordxml.element.bean.paragraph.FontBean;
import wordxml.element.bean.paragraph.ParagraphBean;
import wordxml.element.bean.paragraph.ParagraphLineRule;
import wordxml.element.bean.paragraph.ParagraphRowIndBean;
import wordxml.element.bean.paragraph.ParagraphSpaceBean;
import wordxml.element.bean.paragraph.GroupBean.GroupAlign;
import wordxml.element.bean.paragraph.GroupBean.GroupH;
import wordxml.element.bean.paragraph.GroupBean.GroupW;
import wordxml.element.bean.paragraph.GroupBean.Hanchor;
import wordxml.element.bean.paragraph.GroupBean.Vanchor;
import wordxml.element.bean.table.TableBean;
import wordxml.element.dom.graph.Image;
import wordxml.element.dom.graph.LineGraph;
import wordxml.element.dom.page.Footer;
import wordxml.element.dom.page.Header;
import wordxml.element.dom.page.Sect;
import wordxml.element.dom.paragraph.Group;
import wordxml.element.dom.paragraph.Paragraph;
import wordxml.element.dom.table.Table;

import com.jecn.epros.server.bean.download.ProcessDownloadBean;
import com.jecn.epros.server.common.JecnContants;
import com.jecn.epros.server.download.wordxml.ProcessFileItem;
import com.jecn.epros.server.download.wordxml.ProcessFileModel;

/**
 * 珠海醋酸纤维
 * 
 * @author admin
 * 
 */
public class ZHCSProcessModel extends ProcessFileModel {
	/*** 段落行距单倍倍行距 *****/
	private final ParagraphLineRule vLine1 = new ParagraphLineRule(1F,
			LineRuleType.AUTO);
	private int tableAccumulator=1;
	private int imageAccumulator=1;
	private final ParagraphBean tablePbean=new ParagraphBean(Align.center, "黑体", Constants.wuhao, false);
	private final ParagraphSpaceBean tableSbean=new ParagraphSpaceBean(0.5f,0.5f);

	public ZHCSProcessModel(ProcessDownloadBean processDownloadBean, String path) {
		super(processDownloadBean, path, true);
		super.setDefAscii("黑体");
		setDocStyle("", textTitleStyle());
		/***** 段落空格换行属性bean 宋体 小四 最小值16磅值 *******/
		docProperty.setNewTblWidth(17.49F);
		docProperty.setFlowChartScaleValue(7f);
	}

	/**
	 * 获取默认的节属性
	 * 
	 * @return
	 */
	private SectBean getSectBean() {
		SectBean sectBean = new SectBean();
		sectBean.setDocGrid(new DocGrid(DocGridType.LINES, 16.3F));
		// 设置页边距
		sectBean.setPage(1F, 2.5F, 2F, 2F, 2.5F, 2F);
		sectBean.setSize(21, 29.7F);
		return sectBean;
	}

	/*****
	 * 创建表格项
	 * 
	 * @param contentSect
	 * @param fs
	 * @param list
	 * @return
	 */
	@Override
	protected Table createTableItem(ProcessFileItem processFileItem,
			float[] fs, List<String[]> list) {
		Sect sect = processFileItem.getBelongTo();
		tableSbean.setLineRule(vLine1);
		tablePbean.setParagraphSpaceBean(tableSbean);
		sect.createParagraph("表" + tableAccumulator, tablePbean);
		tableAccumulator++;
		Table tab = super.createTableItem(processFileItem, fs, list);
		return tab;
	}

	/**
	 * 创建文本项
	 * 
	 * @param titleName
	 * @param contentSect
	 * @param content
	 */
	@Override
	protected void createTextItem(ProcessFileItem processFileItem,
			String... content) {
		super.createTextItem(processFileItem, content);
	}

	/**
	 * 添加封面节点数据
	 */
	private void addTitleSectContent() {

		Sect titleSect = docProperty.getTitleSect();
		SectBean sectBean = new SectBean();
		sectBean.setDocGrid(new DocGrid(DocGridType.LINES, 10.5F));
		// 设置页边距
		sectBean.setPage(1F, 2.5F, 2F, 1.5F, 0F, 0F);
		sectBean.setSize(21, 29.7F);
		titleSect.setSectBean(sectBean);

		// zcfc
		createZCFC(titleSect);
		// 公司名称
		createTitleComponyName(titleSect);
		// Q/ZCFC J062.013—2015// 代替 Q/ZCFC J062.013-2012
		standardVersion(titleSect);
		// 一条线
		createTopLine(titleSect);
		// 流程名称
		createProcessName(titleSect);
		// 发布行
		createPubLine(titleSect);
		// 一条线
		createBottomLine(titleSect);
		// 珠海醋酸纤维有限公司   发布
		createComponyPub(titleSect);
	}

	private void createComponyPub(Sect titleSect) {
		Group group = titleSect.createGroup();
		group.getGroupBean()
		.setSize(GroupW.exact, 14, GroupH.exact, 2)
		.setHorizontal(3.79, Hanchor.page, 0.22)
		.setVertical(27, Vanchor.page, 0.32)
		.lock(true);
		
		ParagraphBean pBean = new ParagraphBean(Align.center);
		Paragraph paragraph = new Paragraph(false);
		paragraph.setParagraphBean(pBean);
		FontBean fontBean = new FontBean("黑体", Constants.sanhao, false);
		paragraph.appendTextRun("珠海醋酸纤维有限公司", fontBean);
		fontBean = new FontBean("黑体", Constants.sihao, false);
		paragraph.appendTextRun("    发 布", fontBean);
		group.addParagraph(paragraph);
	}

	private void createPubLine(Sect titleSect) {
		
		String pubDateStr = nullToEmpty(processDownloadBean.getReleaseDate());
		if (pubDateStr.length() > 10) {
			pubDateStr = pubDateStr.substring(0, 10);
		}
		// 2016 - 05 - 01实施 黑体 14
		String nextMonthFirstDay = "";
		if (StringUtils.isNotBlank(pubDateStr)) {

			try {
				// 下一月第一天
				SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
				Date pubDate = format.parse(pubDateStr);
				Calendar cal = Calendar.getInstance();
				cal.setTime(pubDate);
				cal.add(Calendar.MONTH, 1);
				cal.set(Calendar.DAY_OF_MONTH, 1);
				nextMonthFirstDay = format.format(cal.getTime());

			} catch (ParseException e) {
				log.error(e);
			}
		}
		
		// 2016 - 04 - 11发布 黑体 四号
		ParagraphBean pubBean = new ParagraphBean(Align.left, "黑体",
				Constants.sihao, false);
		Group pubGroup = titleSect.createGroup();
		pubGroup.getGroupBean()
		.setSize(GroupW.exact, 7.05, GroupH.exact, 0.83)
		.setHorizontal(2.5, Hanchor.page, 0)
		.setVertical(25.04, Vanchor.page, 0.32)
		.lock(true);
		pubGroup.addParagraph(new Paragraph(pubDateStr+"发布",pubBean));
		
		// 2016 - 04 - 11实施 黑体 四号
		ParagraphBean nextBean = new ParagraphBean(Align.right, "黑体",
				Constants.sihao, false);
		Group nextGroup = titleSect.createGroup();
		nextGroup.getGroupBean()
		.setSize(GroupW.exact, 7.05, GroupH.exact, 0.83)
		.setHorizontal(12.5, Hanchor.page, 0)
		.setVertical(25.04, Vanchor.page, 0.32)
		.lock(true);
		nextGroup.addParagraph(new Paragraph(nextMonthFirstDay+"实施",nextBean));
	}

	private void createProcessName(Sect titleSect) {
		String flowName = nullToEmpty(processDownloadBean.getFlowName());
		// 黑体 26
		ParagraphBean pBean = new ParagraphBean(Align.center, "黑体",
				Constants.yihao, false);
		ParagraphLineRule vLine1 = new ParagraphLineRule(34F,
				LineRuleType.EXACT);
		pBean.setSpace(0f, 0f, vLine1);
		Paragraph paragraph=new Paragraph(flowName, pBean);
		
		Group group = titleSect.createGroup();
		group.getGroupBean()
		.setSize(GroupW.exact, 17, GroupH.exact, 12.2)
		.setHorizontal(2.0, Hanchor.page, 0)
		.setVertical(11.27, Vanchor.page, 0)
		.lock(true);
		group.addParagraph(paragraph);
	}

	private void createTopLine(Sect titleSect) {
		createLine(titleSect,0.95F, 175F,483F, 175F);
	}
	
	private void createBottomLine(Sect titleSect) {
		createLine(titleSect,0.95F, 678.15F,483F, 678.15F);
	}
	
	private void createLine(Sect titleSect,float startX,float startY,float endX,float endY){
		LineGraph line = new LineGraph(LineStyle.single, 0.75F);
		line.setFrom(startX,startY);
		line.setTo(endX,endY);
		PositionBean positionBean = new PositionBean();
		positionBean.setMarginLeft(0);
		positionBean.setMarginRight(0);
		line.setPositionBean(positionBean);
		titleSect.createParagraph(line);
	}

	private void standardVersion(Sect titleSect) {
		ParagraphBean pBean = new ParagraphBean(Align.right, "Times New Roman",
				Constants.sihao, false);
		ParagraphLineRule vLine1 = new ParagraphLineRule(14, LineRuleType.EXACT);
		pBean.setSpace(1f, 0f, vLine1);
		Paragraph vision = new Paragraph("", pBean);
		vision.appendTextRun("Q/ZCFC");
		vision.appendTextRun(" J062.013",new FontBean("黑体", Constants.sihao, false));
		vision.appendTextRun("—");
		vision.appendTextRun("2015",new FontBean("黑体", Constants.sihao, false));
		
		pBean = new ParagraphBean(Align.right, "宋体",
				Constants.wuhao, false);
		vLine1 = new ParagraphLineRule(14F,
				LineRuleType.EXACT);
		pBean.setSpace(0f, 0f, vLine1);
		Paragraph replaceVision = new Paragraph("代替 Q/ZCFC J062.013-2012", pBean);
		
		Group group = titleSect.createGroup();
		group.getGroupBean()
		.setSize(GroupW.exact, 16.12, GroupH.exact, 2.19)
		.setHorizontal(2.9, Hanchor.page, 0.5)
		.setVertical(5.13, Vanchor.page, 0)
		.lock(true);
		group.addParagraph(vision);
		group.addParagraph(replaceVision);

		
	}

	private void createTitleComponyName(Sect titleSect) {

		/*** 最小值 分散对齐 *****/
		ParagraphLineRule vLine = new ParagraphLineRule(0F,
				LineRuleType.AT_LEAST);
		// 宋体 一号 居中对齐 单倍行距
		ParagraphBean pBean = new ParagraphBean(Align.distribute, "黑体",
				Constants.xiaoyi, false);
		pBean.setSpace(0f, 0f, vLine);
		
		Group group = titleSect.createGroup();
		group.createParagraph("珠海醋酸纤维有限公司企业标准", pBean);
		group.getGroupBean()
		.setSize(GroupW.exact, 17, GroupH.exact, 1.1)
		.setHorizontal(2.5, Hanchor.page, 0.32)
		.setVertical(4.03, Vanchor.page, 0.32)
		.lock(true);

	}

	private void createZCFC(Sect titleSect) {
		
		Group group = titleSect.createGroup();
		group.getGroupBean()
		.setSize(GroupW.exact, 10.76, GroupH.exact, 2.45)
		.setHorizontal(8.24, Hanchor.page, 0.32)
		.setVertical(1.16, Vanchor.page, 0.32)
		.lock(true);
		
		// Q/ZCFC 罗马 48号 加粗
		ParagraphBean pBean = new ParagraphBean(Align.right, "Times New Roman", Constants.sishiba, true);
		Paragraph zcfcParagraph = new Paragraph("Q/ZCFC", pBean);
		titleSect.addParagraph(zcfcParagraph);
		group.addParagraph(zcfcParagraph);
	}

	private void addQianYanSectContent(Sect firstSect) {

		ParagraphBean pBean = new ParagraphBean(Align.center, "黑体",
				Constants.sanhao, false);
		ParagraphLineRule vLine = new ParagraphLineRule(1F, LineRuleType.AUTO);		
		pBean.setSpace(32f, 28f, 1, vLine);
		firstSect.createParagraph("前    言", pBean);

//		ProcessAttributeBean responFlow = processDownloadBean.getResponFlow();
//		String dutyOrg = nullToEmpty(responFlow.getDutyOrgName());
//		String createPeople = nullToEmpty(responFlow.getFictionPeopleName());
//		String modifyExplain = "";
//		JecnTaskHistoryNew lastHistoryNew = processDownloadBean
//				.getLatestHistoryNew();
//		if (lastHistoryNew != null) {
//			modifyExplain = nullToEmpty(lastHistoryNew.getModifyExplain());
//		}

		List<String> contents = new ArrayList<String>();
		contents.add("本流程按照GB/T 1.1-2009给出的规则起草。");
		contents.add("本标准由珠海醋酸纤维有限公司管理标准化工作组提出并归口。");
		contents.add("本标准起草单位：");
		contents.add("本标准主要起草人：");
		contents.add("本标准所代替标准历次版本的发布情况为：");
		contents.add("——Q/ZCFC J062.013-2012");

		pBean = new ParagraphBean(Align.both, "宋体", Constants.wuhao, false);
		ParagraphRowIndBean rowInd=new ParagraphRowIndBean();
		rowInd.setFirstLine(0.75F);
		pBean.setParagraphRowIndBean(rowInd);
		vLine = new ParagraphLineRule(12F, LineRuleType.AT_LEAST);
		pBean.setSpace(0f, 0f, vLine);
		firstSect.createParagraph(contents.toArray(new String[6]), pBean);

		Paragraph wrap=new Paragraph(true);
		// 添加分页符号
		firstSect.addParagraph(wrap);
	}

	/**
	 * 添加第一节前面的数据
	 */
	private void addFirstSectContent() {
		// 流程名称
		String flowName = processDownloadBean.getFlowName();
		// 黑体 三号
		ParagraphBean pBean = new ParagraphBean(Align.center, "黑体",
				Constants.sanhao, false);
		ParagraphLineRule line = new ParagraphLineRule(23F, LineRuleType.EXACT);
		pBean.setSpace(15f, 28f, 1, line);
		getDocProperty().getFirstSect().createParagraph(flowName, pBean);

	}

	/**
	 * 活动说明
	 * 
	 * @param _count
	 * @param name
	 * @param allActivityShowList
	 */
	@Override
	protected void a10(ProcessFileItem processFileItem, String[] titles,
			List<String[]> rowData) {

		// 段落显示 1:段落；0:表格（默认）
		if (processDownloadBean.getFlowFileType() == 1) {
			a10InParagraph(processFileItem, rowData);
			return;
		}
		// 修改内容项显示的顺序
		List<String[]> newRowData = new ArrayList<String[]>();
		String[] newRowArr;
		for (String[] arr : rowData) {
			newRowArr = new String[6];
			newRowArr[0] = arr[0];
			newRowArr[1] = arr[2];
			newRowArr[2] = arr[4];
			newRowArr[3] = arr[3];
			newRowArr[4] = arr[5];
			newRowArr[5] = arr[1];
			newRowData.add(newRowArr);
		}
		// 标题
		createTitle(processFileItem);
		titles = new String[] { "编号", "流程活动", "输入", "流程活动描述", "输出", "责任角色" };
		// 添加标题
		newRowData.add(0, titles);
		float[] width = new float[] { 1.12F, 1.8F, 2.4F, 6.8F, 2.4F, 1.8F };
		Table tbl = createTableItem(processFileItem, width, newRowData);
		// 设置第一行居中
		tbl.setRowStyle(0, docProperty.getTblTitleStyle(), docProperty
				.isTblTitleShadow(), docProperty.isTblTitleCrossPageBreaks());
	}

	@Override
	protected void initFirstSect(Sect firstSect) {
		// 添加前言节点数据
		firstSect.setSectBean(getSectBean());
		addQianYanSectContent(firstSect);
		addFirstSectContent();
		createCommhdr(firstSect);
		
		firstSect.getSectBean().setStartNum(1);
		createCommftr(firstSect, HeaderOrFooterType.odd);
	}

	@Override
	protected void initFlowChartSect(Sect flowChartSect) {
		SectBean sectBean = getSectBean();
		sectBean.setHorizontal(true);
		sectBean.setSize(29.7F, 21);
		flowChartSect.setSectBean(sectBean);
		createCommhdr(flowChartSect);
		createCommftr(flowChartSect,HeaderOrFooterType.odd);
	}

	@Override
	protected void initSecondSect(Sect secondSect) {
		secondSect.setSectBean(getSectBean());
		createCommhdr(secondSect);
		createCommftr(secondSect,HeaderOrFooterType.odd);
	}

	@Override
	protected void initTitleSect(Sect titleSect) {
		titleSect.setSectBean(getSectBean());
		// 添加封皮节点数据
		addTitleSectContent();
	}

	@Override
	public ParagraphBean tblContentStyle() {
		ParagraphBean tblContentStyle = new ParagraphBean("宋体",
				Constants.xiaowu, false);
		tblContentStyle.setSpace(0f, 0f, vLine1);
		return tblContentStyle;
	}

	@Override
	public TableBean tblStyle() {
		TableBean tblStyle = new TableBean();// 默认表格样式
		// 所有边框 0.5 磅
		tblStyle.setBorder(0.5F);
		tblStyle.setCellMargin(0.1F, 0.2F, 0, 0.2F);
		// 表格左缩进0.01厘米
		tblStyle.setTableLeftInd(0.01F);
		return tblStyle;
	}

	@Override
	public ParagraphBean tblTitleStyle() {
		ParagraphBean tblTitileStyle = new ParagraphBean(Align.center, "宋体",
				Constants.xiaowu, false);
		tblTitileStyle.setSpace(0f, 0f, vLine1);
		return tblTitileStyle;
	}

	@Override
	public ParagraphBean textContentStyle() {
		ParagraphBean textContentStyle = new ParagraphBean(Align.left, "宋体",
				Constants.wuhao, false);
		textContentStyle.setSpace(0.2f, 0.2f, vLine1);
		textContentStyle.setInd(0f, 0f, 0f, 0.8F);
		return textContentStyle;
	}

	@Override
	public ParagraphBean textTitleStyle() {
		ParagraphBean textTitleStyle = new ParagraphBean("黑体", Constants.wuhao,
				false);
		textTitleStyle.setSpace(1F, 1F, vLine1);
		textTitleStyle.setpStyle(PStyle.LVL1);
		return textTitleStyle;
	}

	/***
	 * 创建通用页眉
	 * 
	 * @param sect
	 */
	private void createCommhdr(Sect sect) {
		ParagraphBean pBean = new ParagraphBean(Align.right, "黑体",
				Constants.wuhao, false);
		pBean.setSpace(0f, 1f, vLine1);
		Header header = sect.createHeader(HeaderOrFooterType.odd);
		Paragraph p = header.createParagraph("",pBean);
		p.appendTextRun("Q/ZCFC J062.013");
		p.appendTextRun("—", new FontBean("Times New Roman",Constants.wuhao, false));
		p.appendTextRun("2015");
	}

	/**
	 * 角色职责
	 * 
	 * @param _count
	 * @param name
	 * @param roleList
	 */
	protected void a08(ProcessFileItem processFileItem, List<String[]> rowData) {
		// 标题
		createTitle(processFileItem);

		Sect sect = processFileItem.getBelongTo();
		ParagraphBean titleBean = new ParagraphBean("黑体", Constants.wuhao,
				false);
		titleBean.setSpace(0.5f, 0.5f, vLine1);
		titleBean.setInd(0, 0, 0, 0f);

		ParagraphBean contentBean = new ParagraphBean("宋体", Constants.wuhao,
				false);
		contentBean.setSpace(0.2f, 0.2f, vLine1);
		contentBean.setInd(0, 0, 0, 0.8f);
		int index = 1;
		for (String[] rowArr : rowData) {
			sect.createParagraph(numberMark + "." + index + " "
					+ nullToEmpty(rowArr[0]), titleBean);
			if (JecnContants.showPosNameBox) {
				sect.createParagraph(nullToEmpty(rowArr[1]), contentBean);
			}
			sect.createParagraph(nullToEmpty(rowArr[2]), contentBean);
			index++;
		}
	}

	@Override
	protected void a09(ProcessFileItem processFileItem, List<Image> images) {
		createTitle(processFileItem);
		ParagraphBean pBean = new ParagraphBean(Align.center);
		for (Image img : images) {
			resize(img);
			processFileItem.getBelongTo().createParagraph(img, pBean);
			processFileItem.getBelongTo().createParagraph("图"+imageAccumulator,tablePbean);
			imageAccumulator++;
		}
	}

	private void resize(Image img) {
		final float maxHeight = 16f;
		final float maxWidth = 26f;
		float width = img.getWidth() > maxWidth ? maxWidth : img.getWidth();
		float height = img.getHeight() > maxHeight ? maxHeight : img
				.getHeight();
		img.setSize(width, height);
	}

	/**
	 * 创建通用的页脚
	 * 
	 * @param sect
	 */
	private void createCommftr(Sect sect,HeaderOrFooterType type) {
		// 宋体 小四号 居中
		ParagraphBean pBean = new ParagraphBean(Align.right, "宋体",
				Constants.xiaowu, false);
		Footer ftr = sect.createFooter(type);
		Paragraph p = ftr.createParagraph("", pBean);
		p.appendCurPageRun();
	}
	
	/**
	 * 相关制度
	 * 
	 * @param _count
	 * @param name
	 * @param ruleNameList
	 */
	@Override
	protected void a14(ProcessFileItem processFileItem, List<String[]> rowData) {
		// 三列改为两列
		// 将其中的String[] 三列改为两列
		List<String[]> targetData=new ArrayList<String[]>();
		for(String[] arr:rowData){
			String[] elem=new String[2];
			elem[0]=arr[0];
			elem[1]=arr[1];
			targetData.add(elem);
		}
		super.a14(processFileItem, targetData);
	}
	
	@Override
	protected void afterHandle() {
		super.afterHandle();
		// 写出下划线
		Sect secondSect = docProperty.getSecondSect();
		secondSect.addParagraph(new Paragraph(""));
		Group group = secondSect.createGroup();
		group.getGroupBean()
		.setSize(GroupW.auto, 0, GroupH.auto, 0)
		.setHorizontal(GroupAlign.center, Hanchor.page, 0.32)
		.setVertical(0, Vanchor.text, 0.32);
		ParagraphBean pBean = new ParagraphBean(Align.center);
		Paragraph paragraph = new Paragraph(false);
		paragraph.setParagraphBean(pBean);
		FontBean fontBean = new FontBean("Times New Roman", Constants.wuhao, false);
		paragraph.appendTextRun("_________________________________", fontBean);
		group.addParagraph(paragraph);
		
	}
	
	@Override
	protected void a03(ProcessFileItem processFileItem, String... content) {
		createTitle(processFileItem);
		Sect sect = processFileItem.getBelongTo();
		ParagraphBean subTitleBean=new ParagraphBean("黑体", Constants.wuhao, false);
	    if(content.length>0){
//	    	3.1 标题1
//	    	内容1
//	    	3.2 标题2
//	    	内容2
	    	String regex="^\\s*\\d+\\.\\d+.*";
	    	for(String subText:content){
	    		if(Pattern.matches(regex, subText)){
	    			sect.createParagraph(subText, subTitleBean);
	    		}else{
	    			sect.createParagraph(subText, docProperty.getTextContentStyle());
	    		}
	    	}
	    }
	}
}
