package epros.designer.gui.process.kpi;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.beanutils.PropertyUtils;

import com.jecn.epros.server.bean.process.JecnFlowKpiNameT;
import com.jecn.epros.server.bean.system.JecnConfigItemBean;
import com.jecn.epros.server.common.JecnConfigContents.ConfigItemPartMapMark;

import epros.designer.util.ConnectionPool;

public enum KpiTitleAndValues {
	INSTANCE;

	public TmpKpiShowValues getConfigKpiNameTAndValues(Long flowId) throws Exception {
		if (flowId == null) {
			return new TmpKpiShowValues();
		}
		List<JecnFlowKpiNameT> kpiNameList = ConnectionPool.getProcessAction().getFlowKpiNameTList(flowId);
		List<JecnConfigItemBean> configItemBeans = ConnectionPool.getConfigAciton().selectConfigItemBeanByType(1, 16);
		TmpKpiShowValues kpiShowValues = new TmpKpiShowValues();
		// 标题名称
		List<JecnConfigItemBean> kpiTitles = new ArrayList<JecnConfigItemBean>();
		// 行数据
		List<List<String>> rowValues = new ArrayList<List<String>>();
		if (configItemBeans == null) {
			return null;
		}
		int count = 0;
		for (JecnConfigItemBean jecnConfigItemBean : configItemBeans) {
			if (jecnConfigItemBean.getIsTableData() == 0 || "0".equals(jecnConfigItemBean.getValue())) {
				continue;
			}
			kpiTitles.add(jecnConfigItemBean);
			if (count == 4) {
				break;
			}
			count++;
		}
		for (JecnFlowKpiNameT jecnFlowKpiNameT : kpiNameList) {
			KpiNameT kpiName = new KpiNameT();
			PropertyUtils.copyProperties(kpiName, jecnFlowKpiNameT);
			rowValues.add(getRowValues(kpiName, configItemBeans));
		}
		kpiShowValues.setKpiRowValues(rowValues);
		kpiShowValues.setKpiTitles(kpiTitles);
		kpiShowValues.setKpiNameList(kpiNameList);
		return kpiShowValues;
	}

	/**
	 * 根据配置获取KPI前几列数据
	 * 
	 * @param flowKpiName
	 * @param configItemBeans
	 * @return
	 */
	public List<String> getConfigKpiRowValues(KpiNameT flowKpiName, List<JecnConfigItemBean> configItemBeans) {
		List<String> rowValues = new ArrayList<String>();
		int count = 0;
		for (JecnConfigItemBean jecnConfigItemBean : configItemBeans) {
			if (jecnConfigItemBean.getIsTableData() == 0) {
				continue;
			}
			rowValues.add(getKpiCells(jecnConfigItemBean.getMark(), flowKpiName));
			if (count == 4) {
				return rowValues;
			}
			count++;
		}
		return rowValues;
	}

	/**
	 * KPI行数据
	 * 
	 * @param flowKpiName
	 * @param configItemBeans
	 * @return
	 */
	private List<String> getRowValues(KpiNameT flowKpiName, List<JecnConfigItemBean> configItemBeans) {
		List<String> rowValues = new ArrayList<String>();
		int count = 0;
		for (JecnConfigItemBean jecnConfigItemBean : configItemBeans) {
			if (jecnConfigItemBean.getIsTableData() == 0 || "0".equals(jecnConfigItemBean.getValue())) {
				continue;
			}
			rowValues.add(getKpiCells(jecnConfigItemBean.getMark(), flowKpiName));
			if (count == 4) {
				return rowValues;
			}
			count++;
		}
		return rowValues;
	}

	private String getKpiCells(String mark, KpiNameT flowKpiName) {
		if (ConfigItemPartMapMark.kpiName.toString().equals(mark)) {
			return flowKpiName.getKpiName();
		} else if (ConfigItemPartMapMark.kpiType.toString().equals(mark)) {
			return flowKpiName.getKpuStringType();
		} else if (ConfigItemPartMapMark.kpiUtilName.toString().equals(mark)) {
			return flowKpiName.getKpiStringVertical();// kpi单位名称
		} else if (ConfigItemPartMapMark.kpiTargetValue.toString().equals(mark)) {
			return flowKpiName.getKpiTarget();// kpi目标值
		} else if (ConfigItemPartMapMark.kpiTimeAndFrequency.toString().equals(mark)) {
			return flowKpiName.getKpiStringHorizontal();// KPI纵坐标 数据统计时间/频率 0：月
			// 1：季度
		} else if (ConfigItemPartMapMark.kpiDefined.toString().equals(mark)) {
			return flowKpiName.getKpiDefinition();// KPI定义
		} else if (ConfigItemPartMapMark.kpiDesignFormulas.toString().equals(mark)) {
			return flowKpiName.getKpiStatisticalMethods();// 流程计算公式
		} else if (ConfigItemPartMapMark.kpiDataProvider.toString().equals(mark)) {
			return flowKpiName.getKpiDataPeopleName();// 数据提供者
		} else if (ConfigItemPartMapMark.kpiDataAccess.toString().equals(mark)) {
			return flowKpiName.getKpiDataMethodString();
		} else if (ConfigItemPartMapMark.kpiIdSystem.toString().equals(mark)) {
			return flowKpiName.getKpiITSystemNames();// IT系统
		} else if (ConfigItemPartMapMark.kpiSourceIndex.toString().equals(mark)) {
			return flowKpiName.getKpiStringTargetType();// 指标来源
		} else if (ConfigItemPartMapMark.kpiRrelevancy.toString().equals(mark)) {
			return flowKpiName.getStringkpiRelevance();// 相关度
		} else if (ConfigItemPartMapMark.kpiSupportLevelIndicator.toString().equals(mark)) {
			return flowKpiName.getFirstTargetContent();// 支持的一级指标
		} else if (ConfigItemPartMapMark.kpiPurpose.toString().equals(mark)) {
			return flowKpiName.getKpiPurpose();// 设置目的
		} else if (ConfigItemPartMapMark.kpiPoint.toString().equals(mark)) {
			return flowKpiName.getKpiPoint();// 测量点
		} else if (ConfigItemPartMapMark.kpiPeriod.toString().equals(mark)) {
			return flowKpiName.getKpiPeriod();// 统计周期
		} else if (ConfigItemPartMapMark.kpiExplain.toString().equals(mark)) {
			return flowKpiName.getKpiExplain();// 说明
		}
		return "";
	}
}
