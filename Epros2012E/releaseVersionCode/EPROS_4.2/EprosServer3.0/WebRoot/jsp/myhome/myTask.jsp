<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>


<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<script type="text/javascript">
var basePath = "${basePath}";
$(function(){
	//在我的任务里没有完成这个阶段
	$("option[value='5']").remove();
});
</script>
	</head>
	<body>
		<div id="div" class="div" style="height: 100%;">
			<div id="search" class="divBorder" align="center">
				<div id="taskSearchText" style="overflow: hidden;" class="minWidth"
					align="center">
					<table width="100%">
						<tr>
							<td width="10%" height="25px" align="right">
								<!-- 任务名称： -->
								<s:text name="taskNameC"></s:text>
							</td>
							<td width="35%" align="left">
								<input id="taskName" type="text" style="width: 80%;"
									maxLength="32" />
							</td>

							<td width="10%" align="right">
								<!-- 创建人： -->
								<s:text name="createrC"></s:text>
							</td>
							<td width="35%" align="left">
								<input type="text" style="vertical-align: middle; width: 63%"
									id="createUserName_myTask"
									onkeyup="search(createUserName_myTask,createUserId_myTask,search_myTask_createUser,'person');"
									onblur="divNode(search_myTask_createUser)" maxLength="32" />
								<!-- 选择 -->
								<span style="cursor: pointer;"
									onclick='selectOrgPosWindow("person","createUserName_myTask","createUserId_myTask")'><img
										style="vertical-align: middle;"
										src="${basePath}images/common/select.gif" /> </span>
								<br />
								<div id="search_myTask_createUser"
									style="display: none; z-index: 1; position: absolute;"></div>
							</td>

							<td style="vertical-align: middle; width: 10%;" align="center">
								<a onClick="toggle(taskSearchText)" style="cursor: pointer;"
									class="packUp"> <img id="packUp"
										src="${basePath}images/iParticipateInProcess/on.gif"
										align="middle" /> <!-- 收起 --> <span id="searchVal"><s:text
											name="packUp" /> </span> </a>
							</td>
						</tr>
						<tr>
							<td height="25px" align="right">
								<!-- 任务类型: -->
								<s:text name="taskTypeC" />
							</td>
							<td align="left">
								<s:select list="listTaskTypeNameBean" listValue="typeName"
									listKey="taskType" value="listTaskTypeNameBean.typeName"
									name="listTaskTypeNameBean.taskType" cssStyle="width: 80%;"
									id="searchTaskType" theme="simple" disabled="false"
									onchange="searchTaskType();">
								</s:select>
							</td>

							<td align="right">
								<!-- 任务阶段: -->
								<s:text name="taskStageC" />
							</td>
							<td align="left">
								<select id="taskType" style="width: 80%;">
									<option value="-1">
										<s:text name="all" />
									</option>
								</select>
								<!-- 流程任务审批阶段 -->
								<s:select list="taskSearchBean.listFlowTaskState"
									listValue="auditLable" listKey="state"
									value="taskSearchBean.listFlowTaskState.auditLable"
									name="taskSearchBean.listFlowTaskState.state"
									cssStyle="width: 80%;display:none" id="flowState"
									theme="simple">
								</s:select>
								<!-- 流程地图任务审批阶段 -->
								<s:select list="taskSearchBean.listFlowMapTaskState"
									listValue="auditLable" listKey="state"
									value="taskSearchBean.listFlowMapTaskState.auditLable"
									name="taskSearchBean.listFlowMapTaskState.state"
									cssStyle="width: 80%;display:none" id="flowMapState"
									theme="simple">
								</s:select>
								<!-- 文件任务审批阶段 -->
								<s:select list="taskSearchBean.listFileTaskState"
									listValue="auditLable" listKey="state"
									value="taskSearchBean.listFileTaskState.auditLable"
									name="taskSearchBean.listFileTaskState.state"
									cssStyle="width: 80%;display:none" id="fileState"
									theme="simple">
								</s:select>
								<!-- 制度任务审批阶段 -->
								<s:select list="taskSearchBean.listRuleTaskState"
									listValue="auditLable" listKey="state"
									value="taskSearchBean.listRuleTaskState.auditLable"
									name="taskSearchBean.listRuleTaskState.state"
									cssStyle="width: 80%;display:none" id="ruleState"
									theme="simple">
								</s:select>
								<!-- 任务类型: 
								<select id="taskStage" style="width: 72%">
									<option value="volvo">
										<s:text name="all" />
									</option>
								</select>-->
							</td>
							<td>
							</td>
						</tr>
						<tr>
							<td align="right">
								<!-- 时间段: -->
								<s:text name="timeC" />
							</td>
							<td align="left">
								<s:textfield readonly="true" cssClass="Wdate" id="startTime"
									cssStyle="width:36%;border-color: #7F9DB9;"
									onclick="WdatePicker();" theme="simple" />
								-
								<s:textfield readonly="true" cssClass="Wdate" id="endTime"
									cssStyle="width:36%;border-color: #7F9DB9;"
									onclick="WdatePicker();" theme="simple" />
								<!-- <input type="checkbox" checked="checked"
									onclick="taskCheckbox();" id="checkbox"> -->
							</td>

							<td></td>
							<td></td>

							<td></td>

						</tr>
					</table>
				</div>
				<!-- 人员选择，隐藏的人员主键ID -->
				<input type="hidden" id="createUserId_myTask" />

				<div id="searchBut" align="center">
					<img style="cursor: pointer;" onclick="myTaskSearch()"
						src="${basePath}images/iParticipateInProcess/search.gif" />
					<img style="cursor: pointer;"
						src="${basePath}images/iParticipateInProcess/reset.gif"
						onclick="myTaskReset()" />
				</div>
			</div>

			<div id="myTask_id">

			</div>
		</div>
	</body>
</html>